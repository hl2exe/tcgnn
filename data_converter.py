import os

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from sample_wrappers import Features, TextExampleList, FeatureFactory, TextMultiGraphExampleList
from utility.log_utils import Logger


class BaseConverter(object):

    def __init__(self, feature_class, is_multilabel=False, max_tokens_limit=None):
        self.feature_class = FeatureFactory.supported_features[feature_class]
        self.max_tokens_limit = max_tokens_limit if max_tokens_limit else 100000
        self.is_multilabel = is_multilabel

    def get_instance_args(self):
        return {
            'is_multilabel': self.is_multilabel,
            'max_tokens_limit': self.max_tokens_limit
        }

    def convert_data(self, examples, tokenizer, label_list, output_file, checkpoint=None,
                     is_training=False, has_labels=True):

        assert issubclass(self.feature_class, Features)

        if is_training:
            Logger.get_logger(__name__).info('Retrieving training set info...this may take a while...')
            self.training_preparation(examples=examples,
                                      label_list=label_list,
                                      tokenizer=tokenizer)

        example_conversion_method = self.feature_class.from_example
        example_feature_method = self.feature_class.get_feature_records

        writer = tf.io.TFRecordWriter(output_file)

        for ex_index, example in enumerate(tqdm(examples, leave=True, position=0)):
            if checkpoint is not None and ex_index % checkpoint == 0:
                Logger.get_logger(__name__).info('Writing example {0} of {1}'.format(ex_index, len(examples)))

            feature = example_conversion_method(example,
                                                label_list,
                                                has_labels=has_labels,
                                                tokenizer=tokenizer,
                                                conversion_args=self.get_conversion_args(),
                                                converter_args=self.get_instance_args())

            features = example_feature_method(feature)

            tf_example = tf.train.Example(features=tf.train.Features(feature=features))
            writer.write(tf_example.SerializeToString())

        writer.close()

    def get_conversion_args(self):
        return {
            'max_seq_length': self.max_seq_length,
            'label_map': self.label_map,
            'num_labels': self.num_labels,
            'feature_class': self.feature_class
        }

    # Computing max text length
    # TODO: we can do better here by defining attribute-defining functions: one for each conversion_args key
    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_seq_length = None
        num_labels = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            text_ids, label_id, label_map = features[0], features[1], features[2]
            features_ids_len = len(text_ids)

            if label_id is not None:
                num_labels = len(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.label_map = features[-1]
        self.max_seq_length = max_seq_length
        self.num_labels = num_labels

    def save_conversion_args(self, filepath, prefix=None):
        if prefix:
            filename = 'converter_info_{}.npy'.format(prefix)
        else:
            filename = 'converter_info.npy'
        filepath = os.path.join(filepath, filename)
        np.save(filepath, self.get_conversion_args())

    @staticmethod
    def load_conversion_args(filepath, prefix=None):
        if prefix:
            filename = 'converter_info_{}.npy'.format(prefix)
        else:
            filename = 'converter_info.npy'
        filepath = os.path.join(filepath, filename)
        return np.load(filepath, allow_pickle=True).item()


class SimpleGraphConverter(BaseConverter):

    def __init__(self, max_graph_nodes_limit=None, is_directional=False, **kwargs):
        super(SimpleGraphConverter, self).__init__(**kwargs)
        self.is_directional = is_directional
        self.max_graph_nodes_limit = max_graph_nodes_limit if max_graph_nodes_limit else 10000000

    def get_instance_args(self):
        instance_args = super(SimpleGraphConverter, self).get_instance_args()
        instance_args['is_directional'] = self.is_directional
        return instance_args

    def get_conversion_args(self):
        conversion_args = dict()
        conversion_args['max_graph_nodes'] = self.max_graph_nodes
        conversion_args['num_labels'] = self.num_labels
        conversion_args['label_map'] = self.label_map
        conversion_args['feature_class'] = self.feature_class

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, label_id, label_map = features

            node_ids_len = len(node_ids)

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

        self.max_graph_nodes = max_graph_nodes
        self.num_labels = num_labels
        self.label_map = label_map


class SimpleDualGraphConverter(SimpleGraphConverter):

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            structure_node_ids, sentence_node_ids, label_id, label_map = features

            node_ids_len = max(len(structure_node_ids), len(sentence_node_ids))

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

        self.max_graph_nodes = max_graph_nodes
        self.num_labels = num_labels
        self.label_map = label_map


class GraphConverter(BaseConverter):

    def __init__(self, max_graph_nodes_limit=None, is_directional=False, **kwargs):
        super(GraphConverter, self).__init__(**kwargs)
        self.is_directional = is_directional
        self.max_graph_nodes_limit = max_graph_nodes_limit if max_graph_nodes_limit else 10000000

    def get_instance_args(self):
        instance_args = super(GraphConverter, self).get_instance_args()
        instance_args['is_directional'] = self.is_directional
        return instance_args

    def get_conversion_args(self):
        conversion_args = dict()
        conversion_args['max_graph_nodes'] = self.max_graph_nodes
        conversion_args['max_graph_edges'] = self.max_graph_edges
        conversion_args['max_graph_depth'] = self.max_graph_depth
        conversion_args['num_labels'] = self.num_labels
        conversion_args['label_map'] = self.label_map
        conversion_args['feature_class'] = self.feature_class

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_edges = None
        max_graph_depth = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, edge_indices, edge_features, \
            node_indices, node_segments, directionality_mask, \
            label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            edges_len = len(edge_features)

            edge_depth_features = [item[0] for item in edge_features]
            graph_depth = max(edge_depth_features)

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_edges is None and is_valid_example:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

            if max_graph_depth is None and is_valid_example:
                max_graph_depth = graph_depth
            elif max_graph_depth < graph_depth and is_valid_example:
                max_graph_depth = graph_depth

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges
        self.max_graph_depth = max_graph_depth
        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class DualGraphConverter(GraphConverter):

    def get_conversion_args(self):
        conversion_args = super(DualGraphConverter, self).get_conversion_args()
        conversion_args['max_graph_subtree_size'] = self.max_graph_subtree_size

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_edges = None
        max_graph_depth = None
        max_graph_subtree_size = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, edge_indices, edge_features, \
            node_indices, node_segments, directionality_mask, subtree_indices, \
            label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            edges_len = len(edge_features)

            edge_depth_features = [item[0] for item in edge_features]
            graph_depth = max(edge_depth_features)

            graph_subtree_size = max(len(subtree_indices[0]), len(subtree_indices[1]))

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_edges is None and is_valid_example:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

            if max_graph_depth is None and is_valid_example:
                max_graph_depth = graph_depth
            elif max_graph_depth < graph_depth and is_valid_example:
                max_graph_depth = graph_depth

            if max_graph_subtree_size is None and is_valid_example:
                max_graph_subtree_size = graph_subtree_size
            elif max_graph_subtree_size < graph_subtree_size and is_valid_example:
                max_graph_subtree_size = graph_subtree_size

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges
        self.max_graph_depth = max_graph_depth
        self.max_graph_subtree_size = max_graph_subtree_size
        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class GraphAdjConverter(BaseConverter):

    def __init__(self, max_graph_nodes_limit=None, is_directional=False, **kwargs):
        super(GraphAdjConverter, self).__init__(**kwargs)
        self.is_directional = is_directional
        self.max_graph_nodes_limit = max_graph_nodes_limit if max_graph_nodes_limit else 10000000

    def get_instance_args(self):
        instance_args = super(GraphAdjConverter, self).get_instance_args()
        instance_args['is_directional'] = self.is_directional
        return instance_args

    def get_conversion_args(self):
        conversion_args = dict()
        conversion_args['max_graph_nodes'] = self.max_graph_nodes
        conversion_args['max_graph_depth'] = self.max_graph_depth
        conversion_args['num_labels'] = self.num_labels
        conversion_args['label_map'] = self.label_map
        conversion_args['feature_class'] = self.feature_class

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_depth = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, edge_features, \
            adjacency_matrix, label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            graph_depth = np.max(edge_features)

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_depth is None and is_valid_example:
                max_graph_depth = graph_depth
            elif is_valid_example and max_graph_depth < graph_depth:
                max_graph_depth = graph_depth

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_depth = max_graph_depth
        self.num_labels = num_labels
        self.label_map = label_map


class MultiGraphAdjConverter(GraphAdjConverter):

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextMultiGraphExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_depth = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids_1, edge_features_1, adjacency_matrix_1, \
            node_ids_2, edge_features_2, adjacency_matrix_2, \
            label_id, label_map = features

            is_valid_example = True

            node_ids_len = max(len(node_ids_1), len(node_ids_2))
            graph_depth = max(np.max(edge_features_1), np.max(edge_features_2))

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_depth is None and is_valid_example:
                max_graph_depth = graph_depth
            elif is_valid_example and max_graph_depth < graph_depth:
                max_graph_depth = graph_depth

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_depth = max_graph_depth
        self.num_labels = num_labels
        self.label_map = label_map


class DualGraphAdjConverter(GraphAdjConverter):

    def get_conversion_args(self):
        conversion_args = dict()
        conversion_args['max_seq_length'] = self.max_seq_length
        conversion_args['max_graph_nodes'] = self.max_graph_nodes
        conversion_args['max_graph_depth'] = self.max_graph_depth
        conversion_args['num_labels'] = self.num_labels
        conversion_args['label_map'] = self.label_map
        conversion_args['feature_class'] = self.feature_class

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_seq_length = None
        max_graph_nodes = None
        max_graph_depth = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            text_ids, node_ids, edge_features, \
            adjacency_matrix, label_id, label_map = features

            text_ids_len = len(text_ids)
            node_ids_len = len(node_ids)
            graph_depth = np.max(edge_features)

            if node_ids_len > self.max_graph_nodes_limit:
                continue

            if label_id is not None:
                num_labels = len(label_id)

            if max_seq_length is None:
                max_seq_length = text_ids_len
            elif max_seq_length < text_ids_len:
                max_seq_length = text_ids_len

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if max_graph_depth is None:
                max_graph_depth = graph_depth
            elif max_graph_depth < graph_depth:
                max_graph_depth = graph_depth

        self.max_seq_length = max_seq_length
        self.max_graph_nodes = max_graph_nodes
        self.max_graph_depth = max_graph_depth
        self.num_labels = num_labels
        self.label_map = label_map


class GraphPooledAdjConverter(GraphAdjConverter):

    def __init__(self, kernel='stk', **kwargs):
        super(GraphPooledAdjConverter, self).__init__(**kwargs)
        self.kernel = kernel

    def get_instance_args(self):
        instance_args = super(GraphPooledAdjConverter, self).get_instance_args()
        instance_args['kernel'] = self.kernel
        return instance_args

    def get_conversion_args(self):
        conversion_args = super(GraphPooledAdjConverter, self).get_conversion_args()
        conversion_args['max_graph_subtrees'] = self.max_graph_subtrees

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_subtrees = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids,\
            adjacency_matrix,\
            assignment_matrix, label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            subtrees_amount = assignment_matrix.shape[1]

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_subtrees is None and is_valid_example:
                max_graph_subtrees = subtrees_amount
            elif max_graph_subtrees < subtrees_amount and is_valid_example:
                max_graph_subtrees = subtrees_amount

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_subtrees = max_graph_subtrees
        self.num_labels = num_labels
        self.label_map = label_map


class GraphTreePooledAdjConverter(GraphAdjConverter):

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        num_labels = None

        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids,\
            adjacency_matrix,\
            node_spans, label_id, label_map = features

            node_ids_len = len(node_ids)

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

        self.max_graph_nodes = max_graph_nodes
        self.num_labels = num_labels
        self.label_map = label_map


class HierarchicalGraphConverter(GraphConverter):

    def __init__(self, kernel='stk', **kwargs):
        super(HierarchicalGraphConverter, self).__init__(**kwargs)
        self.kernel = kernel

    def get_instance_args(self):
        instance_args = super(HierarchicalGraphConverter, self).get_instance_args()
        instance_args['kernel'] = self.kernel
        return instance_args

    def get_conversion_args(self):
        conversion_args = super(HierarchicalGraphConverter, self).get_conversion_args()

        conversion_args['max_graph_subtrees'] = self.max_graph_subtrees
        conversion_args['max_graph_subtree_size'] = self.max_graph_subtree_size
        conversion_args['max_graph_subtree_edges'] = self.max_graph_subtree_edges
        conversion_args['max_graph_subtree_depth'] = self.max_graph_subtree_depth

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        # Level 1: Word level

        max_graph_nodes = None
        max_graph_edges = None
        max_graph_depth = None

        # Level 2: Subtree level

        max_graph_subtrees = None
        max_graph_subtree_size = None
        max_graph_subtree_edges = None
        max_graph_subtree_depth = None

        num_labels = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, edge_indices, edge_features, node_indices, \
            node_segments, directionality_mask, subtree_indices, subtree_edge_indices, \
            subtree_edge_features, subtree_node_indices, subtree_node_segments, \
            subtree_directionality_mask, label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            edges_len = len(edge_features)
            graph_depth = max([len(item) for item in edge_features])

            subtrees_amount = len(subtree_indices)
            subtree_indices_len = max([len(seq) for seq in subtree_indices])
            subtree_edges_len = len(subtree_edge_features)
            subtree_depth = max([len(seq) for seq in subtree_edge_features])

            if label_id is not None:
                num_labels = len(label_id)

            # Level 1: Word level

            if max_graph_nodes is None and node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_edges is None and is_valid_example:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

            if max_graph_depth is None and is_valid_example:
                max_graph_depth = graph_depth
            elif max_graph_depth < graph_depth and is_valid_example:
                max_graph_depth = graph_depth

            # Level 2: Subtree level

            if max_graph_subtrees is None and is_valid_example:
                max_graph_subtrees = subtrees_amount
            elif max_graph_subtrees < subtrees_amount and is_valid_example:
                max_graph_subtrees = subtrees_amount

            if max_graph_subtree_size is None and is_valid_example:
                max_graph_subtree_size = subtree_indices_len
            elif max_graph_subtree_size < subtree_indices_len and is_valid_example:
                max_graph_subtree_size = subtree_indices_len

            if max_graph_subtree_edges is None and is_valid_example:
                max_graph_subtree_edges = subtree_edges_len
            elif max_graph_subtree_edges < subtree_edges_len and is_valid_example:
                max_graph_subtree_edges = subtree_edges_len

            if max_graph_subtree_depth is None and is_valid_example:
                max_graph_subtree_depth = subtree_depth
            elif max_graph_subtree_depth < subtree_depth and is_valid_example:
                max_graph_subtree_depth = subtree_depth

        # Level 1: Word level

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges
        self.max_graph_depth = max_graph_depth

        # Level 2: Subtree level
        self.max_graph_subtrees = max_graph_subtrees
        self.max_graph_subtree_size = max_graph_subtree_size
        self.max_graph_subtree_edges = max_graph_subtree_edges
        self.max_graph_subtree_depth = max_graph_subtree_depth

        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class GraphDepConverter(GraphConverter):

    def get_conversion_args(self):
        conversion_args = dict()
        conversion_args['max_graph_nodes'] = self.max_graph_nodes
        conversion_args['max_graph_edges'] = self.max_graph_edges
        conversion_args['num_labels'] = self.num_labels
        conversion_args['label_map'] = self.label_map
        conversion_args['feature_class'] = self.feature_class

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_edges = None
        num_labels = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, edge_indices, edge_features, \
            node_indices, node_segments, directionality_mask, \
            label_id, label_map = features

            node_ids_len = len(node_ids)
            edges_len = len(edge_features)

            is_valid_example = True

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_edges is None and is_valid_example:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges
        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class HierarchicalGraphDepConverter(GraphDepConverter):

    def __init__(self, kernel='stk', **kwargs):
        super(HierarchicalGraphDepConverter, self).__init__(**kwargs)
        self.kernel = kernel

    def get_instance_args(self):
        instance_args = super(HierarchicalGraphDepConverter, self).get_instance_args()
        instance_args['kernel'] = self.kernel
        return instance_args

    def get_conversion_args(self):
        conversion_args = super(HierarchicalGraphDepConverter, self).get_conversion_args()

        conversion_args['max_graph_subtrees'] = self.max_graph_subtrees
        conversion_args['max_graph_subtree_size'] = self.max_graph_subtree_size
        conversion_args['max_graph_subtree_edges'] = self.max_graph_subtree_edges
        conversion_args['max_graph_subtree_depth'] = self.max_graph_subtree_depth

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        # Level 1: Word level

        max_graph_nodes = None
        max_graph_edges = None

        # Level 2: Subtree level

        max_graph_subtrees = None
        max_graph_subtree_size = None
        max_graph_subtree_edges = None
        max_graph_subtree_depth = None

        num_labels = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, edge_indices, edge_features, node_indices, \
            node_segments, directionality_mask, subtree_indices, subtree_edge_indices, \
            subtree_edge_features, subtree_node_indices, subtree_node_segments, \
            subtree_directionality_mask, label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            edges_len = len(edge_features)

            subtrees_amount = len(subtree_indices)
            subtree_indices_len = max([len(seq) for seq in subtree_indices])
            subtree_edges_len = len(subtree_edge_features)
            if subtree_edge_features:
                subtree_depth = max([len(seq) for seq in subtree_edge_features])

            if label_id is not None:
                num_labels = len(label_id)

            # Level 1: Word level

            if max_graph_nodes is None and node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_edges is None and is_valid_example:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

            # Level 2: Subtree level

            if max_graph_subtrees is None and is_valid_example:
                max_graph_subtrees = subtrees_amount
            elif max_graph_subtrees < subtrees_amount and is_valid_example:
                max_graph_subtrees = subtrees_amount

            if max_graph_subtree_size is None and is_valid_example:
                max_graph_subtree_size = subtree_indices_len
            elif max_graph_subtree_size < subtree_indices_len and is_valid_example:
                max_graph_subtree_size = subtree_indices_len

            if max_graph_subtree_edges is None and is_valid_example:
                max_graph_subtree_edges = subtree_edges_len
            elif max_graph_subtree_edges < subtree_edges_len and is_valid_example:
                max_graph_subtree_edges = subtree_edges_len

            if max_graph_subtree_depth is None and is_valid_example:
                max_graph_subtree_depth = subtree_depth
            elif max_graph_subtree_depth < subtree_depth and is_valid_example:
                max_graph_subtree_depth = subtree_depth

        # Level 1: Word level

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges

        # Level 2: Subtree level
        self.max_graph_subtrees = max_graph_subtrees
        self.max_graph_subtree_size = max_graph_subtree_size
        self.max_graph_subtree_edges = max_graph_subtree_edges
        self.max_graph_subtree_depth = max_graph_subtree_depth

        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class GraphCharConverter(GraphConverter):

    def __init__(self, max_chars_length=20, **kwargs):
        super(GraphCharConverter, self).__init__(**kwargs)
        self.max_chars_length = max_chars_length

    def get_conversion_args(self):
        conversion_args = super(GraphCharConverter, self).get_conversion_args()
        conversion_args['max_graph_chars'] = self.max_graph_chars

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_edges = None
        max_graph_depth = None
        max_graph_chars = None
        num_labels = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, node_chars_ids, edge_indices, edge_features, \
            node_indices, node_segments, directionality_mask, \
            label_id, label_map = features

            is_valid_example = True

            node_ids_len = len(node_ids)
            chars_ids_len = [len(item) for item in node_chars_ids]
            chars_ids_max_len = max(chars_ids_len)
            edges_len = len(edge_features)
            graph_depth = max([len(item) for item in edge_features])

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len
            else:
                is_valid_example = False

            if max_graph_edges is None:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

            if max_graph_depth is None:
                max_graph_depth = graph_depth
            elif max_graph_depth < graph_depth and is_valid_example:
                max_graph_depth = graph_depth

            if max_graph_chars is None:
                max_graph_chars = chars_ids_max_len
            elif max_graph_chars < chars_ids_max_len and is_valid_example:
                max_graph_chars = chars_ids_max_len

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges
        self.max_graph_depth = max_graph_depth
        self.max_graph_chars = min(max_graph_chars, self.max_chars_length)
        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class GraphDepCharConverter(GraphDepConverter):

    def __init__(self, max_chars_length=20, **kwargs):
        super(GraphDepCharConverter, self).__init__(**kwargs)
        self.max_chars_length = max_chars_length

    def get_conversion_args(self):
        conversion_args = super(GraphDepCharConverter, self).get_conversion_args()
        conversion_args['max_graph_chars'] = self.max_graph_chars

        return conversion_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_graph_nodes = None
        max_graph_edges = None
        max_graph_chars = None
        num_labels = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args())
            node_ids, node_chars_ids, edge_indices, edge_features, edge_char_features_ids, \
            node_indices, node_segments, directionality_mask, \
            label_id, label_map = features

            node_ids_len = len(node_ids)
            edges_len = len(edge_features)
            chars_ids_len = [len(item) for item in node_chars_ids]
            feature_chars_ids_len = [len(item) for item in edge_char_features_ids]
            node_chars_ids_max_len = max(chars_ids_len)
            feature_chars_ids_max_len = max(feature_chars_ids_len)
            chars_ids_max_len = max(node_chars_ids_max_len, feature_chars_ids_max_len)

            is_valid_example = True

            if label_id is not None:
                num_labels = len(label_id)

            if max_graph_nodes is None:
                max_graph_nodes = node_ids_len
            elif max_graph_nodes < node_ids_len <= self.max_graph_nodes_limit:
                max_graph_nodes = node_ids_len

            if node_ids_len > self.max_graph_nodes_limit:
                is_valid_example = False

            if max_graph_edges is None and is_valid_example:
                max_graph_edges = edges_len
            elif max_graph_edges < edges_len and is_valid_example:
                max_graph_edges = edges_len

            if max_graph_chars is None:
                max_graph_chars = chars_ids_max_len
            elif max_graph_chars < chars_ids_max_len and is_valid_example:
                max_graph_chars = chars_ids_max_len

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_edges = max_graph_edges
        self.max_graph_chars = min(max_graph_chars, self.max_chars_length)
        self.num_labels = num_labels
        self.label_map = label_map

        if max_graph_edges != max_graph_nodes - 1:
            Logger.get_logger(__name__).warn('Number of edges is different from number of nodes minus 1! Is this ok?')


class BertConverter(BaseConverter):

    def __init__(self, mask_padding_with_zero=True, pad_on_left=False,
                 pad_token=0, pad_token_segment_id=0, **kwargs):
        super(BertConverter, self).__init__(**kwargs)
        self.mask_padding_with_zero = mask_padding_with_zero
        self.pad_on_left = pad_on_left
        self.pad_token = pad_token
        self.pad_token_segment_id = pad_token_segment_id

    def get_instance_args(self):
        instance_args = super(BertConverter, self).get_instance_args()
        instance_args['mask_padding_with_zero'] = self.mask_padding_with_zero
        instance_args['pad_on_left'] = self.pad_on_left
        instance_args['pad_token'] = self.pad_token
        instance_args['pad_token_segment_id'] = self.pad_token_segment_id

        return instance_args

    def training_preparation(self, examples, label_list, tokenizer):
        assert isinstance(examples, TextExampleList)

        max_seq_length = None
        num_labels = None

        for example in tqdm(examples):

            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args())
            input_ids, attention_mask, label_id, label_map = features
            features_ids_len = len(input_ids)

            if label_id is not None:
                num_labels = len(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.max_seq_length = max_seq_length
        self.num_labels = num_labels
        self.label_map = label_map


class DataConverterFactory(object):
    supported_data_converters = {
        'base_converter': BaseConverter,
        'simple_graph_converter': SimpleGraphConverter,
        'simple_dual_graph_converter': SimpleDualGraphConverter,
        'graph_converter': GraphConverter,
        'dual_graph_converter': DualGraphConverter,
        'graph_adj_converter': GraphAdjConverter,
        'graph_pooled_adj_converter': GraphPooledAdjConverter,
        'graph_tree_pooled_adj_converter': GraphTreePooledAdjConverter,
        'hierarchical_graph_converter': HierarchicalGraphConverter,
        'hierarchical_graph_dep_converter': HierarchicalGraphDepConverter,
        'graph_dep_converter': GraphDepConverter,
        'graph_char_converter': GraphCharConverter,
        'graph_dep_char_converter': GraphDepCharConverter,

        'dual_graph_adj_converter': DualGraphAdjConverter,

        'multi_graph_adj_converter': MultiGraphAdjConverter,

        'bert_converter': BertConverter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if DataConverterFactory.supported_data_converters[key]:
            return DataConverterFactory.supported_data_converters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
