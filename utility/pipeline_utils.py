import tensorflow as tf
from utility.python_utils import merge
import numpy as np


def decode_record(record, name_to_features):
    """
    TPU does not support int64
    """

    example = tf.io.parse_single_example(record, name_to_features)

    for name in list(example.keys()):
        t = example[name]
        if t.dtype == tf.int64:
            t = tf.cast(t, tf.int32)
        example[name] = t

    return example


def load_single_dataset(filepath, name_to_features):
    data = tf.data.TFRecordDataset(filepath)
    data = data.map(lambda record: decode_record(record, name_to_features))

    # Disabling auto-sharding -> each worker receives the full dataset
    # if isinstance(filepath, str) or len(filepath) == 1:
    # options = tf.data.Options()
    # options.experimental_distribute.auto_shard_policy = (
    #     tf.data.experimental.AutoShardPolicy.OFF)
    # data = data.with_options(options)
    return data


def create_dataset(filepath, batch_size, name_to_features, selector, is_training=True,
                   input_pipeline_context=None, shuffle_amount=10000, prefetch_amount=1024,
                   reshuffle_each_iteration=True, filter_func=None):
    dataset = load_single_dataset(filepath=filepath, name_to_features=name_to_features)

    # Dataset is sharded by the number of hosts (num_input_pipelines == num_hosts)
    if input_pipeline_context and input_pipeline_context.num_input_pipelines > 1:
        dataset = dataset.shard(input_pipeline_context.num_input_pipelines,
                                input_pipeline_context.input_pipeline_id)

    dataset = dataset.map(selector)

    if filter_func is not None:
        dataset = dataset.filter(filter_func)

    if is_training:
        dataset = dataset.shuffle(shuffle_amount, reshuffle_each_iteration=reshuffle_each_iteration)
        dataset = dataset.repeat()

    dataset = dataset.batch(batch_size, drop_remainder=is_training)
    dataset = dataset.prefetch(prefetch_amount)

    return dataset


def create_pairwise_dataset(filepath, batch_size, name_to_features, selector, is_training=True,
                            input_pipeline_context=None, shuffle_amount=10000, prefetch_amount=1024,
                            reshuffle_each_iteration=True, pairwise_labels=None, label_map=None):

    if pairwise_labels is not None:
        assert label_map is not None

        mapped_pairwise_labels = [np.argmax(label_map[item]) for item in pairwise_labels]

        def filter_by_label(x, y):
            is_allowed = tf.equal(tf.cast(tf.argmax(y), tf.int32),
                                  tf.cast(tf.constant(mapped_pairwise_labels), tf.int32))
            reduced = tf.reduce_sum(tf.cast(is_allowed, tf.float32))
            return tf.greater(reduced, tf.constant(0.))

        filter_func = filter_by_label
    else:
        filter_func = None

    d1 = create_dataset(filepath, batch_size, name_to_features, selector, is_training,
                        input_pipeline_context, shuffle_amount, prefetch_amount,
                        reshuffle_each_iteration, filter_func=filter_func)
    d2 = create_dataset(filepath, batch_size, name_to_features, selector, is_training,
                        input_pipeline_context, shuffle_amount, prefetch_amount,
                        reshuffle_each_iteration)

    dataset = tf.data.Dataset.zip((d1, d2))
    dataset = dataset.map(lambda a, b: (merge({'left_{}'.format(key): value for key, value in a[0].items()},
                                              {'right_{}'.format(key): value for key, value in b[0].items()},
                                              deepcopy=False),
                                        tf.where(tf.argmax(a[1], axis=1) == tf.argmax(b[1], axis=1),
                                                 1,
                                                 -1)
                                        ))

    return dataset


def transform_to_pairwise(data_function, other_data_function=None, is_callable=True):

    if other_data_function is None:
        d1 = data_function
        d2 = data_function
    else:
        d1 = data_function
        d2 = other_data_function

    if is_callable:
        dataset = tf.data.Dataset.zip((d1(), d2()))
    else:
        dataset = tf.data.Dataset.zip((d1, d2))
    dataset = dataset.map(lambda a, b: (merge({'left_{}'.format(key): value for key, value in a[0].items()},
                                              {'right_{}'.format(key): value for key, value in b[0].items()},
                                              deepcopy=False),
                                        tf.where(tf.argmax(a[1], axis=1) == tf.argmax(b[1], axis=1),
                                                 1,
                                                 -1)
                                        ))

    return dataset


def get_dataset_fn(filepath, batch_size, name_to_features, selector, is_training=True,
                   shuffle_amount=1000, prefetch_amount=1024,
                   reshuffle_each_iteration=True):
    """Gets a closure to create a dataset."""

    def _dataset_fn(ctx=None):
        """Returns tf.data.Dataset"""
        bs = ctx.get_per_replica_batch_size(batch_size) if ctx else batch_size
        dataset = create_dataset(filepath=filepath, batch_size=bs,
                                 name_to_features=name_to_features,
                                 selector=selector,
                                 is_training=is_training,
                                 input_pipeline_context=ctx,
                                 shuffle_amount=shuffle_amount,
                                 reshuffle_each_iteration=reshuffle_each_iteration,
                                 prefetch_amount=prefetch_amount)
        return dataset

    return _dataset_fn


def get_pairwise_dataset_fn(filepath, batch_size, name_to_features, selector, is_training=True,
                            pairwise_labels=None, label_map=None, shuffle_amount=1000, prefetch_amount=1024,
                            reshuffle_each_iteration=True):
    def _dataset_fn(ctx=None):
        """Returns tf.data.Dataset"""
        bs = ctx.get_per_replica_batch_size(batch_size) if ctx else batch_size
        dataset = create_pairwise_dataset(filepath=filepath, batch_size=bs,
                                          name_to_features=name_to_features,
                                          selector=selector,
                                          is_training=is_training,
                                          input_pipeline_context=ctx,
                                          shuffle_amount=shuffle_amount,
                                          reshuffle_each_iteration=reshuffle_each_iteration,
                                          prefetch_amount=prefetch_amount,
                                          pairwise_labels=pairwise_labels,
                                          label_map=label_map)
        return dataset

    return _dataset_fn
