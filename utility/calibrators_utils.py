
from itertools import product


def get_gridsearch_parameters(param_dict):
    """
    Builds parameters combinations

    :param param_dict: dictionary that has parameter names as keys and the list of possible values as values
    (see model_gridsearch.json for more information)
    :return: list of dictionaries, each describing a parameters combination
    """

    params_combinations = []

    keys = sorted(param_dict)
    comb_tuples = product(*(param_dict[key] for key in keys))

    for comb_tuple in comb_tuples:
        instance_params = {dict_key: comb_item for dict_key, comb_item in zip(keys, comb_tuple)}
        params_combinations.append(instance_params)

    return params_combinations