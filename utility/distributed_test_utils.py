"""

@Author: Federico Ruggeri

@Date: 24/09/2019

"""

from __future__ import division

import os
from collections import OrderedDict
from copy import deepcopy

import numpy as np
from tensorflow.python.keras import backend as K

import const_define as cd
from custom_callbacks_v2 import TensorBoard
from data_converter import DataConverterFactory
from data_processor import ProcessorFactory
from nn_models_v2 import ModelFactory
from tokenization import TokenizerFactory
from utility.cross_validation_utils import build_metrics, compute_iteration_validation_error, update_cv_validation_info
from utility.data_utils import get_data_config_id, clear_data_config
from utility.json_utils import save_json
from utility.log_utils import Logger
from utility.pipeline_utils import get_dataset_fn, get_pairwise_dataset_fn
from utility.python_utils import flatten, merge


# TODO: add support for external test (build_validation argument)
def distributed_cross_validation(validation_percentage, data_handle, cv, strategy, data_loader_info,
                                 model_type, network_args, training_config, test_path,
                                 error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                                 callbacks=None, compute_test_info=True, save_predictions=False,
                                 use_tensorboard=False, repetitions=1, save_model=False,
                                 split_key=None, checkpoint=None, distributed_info=None):
    """
    [Repeated] Cross-validation routine:

        1. [For each repetition]

        2. For each fold:
            2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
            2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
            2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
            2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
            2E. Model definition: a network model (nn_models_v2.py) is built
            2F. Model training: the network is trained.
            2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets

        3. Results post-processing: macro-average values are computed.
    """

    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        Logger.get_logger(__name__).error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 2: cross validation
    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        prediction_info = OrderedDict()

        for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
            Logger.get_logger(__name__).info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=test_indexes,
                                                              val_indexes=val_indexes,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
            val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
            test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

            save_prefix = 'fold_{}'.format(fold_idx)

            if not os.path.isfile(test_filepath):
                Logger.get_logger(__name__).info(
                    'Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            else:
                tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                  prefix=save_prefix)
                converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            selector = converter.feature_class.get_dataset_selector()
            name_to_features = converter.feature_class.get_mappings(converter_info)

            with strategy.scope():
                train_data = get_dataset_fn(filepath=train_filepath,
                                            batch_size=training_config['batch_size'],
                                            name_to_features=name_to_features,
                                            selector=selector,
                                            is_training=True,
                                            shuffle_amount=distributed_info['shuffle_amount'],
                                            reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                            prefetch_amount=distributed_info['prefetch_amount'])

                fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                                  batch_size=training_config['batch_size'],
                                                  name_to_features=name_to_features,
                                                  selector=selector,
                                                  is_training=False,
                                                  prefetch_amount=distributed_info['prefetch_amount'])

                val_data = get_dataset_fn(filepath=val_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=name_to_features,
                                          selector=selector,
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

                test_data = get_dataset_fn(filepath=test_filepath,
                                           batch_size=training_config['batch_size'],
                                           name_to_features=name_to_features,
                                           selector=selector,
                                           is_training=False,
                                           prefetch_amount=distributed_info['prefetch_amount'])

                # Build model

                network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                          if 'model_class' in value['flags']}
                network_retrieved_args['additional_data'] = data_handle.get_additional_info()
                network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                    cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
                network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

                # Useful stuff
                train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
                eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
                test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

                np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
                np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
                np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

                Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
                Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
                Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

                # computing positive label weights (for unbalanced dataset)
                network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                               mode='multi-label' if network.is_multilabel else 'multi-class')

                # Custom callbacks only
                for callback in callbacks:
                    if hasattr(callback, 'on_build_model_begin'):
                        callback.on_build_model_begin(logs={'network': network})

                text_info = merge(tokenizer_info, converter_info)
                text_info = merge(text_info, training_config)
                network.build_model(text_info=text_info)

                # Custom callbacks only
                for callback in callbacks:
                    if hasattr(callback, 'on_build_model_end'):
                        callback.on_build_model_end(logs={'network': network})

                if use_tensorboard:
                    fold_log_dir = os.path.join(tensorboard_base_dir,
                                                'repetition_{}'.format(repetition),
                                                'fold_{}'.format(fold_idx))
                    os.makedirs(fold_log_dir)
                    tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                              log_dir=fold_log_dir)
                    fold_callbacks = callbacks + [tensorboard]
                else:
                    fold_callbacks = callbacks

                # Training
                network.distributed_fit(train_data=train_data,
                                        epochs=training_config['epochs'], verbose=training_config['verbose'],
                                        callbacks=fold_callbacks, validation_data=val_data,
                                        strategy=strategy, step_checkpoint=training_config['step_checkpoint'],
                                        metrics=training_config['metrics'],
                                        additional_metrics_info=training_config['additional_metrics_info'],
                                        metrics_nicknames=training_config['metrics_nicknames'],
                                        train_num_batches=train_steps,
                                        eval_num_batches=eval_steps,
                                        np_val_y=np_val_y)

            # Inference
            val_predictions = network.distributed_predict(x=network._get_input_iterator(val_data, strategy),
                                                          steps=eval_steps,
                                                          callbacks=fold_callbacks,
                                                          strategy=strategy,
                                                          suffix='val')

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            test_predictions = network.distributed_predict(x=network._get_input_iterator(test_data, strategy),
                                                           steps=test_steps,
                                                           callbacks=fold_callbacks,
                                                           strategy=strategy,
                                                           suffix='test')

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

                if save_predictions:
                    prediction_info[fold_idx] = test_predictions.ravel()

            # Save model
            if save_model:
                filepath = os.path.join(test_path,
                                        '{0}_repetition_{1}_fold_{2}'.format(
                                            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                            repetition,
                                            fold_idx))
                network.save(filepath=filepath)

                filepath = os.path.join(test_path, 'y_test_fold_{}.json'.format(fold_idx))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=np_test_y)

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        if save_predictions:
            for key, item in prediction_info.items():
                total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        if save_predictions:
            total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if save_predictions:
        result['predictions'] = total_preds

    if compute_test_info:
        result['test_info'] = total_test_info

    return result


def cross_validation(validation_percentage, data_handle, cv, data_loader_info,
                     model_type, network_args, training_config, test_path,
                     error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                     callbacks=None, compute_test_info=True, save_predictions=False,
                     use_tensorboard=False, repetitions=1, save_model=False,
                     split_key=None, checkpoint=None, distributed_info=None):
    """
    [Repeated] Cross-validation routine:

        1. [For each repetition]

        2. For each fold:
            2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
            2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
            2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
            2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
            2E. Model definition: a network model (nn_models_v2.py) is built
            2F. Model training: the network is trained.
            2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets

        3. Results post-processing: macro-average values are computed.
    """

    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        Logger.get_logger(__name__).error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Step 2: cross validation
    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
            Logger.get_logger(__name__).info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=test_indexes,
                                                              val_indexes=val_indexes,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
            val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
            test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

            save_prefix = 'fold_{}'.format(fold_idx)

            if not os.path.isfile(test_filepath):
                Logger.get_logger(__name__).info(
                    'Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            else:
                tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                  prefix=save_prefix)
                converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            train_data = get_dataset_fn(filepath=train_filepath,
                                        batch_size=training_config['batch_size'],
                                        name_to_features=converter.feature_class.get_mappings(converter_info),
                                        selector=converter.feature_class.get_dataset_selector(),
                                        is_training=True,
                                        shuffle_amount=distributed_info['shuffle_amount'],
                                        reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                        prefetch_amount=distributed_info['prefetch_amount'])

            fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                              batch_size=training_config['batch_size'],
                                              name_to_features=converter.feature_class.get_mappings(converter_info),
                                              selector=converter.feature_class.get_dataset_selector(),
                                              is_training=False,
                                              prefetch_amount=distributed_info['prefetch_amount'])

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Build model

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

            np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

            Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

            # computing positive label weights (for unbalanced dataset)
            network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                           mode='multi-label' if network.is_multilabel else 'multi-class')

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            text_info = merge(text_info, training_config)
            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            if use_tensorboard:
                fold_log_dir = os.path.join(tensorboard_base_dir,
                                            'repetition_{}'.format(repetition),
                                            'fold_{}'.format(fold_idx))
                os.makedirs(fold_log_dir)
                tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                          log_dir=fold_log_dir,
                                          )
                fold_callbacks = callbacks + [tensorboard]
            else:
                fold_callbacks = callbacks

            # Training
            network.fit(train_data=train_data,
                        epochs=training_config['epochs'], verbose=training_config['verbose'],
                        callbacks=fold_callbacks, validation_data=val_data,
                        step_checkpoint=training_config['step_checkpoint'],
                        metrics=training_config['metrics'],
                        additional_metrics_info=training_config['additional_metrics_info'],
                        metrics_nicknames=training_config['metrics_nicknames'],
                        train_num_batches=train_steps,
                        eval_num_batches=eval_steps,
                        np_val_y=np_val_y)

            # Inference
            val_predictions = network.predict(x=iter(val_data()),
                                              steps=eval_steps,
                                              callbacks=fold_callbacks)

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            test_predictions = network.predict(x=iter(test_data()),
                                               steps=test_steps,
                                               callbacks=callbacks)

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

                if save_predictions:
                    all_preds[fold_idx] = test_predictions.ravel()

            # Save model
            if save_model:
                filepath = os.path.join(test_path,
                                        '{0}_repetition_{1}_fold_{2}'.format(
                                            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                            repetition,
                                            fold_idx))
                network.save(filepath=filepath)

                filepath = os.path.join(test_path, 'y_test_fold_{}.json'.format(fold_idx))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=np_test_y)

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        if save_predictions:
            result['predictions'] = total_preds
    else:
        if save_predictions:
            result['predictions'] = total_preds

    return result


def cross_validation_forward(validation_percentage, data_handle, cv, data_loader_info,
                             model_type, network_args, training_config, test_path,
                             error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                             callbacks=None, compute_test_info=True, save_predictions=False,
                             repetitions=1,
                             split_key=None, distributed_info=None):
    """
    Simple CV variant that retrieves a pre-trained model to do the forward pass for each val/test fold sets.
    """

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 1: cross validation

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_train_preds = OrderedDict()
    total_val_preds = OrderedDict()
    total_test_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_train_preds = OrderedDict()
        all_val_preds = OrderedDict()
        all_test_preds = OrderedDict()

        for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
            Logger.get_logger(__name__).info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=test_indexes,
                                                              val_indexes=val_indexes,
                                                              validation_percentage=validation_percentage)

            test_df.to_csv(os.path.join(test_path, 'test_df_fold_{}.csv'.format(fold_idx)), index=None)

            train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
            val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
            test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

            save_prefix = 'fold_{}'.format(fold_idx)

            if not os.path.isfile(test_filepath):
                Logger.get_logger(__name__).info(
                    'Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer)
            else:
                tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                  prefix=save_prefix)
                converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                              batch_size=training_config['batch_size'],
                                              name_to_features=converter.feature_class.get_mappings(converter_info),
                                              selector=converter.feature_class.get_dataset_selector(),
                                              is_training=False,
                                              prefetch_amount=distributed_info['prefetch_amount'])

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Build model

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

            Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            text_info = merge(text_info, training_config)
            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            # Setup model by feeding an input
            network.predict(x=iter(val_data()), steps=1)

            # load pre-trained weights
            current_weight_filename = os.path.join(test_path,
                                                   '{0}_repetition_{1}_fold_{2}.h5'.format(
                                                       cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                                       repetition,
                                                       fold_idx))
            network.load(os.path.join(test_path, current_weight_filename))

            # Inference
            train_predictions = network.predict(x=iter(fixed_train_data()),
                                                steps=train_steps,
                                                callbacks=callbacks,
                                                suffix='train')

            all_train_preds[fold_idx] = train_predictions

            val_predictions = network.predict(x=iter(val_data()),
                                              steps=eval_steps,
                                              callbacks=callbacks,
                                              suffix='val')

            all_val_preds[fold_idx] = val_predictions

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            test_predictions = network.predict(x=iter(test_data()),
                                               steps=test_steps,
                                               callbacks=callbacks,
                                               suffix='test')

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

                if save_predictions:
                    all_test_preds[fold_idx] = test_predictions

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_train_preds.items():
            total_train_preds.setdefault(key, []).append(item)
        for key, item in all_val_preds.items():
            total_val_preds.setdefault(key, []).append(item)
        for key, item in all_test_preds.items():
            total_test_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_train_preds = {key: np.mean(item, 0) for key, item in total_train_preds.items()}
        total_val_preds = {key: np.mean(item, 0) for key, item in total_val_preds.items()}
        total_test_preds = {key: np.mean(item, 0) for key, item in total_test_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['train_predictions'] = total_train_preds
        result['val_predictions'] = total_val_preds
        result['test_predictions'] = total_test_preds
    else:
        result['train_predictions'] = total_train_preds
        result['val_predictions'] = total_val_preds
        result['test_predictions'] = total_test_preds

    return result


def distributed_loo_test(data_handle, test_path, data_loader_info, loo, strategy,
                         model_type, network_args, training_config,
                         error_metrics, error_metrics_nicknames=None, error_metrics_additional_info=None,
                         callbacks=[], compute_test_info=True, save_predictions=False, save_model=False, split_key=None,
                         validation_percentage=None, use_tensorboard=False, checkpoint=None, distributed_info=None,
                         repetitions=1, clear_afterwards=False):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    split_values = data_handle.get_loo_data(split_key)

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        prediction_info = OrderedDict()

        for train_keys_indexes, excluded_key_indexes in loo.split(split_values):

            # train_keys = split_values[train_keys_indexes]
            excluded_key = split_values[excluded_key_indexes]

            if len(excluded_key) == 1:
                excluded_key = excluded_key[0]

            Logger.get_logger(__name__).info('Excluding: {}'.format(excluded_key))

            # Data conversion

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=excluded_key,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_loo_{}'.format(excluded_key))
            val_filepath = os.path.join(model_path, 'val_data_loo_{}'.format(excluded_key))
            test_filepath = os.path.join(model_path, 'test_data_loo_{}'.format(excluded_key))

            save_prefix = 'loo_{}'.format(excluded_key)

            if not os.path.isfile(test_filepath):
                Logger.get_logger(__name__).info(
                    'Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            else:
                tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                  prefix=save_prefix)
                converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            selector = converter.feature_class.get_dataset_selector()
            name_to_features = converter.feature_class.get_mappings(converter_info)

            train_data = get_dataset_fn(filepath=train_filepath,
                                        batch_size=training_config['batch_size'],
                                        name_to_features=name_to_features,
                                        selector=selector,
                                        is_training=True,
                                        shuffle_amount=distributed_info['shuffle_amount'],
                                        reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                        prefetch_amount=distributed_info['prefetch_amount'])

            fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                              batch_size=training_config['batch_size'],
                                              name_to_features=name_to_features,
                                              selector=selector,
                                              is_training=False,
                                              prefetch_amount=distributed_info['prefetch_amount'])

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=name_to_features,
                                      selector=selector,
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=name_to_features,
                                       selector=selector,
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Useful stuff
            train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

            np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

            Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

            # Building network

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
            if repetitions > 1:
                network_retrieved_args['name'] += '_repetition_{}'.format(repetition)
            network_retrieved_args['name'] += '_loo_{}'.format(excluded_key)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            with strategy.scope():

                # computing positive label weights (for unbalanced dataset)
                network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                               mode='multi-label' if network.is_multilabel else 'multi-class')

                # Custom callbacks only
                for callback in callbacks:
                    if hasattr(callback, 'on_build_model_begin'):
                        callback.on_build_model_begin(logs={'network': network})

                text_info = merge(tokenizer_info, converter_info)
                text_info = merge(text_info, training_config)
                network.build_model(text_info=text_info)

                # Custom callbacks only
                for callback in callbacks:
                    if hasattr(callback, 'on_build_model_end'):
                        callback.on_build_model_end(logs={'network': network})

                if use_tensorboard:
                    if repetitions > 1:
                        fold_log_dir = os.path.join(tensorboard_base_dir,
                                                    'repetition_{}'.format(repetition),
                                                    'loo_{}'.format(excluded_key))
                    else:
                        fold_log_dir = os.path.join(tensorboard_base_dir,
                                                    'loo_{}'.format(excluded_key))
                    os.makedirs(fold_log_dir)
                    tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                              log_dir=fold_log_dir)
                    fold_callbacks = callbacks + [tensorboard]
                else:
                    fold_callbacks = callbacks

                # Training
                network.distributed_fit(train_data=train_data,
                                        epochs=training_config['epochs'], verbose=training_config['verbose'],
                                        callbacks=fold_callbacks, validation_data=val_data,
                                        strategy=strategy, step_checkpoint=training_config['step_checkpoint'],
                                        metrics=training_config['metrics'],
                                        additional_metrics_info=training_config['additional_metrics_info'],
                                        metrics_nicknames=training_config['metrics_nicknames'],
                                        train_num_batches=train_steps,
                                        eval_num_batches=eval_steps,
                                        np_val_y=np_val_y)

            # Inference
            val_predictions = network.distributed_predict(x=network._get_input_iterator(val_data, strategy),
                                                          steps=eval_steps,
                                                          callbacks=fold_callbacks,
                                                          strategy=strategy)

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            if compute_test_info:
                test_predictions = network.distributed_predict(x=network._get_input_iterator(test_data, strategy),
                                                               steps=test_steps,
                                                               callbacks=callbacks,
                                                               strategy=strategy)

                if save_predictions:
                    prediction_info[str(excluded_key)] = test_predictions

                iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                          true_values=np_test_y,
                                                                          predicted_values=test_predictions,
                                                                          error_metrics_additional_info=error_metrics_additional_info,
                                                                          error_metrics_nicknames=error_metrics_nicknames)

                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

            # Save model
            if save_model:
                filename = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
                if repetitions > 1:
                    filename += '_repetition_{}'.format(repetition)
                filename += '_key_{}'.format(excluded_key)
                filepath = os.path.join(test_path, filename)
                network.save(filepath=filepath)

                # kb_path = os.path.join(test_path,
                #                        '{0}_key_{1}_kb.npy'.format(cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                #                                                    excluded_key))
                # np.save(kb_path, network.knowledge)

                # Save ground truth
                filepath = os.path.join(test_path, 'y_test_key_{}.json'.format(excluded_key))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=np_test_y)

            # Flush
            K.clear_session()

            # Clear memory
            del network

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        if save_predictions:
            for key, item in prediction_info.items():
                total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        if save_predictions:
            total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    if clear_afterwards:
        Logger.get_logger(__name__).info('Clearing tests data to save disk space...')
        clear_data_config(filepath=model_base_path, config=config_name, config_id=config_id)

    result = {
        'validation_info': total_validation_info,
    }

    if save_predictions:
        result['predictions'] = total_preds

    if compute_test_info:
        result['test_info'] = total_test_info

    return result


def loo_test(data_handle, test_path, data_loader_info, loo,
             callbacks, model_type, network_args, training_config,
             error_metrics, error_metrics_nicknames=None, error_metrics_additional_info=None,
             compute_test_info=True, save_model=False, split_key=None, validation_percentage=None,
             use_tensorboard=False, checkpoint=None, distributed_info=None, repetitions=1, clear_afterwards=False):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Step 2: LOO test

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    split_values = data_handle.get_loo_data(split_key)

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        prediction_info = OrderedDict()

        for train_keys_indexes, excluded_key_indexes in loo.split(split_values):

            excluded_key = split_values[excluded_key_indexes]

            if len(excluded_key) == 1:
                excluded_key = excluded_key[0]

            Logger.get_logger(__name__).info('Excluding: {}'.format(excluded_key))

            # Data conversion

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=excluded_key,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_loo_{}'.format(excluded_key))
            val_filepath = os.path.join(model_path, 'val_data_loo_{}'.format(excluded_key))
            test_filepath = os.path.join(model_path, 'test_data_loo_{}'.format(excluded_key))

            save_prefix = 'loo_{}'.format(excluded_key)

            if not os.path.isfile(test_filepath):
                Logger.get_logger(__name__).info(
                    'Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            else:
                tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                  prefix=save_prefix)
                converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            train_data = get_dataset_fn(filepath=train_filepath,
                                        batch_size=training_config['batch_size'],
                                        name_to_features=converter.feature_class.get_mappings(converter_info),
                                        selector=converter.feature_class.get_dataset_selector(),
                                        is_training=True,
                                        shuffle_amount=distributed_info['shuffle_amount'],
                                        reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                        prefetch_amount=distributed_info['prefetch_amount'])

            fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                              batch_size=training_config['batch_size'],
                                              name_to_features=converter.feature_class.get_mappings(converter_info),
                                              selector=converter.feature_class.get_dataset_selector(),
                                              is_training=False,
                                              prefetch_amount=distributed_info['prefetch_amount'])

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Building network

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
            if repetitions > 1:
                network_retrieved_args['name'] += '_repetition_{}'.format(repetition)
            network_retrieved_args['name'] += '_loo_{}'.format(excluded_key)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

            np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

            Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

            # computing positive label weights (for unbalanced dataset)
            network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                           mode='multi-label' if network.is_multilabel else 'multi-class')

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            text_info = merge(text_info, training_config)

            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            if use_tensorboard:
                if repetitions > 1:
                    fold_log_dir = os.path.join(tensorboard_base_dir,
                                                'repetition_{}'.format(repetition),
                                                'loo_{}'.format(excluded_key))
                else:
                    fold_log_dir = os.path.join(tensorboard_base_dir,
                                                'loo_{}'.format(excluded_key))
                os.makedirs(fold_log_dir)
                tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                          log_dir=fold_log_dir)
                fold_callbacks = callbacks + [tensorboard]
            else:
                fold_callbacks = callbacks

            # Training
            network.fit(train_data=train_data,
                        epochs=training_config['epochs'], verbose=training_config['verbose'],
                        callbacks=fold_callbacks,
                        validation_data=val_data,
                        step_checkpoint=training_config['step_checkpoint'],
                        metrics=training_config['metrics'],
                        additional_metrics_info=training_config['additional_metrics_info'],
                        metrics_nicknames=training_config['metrics_nicknames'],
                        train_num_batches=train_steps,
                        eval_num_batches=eval_steps,
                        np_val_y=np_val_y)

            # Inference
            val_predictions = network.predict(x=iter(val_data()),
                                              steps=eval_steps,
                                              callbacks=fold_callbacks)

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            if compute_test_info:
                test_predictions = network.predict(x=iter(test_data()),
                                                   steps=test_steps,
                                                   callbacks=callbacks)

                prediction_info[str(excluded_key)] = test_predictions

                iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                          true_values=np_test_y,
                                                                          predicted_values=test_predictions,
                                                                          error_metrics_additional_info=error_metrics_additional_info,
                                                                          error_metrics_nicknames=error_metrics_nicknames)

                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

            # Save model
            if save_model:
                filename = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
                if repetitions > 1:
                    filename += '_repetition_{}'.format(repetition)
                filename += '_key_{}'.format(excluded_key)
                filepath = os.path.join(test_path, filename)
                network.save(filepath=filepath)

                # kb_path = os.path.join(test_path,
                #                        '{0}_key_{1}_kb.npy'.format(cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                #                                                    excluded_key))
                # np.save(kb_path, network.knowledge)

                # Save ground truth
                filepath = os.path.join(test_path, 'y_test_key_{}.json'.format(excluded_key))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=np_test_y)

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in prediction_info.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    if clear_afterwards:
        Logger.get_logger(__name__).info('Clearing tests data to save disk space...')
        clear_data_config(filepath=model_base_path, config=config_name, config_id=config_id)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result


def pairwise_loo_test(data_handle, test_path, data_loader_info, loo,
                      callbacks, model_type, network_args, training_config,
                      error_metrics, error_metrics_nicknames=None, error_metrics_additional_info=None,
                      compute_test_info=True, save_model=False, split_key=None, validation_percentage=None,
                      use_tensorboard=False, checkpoint=None, distributed_info=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Step 2: LOO test

    validation_info = OrderedDict()
    test_info = OrderedDict()
    prediction_info = OrderedDict()

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    split_values = data_handle.get_loo_data(split_key)
    for train_keys_indexes, excluded_key_indexes in loo.split(split_values):

        # train_keys = split_values[train_keys_indexes]
        excluded_key = split_values[excluded_key_indexes]

        if len(excluded_key) == 1:
            excluded_key = excluded_key[0]

        Logger.get_logger(__name__).info('Excluding: {}'.format(excluded_key))

        # Data conversion

        train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                          key_values=excluded_key,
                                                          validation_percentage=validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data_loo_{}'.format(excluded_key))
        val_filepath = os.path.join(model_path, 'val_data_loo_{}'.format(excluded_key))
        test_filepath = os.path.join(model_path, 'test_data_loo_{}'.format(excluded_key))

        save_prefix = 'loo_{}'.format(excluded_key)

        if not os.path.isfile(test_filepath):
            Logger.get_logger(__name__).info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            converter.convert_data(examples=val_data,
                                   label_list=processor.get_labels(),
                                   output_file=val_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint)

            converter.convert_data(examples=test_data,
                                   label_list=processor.get_labels(),
                                   output_file=test_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint)
        else:
            tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                              prefix=save_prefix)
            converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        train_data = get_pairwise_dataset_fn(filepath=train_filepath,
                                             batch_size=training_config['batch_size'],
                                             name_to_features=converter.feature_class.get_mappings(converter_info),
                                             selector=converter.feature_class.get_dataset_selector(),
                                             is_training=True,
                                             shuffle_amount=distributed_info['shuffle_amount'],
                                             reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                             prefetch_amount=distributed_info['prefetch_amount'],
                                             pairwise_labels=data_handle.get_additional_info()['pairwise_labels'],
                                             label_map=converter_info['label_map']
                                             )

        fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

        val_data = get_dataset_fn(filepath=val_filepath,
                                  batch_size=training_config['batch_size'],
                                  name_to_features=converter.feature_class.get_mappings(converter_info),
                                  selector=converter.feature_class.get_dataset_selector(),
                                  is_training=False,
                                  prefetch_amount=distributed_info['prefetch_amount'])

        test_data = get_dataset_fn(filepath=test_filepath,
                                   batch_size=training_config['batch_size'],
                                   name_to_features=converter.feature_class.get_mappings(converter_info),
                                   selector=converter.feature_class.get_dataset_selector(),
                                   is_training=False,
                                   prefetch_amount=distributed_info['prefetch_amount'])

        # Building network

        network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_loo_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], excluded_key)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
        eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

        np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
        np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

        Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
        Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
        Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                       mode='multi-label' if network.is_multilabel else 'multi-class')

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        if use_tensorboard:
            fold_log_dir = os.path.join(tensorboard_base_dir,
                                        'loo_{}'.format(excluded_key))
            os.makedirs(fold_log_dir)
            tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                      log_dir=fold_log_dir)
            fold_callbacks = callbacks + [tensorboard]
        else:
            fold_callbacks = callbacks

        # Training
        network.pairwise_fit(train_generator=train_data,
                             train_data=fixed_train_data,
                             verbose=training_config['verbose'],
                             callbacks=fold_callbacks, validation_data=val_data,
                             step_checkpoint=training_config['step_checkpoint'],
                             metrics=training_config['metrics'],
                             additional_metrics_info=training_config['additional_metrics_info'],
                             metrics_nicknames=training_config['metrics_nicknames'],
                             train_num_batches=train_steps,
                             eval_num_batches=eval_steps,
                             np_val_y=np_val_y,
                             batch_size=training_config['batch_size'],
                             shuffle_amount=distributed_info['shuffle_amount'],
                             np_train_y=np_train_y
                             )

        # Inference
        val_predictions = network.pairwise_predict(x=val_data,
                                                   knowledge=network.model.knowledge,
                                                   steps=eval_steps,
                                                   callbacks=fold_callbacks)

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=np_val_y,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info,
                                                                        error_metrics_nicknames=error_metrics_nicknames)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

        if compute_test_info:
            test_predictions = network.predict(x=test_data,
                                               knowledge=network.model.knowledge,
                                               steps=test_steps,
                                               callbacks=callbacks)

            prediction_info[str(excluded_key)] = test_predictions

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

        # Save model
        if save_model:
            filepath = os.path.join(test_path,
                                    '{0}_key_{1}'.format(
                                        cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                        excluded_key))
            network.save(filepath=filepath)

            # kb_path = os.path.join(test_path,
            #                        '{0}_key_{1}_kb.npy'.format(cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
            #                                                    excluded_key))
            # np.save(kb_path, network.knowledge)

            # Save ground truth
            filepath = os.path.join(test_path, 'y_test_key_{}.json'.format(excluded_key))
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=np_test_y)

        # Flush
        K.clear_session()

    avg_validation_info = OrderedDict()
    for key, value in validation_info.items():
        avg_validation_info['avg_{}'.format(key)] = np.mean(value, axis=0)
        avg_validation_info[key] = value

    result = {
        'validation_info': avg_validation_info
    }

    if compute_test_info:

        avg_test_info = OrderedDict()

        for key, value in test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(value, axis=0)
            avg_test_info[key] = value

        result['test_info'] = avg_test_info
        result['predictions'] = prediction_info
        return result
    else:
        return result


def loo_test_forward(data_handle, test_path, data_loader_info, loo,
                     callbacks, model_type, network_args, training_config,
                     error_metrics, error_metrics_nicknames=None, error_metrics_additional_info=None,
                     repetitions=1,
                     compute_test_info=True, split_key=None, validation_percentage=None, distributed_info=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 2: LOO test

    split_values = data_handle.get_loo_data(split_key)

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        prediction_info = OrderedDict()

        for train_keys_indexes, excluded_key_indexes in loo.split(split_values):

            excluded_key = split_values[excluded_key_indexes]

            if len(excluded_key) == 1:
                excluded_key = excluded_key[0]

            Logger.get_logger(__name__).info('Excluding: {}'.format(excluded_key))

            # Data conversion

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=excluded_key,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_loo_{}'.format(excluded_key))
            val_filepath = os.path.join(model_path, 'val_data_loo_{}'.format(excluded_key))
            test_filepath = os.path.join(model_path, 'test_data_loo_{}'.format(excluded_key))

            save_prefix = 'loo_{}'.format(excluded_key)

            if not os.path.isfile(test_filepath):
                Logger.get_logger(__name__).info(
                    'Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer)
            else:
                tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                  prefix=save_prefix)
                converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Building network

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
            if repetitions > 1:
                network_retrieved_args['name'] += '_repetition_{}'.format(repetition)
            network_retrieved_args['name'] += '_loo_{}'.format(excluded_key)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            text_info = merge(text_info, training_config)
            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            # Setup model by feeding an input
            network.predict(x=iter(val_data()), steps=1)

            # load pre-trained weights
            if repetitions > 1:
                current_weight_filename = '{0}_repetition_{1}_key_{2}.h5'.format(
                    cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                    repetition,
                    excluded_key)
            else:
                current_weight_filename = '{0}_key_{1}.h5'.format(
                    cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                    excluded_key)
            network.load(os.path.join(test_path, current_weight_filename))

            # Inference
            # train_predictions = network.predict(x=iter(fixed_train_data()),
            #                                     steps=train_steps,
            #                                     callbacks=callbacks,
            #                                     suffix='train')

            val_predictions = network.predict(x=iter(val_data()),
                                              steps=eval_steps,
                                              callbacks=callbacks,
                                              suffix='val')

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            if compute_test_info:
                test_predictions = network.predict(x=iter(test_data()),
                                                   steps=test_steps,
                                                   callbacks=callbacks,
                                                   suffix='test')

                prediction_info[str(excluded_key)] = test_predictions

                iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                          true_values=np_test_y,
                                                                          predicted_values=test_predictions,
                                                                          error_metrics_additional_info=error_metrics_additional_info,
                                                                          error_metrics_nicknames=error_metrics_nicknames)

                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in prediction_info.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result


def distributed_train_and_test(data_handle, test_path, data_loader_info, strategy,
                               callbacks, model_type, network_args, training_config,
                               error_metrics, error_metrics_additional_info=None,
                               error_metrics_nicknames=None, compute_test_info=True,
                               save_model=False, validation_percentage=None,
                               use_tensorboard=False, checkpoint=None, distributed_info=None,
                               repetitions=1, clear_afterwards=False):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 1: Train and test

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        train_df, val_df, test_df = data_handle.get_data(validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data')
        val_filepath = os.path.join(model_path, 'val_data')
        test_filepath = os.path.join(model_path, 'test_data')

        save_prefix = None

        if (not os.path.isfile(test_filepath) and test_df is not None) \
                or (test_df is None and not os.path.isfile(val_filepath)):
            Logger.get_logger(__name__).info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            if val_df is not None:
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            if test_df is not None:
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            if val_df is not None:
                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            if test_df is not None:
                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
        else:
            tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                              prefix=save_prefix)
            converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        with strategy.scope():
            train_data = get_dataset_fn(filepath=train_filepath,
                                        batch_size=training_config['batch_size'],
                                        name_to_features=converter.feature_class.get_mappings(converter_info),
                                        selector=converter.feature_class.get_dataset_selector(),
                                        is_training=True,
                                        shuffle_amount=distributed_info['shuffle_amount'],
                                        reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                        prefetch_amount=distributed_info['prefetch_amount'])

            fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                              batch_size=training_config['batch_size'],
                                              name_to_features=converter.feature_class.get_mappings(converter_info),
                                              selector=converter.feature_class.get_dataset_selector(),
                                              is_training=False,
                                              prefetch_amount=distributed_info['prefetch_amount'])

            if os.path.isfile(val_filepath):
                val_data = get_dataset_fn(filepath=val_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])
            else:
                val_data = None

            if os.path.isfile(test_filepath):
                test_data = get_dataset_fn(filepath=test_filepath,
                                           batch_size=training_config['batch_size'],
                                           name_to_features=converter.feature_class.get_mappings(converter_info),
                                           selector=converter.feature_class.get_dataset_selector(),
                                           is_training=False,
                                           prefetch_amount=distributed_info['prefetch_amount'])
            else:
                test_data = None

        # Building network

        network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))

        if val_df is not None:
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        else:
            eval_steps = None

        if test_df is not None:
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))
        else:
            test_steps = None

        np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])

        if val_df is not None:
            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        else:
            np_val_y = None

        if test_df is not None:
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])
        else:
            np_test_y = None

        Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
        if val_data is not None:
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
        if test_data is not None:
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                       mode='multi-label' if network.is_multilabel else 'multi-class')

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        text_info = merge(text_info, training_config)

        with strategy.scope():
            network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        if use_tensorboard:
            specific_log_dir = os.path.join(tensorboard_base_dir, 'repetition_{}'.format(repetition))
            os.makedirs(specific_log_dir)
            tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                      log_dir=specific_log_dir)
            callbacks = callbacks + [tensorboard]

        # Training
        with strategy.scope():
            network.distributed_fit(train_data=train_data,
                                    epochs=training_config['epochs'], verbose=training_config['verbose'],
                                    callbacks=callbacks, validation_data=val_data,
                                    strategy=strategy, step_checkpoint=training_config['step_checkpoint'],
                                    metrics=training_config['metrics'],
                                    additional_metrics_info=training_config['additional_metrics_info'],
                                    metrics_nicknames=training_config['metrics_nicknames'],
                                    train_num_batches=train_steps,
                                    eval_num_batches=eval_steps,
                                    np_val_y=np_val_y)

            # Inference
            if val_data is not None:
                # val_predictions = network.distributed_predict(x=network._get_input_iterator(val_data, strategy),
                #                                               strategy=strategy,
                #                                               steps=eval_steps,
                #                                               callbacks=callbacks)
                val_predictions = network.predict(x=iter(val_data()),
                                                  steps=eval_steps,
                                                  callbacks=callbacks)

                iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                                true_values=np_val_y,
                                                                                predicted_values=val_predictions,
                                                                                error_metrics_additional_info=error_metrics_additional_info,
                                                                                error_metrics_nicknames=error_metrics_nicknames)

                validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                            iteration_validation_info=iteration_validation_error)

                Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            if compute_test_info and test_data is not None:
                # test_predictions = network.distributed_predict(x=network._get_input_iterator(test_data, strategy),
                #                                                strategy=strategy,
                #                                                steps=test_steps,
                #                                                callbacks=callbacks)
                test_predictions = network.predict(x=iter(test_data()),
                                                   steps=test_steps,
                                                   callbacks=callbacks)

                all_preds[repetition] = test_predictions.ravel()

                iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                          true_values=np_test_y,
                                                                          predicted_values=test_predictions,
                                                                          error_metrics_additional_info=error_metrics_additional_info,
                                                                          error_metrics_nicknames=error_metrics_nicknames)

                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)

                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

        # Save model
        if save_model:
            filepath = os.path.join(test_path, '{0}_repetition_{1}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                repetition))
            network.save(filepath=filepath)

            # Save ground truth
            filepath = os.path.join(test_path, 'y_test.json')
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=np_test_y)

        # Flush
        K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result


def distributed_train_and_test_forward(data_handle, test_path, data_loader_info, strategy,
                                       callbacks, model_type, network_args, training_config,
                                       error_metrics, error_metrics_additional_info=None,
                                       error_metrics_nicknames=None, compute_test_info=True,
                                       validation_percentage=None, repetitions=1,
                                       checkpoint=None, distributed_info=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 1: Train and test

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        train_df, val_df, test_df = data_handle.get_data(validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data')
        val_filepath = os.path.join(model_path, 'val_data')
        test_filepath = os.path.join(model_path, 'test_data')

        save_prefix = None

        if (not os.path.isfile(test_filepath) and test_df is not None) \
                or (test_df is None and not os.path.isfile(val_filepath)):
            Logger.get_logger(__name__).info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            if val_df is not None:
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            if test_df is not None:
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            if val_df is not None:
                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            if test_df is not None:
                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
        else:
            tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                              prefix=save_prefix)
            converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        with strategy.scope():
            if os.path.isfile(val_filepath):
                val_data = get_dataset_fn(filepath=val_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])
            else:
                val_data = None

            if os.path.isfile(test_filepath):
                test_data = get_dataset_fn(filepath=test_filepath,
                                           batch_size=training_config['batch_size'],
                                           name_to_features=converter.feature_class.get_mappings(converter_info),
                                           selector=converter.feature_class.get_dataset_selector(),
                                           is_training=False,
                                           prefetch_amount=distributed_info['prefetch_amount'])
            else:
                test_data = None

        # Building network

        network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        if val_df is not None:
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        else:
            eval_steps = None

        if test_df is not None:
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))
        else:
            test_steps = None

        if val_df is not None:
            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        else:
            np_val_y = None

        if test_df is not None:
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])
        else:
            np_test_y = None

        if val_data is not None:
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
        if test_data is not None:
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        text_info = merge(text_info, training_config)

        with strategy.scope():
            network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        # Training
        with strategy.scope():
            # Inference
            if val_data is not None:
                val_predictions = network.distributed_predict(x=network._get_input_iterator(val_data, strategy),
                                                              strategy=strategy,
                                                              steps=eval_steps,
                                                              callbacks=callbacks,
                                                              suffix='val')

                iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                                true_values=np_val_y,
                                                                                predicted_values=val_predictions,
                                                                                error_metrics_additional_info=error_metrics_additional_info,
                                                                                error_metrics_nicknames=error_metrics_nicknames)

                validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                            iteration_validation_info=iteration_validation_error)

                Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

            if compute_test_info and test_data is not None:
                test_predictions = network.distributed_predict(x=network._get_input_iterator(test_data, strategy),
                                                               strategy=strategy,
                                                               steps=test_steps,
                                                               callbacks=callbacks,
                                                               suffix='test')

                all_preds[repetition] = test_predictions

                iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                          true_values=np_test_y,
                                                                          predicted_values=test_predictions,
                                                                          error_metrics_additional_info=error_metrics_additional_info,
                                                                          error_metrics_nicknames=error_metrics_nicknames)

                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)

                Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

        # Flush
        K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result


def train_and_test(data_handle, test_path, data_loader_info,
                   callbacks, model_type, network_args, training_config,
                   error_metrics, error_metrics_additional_info=None,
                   error_metrics_nicknames=None, compute_test_info=True,
                   save_model=False, validation_percentage=None, repetitions=1,
                   use_tensorboard=False, checkpoint=None, distributed_info=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 1: Train and test

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        train_df, val_df, test_df = data_handle.get_data(validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data')
        val_filepath = os.path.join(model_path, 'val_data')
        test_filepath = os.path.join(model_path, 'test_data')

        save_prefix = None

        if (not os.path.isfile(test_filepath) and test_df is not None) \
                or (test_df is None and not os.path.isfile(val_filepath)):
            Logger.get_logger(__name__).info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            if val_df is not None:
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            if test_df is not None:
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            if val_df is not None:
                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            if test_df is not None:
                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
        else:
            tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                              prefix=save_prefix)
            converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        train_data = get_dataset_fn(filepath=train_filepath,
                                    batch_size=training_config['batch_size'],
                                    name_to_features=converter.feature_class.get_mappings(converter_info),
                                    selector=converter.feature_class.get_dataset_selector(),
                                    is_training=True,
                                    shuffle_amount=distributed_info['shuffle_amount'],
                                    reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                    prefetch_amount=distributed_info['prefetch_amount'])

        fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

        if os.path.isfile(val_filepath):
            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])
        else:
            val_data = None

        if os.path.isfile(test_filepath):
            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])
        else:
            test_data = None

        # Building network

        network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))

        if val_df is not None:
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        else:
            eval_steps = None

        if test_df is not None:
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))
        else:
            test_steps = None

        np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])

        if val_df is not None:
            np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        else:
            np_val_y = None

        if test_df is not None:
            np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])
        else:
            np_test_y = None

        Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
        if val_data is not None:
            Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
        if test_data is not None:
            Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                       mode='multi-label' if network.is_multilabel else 'multi-class')

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        text_info = merge(text_info, training_config)
        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        if use_tensorboard:
            specific_log_dir = os.path.join(tensorboard_base_dir, 'repetition_{}'.format(repetition))
            os.makedirs(specific_log_dir)
            tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                      log_dir=specific_log_dir)
            callbacks = callbacks + [tensorboard]

        # Training
        network.fit(train_data=train_data,
                    epochs=training_config['epochs'], verbose=training_config['verbose'],
                    callbacks=callbacks, validation_data=val_data,
                    step_checkpoint=training_config['step_checkpoint'],
                    metrics=training_config['metrics'],
                    additional_metrics_info=training_config['additional_metrics_info'],
                    metrics_nicknames=training_config['metrics_nicknames'],
                    train_num_batches=train_steps,
                    eval_num_batches=eval_steps,
                    np_val_y=np_val_y)

        # Inference
        if val_data is not None:
            val_predictions = network.predict(x=iter(val_data()),
                                              steps=eval_steps,
                                              callbacks=callbacks)

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

        if compute_test_info and test_data is not None:
            test_predictions = network.predict(x=iter(test_data()),
                                               steps=test_steps,
                                               callbacks=callbacks)

            all_preds[repetition] = test_predictions.ravel()

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)

            Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

        # Save model
        if save_model:
            filepath = os.path.join(test_path, '{0}_repetition_{1}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                repetition))
            network.save(filepath=filepath)

            # Save ground truth
            filepath = os.path.join(test_path, 'y_test.json')
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=np_test_y)

        # Flush
        K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result


def train_and_test_forward(data_handle, test_path, data_loader_info,
                           callbacks, model_type, network_args, training_config,
                           error_metrics, error_metrics_additional_info=None,
                           error_metrics_nicknames=None, compute_test_info=True,
                           validation_percentage=None, repetitions=1,
                           checkpoint=None, distributed_info=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 1: Train and test

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_train_preds = OrderedDict()
    total_val_preds = OrderedDict()
    total_test_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_train_preds = OrderedDict()
        all_val_preds = OrderedDict()
        all_test_preds = OrderedDict()

        train_df, val_df, test_df = data_handle.get_data(validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data')
        val_filepath = os.path.join(model_path, 'val_data')
        test_filepath = os.path.join(model_path, 'test_data')

        save_prefix = None

        if (not os.path.isfile(test_filepath) and test_df is not None) \
                or (test_df is None and not os.path.isfile(val_filepath)):
            Logger.get_logger(__name__).info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            if val_df is not None:
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            if test_df is not None:
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            if val_df is not None:
                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            if test_df is not None:
                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
        else:
            tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                              prefix=save_prefix)
            converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

        val_data = get_dataset_fn(filepath=val_filepath,
                                  batch_size=training_config['batch_size'],
                                  name_to_features=converter.feature_class.get_mappings(converter_info),
                                  selector=converter.feature_class.get_dataset_selector(),
                                  is_training=False,
                                  prefetch_amount=distributed_info['prefetch_amount'])

        test_data = get_dataset_fn(filepath=test_filepath,
                                   batch_size=training_config['batch_size'],
                                   name_to_features=converter.feature_class.get_mappings(converter_info),
                                   selector=converter.feature_class.get_dataset_selector(),
                                   is_training=False,
                                   prefetch_amount=distributed_info['prefetch_amount'])

        # Building network

        network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
        eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

        np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

        Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
        Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
        Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        text_info = merge(text_info, training_config)
        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        # Setup model by feeding an input
        network.predict(x=iter(val_data()), steps=1)

        # load pre-trained weights
        current_weight_filename = os.path.join(test_path,
                                               '{0}_repetition_{1}.h5'.format(
                                                   cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                                   repetition))
        network.load(os.path.join(test_path, current_weight_filename))

        # Inference
        train_predictions = network.predict(x=iter(fixed_train_data()),
                                            steps=train_steps,
                                            callbacks=callbacks,
                                            suffix='train')

        all_train_preds[repetition] = train_predictions

        val_predictions = network.predict(x=iter(val_data()),
                                          steps=eval_steps,
                                          callbacks=callbacks,
                                          suffix='val')

        all_val_preds[repetition] = val_predictions

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=np_val_y,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info,
                                                                        error_metrics_nicknames=error_metrics_nicknames)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

        if compute_test_info:
            test_predictions = network.predict(x=iter(test_data()),
                                               steps=test_steps,
                                               callbacks=callbacks,
                                               suffix='test')

            all_test_preds[repetition] = test_predictions

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)

            Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

        # Flush
        K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_train_preds.items():
            total_train_preds.setdefault(key, []).append(item)
        for key, item in all_val_preds.items():
            total_val_preds.setdefault(key, []).append(item)
        for key, item in all_test_preds.items():
            total_test_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_train_preds = {key: np.mean(item, 0) for key, item in total_train_preds.items()}
        total_val_preds = {key: np.mean(item, 0) for key, item in total_val_preds.items()}
        total_test_preds = {key: np.mean(item, 0) for key, item in total_test_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

        total_train_preds = {key: np.mean(item, 0) for key, item in total_train_preds.items()}
        total_val_preds = {key: np.mean(item, 0) for key, item in total_val_preds.items()}
        total_test_preds = {key: np.mean(item, 0) for key, item in total_test_preds.items()}

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['train_predictions'] = total_train_preds
        result['val_predictions'] = total_val_preds
        result['test_predictions'] = total_test_preds
    else:
        result['train_predictions'] = total_train_preds
        result['val_predictions'] = total_val_preds
        result['test_predictions'] = total_test_preds

    return result


def pairwise_train_and_test(data_handle, test_path, data_loader_info,
                            callbacks, model_type, network_args, training_config,
                            error_metrics, error_metrics_additional_info=None,
                            error_metrics_nicknames=None, compute_test_info=True,
                            save_model=False, validation_percentage=None, repetitions=1,
                            use_tensorboard=False, checkpoint=None, distributed_info=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_factory = ProcessorFactory()
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 1: Train and test

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        Logger.get_logger(__name__).info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        train_df, val_df, test_df = data_handle.get_data(validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data')
        val_filepath = os.path.join(model_path, 'val_data')
        test_filepath = os.path.join(model_path, 'test_data')

        save_prefix = None

        if not os.path.isfile(test_filepath):
            Logger.get_logger(__name__).info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            converter.convert_data(examples=val_data,
                                   label_list=processor.get_labels(),
                                   output_file=val_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint)

            converter.convert_data(examples=test_data,
                                   label_list=processor.get_labels(),
                                   output_file=test_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint)
        else:
            tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                              prefix=save_prefix)
            converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        train_data = get_pairwise_dataset_fn(filepath=train_filepath,
                                             batch_size=training_config['batch_size'],
                                             name_to_features=converter.feature_class.get_mappings(converter_info),
                                             selector=converter.feature_class.get_dataset_selector(),
                                             is_training=True,
                                             shuffle_amount=distributed_info['shuffle_amount'],
                                             reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                             prefetch_amount=distributed_info['prefetch_amount'],
                                             pairwise_labels=data_handle.get_additional_info()['pairwise_labels'],
                                             label_map=converter_info['label_map'])

        fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

        val_data = get_dataset_fn(filepath=val_filepath,
                                  batch_size=training_config['batch_size'],
                                  name_to_features=converter.feature_class.get_mappings(converter_info),
                                  selector=converter.feature_class.get_dataset_selector(),
                                  is_training=False,
                                  prefetch_amount=distributed_info['prefetch_amount'])

        test_data = get_dataset_fn(filepath=test_filepath,
                                   batch_size=training_config['batch_size'],
                                   name_to_features=converter.feature_class.get_mappings(converter_info),
                                   selector=converter.feature_class.get_dataset_selector(),
                                   is_training=False,
                                   prefetch_amount=distributed_info['prefetch_amount'])

        # Building network

        network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
        eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

        np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
        np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

        Logger.get_logger(__name__).info('Total train steps: {}'.format(train_steps))
        Logger.get_logger(__name__).info('Total eval steps: {}'.format(eval_steps))
        Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                       mode='multi-label' if network.is_multilabel else 'multi-class')

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        if use_tensorboard:
            tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                      log_dir=tensorboard_base_dir)
            callbacks = callbacks + [tensorboard]

        # Training
        network.pairwise_fit(train_generator=train_data, train_data=fixed_train_data,
                             verbose=training_config['verbose'],
                             callbacks=callbacks, validation_data=val_data,
                             step_checkpoint=training_config['step_checkpoint'],
                             metrics=training_config['metrics'],
                             additional_metrics_info=training_config['additional_metrics_info'],
                             metrics_nicknames=training_config['metrics_nicknames'],
                             train_num_batches=train_steps,
                             eval_num_batches=eval_steps,
                             np_train_y=np_train_y,
                             np_val_y=np_val_y,
                             batch_size=training_config['batch_size'],
                             shuffle_amount=distributed_info['shuffle_amount'])

        # Inference
        val_predictions = network.pairwise_predict(x=val_data,
                                                   knowledge=network.model.knowledge,
                                                   steps=eval_steps,
                                                   callbacks=callbacks)

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=np_val_y,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info,
                                                                        error_metrics_nicknames=error_metrics_nicknames)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        Logger.get_logger(__name__).info('Iteration validation info: {}'.format(iteration_validation_error))

        if compute_test_info:
            test_predictions = network.pairwise_predict(x=test_data,
                                                        knowledge=network.model.knowledge,
                                                        steps=test_steps,
                                                        callbacks=callbacks)

            all_preds[repetition] = test_predictions.ravel()

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)

            Logger.get_logger(__name__).info('Iteration test info: {}'.format(iteration_test_error))

        # Save model
        if save_model:
            filepath = os.path.join(test_path, '{0}_repetition_{1}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                repetition))
            network.save(filepath=filepath)

            # Save ground truth
            filepath = os.path.join(test_path, 'y_test.json')
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=np_test_y)

        # Flush
        K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result


def unseen_data_test(data_handle, test_path, data_loader_info, trained_data_name,
                     callbacks, model_type, network_args, training_config, test_prefix=None,
                     distributed_info=None, save_prefix=None, repetition_prefix=None):
    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   trained_data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    unseen_model_path = os.path.join(cd.TESTS_DATA_DIR,
                                     data_handle.data_name,
                                     model_type,
                                     str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    if not os.path.isdir(unseen_model_path):
        os.makedirs(unseen_model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_factory = ProcessorFactory()
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor_args['retrieve_label'] = False
    processor = processor_factory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_factory = TokenizerFactory()
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_factory = DataConverterFactory()
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = converter_factory.factory(converter_type, **converter_args)

    # Step 1: Unseen data test

    test_df = data_handle.get_data()

    test_name = 'test_data' if save_prefix is None else 'test_data_{}'.format(save_prefix)
    test_filepath = os.path.join(unseen_model_path, test_name)

    tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                      prefix='{0}_{1}'.format(
                                                                                          test_prefix,
                                                                                          save_prefix)
                                                                                      if test_prefix is not None
                                                                                      else save_prefix)
    converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
        filepath=model_path,
        prefix='{0}_{1}'.format(test_prefix, save_prefix) if test_prefix is not None else save_prefix)

    if not os.path.isfile(test_filepath):
        Logger.get_logger(__name__).info(
            'Dataset not found! Building new one from scratch....it may require some minutes')

        # Processor
        test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

        # Tokenizer
        tokenizer.initalize_with_vocab(tokenizer_info['vocab'])

        # Conversion
        for key, value in converter_info.items():
            setattr(converter, key, value)
        converter.convert_data(examples=test_data,
                               label_list=processor.get_labels(),
                               output_file=test_filepath,
                               has_labels=False,
                               tokenizer=tokenizer)

    # Create Datasets
    test_data = get_dataset_fn(filepath=test_filepath,
                               batch_size=training_config['batch_size'],
                               name_to_features=converter.feature_class.get_mappings(converter_info, has_labels=False),
                               selector=converter.feature_class.get_dataset_selector(),
                               is_training=False,
                               prefetch_amount=distributed_info['prefetch_amount'])

    # Debug
    tokenizer.show_info(tokenizer_info)
    Logger.get_logger(__name__).info('Converter info: \n{}'.format(converter_info))

    # Building network

    network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                              if 'model_class' in value['flags']}
    network_retrieved_args['additional_data'] = data_handle.get_additional_info()
    network_retrieved_args['name'] = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
    if repetition_prefix is not None:
        network_retrieved_args['name'] += '_repetition_{}'.format(repetition_prefix)
    # if save_prefix is not None:
    #     network_retrieved_args['name'] += '_key_{}'.format(save_prefix)
    network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

    # Useful stuff
    test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

    Logger.get_logger(__name__).info('Total test steps: {}'.format(test_steps))

    # Custom callbacks only
    for callback in callbacks:
        if hasattr(callback, 'on_build_model_begin'):
            callback.on_build_model_begin(logs={'network': network})

    text_info = merge(tokenizer_info, converter_info)
    network.build_model(text_info=text_info)

    # Custom callbacks only
    for callback in callbacks:
        if hasattr(callback, 'on_build_model_end'):
            callback.on_build_model_end(logs={'network': network})

    # Setup model by feeding an input
    network.predict(x=iter(test_data()), steps=1)

    # load pre-trained weights
    pretrained_weight_filename = network_retrieved_args['name']
    if save_prefix:
        pretrained_weight_filename += '_key_{}'.format(save_prefix)
    current_weight_filename = os.path.join(test_path,
                                           '{}.h5'.format(pretrained_weight_filename))
    network.load(os.path.join(test_path, current_weight_filename))

    # Inference
    test_predictions = network.predict(x=iter(test_data()),
                                       steps=test_steps,
                                       callbacks=callbacks)

    # Flush
    K.clear_session()

    return test_predictions
