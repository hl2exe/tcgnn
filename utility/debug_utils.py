import tracemalloc
import os
import linecache
from utility.log_utils import Logger
from pympler import muppy, summary
import psutil


def display_top(snapshot, key_type='lineno', limit=3):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    Logger.get_logger(__name__).info("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        Logger.get_logger(__name__).info("#%s: %s:%s: %.1f KiB"
                    % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            Logger.get_logger(__name__).info('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        Logger.get_logger(__name__).info("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    Logger.get_logger(__name__).info("Total allocated size: %.1f KiB" % (total / 1024))


def start_debug():
    tracemalloc.start()


def display_debug():
    display_top(tracemalloc.take_snapshot())


def print_pympler_summary():
    all_objects = muppy.get_objects()
    objects_summary = summary.summarize(all_objects)
    summary.print_(objects_summary)


def view_used_mem():
    used_mem = psutil.virtual_memory().used
    Logger.get_logger(__name__).info("Used memory: {} Mb".format(used_mem / 1024 / 1024))
