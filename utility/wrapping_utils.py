import tensorflow as tf


def create_int_feature(values):
    f = tf.train.Feature(int64_list=tf.train.Int64List(value=list(values)))
    return f


def create_text_feature(text):
    f = tf.train.Feature(bytes_list=tf.train.BytesList(value=[text.encode('utf-8')]))
    return f