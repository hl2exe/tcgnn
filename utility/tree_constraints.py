"""

Collection of tree pooling constraints (numpy version)

"""

import numpy as np


# Utility


def softmax(x, axis=-1):
    return np.exp(x) / np.sum(np.exp(x), axis=axis, keepdims=True)


# Kernel specific constraints

def stk_contiguous_sequence_constraint(pooling_matrix, adjacency_matrix, node_mask=None):
    adjacency_matrix = adjacency_matrix.astype(pooling_matrix.dtype)

    if node_mask is None:
        node_mask = np.ones_like(adjacency_matrix)
    else:
        node_mask = node_mask[:, :, np.newaxis]

    # avoid counting padding nodes
    adjacency_matrix *= node_mask
    adjacency_matrix *= np.transpose(node_mask, [0, 2, 1])

    node_amount = adjacency_matrix.shape[-1]

    # Adjacency and degree matrices

    # [batch_size, N, N]
    forward_adjacency_matrix = np.triu(np.ones((node_amount, node_amount), dtype=pooling_matrix.dtype))
    forward_adjacency_matrix -= np.eye(node_amount, dtype=pooling_matrix.dtype)
    forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

    # [batch_size, N, N]
    backward_adjacency_matrix = np.tril(np.ones((node_amount, node_amount), dtype=pooling_matrix.dtype))
    backward_adjacency_matrix -= np.eye(node_amount, dtype=pooling_matrix.dtype)
    backward_adjacency_matrix = adjacency_matrix * backward_adjacency_matrix[None, :, :]

    # [batch_size, N, N]
    forward_degree_matrix = np.sum(forward_adjacency_matrix, axis=-1)
    forward_degree_matrix = forward_degree_matrix[:, :, None] * np.eye(adjacency_matrix.shape[-1],
                                                                       dtype=adjacency_matrix.dtype)[None, :, :]

    backward_degree_matrix = np.sum(backward_adjacency_matrix, axis=-1)
    backward_degree_matrix = backward_degree_matrix[:, :, None] * np.eye(adjacency_matrix.shape[-1],
                                                                         dtype=adjacency_matrix.dtype)[None, :, :]

    # Leaf mask
    # Mask out leaves: a leaf node no children -> the minimum returns 0.
    # is leaf? -> 0.0
    # [batch_size, max_nodes]
    leaf_mask = np.minimum(np.sum(forward_adjacency_matrix, axis=-1), 1.)

    # Apply masked adjacency matrix

    # [batch_size, max_nodes, max_nodes]
    numerator_adjacency_matrix = forward_adjacency_matrix + backward_adjacency_matrix * (
            1. - leaf_mask[:, :, None])

    # [batch_size, K, K]
    numerator = np.matmul(np.matmul(np.transpose(pooling_matrix, [0, 2, 1]), numerator_adjacency_matrix),
                          pooling_matrix)

    # Apply masked degree matrix

    # [batch_size, max_nodes, max_nodes]
    denominator_degree_matrix = forward_degree_matrix + backward_degree_matrix * (
            1. - leaf_mask[:, :, None])

    # [batch_size, K, K]
    denominator = np.matmul(np.matmul(np.transpose(pooling_matrix, [0, 2, 1]), denominator_degree_matrix),
                            pooling_matrix)

    # [batch_size]
    numerator = np.trace(numerator, axis1=1, axis2=2)
    denominator = np.trace(denominator, axis1=1, axis2=2) + 1e-12

    # [batch_size]
    stk_penalty = np.maximum(1. - numerator / denominator, 0.)
    # stk_penalty = tf.linalg.trace(stk_penalty)

    return stk_penalty


def sstk_contiguous_sequence_constraint(pooling_matrix, adjacency_matrix, node_mask=None):
    adjacency_matrix = adjacency_matrix.astype(pooling_matrix.dtype)

    if node_mask is None:
        node_mask = np.ones_like(adjacency_matrix)
    else:
        node_mask = node_mask[:, :, np.newaxis]

    # avoid counting padding nodes
    adjacency_matrix *= node_mask
    adjacency_matrix *= np.transpose(node_mask, [0, 2, 1])

    node_amount = adjacency_matrix.shape[-1]

    # Adjacency and degree matrices

    # [batch_size, N, N]
    forward_adjacency_matrix = np.triu(np.ones((node_amount, node_amount), dtype=pooling_matrix.dtype))
    forward_adjacency_matrix -= np.eye(node_amount, dtype=pooling_matrix.dtype)
    forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

    # Leaf mask
    # Mask out leaves: a leaf node no children -> the minimum returns 0.
    # is leaf? -> 0.0
    # [batch_size, max_nodes]
    leaf_mask = np.minimum(np.sum(forward_adjacency_matrix, axis=-1), 1.)

    # Ignore leaves from degree count
    forward_degree_matrix = np.sum(forward_adjacency_matrix * leaf_mask[:, None, :], axis=-1)

    # Ignore pooled leaves from constraint
    pooling_matrix *= leaf_mask[:, :, None]

    forward_degree_matrix = forward_degree_matrix[:, :, None] * np.eye(adjacency_matrix.shape[-1],
                                                                       dtype=adjacency_matrix.dtype)[None, :, :]

    # [batch_size, K, K]
    numerator = np.matmul(np.matmul(np.transpose(pooling_matrix, [0, 2, 1]), forward_adjacency_matrix),
                          pooling_matrix)

    # Apply masked degree matrix

    denominator = np.matmul(np.matmul(np.transpose(pooling_matrix, [0, 2, 1]), forward_degree_matrix),
                            pooling_matrix)

    # [batch_size]
    numerator = np.trace(numerator, axis1=1, axis2=2)
    denominator = np.trace(denominator, axis1=1, axis2=2) + 1e-12

    # [batch_size]
    sstk_penalty = np.maximum(1. - numerator / denominator, 0.)
    # stk_penalty = tf.linalg.trace(stk_penalty)

    return sstk_penalty


# General constraints

def contiguous_sequence_constraint(pooling_matrix, adjacency_matrix, node_mask=None):
    node_amount = adjacency_matrix.shape[-1]

    adjacency_matrix = adjacency_matrix.astype(pooling_matrix.dtype)

    if node_mask is None:
        node_mask = np.ones_like(adjacency_matrix)
    else:
        node_mask = node_mask[:, :, np.newaxis]

    # avoid counting padding nodes
    adjacency_matrix *= node_mask
    adjacency_matrix *= np.transpose(node_mask, [0, 2, 1])

    # [batch_size, max_nodes, max_nodes]
    forward_adjacency_matrix = np.triu(np.ones((node_amount, node_amount), dtype=pooling_matrix.dtype))
    forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

    # [batch_size, K, max_nodes]
    pooling_matrix = np.transpose(pooling_matrix, [0, 2, 1])

    # [batch_size, K, max_nodes, max_nodes]
    pair_intensities = pooling_matrix[:, :, :, np.newaxis] * pooling_matrix[:, :, np.newaxis, :]

    # [batch_size, K, max_nodes, max_nodes]
    self_pairs_intensities = pair_intensities * np.eye(node_amount, dtype=pair_intensities.dtype)[
                                                None, None, :, :]

    # [batch_size, K]
    pooled_mask = np.zeros_like(self_pairs_intensities)
    pooled_mask[self_pairs_intensities >= 1e-2] = 1.
    pooled_mask = np.sum(pooled_mask, axis=-1)
    pooled_mask = np.sum(pooled_mask, axis=-1)

    pair_intensities = pair_intensities - self_pairs_intensities
    pair_intensities *= forward_adjacency_matrix[:, np.newaxis, :, :]

    # [batch_size, K]
    numerator = np.sum(pair_intensities, axis=-1)
    numerator = np.sum(numerator, axis=-1)

    # [batch_size, K]
    mean_self_intensity = np.sum(self_pairs_intensities, axis=-1)
    mean_self_intensity = np.sum(mean_self_intensity, axis=-1)
    mean_self_intensity = mean_self_intensity / pooled_mask

    # [batch_size, K]
    denominator = np.sum(self_pairs_intensities, axis=-1)
    denominator = np.sum(denominator, axis=-1) - mean_self_intensity

    # [batch_size]
    numerator = np.sum(numerator, axis=-1)
    denominator = np.sum(denominator, axis=-1) + 1e-12

    # [batch_size]
    contiguous_penalty = np.maximum(1. - numerator / denominator, 0)

    return contiguous_penalty


def kernel_contiguous_constraint(kernel, pooling_matrix, adjacency_matrix, node_mask):
    if kernel.lower() == 'ptk':
        return np.zeros((pooling_matrix.shape[0],), dtype=pooling_matrix.dtype)
    elif kernel.lower() == 'stk':
        return stk_contiguous_sequence_constraint(pooling_matrix=pooling_matrix,
                                                  adjacency_matrix=adjacency_matrix,
                                                  node_mask=node_mask)
    else:
        return sstk_contiguous_sequence_constraint(pooling_matrix=pooling_matrix,
                                                   adjacency_matrix=adjacency_matrix,
                                                   node_mask=node_mask)


def minimal_tree_loss(pooling_matrix, overlapping_threshold=0.1):
    # pooling_matrix -> [batch_size, max_nodes, K]
    # node_mask -> [batch_size, max_nodes]

    meta_node_amount = pooling_matrix.shape[-1]

    # [batch_size, K, K]
    self_intensities = np.matmul(np.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)
    normalized_self_intensities = self_intensities / (
            np.linalg.norm(self_intensities, axis=(-1, -2))[:, None, None] + 1e-12)

    # [1, K, K]
    normalized_percentage = np.eye(meta_node_amount, dtype=pooling_matrix.dtype)
    normalized_percentage = normalized_percentage / np.linalg.norm(normalized_percentage, axis=(-1, -2))
    normalized_percentage = normalized_percentage[np.newaxis, :, :]

    # [batch_size, K, K]
    per_cluster_penalty = normalized_self_intensities - normalized_percentage
    per_cluster_penalty *= np.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :]

    per_cluster_overlap_penalty = np.maximum(normalized_self_intensities - overlapping_threshold, 0.)
    per_cluster_overlap_penalty *= (
            np.ones_like(per_cluster_overlap_penalty) - np.eye(meta_node_amount, dtype=pooling_matrix.dtype)[
                                                        None, :, :])

    per_cluster_total_penalty = per_cluster_penalty + per_cluster_overlap_penalty

    # [batch_size,]
    red_per_cluster_penalty = np.linalg.norm(per_cluster_total_penalty, axis=(-1, -2))

    return red_per_cluster_penalty


def minimal_tree_intensity_loss(pooling_matrix, node_mask, connectivity_threshold=0.3):
    # pooling_matrix -> [batch_size, max_nodes, K]
    # node_mask -> [batch_size, max_nodes]

    meta_node_amount = pooling_matrix.shape[-1]

    # [batch_size, K, K]
    self_intensities = np.matmul(np.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)
    diag_self_intensities = self_intensities * np.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :]

    # [batch_size]
    true_tree_size = np.sum(node_mask, axis=-1)
    bound_intensity = connectivity_threshold * (true_tree_size / meta_node_amount)

    # [batch_size, K, K]
    minimum_intensity = np.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :] * bound_intensity[:, None,
                                                                                           None]

    # [batch_size, K, K]
    intensity_penalty = np.maximum(minimum_intensity - diag_self_intensities, 0.)

    # [batch_size,]
    intensity_penalty = np.linalg.norm(intensity_penalty, axis=(-1, -2))

    return intensity_penalty
