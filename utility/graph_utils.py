"""

Graph and Tree utils for GNNs

"""

import nltk
from utility.embedding_utils import pad_data
import numpy as np
from tqdm import tqdm
import ast
from itertools import combinations, product


def depth_node_search(nodes, parent, depth, edges, node_indexes, edge_indices,
                      node_indices, node_segments, directionality_mask, leaf_indicator, is_directional=False):
    forward_value = 1
    backward_value = 1

    if is_directional:
        backward_value = -1

    parent_value = parent.label()
    for node in parent:
        # If not leaf
        if type(node) is nltk.Tree:
            node_value = node.label()
            nodes.append(node_value)
            leaf_indicator.append(0)

            edges.append(depth + 1)
            edge_idx = len(edges) - 1

            parent_idx = node_indexes[parent_value]
            current_idx = max(list(node_indexes.values())) + 1
            edge_indices.append([parent_idx, current_idx])
            node_indexes[node_value] = current_idx

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(edge_idx)
                node_segments[parent_idx].append(parent_idx)
                directionality_mask[parent_idx].append(backward_value)
            else:
                node_indices.append([edge_idx])
                node_segments.append([parent_idx])
                directionality_mask.append([backward_value])
            node_indices.append([edge_idx])
            node_segments.append([current_idx])
            directionality_mask.append([forward_value])

            depth_node_search(nodes=nodes, parent=node, depth=depth + 1,
                              edges=edges, node_indexes=node_indexes,
                              edge_indices=edge_indices, node_indices=node_indices,
                              node_segments=node_segments, directionality_mask=directionality_mask,
                              leaf_indicator=leaf_indicator,
                              is_directional=False)
        else:
            node_value = node
            nodes.append(node_value)
            leaf_indicator.append(1)

            edges.append(depth + 1)
            edge_idx = len(edges) - 1

            parent_idx = node_indexes[parent_value]
            current_idx = max(list(node_indexes.values())) + 1
            edge_indices.append([parent_idx, current_idx])
            node_indexes[node_value] = current_idx

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(edge_idx)
                node_segments[parent_idx].append(parent_idx)
                directionality_mask[parent_idx].append(backward_value)
            else:
                node_indices.append([edge_idx])
                node_segments.append([parent_idx])
                directionality_mask.append([backward_value])
            node_indices.append([edge_idx])
            node_segments.append([current_idx])
            directionality_mask.append([forward_value])

    return nodes, edge_indices, edges, node_indices, node_segments, directionality_mask, leaf_indicator


def get_nodes_depths(tree_text):
    tree = nltk.tree.Tree.fromstring(tree_text)
    node_depths = [0]
    current_depth = 1

    def _expand(parent, node_depths, current_depth):
        for node in parent:
            node_depths.append(current_depth)
            if type(node) is nltk.Tree:
                _expand(parent=node, node_depths=node_depths, current_depth=current_depth + 1)

    _expand(parent=tree, node_depths=node_depths, current_depth=current_depth)

    return node_depths


def get_dep_nodes_depths(tree_text):
    dep_tree = ast.literal_eval(tree_text)

    def _expand(node, parent_node_depth, parent_node_depths, dep_tree, to_explore, tree_depths):

        to_keep = []
        found_children = []

        for item_idx, item in enumerate(dep_tree):
            if node == item[2]:
                found_children.append((item[3], item[4], item[0]))
            else:
                to_keep.append(item_idx)

        # depth-first
        found_children = sorted(found_children, key=lambda item: item[0], reverse=True)
        for item in found_children:
            to_explore.insert(0, item[1])
            parent_node_depths.insert(0, parent_node_depth + 1)

        # filter explored triples
        dep_tree = [item for idx, item in enumerate(dep_tree) if idx in to_keep]

        # update explored nodes
        tree_depths.append(parent_node_depth)

        return to_explore, dep_tree, parent_node_depths, tree_depths

    to_explore = ['ROOT']
    tree_depths = []
    parent_node_depths = [0]

    while len(to_explore) > 0:
        first = to_explore.pop(0)
        first_node_depth = parent_node_depths.pop(0)
        to_explore, dep_tree, parent_node_depths, tree_depths = _expand(first,
                                                                        first_node_depth,
                                                                        parent_node_depths,
                                                                        dep_tree,
                                                                        to_explore,
                                                                        tree_depths)

    return tree_depths


def get_most_frequent_edges_to_truncate(edge_indices, truncation_amount):
    # Count for each node idx, the number of edges
    edges_amount = {}
    for pair in edge_indices:
        if pair[0] in edges_amount:
            edges_amount[pair[0]] += 1
        else:
            edges_amount[pair[0]] = 0

        if pair[1] in edges_amount:
            edges_amount[pair[1]] += 1
        else:
            edges_amount[pair[1]] = 0

    edges_amount_list = [(key, value) for key, value in edges_amount.items() if value > 1]
    edges_amount_list = sorted(edges_amount_list, key=lambda pair: pair[1], reverse=True)

    truncated_indexes = []
    removed_counts = {pair[0]: 0 for pair in edges_amount_list}
    edge_indices_str = ['-'.join(pair) for pair in edge_indices]

    for j in range(len(edges_amount_list) - 1):
        for k in range(j + 1, len(edges_amount_list)):
            pair = [edges_amount_list[j][0], edges_amount_list[k][0]]
            rev_pair = [edges_amount_list[k][0], edges_amount_list[j][0]]

            # Pair
            if pair in edge_indices \
                    and edges_amount[pair[0]] - removed_counts[pair[0]] > 1 \
                    and edges_amount[pair[1]] - removed_counts[pair[1]] > 1:
                pair_str = '-'.join(pair)
                pair_idx = edge_indices_str.index(pair_str)
                truncated_indexes.append(pair_idx)
                removed_counts[pair[0]] += 1
                removed_counts[pair[1]] += 1

            if len(truncated_indexes) == truncation_amount:
                return truncated_indexes

            # Reverse pair
            if rev_pair in edge_indices \
                    and edges_amount[rev_pair[0]] - removed_counts[rev_pair[0]] > 1 \
                    and edges_amount[rev_pair[1]] - removed_counts[rev_pair[1]] > 1:
                rev_pair_str = '-'.join(rev_pair)
                rev_pair_idx = edge_indices_str.index(rev_pair_str)
                truncated_indexes.append(rev_pair_idx)
                removed_counts[rev_pair[0]] += 1
                removed_counts[rev_pair[1]] += 1

            if len(truncated_indexes) == truncation_amount:
                return truncated_indexes

    if len(truncated_indexes) < truncation_amount:
        raise RuntimeError('Could not find enough edges to remove without'
                           ' isolating some nodes! This should never happen...'
                           '(Remaining: {}'.format(truncation_amount - len(truncated_indexes)))


def retrieve_dep_tree_info(tree_str, is_directional=True):
    """
    Orders data according to dependency relations
    tuple format: (dependency, source_idx, source_token, target_idx, target_token)
    """

    dep_tree = ast.literal_eval(tree_str)

    def _expand(node, node_idx, parent_idx, dep_tree, to_explore, tree_nodes, tree_features, edge_indices,
                node_tree_ids, parent_node_ids, node_indices, directionality_mask, leaf_indicator, forward_value,
                backward_value):

        to_keep = []
        found_children = []

        for item_idx, item in enumerate(dep_tree):
            if node == item[2]:
                found_children.append((item[3], item[4], item[0]))
            else:
                to_keep.append(item_idx)

        # depth-first
        found_children = sorted(found_children, key=lambda item: item[0], reverse=True)
        for item in found_children:
            to_explore.insert(0, item[1])
            tree_features.append(item[2])
            parent_node_ids.insert(0, node_idx)

        if found_children:
            leaf_indicator.append(0)
        else:
            leaf_indicator.append(1)

        # filter explored triples
        dep_tree = [item for idx, item in enumerate(dep_tree) if idx in to_keep]

        # update explored nodes
        tree_nodes.append(node)
        node_tree_ids.append(node_idx + 1)

        if parent_idx is not None:
            edge_indices.append([parent_idx, node_idx])

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(node_idx - 1)
                directionality_mask[parent_idx].append(backward_value)
            else:
                node_indices.append([node_idx - 1])
                directionality_mask.append([backward_value])
            node_indices.append([node_idx - 1])
            directionality_mask.append([forward_value])

        return to_explore, node_tree_ids, parent_node_ids, \
               tree_nodes, tree_features, edge_indices, node_indices, directionality_mask, leaf_indicator, dep_tree

    to_explore = ['ROOT']
    tree_nodes = []
    tree_features = []
    edge_indices = []
    node_indices = []
    directionality_mask = []
    node_tree_ids = [0]
    parent_node_ids = [None]
    leaf_indicator = [0]

    forward_value = 1
    backward_value = 1

    if is_directional:
        backward_value = -1

    while len(to_explore) > 0:
        first = to_explore.pop(0)
        first_idx = node_tree_ids.pop(0)
        first_parent_idx = parent_node_ids.pop(0)
        to_explore, node_tree_ids, parent_node_ids, tree_nodes, \
        tree_features, edge_indices, \
        node_indices, directionality_mask, leaf_indicator, dep_tree = _expand(node=first,
                                                                              node_idx=first_idx,
                                                                              parent_idx=first_parent_idx,
                                                                              dep_tree=dep_tree,
                                                                              to_explore=to_explore,
                                                                              tree_nodes=tree_nodes,
                                                                              tree_features=tree_features,
                                                                              edge_indices=edge_indices,
                                                                              node_tree_ids=node_tree_ids,
                                                                              parent_node_ids=parent_node_ids,
                                                                              node_indices=node_indices,
                                                                              directionality_mask=directionality_mask,
                                                                              forward_value=forward_value,
                                                                              backward_value=backward_value,
                                                                              leaf_indicator=leaf_indicator)

    node_segments = [[idx] * len(item) for idx, item in enumerate(node_indices)]
    node_segments = [item for seq in node_segments for item in seq]
    node_indices = [item for seq in node_indices for item in seq]
    directionality_mask = [item for seq in directionality_mask for item in seq]

    return tree_nodes, edge_indices, tree_features, node_indices, node_segments, directionality_mask, leaf_indicator


def retrieve_dep_tree_info_adj(tree_str, is_directional=True):
    """
    Orders data according to dependency relations
    tuple format: (dependency, source_idx, source_token, target_idx, target_token)
    """

    dep_tree = ast.literal_eval(tree_str)

    def _expand(node, node_idx, parent_idx, dep_tree, to_explore, tree_nodes, tree_features, edge_indices,
                node_tree_ids, parent_node_ids, node_indices, directionality_mask, forward_value, backward_value):

        to_keep = []
        found_children = []

        for item_idx, item in enumerate(dep_tree):
            if node == item[2]:
                found_children.append((item[3], item[4], item[0]))
            else:
                to_keep.append(item_idx)

        # depth-first
        found_children = sorted(found_children, key=lambda item: item[0], reverse=True)
        for item in found_children:
            to_explore.insert(0, item[1])
            tree_features.append(item[2])
            parent_node_ids.insert(0, node_idx)

        # filter explored triples
        dep_tree = [item for idx, item in enumerate(dep_tree) if idx in to_keep]

        # update explored nodes
        tree_nodes.append(node)
        node_tree_ids.append(node_idx + 1)

        if parent_idx is not None:
            edge_indices.append([parent_idx, node_idx])

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(node_idx - 1)
                directionality_mask[parent_idx].append(backward_value)
            else:
                node_indices.append([node_idx - 1])
                directionality_mask.append([backward_value])
            node_indices.append([node_idx - 1])
            directionality_mask.append([forward_value])

        return to_explore, node_tree_ids, parent_node_ids, \
               tree_nodes, tree_features, edge_indices, node_indices, directionality_mask, dep_tree

    to_explore = ['ROOT']
    tree_nodes = []
    tree_features = []
    edge_indices = []
    node_indices = []
    directionality_mask = []
    node_tree_ids = [0]
    parent_node_ids = [None]

    forward_value = 1
    backward_value = 1

    if is_directional:
        backward_value = -1

    while len(to_explore) > 0:
        first = to_explore.pop(0)
        first_idx = node_tree_ids.pop(0)
        first_parent_idx = parent_node_ids.pop(0)
        to_explore, node_tree_ids, parent_node_ids, tree_nodes, \
        tree_features, edge_indices, \
        node_indices, directionality_mask, dep_tree = _expand(node=first,
                                                              node_idx=first_idx,
                                                              parent_idx=first_parent_idx,
                                                              dep_tree=dep_tree,
                                                              to_explore=to_explore,
                                                              tree_nodes=tree_nodes,
                                                              tree_features=tree_features,
                                                              edge_indices=edge_indices,
                                                              node_tree_ids=node_tree_ids,
                                                              parent_node_ids=parent_node_ids,
                                                              node_indices=node_indices,
                                                              directionality_mask=directionality_mask,
                                                              forward_value=forward_value,
                                                              backward_value=backward_value)

    # node_segments = [[idx] * len(item) for idx, item in enumerate(node_indices)]
    # node_segments = [item for seq in node_segments for item in seq]
    # node_indices = [item for seq in node_indices for item in seq]
    # directionality_mask = [item for seq in directionality_mask for item in seq]

    row_idxs = [pair[0] for pair in edge_indices]
    col_idxs = [pair[1] for pair in edge_indices]

    adjacency_matrix = np.zeros((len(tree_nodes), len(tree_nodes)), dtype=np.int32)
    adjacency_matrix[row_idxs, col_idxs] = -1 if is_directional else 1  # senders
    adjacency_matrix[col_idxs, row_idxs] = 1  # receivers

    # repeated_edge_features = np.zeros((len(tree_nodes), len(tree_nodes)), dtype=np.int32)
    # repeated_edge_features[row_idxs, col_idxs] = tree_features
    # repeated_edge_features[col_idxs, row_idxs] = [item - 1 for item in tree_features]

    return tree_nodes, None, adjacency_matrix


def get_stk(node_idxs, depths, is_directional=False):
    sub_trees = []
    subtrees_per_node = {}
    subtree_to_idx = {}

    edge_indices = []
    edge_features = []
    node_indices = []
    node_segments = []
    directionality_mask = []

    forward_value = 1
    backward_value = 1

    if is_directional:
        backward_value = -1

    max_depth = np.max(depths)

    for node_enum, (node_idx, node_depth) in enumerate(zip(node_idxs, depths)):
        if node_idx == 0:
            continue

        if node_depth == max_depth:
            continue

        limit_boundary = []
        for idx, value in enumerate(depths[node_enum + 1:]):
            if value < node_depth:
                limit_boundary.append(idx + node_enum + 1)
                break
            if value == node_depth:
                limit_boundary.append(idx + node_enum + 1)

        if not limit_boundary:
            limit_boundary = len(node_idxs)
        else:
            limit_boundary = limit_boundary[0]

        to_add = node_idxs[node_enum:limit_boundary]
        if len(to_add) > 1:
            sub_trees.append(to_add)
            subtrees_per_node.setdefault(node_idx, []).append(to_add)
            subtree_to_idx["-".join(to_add.astype(str))] = len(sub_trees) - 1

        # connect subtree to previous node subtrees
        if node_depth > 1 and len(to_add) > 1:
            previous_node = [other_node_idx for other_node_enum, (other_node_idx, other_node_depth) in
                             enumerate(zip(node_idxs, depths))
                             if other_node_depth == node_depth - 1 and other_node_enum < node_enum]
            assert len(previous_node) >= 1
            previous_node = previous_node[-1]

            assert previous_node in subtrees_per_node

            left_edge_index = subtree_to_idx['-'.join(subtrees_per_node[previous_node][0].astype(str))]
            right_edge_index = len(sub_trees) - 1
            edge_indices.append([left_edge_index, right_edge_index])
            edge_features.append(node_depth)

    # Connect first level subtrees in sequential pairwise fashion
    first_level_node_indexes = [node_idx for node_idx, node_depth in zip(node_idxs, depths) if
                                node_depth == 1 and node_idx in subtrees_per_node]
    for j in range(len(first_level_node_indexes) - 1):
        left_pair_element = first_level_node_indexes[j]
        right_pair_element = first_level_node_indexes[j + 1]

        left_edge_index = subtree_to_idx['-'.join(subtrees_per_node[left_pair_element][0].astype(str))]
        right_edge_index = subtree_to_idx['-'.join(subtrees_per_node[right_pair_element][0].astype(str))]

        edge_indices.append([left_edge_index, right_edge_index])
        edge_features.append(1)

    # Build node indices
    for tree_idx in range(len(sub_trees)):
        for edge_idx, edge_pair in enumerate(edge_indices):
            if tree_idx in edge_pair:
                node_indices.append(edge_idx)
                node_segments.append(tree_idx)

                # Node is a sender
                if tree_idx == edge_pair[0]:
                    directionality_mask.append(backward_value)
                else:
                    directionality_mask.append(forward_value)

    return sub_trees, edge_indices, edge_features, node_indices, node_segments, directionality_mask


def get_terminal_indexes(node_idxs, depths):
    """
    Detect depth decrease or plateau
    """

    local_terminal_idxs = []

    if len(node_idxs) == 2:
        return local_terminal_idxs

    for idx, (node_idx, depth_value) in enumerate(zip(node_idxs, depths)):
        if idx + 1 < len(depths) and depths[idx + 1] <= depth_value:
            local_terminal_idxs.append(node_idx)

    local_terminal_idxs.append(node_idxs[-1])

    return local_terminal_idxs


def get_sstk(node_idxs, depths, is_directional=False):
    sub_trees = []
    subtrees_per_node = {}
    subtree_to_idx = {}

    edge_features = []
    edge_indices = []
    node_indices = []
    node_segments = []
    directionality_mask = []

    forward_value = 1
    backward_value = 1

    if is_directional:
        backward_value = -1

    max_depth = np.max(depths)

    for node_enum, (node_idx, node_depth) in enumerate(zip(node_idxs, depths)):
        if node_idx == 0:
            continue

        if node_depth == max_depth:
            continue

        limit_boundary = []
        for idx, value in enumerate(depths[node_enum + 1:]):
            if value < node_depth:
                limit_boundary.append(idx + node_enum + 1)
                break
            if value == node_depth:
                limit_boundary.append(idx + node_enum + 1)

        if not limit_boundary:
            limit_boundary = len(node_idxs)
        else:
            limit_boundary = limit_boundary[0]

        to_add = node_idxs[node_enum:limit_boundary]
        if len(to_add) > 1:
            sub_trees.append(to_add)
            subtrees_per_node.setdefault(node_idx, []).append(to_add)
            subtree_to_idx["-".join(to_add.astype(str))] = len(sub_trees) - 1

            # get local terminals
            local_terminal_idxs = get_terminal_indexes(to_add, depths[node_enum:limit_boundary])

            # remove local terminal combinations (from single local terminal to all of them)
            for combination_range_value in tqdm(range(1, len(local_terminal_idxs) + 1)):
                curr_combination_set = list(combinations(local_terminal_idxs, combination_range_value))
                for comb in curr_combination_set:
                    mod_to_add = np.array([item for item in to_add if item not in comb])
                    sub_trees.append(mod_to_add)
                    subtrees_per_node.setdefault(node_idx, []).append(mod_to_add)
                    subtree_to_idx["-".join(mod_to_add.astype(str))] = len(sub_trees) - 1

            # Connect node subtrees to each other
            for j in range(len(subtrees_per_node[node_idx]) - 1):
                for k in range(j + 1, len(subtrees_per_node[node_idx])):
                    left_pair_element = subtrees_per_node[node_idx][j]
                    right_pair_element = subtrees_per_node[node_idx][k]

                    left_edge_index = subtree_to_idx['-'.join(left_pair_element.astype(str))]
                    right_edge_index = subtree_to_idx['-'.join(right_pair_element.astype(str))]

                    edge_indices.append([left_edge_index, right_edge_index])
                    edge_features.append(node_depth)

            # Connect subtree to previous node subtrees
            if node_depth > 1 and len(to_add) > 1:
                previous_node = [other_node_idx for other_node_enum, (other_node_idx, other_node_depth) in
                                 enumerate(zip(node_idxs, depths))
                                 if other_node_depth == node_depth - 1 and other_node_enum < node_enum]
                assert len(previous_node) >= 1
                previous_node = previous_node[-1]

                assert previous_node in subtrees_per_node

                for j in range(len(subtrees_per_node[previous_node])):
                    for k in range(len(subtrees_per_node[node_idx])):
                        left_pair_element = subtrees_per_node[previous_node][j]
                        right_pair_element = subtrees_per_node[node_idx][k]

                        left_edge_index = subtree_to_idx['-'.join(left_pair_element.astype(str))]
                        right_edge_index = subtree_to_idx['-'.join(right_pair_element.astype(str))]

                        edge_indices.append([left_edge_index, right_edge_index])
                        edge_features.append(node_depth)

    # Connect first level subtrees in sequential pairwise fashion
    first_level_node_indexes = [node_idx for node_idx, node_depth in zip(node_idxs, depths) if
                                node_depth == 1 and node_idx in subtrees_per_node]
    for j in range(len(first_level_node_indexes) - 1):
        left_tree_idx = first_level_node_indexes[j]
        right_tree_idx = first_level_node_indexes[j + 1]

        for z in range(len(subtrees_per_node[left_tree_idx])):
            for k in range(len(subtrees_per_node[right_tree_idx])):
                left_pair_element = subtrees_per_node[left_tree_idx][z]
                right_pair_element = subtrees_per_node[right_tree_idx][k]

                left_edge_index = subtree_to_idx['-'.join(left_pair_element.astype(str))]
                right_edge_index = subtree_to_idx['-'.join(right_pair_element.astype(str))]

                edge_indices.append([left_edge_index, right_edge_index])
                edge_features.append(1)

    # Build node indices
    for tree_idx in range(len(sub_trees)):
        for edge_idx, edge_pair in enumerate(edge_indices):
            if tree_idx in edge_pair:
                node_indices.append(edge_idx)
                node_segments.append(tree_idx)

                # Node is a sender
                if tree_idx == edge_pair[0]:
                    directionality_mask.append(backward_value)
                else:
                    directionality_mask.append(forward_value)

    return sub_trees, edge_indices, edge_features, node_indices, node_segments, directionality_mask


def get_subtree_info(node_idxs, depths, is_directional=False, kernel='stk'):
    if kernel.lower() == 'stk':
        return get_stk(node_idxs, depths, is_directional=is_directional)
    elif kernel.lower() == 'sstk':
        return get_sstk(node_idxs, depths, is_directional=is_directional)
    else:
        raise RuntimeError('Unsupported kernel mode! Got: {}'.format(kernel))


def build_assignment_matrix(subtrees, nodes):
    assignment_matrix = np.zeros((len(nodes), len(subtrees)), dtype=np.int32)

    for idx, subtree in enumerate(subtrees):
        for node_idx in subtree:
            assignment_matrix[node_idx, idx] = 1.0

    return assignment_matrix


def build_node_spans(nodes, subtree_nodes):
    node_spans = np.zeros((len(nodes), len(nodes)))

    for subtree in subtree_nodes:
        node_spans[subtree[0], subtree] = 1.

    for node in nodes:
        node_spans[node, node] = 1.

    node_spans[0] = np.ones_like(node_spans[0])

    return node_spans


def retrieve_tree_info(tree_text, is_directional=False):
    tree = nltk.tree.Tree.fromstring(tree_text)

    nodes = ['ROOT']
    edges = []
    depth = 0
    node_indexes = {'ROOT': 0}
    node_edges_info = []
    node_segments = []
    edge_indices = []
    directionality_mask = []
    leaf_indicator = [0]

    full_nodes, full_edge_indices, \
    full_edge_features, full_node_indices, \
    full_node_segments, full_directionality_mask, full_leaf_indicator = depth_node_search(nodes=nodes,
                                                                                          parent=tree,
                                                                                          depth=depth,
                                                                                          node_indices=node_edges_info,
                                                                                          node_indexes=node_indexes,
                                                                                          edge_indices=edge_indices,
                                                                                          node_segments=node_segments,
                                                                                          edges=edges,
                                                                                          directionality_mask=directionality_mask,
                                                                                          is_directional=is_directional,
                                                                                          leaf_indicator=leaf_indicator
                                                                                          )

    full_node_indices = [item for seq in full_node_indices for item in seq]
    full_node_segments = [item for seq in full_node_segments for item in seq]
    full_directionality_mask = [item for seq in full_directionality_mask for item in seq]

    return full_nodes, full_edge_indices, full_edge_features, full_node_indices, \
           full_node_segments, full_directionality_mask, full_leaf_indicator


def retrieve_tree_info_adj(tree_text, is_directional=False):
    tree = nltk.tree.Tree.fromstring(tree_text)

    nodes = ['ROOT']
    edges = []
    depth = 0
    node_indexes = {'ROOT': 0}
    node_edges_info = []
    node_segments = []
    edge_indices = []
    directionality_mask = []
    leaf_indicator = [0]

    full_nodes, full_edge_indices, \
    full_edge_features, full_node_indices, \
    full_node_segments, full_directionality_mask, full_leaf_indicator = depth_node_search(nodes=nodes,
                                                                                          parent=tree,
                                                                                          depth=depth,
                                                                                          node_indices=node_edges_info,
                                                                                          node_indexes=node_indexes,
                                                                                          edge_indices=edge_indices,
                                                                                          node_segments=node_segments,
                                                                                          edges=edges,
                                                                                          directionality_mask=directionality_mask,
                                                                                          is_directional=is_directional,
                                                                                          leaf_indicator=leaf_indicator
                                                                                          )

    row_idxs = [pair[0] for pair in full_edge_indices]
    col_idxs = [pair[1] for pair in full_edge_indices]

    adjacency_matrix = np.zeros((len(full_nodes), len(full_nodes)), dtype=np.int32)
    adjacency_matrix[row_idxs, col_idxs] = -1 if is_directional else 1  # senders
    adjacency_matrix[col_idxs, row_idxs] = 1  # receivers
    adjacency_matrix += np.eye(len(full_nodes), dtype=adjacency_matrix.dtype)

    repeated_edge_features = np.zeros((len(full_nodes), len(full_nodes)), dtype=np.int32)
    repeated_edge_features[row_idxs, col_idxs] = full_edge_features
    repeated_edge_features[col_idxs, row_idxs] = [item - 1 for item in full_edge_features]

    return full_nodes, repeated_edge_features, adjacency_matrix


def retrieve_hierarchical_tree_adj_info(tree_text, is_directional=False, kernel='stk'):
    nodes, edge_features, adjacency_matrix = retrieve_tree_info_adj(tree_text, is_directional=is_directional)

    node_idxs = np.arange(len(nodes))
    node_depths = get_nodes_depths(tree_text)

    subtree_nodes, subtree_edge_indices, subtree_edge_features, \
    subtree_node_indices, subtree_node_segments, \
    subtree_directionality_mask = get_subtree_info(node_idxs=node_idxs,
                                                   depths=node_depths,
                                                   is_directional=is_directional,
                                                   kernel=kernel)

    return nodes, edge_features, adjacency_matrix, subtree_nodes


def retrieve_hierarchical_dep_tree_adj_info(tree_text, is_directional=False, kernel='stk'):
    nodes, edge_features, adjacency_matrix = retrieve_dep_tree_info_adj(tree_text, is_directional=is_directional)

    node_idxs = np.arange(len(nodes))
    node_depths = get_dep_nodes_depths(tree_text)

    subtree_nodes, subtree_edge_indices, subtree_edge_features, \
    subtree_node_indices, subtree_node_segments, \
    subtree_directionality_mask = get_subtree_info(node_idxs=node_idxs,
                                                   depths=node_depths,
                                                   is_directional=is_directional,
                                                   kernel=kernel)

    return nodes, edge_features, adjacency_matrix, subtree_nodes


def retrieve_hierarchical_tree_info(tree_text, is_directional=False, kernel='stk'):
    nodes, edge_indices, edge_features, \
    node_indices, node_segments, \
    directionality_mask, leaf_indicator = retrieve_tree_info(tree_text,
                                                             is_directional=is_directional)

    node_idxs = np.arange(len(nodes))
    node_depths = get_nodes_depths(tree_text)

    subtree_nodes, subtree_edge_indices, subtree_edge_features, \
    subtree_node_indices, subtree_node_segments, \
    subtree_directionality_mask = get_subtree_info(node_idxs=node_idxs,
                                                   depths=node_depths,
                                                   is_directional=is_directional,
                                                   kernel=kernel)

    return nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask, \
           subtree_nodes, subtree_edge_indices, subtree_edge_features, subtree_node_indices, \
           subtree_node_segments, subtree_directionality_mask


def retrieve_hierarchical_dep_tree_info(tree_text, is_directional=False, kernel='stk'):
    nodes, edge_indices, edge_features, \
    node_indices, node_segments, \
    directionality_mask, leaf_indicator = retrieve_dep_tree_info(tree_text,
                                                 is_directional=is_directional)

    node_idxs = np.arange(len(nodes))
    node_depths = get_dep_nodes_depths(tree_text)

    subtree_nodes, subtree_edge_indices, subtree_edge_features, \
    subtree_node_indices, subtree_node_segments, \
    subtree_directionality_mask = get_subtree_info(node_idxs=node_idxs,
                                                   depths=node_depths,
                                                   is_directional=is_directional,
                                                   kernel=kernel)

    return nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask, leaf_indicator, \
           subtree_nodes, subtree_edge_indices, subtree_edge_features, subtree_node_indices, \
           subtree_node_segments, subtree_directionality_mask


def retrieve_trees_info(trees, is_directional=False, memo=None):
    nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask, leaf_indicator = [], [], [], [], [], [], []

    for tree in tqdm(trees):

        if memo is not None and tree in memo:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, tree_directionality_mask, tree_leaf_indicator = memo[tree]
        else:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, tree_directionality_mask, tree_leaf_indicator = retrieve_tree_info(
                tree,
                is_directional=is_directional)

            if memo is not None:
                memo[tree] = (
                    tree_nodes, tree_edge_indices, tree_edge_features, tree_node_indices, tree_node_segments,
                    tree_directionality_mask, tree_leaf_indicator)

        nodes.append(tree_nodes)
        edge_indices.append(tree_edge_indices)
        edge_features.append(tree_edge_features)
        node_indices.append(tree_node_indices)
        node_segments.append(tree_node_segments)
        directionality_mask.append(tree_directionality_mask)
        leaf_indicator.append(tree_leaf_indicator)

    return (nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask,
            leaf_indicator), memo


def retrieve_hierarchical_trees_info(trees, is_directional=False, kernel='stk', memo=None):
    nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask = [], [], [], [], [], []
    subtree_indices, subtree_edge_indices, subtree_edge_features, \
    subtree_node_indices, subtree_node_segments, subtree_directionality_mask = [], [], [], [], [], []

    for tree in tqdm(trees):

        if memo is not None and tree in memo:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, \
            tree_directionality_mask, tree_subtree_indices, \
            tree_subtree_edge_indices, tree_subtree_edge_features, \
            tree_subtree_node_indices, tree_subtree_node_segments, \
            tree_subtree_directionality_mask = memo[tree]
        else:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, \
            tree_directionality_mask, tree_subtree_indices, \
            tree_subtree_edge_indices, tree_subtree_edge_features, \
            tree_subtree_node_indices, tree_subtree_node_segments, \
            tree_subtree_directionality_mask = retrieve_hierarchical_tree_info(tree,
                                                                               is_directional=is_directional,
                                                                               kernel=kernel)

            if memo is not None:
                memo[tree] = (
                    tree_nodes, tree_edge_indices, tree_edge_features,
                    tree_node_indices, tree_node_segments,
                    tree_directionality_mask, tree_subtree_indices,
                    tree_subtree_edge_indices, tree_subtree_edge_features,
                    tree_subtree_node_indices, tree_subtree_node_segments,
                    tree_subtree_directionality_mask)

        nodes.append(tree_nodes)
        edge_indices.append(tree_edge_indices)
        edge_features.append(tree_edge_features)
        node_indices.append(tree_node_indices)
        node_segments.append(tree_node_segments)
        directionality_mask.append(tree_directionality_mask)

        subtree_indices.append(tree_subtree_indices)
        subtree_edge_indices.append(tree_subtree_edge_indices)
        subtree_edge_features.append(tree_subtree_edge_features)
        subtree_node_indices.append(tree_subtree_node_indices)
        subtree_node_segments.append(tree_subtree_node_segments)
        subtree_directionality_mask.append(tree_subtree_directionality_mask)

    return (nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask,
            subtree_indices, subtree_edge_indices, subtree_edge_features,
            subtree_node_indices, subtree_node_segments, subtree_directionality_mask
            ), memo


def retrieve_hierarchical_dep_trees_info(trees, is_directional=False, kernel='stk', memo=None):
    nodes, edge_indices, edge_features,\
    node_indices, node_segments,\
    directionality_mask, leaf_indicator = [], [], [], [], [], [], []
    subtree_indices, subtree_edge_indices, subtree_edge_features, \
    subtree_node_indices, subtree_node_segments, subtree_directionality_mask = [], [], [], [], [], []

    for tree in tqdm(trees):

        if memo is not None and tree in memo:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, \
            tree_directionality_mask, tree_leaf_indicator, tree_subtree_indices, \
            tree_subtree_edge_indices, tree_subtree_edge_features, \
            tree_subtree_node_indices, tree_subtree_node_segments, \
            tree_subtree_directionality_mask = memo[tree]
        else:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, \
            tree_directionality_mask, tree_leaf_indicator, tree_subtree_indices, \
            tree_subtree_edge_indices, tree_subtree_edge_features, \
            tree_subtree_node_indices, tree_subtree_node_segments, \
            tree_subtree_directionality_mask = retrieve_hierarchical_dep_tree_info(tree,
                                                                                   is_directional=is_directional,
                                                                                   kernel=kernel)

            if memo is not None:
                memo[tree] = (
                    tree_nodes, tree_edge_indices, tree_edge_features,
                    tree_node_indices, tree_node_segments,
                    tree_directionality_mask, tree_leaf_indicator, tree_subtree_indices,
                    tree_subtree_edge_indices, tree_subtree_edge_features,
                    tree_subtree_node_indices, tree_subtree_node_segments,
                    tree_subtree_directionality_mask)

        nodes.append(tree_nodes)
        edge_indices.append(tree_edge_indices)
        edge_features.append(tree_edge_features)
        node_indices.append(tree_node_indices)
        node_segments.append(tree_node_segments)
        directionality_mask.append(tree_directionality_mask)
        leaf_indicator.append(tree_leaf_indicator)

        subtree_indices.append(tree_subtree_indices)
        subtree_edge_indices.append(tree_subtree_edge_indices)
        subtree_edge_features.append(tree_subtree_edge_features)
        subtree_node_indices.append(tree_subtree_node_indices)
        subtree_node_segments.append(tree_subtree_node_segments)
        subtree_directionality_mask.append(tree_subtree_directionality_mask)

    return (nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask, leaf_indicator,
            subtree_indices, subtree_edge_indices, subtree_edge_features,
            subtree_node_indices, subtree_node_segments, subtree_directionality_mask
            ), memo


# TODO: directionality and memo don't work well together, memo is deprecated atm
def retrieve_dep_trees_info(trees, is_directional=True, memo=None):
    nodes, edge_indices, edge_features, node_indices, \
    node_segments, directionality_mask, leaf_indicator = [], [], [], [], [], [], []

    for tree in tqdm(trees):

        if memo is not None and tree in memo:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments, tree_directionality_mask = memo[tree]
        else:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, \
            tree_node_segments, \
            tree_directionality_mask, tree_leaf_indicator = retrieve_dep_tree_info(tree,
                                                                                   is_directional=is_directional)

            if memo is not None:
                memo[tree] = (tree_nodes,
                              tree_edge_indices,
                              tree_edge_features,
                              tree_node_indices,
                              tree_node_segments,
                              tree_directionality_mask)

        nodes.append(tree_nodes)
        edge_indices.append(tree_edge_indices)
        edge_features.append(tree_edge_features)
        node_indices.append(tree_node_indices)
        node_segments.append(tree_node_segments)
        directionality_mask.append(tree_directionality_mask)
        leaf_indicator.append(tree_leaf_indicator)

    return (nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask, leaf_indicator), memo


def pad_tree_data(nodes, edge_indices, edge_features, node_indices, node_segments,
                  padding_length=None, features_padding=None, padding='post'):
    if padding_length is None:
        padding_length = max([len(graph) for graph in nodes])
        edge_padding = max([len(edge) for edge in edge_features])
    else:
        edge_padding = padding_length - 1

    if features_padding is None:
        features_padding = max([len(seq) for features in edge_features for seq in features])

    graphs_pad_amount = [padding_length - len(graph) for graph in nodes]

    padded_nodes = pad_data(nodes, padding=padding, padding_length=padding_length)

    padded_edge_features = pad_data(list(map(lambda features: pad_data(features,
                                                                       padding='pre',
                                                                       padding_length=features_padding),
                                             edge_features)),
                                    padding=padding,
                                    padding_length=edge_padding)

    padded_edge_indices = [edge_indices + [[0, 0] for _ in range(pad_amount)] for edge_indices, pad_amount in
                           zip(edge_indices, graphs_pad_amount)]
    padded_edge_indices = np.array([item[:edge_padding] for item in padded_edge_indices])

    edge_mask = [[1] * (padding_length - 1 - pad_amount) + [0] * pad_amount for pad_amount in graphs_pad_amount]
    edge_mask = np.array([item[:edge_padding] for item in edge_mask])

    padded_node_indices = [indices + [max(indices)] * pad_amount * 2 for indices, pad_amount in
                           zip(node_indices, graphs_pad_amount)]
    padded_node_indices = np.array([item[:edge_padding * 2] for item in padded_node_indices])

    padded_node_segments = []
    for idx, (segments, pad_amount) in enumerate(zip(node_segments, graphs_pad_amount)):
        segment_max = max(segments)
        to_add = np.arange(segment_max + 1, (segment_max + 1) + pad_amount).tolist()
        padded_segments = segments + [val for val in to_add for _ in (0, 1)]
        padded_segments = [item + idx * padding_length for item in padded_segments]
        padded_segments = padded_segments[:edge_padding * 2]
        padded_node_segments.append(padded_segments)

    padded_node_segments = np.array(padded_node_segments)

    return padded_nodes, padded_edge_indices, padded_edge_features, \
           padded_node_indices, padded_node_segments, edge_mask
