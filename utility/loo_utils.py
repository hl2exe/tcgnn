
import numpy as np


class LOOSplitter(object):

    def split(self, data, split_key):
        unique_keys = np.unique(data[split_key].values)
        for key in unique_keys:
            yield data[~data[split_key].isin([key])], data[data[split_key].isin([key])], key