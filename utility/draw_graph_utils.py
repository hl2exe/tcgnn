from datetime import datetime

import matplotlib.pyplot as plt
import networkx as nx
import nltk
import numpy as np
from networkx.drawing.nx_agraph import graphviz_layout
from nltk.draw import draw_trees
from tqdm import tqdm
from utility.log_utils import Logger
from collections import Counter


def draw_tree(nodes, adjacency_matrix, pad_mask=None, node_names=None, title_name='Tree', image_path='.',
              image_name=None):
    # ordering -> depth-first
    # nodes -> intensity of nodes (vector of N elements)
    # adjacency_matrix -> N x N

    G = nx.DiGraph()

    pad_mask = pad_mask if pad_mask is not None else np.ones_like(nodes)

    if type(pad_mask) != np.ndarray:
        pad_mask = np.array(pad_mask)

    # Filter out padding nodes
    nodes = nodes[pad_mask == 1]

    node_names = node_names if node_names is not None else list(map(str, range(len(nodes))))

    if type(node_names) != np.ndarray:
        node_names = np.array(node_names)

    # Retrieve edges
    node_amount = len(nodes)

    edges = []
    for i in range(node_amount):
        for j in range(i + 1, node_amount):
            if adjacency_matrix[i, j] and pad_mask[i] == 1 and pad_mask[j] == 1:
                edges.append((node_names[i], node_names[j]))

    G.add_nodes_from(node_names)
    G.add_edges_from(edges)

    # Attributes
    adjusted_node_intensities = [nodes[np.where(node_names == G_node)[0][0]] for G_node in G.nodes()]

    node_attributes = {node_name: "{:.2f}".format(node_strength) for node_name, node_strength
                       in zip(G.nodes(), adjusted_node_intensities)}

    # Save graph in dot format
    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    # image_name = image_name if image_name is not None else 'tree_{}.dot'.format(current_date)
    # write_dot(G, os.path.join(image_path, image_name))

    # Draw tree

    fig, ax = plt.subplots(1, 1)
    ax.set_title(title_name)
    pos = graphviz_layout(G, prog='dot')

    pos_attrs = {}
    for node, coords in pos.items():
        pos_attrs[node] = (coords[0], coords[1])

    nx.draw(G, pos, cmap=plt.get_cmap('coolwarm'),
            node_color=adjusted_node_intensities,
            vmin=0.0,
            vmax=1.0,
            with_labels=False,
            font_color='white',
            node_size=900.)
    nx.draw_networkx_labels(G, pos_attrs, labels=node_attributes)

    # Clean
    # os.remove(os.path.join(image_path, image_name))

    return fig


def draw_nltk_tree(tree_text):
    tree = nltk.tree.Tree.fromstring(tree_text)
    draw_trees(tree)


def retrieve_pooling_largest_sequences(pooling_matrix, adjacency_matrix,
                                       node_spans, node_mask, pooling_threshold,
                                       ignore_leaves=False):
    samples = pooling_matrix.shape[0]
    meta_node_amount = pooling_matrix.shape[1]

    # [batch_size,]
    true_size = np.sum(node_mask, axis=-1)

    # zero-out intensities below threshold
    pooling_matrix[pooling_matrix < pooling_threshold] = 0.0

    # node loop
    def _find_node_sequence(node_idx, pooling_matrix, adjacency_matrix, node_spans):
        sequence = [node_idx]

        parent = node_idx

        forward_adjacency_matrix = np.triu(adjacency_matrix) - np.eye(adjacency_matrix.shape[0])

        to_explore = np.where(forward_adjacency_matrix[parent])[0]

        while len(to_explore):
            node, to_explore = to_explore[0], to_explore[1:]

            if pooling_matrix[node] == 0.0 or (ignore_leaves and np.sum(node_spans[sample, node]) == 1 and node != 0):
                continue
            else:
                if node_spans[node_idx, node] == 1.0:
                    sequence.append(node)
                    to_add = np.where(forward_adjacency_matrix[node])[0]
                    to_explore = np.concatenate((to_add, to_explore))

        return sequence

    k_sequences = {}
    for sample in tqdm(range(samples)):
        for k in range(meta_node_amount):

            cluster_sequences = []

            # get per node sequence
            for node in range(true_size[sample]):

                if pooling_matrix[sample, k, node] == 0. or \
                        (ignore_leaves and np.sum(node_spans[sample, node]) == 1 and node != 0):
                    continue
                else:
                    node_sequence = _find_node_sequence(node, pooling_matrix[sample, k],
                                                        adjacency_matrix[sample], node_spans[sample])
                    cluster_sequences.append(node_sequence)

            # get longest sequence
            lengths = [len(seq) for seq in cluster_sequences]
            longest_sequence = cluster_sequences[np.argmax(lengths)]
            k_sequences.setdefault(sample, []).append(longest_sequence)

    return k_sequences


def convert_pooling_sequences_to_text(sequences, tokenizer, token_ids):

    token_sequences = {}
    for sample, ids_seq in tqdm(enumerate(token_ids)):
        for relative_id_seq in sequences[sample]:
            absolute_id_seq = ids_seq[relative_id_seq]
            conv_id_seq = tokenizer.convert_ids_to_tokens(absolute_id_seq)
            token_sequences.setdefault(sample, []).append(conv_id_seq)

    return token_sequences


def compute_pooling_sequence_statistics(token_sequences, labels, classes, max_samples=3):

    # General statistics
    all_token_sequences = list(token_sequences.values())
    all_token_sequences = [seq for group in all_token_sequences for seq in group]
    str_all_token_sequences = ['-'.join(seq) for seq in all_token_sequences]

    unique_sequences = set(str_all_token_sequences)
    lengths = [len(item) for item in all_token_sequences]

    Logger.get_logger(__name__).info('Total sequences: {}'.format(len(all_token_sequences)))
    Logger.get_logger(__name__).info('Unique sequences: {}'.format(len(unique_sequences)))
    Logger.get_logger(__name__).info('Average sequence length: {}'.format(lengths))

    # Per class statistics
    common_sequences = None

    for c in range(classes):
        Logger.get_logger(__name__).info('Considering class: {}'.format(c))

        class_idxs = np.where(labels == c)[0]
        class_token_sequences = {key: value for key, value in token_sequences.items() if key in class_idxs}
        class_token_sequences = list(class_token_sequences.values())
        class_token_sequences = [seq for group in class_token_sequences for seq in group]
        str_class_token_sequences = ['-'.join(seq) for seq in class_token_sequences]

        class_top_counts = Counter(str_class_token_sequences).most_common(max_samples)

        Logger.get_logger(__name__).info('Class {0} total sequences: {1}'.format(c, len(str_class_token_sequences)))
        Logger.get_logger(__name__).info('Class {0} top {1} sequences: {2}'.format(c, max_samples, class_top_counts))

        if common_sequences is None:
            common_sequences = set(str_class_token_sequences)
        else:
            common_sequences = common_sequences.intersection(set(str_class_token_sequences))

    # Shared sequences
    Logger.get_logger(__name__).info('Shared total sequences: {}'.format(len(common_sequences)))
    Logger.get_logger(__name__).info('Shared sequences: {}'.format(common_sequences))
