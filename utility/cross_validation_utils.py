"""

@Author: Federico Ruggeri
@Date: 22/03/2019

TODO: merge compute_metrics and compute_iteration_validation_error

"""

import numpy as np
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.model_selection._split import _BaseKFold

from utility.json_utils import save_json, load_json
from utility.log_utils import Logger

supported_custom_metrics = {
}


def build_metrics(error_metrics):
    """
    Build validation metrics from given metrics name and problem type

    :param error_metrics: list of error metrics names (strings)
    :return: list of pointers to error metrics functions
    """

    parsed_metrics = []

    for metric in error_metrics:
        if hasattr(metrics, metric):
            parsed_metrics.append(getattr(metrics, metric))
        elif metric in supported_custom_metrics:
            parsed_metrics.append(supported_custom_metrics[metric])
        else:
            Logger.get_logger(__name__).warn('Unknown metric given (got {})! Skipping...'.format(metric))

    # Empty metrics list
    if not parsed_metrics:
        message = 'No valid metrics found! Aborting...'
        Logger.get_logger(__name__).error(message)
        raise RuntimeError(message)

    return parsed_metrics


def compute_metrics(parsed_metrics, true_values, predicted_values, additional_metrics_info,
                    metrics_nicknames=None, suffix=None, prefix=None, is_multilabel=False):
    """
    Computes each given metric value, given true and predicted values.
    """

    if not is_multilabel:
        if len(true_values.shape) > 1 and true_values.shape[1] > 1:
            true_values = np.argmax(true_values, axis=1)
            predicted_values = np.argmax(predicted_values, axis=1)

    if metrics_nicknames is None:
        metrics_nicknames = [metric.__name__ for metric in parsed_metrics]

    metrics_result = {}
    for metric, metric_info, metric_name in zip(parsed_metrics, additional_metrics_info, metrics_nicknames):

        signal_error = metric(y_true=true_values, y_pred=predicted_values,
                              **metric_info)

        if suffix is not None:
            metric_name = '{0}_{1}'.format(metric_name, suffix)
        if prefix is not None:
            metric_name = '{0}_{1}'.format(prefix, metric_name)

        metrics_result[metric_name] = signal_error

    return metrics_result


def show_data_shapes(data, key):
    """
    Prints data shape (it could be a nested structure).
    Currently, the following nested structures are supported:

        1) List
        2) Numpy.ndarray
        3) Dict
    """

    if type(data) is np.ndarray:
        data_shape = data.shape
    elif type(data) is list:
        data_shape = [len(item) for item in data]
    else:
        data_shape = [(key, len(item)) for key, item in data.items()]

    Logger.get_logger(__name__).info('[{0}] Shape(s): {1}'.format(key, data_shape))


def compute_iteration_validation_error(parsed_metrics, true_values, predicted_values,
                                       error_metrics_additional_info=None,
                                       error_metrics_nicknames=None):
    """
    Computes each given metric value, given true and predicted values.

    :param parsed_metrics: list of metric functions (typically sci-kit learn metrics)
    :param true_values: ground-truth values
    :param predicted_values: model predicted values
    :param error_metrics_additional_info: additional arguments for each metric
    :param error_metrics_nicknames: custom nicknames for each metric
    :return: dict as follows:

        key: metric.__name__
        value: computed metric value
    """

    error_metrics_additional_info = error_metrics_additional_info or {}
    fold_error_info = {}

    if len(true_values.shape) > 1 and true_values.shape[1] > 1:
        true_values = np.argmax(true_values, axis=1)
        predicted_values = np.argmax(predicted_values, axis=1)

    if error_metrics_nicknames is None:
        error_metrics_nicknames = [metric.__name__ for metric in parsed_metrics]

    for metric, metric_info, metric_name in zip(parsed_metrics,
                                                error_metrics_additional_info,
                                                error_metrics_nicknames):
        signal_error = metric(y_true=true_values, y_pred=predicted_values,
                              **metric_info)
        fold_error_info.setdefault(metric_name, signal_error)

    return fold_error_info


# TODO: move to python_utils (make more general)
def update_cv_validation_info(test_validation_info, iteration_validation_info):
    """
    Updates a dictionary with given values
    """

    test_validation_info = test_validation_info or {}

    for metric in iteration_validation_info:
        test_validation_info.setdefault(metric, []).append(iteration_validation_info[metric])

    return test_validation_info


def mp_update_cv_info(iteration_info, repetition_id, fold_idx, info=None):
    """
    Updates a dictionary with given values.
    """

    info = info or {}

    for metric in iteration_info:
        info.setdefault(metric, {}).setdefault(fold_idx, {}).setdefault(repetition_id, iteration_info[metric])

    return info


def average_validation_info(test_validation_info):
    """
    Computes the mean value of each metric value list.
    """

    for metric in test_validation_info:
        test_validation_info[metric] = np.mean(test_validation_info[metric])

    return test_validation_info


def get_validate_condition_value(validation_error, validate_on):
    """
    Returns the metric to validate on during hyper-parameters calibration.
    """

    return validation_error[validate_on]


class PrebuiltCV(_BaseKFold):
    """
    Simple CV wrapper for custom fold definition.
    """

    def __init__(self, cv_type='kfold', held_out_key='validation', **kwargs):
        super(PrebuiltCV, self).__init__(**kwargs)
        self.folds = None
        self.key_listing = None
        self.held_out_key = held_out_key

        if cv_type == 'kfold':
            self.cv = KFold(n_splits=self.n_splits, shuffle=self.shuffle)
        elif cv_type == 'stratifiedkfold':
            self.cv = StratifiedKFold(n_splits=self.n_splits, shuffle=self.shuffle)
        else:
            raise AttributeError('Invalid cv_type! Got: {}'.format(cv_type))

    def build_folds(self, X, y):
        self.folds = {}
        for fold, (train_indexes, held_out_indexes) in enumerate(self.cv.split(X, y)):
            self.folds['fold_{}'.format(fold)] = {
                'train': train_indexes,
                self.held_out_key: held_out_indexes
            }

    def build_all_sets_folds(self, X, y, validation_n_splits=None):
        assert self.held_out_key == 'test'

        validation_n_splits = self.n_splits if validation_n_splits is None else validation_n_splits

        self.folds = {}
        for fold, (train_indexes, held_out_indexes) in enumerate(self.cv.split(X, y)):
            sub_X = X[train_indexes]
            sub_y = y[train_indexes]

            self.cv.n_splits = validation_n_splits
            sub_train_indexes, sub_val_indexes = list(self.cv.split(sub_X, sub_y))[0]
            self.cv.n_splits = self.n_splits

            self.folds['fold_{}'.format(fold)] = {
                'train': train_indexes[sub_train_indexes],
                self.held_out_key: held_out_indexes,
                'validation': train_indexes[sub_val_indexes]
            }

    def load_dataset_list(self, load_path):
        with open(load_path, 'r') as f:
            dataset_list = [item.strip() for item in f.readlines()]

        return dataset_list

    def save_folds(self, save_path, tolist=False):

        if tolist:
            to_save = {}
            for fold_key in self.folds:
                for split_set in self.folds[fold_key]:
                    to_save.setdefault(fold_key, {}).setdefault(split_set, self.folds[fold_key][split_set].tolist())
            save_json(save_path, to_save)
        else:
            save_json(save_path, self.folds)

    def load_folds(self, load_path):
        self.folds = load_json(load_path)
        self.n_splits = len(self.folds)
        key_path = load_path.split('.json')[0] + '.txt'
        with open(key_path, 'r') as f:
            self.key_listing = list(map(lambda item: item.strip(), f.readlines()))
        self.key_listing = np.array(self.key_listing)

    def _iter_test_indices(self, X=None, y=None, groups=None):

        fold_list = sorted(list(self.folds.keys()))

        for fold in fold_list:
            yield self.folds[fold][self.held_out_key]

    def split(self, X, y=None, groups=None):

        fold_list = sorted(list(self.folds.keys()))

        for fold in fold_list:
            val_indexes = self.key_listing[self.folds[fold]['validation']] if 'validation' in self.folds[fold] else None
            test_indexes = self.key_listing[self.folds[fold]['test']] if 'test' in self.folds[fold] else None

            assert val_indexes is not None or test_indexes is not None

            yield self.key_listing[self.folds[fold]['train']], \
                  val_indexes, \
                  test_indexes
