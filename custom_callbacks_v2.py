"""

@Author: Federico Ruggeri

@Date: 08/01/19

"""

import os

import matplotlib.pyplot as plt
import numpy as np
from tensorflow.keras.callbacks import Callback
from tensorflow.python.eager import context
from tensorflow.python.framework import ops
from tensorflow.python.keras import backend as K
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import summary_ops_v2
from tensorflow.python.platform import tf_logging as logging
import tensorflow

from utility.draw_graph_utils import draw_tree
from utility.json_utils import save_json, load_json
from utility.log_utils import Logger
import const_define as cd
from datetime import datetime
import time


class TensorBoard(Callback):
    """TensorBoard basic visualizations.
    [TensorBoard](https://www.tensorflow.org/guide/summaries_and_tensorboard)
    is a visualization tool provided with TensorFlow.
    This callback writes a log for TensorBoard, which allows
    you to visualize dynamic graphs of your training and test
    metrics, as well as activation histograms for the different
    layers in your model.
    If you have installed TensorFlow with pip, you should be able
    to launch TensorBoard from the command line:
    ```sh
    tensorboard --logdir=/full_path_to_your_logs
    ```
    When using a backend other than TensorFlow, TensorBoard will still work
    (if you have TensorFlow installed), but the only feature available will
    be the display of the losses and metrics plots.
    # Arguments
        log_dir: the path of the directory where to save the log
            files to be parsed by TensorBoard.
        histogram_freq: frequency (in epochs) at which to compute activation
            and weight histograms for the layers of the model. If set to 0,
            histograms won't be computed. Validation data (or split) must be
            specified for histogram visualizations.
        batch_size: size of batch of inputs to feed to the network
            for histograms computation.
        write_graph: whether to visualize the graph in TensorBoard.
            The log file can become quite large when
            write_graph is set to True.
        write_grads: whether to visualize gradient histograms in TensorBoard.
            `histogram_freq` must be greater than 0.
        write_images: whether to write model weights to visualize as
            image in TensorBoard.
        embeddings_freq: frequency (in epochs) at which selected embedding
            layers will be saved. If set to 0, embeddings won't be computed.
            Data to be visualized in TensorBoard's Embedding tab must be passed
            as `embeddings_data`.
        embeddings_layer_names: a list of names of layers to keep eye on. If
            None or empty list all the embedding layer will be watched.
        embeddings_metadata: a dictionary which maps layer name to a file name
            in which metadata for this embedding layer is saved. See the
            [details](https://www.tensorflow.org/guide/embedding#metadata)
            about metadata files format. In case if the same metadata file is
            used for all embedding layers, string can be passed.
        embeddings_data: data to be embedded at layers specified in
            `embeddings_layer_names`. Numpy array (if the model has a single
            input) or list of Numpy arrays (if the model has multiple inputs).
            Learn [more about embeddings](
            https://www.tensorflow.org/guide/embedding).
        update_freq: `'batch'` or `'epoch'` or integer. When using `'batch'`, writes
            the losses and metrics to TensorBoard after each batch. The same
            applies for `'epoch'`. If using an integer, let's say `10000`,
            the callback will write the metrics and losses to TensorBoard every
            10000 samples. Note that writing too frequently to TensorBoard
            can slow down your training.
    """

    def __init__(self, log_dir='./logs',
                 histogram_freq=0,
                 batch_size=32,
                 write_graph=False,
                 write_images=False,
                 embeddings_freq=0,
                 profile_batch=2,
                 embeddings_metadata=None,
                 update_freq='epoch',
                 **kwargs):
        super(TensorBoard, self).__init__()
        self._validate_kwargs(kwargs)

        self.log_dir = log_dir
        self.histogram_freq = histogram_freq
        self.write_graph = write_graph
        self.write_images = write_images
        self.embeddings_freq = embeddings_freq
        self.embeddings_metadata = embeddings_metadata
        self.batch_size = batch_size
        if update_freq == 'batch':
            # It is the same as writing as frequently as possible.
            self.update_freq = 1
        else:
            self.update_freq = update_freq

        self._samples_seen = 0
        self._samples_seen_at_last_write = 0
        self._current_batch = 0
        self._total_batches_seen = 0
        self._total_val_batches_seen = 0

        self._writers = {}
        self.writer = summary_ops_v2.create_file_writer_v2(self.log_dir)

        self._profile_batch = profile_batch
        self._is_tracing = False
        self._chief_worke_only = True

    def _validate_kwargs(self, kwargs):
        """Handle arguments were supported in V1."""
        if kwargs.get('write_grads', False):
            logging.warning('`write_grads` will be ignored in TensorFlow 2.0 '
                            'for the `TensorBoard` Callback.')
        if kwargs.get('batch_size', False):
            logging.warning('`batch_size` is no longer needed in the '
                            '`TensorBoard` Callback and will be ignored '
                            'in TensorFlow 2.0.')
        if kwargs.get('embeddings_layer_names', False):
            logging.warning('`embeddings_layer_names` is not supported in '
                            'TensorFlow 2.0. Instead, all `Embedding` layers '
                            'will be visualized.')
        if kwargs.get('embeddings_data', False):
            logging.warning('`embeddings_data` is not supported in TensorFlow '
                            '2.0. Instead, all `Embedding` variables will be '
                            'visualized.')

        unrecognized_kwargs = set(kwargs.keys()) - {
            'write_grads', 'embeddings_layer_names', 'embeddings_data', 'batch_size'
        }

        # Only allow kwargs that were supported in V1.
        if unrecognized_kwargs:
            raise ValueError('Unrecognized arguments in `TensorBoard` '
                             'Callback: ' + str(unrecognized_kwargs))

    def set_model(self, model):
        """Sets Keras model and writes graph if specified."""
        self.model = model
        with context.eager_mode():
            # self._close_writers()
            if self.write_graph:
                with self.writer.as_default():
                    with summary_ops_v2.always_record_summaries():
                        if not model.run_eagerly:
                            summary_ops_v2.graph(K.get_graph(), step=0)

                        summary_writable = (
                                self.model.model._is_graph_network or  # pylint: disable=protected-access
                                self.model.model.__class__.__name__ == 'Sequential')  # pylint: disable=protected-access
                        if summary_writable:
                            summary_ops_v2.keras_model('keras', self.model.model, step=0)

        if self.embeddings_freq:
            self._configure_embeddings()

    def _configure_embeddings(self):
        """Configure the Projector for embeddings."""
        from tensorflow.python.keras.layers import embeddings
        try:
            from tensorboard.plugins import projector
        except ImportError:
            raise ImportError('Failed to import TensorBoard. Please make sure that '
                              'TensorBoard integration is complete."')
        config = projector.ProjectorConfig()
        for layer in self.model.model.layers:
            if isinstance(layer, embeddings.Embedding):
                embedding = config.embeddings.add()
                embedding.tensor_name = layer.embeddings.name

                if self.embeddings_metadata is not None:
                    if isinstance(self.embeddings_metadata, str):
                        embedding.metadata_path = self.embeddings_metadata
                    else:
                        if layer.name in embedding.metadata_path:
                            embedding.metadata_path = self.embeddings_metadata.pop(layer.name)

        if self.embeddings_metadata:
            raise ValueError('Unrecognized `Embedding` layer names passed to '
                             '`keras.callbacks.TensorBoard` `embeddings_metadata` '
                             'argument: ' + str(self.embeddings_metadata.keys()))

        class DummyWriter(object):
            """Dummy writer to conform to `Projector` API."""

            def __init__(self, logdir):
                self.logdir = logdir

            def get_logdir(self):
                return self.logdir

        writer = DummyWriter(self.log_dir)
        projector.visualize_embeddings(writer, config)

    def _close_writers(self):
        """Close all remaining open file writers owned by this callback.
        If there are no such file writers, this is a no-op.
        """
        with context.eager_mode():
            self.writer.close()

    def on_train_begin(self, logs=None):
        if self._profile_batch == 1:
            summary_ops_v2.trace_on(graph=True, profiler=True)
            self._is_tracing = True

    def on_batch_end(self, batch, logs=None):
        """Writes scalar summaries for metrics on every training batch.
        Performs profiling if current batch is in profiler_batches.
        Arguments:
          batch: Integer, index of batch within the current epoch.
          logs: Dict. Metric results for this batch.
        """
        # Don't output batch_size and batch number as TensorBoard summaries
        logs = logs or {}
        self._samples_seen += logs.get('size', 1)
        samples_seen_since = self._samples_seen - self._samples_seen_at_last_write
        if self.update_freq != 'epoch' and samples_seen_since >= self.update_freq:
            self._log_metrics(logs, prefix='batch_', step=self._total_batches_seen)
            self._samples_seen_at_last_write = self._samples_seen
        self._total_batches_seen += 1
        if self._is_tracing:
            self._log_trace()
        elif (not self._is_tracing and
              self._total_batches_seen == self._profile_batch - 1):
            self._enable_trace()

    def on_epoch_end(self, epoch, logs=None):
        """Runs metrics and histogram summaries at epoch end."""
        step = epoch if self.update_freq == 'epoch' else self._samples_seen
        # TODO: add logs control method
        self._log_metrics(logs, prefix='', step=step)

        if self.histogram_freq and epoch % self.histogram_freq == 0:
            self._log_weights(epoch)

        if self.embeddings_freq and epoch % self.embeddings_freq == 0:
            self._log_embeddings(epoch)

    def on_train_end(self, logs=None):
        if self._is_tracing:
            self._log_trace()
        self._close_writers()

    def _enable_trace(self):
        if context.executing_eagerly():
            summary_ops_v2.trace_on(graph=True, profiler=True)
            self._is_tracing = True

    def _log_trace(self):
        if context.executing_eagerly():
            with self.writer.as_default(), \
                 summary_ops_v2.always_record_summaries():
                summary_ops_v2.trace_export(
                    name='batch_%d' % self._total_batches_seen,
                    step=self._total_batches_seen,
                    profiler_outdir=os.path.join(self.log_dir, 'train'))

            self._is_tracing = False

    def _log_metrics(self, logs, prefix, step):
        """Writes metrics out as custom scalar summaries.
        Arguments:
            logs: Dict. Keys are scalar summary names, values are NumPy scalars.
            prefix: String. The prefix to apply to the scalar summary names.
            step: Int. The global step to use for TensorBoard.
        """
        if logs is None:
            logs = {}

        # Group metrics by the name of their associated file writer. Values
        # are lists of metrics, as (name, scalar_value) pairs.
        validation_prefix = 'val_'
        logs_by_writer = []
        for (name, value) in logs.items():
            if name in ('batch', 'size', 'num_steps'):
                # Scrub non-metric items.
                continue
            name = prefix + name  # assign batch or epoch prefix
            logs_by_writer.append((name, value))

        with context.eager_mode():
            with summary_ops_v2.always_record_summaries():
                if not logs_by_writer:
                    # Don't create a "validation" events file if we don't
                    # actually have any validation data.
                    pass
                with self.writer.as_default():
                    for (name, value) in logs_by_writer:
                        summary_ops_v2.scalar(name, value, step=step)

    def _log_weights(self, epoch):
        """Logs the weights of the Model to TensorBoard."""
        with context.eager_mode(), \
             self.writer.as_default(), \
             summary_ops_v2.always_record_summaries():
            for layer in self.model.model.layers:
                for weight in layer.weights:
                    weight_name = weight.name.replace(':', '_')
                    with ops.init_scope():
                        weight = K.get_value(weight)
                    summary_ops_v2.histogram(weight_name, weight, step=epoch)
                    if self.write_images:
                        self._log_weight_as_image(weight, weight_name, epoch)
            self.writer.flush()

    def _log_weight_as_image(self, weight, weight_name, epoch):
        """Logs a weight as a TensorBoard image."""
        w_img = array_ops.squeeze(weight)
        shape = K.int_shape(w_img)
        if len(shape) == 1:  # Bias case
            w_img = array_ops.reshape(w_img, [1, shape[0], 1, 1])
        elif len(shape) == 2:  # Dense layer kernel case
            if shape[0] > shape[1]:
                w_img = array_ops.transpose(w_img)
                shape = K.int_shape(w_img)
            w_img = array_ops.reshape(w_img, [1, shape[0], shape[1], 1])
        elif len(shape) == 3:  # ConvNet case
            if K.image_data_format() == 'channels_last':
                # Switch to channels_first to display every kernel as a separate
                # image.
                w_img = array_ops.transpose(w_img, perm=[2, 0, 1])
                shape = K.int_shape(w_img)
            w_img = array_ops.reshape(w_img, [shape[0], shape[1], shape[2], 1])

        shape = K.int_shape(w_img)
        # Not possible to handle 3D convnets etc.
        if len(shape) == 4 and shape[-1] in [1, 3, 4]:
            summary_ops_v2.image(weight_name, w_img, step=epoch)

    def _log_embeddings(self, epoch):
        embeddings_ckpt = os.path.join(self.log_dir, 'train',
                                       'keras_embedding.ckpt-{}'.format(epoch))
        self.model.model.save_weights(embeddings_ckpt)


class GeneratorEarlyStopping(Callback):
    """Stop training when a monitored quantity has stopped improving.
    Arguments:
        monitor: Quantity to be monitored.
        min_delta: Minimum change in the monitored quantity
            to qualify as an improvement, i.e. an absolute
            change of less than min_delta, will count as no
            improvement.
        patience: Number of epochs with no improvement
            after which training will be stopped.
        verbose: verbosity mode.
        mode: One of `{"auto", "min", "max"}`. In `min` mode,
            training will stop when the quantity
            monitored has stopped decreasing; in `max`
            mode it will stop when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        baseline: Baseline value for the monitored quantity.
            Training will stop if the model doesn't show improvement over the
            baseline.
        restore_best_weights: Whether to restore model weights from
            the epoch with the best value of the monitored quantity.
            If False, the model weights obtained at the last step of
            training are used.
    Example:
    ```python
    callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
    # This callback will stop the training when there is no improvement in
    # the validation loss for three consecutive epochs.
    model.fit(data, labels, epochs=100, callbacks=[callback],
        validation_data=(val_data, val_labels))
    ```
    """

    def __init__(self,
                 monitor='val_loss',
                 min_delta=0,
                 patience=0,
                 verbose=0,
                 mode='auto',
                 baseline=None,
                 restore_best_weights=False):
        super(GeneratorEarlyStopping, self).__init__()

        self.monitor = monitor
        self.patience = patience
        self.verbose = verbose
        self.baseline = baseline
        self.min_delta = abs(min_delta)
        self.wait = 0
        self.step_checkpoint = 0
        self.restore_best_weights = restore_best_weights
        self.best_weights = None

        if mode not in ['auto', 'min', 'max']:
            logging.warning('EarlyStopping mode %s is unknown, '
                            'fallback to auto mode.', mode)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
        elif mode == 'max':
            self.monitor_op = np.greater
        else:
            if 'acc' in self.monitor:
                self.monitor_op = np.greater
            else:
                self.monitor_op = np.less

        if self.monitor_op == np.greater:
            self.min_delta *= 1
        else:
            self.min_delta *= -1

    def on_train_begin(self, logs=None):
        # Allow instances to be re-used
        self.wait = 0
        self.step_checkpoint = 0
        if self.baseline is not None:
            self.best = self.baseline
        else:
            self.best = np.Inf if self.monitor_op == np.less else -np.Inf

    def on_step_checkpoint(self, step_checkpoint, logs=None):
        current = self.get_monitor_value(logs)
        if current is None:
            return

        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.wait = 0
            if self.restore_best_weights:
                self.best_weights = self.model.get_weights()
                self.best_knowledge = self.model.knowledge
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.model.stop_training = True
                self.step_checkpoint = step_checkpoint
                if self.restore_best_weights:
                    if self.verbose > 0:
                        print('Restoring model weights from the end of '
                              'the best checkpoint')
                    self.model.set_weights(self.best_weights)
                    self.model.knowledge = self.best_knowledge

    def on_train_end(self, logs=None):
        if self.step_checkpoint > 0 and self.verbose > 0:
            print('Epoch %05d: early stopping' % (self.step_checkpoint + 1))

    def get_monitor_value(self, logs):
        logs = logs or {}
        monitor_value = logs.get(self.monitor)
        if monitor_value is None:
            logging.warning('Early stopping conditioned on metric `%s` '
                            'which is not available. Available metrics are: %s',
                            self.monitor, ','.join(list(logs.keys())))
        return monitor_value


class EarlyStopping(Callback):
    """Stop training when a monitored quantity has stopped improving.
    Arguments:
        monitor: Quantity to be monitored.
        min_delta: Minimum change in the monitored quantity
            to qualify as an improvement, i.e. an absolute
            change of less than min_delta, will count as no
            improvement.
        patience: Number of epochs with no improvement
            after which training will be stopped.
        verbose: verbosity mode.
        mode: One of `{"auto", "min", "max"}`. In `min` mode,
            training will stop when the quantity
            monitored has stopped decreasing; in `max`
            mode it will stop when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        baseline: Baseline value for the monitored quantity.
            Training will stop if the model doesn't show improvement over the
            baseline.
        restore_best_weights: Whether to restore model weights from
            the epoch with the best value of the monitored quantity.
            If False, the model weights obtained at the last step of
            training are used.
    Example:
    ```python
    callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
    # This callback will stop the training when there is no improvement in
    # the validation loss for three consecutive epochs.
    model.fit(data, labels, epochs=100, callbacks=[callback],
        validation_data=(val_data, val_labels))
    ```
    """

    def __init__(self,
                 monitor='val_loss',
                 min_delta=0,
                 patience=0,
                 verbose=0,
                 mode='auto',
                 baseline=None,
                 restore_best_weights=False,
                 save_base_path=None,
                 save_weights_to_file=True):
        super(EarlyStopping, self).__init__()

        self.monitor = monitor
        self.patience = patience
        self.verbose = verbose
        self.baseline = baseline
        self.min_delta = abs(min_delta)
        self.wait = 0
        self.best_epoch = 0
        self.stopped_epoch = 0
        self.restore_best_weights = restore_best_weights
        self.best_weights = None
        self.save_base_path = save_base_path
        self.save_weights_to_file = save_weights_to_file
        self.file_checkpoint = None

        if mode not in ['auto', 'min', 'max']:
            logging.warning('EarlyStopping mode %s is unknown, '
                            'fallback to auto mode.', mode)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
        elif mode == 'max':
            self.monitor_op = np.greater
        else:
            if 'acc' in self.monitor:
                self.monitor_op = np.greater
            else:
                self.monitor_op = np.less

        if self.monitor_op == np.greater:
            self.min_delta *= 1
        else:
            self.min_delta *= -1

        # Setup save path for temporary storage of best weights
        if self.restore_best_weights:
            if save_base_path is not None and os.path.isdir(save_base_path):
                self.save_base_path = save_base_path
            else:
                self.save_base_path = os.path.join(cd.PROJECT_DIR, 'tmp')
                if not os.path.isdir(self.save_base_path):
                    os.makedirs(self.save_base_path)

    def on_train_begin(self, logs=None):
        # Allow instances to be re-used
        self.wait = 0
        self.stopped_epoch = 0
        if self.baseline is not None:
            self.best = self.baseline
        else:
            self.best = np.Inf if self.monitor_op == np.less else -np.Inf

        # Clear last checkpoint file if still existing
        if self.file_checkpoint is not None and os.path.isfile(self.file_checkpoint):
            os.remove(self.file_checkpoint)

        self.best_weights = None

    def _save_weights_to_file(self, weights, epoch):

        # Remove previous checkpoint (if any)
        if self.file_checkpoint is not None and os.path.isfile(self.file_checkpoint):
            os.remove(self.file_checkpoint)

        # Define new checkpoint
        self.file_checkpoint = os.path.join(self.save_base_path, "es_{0}_{1}.npy".format(epoch + 1,
                                                                                         datetime.today().strftime(
                                                                                             '%d-%m-%Y-%H-%M-%S-%f')[
                                                                                         :-4]))
        np.save(self.file_checkpoint, weights)

    def _load_weights_from_file(self):
        return np.load(self.file_checkpoint, allow_pickle=True)

    def on_epoch_end(self, epoch, logs=None):
        current = self.get_monitor_value(logs)
        strategy = logs['strategy'] if 'strategy' in logs else None
        if current is None:
            return
        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.best_epoch = epoch
            self.wait = 0
            if self.restore_best_weights:
                if self.save_weights_to_file:
                    self._save_weights_to_file(self.model.get_weights(strategy), epoch)
                else:
                    self.best_weights = self.model.get_weights(strategy)
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.model.stop_training = True
                if self.restore_best_weights:
                    if self.verbose > 0:
                        Logger.get_logger(__name__).info(
                            'Restoring model weights from the end of the best epoch ({}).'.format(self.best_epoch + 1))

                    if self.save_weights_to_file:
                        self.model.set_weights(self._load_weights_from_file(), strategy)
                    else:
                        self.model.set_weights(self.best_weights, strategy)

    def on_train_end(self, logs=None):
        if self.stopped_epoch > 0 and self.verbose > 0:
            Logger.get_logger(__name__).info('Epoch %05d: early stopping' % (self.stopped_epoch + 1))

        # Clear memory
        self.best_weights = None

        # Remove previous checkpoint (if any)
        if self.file_checkpoint is not None and os.path.isfile(self.file_checkpoint):
            os.remove(self.file_checkpoint)

    def get_monitor_value(self, logs):
        logs = logs or {}
        monitor_value = logs.get(self.monitor)
        if monitor_value is None:
            logging.warning('Early stopping conditioned on metric `%s` '
                            'which is not available. Available metrics are: %s',
                            self.monitor, ','.join(list(logs.keys())))
        return monitor_value


class EarlyPerformanceStopping(Callback):

    def __init__(self, monitor, threshold, patience=10, operator='min'):
        self.monitor = monitor
        self.threshold = threshold
        self.patience = patience
        self.best_achieved = 1e9 if operator.lower() == 'min' else -1e9
        self.operator = np.less if operator.lower() == 'min' else np.greater

    def get_monitor_value(self, logs):
        logs = logs or {}
        monitor_value = logs.get(self.monitor)
        if monitor_value is None:
            logging.warning('Early Performance stopping conditioned on metric `%s` '
                            'which is not available. Available metrics are: %s',
                            self.monitor, ','.join(list(logs.keys())))
        return monitor_value

    def on_epoch_end(self, epoch, logs=None):
        current = self.get_monitor_value(logs)
        if current is None:
            return

        if self.operator(current, self.best_achieved):
            self.best_achieved = current

        if epoch == self.patience - 1 and self.operator(self.threshold, self.best_achieved):
            Logger.get_logger(__name__).info("Prematurely stopping configuration due to low performance"
                        " (requested: {0} - achieved: {1})...".format(self.threshold, self.best_achieved))
            self.model.model.stop_training = True


class TrainingLogger(Callback):

    def __init__(self, filepath, save_model=False, **kwargs):
        super(TrainingLogger, self).__init__(**kwargs)
        self.filepath = filepath
        self.info = {}

        if save_model and not os.path.isdir(self.filepath):
            os.makedirs(self.filepath)

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_epoch_end(self, epoch, logs=None):
        if logs is not None:
            assert type(logs) == dict

            for key, item in logs.items():
                if key.startswith('val') or key.startswith('train') or 'score' in key:
                    self.info.setdefault(key, []).append(item)

    def on_train_end(self, logs=None):
        filename = '{}_info.npy'.format(self.network.name)
        savepath = os.path.join(self.filepath, filename)

        np.save(savepath, self.info)

        # Clear
        self.info = {}


class PredictionRetriever(Callback):
    """
    Simple callback that allows to extract and save attention tensors during prediction phase.
    Extraction is simply implemented as attribute inspection.
    """

    def __init__(self, save_path, save_suffix=None):
        super(Callback, self).__init__()
        self.start_monitoring = False
        self.stored_network_predictions = None
        self.save_path = save_path
        self.save_suffix = save_suffix

    def set_model(self, model):
        self.model = model

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_prediction_begin(self, logs=None):
        self.start_monitoring = True

    def on_batch_prediction_end(self, batch, logs=None):

        if self.start_monitoring:

            # [batch_size, hops, mem_size]
            model_additional_info = logs['model_additional_info']

            if type(model_additional_info) == dict:
                network_predictions = model_additional_info['answer'].numpy()
            else:
                network_predictions = model_additional_info.numpy()

            if batch == 0:
                # [batch_size, hops, mem_size]
                self.stored_network_predictions = network_predictions
            else:
                # [samples, hops, mem_size]
                self.stored_network_predictions = np.append(self.stored_network_predictions, network_predictions,
                                                            axis=0)

    def on_prediction_end(self, logs=None):

        if self.start_monitoring:
            # Saving
            save_name = self.network.name
            if self.save_suffix is not None:
                save_name += '_{}'.format(self.save_suffix)
            filepath = os.path.join(self.save_path,
                                    '{}_raw_predictions.npy'.format(save_name))
            np.save(filepath, self.stored_network_predictions)

            # Resetting
            self.start_monitoring = None
            self.stored_network_predictions = {}


class AttentionRetriever(Callback):
    """
    Simple callback that allows to extract and save attention tensors during prediction phase.
    Extraction is simply implemented as attribute inspection.
    """

    def __init__(self, save_path, save_suffix=None):
        super(Callback, self).__init__()
        self.start_monitoring = False
        self.stored_attention_weights = None
        self.save_path = save_path
        self.save_suffix = save_suffix
        self.is_training = False
        self.current_epoch = None

    def save_attention(self):

        if self.start_monitoring:
            # Saving
            save_name = self.network.name
            if self.save_suffix:
                save_name += '_{}'.format(self.save_suffix)
            if self.is_training:
                save_name += '_train_{}'.format(self.current_epoch)

            save_name += '_attention_weights.npy'

            filepath = os.path.join(self.save_path,
                                    save_name)
            np.save(filepath, self.stored_attention_weights)

            # Resetting
            self.start_monitoring = None
            self.stored_attention_weights = {}

    def set_model(self, model):
        self.model = model

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_train_begin(self, logs=None):
        self.is_training = True
        self.current_epoch = 0

    def on_epoch_end(self, epoch, logs=None):
        self.current_epoch = epoch

    def on_train_end(self, logs=None):
        self.is_training = False

        # Merge all partial results
        partial_train_files = [item for item in os.listdir(self.save_path) if
                               'train' in item and 'attention_weights' in item]
        merged = np.concatenate([load_json(item)[:, np.newaxis, :] for item in partial_train_files], axis=1)
        merged = merged.squeeze()
        save_name = self.network.name

        if self.save_suffix:
            save_name += '_{}'.format(self.save_suffix)

        save_name += '_train_attention_weights.json'

        save_json(os.path.join(self.save_path, save_name), merged)

        for item in partial_train_files:
            os.remove(os.path.join(self.save_path, item))

    def on_prediction_begin(self, logs=None):
        self.start_monitoring = True

    def on_batch_prediction_end(self, batch, logs=None):
        if self.start_monitoring:
            model_additional_info = logs['model_additional_info']

            if type(model_additional_info) == dict:
                attention_weights = model_additional_info['gate'].numpy()
            else:
                attention_weights = model_additional_info.numpy()

            if batch == 0:
                # [batch_size, hops, mem_size]
                self.stored_attention_weights = attention_weights
            else:
                # [samples, hops, mem_size]
                self.stored_attention_weights = np.append(self.stored_attention_weights, attention_weights, axis=0)

    def on_prediction_end(self, logs=None):
        self.save_attention()


class TimeLogger(Callback):
    """
    Simple callback that allows to extract and save attention tensors during prediction phase.
    Extraction is simply implemented as attribute inspection.
    """

    def __init__(self, save_path, save_suffix=None):
        super(Callback, self).__init__()
        self.start_monitoring = False
        self.stored_batch_training_times = []
        self.stored_epoch_training_times = []
        self.stored_inference_times = []
        self.save_path = save_path
        self.save_suffix = save_suffix
        self.is_training = False
        self.start_time = None
        self.end_time = None

    def set_model(self, model):
        self.model = model

    def _save_data(self, data, data_suffix):
        save_name = self.network.name
        if self.save_suffix:
            save_name += '_{}'.format(self.save_suffix)

        save_name += '_' + data_suffix

        save_name += '_times.npy'

        filepath = os.path.join(self.save_path, save_name)
        np.save(filepath, data)

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_train_begin(self, logs=None):
        self.is_training = True

    def on_batch_begin(self, batch, logs=None):
        self.start_time = time.time()

    def on_batch_end(self, batch, logs=None):
        self.end_time = time.time()
        self.stored_batch_training_times.append(self.end_time - self.start_time)

    def on_epoch_end(self, epoch, logs=None):
        self.current_epoch = epoch
        self.stored_epoch_training_times.append(np.mean(self.stored_batch_training_times))
        self.stored_batch_training_times = []

    def on_train_end(self, logs=None):
        self.is_training = False

        self._save_data(data=self.stored_epoch_training_times,
                        data_suffix='train')

        self.stored_epoch_training_times =  []

    def on_batch_prediction_begin(self, batch, logs=None):
        self.start_time = time.time()

    def on_batch_prediction_end(self, batch, logs=None):
        self.end_time = time.time()
        self.stored_inference_times.append(self.end_time - self.start_time)

    def on_prediction_end(self, logs=None):
        self._save_data(data=self.stored_inference_times,
                        data_suffix=logs['suffix'])

        self.stored_inference_times = []


class TreeConstraintCalculator(Callback):
    """
    Computes tree constraints on the fly and stores them
    Current version: contiguous sequence constraint only
    """

    def __init__(self, save_path, save_suffix=None):
        super(Callback, self).__init__()
        self.start_monitoring = False

        self.pooling_matrix_values = None
        self.adjacency_matrix_values = None
        self.pad_mask_values = None

        self.contiguous_constraint_values = None
        self.minimal_size_constraint_values = None
        self.kernel_constraint_values = None
        self.tree_intensity_constraint_values = None

        self.save_path = save_path
        self.save_suffix = save_suffix
        self.prediction_suffix = None

    def set_model(self, model):
        self.model = model

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_prediction_begin(self, logs=None):
        self.start_monitoring = True
        self.prediction_suffix = logs['suffix']

    def on_batch_prediction_end(self, batch, logs=None):
        if self.start_monitoring:
            model_additional_info = logs['model_additional_info']

            # [batch_size, max_nodes, meta_nodes]
            pooling_matrix = model_additional_info['pooling_matrix_0'].numpy()

            # [batch_size, max_nodes, max_nodes]
            adjacency_matrix = model_additional_info['adjacency_matrix_0'].numpy()
            adjacency_matrix = adjacency_matrix.reshape(-1, pooling_matrix.shape[1], pooling_matrix.shape[1])

            # [batch_size, max_nodes]
            pad_mask = model_additional_info['node_mask_0'].numpy()

            # 1) Contiguous sequence constraint
            # [batch_size,]
            if 'cluster_sequence_reg_0' in model_additional_info:
                contiguous_constraint = np.mean(model_additional_info['cluster_sequence_reg_0'],
                                                axis=-1)
                # [samples, meta_nodes]
                if batch == 0:
                    self.contiguous_constraint_values = [contiguous_constraint]
                else:
                    self.contiguous_constraint_values.append(contiguous_constraint)

            # 2) Minimal size constraint
            # [batch_size, ]
            if 'cluster_mean_size_reg_0' in model_additional_info:
                minimal_size_constraint = np.mean(model_additional_info['cluster_mean_size_reg_0'], axis=-1)
                if batch == 0:
                    self.minimal_size_constraint_values = [minimal_size_constraint]
                else:
                    self.minimal_size_constraint_values.append(minimal_size_constraint)

            # 3) Kernel specific constraint
            if 'cluster_kernel_reg_0' in model_additional_info:
                kernel_constraint = np.mean(model_additional_info['cluster_kernel_reg_0'], axis=-1)
                if batch == 0:
                    self.kernel_constraint_values = [kernel_constraint]
                else:
                    self.kernel_constraint_values.append(kernel_constraint)

            # 4) Tree intensity constraint
            if 'cluster_tree_intensity_reg_0' in model_additional_info:
                tree_intensity_constraint = np.mean(model_additional_info['cluster_tree_intensity_reg_0'], axis=-1)
                if batch == 0:
                    self.tree_intensity_constraint_values = [tree_intensity_constraint]
                else:
                    self.tree_intensity_constraint_values.append(tree_intensity_constraint)

            if batch == 0:
                # [batch_size, max_nodes, meta_nodes]
                self.pooling_matrix_values = [pooling_matrix]

                # [batch_size, max_nodes, max_nodes]
                self.adjacency_matrix_values = [adjacency_matrix]

                # [batch_size, max_nodes]
                self.pad_mask_values = [pad_mask]
            else:
                # [samples, max_nodes, meta_nodes]
                self.pooling_matrix_values.append(pooling_matrix)

                # [samples, max_nodes, max_nodes]
                self.adjacency_matrix_values.append(adjacency_matrix)

                # [samples, max_nodes]
                self.pad_mask_values.append(pad_mask)

    def on_prediction_end(self, logs=None):
        if self.start_monitoring:

            # Saving tree rules
            save_name = self.network.name
            if self.save_suffix:
                save_name += '_{}'.format(self.save_suffix)

            save_name += '_{}'.format(self.prediction_suffix)

            if self.contiguous_constraint_values is not None:
                tree_save_name = save_name + '_constraints.npy'

                filepath = os.path.join(self.save_path,
                                        tree_save_name)

                # [samples, # constraints]
                constraints = np.hstack((
                    np.concatenate(self.contiguous_constraint_values, axis=0)[:, np.newaxis],
                    np.concatenate(self.kernel_constraint_values, axis=0)[:, np.newaxis],
                    np.concatenate(self.minimal_size_constraint_values, axis=0)[:, np.newaxis],
                    np.concatenate(self.tree_intensity_constraint_values, axis=0)[:, np.newaxis]
                ))

                np.save(filepath, constraints)

            # Saving pooling matrix
            pooling_save_name = save_name + '_pooling_matrices.npy'
            np.save(os.path.join(self.save_path, pooling_save_name), np.concatenate(self.pooling_matrix_values, axis=0))

            # Saving adjacency matrix
            adjacency_save_name = save_name + '_adjacency_matrices.npy'
            np.save(os.path.join(self.save_path, adjacency_save_name), np.concatenate(self.adjacency_matrix_values, axis=0))

            # Saving pad mask
            pad_mask_save_name = save_name + '_pad_masks.npy'
            np.save(os.path.join(self.save_path, pad_mask_save_name), np.concatenate(self.pad_mask_values, axis=0))

            # Resetting
            self.start_monitoring = None
            self.has_tree_pooling = True

            self.pooling_matrix_values = None
            self.adjacency_matrix_values = None

            self.contiguous_constraint_values = None
            self.minimal_size_constraint_values = None
            self.kernel_constraint_values = None


class TreeDebugger(Callback):

    def __init__(self, save_path, save_suffix=None, full_inference=False, **kwargs):
        super(TreeDebugger, self).__init__(**kwargs)
        self.is_training = False
        self.current_epoch = 0

        self.save_path = save_path
        self.save_suffix = save_suffix

        self.full_inference = full_inference

    def set_model(self, model):
        self.model = model

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_train_begin(self, logs=None):
        self.is_training = True

    def on_train_end(self, logs=None):
        self.is_training = False
        self.current_epoch = 0

    def on_epoch_begin(self, epoch, logs=None):
        self.current_epoch = epoch

    def _save_tree(self, batch, node_intensities, adjacency_matrix, pad_mask, tree_type, logs=None):

        # Tree
        save_name = self.network.name
        if self.save_suffix:
            save_name += '_{}'.format(self.save_suffix)
        save_name += '_{}'.format(tree_type)

        dir_path = os.path.join(self.save_path, save_name)

        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)

        for k in range(node_intensities.shape[-1]):
            if self.is_training and batch == 0:
                title_name = "Epoch: {0} - Batch: {1} - Meta-Node: {2}".format(self.current_epoch, batch, k + 1)
                fig = draw_tree(nodes=node_intensities[0, :, k], adjacency_matrix=adjacency_matrix[0],
                                pad_mask=pad_mask[0],
                                title_name=title_name)
                save_path = os.path.join(dir_path, title_name + '.png')
                plt.savefig(save_path)
                if fig is not None:
                    plt.close(fig)
            else:
                title_name = "Set: {0} - Sample: {1} - Meta-Node: {2}".format(logs['suffix'], 1, k + 1)
                fig = draw_tree(nodes=node_intensities[0, :, k],
                                adjacency_matrix=adjacency_matrix[0],
                                pad_mask=pad_mask[0],
                                title_name=title_name)
                save_path = os.path.join(dir_path, title_name + '.png')
                plt.savefig(save_path)
                if fig is not None:
                    plt.close(fig)

    def on_batch_prediction_end(self, batch, logs=None):
        if (self.is_training and batch == 0) or self.full_inference:
            model_additional_info = logs['model_additional_info']

            if 'pooling_matrix' not in model_additional_info:
                return

            # [batch_size, max_nodes, meta_nodes]
            pooling_matrix = model_additional_info['pooling_matrix'].numpy()

            # [batch_size, max_nodes, max_nodes]
            adjacency_matrix = model_additional_info['adjacency_matrix'].numpy()
            adjacency_matrix = adjacency_matrix.reshape(-1, pooling_matrix.shape[1], pooling_matrix.shape[1])

            # [batch_size, max_nodes, 1]
            pad_mask = model_additional_info['pad_mask'].numpy()
            pad_mask = np.squeeze(pad_mask, axis=-1)

            self._save_tree(batch=batch, node_intensities=pooling_matrix, adjacency_matrix=adjacency_matrix,
                            pad_mask=pad_mask, tree_type='node_pooling', logs=logs)

            # batch_size, meta_nodes, max_nodes]
            if 'root_pooling' in model_additional_info:
                root_pooling = model_additional_info['root_pooling'].numpy()
                root_pooling = root_pooling.transpose(0, 2, 1)

                self._save_tree(batch=batch, node_intensities=root_pooling, adjacency_matrix=adjacency_matrix,
                                pad_mask=pad_mask, tree_type='root_pooling', logs=logs)


class TreeRegularizationExponentialDecay(Callback):

    def __init__(self, variable_names, decay_speed=0.01):
        self.variable_names = variable_names

        for name in variable_names:
            setattr(self, '{}_min_value'.format(name), 0.)
            setattr(self, '{}_max_value'.format(name), None)

        self.decay_speed = decay_speed

    def on_train_begin(self, logs=None):
        if hasattr(self.model, 'max_{}'.format(self.variable_names[0])):
            for name in self.variable_names:
                setattr(self, '{}_max_value'.format(name), getattr(self.model, 'max_{}'.format(name)))
                setattr(self.model, name, 0.)

    def _decay(self, max_value, min_value, decay_speed, time):
        return min_value + (max_value - min_value) * np.exp(-decay_speed * (time + 1))

    def on_epoch_end(self, epoch, logs=None):
        if hasattr(self.model, 'max_{}'.format(self.variable_names[0])):
            logging_info = {}
            for name in self.variable_names:
                current_max_value = getattr(self, '{}_max_value'.format(name))
                current_min_value = getattr(self, '{}_min_value'.format(name))
                decay = self._decay(max_value=current_max_value, min_value=current_min_value,
                                    decay_speed=self.decay_speed, time=epoch + 1)
                setattr(self.model, name, 1. - decay)
                logging_info[name] = 1. - decay

            Logger.get_logger(__name__).info('[TreeDecay] Setting regularization coefficients to: {}'.format(logging_info))


class TreeLangrangianEnabler(Callback):

    def __init__(self, variable_names, patience=10):
        super(TreeLangrangianEnabler, self).__init__()
        self.variable_names = variable_names
        self.patience = patience

    def on_train_begin(self, logs=None):
        if hasattr(self.model, 'coefficients_learning') \
                and getattr(self.model, 'coefficients_learning') == 'lagrangian':
            for name in self.variable_names:
                setattr(self.model, name, 0.)

    def on_epoch_end(self, epoch, logs=None):
        if hasattr(self.model, 'coefficients_learning') \
                and getattr(self.model, 'coefficients_learning') == 'lagrangian':
            if epoch == self.patience:
                for name in self.variable_names:
                    setattr(self.model, name, 1.)

                    Logger.get_logger(__name__).info('[TreeLagrangianEnabler] Enabling dual optimization!'
                                ' Lambda gradient coefficient set to: {}'.format(getattr(self.model, name)))

            Logger.get_logger(__name__).info(
                '[TreeLagrangianEnabler] Multipliers value: {}'.format(getattr(self.model.model, 'multipliers')))


class TreeConsistencyModifier(Callback):

    def __init__(self, variable_name, min_value=0.0, decay_speed=0.01):
        self.variable_name = variable_name
        self.min_value = min_value
        self.decay_speed = decay_speed

    def on_train_begin(self, logs=None):
        if hasattr(self.model, self.variable_name):
            self.max_value = getattr(self.model, self.variable_name)

    def on_epoch_end(self, epoch, logs=None):
        if hasattr(self.model, self.variable_name):
            decay = self.min_value + (self.max_value - self.min_value) * np.exp(-self.decay_speed * (epoch + 1))
            current_value = getattr(self.model, self.variable_name)
            setattr(self.model, self.variable_name, current_value * decay)
            upd_value = getattr(self.model, self.variable_name)
            Logger.get_logger(__name__).info('[TreeConsistencyModifier] Setting {0} to: {1}'.format(self.variable_name, upd_value))


class RepresentationRetriever(Callback):
    """
    Simple callback that allows to extract and save attention tensors during prediction phase.
    Extraction is simply implemented as attribute inspection.
    """

    def __init__(self, save_path, save_suffix=None):
        super(Callback, self).__init__()
        self.start_monitoring = False
        self.stored_representations = None
        self.save_path = save_path
        self.save_suffix = save_suffix

    def set_model(self, model):
        self.model = model

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']

    def on_prediction_begin(self, logs=None):
        self.start_monitoring = True

    def on_batch_prediction_end(self, batch, logs=None):

        if self.start_monitoring:

            # [batch_size, hops, mem_size]
            model_additional_info = logs['model_additional_info']

            if type(model_additional_info) == dict:
                representations = model_additional_info['representation'].numpy()
            else:
                representations = model_additional_info.numpy()

            if batch == 0:
                # [batch_size, hops, mem_size]
                self.stored_representations = representations
            else:
                # [samples, hops, mem_size]
                self.stored_representations = np.append(self.stored_representations, representations, axis=0)

    def on_prediction_end(self, logs=None):

        if self.start_monitoring:
            # Saving
            save_name = self.network.name
            if self.save_suffix is not None:
                save_name += '_{}'.format(self.save_suffix)
            filepath = os.path.join(self.save_path,
                                    '{}_graph_representations.npy'.format(save_name))
            np.save(filepath, self.stored_representations)

            # Resetting
            self.start_monitoring = None
            self.stored_representations = {}


class GeneratorTrainingLogger(Callback):

    def __init__(self, filepath, suffix=None, **kwargs):
        super(GeneratorTrainingLogger, self).__init__(**kwargs)
        self.filepath = filepath
        self.suffix = suffix
        self.info = {}

        if not os.path.isdir(filepath):
            os.makedirs(filepath)

    def on_step_checkpoint(self, step_checkpoint, logs=None):
        if logs is not None:
            assert type(logs) == dict

            for key, item in logs.items():
                self.info.setdefault(key, []).append(item)

    def on_train_end(self, logs=None):
        if self.suffix is None:
            filename = 'info.npy'
        else:
            filename = '{}_info.npy'.format(self.suffix)
        savepath = os.path.join(self.filepath, filename)
        np.save(savepath, self.info)

        # Clear
        self.info = {}
