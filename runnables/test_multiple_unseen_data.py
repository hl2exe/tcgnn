"""

Simple inference script on a given test data

"""

import os

import tensorflow as tf

import const_define as cd
from custom_callbacks_v2 import PredictionRetriever, RepresentationRetriever
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import unseen_data_test
from utility.json_utils import load_json, save_json
from utility.log_utils import Logger
from utility.python_utils import merge

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    # Settings

    model_type = "ibm2015_experimental_single_gnn_v2"
    test_name = "16-05-2020-15-37-20"
    evaluation_folder = cd.LOO_DIR
    save_predictions = True
    test_prefix = "loo"
    repetition_prefix = None

    model_path = os.path.join(evaluation_folder, model_type, test_name)

    # Logging
    Logger.set_log_path(model_path)
    logger = Logger.get_logger(__name__)

    # Configs

    model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))
    training_config = load_json(os.path.join(model_path, cd.JSON_TRAINING_CONFIG_NAME))
    distributed_info = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    # Loading data
    data_loader = DataLoaderFactory.factory(cl_type='cord19')
    data_handle = data_loader.load()

    trained_data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
    trained_data_loader_type = trained_data_loader_config['type']
    trained_data_loader_info = trained_data_loader_config['configs'][trained_data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    trained_data_loader_info = merge(trained_data_loader_info, loader_additional_info)
    trained_data_name = trained_data_loader_config['type'].upper()

    save_prefixes = [name for name in os.listdir(model_path) if 'key' in name]
    save_prefixes = [name.split('key')[1].split('_')[1].split('.')[0] for name in save_prefixes]
    save_prefixes = list(set(save_prefixes))

    for save_prefix in save_prefixes:

        # Callbacks
        # TODO: automatize callbacks creation
        callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
        pred_retriever = PredictionRetriever(save_path=os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name),
                                             save_suffix='key_{}'.format(save_prefix) if save_prefix else None)
        repr_retriever = RepresentationRetriever(save_path=os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name),
                                                 save_suffix='key_{}'.format(save_prefix) if save_prefix else None)
        callbacks = [
            pred_retriever,
            repr_retriever
        ]

        if save_predictions:
            save_base_path = os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name)

            if not os.path.isdir(save_base_path):
                os.makedirs(save_base_path)

        predictions = unseen_data_test(data_handle=data_handle,
                                       test_path=model_path,
                                       data_loader_info=trained_data_loader_info,
                                       trained_data_name=trained_data_name,
                                       callbacks=callbacks,
                                       model_type=model_type,
                                       network_args=model_config,
                                       training_config=training_config,
                                       distributed_info=distributed_info,
                                       save_prefix=save_prefix,
                                       repetition_prefix=repetition_prefix,
                                       test_prefix=test_prefix)

        if save_predictions:
            save_name = model_type
            if repetition_prefix is not None:
                save_name += '_repetition_{}'.format(repetition_prefix)
            if save_prefix is not None:
                save_name += '_key_{}'.format(save_prefix)
            save_json(os.path.join(save_base_path, '{0}_{1}'.format(save_name, cd.JSON_UNSEEN_PREDICTIONS_NAME)),
                      predictions)
