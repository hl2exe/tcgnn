"""

@Author: Federico Ruggeri

@Date: 27/02/2020

"""

import os
from datetime import datetime

import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

import const_define as cd
from custom_callbacks_v2 import EarlyStopping, TrainingLogger, TreeRegularizationExponentialDecay, TreeDebugger, TreeLangrangianEnabler
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import distributed_train_and_test
from utility.json_utils import save_json, load_json
from utility.log_utils import Logger
from utility.python_utils import merge
import numpy as np

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    train_and_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME))

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[
        train_and_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)

    data_handle = data_loader.load(**data_loader_info)

    # Strategy
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))
    strategy_name = distributed_info['strategy']
    if hasattr(tf.distribute.experimental, strategy_name):
        strategy = getattr(tf.distribute.experimental, strategy_name)
    elif hasattr(tf.distribute, strategy_name):
        strategy = getattr(tf.distribute, strategy_name)
    else:
        raise RuntimeError('Could not find distributed strategy! Got: {}'.format(strategy_name))

    strategy = strategy(**distributed_info['strategy_args'][strategy_name])

    if train_and_test_config['pre_loaded_model'] is None:
        current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
        save_base_path = os.path.join(cd.TRAIN_AND_TEST_DIR, train_and_test_config['model_type'], current_date)

        if train_and_test_config['save_model'] and not os.path.isdir(save_base_path):
            os.makedirs(save_base_path)
    else:
        save_base_path = os.path.join(cd.TRAIN_AND_TEST_DIR, train_and_test_config['model_type'],
                                      train_and_test_config['pre_loaded_model'])
        if not os.path.isdir(save_base_path):
            msg = "Can't find given pre-trained model. Got: {}".format(save_base_path)
            raise RuntimeError(msg)

    # Logging
    Logger.set_log_path(save_base_path)
    logger = Logger.get_logger(__name__)

    # Callbacks
    # TODO: automatize callbacks creation
    with strategy.scope():
        callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
        early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
        tree_decay = TreeRegularizationExponentialDecay(**callbacks_data['treeregularizationexponentialdecay'])
        tree_debugger = TreeDebugger(save_path=save_base_path)
        training_logger = TrainingLogger(filepath=save_base_path,
                                         save_model=train_and_test_config['save_model'])
        lagrangian_enabler = TreeLangrangianEnabler(**callbacks_data['treelagrangianenabler'])
        callbacks = [
            early_stopper,
            # tree_debugger,
            # tree_decay
            lagrangian_enabler
        ]

    if train_and_test_config['save_model']:
        callbacks.append(training_logger)

    scores = distributed_train_and_test(data_handle=data_handle,
                                        callbacks=callbacks,
                                        model_type=train_and_test_config['model_type'],
                                        training_config=training_config,
                                        error_metrics=train_and_test_config['error_metrics'],
                                        error_metrics_additional_info=train_and_test_config[
                                            'error_metrics_additional_info'],
                                        error_metrics_nicknames=train_and_test_config['error_metrics_nicknames'],
                                        compute_test_info=train_and_test_config['compute_test_info'],
                                        network_args=model_config,
                                        save_model=train_and_test_config['save_model'],
                                        test_path=save_base_path,
                                        repetitions=train_and_test_config['repetitions'],
                                        data_loader_info=data_loader_info,
                                        validation_percentage=train_and_test_config['validation_percentage'],
                                        distributed_info=distributed_info,
                                        strategy=strategy)

    # Validation
    if train_and_test_config['repetitions'] > 1:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))

    # Test
    if train_and_test_config['repetitions'] > 1:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if not key.startswith('avg')}))

    if train_and_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_PREDICTIONS_NAME), scores['predictions'])
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME), model_config)
        save_json(os.path.join(save_base_path, cd.JSON_TRAINING_CONFIG_NAME), training_config)
        save_json(os.path.join(save_base_path, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME), data=train_and_test_config)
        save_json(os.path.join(save_base_path, cd.JSON_CALLBACKS_NAME), data=callbacks_data)
        save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_CONFIG_NAME), data=distributed_info)
