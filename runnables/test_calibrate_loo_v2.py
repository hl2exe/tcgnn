"""

@Author: Federico Ruggeri

@Date: 03/09/2019

Hyperopt hyper-parameters calibration.

** BEFORE RUNNING **

1. Check the following configuration files:
    calibrator_info.json, hyperopt_model_gridsearch.json, distributed_config.json, callbacks.json, data_loader.json,
    model_config.json, training_config.json

"""

import os

import tensorflow as tf

from sklearn.model_selection import LeaveOneOut

import const_define as cd
from calibrators import HyperOptCalibrator
from custom_callbacks_v2 import EarlyStopping, EarlyPerformanceStopping, TreeLangrangianEnabler
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import loo_test
from utility.json_utils import load_json
from utility.python_utils import merge

if __name__ == '__main__':
    # Step 1: Validator config

    loo_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_LOO_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = loo_test_config['gpu_start_index']
            gpu_end_index = loo_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[
        loo_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Step 2: Calibrator config

    calibrator_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALIBRATOR_INFO_NAME))

    # Callbacks
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    early_performance_stopper = EarlyPerformanceStopping(**callbacks_data['earlyperformancestopping'])
    lagrangian_enabler = TreeLangrangianEnabler(**callbacks_data['treelagrangianenabler'])

    # LOO
    loo = LeaveOneOut()

    # Strategy
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    validator_base_args = {
        'validation_percentage': loo_test_config['validation_percentage'],
        'data_handle': data_handle,
        'network_args': model_config,
        'data_loader_info': data_loader_info,
        'model_type': loo_test_config['model_type'],
        'training_config': training_config,
        'error_metrics': loo_test_config['error_metrics'],
        'error_metrics_nicknames': loo_test_config['error_metrics_nicknames'],
        'error_metrics_additional_info': loo_test_config['error_metrics_additional_info'],
        'loo': loo,
        'save_model': False,
        'test_path': None,
        'use_tensorboard': False,
        'compute_test_info': False,
        'distributed_info': distributed_info,
        'callbacks': [early_stopper, early_performance_stopper, lagrangian_enabler],
        'split_key': loo_test_config['split_key'],
        'repetitions': loo_test_config['repetitions']
    }

    calibrator = HyperOptCalibrator(model_type=loo_test_config['model_type'],
                                    validator_method=loo_test,
                                    validator_base_args=validator_base_args,
                                    **calibrator_config)

    space = calibrator.load_space()
    calibrator.run(space=space)
