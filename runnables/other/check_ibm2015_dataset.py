"""

Check data separation among folds

"""

from utility.json_utils import load_json
import const_define as cd
from data_loader import DataLoaderFactory
from utility.log_utils import Logger
import os
from sklearn.model_selection import LeaveOneOut
import numpy as np

logger = Logger.get_logger(__name__)


def get_overlap_percentage(label, train_or_dev_df, test_df):
    train_or_dev_args = train_or_dev_df[train_or_dev_df[label] == 1]['Sentence'].values
    test_args = test_df[test_df[label] == 1]['Sentence'].values

    overlaps = np.sum([t_arg in train_or_dev_args for t_arg in test_args])
    return float(overlaps) / test_args.shape[0], overlaps, test_args.shape[0]


split_key = "Topic id"
validation_percentage = None
loo = LeaveOneOut()

# Loading data
data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
data_loader_type = "ibm2015"
data_loader_info = data_loader_config['configs'][data_loader_type]

data_loader = DataLoaderFactory.factory(data_loader_type)

data_handle = data_loader.load(**data_loader_info)

split_values = data_handle.get_loo_data(split_key)
for train_keys_indexes, excluded_key_indexes in loo.split(split_values):

    # train_keys = split_values[train_keys_indexes]
    excluded_key = split_values[excluded_key_indexes]

    if len(excluded_key) == 1:
        excluded_key = excluded_key[0]

    logger.info('Excluding: {}'.format(excluded_key))

    # Data conversion

    train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                      key_values=excluded_key,
                                                      validation_percentage=validation_percentage)

    train_claim_overlaps, \
    train_claim_overlap_amount, \
    train_claim_amount = get_overlap_percentage(label='C_Label',
                                                train_or_dev_df=train_df,
                                                test_df=val_df)
    val_claim_overlaps, \
    val_claim_overlap_amount, \
    val_claim_amount = get_overlap_percentage(label='C_Label',
                                              train_or_dev_df=val_df,
                                              test_df=test_df)

    train_evidence_overlaps,\
    train_evidence_overlap_amount,\
    train_evidence_amount = get_overlap_percentage(label='E_Label', train_or_dev_df=train_df, test_df=val_df)

    val_evidence_overlaps,\
    val_evidence_overlap_amount,\
    val_evidence_amount = get_overlap_percentage(label='E_Label', train_or_dev_df=val_df, test_df=test_df)

    logger.info("Fold stats: \n"
                "Claim overlaps: Train {0} ({1}/{2})"
                " -- Val {3} ({4}/{5})\n"
                "Evidence overlaps: Train {6} ({7}/{8})"
                " -- Val {9} ({10}/{11})\n".format(train_claim_overlaps, train_claim_overlap_amount, train_claim_amount,
                                                     val_claim_overlaps, val_claim_overlap_amount, val_claim_amount,
                                                     train_evidence_overlaps, train_evidence_overlap_amount, train_evidence_amount,
                                                     val_evidence_overlaps, val_evidence_overlap_amount, val_evidence_amount))
