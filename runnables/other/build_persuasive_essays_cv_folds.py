"""

@Author: Federico Ruggeri

@Date: 26/03/2019

Builds PE calibration folds (5 folds)

"""

import os

import numpy as np

import const_define as cd
from data_loader import PersuasiveEssaysCalibrationLoader
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json

if __name__ == '__main__':
    # Step 1: load dataset
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))['configs']

    loader = PersuasiveEssaysCalibrationLoader()

    data_handle = loader.load(**data_loader_config['persuasive_essays_calibration'])

    train_data = data_handle.data[data_handle.data.Split == 'TRAIN']

    dataset_list = np.unique(train_data['ID'].values)

    cv = PrebuiltCV(n_splits=5, cv_type='kfold',
                    shuffle=True, random_state=None, held_out_key='test')
    cv.build_folds(X=dataset_list, y=dataset_list)

    base_path = os.path.join(cd.PROJECT_DIR, 'prebuilt_folds')

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    folds_save_path = os.path.join(base_path, 'PE_splits_5_kfold.json')
    cv.save_folds(folds_save_path, tolist=True)

    list_save_path = os.path.join(base_path, 'PE_splits_5_kfold.txt')

    with open(list_save_path, 'w') as f:
        f.writelines(os.linesep.join(dataset_list))
