"""

Align MARGOT output predictions with dataset for model comparison

"""

import os

import numpy as np
import pandas as pd

import const_define as cd
from utility.preprocessing_utils import filter_line

# Load Dataset

df_path = os.path.join(cd.IBM2015_DIR, 'dataset.csv')
df = pd.read_csv(df_path)

text = df.Sentence.values

# Load MARGOT preds

margot_path = os.path.join(cd.MARGOT_DIR, 'output', '{}')

with open(margot_path.format('input.sentences_fix'), 'r') as f:
    margot_sentences = f.readlines()

with open(margot_path.format('output.claim.svm'), 'r') as f:
    margot_claim_preds = f.readlines()

with open(margot_path.format('output.evidence.svm'), 'r') as f:
    margot_evidence_preds = f.readlines()

# Remove new line and fullstop (MARGOT)
margot_sentences = list(map(lambda item: item[:-3], margot_sentences))

# Preprocessing

function_names = ['sentence_to_lower', 'punctuation_filtering', 'remove_special_words']

text = list(map(lambda item: filter_line(item, function_names), text))

if os.path.isfile('margot_sentences.npy'):
    margot_sentences = np.load('margot_sentences.npy')
else:
    margot_sentences = list(map(lambda item: filter_line(item, function_names), margot_sentences))
    np.save('margot_sentences.npy', margot_sentences)

non_exact_matches = set(margot_sentences).difference(set(text))
print('Remaining: ', len(non_exact_matches))

if os.path.isfile('merged_margot_info.npy'):
    merged_margot_info = np.load('merged_margot_info.npy', allow_pickle=True).tolist()
    checkpoint_info = np.load('checkpoint_info.npy', allow_pickle=True)
    idx, margot_idx = checkpoint_info[0], checkpoint_info[1]
    acc_claim_scores, acc_evidence_scores = checkpoint_info[2], checkpoint_info[3]
else:
    merged_margot_info = []
    margot_idx = 0
    idx = 0
    acc_claim_scores = []
    acc_evidence_scores = []

step_checkpoint = 200

while idx < len(text):
    orig_sent = text[idx]
    margot_sent = margot_sentences[margot_idx]
    claim_score = margot_claim_preds[margot_idx]
    evidence_score = margot_evidence_preds[margot_idx]

    if idx > 0 and idx % step_checkpoint == 0:
        print('#### Checkpoint {}! Saving all info... ####'.format(idx))
        np.save('merged_margot_info.npy', merged_margot_info)
        np.save('checkpoint_info.npy', [idx, margot_idx, acc_claim_scores, acc_evidence_scores])

    if orig_sent != margot_sent:
        print('Idx: ', idx)
        print('MARGOT idx: ', margot_idx)
        print('Original: ', orig_sent)
        print('MARGOT: ', margot_sent)
        print('(Next MARGOT: {})'.format(margot_sentences[margot_idx + 1]))
        check = input('Hot fix? Y/N')

        if check.lower() == 'y':
            margot_sentences[margot_idx] = orig_sent
            merged_margot_info.append((margot_sent, claim_score, evidence_score))
        else:
            if len(margot_sent) < len(orig_sent):
                # Check next
                print('Next MARGOT: ', margot_sentences[margot_idx + 1])
                stand_still = input('Stand still? Y/N ')

                if stand_still.lower() == 'y':
                    # Stand still with current original sentence
                    idx -= 1
                    acc_claim_scores.append(claim_score)
                    acc_evidence_scores.append(evidence_score)
                else:
                    merged_margot_info.append((margot_sent, max(acc_claim_scores), max(acc_evidence_scores)))
                    acc_claim_scores.clear()
                    acc_evidence_scores.clear()

            else:
                print('Next original: ', text[idx + 1])
                stand_still = input('Stand still? Y/N ')

                if stand_still.lower() == 'y':
                    # Stand still with current margot sentence
                    margot_idx -= 1
                    merged_margot_info.append((orig_sent, claim_score, evidence_score))
    else:
        merged_margot_info.append((margot_sent, claim_score, evidence_score))

    idx += 1
    margot_idx += 1

np.save('margot_sentences.npy', margot_sentences)

print(len(text))
print(len(merged_margot_info))
