"""

Computes graph statistics for given dataset

"""

import numpy as np
from data_loader import DataLoaderFactory
from utility.graph_utils import retrieve_hierarchical_trees_info

dataset_name = "ibm2015"
loader_args = {'selector': 'tree'}
retriever_args = {
    'kernel': 'stk'
}

data_loader = DataLoaderFactory.factory(dataset_name)

data_handle = data_loader.load(**loader_args)

trees = data_handle.train_data[data_handle.data_keys['tree']].values

(nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask,
 subtree_indices, subtree_edge_indices, subtree_edge_features,
 subtree_node_indices,
 subtree_node_segments, subtree_directionality_mask,), _ = retrieve_hierarchical_trees_info(trees,
                                                                                           **retriever_args)

# Compute statistics

graphs_len = [len(seq) for seq in nodes]
subtrees_len = [len(seq) for seq in subtree_indices]

print("Level 1: Word Level Statistics")

print('Mean graph size: ', np.mean(graphs_len))
print('Max graph size: ', np.max(graphs_len))
print('Min graph size: ', np.min(graphs_len))
print('99% graph size quantile: ', np.quantile(graphs_len, q=0.99))
print('95% graph size quantile: ', np.quantile(graphs_len, q=0.95))
#
print("Level 2: Subtree Level Statistics")

print('Mean subtree size: ', np.mean(subtrees_len))
print('Max subtree size: ', np.max(subtrees_len))
print('Min subtree size: ', np.min(subtrees_len))
print('99% subtree size quantile: ', np.quantile(subtrees_len, q=0.99))
print('95% subtree size quantile: ', np.quantile(subtrees_len, q=0.95))

# dep_trees = data_handle.train_data[data_handle.data_keys['tree']].values
#
# (nodes, edge_indices, edge_features,
#  node_indices, node_segments, directionality_mask), _ = retrieve_dep_trees_info(dep_trees,
#                                                                                 **retriever_args)
#
# graphs_len = [len(seq) for seq in nodes]
#
# print("Level 1: Word Level Statistics")
#
# print('Mean graph size: ', np.mean(graphs_len))
# print('Max graph size: ', np.max(graphs_len))
# print('Min graph size: ', np.min(graphs_len))
# print('99% graph size quantile: ', np.quantile(graphs_len, q=0.99))
# print('95% graph size quantile: ', np.quantile(graphs_len, q=0.95))
