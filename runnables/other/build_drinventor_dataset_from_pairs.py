"""

Builds dataset from Andrea's pairwise dataset

Dataset columns: source_proposition, target_proposition, source_type, target_type



"""

import pandas as pd
import const_define as cd
import os
import numpy as np


def build_dataset_from_pairwise(df_path):
    df = pd.read_csv(df_path)
    source_sentences = df['source_proposition'].values
    target_sentences = df['target_proposition'].values
    sentences = np.concatenate((source_sentences, target_sentences), axis=0)
    source_labels = df['source_type'].values
    target_labels = df['target_type'].values
    source_ID = df['source_ID'].values
    target_ID = df['target_ID'].values
    labels = np.concatenate((source_labels, target_labels), axis=0)
    IDs = np.concatenate((source_ID, target_ID), axis=0)

    merged = [(sent, sent_ID, label) for sent, sent_ID, label in zip(sentences, IDs, labels)]
    merged = list(set(merged))
    merged_dict = {
        'sentence': [pair[0] for pair in merged],
        'label': [pair[2] for pair in merged],
        'ID': [pair[1] for pair in merged]
    }
    merged_df = pd.DataFrame.from_dict(merged_dict)

    print('Original shape: ', df.shape[0])
    print('Reduced shape: ', merged_df.shape[0])

    return merged_df


base_path = os.path.join(cd.DR_INVENTOR_DIR, '{}_pair.csv')

train_df = build_dataset_from_pairwise(base_path.format('train'))
val_df = build_dataset_from_pairwise(base_path.format('validation'))
test_df = build_dataset_from_pairwise(base_path.format('test'))

save_path = os.path.join(cd.DR_INVENTOR_DIR, '{}_single.csv')

train_df.to_csv(save_path.format('train'), index=False)
val_df.to_csv(save_path.format('validation'), index=False)
test_df.to_csv(save_path.format('test'), index=False)
