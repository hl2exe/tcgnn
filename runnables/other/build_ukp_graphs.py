"""

Builds dependency trees with CoreNLP for UKP 2.0 dataset

"""

import os
import const_define as cd
from pycorenlp import StanfordCoreNLP
import pandas as pd
from tqdm import tqdm
import json


def corenlp_parse(texts, parser):
    trees = []
    dep_trees = []
    parsed_sentences = []
    for sentence in tqdm(texts, position=0, leave=True):
        # print('Considering sentence: {}'.format(sentence))

        annotation = parser.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'parse'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            # Sentences
            tokens = data['sentences'][0]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            parsed_sentences.append(parsed_sentence)

            data_tree = data['sentences'][0]['parse'].replace('\n', ' ')
            data_tree = data_tree.split()
            data_tree = ' '.join(data_tree)
            trees.append(data_tree)

            # Dependency tree
            data_dep_tree = [tuple((dep['dep'],
                                    dep['governor'],
                                    dep['governorGloss'],
                                    dep['dependent'],
                                    dep['dependentGloss']))
                             for dep in data['sentences'][0]['basicDependencies']]
            dep_trees.append(data_dep_tree)

    return trees, dep_trees, parsed_sentences


# Settings

port = 9000

df_path = cd.UKP_DIR

# Load dataset

df = pd.read_csv(os.path.join(df_path, 'dataset.csv'))

text = df.sentence.values

# CoreNLP parsing

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

trees, dep_trees, parsed_sentences = corenlp_parse(texts=text, parser=parser)

df['Tree'] = trees
df['Dep_Tree'] = dep_trees
df['Parsed_Sentence'] = parsed_sentences

df.to_csv(os.path.join(df_path, 'dataset_mod.csv'))
