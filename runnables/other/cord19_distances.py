import os

import numpy as np
import pandas as pd
import textdistance

import const_define as cd
from utility.json_utils import load_json


def get_top_K_indexes(data, K, reverse=True):
    if reverse:
        step = -1
    else:
        step = 1

    best_indexes = np.argsort(data)[::step]
    best_indexes = best_indexes[:K]

    return best_indexes


def cosine_similarity(a, b, transpose_b=False):
    a = a / np.linalg.norm(a, axis=-1)
    b = b / np.linalg.norm(b, axis=-1)
    if transpose_b:
        b = b.transpose()

    return np.matmul(a, b)


def compute_distance(target, source_sequences, dist_operator, reverse, K):
    distances = np.array([dist_operator(target, source_item) for source_item in source_sequences])

    top_indexes = get_top_K_indexes(distances, K=K, reverse=reverse)

    return top_indexes


model_type = "ibm2015_experimental_single_gnn_v2"
test_name = "16-05-2020-15-37-20"
base_path = os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name)

model_suffix = 'key_1'
task_name = 'Task1'
queries_path = os.path.join(base_path, "{0}_{1}_{2}_graph_representations.json")
data_path = os.path.join(base_path, "{0}_{1}_graph_representations.json")
sentences_path = os.path.join(base_path, '{0}_{1}_argumentative_dataset.csv')

print('Loading stuff...')

queries = load_json(queries_path.format(model_type, model_suffix, task_name))

with open(os.path.join(cd.CORD19_DIR, "{}.txt".format(task_name)), "r") as f:
    query_sentences = f.readlines()

print('Query data loaded!...')

embeddings = load_json(data_path.format(model_type, model_suffix))
df = pd.read_csv(sentences_path.format(model_type, model_suffix))

argumentative_indexes = df.Index.values
argumentative_embeddings = embeddings[argumentative_indexes]
argumentative_sentences = df.Sentence.values

print('Argumentative data loaded!...')

assert argumentative_embeddings.shape[0] == argumentative_sentences.shape[0]

top_k = 10

for query_emb, query_sentence in zip(queries, query_sentences):
    print('Considering query: ', query_sentence)

    emb_top_indexes = compute_distance(target=query_emb,
                                       source_sequences=argumentative_embeddings,
                                       reverse=True,
                                       K=top_k,
                                       dist_operator=cosine_similarity)

    print()
    print("Embeddings - Cosine distance")
    print('Top indexes: ', emb_top_indexes)
    print('Top sentences: ', argumentative_sentences[emb_top_indexes])
    print()

    jaccard_top_indexes = compute_distance(target=query_sentence,
                                           source_sequences=argumentative_sentences,
                                           reverse=False,
                                           K=top_k,
                                           dist_operator=textdistance.jaccard.distance)

    print("Textual - Jaccard")
    print('Top indexes: ', jaccard_top_indexes)
    print('Top sentences: ', argumentative_sentences[jaccard_top_indexes])
    print()

    hamming_top_indexes = compute_distance(target=query_sentence,
                                           source_sequences=argumentative_sentences,
                                           reverse=False,
                                           K=top_k,
                                           dist_operator=textdistance.hamming.distance)

    print("Textual - Hamming")
    print('Top indexes: ', hamming_top_indexes)
    print('Top sentences: ', argumentative_sentences[hamming_top_indexes])
    print()

    # levenshtein_top_indexes = compute_distance(target=query_sentence,
    #                                            source_sequences=argumentative_sentences,
    #                                            reverse=False,
    #                                            K=top_k,
    #                                            dist_operator=textdistance.levenshtein.distance)
    #
    # print("Textual - Levenshtein")
    # print('Top indexes: ', levenshtein_top_indexes)
    # print('Top sentences: ', argumentative_sentences[levenshtein_top_indexes])
    # print()