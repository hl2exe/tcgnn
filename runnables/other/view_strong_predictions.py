"""

Extracts pooling matrix from a pre-trained model
Gets most confident correct predictions (>= threshold) and shows poolings

"""

import os

import numpy as np
import tensorflow as tf

import const_define as cd
import matplotlib.pyplot as plt
from utility.log_utils import Logger
from utility.json_utils import load_json
import const_define as exp_cd
from data_converter import DataConverterFactory
from utility.python_utils import merge, flatten
from utility.data_utils import get_data_config_id
from utility.draw_graph_utils import draw_tree

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    model_type = "ukp_experimental_single_pooled_adj_gnn_v2"
    test_name = "05-09-2020-22-20-02"
    repetition_id = 0
    test_key = "loo_abortion"
    dataset_name = 'UKPLoo2.0'
    show_amount = 1

    model_path = os.path.join(cd.LOO_DIR, model_type, test_name)

    # Logging
    Logger.set_log_path(model_path)
    logger = Logger.get_logger(__name__)

    data_base_name = model_type
    if repetition_id is not None:
        data_base_name += "_repetition_{}".format(repetition_id)
    data_base_name += "_{0}_{1}_{2}.npy"

    # Get test set pooling matrix

    pooling_matrices_path = os.path.join(model_path, data_base_name.format(test_key, 'test', 'pooling_matrices'))
    pooling_matrices = np.load(pooling_matrices_path)

    # Get test set adjacency matrix

    adjacency_matrices_path = os.path.join(model_path, data_base_name.format(test_key, 'test', 'adjacency_matrices'))
    adjacency_matrices = np.load(adjacency_matrices_path)

    # Get test set pad mask

    pad_mask_path = os.path.join(model_path, data_base_name.format(test_key, 'test', 'pad_masks'))
    pad_masks = np.load(pad_mask_path)

    # Get test set predictions
    predictions = load_json(os.path.join(model_path, 'predictions.json'))
    fold = test_key.split('_')[1]
    fold_predictions = np.array(predictions[fold])

    if repetition_id is not None:
        fold_predictions = fold_predictions[repetition_id]

    fold_predictions = np.argmax(fold_predictions, axis=1)

    # Get test set true values
    fold_true_values = load_json(os.path.join(model_path, 'y_test_key_{}.json'.format(fold)))
    fold_true_values = np.argmax(fold_true_values, axis=1)

    # Get labels

    network_args = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

    data_loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_info = data_loader_info['configs'][data_loader_info['type']]

    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    info_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                  dataset_name,
                                  model_type)
    config_id = get_data_config_id(filepath=info_base_path, config=config_name)
    info_path = os.path.join(info_base_path, str(config_id))

    converter_type = exp_cd.MODEL_CONFIG[model_type]['converter']
    converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
        filepath=info_path,
        prefix=test_key)

    label_map = converter_info['label_map']
    label_map = {key: np.argmax(value) for key, value in label_map.items()}

    for key, value in label_map.items():

        correct_indexes = np.where((np.equal(fold_true_values, fold_predictions))
                                   & (fold_true_values == value)
                                   & (fold_predictions == value))[0]
        print("Total incorrect predictions for class {0}: {1}".format(key, len(correct_indexes)))

        if len(correct_indexes >= show_amount):
            selections = np.random.choice(correct_indexes, size=show_amount, replace=False)

            for i in selections:
                # draw_tree(nodes=pooling_matrices[i, :, 0], adjacency_matrix=adjacency_matrices[i], pad_mask=pad_masks[i])
                for k in range(pooling_matrices.shape[-1]):
                    draw_tree(nodes=pooling_matrices[i, :, k],
                              adjacency_matrix=adjacency_matrices[i],
                              pad_mask=pad_masks[i])

            plt.show()
