import pandas as pd
import os
import const_define as cd
from tqdm import tqdm
import json
from pycorenlp import StanfordCoreNLP
import numpy as np


def parse_corenlp(sentences, corenlp):
    parsed_sentences = []
    for sentence in sentences:
        annotation = corenlp.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'parse'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            # Sentences
            if len(data['sentences']) > 1:
                tokens = []
                for sent in data['sentences']:
                    tokens.extend(sent['tokens'])
            else:
                tokens = data['sentences'][0]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            parsed_sentences.append(parsed_sentence)

    return parsed_sentences[0]


def get_annotations(filename, file_path):
    with open(os.path.join(file_path, '{}.ann'.format(filename)), 'r') as f:
        data = f.readlines()

    data = [line for line in data if line.startswith('T')]

    arg_data = []

    for line in data:
        arg_type = line.split('\t')[1].split(' ')[0]
        arg_start_idx = int(line.split('\t')[1].split(' ')[1])
        arg_end_idx = int(line.split('\t')[1].split(' ')[2])
        arg_sentence = line.split('\t')[2:]
        arg_sentence = ' '.join(arg_sentence).strip()
        arg_data.append((arg_start_idx, arg_end_idx, filename.split('.')[0], arg_sentence, arg_type))

    arg_data = sorted(arg_data, key=lambda item: item[0])

    return arg_data


def check_missing(df_sentences, df_sections, annotations, key):
    missing = []
    for (arg_start_idx, arg_end_idx, arg_id, arg_sent, arg_type) in annotations:
        for df_sent, df_id in zip(df_sentences, df_sections):
            if arg_type == key and df_id == arg_id:
                if arg_sent.lower() in df_sent.lower():
                    missing.append((arg_sent, arg_type, arg_id))

    return missing


df_path = os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'dataset_mod.csv')
df = pd.read_csv(df_path)

data_path = os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'brat-project-final')

if not os.path.isfile('pe_annotations.npy'):
    annotations = []

    for doc_name in tqdm(os.listdir(data_path), leave=True, position=0):
        if not doc_name.endswith('.txt'):
            continue
        else:
            parsed_doc_name = doc_name.split('.')[0]

            # Get argumentative annotations
            doc_annotations = get_annotations(filename=parsed_doc_name, file_path=data_path)
            annotations.extend(doc_annotations)

    port = 9000
    parser = StanfordCoreNLP('http://localhost:{}'.format(port))

    annotations = [(item[0], item[1], item[2], parse_corenlp([item[3]], parser), item[4]) for item in tqdm(annotations)]

    np.save('pe_annotations', annotations)
else:
    annotations = np.load('pe_annotations.npy').tolist()

for key in ['Claim', 'MajorClaim', 'Premise']:
    df_sentences = df[df[key] == 0].Sentence.values
    df_sections = df[df[key] == 0].ID.values
    missing = check_missing(df_sentences=df_sentences, df_sections=df_sections, annotations=annotations, key=key)
    print('Key = {0} -- Missing = {1}'.format(key, len(missing)))

    if missing:
        print(missing)

    print('*' * 50)
