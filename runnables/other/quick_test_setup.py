"""

Simple test interface for quick and no error-prone setup

"""

import argparse
import json
import os
import time

import const_define as cd
from utility.json_utils import load_json, save_json
from utility.log_utils import Logger

logger = Logger.get_logger(__name__)


if __name__ == '__main__':

    # Immediately lock access to configuration files
    while os.path.isfile('quick_test_setup.lock'):
        logger.info("Another process is modifying access files, waiting...")
        time.sleep(1.0)
    with open('quick_test_setup.lock', 'w') as f:
        pass

    # Any model name
    model_type = "bert-base-uncased"

    # ukp, ibm2015, abst_rct, persuasive_essays
    model_dataset_type = "persuasive_essays"

    # ukp_loo, abst_rct, persuasive_essays, ibm2015cvmerged
    task_type = "persuasive_essays"

    # ukp_loo -> loo_test
    # abst_rct, persuasive_essays -> train_and_test
    # ibm2015cvmerged -> cv_test
    test_type = "train_and_test"

    # ukp_loo, ibm2015cvmerged, abst_rct, persuasive_essays
    dataset_name = "persuasive_essays"

    # If you want to save results and do a proper test -> True
    is_valid_test = True

    # Rename folder
    rename_folder = "f1_es_merged_trainable"

    # GPU
    gpu_start_index = 0
    gpu_end_index = 1

    # ibm2015 -> merge_label: C_Label, E_Label; merge: True, False; selector: tree, dep_tree
    # ukp -> merge: True, False; selector: tree, dep_tree
    # persuasive_essays -> merge: True, False; selector: tree, dep_tree
    # abst_rct -> selector: tree, dep_tree
    loader_args = {
        "merge": True,
        "selector": "tree",
        # "merge_label": "C_Label"
    }

    # earlystopping -> patience
    callback_args = {
        "earlystopping": {
            "patience": 10,
            "monitor": "val_macro_f1_score"
        }
    }

    batch_size = 16
    epochs = 10000
    step_checkpoint = 150

    quick_settings = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_QUICK_SETUP_CONFIG_NAME))

    # Parser
    parser = argparse.ArgumentParser(description="Argument Parser")
    parser.add_argument("-mt", "--model_type", help="Model architecture name."
                                                    " Check const_define.py -> MODEL_CONFIG variable for more info.",
                        type=str, default=model_type)
    parser.add_argument("-tt", "--task_type", help="ukp_loo, abst_rct, persuasive_essays, ibm2015cvmerged",
                        type=str, default=task_type)
    parser.add_argument("-mdt", "--model_dataset_type", help="ukp, ibm2015, persuasive_essays, abst_rct",
                        type=str, default=model_dataset_type)
    parser.add_argument("-dn", "--dataset_name", help="ukp_loo, ibm2015cvmerged, abst_rct, persuasive_essays",
                        type=str, default=dataset_name)
    parser.add_argument("-la", "--loader_args", help="ibm2015 -> merge_label, merge, selector;"
                                                     " ukp -> merge, selector;"
                                                     " persuasive_essays -> merge, selector;"
                                                     "abst_rct -> selector",
                        type=json.loads, default=loader_args)
    parser.add_argument("-test", "--test_type", help="cv_test, loo_test, train_and_test",
                        type=str, default=test_type)
    parser.add_argument("-ca", "--callback_args", help="generally set to 50 for GNNs",
                        type=json.loads, default=callback_args)
    parser.add_argument("-ivt", "--is_valid_test", help="If True, benchmark test configuration is loaded "
                                                        "and test results will be saved.",
                        type=bool, default=is_valid_test)
    parser.add_argument("-rf", "--rename_folder", help="How to rename the test folder when the test ends",
                        type=str, default=rename_folder)
    parser.add_argument("-gsi", "--gpu_start_index", help="Start index of visible gpu",
                        type=int, default=gpu_start_index)
    parser.add_argument("-gei", "--gpu_end_index", help="End index of visible gpu",
                        type=int, default=gpu_end_index)
    parser.add_argument("-bs", "--batch_size", help="Number of samples per batch",
                        type=int, default=batch_size)
    parser.add_argument("-e", "--epochs", help="Number of maximum epochs",
                        type=int, default=epochs)
    args = parser.parse_args()

    model_type = args.model_type
    model_dataset_type = args.model_dataset_type
    task_type = args.task_type
    test_type = args.test_type
    dataset_name = args.dataset_name
    is_valid_test = args.is_valid_test
    batch_size = args.batch_size
    epochs = args.epochs
    loader_args = args.loader_args
    callback_args = args.callback_args
    rename_folder = args.rename_folder
    gpu_start_index = args.gpu_start_index
    gpu_end_index = args.gpu_end_index

    logger.info("Debugging argparse...")
    for key, value in args.__dict__.items():
        logger.info("Arg: {0} -- Value: {1}".format(key, value))
    logger.info("End of debugging... {}".format(os.linesep))

    logger.info('Quick setup start!')

    # Data loader

    data_loader_settings = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_settings['type'] = task_type
    if dataset_name in quick_settings['input_specific_settings']:

        logger.info('Uploading data loader settings...')

        for key, value in quick_settings['input_specific_settings'][dataset_name].items():
            data_loader_settings['configs'][task_type][key] = value

        for key, value in loader_args.items():
            if key in data_loader_settings['configs'][task_type]:
                data_loader_settings['configs'][task_type][key] = value
            else:
                logger.info("Ignoring key {}... (not in original configuration)".format(key))

    else:
        logger.warning('Could not find dataset input specific settings...Is it ok?')

    logger.info('Data loader settings uploaded! Do not forget to check loader specific args!!')

    save_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME), data_loader_settings)

    # Callbacks
    logger.info('Uploading callbacks settings...')

    callbacks_settings = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    for key, key_config in callback_args.items():
        if key in callbacks_settings:
            for config_key, config_value in key_config.items():
                callbacks_settings[key][config_key] = config_value

    save_json((os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME)), callbacks_settings)

    logger.info('Callbacks settings uploaded!')

    # Specific test settings

    if model_dataset_type:
        actual_model_type = model_dataset_type + '_' + model_type
    else:
        actual_model_type = model_type
    test_settings = load_json(os.path.join(cd.CONFIGS_DIR, '{}_config.json'.format(test_type)))
    test_settings['model_type'] = actual_model_type

    if dataset_name in quick_settings['data_specific_settings']:

        logger.info('Uploading {} settings...'.format(test_type))

        for key, value in quick_settings['data_specific_settings'][dataset_name].items():
            test_settings[key] = value
    else:
        logger.warning('Could not find dataset test specific settings...Aborting..')
        exit(-1)

    if is_valid_test:
        if test_type in quick_settings['valid_test_settings'] and dataset_name in quick_settings['valid_test_settings'][
            test_type]:
            for key, value in quick_settings['valid_test_settings'][test_type][dataset_name].items():
                test_settings[key] = value

        test_settings['save_model'] = True
        test_settings['compute_test_info'] = True
    else:
        if test_type in quick_settings['default_test_settings'] and dataset_name in \
                quick_settings['default_test_settings'][test_type]:
            for key, value in quick_settings['default_test_settings'][test_type][dataset_name].items():
                test_settings[key] = value

        test_settings['save_model'] = False

    if model_type in quick_settings['model_specific_settings']:
        for key, value in quick_settings['model_specific_settings'][model_type].items():
            test_settings[key] = value
    else:
        logger.warning('Could not find model specific settings! Is this ok?')

    test_settings['rename_folder'] = rename_folder
    test_settings['gpu_start_index'] = gpu_start_index
    test_settings['gpu_end_index'] = gpu_end_index

    save_json(os.path.join(cd.CONFIGS_DIR, '{}_config.json'.format(test_type)), test_settings)

    logger.info('Data loader settings uploaded! Do not forget to check loader specific args!!')

    # Training settings

    training_path = os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME)
    training_settings = load_json(training_path)

    logger.info('Uploading {} training settings...'.format(test_type))

    training_settings['batch_size'] = batch_size
    training_settings['epochs'] = epochs
    training_settings['metrics'] = quick_settings['data_specific_settings'][dataset_name]['error_metrics']
    training_settings['additional_metrics_info'] = quick_settings['data_specific_settings'][dataset_name][
        'error_metrics_additional_info']
    training_settings['metrics_nicknames'] = quick_settings['data_specific_settings'][dataset_name][
        'error_metrics_nicknames']

    save_json(training_path, training_settings)

    logger.info(
        'Quick setup upload done! Check loader specific settings and your model config before running the test!')

    # Release access
    os.remove('quick_test_setup.lock')