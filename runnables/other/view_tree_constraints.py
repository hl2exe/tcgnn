"""

Extracts pooling matrix from a pre-trained model
Computes contiguous sequence constraint
Plots it

Notes:
    PE (3-classes):
        PTK: connectivity = 0.1|0.2|0.3|0.4, overlapping_threshold = 0.5 (0.297)
        SSTK: connectivity = 0.1|0.2|0.3, overlapping_threshold = 0.5 (0.298)
        STK: connectivity = 0.1|0.2|0.3, overlapping_threshold = 0.5 (0.297)

    PE (2-classes):
        PTK: connectivity = 0.1|0.2|0.3|0.4, overlapping_threshold = 0.5 (0.293)
        SSTK: connectivity = 0.1|0.2, overlapping_threshold = 0.5 (0.293)
        STK: connectivity = 0.1|0.2|0.3|0.4, overlapping_threshold = 0.5 (0.293)

    UKP:

    AbstRCT:

    IBM2015 (Evidence):
        PTK: connectivity = 0.1|0.2, overlapping = 0.3|0.4|0.5 (0.500)
        SSTK: connectivity = 0.1, overlapping= 0.3|0.4|0.5 (0.500)
        STK: connectivity = 0.1|0.2, overlapping= 0.3|0.4|0.5 (0.501)

    IBM2015 (Claim):

"""

import os

import numpy as np
import tensorflow as tf

import const_define as cd
import matplotlib.pyplot as plt
from utility.log_utils import Logger
from utility.draw_graph_utils import draw_tree
import utility.tree_constraints as tc
from utility.json_utils import load_json
from itertools import product
from tqdm import tqdm


def get_top_K_indexes(data, K):
    """
    Returns the top K indexes of a 1-dimensional array (descending order)
    Example:
        data = [0, 7, 2, 1]
        best_indexes:
        K = 1 -> [1] (data[1] = 7)
        K = 2 -> [1, 2]
        K = 3 -> [1, 2, 3]
        K = 4 -> [1, 2, 3, 4]

    :param data: 1-d dimensional array
    :param K: number of highest value elements to consider

    :return
        - array of indexes corresponding to elements of highest value
    """
    best_indexes = np.argsort(data, axis=0)
    best_indexes = best_indexes[:K]

    return best_indexes


def get_bottom_K_indexes(data, K):
    best_indexes = np.argsort(data, axis=0)[::-1]
    best_indexes = best_indexes[:K]

    return best_indexes


def plot_sample_tree_data(pooling_matrix, adjacency_matrix, node_mask, K, suffix):
    cluster_amount = pooling_matrix.shape[-1]

    for c in range(cluster_amount):
        draw_tree(nodes=pooling_matrix[:, c],
                  adjacency_matrix=adjacency_matrix,
                  pad_mask=node_mask,
                  title_name='Node pooling ({0} sample = {1}, cluster = {2})'.format(suffix, K + 1, c + 1))


def compute_average_violation(constraints):
    constraints = np.sum(constraints, axis=-1)
    return np.mean(constraints)


def compute_non_tree_model_constraints(pooling_matrix, adjacency_matrix, node_mask, kernel,
                                       connectivity_threshold, overlapping_threshold):
    constraint_values = np.hstack((tc.contiguous_sequence_constraint(pooling_matrix=pooling_matrix,
                                                                     adjacency_matrix=adjacency_matrix,
                                                                     node_mask=node_mask)[:, np.newaxis],
                                   tc.kernel_contiguous_constraint(kernel=kernel,
                                                                   pooling_matrix=pooling_matrix,
                                                                   adjacency_matrix=adjacency_matrix,
                                                                   node_mask=node_mask)[:, np.newaxis],
                                   tc.minimal_tree_loss(pooling_matrix=pooling_matrix,
                                                        overlapping_threshold=overlapping_threshold)[:, np.newaxis],
                                   tc.minimal_tree_intensity_loss(pooling_matrix=pooling_matrix,
                                                                  node_mask=node_mask,
                                                                  connectivity_threshold=connectivity_threshold)[:,
                                   np.newaxis]))

    return constraint_values


def compute_batched_non_tree_model_constraints(pooling_matrix, adjacency_matrix, node_mask, kernel,
                                               connectivity_threshold, overlapping_threshold,
                                               batch_size=128):
    samples = pooling_matrix.shape[0]
    num_batches = np.ceil(samples / batch_size).astype(np.int32)

    total_constraint_values = None

    for batch_idx in tqdm(range(num_batches)):
        start_index = batch_size * batch_idx
        end_index = start_index + batch_size

        constraint_values = compute_non_tree_model_constraints(
            pooling_matrix=pooling_matrix[start_index:end_index],
            adjacency_matrix=adjacency_matrix[start_index:end_index],
            node_mask=node_mask[start_index:end_index],
            kernel=kernel,
            connectivity_threshold=connectivity_threshold,
            overlapping_threshold=overlapping_threshold)

        if total_constraint_values is None:
            total_constraint_values = constraint_values
        else:
            total_constraint_values = np.concatenate((total_constraint_values, constraint_values), axis=0)

    return total_constraint_values


def get_gridsearch_parameters(param_dict):
    """
    Builds parameters combinations

    :param param_dict: dictionary that has parameter names as keys and the list of possible values as values
    (see model_gridsearch.json for more information)
    :return: list of dictionaries, each describing a parameters combination
    """

    params_combinations = []

    keys = sorted(param_dict)
    comb_tuples = product(*(param_dict[key] for key in keys))

    for comb_tuple in comb_tuples:
        instance_params = {dict_key: comb_item for dict_key, comb_item in zip(keys, comb_tuple)}
        params_combinations.append(instance_params)

    return params_combinations


def combination_to_string(kernel, connectivity_threshold, overlapping_threshold):
    comb_config_str = 'Kernel: {0} -- Connectivity Threshold: {1}' \
                      ' -- Overlapping threshold: {2}'.format(kernel,
                                                              connectivity_threshold,
                                                              overlapping_threshold)

    return comb_config_str


def compute_violation(test_keys, repetition_ids, constraints_config, visualize):
    total_average_violation = {}
    for test_key in test_keys:
        print("Considering fold: ", test_key)

        for repetition_id in repetition_ids:
            print("Considering repetition: ", repetition_id)

            # Get name
            data_base_name = model_type
            if repetition_id is not None:
                data_base_name += "_repetition_{}".format(repetition_id)
            else:
                data_base_name += '_repetition_0'
            if test_key is not None:
                data_base_name += "_{0}_{1}".format(test_prefix, test_key)
            data_base_name += '_{}'.format(set_suffix)
            data_base_name += '_{}.npy'
            data_base_path = os.path.join(model_path, data_base_name)

            # Load stuff

            # [samples, max_nodes, meta_nodes]
            pooling_matrices = np.load(data_base_path.format('pooling_matrices'))

            # [samples, max_nodes, max_nodes]
            adjacency_matrices = np.load(data_base_path.format('adjacency_matrices'))

            # [samples, max_nodes]
            pad_masks = np.load((data_base_path.format('pad_masks')))

            if os.path.isfile(data_base_path.format('constraints')):
                # [samples, # constraints]
                # Constraints in order: out of span; contiguous sequence; separation; minimal size; overlapping; ALO
                constraint_values = np.load(data_base_path.format('constraints'))

            else:
                print("No constraints found! Checking if it is a tree pooling based model...")
                model_config_path = os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME)
                model_config = load_json(model_config_path)

                if 'kernel' in model_config:
                    raise RuntimeError("Tree pooling based model detected but no constraint values found!"
                                       " Make sure you've run the forward test beforehand")
                else:
                    print("Model is not a tree pooling based model,"
                          " computing constraints and finding best configuration!")

                # [samples, # constraints]
                # Constraints in order: contiguous, kernel, size and overlap, intensity
                constraint_values = compute_batched_non_tree_model_constraints(
                    pooling_matrix=pooling_matrices,
                    adjacency_matrix=adjacency_matrices,
                    node_mask=pad_masks,
                    **constraints_config)

            # Visualization
            if visualize:

                # Compute sum
                # [samples]
                constraint_sum = np.sum(constraint_values, axis=-1)

                # Get top K values
                constraint_top_K_indexes = get_top_K_indexes(constraint_sum, K)

                for k, best_index in enumerate(constraint_top_K_indexes):
                    plot_sample_tree_data(pooling_matrix=pooling_matrices[best_index],
                                          adjacency_matrix=adjacency_matrices[best_index],
                                          node_mask=pad_masks[best_index],
                                          K=k,
                                          suffix='Top')

                plt.show()

                constraint_bottom_K_indexes = get_bottom_K_indexes(constraint_sum, K)

                for k, worst_index in enumerate(constraint_bottom_K_indexes):
                    plot_sample_tree_data(pooling_matrix=pooling_matrices[worst_index],
                                          adjacency_matrix=adjacency_matrices[worst_index],
                                          node_mask=pad_masks[worst_index],
                                          K=k,
                                          suffix='Bottom')

                plt.show()

            average_violation = compute_average_violation(constraint_values)
            # print('[Set: {0}] Average violation: '.format(set_suffix), average_violation)
            total_average_violation.setdefault(test_key, []).append(average_violation)

    all_set_values = list(total_average_violation.values())
    return np.mean(all_set_values), np.std(all_set_values)


if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    model_type = "ibm2015_experimental_single_tree_pooled_adj_gnn_v2"
    test_name = "C_f1_es_STK_fixed"
    repetition_ids = [None]
    # repetition_ids = np.arange(3)
    test_prefix = 'fold'
    # test_keys = [
    #     'abortion',
    #     'cloning',
    #     'death penalty',
    #     'gun control',
    #     'marijuana legalization',
    #     'minimum wage',
    #     'nuclear energy',
    #     'school uniforms'
    # ]
    test_keys = ['0', '1', '2', '3', '4']
    test_directory = cd.CV_DIR
    K = 1
    is_tree_gnn = True if 'tree' in model_type else False
    visualize = False
    calibrate = False
    a_priori_constraints_config = {
        'kernel': 'ptk',
        'connectivity_threshold': 0.3,
        'overlapping_threshold': 0.6,
    }
    constraints_config = a_priori_constraints_config
    comb_values = {
        'kernel': [
            'ptk',
            'sstk',
            'stk'
        ],
        'connectivity_threshold': [0.1, 0.2, 0.3, 0.4, 0.5],
        'overlapping_threshold': [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
    }

    model_path = os.path.join(test_directory, model_type, test_name)

    # Logging
    Logger.set_log_path(model_path)
    logger = Logger.get_logger(__name__)

    set_names = [
        # 'Train',
        'Validation',
        'Test'
    ]
    set_suffixes = [
        # 'train',
        'val',
        'test'
    ]

    for set_suffix in set_suffixes:
        print("Considering set: ", set_suffix)

        if not is_tree_gnn and calibrate and set_suffix.lower() == 'val':
            comb_violations = []

            for current_comb_config in tqdm(get_gridsearch_parameters(comb_values)):
                print('[Configuration] ', combination_to_string(**current_comb_config))

                set_violation = compute_violation(test_keys, repetition_ids, current_comb_config, visualize)

                comb_violations.append((current_comb_config, set_violation))

            comb_violations = sorted(comb_violations, key=lambda pair: pair[1])
            print("Sorted combinations: ", comb_violations)
            print('Best combination: ', combination_to_string(**comb_violations[0][0]))
            print('Best combination violation: ', comb_violations[0][1])
        else:
            mean_set_violation, std_set_violation = compute_violation(test_keys, repetition_ids, constraints_config, visualize)
            print("Total mean violation: ", mean_set_violation)
            print("Total std violation: ", std_set_violation)
