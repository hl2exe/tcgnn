"""

@Author: Federico Ruggeri

@Date: 29/01/2020

@Description: Given data-set sentences, MARGOT is invoked on each of them and results are eventually collected.
Lastly, all predictions are saved to file.

"""

import os
from collections import OrderedDict
from subprocess import Popen

import numpy as np
from sklearn.metrics import f1_score
from sklearn.model_selection import LeaveOneOut
import pandas as pd

import const_define as cd
from data_loader import IBM2015Loader


def parse_output(file_path):

    evidence_path = os.path.join(file_path, 'output.evidence.svm')
    claim_path = os.path.join(file_path, 'output.claim.svm')

    with open(evidence_path, 'r') as f:
        evidence_preds = f.readlines()

    evidence_preds = [float(item.strip()) for item in evidence_preds]

    with open(claim_path, 'r') as f:
        claim_preds = f.readlines()

    claim_preds = [float(item.strip()) for item in claim_preds]

    assert len(evidence_preds) == len(claim_preds)

    predictions = []
    for claim_pred, evidence_pred in zip(claim_preds, evidence_preds):
        curr_pred = [0, 0, 0]
        if claim_pred > 0:
            curr_pred[1] = 1
        if evidence_pred > 0:
            curr_pred[2] = 1

        if np.sum(curr_pred) == 0:
            curr_pred[0] = 1

        predictions.append(curr_pred)

    return np.array(predictions)


# Settings

margot_path = cd.MARGOT_DIR
extraction_dir = os.path.join(margot_path, 'OUTPUT')
extension = '.json'

if not os.path.isdir(extraction_dir):
    os.makedirs(extraction_dir)

# Load Dataset

data_loader = IBM2015Loader()
data_handle = data_loader.load()

data_path = os.path.join(cd.IBM2015_DIR, 'dataset_final.csv')
data = pd.read_csv(data_path)
sentences = data.Sentence.values
trees = data.Tree.values

# Save sentence to file
with open(os.path.join(margot_path, 'sentences_tmp.txt'), 'w') as f:
    f.writelines(list(map(lambda item: item + os.linesep, sentences)))
    sent_tmp_path = f.name

# Save tree to file
with open(os.path.join(margot_path, 'trees_tmp.txt'), 'w') as f:
    f.writelines(list(map(lambda item: item + os.linesep, trees)))
    tree_tmp_path = f.name

# Run MARGOT predictor
process = Popen(os.path.join(margot_path, 'slim_run_margot.sh {0} {1} {2}'.format(sent_tmp_path, tree_tmp_path, extraction_dir)),
                shell=True, cwd=margot_path)

process.wait()

# Get MARGOT output
predictions = parse_output(extraction_dir)

np.save('margot_predictions.npy', predictions)

predictions = np.load('margot_predictions.npy')

# LOO
split_key = "Topic id"
validation_percentage = None

loo = LeaveOneOut()
split_values = data_handle.get_loo_data(split_key)

test_info = OrderedDict()
for train_keys_indexes, excluded_key_indexes in loo.split(split_values):
    excluded_key = split_values[excluded_key_indexes]

    if len(excluded_key) == 1:
        excluded_key = excluded_key[0]

    print('Excluding: {}'.format(excluded_key))

    train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                      key_values=excluded_key,
                                                      validation_percentage=validation_percentage)

    test_labels = np.concatenate((test_df['NA_Label'].values[:, np.newaxis],
                                  test_df['C_Label'].values[:, np.newaxis],
                                  test_df['E_Label'].values[:, np.newaxis]), axis=1)
    loo_predictions = predictions[test_df.index.values]

    avg_f1 = f1_score(y_true=test_labels, y_pred=loo_predictions, average='weighted')
    f1 = f1_score(y_true=test_labels, y_pred=loo_predictions, average=None)

    test_info.setdefault('weighted_f1_score', []).append(avg_f1)
    test_info.setdefault('per_class_f1_score', []).append(f1)


print('Macro weighted f1 score: {}'.format(np.mean(test_info['weighted_f1_score'])))
print('Macro per class f1 score: {}'.format(np.mean(test_info['per_class_f1_score'], axis=0)))

