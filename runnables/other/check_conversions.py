"""

Compares v1 and experimental v2 tree conversions

"""

import numpy as np
from utility.graph_utils import pad_tree_data

x_test_v1 = np.load('x_test_v1.npy', allow_pickle=True).item()
x_test_v2 = np.load('x_test_v2.npy', allow_pickle=True).item()

samples = 10000
reduced_v1 = {key: item[:samples] for key, item in x_test_v1.items()}

max_graph_nodes = x_test_v2['node_ids'][0].shape[-1]
max_graph_edges = int(x_test_v2['edge_indices'][0].shape[-1] / 2)
max_graph_depth = int(x_test_v2['edge_features'][0].shape[-1] / max_graph_edges)

print('Max Nodes: ', max_graph_nodes)
print('Max Edges: ', max_graph_edges)
print('Max Depth: ', max_graph_depth)

reduced_v2 = {key: np.array(item[:samples]) for key, item in x_test_v2.items()}

# Get V1 Data

v1_node_ids = reduced_v1['nodes']
v1_edge_features = reduced_v1['edge_features']
v1_edge_indices = reduced_v1['edge_indices']
v1_node_indices = reduced_v1['node_indices']
v1_node_segments = reduced_v1['node_segments']

v1_node_ids, v1_edge_indices, v1_edge_features, \
v1_node_indices, v1_node_segments, v1_edge_mask = pad_tree_data(v1_node_ids,
                                                                v1_edge_indices,
                                                                v1_edge_features,
                                                                v1_node_indices,
                                                                v1_node_segments,
                                                                max_graph_nodes,
                                                                max_graph_depth)

print('V1 Shapes')
print('Node ids: ', v1_node_ids.shape)
print('Edge features: ', v1_edge_features.shape)
print('Edge indices: ', v1_edge_indices.shape)
print('Node indices: ', v1_node_indices.shape)
print('Node segments: ', v1_node_segments.shape)
print('Edge mask: ', v1_edge_mask.shape)
print()

# Get V2 Data

v2_node_ids = reduced_v2['node_ids'].squeeze()
v2_edge_features = reduced_v2['edge_features'].squeeze().reshape(-1, max_graph_edges, max_graph_depth)
v2_edge_indices = reduced_v2['edge_indices'].squeeze().reshape(-1, max_graph_edges, 2)
v2_node_indices = reduced_v2['node_indices'].squeeze()
v2_node_segments = reduced_v2['node_segments'].squeeze()

sample_max = np.max(v2_node_segments, axis=1)
batch_indexes = np.arange(v2_node_segments.shape[0])
to_add = (sample_max + 1) * batch_indexes
to_add = to_add[:, np.newaxis]
v2_node_segments += to_add

v2_edge_mask = reduced_v2['edge_mask'].squeeze()

print('V2 Shapes')
print('Node ids: ', v2_node_ids.shape)
print('Edge features: ', v2_edge_features.shape)
print('Edge indices: ', v2_edge_indices.shape)
print('Node indices: ', v2_node_indices.shape)
print('Node segments: ', v2_node_segments.shape)
print('Edge mask: ', v2_edge_mask.shape)
print()


print('Same node_ids? ', np.equal(v1_node_ids, v2_node_ids).all())
print('Same edge features? ', np.equal(v1_edge_features, v2_edge_features).all())
print('Same edge_indices? ', np.equal(v1_edge_indices, v2_edge_indices).all())
print('Same node indices? ', np.equal(v1_node_indices, v2_node_indices).all())
print('Same node segments? ', np.equal(v1_node_segments, v2_node_segments).all())
print('Same edge mask? ', np.equal(v1_edge_mask, v2_edge_mask).all())
