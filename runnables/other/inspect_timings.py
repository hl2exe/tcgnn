
import numpy as np
import os
import const_define as cd


model_type = "ibm2015_experimental_single_tree_pooled_adj_gnn_v2"
test_name = "C_STK_timing"
folder = cd.CV_DIR

model_path = os.path.join(folder, model_type, test_name)

for set_split in ['train', 'test']:
    data_filename = [item for item in os.listdir(model_path) if '{}_times.npy'.format(set_split) in item][0]
    data = np.load(os.path.join(model_path, data_filename))
    print('[{0}] Average timing: {1}'.format(set_split, np.mean(data)))
    print('[{0}] Std timing: {1}'.format(set_split, np.std(data)))
