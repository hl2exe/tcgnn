"""

Claims not found:
    1. a community of separate cultures is divisive .
    2. multiculturalism is divisive .
    3. the patent monopoly .
    4. sexual activity outside of the context of marriage is likely to have harmful psychological and physical effects .

Evidences not found:
    1. when my first wife and i began the school , we had one main idea : to make the school fit the child - instead of making the child fit the school '' -- a.s. neill .
    2. when my first wife and i began the school , we had one main idea : to make the school fit the child - instead of making the child fit the school '' -- a.s. neill .
    3. s.k.b ' .
    4. rehnquist wrote , .
    5. rehnquist wrote , .
    6. rehnquist wrote , .
    7. rehnquist wrote , .

"""

import json
import os

import numpy as np
import pandas as pd
from pycorenlp import StanfordCoreNLP
from tqdm import tqdm

import const_define as cd


def parse_corenlp(sentences, corenlp):
    parsed_sentences = []
    for sentence in tqdm(sentences):
        annotation = corenlp.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'parse'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            # Sentences
            for idx in range(len(data['sentences'])):
                tokens = data['sentences'][idx]['tokens']
                tokens = [item['word'] for item in tokens]
                parsed_sentence = ' '.join(tokens)
                parsed_sentence = parsed_sentence.lower()
                parsed_sentences.append(parsed_sentence)

    return parsed_sentences


df_path = os.path.join(cd.IBM2015_DIR, 'dataset.csv')
df = pd.read_csv(df_path)

print("Loaded shape: ", df.shape)

arg_collisions = []
for arg_key, arg_filter in zip(['C', 'E'], [15, 40]):
    print('Considering argument: ', arg_key)

    args = df[df.Label == arg_key].Sentence.values.astype(str)
    args_indexes = df[df.Label == arg_key].index.values
    normal = df[df.Label == 'NOT-ARG'].Sentence.values.astype(str)
    normal_indexes = df[df.Label == 'NOT-ARG'].index.values

    if arg_key == 'C':
        args_corrected = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'claims.txt'), sep='\t')[
            'Claim corrected version'].values.astype(str)

        if not os.path.isfile(os.path.join(cd.IBM2015_DIR, 'claims_original_parsed.npy')):
            print('Computing corenlp claims!')
            nlp = StanfordCoreNLP('http://localhost:{}'.format(9000))
            args_corrected = parse_corenlp(args_corrected, nlp)
            args_corrected = np.array(args_corrected)
            np.save(os.path.join(cd.IBM2015_DIR, 'claims_original_parsed.npy'), args_corrected)
        else:
            print('Loading pre-parsed claims!')
            args_corrected = np.load(os.path.join(cd.IBM2015_DIR, 'claims_original_parsed.npy'))

    else:
        args_corrected = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'fixed_evidence.txt'))[
            'Evidence'].values.astype(str)
        # args_corrected = np.array([' '.join(word_tokenize(item)) for item in args_corrected])

        if not os.path.isfile(os.path.join(cd.IBM2015_DIR, 'evidence_original_parsed.npy')):
            print('Computing corenlp evidence!')
            nlp = StanfordCoreNLP('http://localhost:{}'.format(9000))
            args_corrected = parse_corenlp(args_corrected, nlp)
            args_corrected = np.array(args_corrected)
            np.save(os.path.join(cd.IBM2015_DIR, 'evidence_original_parsed.npy'), args_corrected)
        else:
            print('Loading pre-parsed evidence!')
            args_corrected = np.load(os.path.join(cd.IBM2015_DIR, 'evidence_original_parsed.npy'))

    # assert args.shape[0] == args_corrected.shape[0]

    lengths = np.array([len(item) for item in args_corrected])
    valid_indexes = np.where(lengths > arg_filter)[0]
    args_corrected = args_corrected[valid_indexes]

    print('Args shape: ', args.shape)
    print('Corenlp parsed args shape: ', args_corrected.shape)

    collisions = []

    print('Checking argument as it is')
    for arg in tqdm(args):
        for n_idx, n in zip(normal_indexes, normal):
            if arg[:-2].lower() in n.lower():
                collisions.append(n_idx)

    print('Checking parsed argument')
    for arg_corrected in tqdm(args_corrected):
        debug_counter = 0
        for n_idx, n in zip(normal_indexes, normal):
            if arg_corrected[:-2].lower() in n.lower():
                collisions.append(n_idx)
                debug_counter += 1
        if debug_counter == 0:
            print('Could not find any match for argument: ', arg_corrected)

    # for arg, arg_corrected in tqdm(zip(args, args_corrected), total=args.shape[0]):
    #     debug_counter = 0
    #     for n_idx, n in zip(normal_indexes, normal):
    #         if arg[:-2] in n or arg_corrected[:-2].lower() in n:
    #             # if arg_corrected.lower() in n:
    #             # if arg[:-2] in n:
    #             collisions.append(n_idx)
    #             debug_counter += 1
    #         # if arg[:-2] in n and not arg_corrected[:-2].lower() in n:
    #         #     print('Check this out:\n'
    #         #           'Normal: ', n, '\n',
    #         #           'Original arg: ', arg, '\n',
    #         #           'Corrected arg: ', arg_corrected)
    #     if debug_counter == 0:
    #         print('Could not find any match for argument: ', arg)

    print('All collisions: ', len(collisions))
    collisions = list(set(collisions))
    print('Unique collisions: ', len(collisions))

    arg_collisions.append(collisions)

labels = {}
for arg_key, arg_colls in zip(['C', 'E'], arg_collisions):
    arg_colls = np.array(arg_colls, dtype=np.int32)
    arg_labels = np.zeros(df.shape[0], dtype=np.int32)
    arg_labels[arg_colls] = 1
    labels['{}_Label'.format(arg_key)] = arg_labels

na_labels = np.ones(df.shape[0])
for arg_colls in arg_collisions:
    arg_colls = np.array(arg_colls, dtype=np.int32)
    na_labels[arg_colls] = 0

for arg_key in ['C', 'E']:
    df.loc[:, '{}_Label'.format(arg_key)] = labels['{}_Label'.format(arg_key)]

df.loc[:, 'NA_Label'] = na_labels

for arg_key in ['C', 'E']:
    arg_remove_indexes = df[df.Label == arg_key].index.values
    df = df.drop(arg_remove_indexes)

# if collisions:
#     df.loc[collisions, 'Label'] = 'New_{}'.format(arg_key)
#     df = df.drop(args_indexes)
#     df.loc[collisions, 'Label'] = arg_key
#
del df['Label']
print("Fixed dataset shape: ", df.shape)
print('Claim examples: ', df[df['C_Label'] == 1].shape)
print('Evidence examples: ', df[df['E_Label'] == 1].shape)
print('Both claim and evidence examples: ', df[(df['C_Label'] == 1) & (df['E_Label'] == 1)].shape)
print('Not argumentative examples: ', df[df['NA_Label'] == 1].shape)
#
df.to_csv(os.path.join(cd.IBM2015_DIR, 'dataset_final.csv'), index=False)
