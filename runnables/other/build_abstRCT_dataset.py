"""

Builds a pandas.DataFrame for AbstRCT (Clinical Trials) dataset with the following columns:

Doc     Sentence        Label       Split

where Label :- [MajorClaim, Claim, Premise, Not-Argumentative]; Split :- [Train, Test]

...starting from disgusting brat-compliant format (so shitty jeez)

How to get sentence-level info:

1. Parse each document with CoreNLP -> sentences
2. Read annotations -> argumentative phrases
3. Match argument phrase with corresponding containing sentence

"""

import const_define as cd
import pandas as pd
import os
from tqdm import tqdm
from pycorenlp import StanfordCoreNLP
import simplejson as sj
from utility.log_utils import Logger
import numpy as np
from nltk import word_tokenize
import re

logger = Logger.get_logger(__name__)


def get_annotations(filename, file_path):
    with open(os.path.join(file_path, '{}.ann'.format(filename)), 'r') as f:
        data = f.readlines()

    data = [line for line in data if line.startswith('T')]

    arg_data = []

    for line in data:
        arg_type = line.split('\t')[1].split(' ')[0]
        arg_start_idx = int(line.split('\t')[1].split(' ')[1])
        arg_end_idx = int(line.split('\t')[1].split(' ')[2])
        arg_sentence = line.split('\t')[2:]
        arg_sentence = ' '.join(arg_sentence).strip()
        arg_data.append((arg_start_idx, arg_end_idx, filename.split('.')[0], arg_sentence, arg_type))

    arg_data = sorted(arg_data, key=lambda item: item[0])

    return arg_data


def find_annotation(orig_sent, doc_annotations):
    # MajorClaim    Claim   Premise     Not-Argumentative
    labels = [0, 0, 0, 0]
    to_remove = []
    for idx, (ann_start_idx, ann_end_idx, ann_sent, ann_type) in enumerate(doc_annotations):
        if ann_sent.lower() in orig_sent.lower() or ' '.join(
                word_tokenize(ann_sent)).lower() in orig_sent.lower():

            current_label = ann_type
            to_remove.append(idx)

            if current_label == 'MajorClaim':
                labels[0] = 1
            elif current_label == 'Claim':
                labels[1] = 1
            elif current_label == 'Premise':
                labels[2] = 1
        # elif sequence_pointer_max not in [ann_start_idx - offset, ann_end_idx + offset]:
        #     logger.warn('Sequence pointer not in range! Pointer = {0} -- Range = [{1}, {2}]'.format(sequence_pointer_max,
        #                                                                                            ann_start_idx - offset,
        #                                                                                            ann_end_idx + offset))

    if np.sum(labels) == 0:
        labels[-1] = 1
    else:
        for item in to_remove:
            del doc_annotations[item]

    return labels, doc_annotations


def parse_document(doc_text, doc_name, parser):
    annotation = parser.annotate(doc_text, properties={'timeout': 10000000, 'pipelineLanguage': 'en',
                                                       'annotators': 'parse,depparse,ssplit'})

    if annotation:
        try:
            data = sj.loads(annotation)
        except Exception as e:
            logger.info('Failed to annotate doc: {}'.format(doc_name))
            logger.info('{}'.format(e))

    for sentence_idx, sentence in enumerate(data['sentences']):
        tokens = sentence['tokens']
        tokens = [item['word'] for item in tokens]

        parsed_sentence = ' '.join(tokens)
        parsed_sentence = parsed_sentence.lower()

        # Tree
        data_tree = sentence['parse'].replace('\n', ' ')
        data_tree = data_tree.split()
        data_tree = ' '.join(data_tree)

        # Dependency tree
        data_dep_tree = [tuple((dep['dep'],
                                dep['governor'],
                                dep['governorGloss'],
                                dep['dependent'],
                                dep['dependentGloss']))
                         for dep in sentence['basicDependencies']]

        yield sentence_idx, parsed_sentence, data_tree, data_dep_tree


def parse_doc_arguments(doc_arguments, parser, doc_name):
    parsed_arguments = []

    for arg_sentence in doc_arguments:

        arg_sentence = arg_sentence.replace("%", "%25")
        arg_sentence = arg_sentence.replace("\\+", "%2B")
        arg_sentence = re.sub(r'[^\x00-\x7f]', r'', arg_sentence)
        arg_sentence = arg_sentence.strip()

        annotation = parser.annotate(arg_sentence, properties={'timeout': 10000000, 'pipelineLanguage': 'en'})

        if annotation:
            try:
                data = sj.loads(annotation)
            except Exception as e:
                logger.info('[Doc: {0}] Failed to annotate doc argument: {1}'.format(doc_name, arg_sentence))
                logger.info('{}'.format(e))

        assert len(data['sentences']) == 1

        tokens = data['sentences'][0]['tokens']
        tokens = [item['word'] for item in tokens]

        parsed_sentence = ' '.join(tokens)
        parsed_sentence = parsed_sentence.lower()

        parsed_arguments.append(parsed_sentence)

    return parsed_arguments


# Settings

data_path = os.path.join(cd.RCT_DIR, 'data')
port = 9000

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

df_data = {}

# warning_debug = 0
# total_not_found = 0

for split_set in ['train', 'dev', 'test']:
    for folder in os.listdir(os.path.join(data_path, split_set)):
        logger.info('Considering split set: {}'.format(folder))
        if os.path.isdir(os.path.join(data_path, split_set, folder)):
            for doc_name in tqdm(os.listdir(os.path.join(data_path, split_set, folder)), leave=True, position=0):
                if doc_name.endswith('.txt'):
                    parsed_doc_name = doc_name.split('.')[0]

                    # Get text
                    with open(os.path.join(data_path, split_set, folder, doc_name), 'r') as f:
                        doc_text = f.readlines()
                        doc_text = ''.join(doc_text)

                    # Correct special tokens
                    doc_text = doc_text.replace("%", "%25")
                    doc_text = doc_text.replace("\\+", "%2B")
                    doc_text = re.sub(r'[^\x00-\x7f]', r'', doc_text)
                    doc_text = doc_text.strip()

                    # Get annotations
                    doc_annotations = get_annotations(filename=parsed_doc_name,
                                                      file_path=os.path.join(data_path, split_set, folder))
                    doc_arguments = [item[3] for item in doc_annotations]
                    doc_arguments = parse_doc_arguments(doc_arguments, parser, parsed_doc_name)
                    doc_labels = [item[4] for item in doc_annotations]

                    found_arguments = 0
                    total_arguments = len(doc_arguments)

                    # Run CoreNLP
                    for p_sentence_idx, p_sentence, p_tree, p_dep_tree in parse_document(doc_text=doc_text,
                                                                                         doc_name=parsed_doc_name,
                                                                                         parser=parser):
                        if p_sentence in doc_arguments:
                            found_arguments += 1
                            arg_index = doc_arguments.index(p_sentence)
                            doc_arguments.pop(arg_index)
                            p_label = doc_labels[arg_index]
                        else:
                            p_label = 'NA'

                        # Add data to df
                        df_data.setdefault('Sentence ID', []).append(p_sentence_idx)
                        df_data.setdefault('ID', []).append(parsed_doc_name)
                        df_data.setdefault('Category', []).append(folder.split('_')[0])
                        df_data.setdefault('Split', []).append(split_set)
                        df_data.setdefault('Sentence', []).append(p_sentence)
                        df_data.setdefault('Tree', []).append(p_tree)
                        df_data.setdefault('Dep_Tree', []).append(p_dep_tree)
                        df_data.setdefault('Label', []).append(p_label)

                    if found_arguments != total_arguments:
                        logger.warning('[Split {0}, Doc {1}] Argument matching failed! Found: {2} - Total: {3}'
                                       .format(split_set, parsed_doc_name, found_arguments, total_arguments))
                        logger.warning('[Split {0}, Doc {1}] Unmatched arguments: {2}'
                                       .format(split_set, parsed_doc_name, doc_arguments))

# Saving
df = pd.DataFrame.from_dict(df_data)
df.to_csv(os.path.join(cd.RCT_DIR, 'dataset_final.csv'), index=False)
