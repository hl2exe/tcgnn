import argparse
import os

import numpy as np
import tensorflow_hub as hub
import textdistance

parser = argparse.ArgumentParser(description="")
parser.add_argument("query", help="query sentence")
parser.add_argument('embeddings',
                    help="The path of a file which contains pre-loaded embeddings")
parser.add_argument('sentences',
                    help="The path of a text file which contains all the sentences")

args = parser.parse_args()
sentences_list = []

with open(args.sentences) as f:
    for line in f:
        if line is not "":
            sentences_list.append(line.strip().lower())

s1 = args.query.strip().lower()

with hub.eval_function_for_module("https://tfhub.dev/google/elmo/2") as elmo:
    batch_in = np.array([s1])
    s1e = elmo(batch_in, as_dict=True)['default'][0]
    s1e = np.ravel(s1e)

top_k = 10

max = 999
best_idx = 0

distances = []

for idx2 in range(len(sentences_list)):
    s2 = sentences_list[idx2]

    if args.embeddings is not None and os.path.isfile(args.embeddings):
        dictionary = np.load(args.embeddings, allow_pickle=True).item()

    s2e = dictionary[s2]

    d = distances.cosine(s1e, s2e)

    if d < max:
        max = d
        best_idx = idx2
    distances.append(d)

print("EMBEDDINGS")
print(sentences_list[best_idx])
print(max)

for n in range(top_k):
    best = np.argmin(distances)

    print(distances[best])
    print(sentences_list[best])
    distances[best] = 999

    for tdistance in [textdistance.jaccard, textdistance.hamming, textdistance.levenshtein]:
        max = 999
        best_idx = 0
        distances = []
        for idx2 in range(len(sentences_list)):
            s2 = sentences_list[idx2]

        if args.embeddings is not None and os.path.isfile(args.embeddings):
            dictionary = np.load(args.embeddings, allow_pickle=True).item()

        d = tdistance.distance(s1, s2)

        if d < max:
            max = d
        best_idx = idx2
        print(tdistance)
        print(sentences_list[best_idx])
        print(max)

        for n in range(top_k):
            best = np.argmin(distances)

        print(distances[best])
        print(sentences_list[best])
        distances[best] = 999
