"""

Builds dependency trees with CoreNLP for ACL 18 (IBM) dataset

"""

import os
import const_define as cd
from pycorenlp import StanfordCoreNLP
import pandas as pd
from tqdm import tqdm
import json


def corenlp_parse(texts, parser):
    trees = []
    dep_trees = []
    for sentence in tqdm(texts, position=0, leave=True):
        # print('Considering sentence: {}'.format(sentence))

        annotation = parser.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'parse, depparse'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            # Constituency tree
            data_tree = data['sentences'][0]['parse'].replace('\n', ' ')
            data_tree = data_tree.split()
            data_tree = ' '.join(data_tree)
            trees.append(data_tree)

            # Dependency tree
            data_dep_tree = [tuple((dep['dep'],
                                    dep['governor'],
                                    dep['governorGloss'],
                                    dep['dependent'],
                                    dep['dependentGloss']))
                             for dep in data['sentences'][0]['basicDependencies']]
            dep_trees.append(data_dep_tree)

    return trees, dep_trees


# Settings

port = 9000

df_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'IBMDebaterEvidenceSentences')

# Load dataset

train_df = pd.read_csv(os.path.join(df_path, 'train.csv'))
test_df = pd.read_csv(os.path.join(df_path, 'test.csv'))

train_text = train_df.candidate.values
test_text = test_df.candidate.values

# CoreNLP parsing

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

train_trees, train_dep_trees = corenlp_parse(texts=train_text, parser=parser)
test_trees, test_dep_trees = corenlp_parse(texts=test_text, parser=parser)

train_df['candidate_tree'] = train_trees
train_df['candidate_dep_tree'] = train_dep_trees

test_df['candidate_tree'] = test_trees
test_df['candidate_dep_tree'] = test_dep_trees

train_df.to_csv(os.path.join(df_path, 'train_mod_dep.csv'))
test_df.to_csv(os.path.join(df_path, 'test_mod_dep.csv'))
