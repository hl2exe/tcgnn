"""

Builds dataset just from .ann files
"""

import os
from tqdm import tqdm
import const_define as cd


def get_annotations(filename, file_path):
    with open(os.path.join(file_path, '{}.ann'.format(filename)), 'r') as f:
        data = f.readlines()

    data = [line for line in data if line.startswith('T')]

    arg_data = []

    for line in data:
        arg_type = line.split('\t')[1].split(' ')[0]
        arg_start_idx = int(line.split('\t')[1].split(' ')[1])
        arg_end_idx = int(line.split('\t')[1].split(' ')[2])
        arg_sentence = line.split('\t')[2:]
        arg_sentence = ' '.join(arg_sentence).strip()
        arg_data.append((arg_start_idx, arg_end_idx, filename.split('.')[0], arg_sentence, arg_type))

    arg_data = sorted(arg_data, key=lambda item: item[0])

    return arg_data


data_path = os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'brat-project-final')
annotations = []

for doc_name in tqdm(os.listdir(data_path), leave=True, position=0):
    if not doc_name.endswith('.txt'):
        continue
    else:
        parsed_doc_name = doc_name.split('.')[0]

        # Get argumentative annotations
        doc_annotations = get_annotations(filename=parsed_doc_name, file_path=data_path)
        annotations.extend(doc_annotations)


print()