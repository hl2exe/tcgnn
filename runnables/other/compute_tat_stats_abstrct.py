"""

Evaluates train and test stats

"""

from utility.json_utils import load_json
import os
import const_define as cd
import numpy as np
from utility.python_utils import flatten, merge
from utility.data_utils import get_data_config_id
from data_converter import DataConverterFactory
import pandas as pd
from utility.cross_validation_utils import build_metrics, compute_iteration_validation_error, update_cv_validation_info
from copy import deepcopy
from tensorflow.keras.utils import to_categorical


def compute_mean_and_std(info, metric):
    values = info[metric]
    values = np.array(values)

    if len(values.shape) == 1:
        values = values[np.newaxis, :]

    per_repetition_mean = np.mean(values, axis=1)
    values_mean = np.mean(per_repetition_mean)
    values_std = np.std(per_repetition_mean)

    return values_mean, values_std


def compute_per_class_mean_and_std(info, metric):
    values = info[metric]
    values = np.array(values)

    if len(values.shape) == 2:
        values = values[np.newaxis, :, :]

    per_repetition_mean = np.mean(values, axis=1)
    values_mean = np.mean(per_repetition_mean, axis=0)
    values_std = np.std(per_repetition_mean, axis=0)

    return values_mean, values_std


prefix = 'abst_rct'
model_type = prefix + "_bert-base-uncased"
test_name = "f1_es_trainable"
metric = 'macro_f1_score'
per_class_metric = 'per_class_f1_score'
dataset_name = 'AbstRCT'
voting_members = -1
majority_threshold = 0.5

model_path = os.path.join(cd.TRAIN_AND_TEST_DIR, model_type, test_name)

validation_info = load_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME))
test_info = load_json(os.path.join(model_path, cd.JSON_TEST_INFO_NAME))

# Compute avg and std values

val_mean, val_std = compute_mean_and_std(validation_info, metric)

print('[Validation - {0}] {1} +/- {2}'.format(metric, val_mean, val_std))

test_mean, test_std = compute_mean_and_std(test_info, metric)

print('[Test - {0}] {1} +/- {2}'.format(metric, test_mean, test_std))

# Per class avg and std values

network_args = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

data_loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
data_loader_info = data_loader_info['configs'][data_loader_info['type']]

config_args = {key: arg['value']
               for key, arg in network_args.items()
               if 'processor' in arg['flags']
               or 'tokenizer' in arg['flags']
               or 'converter' in arg['flags']
               or 'data_loader' in arg['flags']}
config_args = flatten(config_args)
config_args = merge(config_args, data_loader_info)
config_args_tuple = [(key, value) for key, value in config_args.items()]
config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
info_base_path = os.path.join(cd.TESTS_DATA_DIR,
                              dataset_name,
                              model_type)
config_id = get_data_config_id(filepath=info_base_path, config=config_name)
info_path = os.path.join(info_base_path, str(config_id))

converter_type = cd.MODEL_CONFIG[model_type]['converter']
converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
    filepath=info_path,
    prefix=None)

label_map = converter_info['label_map']

print('Label map: ', label_map)

val_pc_mean, val_pc_std = compute_per_class_mean_and_std(validation_info, per_class_metric)

# print('[Validation per class - {0}] {1} +/- {2}'.format(metric, val_pc_mean, val_pc_std))

test_pc_mean, test_pc_std = compute_per_class_mean_and_std(test_info, per_class_metric)

# print('[Test per class - {0}] {1} +/- {2}'.format(metric, test_pc_mean, test_pc_std))

for key, value in label_map.items():
    key_index = np.argmax(value)
    print('Validation - {0} - {1}: {2} +/- {3}'.format(per_class_metric,
                                                       key,
                                                       val_pc_mean[key_index],
                                                       val_pc_std[key_index]))

for key, value in label_map.items():
    key_index = np.argmax(value)
    print('Test - {0} - {1}: {2} +/- {3}'.format(per_class_metric,
                                                 key,
                                                 test_pc_mean[key_index],
                                                 test_pc_std[key_index]))

# Compute per test performance
df_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'AbstRCT_corpus', 'dataset_final.csv')
df = pd.read_csv(df_path)
df_test = df[df.Split == 'test']
df_test = df_test.set_index(np.arange(df_test.shape[0]))

# Metrics
train_and_test_config = load_json(os.path.join(model_path, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME))
parsed_metrics = build_metrics(error_metrics=train_and_test_config['error_metrics'])

test_predictions = load_json(os.path.join(model_path, cd.JSON_TEST_PREDICTIONS_NAME))
test_truth = load_json(os.path.join(model_path, 'y_test.json'))

val_info_path = os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME)

val_info = load_json(val_info_path)

for folder in ['glaucoma_test', 'mixed_test', 'neoplasm_test']:
    folder_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'AbstRCT_corpus', 'data', 'test', folder)
    ids = [item.split('.ann')[0] for item in os.listdir(folder_path) if item.endswith('.ann')]

    test_indexes = df_test[df_test.ID.isin(ids)].index.values

    avg_metrics = {}

    all_folder_predictions = []
    majority_truth = None

    for repetition in range(len(test_predictions)):
        folder_predictions = test_predictions[str(repetition)][0]
        folder_predictions = folder_predictions.reshape(test_truth.shape)
        folder_predictions = folder_predictions[test_indexes]
        folder_truth = test_truth[test_indexes]
        majority_truth = folder_truth

        all_folder_predictions.append(folder_predictions)

        folder_metrics = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                            true_values=folder_truth,
                                                            predicted_values=folder_predictions,
                                                            error_metrics_additional_info=train_and_test_config[
                                                                'error_metrics_additional_info'],
                                                            error_metrics_nicknames=train_and_test_config[
                                                                'error_metrics_nicknames'])
        avg_metrics = update_cv_validation_info(avg_metrics, folder_metrics)

        # print("Test set -> ", folder)
        # print("Metrics -> ", folder_metrics)

    avg_metrics = {key: np.mean(value, axis=0) for key, value in avg_metrics.items()}
    print("Average metrics {0} -> ".format(folder), avg_metrics)

    # Majority Voting

    if len(test_predictions) > 1:

        if voting_members < 0:
            voting_members = len(test_predictions)

        # print("\n Majority Voting with {} members".format(voting_members))

        # [samples, members, classes]
        preds_majority = np.stack(all_folder_predictions, axis=1)
        preds_majority[preds_majority >= majority_threshold] = 1.0
        preds_majority[preds_majority < majority_threshold] = 0.0

        # [samples, members]
        preds_majority = np.argmax(preds_majority, axis=-1)
        preds_majority = np.sum(preds_majority, axis=-1)

        final_majority = deepcopy(preds_majority)
        final_majority[preds_majority >= voting_members / 2] = 1.0
        final_majority[preds_majority < voting_members / 2] = 0.0
        final_majority = to_categorical(final_majority, majority_truth.shape[-1])

        majority_info = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                           true_values=majority_truth,
                                                           predicted_values=final_majority,
                                                           error_metrics_additional_info=train_and_test_config[
                                                               'error_metrics_additional_info'],
                                                           error_metrics_nicknames=train_and_test_config[
                                                               'error_metrics_nicknames'])

        # print("Majority metrics {0} -> ".format(folder), majority_info)
    else:
        print('Skipping majority metrics since more than 1 repetition is required...')
