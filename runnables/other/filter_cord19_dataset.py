"""

Filters CORD-19 built dataset

"""

import os
import const_define as cd
import pandas as pd
from nltk import word_tokenize
import numpy as np
from collections import Counter
import re

dataset_name = 'dataset_chunk_15'
data_path = os.path.join(cd.CORD19_DIR, '{}.csv'.format(dataset_name))

data = pd.read_csv(data_path)

print('Initial shape: ', data.shape)

# Drop nans
data = data.dropna()

print('Nan filtering shape: ', data.shape)

# Drop invalid trees
data = data[data.Tree.str.startswith('(ROOT', na=False)]

# Drop trees with no VP
data = data[data.Tree.str.contains('VP')]

print('Invalid trees shape: ', data.shape)

# Drop numeric sections
non_numeric_section_ids = []
for item_id, item in enumerate(data.Section.values):
    try:
        float(item)
    except:
        non_numeric_section_ids.append(item_id)
        continue

data = data.iloc[non_numeric_section_ids]

print('Numeric sections shape: ', data.shape)

# Drop short sentences
minimum_length_excluded = 5

lengths = [len(word_tokenize(item)) for item in data.Sentence.values]
lengths = np.array(lengths)

data = data.iloc[np.where(lengths > minimum_length_excluded)[0]]

print('Short sentences shape: ', data.shape)

# Filter sections

top_K = 100

data_sections = data.Section.values
counter = Counter(data_sections)
top_K_sections = counter.most_common(n=top_K)
top_K_sections = [item[0] for item in top_K_sections]

sections_to_filter = ['figure',
                      'table',
                      'acknowledgement',
                      'list of figures',
                      'appendix',
                      'supplementary material',
                      'funding'
                      ]

top_K_filter = np.where(np.isin(data_sections, top_K_sections))[0]
avoid_filter = [idx for idx, section in enumerate(data_sections) if
                all([sec_filter.lower() not in section.lower()
                     for sec_filter in sections_to_filter])]
avoid_filter = np.array(avoid_filter)

sections_filter = set(top_K_filter).intersection(set(avoid_filter))
sections_filter = sorted(list(sections_filter))

data = data.iloc[sections_filter]

print('Section filter shape: ', data.shape)

# Replacing http(s) links

pattern = re.compile(r"(http?s://\S+)")

parsed_sentences = []
parsed_trees = []

for sentence, tree in zip(data.Sentence.values, data.Tree.values):
    filtered_sentence = pattern.sub("REF", sentence)
    filtered_tree = ' '.join([word if not word.startswith('http')
                              else "REF" + word[word.find(')'):]
                              for word in tree.split()])
    parsed_sentences.append(filtered_sentence)
    parsed_trees.append(filtered_tree)

data['Sentence'] = parsed_sentences
data['Tree'] = parsed_trees

save_path = os.path.join(cd.CORD19_DIR, '{}_filtered.csv'.format(dataset_name))
data.to_csv(save_path, index=False)
