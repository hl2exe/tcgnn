"""

Given some tree, evaluate tree constraint distribution when considering several nodes combinations


TODO: compare softmax- and sigmoid-based approaches

"""

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

# import tensorflow as tf
#
# # Limiting GPU access
# gpus = tf.config.experimental.list_physical_devices('GPU')
# if gpus:
#     try:
#         for gpu in gpus:
#             tf.config.experimental.set_memory_growth(gpu, True)
#     except RuntimeError as e:
#         print(e)

from utility.graph_utils import retrieve_hierarchical_tree_adj_info
from utility.draw_graph_utils import draw_tree

# Step 0: settings

use_sigmoid = True
alpha = 2.
epsilon = 1e-1

# Step 1: get tree info

print('[Step 1] Retrieving tree info...')

tree_text = "(ROOT (S (NP (NNP Marom)) (VP (VBD stated) (SBAR (IN that) (S (NP (DT the) (NNP IDF)) (VP (VP (VBD had)" \
            " (NP (NP (DT no) (NN intention)) (PP (IN of) (S (VP (VBG harming) (NP (DT any) (NN flotilla)" \
            " (NNS passengers))))))) (CC and) (VP (MD would) (VP (VB act) (ADVP (RB professionally)))))))) (. .)))"
nodes, _, adjacency_matrix, subtree_nodes = retrieve_hierarchical_tree_adj_info(tree_text=tree_text, kernel='stk')
node_indices = np.arange(len(nodes))


def build_node_spans(nodes, subtree_nodes):
    node_spans = np.zeros((len(nodes), len(nodes)))

    for subtree in subtree_nodes:
        node_spans[subtree[0], subtree] = 1.

    for node in nodes:
        node_spans[node, node] = 1.

    node_spans[0] = np.ones_like(node_spans[0])

    return node_spans


node_spans = build_node_spans(nodes=node_indices, subtree_nodes=subtree_nodes)

print('[Step 1] Completed!')


# Step 2: mock pooling

def softmax(x, axis=-1):
    return np.exp(x) / np.sum(np.exp(x), axis=axis, keepdims=True)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def mock_pooling(nodes, K, use_sigmoid=False):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    att_operator = sigmoid if use_sigmoid else softmax

    for node_idx in np.arange(len(nodes)):
        # hard_pooling = np.random.randint(low=0, high=K)
        # hard_encoding = np.zeros(K)
        # hard_encoding[hard_pooling] = 1.0
        pooling_matrix[node_idx] = att_operator(
            np.random.rand(K) * np.random.randint(0, 10) * np.random.choice([-1., 1.], size=1))
        # pooling_matrix[node_idx] = hard_encoding

    return pooling_matrix


def mock_root_pooling(nodes, K):
    pooling_matrix = np.zeros((K, len(nodes)), dtype=np.float32)
    for k in range(K):
        hot_encoding = np.zeros(len(nodes))
        rand_pos = np.random.randint(low=0, high=len(nodes))
        hot_encoding[rand_pos] = 1.
        pooling_matrix[k] = hot_encoding

    return pooling_matrix


def mock_soft_root_pooling(nodes, K):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    for node_idx in np.arange(len(nodes)):
        pooling_matrix[node_idx] = np.random.rand(K) * 5

    for k in range(K):
        pooling_matrix[:, k] = softmax(pooling_matrix[:, k])

    return pooling_matrix.transpose()


def auto_root_pooling(node_pooling, adjacency_matrix):
    # Node pooling -> N x K

    # Mask: 1 x N x N
    mask = np.tril(np.ones(adjacency_matrix.shape, dtype=node_pooling.dtype))
    mask -= np.eye(adjacency_matrix.shape[0], dtype=node_pooling.dtype)
    mask = mask.astype(np.float32)
    mask = mask[np.newaxis, :, :]

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    # adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # K x N
    tree_pooling = np.sum(exp_node_pooling * mask * adjacency_matrix * exp_node_pooling.transpose([0, 2, 1]), axis=-1)

    # Normalize by connectivity (use this in case of a graph)
    # connectivity_norm = np.sum(mask * adjacency_matrix, axis=-1)
    # connectivity_norm = np.maximum(connectivity_norm, 1.)
    # tree_pooling = tree_pooling / connectivity_norm
    # tree_pooling = tree_pooling / np.sum(tree_pooling, axis=1)[:, np.newaxis]
    tree_pooling = 1. - tree_pooling

    tree_pooling = tree_pooling * node_pooling.transpose()

    # Normalize
    tree_norm = np.sum(tree_pooling, axis=-1)
    tree_pooling = tree_pooling / tree_norm[:, np.newaxis]

    return tree_pooling


def lin_auto_root_pooling(node_pooling, adjacency_matrix):
    # Node pooling -> N x K

    # Mask: 1 x N x N
    mask = np.tril(np.ones(adjacency_matrix.shape, dtype=node_pooling.dtype))
    mask -= np.eye(adjacency_matrix.shape[0], dtype=node_pooling.dtype)
    mask = mask.astype(np.float32)

    # K x N
    tree_pooling = np.matmul(node_pooling.transpose(), adjacency_matrix * mask.transpose())

    # Normalize by connectivity (use this in case of a graph)
    # connectivity_norm = np.sum(mask * adjacency_matrix, axis=-1)
    # connectivity_norm = np.maximum(connectivity_norm, 1.)
    # tree_pooling = tree_pooling / connectivity_norm
    # tree_pooling = tree_pooling / np.sum(tree_pooling, axis=1)[:, np.newaxis]
    tree_pooling = 1. - tree_pooling

    tree_pooling = tree_pooling * node_pooling.transpose()

    # Mask leaves
    leaf_mask = np.minimum(np.sum(adjacency_matrix, axis=-1) - 1., 1.)
    tree_pooling *= leaf_mask[np.newaxis, :]

    # Normalize
    tree_norm = np.sum(tree_pooling, axis=-1)
    tree_pooling = tree_pooling / tree_norm[:, np.newaxis]

    return tree_pooling


def auto_root_pooling_alt(node_pooling, adjacency_matrix):
    # Node pooling -> N x K

    # Mask: 1 x N x N
    mask = np.tril(np.ones(adjacency_matrix.shape, dtype=node_pooling.dtype))
    mask -= np.eye(adjacency_matrix.shape[0], dtype=node_pooling.dtype)
    mask = mask.astype(np.float32)
    mask = mask[np.newaxis, :, :]

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling >= 1e-3
    exp_node_pooling = exp_node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    # adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # K x N
    tree_pooling = np.sum(exp_node_pooling * mask * adjacency_matrix * exp_node_pooling.transpose([0, 2, 1]), axis=-1)

    # Normalize by connectivity (use this in case of a graph)
    # connectivity_norm = np.sum(mask * adjacency_matrix, axis=-1)
    # connectivity_norm = np.maximum(connectivity_norm, 1.)
    # tree_pooling = tree_pooling / connectivity_norm
    # tree_pooling = tree_pooling / np.sum(tree_pooling, axis=1)[:, np.newaxis]
    tree_pooling = 1. - tree_pooling

    tree_pooling = tree_pooling * node_pooling.transpose()

    # Normalize
    tree_norm = np.sum(tree_pooling, axis=-1)
    tree_pooling = tree_pooling / tree_norm[:, np.newaxis]

    return tree_pooling


# Step 3: define constraints (the lower the better)

# Tree constraint

def tree_constraint(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> N x K

    root_pooling = np.transpose(root_pooling)

    # This is a little better than just normalizing by np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    pooling_mask = node_pooling >= 1e-3
    pooling_mask = pooling_mask.astype(np.float32)
    normalization = pooling_mask * np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # N x K
    span_pooling = np.matmul(1. - node_spans, node_pooling) / normalization

    # K x 1
    weighted_span_pooling = np.sum(root_pooling * span_pooling, axis=0)

    # K x 1
    return weighted_span_pooling


def tree_constraint_alt(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> K x N

    meta_node_amount = node_pooling.shape[1]

    # N x K
    numerator = np.matmul(1 - node_spans, node_pooling)

    # mask = node_pooling >= 1e-3
    # mask = mask.astype(np.float)

    # N x K
    # denominator = np.matmul(1 - node_spans, mask)
    # denominator = np.maximum(denominator, 1.)
    denominator = np.sum(node_pooling, axis=0)

    # K x K
    constraint = numerator / denominator
    constraint = np.matmul(root_pooling, constraint)

    # K x 1
    constraint = np.sum(constraint * np.eye(meta_node_amount), axis=-1)

    return constraint


def mod_tree_constraint(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> K x N

    meta_node_amount = node_pooling.shape[1]

    # K x N
    compl_root_pooling = 1. - root_pooling
    adjusted_node_pooling = node_pooling * compl_root_pooling.transpose()

    # N x K
    numerator = np.matmul(1 - node_spans, adjusted_node_pooling)

    # mask = node_pooling >= 1e-3
    # mask = mask.astype(np.float)

    # N x K
    # denominator = np.matmul(1 - node_spans, mask)
    # denominator = np.maximum(denominator, 1.)
    denominator = np.sum(node_pooling, axis=0)

    # K x K
    constraint = numerator / denominator
    constraint = np.matmul(root_pooling, constraint)

    # K x 1
    constraint = np.sum(constraint * np.eye(meta_node_amount), axis=-1)

    return constraint


def mod_tree_constraint_naive(node_pooling, root_pooling, node_spans):
    tree_plus = np.zeros((node_pooling.shape[1]))
    for k in range(node_pooling.shape[1]):
        for i in range(node_pooling.shape[0]):
            total_sum = 0.
            total_norm = 0.
            for j in range(node_pooling.shape[0]):
                total_sum += node_pooling[j, k] * (1. - node_spans[i, j]) * (1. - root_pooling[k, j])
                total_norm += float(node_pooling[j, k] >= 1e-3) * (1 - node_spans[i, j])
            total_norm = np.maximum(total_norm, 1.)
            total_sum = total_sum / total_norm
            total_sum *= root_pooling[k, i]
            tree_plus[k] += total_sum

    return tree_plus


def log_tree_constraint(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> N x K

    root_pooling = np.transpose(root_pooling)

    # This is a little better than just normalizing by np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    pooling_mask = node_pooling >= 1e-3
    pooling_mask = pooling_mask.astype(np.float32)
    normalization = np.matmul(1 - node_spans, pooling_mask)
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # N x K
    span_pooling = np.matmul(1. - node_spans, node_pooling) / normalization
    span_pooling = 1. - span_pooling
    span_pooling = - np.log(span_pooling + 1e-7)
    span_pooling = np.minimum(span_pooling, 40.)

    # K x 1
    weighted_span_pooling = np.sum(root_pooling * span_pooling, axis=0)

    # K x 1
    return weighted_span_pooling


def log_tree_constraint_alt(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> K x N

    meta_node_amount = node_pooling.shape[1]

    # N x K
    numerator = np.matmul(1 - node_spans, node_pooling)

    # mask = node_pooling >= 1e-3
    # mask = mask.astype(np.float)

    # N x K
    # denominator = np.matmul(1 - node_spans, mask)
    # denominator = np.maximum(denominator, 1.)

    # 1 x K
    denominator = np.sum(node_pooling, axis=0)
    denominator = denominator[np.newaxis, :]

    # N x K
    constraint = numerator / denominator
    constraint = 1. - constraint
    constraint = - np.log(constraint + 1e-7)
    constraint = np.minimum(constraint, 40.)

    # K x K
    constraint = np.matmul(root_pooling, constraint)

    # K x 1
    constraint = np.sum(constraint * np.eye(meta_node_amount), axis=-1)

    return constraint


def log_mod_tree_constraint_alt(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> K x N

    meta_node_amount = node_pooling.shape[1]

    # K x N
    compl_root_pooling = 1. - root_pooling
    adjusted_node_pooling = node_pooling * compl_root_pooling.transpose()

    # N x K
    numerator = np.matmul(1 - node_spans, adjusted_node_pooling)

    # mask = node_pooling >= 1e-3
    # mask = mask.astype(np.float)

    # N x K
    # denominator = np.matmul(1 - node_spans, mask)
    # denominator = np.maximum(denominator, 1.)
    denominator = np.sum(node_pooling, axis=0)

    # N x K
    constraint = numerator / denominator
    constraint = 1. - constraint
    constraint = - np.log(constraint + 1e-7)
    constraint = np.minimum(constraint, 40.)

    # K x K
    constraint = np.matmul(root_pooling, constraint)

    # K x 1
    constraint = np.sum(constraint * np.eye(meta_node_amount), axis=-1)

    return constraint


# Contiguous sequence constraint


def contiguous_sequence_constraint(node_pooling, adjacency_matrix):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N

    # K x N x N
    node_pooling = node_pooling.transpose()
    node_pooling = node_pooling[:, :, np.newaxis]
    node_pooling = np.repeat(node_pooling, axis=-1, repeats=node_pooling.shape[1])
    node_pooling = np.transpose(node_pooling, [0, 2, 1])

    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=node_pooling.shape[0])

    horizontal_sim = node_pooling * adjacency_matrix
    vertical_sim = node_pooling * np.transpose(horizontal_sim, [0, 2, 1])

    # Count pooled nodes only
    # N x K
    mask = vertical_sim >= 1e-3
    mask = mask.astype(vertical_sim.dtype)

    # Reduce
    vertical_sim = np.sum(vertical_sim, axis=-1)
    vertical_sim = np.sum(vertical_sim, axis=-1)

    # K x 1
    mask = np.sum(mask, axis=-1)
    mask = np.sum(mask, axis=-1)

    return 1. - vertical_sim / mask


def contiguous_sequence_constraint_alt(node_pooling, adjacency_matrix):
    # N x N
    adjacency_eye = np.eye(adjacency_matrix.shape[0])
    partial_adjacency = adjacency_matrix - adjacency_eye * adjacency_matrix
    partial_adjacency = partial_adjacency / 2

    # K x N
    constraint = np.matmul(node_pooling.transpose(), partial_adjacency)
    # constraint = np.matmul(constraint, mask)

    # K x K
    constraint = np.matmul(constraint, node_pooling)

    # K x 1 (take only diagonal values, other values are all possible k-k combinations (k1k2, k2k1, etc..)
    constraint = np.sum(constraint * np.eye(constraint.shape[0]), axis=1)

    # K x 1
    # normalization = node_pooling >= epsilon
    # normalization = normalization.astype(np.float32)
    # normalization = np.sum(normalization, axis=0) - 1.
    # normalization = np.maximum(normalization, np.ones_like(normalization))
    normalization = np.sum(node_pooling, axis=0) - np.max(node_pooling, axis=0)
    # normalization[normalization < 0] = normalization[normalization < 0] + 1
    normalization = np.maximum(normalization, 1e-4)

    # K x 1
    return 1 - (constraint / normalization)


def log_contiguous_sequence_constraint_alt(node_pooling, adjacency_matrix):
    # N x N
    adjacency_eye = np.eye(adjacency_matrix.shape[0])
    partial_adjacency = adjacency_matrix - adjacency_eye * adjacency_matrix
    partial_adjacency = partial_adjacency / 2

    # K x N
    constraint = np.matmul(node_pooling.transpose(), partial_adjacency)
    # constraint = np.matmul(constraint, mask)

    # K x K
    constraint = np.matmul(constraint, node_pooling)

    # K x 1 (take only diagonal values, other values are all possible k-k combinations (k1k2, k2k1, etc..)
    constraint = np.sum(constraint * np.eye(constraint.shape[0]), axis=1)

    # K x 1
    normalization = node_pooling >= 1e-3
    normalization = normalization.astype(np.float32)
    normalization = np.sum(normalization, axis=0) - 1.
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # K x 1
    return - np.log(constraint / normalization)


def mod_contiguous_sequence_constraint_alt(node_pooling, adjacency_matrix):
    # N x N
    adjacency_eye = np.eye(adjacency_matrix.shape[0])
    partial_adjacency = adjacency_matrix - adjacency_eye * adjacency_matrix
    partial_adjacency = partial_adjacency / 2

    # K x N
    constraint = np.matmul(node_pooling.transpose(), partial_adjacency)
    # constraint = np.matmul(constraint, mask)

    # K x K
    constraint = np.matmul(constraint, node_pooling)

    # K x 1 (take only diagonal values, other values are all possible k-k combinations (k1k2, k2k1, etc..)
    constraint = np.sum(constraint * np.eye(constraint.shape[0]), axis=1)

    # K x 1
    normalization = np.sum(node_pooling, axis=0) - 1.
    normalization = np.maximum(normalization, 0.)
    normalization[normalization == 0.] = 1.

    # K x 1
    return 1 - (constraint / normalization)


def contiguous_sequence_constraint_complete(node_pooling, adjacency_matrix, node_spans, root_pooling):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # node_spans -> N x N
    # root_pooling -> K x N

    node_amount = node_spans.shape[1]
    meta_nodes_amount = node_pooling.shape[1]

    # Node spans

    # N x N x N
    node_spans = node_spans[:, :, np.newaxis]
    node_spans = np.repeat(node_spans, axis=-1, repeats=node_amount)
    node_spans = np.transpose(node_spans, [0, 2, 1])

    # Adjacency matrix
    # N x N x N
    adjacency_matrix = np.tile(adjacency_matrix.ravel(), (1, 1, node_amount))
    adjacency_matrix = adjacency_matrix.reshape(node_amount, node_amount, node_amount)

    # N x N x N
    boolean_matrix = np.minimum(2 - (node_spans + node_spans.transpose(0, 2, 1)), 1.)
    c_matrix = adjacency_matrix * boolean_matrix

    # N x N x N
    eye_matrix = np.eye(node_spans.shape[1])
    eye_matrix = np.tile(eye_matrix.ravel(), (1, 1, node_amount))
    eye_matrix = eye_matrix.reshape(node_amount, node_amount, node_amount)

    # N x N x N
    c_matrix = c_matrix - c_matrix * eye_matrix
    c_matrix = 0.5 * c_matrix

    # Node pooling
    # N x N x K
    node_pooling = np.tile(node_pooling.ravel(), (node_amount, 1, 1))
    node_pooling = node_pooling.reshape(node_amount, node_amount, meta_nodes_amount)

    # N x K x N
    per_root_constraint = np.matmul(node_pooling.transpose([0, 2, 1]), c_matrix)

    #  N x K x K
    per_root_constraint = np.matmul(per_root_constraint, node_pooling)

    # Reduce by taking only diagonal values

    # N x K x K
    root_eye_matrix = np.eye(meta_nodes_amount)
    root_eye_matrix = np.tile(root_eye_matrix.ravel(), (node_amount, 1, 1))
    root_eye_matrix = root_eye_matrix.reshape(node_amount, meta_nodes_amount, meta_nodes_amount)

    # N x K
    per_root_constraint = np.sum(per_root_constraint * root_eye_matrix, axis=-1)

    # K x 1
    constraint = np.sum(root_pooling.transpose() * per_root_constraint, axis=0)

    # Normalization factor
    mask_pooling = node_pooling >= epsilon
    mask_pooling = mask_pooling.astype(np.float)

    # N x K x K
    normalization = np.matmul(mask_pooling.transpose(0, 2, 1), c_matrix)
    normalization = np.matmul(normalization, mask_pooling)

    # Reduce by taking only diagonal values

    # N x K
    normalization = np.sum(normalization * root_eye_matrix, axis=-1)

    # K x 1
    normalization = np.sum(root_pooling.transpose() * normalization, axis=0)
    normalization = np.maximum(normalization, np.ones_like(normalization))

    return constraint / normalization
    # return constraint


def parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    # Combined adjacency and span mask

    # N x N x N
    boolean_matrix = adjacency_matrix[np.newaxis, :, :] * (node_spans[:, :, np.newaxis] * node_spans[:, np.newaxis, :])

    # Remove self-references and symmetries
    eye_matrix = np.eye(adjacency_matrix.shape[1])
    eye_matrix = eye_matrix[np.newaxis, :, :]

    # N x N x N
    boolean_matrix = boolean_matrix - boolean_matrix * eye_matrix
    # boolean_matrix = 0.5 * boolean_matrix

    # All pair intensities

    # K x N
    node_pooling = node_pooling.transpose()

    # K x N x N
    pair_intensities = node_pooling[:, :, np.newaxis] * node_pooling[:, np.newaxis, :]

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # Connectivity penalty

    # K x N x N x N
    all_intensities = pair_intensities[:, np.newaxis, :, :] * boolean_matrix[np.newaxis, :, :, :]

    # K x N x N
    all_intensities = np.max(all_intensities, axis=-1)
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)

    # filter out leaves and check node span
    # 1 x 1 x N
    leaf_mask = np.minimum(np.sum(adjacency_matrix, axis=-1) - 1., 1.)
    leaf_mask[0] = 1.0
    leaf_mask = leaf_mask[np.newaxis, np.newaxis, :]

    # K x N x N
    all_intensities = all_intensities * leaf_mask * node_spans[np.newaxis, :, :]

    # Penalty weight

    # K x N x N
    asyn_adjacency_matrix = adjacency_matrix - np.tril(adjacency_matrix)
    j_weight = np.max(node_pooling[:, np.newaxis, np.newaxis, :] * asyn_adjacency_matrix[np.newaxis, np.newaxis, :, :],
                      axis=-1)
    i_weight = node_pooling[:, np.newaxis, :]
    t_weight = root_pooling[:, np.newaxis, :]
    penalty_weight = np.maximum(j_weight, i_weight)
    penalty_weight = np.maximum(penalty_weight, t_weight)

    # Reduction

    # K x N
    constraint = np.sum(all_intensities * penalty_weight, axis=-1)
    # constraint = np.sum(all_intensities, axis=-1)

    # K
    constraint = np.sum(constraint * root_pooling, axis=-1)

    return constraint


# Disconnected nodes


def disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # root_pooling -> K x N

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # General connectivity constraint
    connectivity_penalization = np.sum(exp_node_pooling * adjacency_matrix, axis=-1)

    connectivity_normalization = exp_node_pooling >= epsilon
    connectivity_normalization = connectivity_normalization.astype(np.float32)
    connectivity_normalization = np.sum(connectivity_normalization * adjacency_matrix, axis=-1)
    connectivity_normalization = np.maximum(connectivity_normalization, np.ones_like(connectivity_normalization))

    # K x N
    connectivity_penalization = 1. - connectivity_penalization / connectivity_normalization

    node_pooling = node_pooling.transpose()

    # K x 1
    all_disconnect_constraint = np.sum(node_pooling * connectivity_penalization, axis=-1)
    root_node_residual = np.sum(root_pooling * node_pooling * connectivity_penalization, axis=-1)

    general_disconnect_normalization = node_pooling >= epsilon
    general_disconnect_normalization = general_disconnect_normalization.astype(np.float32)
    general_disconnect_normalization = np.sum(general_disconnect_normalization, axis=-1)
    general_disconnect_normalization = np.maximum(general_disconnect_normalization,
                                                  np.ones_like(general_disconnect_normalization))

    # K x 1
    general_disconnect_constraint = (all_disconnect_constraint - root_node_residual) / general_disconnect_normalization

    # Root node connectivity constraint
    root_node_disconnect_constraint = alpha * root_node_residual

    root_node_disconnect_normalization = node_pooling >= epsilon
    root_node_disconnect_normalization = root_node_disconnect_normalization.astype(np.float32)
    root_node_disconnect_normalization = np.sum(root_node_disconnect_normalization * root_pooling, axis=-1)
    root_node_disconnect_normalization = np.maximum(root_node_disconnect_normalization,
                                                    np.ones_like(root_node_disconnect_normalization))

    # K x 1
    root_node_disconnect_constraint = root_node_disconnect_constraint / root_node_disconnect_normalization

    return general_disconnect_constraint, root_node_disconnect_constraint


def disconnected_nodes_constraint_naive(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    general_disconnect = np.zeros((node_pooling.shape[1]))
    root_disconnect = np.zeros((node_pooling.shape[1]))

    for k in range(node_pooling.shape[1]):
        total_gen_sum = 0.
        total_gen_norm = 0.
        total_root_sum = 0.
        total_root_norm = 0.
        for i in range(node_pooling.shape[0]):
            disconnect_penalization = 0.
            disconnect_norm = 0.
            for j in range(node_pooling.shape[0]):
                if i != j:
                    disconnect_penalization += node_pooling[j, k] * adjacency_matrix[i, j]
                    disconnect_norm += float(node_pooling[j, k] >= 1e-3) * adjacency_matrix[i, j]
            disconnect_norm = np.maximum(disconnect_norm, 1.)
            disconnect_penalization = disconnect_penalization / disconnect_norm

            total_gen_sum += node_pooling[i, k] * (1 - disconnect_penalization) \
                             - root_pooling[k, i] * node_pooling[i, k] * (1 - disconnect_penalization)
            total_gen_norm += float(node_pooling[i, k] >= 1e-3)

            total_root_sum += alpha * root_pooling[k, i] * node_pooling[i, k] * (1 - disconnect_penalization)
            total_root_norm += root_pooling[k, i] * float(node_pooling[i, k] >= 1e-3)

        total_gen_norm = np.maximum(total_gen_norm, 1.)
        general_disconnect[k] = total_gen_sum / total_gen_norm

        total_root_norm = np.maximum(total_root_norm, 1.)
        root_disconnect[k] = total_root_sum / total_root_norm

    return general_disconnect, root_disconnect


def unified_disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # root_pooling -> K x N

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # General connectivity constraint
    connectivity_penalization = np.sum(exp_node_pooling * adjacency_matrix, axis=-1)

    connectivity_normalization = exp_node_pooling >= epsilon
    connectivity_normalization = connectivity_normalization.astype(np.float32)
    connectivity_normalization = np.sum(connectivity_normalization * adjacency_matrix, axis=-1)
    connectivity_normalization = np.maximum(connectivity_normalization, np.ones_like(connectivity_normalization))

    # K x N
    connectivity_penalization = 1. - connectivity_penalization / connectivity_normalization

    node_pooling = node_pooling.transpose()

    # K x 1
    unified_disconnect_coefficient = 1 + (alpha - 1) * root_pooling
    unified_disconnect_constraint = np.sum(unified_disconnect_coefficient * node_pooling * connectivity_penalization,
                                           axis=-1)

    # general_disconnect_normalization = node_pooling >= 1e-3
    # general_disconnect_normalization = general_disconnect_normalization.astype(np.float32)
    # general_disconnect_normalization = np.sum(general_disconnect_normalization, axis=-1)
    # general_disconnect_normalization = np.maximum(general_disconnect_normalization,
    #                                               np.ones_like(general_disconnect_normalization))
    general_disconnect_normalization = np.sum(node_pooling, axis=-1)

    unified_disconnect_constraint = unified_disconnect_constraint / general_disconnect_normalization

    return unified_disconnect_constraint


# Sparsity

def sparsity_constraint(root_pooling):
    # K
    sparsity = - np.sum(np.log(root_pooling + 1e-7) * root_pooling, axis=1)
    return sparsity


# Separation

def cluster_separation_constraint(node_pooling):
    # K x K
    cluster_overlap = np.matmul(node_pooling.transpose(), node_pooling)
    cluster_overlap = 0.5 * (cluster_overlap - np.eye(cluster_overlap.shape[-1]) * cluster_overlap)

    # K x K
    normalization = np.sum(node_pooling, axis=0)
    normalization = np.minimum(normalization[:, np.newaxis], normalization[np.newaxis, :])

    # []
    constraint = np.sum(cluster_overlap / normalization)
    pairs_amount = node_pooling.shape[-1] * (node_pooling.shape[-1] - 1) / 2
    pairs_amount = np.maximum(pairs_amount, 1.)
    constraint = constraint / pairs_amount

    return constraint


# Step 4: run some experiments
K = 3
iterations = 1000

print('[Step 2] Running experiments...')

exp_tree_constraint = []
exp_contiguous_sequence_constraint = []
exp_contiguous_sequence_complete_constraint = []
exp_parametric_contiguous_sequence_constraint = []
exp_general_disconnect_constraint = []
exp_root_node_disconnect_constraint = []
exp_unified_disconnect_constraint = []
exp_root_sparsity_constraint = []
exp_cluster_separation_constraint = []

exp_subtrees = np.zeros((iterations * K, len(nodes)))
for it in tqdm(range(iterations)):
    node_pooling = mock_pooling(nodes, K, use_sigmoid=use_sigmoid)
    # root_pooling = mock_root_pooling(nodes, K)
    root_pooling = lin_auto_root_pooling(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)

    exp_tree_constraint.extend(tree_constraint_alt(node_pooling=node_pooling, root_pooling=root_pooling,
                                                   node_spans=node_spans))
    exp_contiguous_sequence_constraint.extend(contiguous_sequence_constraint_alt(node_pooling=node_pooling,
                                                                                 adjacency_matrix=adjacency_matrix))

    exp_contiguous_sequence_complete_constraint.extend(
        contiguous_sequence_constraint_complete(node_pooling=node_pooling,
                                                adjacency_matrix=adjacency_matrix,
                                                root_pooling=root_pooling,
                                                node_spans=node_spans))

    exp_parametric_contiguous_sequence_constraint.extend(
        parametric_contiguous_constraint(node_pooling=node_pooling,
                                         adjacency_matrix=adjacency_matrix,
                                         root_pooling=root_pooling,
                                         node_spans=node_spans,
                                         alpha=0.5))

    general_disconnect_constraint, \
    root_node_disconnect_constraint = disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                    root_pooling=root_pooling,
                                                                    adjacency_matrix=adjacency_matrix,
                                                                    alpha=alpha)

    unified_disconnect_constraint = unified_disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                          root_pooling=root_pooling,
                                                                          adjacency_matrix=adjacency_matrix,
                                                                          alpha=alpha)

    exp_general_disconnect_constraint.extend(general_disconnect_constraint)
    exp_root_node_disconnect_constraint.extend(root_node_disconnect_constraint)
    exp_unified_disconnect_constraint.extend(unified_disconnect_constraint)

    exp_root_sparsity_constraint.extend(sparsity_constraint(root_pooling))

    exp_cluster_separation_constraint.append(cluster_separation_constraint(node_pooling))

    exp_subtrees[it * K: it * K + K] = node_pooling.transpose()

exp_tree_constraint = np.array(exp_tree_constraint)
exp_contiguous_sequence_constraint = np.array(exp_contiguous_sequence_constraint)
exp_contiguous_sequence_complete_constraint = np.array(exp_contiguous_sequence_complete_constraint)
exp_parametric_contiguous_sequence_constraint = np.array(exp_parametric_contiguous_sequence_constraint)
exp_general_disconnect_constraint = np.array(exp_general_disconnect_constraint)
exp_root_node_disconnect_constraint = np.array(exp_root_node_disconnect_constraint)
exp_unified_disconnect_constraint = np.array(exp_unified_disconnect_constraint)
exp_root_sparsity_constraint = np.array(exp_root_sparsity_constraint)
exp_cluster_separation_constraint = np.array(exp_cluster_separation_constraint)

print('[Step 2] Completed!')
print('[Step 3] Assessing constraints on real subtrees...')

# transform real subtrees
added_subtrees = np.zeros((len(subtree_nodes), len(nodes)))
added_tree_constraint = np.zeros(len(subtree_nodes))
added_contiguous_sequence_constraint = np.zeros(len(subtree_nodes))
added_contiguous_sequence_complete_constraint = np.zeros(len(subtree_nodes))
added_parametric_contiguous_sequence_constraint = np.zeros(len(subtree_nodes))
added_general_disconnect_constraint = np.zeros(len(subtree_nodes))
added_root_node_disconnect_constraint = np.zeros(len(subtree_nodes))
added_unified_node_disconnect_constraint = np.zeros(len(subtree_nodes))
added_cluster_separation_constraint = np.zeros(len(subtree_nodes))
for idx, subtree in enumerate(tqdm(subtree_nodes)):
    subtree_encoding = np.zeros(len(nodes))
    subtree_encoding[subtree] = 1.
    added_subtrees[idx] = subtree_encoding

    # subtree_root_pooling = np.zeros(len(nodes))
    # subtree_root_pooling[subtree[0]] = 1.
    # subtree_root_pooling = subtree_root_pooling[np.newaxis, :]

    subtree_pooling = subtree_encoding[:, np.newaxis]
    subtree_root_pooling = lin_auto_root_pooling(node_pooling=subtree_pooling, adjacency_matrix=adjacency_matrix)

    added_tree_constraint[idx] = tree_constraint_alt(node_pooling=subtree_pooling,
                                                     root_pooling=subtree_root_pooling,
                                                     node_spans=node_spans)[0]
    added_contiguous_sequence_constraint[idx] = contiguous_sequence_constraint_alt(node_pooling=subtree_pooling,
                                                                                   adjacency_matrix=adjacency_matrix)[
        0]

    added_contiguous_sequence_complete_constraint[idx] = \
        contiguous_sequence_constraint_complete(node_pooling=subtree_pooling,
                                                root_pooling=subtree_root_pooling,
                                                node_spans=node_spans,
                                                adjacency_matrix=adjacency_matrix)[0]

    added_parametric_contiguous_sequence_constraint[idx] = \
    parametric_contiguous_constraint(node_pooling=subtree_pooling,
                                     root_pooling=subtree_root_pooling,
                                     node_spans=node_spans,
                                     adjacency_matrix=adjacency_matrix,
                                     alpha=0.5)[0]

    general_disconnect_constraint, \
    root_node_disconnect_constraint = disconnected_nodes_constraint(node_pooling=subtree_pooling,
                                                                    root_pooling=subtree_root_pooling,
                                                                    adjacency_matrix=adjacency_matrix,
                                                                    alpha=alpha)

    unified_disconnect_constraint = unified_disconnected_nodes_constraint(node_pooling=subtree_pooling,
                                                                          root_pooling=subtree_root_pooling,
                                                                          adjacency_matrix=adjacency_matrix,
                                                                          alpha=alpha)

    added_general_disconnect_constraint[idx] = general_disconnect_constraint[0]
    added_root_node_disconnect_constraint[idx] = root_node_disconnect_constraint[0]
    added_unified_node_disconnect_constraint[idx] = unified_disconnect_constraint[0]

    added_cluster_separation_constraint[idx] = cluster_separation_constraint(subtree_pooling)

print('[Step 3] Completed!')

# Step 5: plotting
# X-axis: node pooling combination
# Y-axis: constraint score
# Highlight valid subtrees

print('[Step 4] Plotting results...')

# plt.scatter(x=np.arange(iterations * K), y=exp_tree_constraint * exp_root_sparsity_constraint, c='green', marker='o')
# plt.scatter(x=np.arange(added_tree_constraint.shape[0]) + iterations * K, y=added_tree_constraint, c='red', marker='x')
# plt.legend(['Generated - Entropy', 'Valid'])
# plt.title("Tree constraint (the lower the better)")
# plt.show()

fig, ax = plt.subplots(1, 1)
ax.set_title("Tree constraint (the lower the better)")
ax.scatter(x=np.arange(iterations * K), y=exp_tree_constraint, c='green', marker='o')
ax.scatter(x=np.arange(added_tree_constraint.shape[0]) + iterations * K, y=added_tree_constraint, c='red', marker='x')
ax.legend(['Generated', 'Valid'])


fig, ax = plt.subplots(1, 1)
ax.scatter(x=np.arange(iterations * K), y=exp_parametric_contiguous_sequence_constraint, c='green', marker='o')
ax.scatter(x=np.arange(added_parametric_contiguous_sequence_constraint.shape[0]) + iterations * K,
            y=added_parametric_contiguous_sequence_constraint, c='red', marker='x')
ax.legend(['Generated', 'Valid'])
ax.set_title("Parametric contiguous sequence constraint (the lower the better)")

fig, ax = plt.subplots(1, 1)
ax.scatter(x=np.arange(iterations * K),
           y=exp_contiguous_sequence_constraint + exp_contiguous_sequence_complete_constraint, c='green', marker='o')
ax.scatter(x=np.arange(added_contiguous_sequence_complete_constraint.shape[0]) + iterations * K,
           y=added_contiguous_sequence_complete_constraint + added_contiguous_sequence_constraint, c='red', marker='x')
ax.legend(['Generated', 'Valid'])
ax.set_title("Complete contiguous sequence constraint (the lower the better)")

# #
# plt.scatter(x=np.arange(iterations * K), y=exp_general_disconnect_constraint, c='green', marker='o')
# plt.scatter(x=np.arange(added_general_disconnect_constraint.shape[0]) + iterations * K,
#             y=added_general_disconnect_constraint, c='red', marker='x')
# plt.legend(['Generated', 'Valid'])
# plt.title("General disconnect constraint (the lower the better)")
# plt.show()
#
# plt.scatter(x=np.arange(iterations * K), y=exp_root_node_disconnect_constraint, c='green', marker='o')
# plt.scatter(x=np.arange(added_root_node_disconnect_constraint.shape[0]) + iterations * K,
#             y=added_root_node_disconnect_constraint, c='red', marker='x')
# plt.legend(['Generated', 'Valid'])
# plt.title("Root node disconnect constraint (the lower the better, alpha = {})".format(alpha))
# plt.show()
#
#
# fig, ax = plt.subplots(1, 1)
# ax.scatter(x=np.arange(iterations * K), y=exp_unified_disconnect_constraint, c='green', marker='o')
# ax.scatter(x=np.arange(added_unified_node_disconnect_constraint.shape[0]) + iterations * K,
#            y=added_unified_node_disconnect_constraint, c='red', marker='x')
# ax.legend(['Generated', 'Valid'])
# ax.set_title("Unified disconnect constraint (the lower the better, alpha = {})".format(alpha))
# plt.show()

fig, ax = plt.subplots(1, 1)
ax.scatter(x=np.arange(iterations),
           y=exp_cluster_separation_constraint, c='green', marker='o')
ax.scatter(x=np.arange(added_contiguous_sequence_complete_constraint.shape[0]) + iterations,
           y=added_cluster_separation_constraint, c='red', marker='x')
ax.legend(['Generated', 'Valid'])
ax.set_title("Cluster separation constraint (the lower the better)")
plt.show()

print('[Step 4] Completed!')

# Step 6: ranking (debug)

print('[Step 5] Showing some experimental rankings for debug...')

L = 10

# sorted_tree_constraint_indexes = np.argsort(exp_tree_constraint)
# sorted_sequence_constraint_indexes = np.argsort(exp_contiguous_sequence_constraint)

# print('Top ', L, ' experimental sequences based on tree constraint:')
# sorted_tree_constraint_poolings = exp_subtrees[sorted_tree_constraint_indexes]
# for item in sorted_tree_constraint_poolings[:L, :]:
#     print('Item: {0} -- Is valid: {1}'.format(item, item in added_subtrees))
#
# print('Top ', L, ' experimental sequences based on sequence constraint:')
# sorted_sequence_constraint_poolings = exp_subtrees[sorted_sequence_constraint_indexes]
# for item in sorted_sequence_constraint_poolings[:L, :]:
#     print('Item: {0} -- Is valid: {1}'.format(item, item in added_subtrees))

print('[Step 5] Completed!')

print('[Step 6] Showing incremental configuration scoring...')


def random_subtree_incremental_test(added_subtrees, node_spans, adjacency_matrix, outliers=0):
    random_subtree_idx = np.random.randint(low=0, high=len(added_subtrees))
    random_subtree = added_subtrees[random_subtree_idx]

    incremental_config = np.zeros_like(random_subtree)
    subtree_slots = np.where(random_subtree == 1.)[0]

    print("Random subtree: ", random_subtree)

    if outliers > 0:
        outlier_slots = np.where(random_subtree == 0.)[0]
        selected_outliers = np.random.choice(outlier_slots, size=outliers)
        subtree_slots = np.concatenate((subtree_slots, selected_outliers))

    random_tree_constraint = []
    random_mod_tree_constraint = []
    random_contiguous_constraint = []
    random_contiguous_complete_constraint = []
    random_general_disconnect_constraint = []
    random_root_node_disconnect_constraint = []
    random_unified_disconnect_constraint = []
    random_root_sparsity_constraint = []

    draw_tree(nodes=incremental_config, adjacency_matrix=adjacency_matrix, title_name='Original')

    incremental_step = 0.05
    for _ in np.arange(0., 1., incremental_step):
        incremental_config[subtree_slots] += incremental_step

        # random_root_pooling = np.zeros(len(nodes))
        # random_root_pooling[subtree_slots[0]] = 1.
        # random_root_pooling = random_root_pooling[np.newaxis, :]

        random_pooling = incremental_config[:, np.newaxis]

        random_root_pooling = lin_auto_root_pooling(node_pooling=random_pooling, adjacency_matrix=adjacency_matrix)
        # print(random_root_pooling.ravel()[random_root_pooling.ravel() > 0.])

        random_tree_constraint.append(tree_constraint_alt(node_pooling=random_pooling,
                                                          root_pooling=random_root_pooling,
                                                          node_spans=node_spans)[0])
        random_mod_tree_constraint.append(mod_tree_constraint(node_pooling=random_pooling,
                                                              root_pooling=random_root_pooling,
                                                              node_spans=node_spans)[0])

        random_contiguous_constraint.append(contiguous_sequence_constraint_alt(node_pooling=random_pooling,
                                                                               adjacency_matrix=adjacency_matrix)[
                                                0])

        random_contiguous_complete_constraint.append(
            contiguous_sequence_constraint_complete(node_pooling=random_pooling,
                                                    root_pooling=random_root_pooling,
                                                    node_spans=node_spans,
                                                    adjacency_matrix=adjacency_matrix)[0])

        random_general_disconnect, random_root_disconnect = disconnected_nodes_constraint(node_pooling=random_pooling,
                                                                                          root_pooling=random_root_pooling,
                                                                                          adjacency_matrix=adjacency_matrix,
                                                                                          alpha=alpha)

        random_unified_disconnect = unified_disconnected_nodes_constraint(node_pooling=random_pooling,
                                                                          root_pooling=random_root_pooling,
                                                                          adjacency_matrix=adjacency_matrix,
                                                                          alpha=alpha)

        random_general_disconnect_constraint.append(random_general_disconnect[0])
        random_root_node_disconnect_constraint.append(random_root_disconnect[0])
        random_unified_disconnect_constraint.append(random_unified_disconnect[0])

        random_root_sparsity_constraint.append(sparsity_constraint(random_root_pooling)[0])

    print("Final pooling: ", random_pooling.ravel())
    print("Final root pooling: ", random_root_pooling.ravel())

    draw_tree(nodes=random_pooling.ravel(), adjacency_matrix=adjacency_matrix, title_name='Final')

    random_tree_constraint = np.array(random_tree_constraint)
    random_mod_tree_constraint = np.array(random_mod_tree_constraint)
    random_contiguous_constraint = np.array(random_contiguous_constraint)
    random_contiguous_complete_constraint = np.array(random_contiguous_complete_constraint)
    random_general_disconnect_constraint = np.array(random_general_disconnect_constraint)
    random_root_node_disconnect_constraint = np.array(random_root_node_disconnect_constraint)
    random_unified_disconnect_constraint = np.array(random_unified_disconnect_constraint)
    random_root_sparsity_constraint = np.array(random_root_sparsity_constraint)

    random_true_tree_constraint = added_tree_constraint[random_subtree_idx]
    random_true_contiguous_constraint = added_contiguous_sequence_constraint[random_subtree_idx]
    random_true_contiguous_complete_constraint = added_contiguous_sequence_complete_constraint[random_subtree_idx]
    random_true_general_disconnect_constraint = added_general_disconnect_constraint[random_subtree_idx]
    random_true_root_disconnect_constraint = added_root_node_disconnect_constraint[random_subtree_idx]
    random_true_unified_disconnect_constraint = added_unified_node_disconnect_constraint[random_subtree_idx]

    fig, ax = plt.subplots(1, 1)
    ax.scatter(np.arange(len(random_root_sparsity_constraint)), random_root_sparsity_constraint, c='b', marker='s')
    ax.scatter(np.arange(len(random_tree_constraint)), random_mod_tree_constraint, c='c', marker='o')
    ax.scatter(np.arange(len(random_tree_constraint)), random_tree_constraint, c='g', marker='o')
    ax.scatter(np.arange(len(random_tree_constraint)), random_mod_tree_constraint * random_root_sparsity_constraint,
               c='c', marker='^')
    ax.scatter(np.arange(len(random_tree_constraint)), random_tree_constraint * random_root_sparsity_constraint, c='g',
               marker='^')
    ax.scatter([len(random_tree_constraint) + 1], [random_true_tree_constraint], c='r', marker='x')
    ax.legend(['Entropy', 'Generated - Mod', 'Generated - Default', 'Generated - Mod - Entropy',
               'Generated - Default - Entropy',
               'Valid'])
    ax.set_title(
        "Tree constraints as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
            outliers))

    # plt.scatter(np.arange(len(random_contiguous_constraint)), random_contiguous_constraint, c='g', marker='o')
    # plt.scatter([len(random_contiguous_constraint) + 1], [random_true_contiguous_constraint], c='r', marker='x')
    # plt.legend(['Generated', 'Valid'])
    # plt.title(
    #     "Sequence constraint as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
    #         outliers))
    # plt.show()

    fig, ax = plt.subplots(1, 1)
    ax.scatter(np.arange(len(random_contiguous_complete_constraint)),
               random_contiguous_constraint + random_contiguous_complete_constraint, c='c',
               marker='^')
    ax.scatter(np.arange(len(random_contiguous_constraint)), random_contiguous_constraint, c='g', marker='o')
    ax.scatter([len(random_contiguous_constraint) + 1], [random_true_contiguous_constraint], c='r', marker='x')
    ax.legend(['Generated - Span', 'Generated - Default', 'Valid'])
    ax.set_title(
        "Sequence constraints as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
            outliers))

    # plt.scatter([len(random_contiguous_complete_constraint) + 1],
    #             [random_true_contiguous_constraint + random_true_contiguous_complete_constraint], c='r',
    #             marker='x')
    # plt.legend(['Generated', 'Valid'])
    # plt.title(
    #     "Complete sequence constraint as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
    #         outliers))
    # plt.show()

    # plt.scatter(np.arange(len(random_general_disconnect_constraint)), random_general_disconnect_constraint, c='g',
    #             marker='o')
    # plt.scatter([len(random_general_disconnect_constraint) + 1], [random_true_general_disconnect_constraint], c='r',
    #             marker='x')
    # plt.legend(['Generated', 'Valid'])
    # plt.title(
    #     "General disconnect constraint as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
    #         outliers))
    # plt.show()
    #
    # plt.scatter(np.arange(len(random_root_node_disconnect_constraint)), random_root_node_disconnect_constraint, c='g',
    #             marker='o')
    # plt.scatter([len(random_root_node_disconnect_constraint) + 1], [random_true_root_disconnect_constraint], c='r',
    #             marker='x')
    # plt.legend(['Generated', 'Valid'])
    # plt.title(
    #     "Root node disconnect constraint as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
    #         outliers))
    # plt.show()
    #

    fig, ax = plt.subplots(1, 1)
    ax.scatter(np.arange(len(random_unified_disconnect_constraint)), random_unified_disconnect_constraint, c='g',
               marker='o')
    ax.scatter([len(random_unified_disconnect_constraint) + 1], [random_true_unified_disconnect_constraint], c='r',
               marker='x')
    ax.legend(['Generated', 'Valid'])
    ax.set_title(
        "Unified disconnect constraint as a random config in a span becomes close to a valid tree (N° outliers = {})".format(
            outliers))
    plt.show()


random_subtree_incremental_test(added_subtrees=added_subtrees,
                                node_spans=node_spans,
                                adjacency_matrix=adjacency_matrix)

random_subtree_incremental_test(added_subtrees=added_subtrees,
                                node_spans=node_spans,
                                adjacency_matrix=adjacency_matrix,
                                outliers=5)

print('[Step 6] Completed!')

print('[Step 7] Assessing root pooling sparsity constraint...')

test_root_pooling = mock_soft_root_pooling(nodes, K)
random_idx = np.random.randint(low=0, high=len(nodes))
remaining_idxs = np.array([idx for idx in range(len(nodes)) if idx != random_idx])
delta = 0.05
init_step = test_root_pooling[:, random_idx][0]

sparsity_values = []
for _ in np.arange(init_step, 1.0, delta):
    sparsity_values.append(sparsity_constraint(test_root_pooling))

    test_root_pooling[:, random_idx] += delta
    test_root_pooling[:, random_idx] = np.minimum(test_root_pooling[:, random_idx], 1.)
    test_root_pooling[:, remaining_idxs] -= delta / (remaining_idxs.shape[0] - 1)
    test_root_pooling[:, remaining_idxs] = np.maximum(test_root_pooling[:, remaining_idxs],
                                                      np.zeros_like(test_root_pooling[:, remaining_idxs]))

sparsity_values = np.array(sparsity_values)
print('Test root pooling: \n', test_root_pooling)

# for k in range(K):
#     plt.plot(sparsity_values[:, k])
# plt.legend(["k={}".format(k) for k in range(K)])
# plt.title("Root pooling sparsity constraint as it becomes more and more sparse")
# plt.show()

print('[Step 7] Completed!')

print('[Step 8] Check tree constraints with tricky configuration...')


def random_subtree_incremental_test(added_subtrees, node_spans, adjacency_matrix):
    max_removal = -1

    while max_removal < 0:
        random_subtree_idx = np.random.randint(low=0, high=len(added_subtrees))
        random_subtree = added_subtrees[random_subtree_idx]
        max_removal = np.sum(random_subtree) - 5

    subtree_slots = np.where(random_subtree == 1.)[0]
    indexes_to_reduce = subtree_slots[1:3]

    random_tree_constraint = []
    random_mod_tree_constraint = []
    random_contiguous_constraint = []
    random_contiguous_complete_constraint = []
    random_general_disconnect_constraint = []
    random_root_disconnect_constraint = []
    random_unified_disconnect_constraint = []
    random_root_sparsity_constraint = []

    print("Random subtree: ", random_subtree)

    draw_tree(nodes=random_subtree, adjacency_matrix=adjacency_matrix, title_name='Original')

    incremental_step = 0.05
    for _ in np.arange(1. + incremental_step, incremental_step, -incremental_step):
        random_subtree[indexes_to_reduce] -= incremental_step
        random_subtree[indexes_to_reduce] = np.maximum(random_subtree[indexes_to_reduce], 0.)

        random_pooling = random_subtree[:, np.newaxis]

        # random_root_pooling = np.zeros(len(nodes))
        # random_root_pooling[subtree_slots[0]] = 1.
        # random_root_pooling = random_root_pooling[np.newaxis, :]
        random_root_pooling = lin_auto_root_pooling(node_pooling=random_pooling, adjacency_matrix=adjacency_matrix)

        random_tree_constraint.append(tree_constraint_alt(node_pooling=random_pooling,
                                                          root_pooling=random_root_pooling,
                                                          node_spans=node_spans)[0])

        random_mod_tree_constraint.append(mod_tree_constraint(node_pooling=random_pooling,
                                                              root_pooling=random_root_pooling,
                                                              node_spans=node_spans)[0])

        random_contiguous_constraint.append(contiguous_sequence_constraint_alt(node_pooling=random_pooling,
                                                                               adjacency_matrix=adjacency_matrix)[
                                                0])

        random_contiguous_complete_constraint.append(
            contiguous_sequence_constraint_complete(node_pooling=random_pooling,
                                                    root_pooling=random_root_pooling,
                                                    node_spans=node_spans,
                                                    adjacency_matrix=adjacency_matrix)[0])

        general_disconnect, root_disconnect = disconnected_nodes_constraint(node_pooling=random_pooling,
                                                                            root_pooling=random_root_pooling,
                                                                            adjacency_matrix=adjacency_matrix,
                                                                            alpha=alpha)

        random_general_disconnect_constraint.append(general_disconnect[0])
        random_root_disconnect_constraint.append(root_disconnect[0])

        random_unified_disconnect_constraint.append(unified_disconnected_nodes_constraint(node_pooling=random_pooling,
                                                                                          root_pooling=random_root_pooling,
                                                                                          adjacency_matrix=adjacency_matrix,
                                                                                          alpha=alpha)[0])

        random_root_sparsity_constraint.append(sparsity_constraint(random_root_pooling)[0])

    print("Final pooling: ", random_pooling.ravel())
    print("Final root pooling: ", random_root_pooling.ravel())

    draw_tree(nodes=random_pooling.ravel(), adjacency_matrix=adjacency_matrix, title_name='Final')

    random_tree_constraint = np.array(random_tree_constraint)
    random_mod_tree_constraint = np.array(random_mod_tree_constraint)
    random_contiguous_constraint = np.array(random_contiguous_constraint)
    random_contiguous_complete_constraint = np.array(random_contiguous_complete_constraint)
    random_general_disconnect_constraint = np.array(random_general_disconnect_constraint)
    random_root_disconnect_constraint = np.array(random_root_disconnect_constraint)
    random_unified_disconnect_constraint = np.array(random_unified_disconnect_constraint)
    random_root_sparsity_constraint = np.array(random_root_sparsity_constraint)

    random_true_tree_constraint = added_tree_constraint[random_subtree_idx]
    random_true_contiguous_constraint = added_contiguous_sequence_constraint[random_subtree_idx]
    random_true_contiguous_complete_constraint = added_contiguous_sequence_complete_constraint[random_subtree_idx]
    random_true_general_disconnect_constraint = added_general_disconnect_constraint[random_subtree_idx]
    random_true_root_disconnect_constraint = added_root_node_disconnect_constraint[random_subtree_idx]
    random_true_unified_disconnect_constraint = added_unified_node_disconnect_constraint[random_subtree_idx]

    fig, ax = plt.subplots(1, 1)
    ax.scatter(np.arange(len(random_root_sparsity_constraint)), random_root_sparsity_constraint, c='b', marker='s')
    ax.scatter(np.arange(len(random_tree_constraint)), random_mod_tree_constraint, c='c', marker='o')
    ax.scatter(np.arange(len(random_tree_constraint)), random_tree_constraint, c='g', marker='o')
    ax.scatter(np.arange(len(random_tree_constraint)), random_mod_tree_constraint * random_root_sparsity_constraint,
               c='c', marker='^')
    ax.scatter(np.arange(len(random_tree_constraint)), random_tree_constraint * random_root_sparsity_constraint, c='g',
               marker='^')
    ax.scatter([len(random_tree_constraint) + 1], [random_true_tree_constraint], c='r', marker='x')
    ax.legend(
        ['Entropy', 'Generated - Mod', 'Generated - Valid', 'Generated - Mod - Entropy', 'Generated - Valid - Entropy',
         'Valid'])
    ax.set_title("Tree constraint with a tricky configuration in which some tree nodes are slowly removed")

    # plt.scatter(np.arange(len(random_contiguous_constraint)), random_contiguous_constraint, c='g', marker='o')
    # plt.scatter([len(random_contiguous_constraint) + 1], [random_true_contiguous_constraint], c='r', marker='x')
    # plt.legend(['Generated', 'Valid'])
    # plt.title("Sequence constraint with a tricky configuration in which some tree nodes are slowly removed")
    # plt.show()

    fig, ax = plt.subplots(1, 1)
    ax.scatter(np.arange(len(random_contiguous_complete_constraint)),
               random_contiguous_constraint + random_contiguous_complete_constraint, c='c',
               marker='^')
    ax.scatter(np.arange(len(random_contiguous_constraint)), random_contiguous_constraint, c='g', marker='o')
    ax.scatter([len(random_contiguous_constraint) + 1], [random_true_contiguous_constraint], c='r', marker='x')
    ax.legend(['Generated - Span', 'Generated - Default', 'Valid'])
    ax.set_title("Sequence constraint with a tricky configuration in which some tree nodes are slowly removed")

    # plt.scatter([len(random_contiguous_complete_constraint) + 1],
    #             [random_true_contiguous_constraint + random_true_contiguous_complete_constraint], c='r',
    #             marker='x')
    # plt.legend(['Generated ', 'Valid'])
    # plt.title("Complete sequence constraint with a tricky configuration in which some tree nodes are slowly removed")
    # plt.show()
    #
    # plt.scatter(np.arange(len(random_general_disconnect_constraint)), random_general_disconnect_constraint, c='g',
    #             marker='o')
    # plt.scatter([len(random_general_disconnect_constraint) + 1], [random_true_general_disconnect_constraint], c='r',
    #             marker='x')
    # plt.legend(['Generated', 'Valid'])
    # plt.title("General disconnect constraint with a tricky configuration in which some tree nodes are slowly removed")
    # plt.show()
    #

    fig, ax = plt.subplots(1, 1)
    ax.scatter(np.arange(len(random_unified_disconnect_constraint)), random_unified_disconnect_constraint, c='g',
               marker='o')
    ax.scatter([len(random_unified_disconnect_constraint) + 1], [random_true_unified_disconnect_constraint],
               c='r',
               marker='x')
    ax.legend(['Generated', 'Valid'])
    ax.set_title(
        "Unified node disconnect constraint with a tricky configuration in which some tree nodes are slowly removed")
    plt.show()


random_subtree_incremental_test(added_subtrees=added_subtrees,
                                node_spans=node_spans, adjacency_matrix=adjacency_matrix)

print('[Step 8] Completed!')

print('[Step 9] Check tree constraints when root pooling becomes sparse...')


def evaluate_root_pooling_sparseness(node_pooling, adjacency_matrix, node_spans, tree_type='valid'):
    # K x N
    test_root_pooling = mock_soft_root_pooling(nodes, node_pooling.shape[1])
    # test_root_pooling = auto_root_pooling(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)
    chosen_roots = np.argmax(node_pooling, axis=0)
    remaining_idxs_y = [[idx for idx in range(len(nodes)) if idx != k_root] for k_root in chosen_roots]
    remaining_idxs_y = np.array(remaining_idxs_y)
    remaining_idxs_x = [[k] * (len(nodes) - 1) for k in range(len(chosen_roots))]
    remaining_idxs_x = np.array(remaining_idxs_x)
    delta = 0.05
    decrement_step = delta / (test_root_pooling.shape[1] - 1)

    tree_constraint_values = []
    sequence_constraint_values = []
    sequence_complete_constraint_values = []
    general_disconnect_constraint_values = []
    root_disconnect_constraint_values = []

    while np.not_equal(test_root_pooling[np.arange(test_root_pooling.shape[0]), chosen_roots],
                       np.ones_like(test_root_pooling[np.arange(test_root_pooling.shape[0]), chosen_roots])).any():
        test_root_pooling[np.arange(test_root_pooling.shape[0]), chosen_roots] += delta

        test_root_pooling[np.arange(test_root_pooling.shape[0]), chosen_roots] = np.minimum(
            test_root_pooling[np.arange(test_root_pooling.shape[0]), chosen_roots],
            np.ones_like(test_root_pooling[np.arange(test_root_pooling.shape[0]), chosen_roots]))

        test_root_pooling[remaining_idxs_x, remaining_idxs_y] -= decrement_step

        test_root_pooling[remaining_idxs_x, remaining_idxs_y] = np.maximum(
            test_root_pooling[remaining_idxs_x, remaining_idxs_y],
            np.zeros_like(test_root_pooling[remaining_idxs_x, remaining_idxs_y]))

        tree_constraint_values.append(tree_constraint(node_pooling=node_pooling,
                                                      root_pooling=test_root_pooling,
                                                      node_spans=node_spans))
        sequence_constraint_values.append(contiguous_sequence_constraint_alt(node_pooling=node_pooling,
                                                                             adjacency_matrix=adjacency_matrix))

        sequence_complete_constraint_values.append(contiguous_sequence_constraint_complete(node_pooling=node_pooling,
                                                                                           root_pooling=test_root_pooling,
                                                                                           adjacency_matrix=adjacency_matrix,
                                                                                           node_spans=node_spans))

        general_disconnect, root_disconnect = disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                            root_pooling=test_root_pooling,
                                                                            adjacency_matrix=adjacency_matrix,
                                                                            alpha=alpha)
        general_disconnect_constraint_values.append(general_disconnect)
        root_disconnect_constraint_values.append(root_disconnect)

    tree_constraint_values = np.array(tree_constraint_values)
    sequence_constraint_values = np.array(sequence_constraint_values)
    sequence_complete_constraint_values = np.array(sequence_complete_constraint_values)
    general_disconnect_constraint_values = np.array(general_disconnect_constraint_values)
    root_disconnect_constraint_values = np.array(root_disconnect_constraint_values)

    if len(tree_constraint_values.shape) == 1:
        tree_constraint_values = tree_constraint_values[:, np.newaxis]
        sequence_constraint_values = sequence_constraint_values[:, np.newaxis]
        sequence_complete_constraint_values = sequence_complete_constraint_values[:, np.newaxis]
        general_disconnect_constraint_values = general_disconnect_constraint_values[:, np.newaxis]
        root_disconnect_constraint_values = root_disconnect_constraint_values[:, np.newaxis]

    for k in range(node_pooling.shape[1]):
        plt.plot(tree_constraint_values[:, k])
    plt.legend(["k={}".format(k) for k in range(K)])
    plt.title("Tree constraint as root pooling becomes more sparse (subtree type = {})".format(tree_type))
    plt.show()

    for k in range(node_pooling.shape[1]):
        plt.plot(sequence_constraint_values[:, k])
    plt.legend(["k={}".format(k) for k in range(K)])
    plt.title(
        "Contiguous sequence constraint as root pooling becomes more sparse (subtree type = {})".format(tree_type))
    plt.show()

    for k in range(node_pooling.shape[1]):
        plt.plot(sequence_constraint_values[:, k] + sequence_complete_constraint_values[:, k])
    plt.legend(["k={}".format(k) for k in range(K)])
    plt.title(
        "Complete contiguous sequence constraint as root pooling becomes more sparse (subtree type = {})".format(
            tree_type))
    plt.show()

    for k in range(node_pooling.shape[1]):
        plt.plot(general_disconnect_constraint_values[:, k])
    plt.legend(["k={}".format(k) for k in range(K)])
    plt.title(
        "General disconnect sequence constraint as root pooling becomes more sparse (subtree type = {})".format(
            tree_type))
    plt.show()

    for k in range(node_pooling.shape[1]):
        plt.plot(root_disconnect_constraint_values[:, k])
    plt.legend(["k={}".format(k) for k in range(K)])
    plt.title(
        "Root node disconnect sequence constraint as root pooling becomes more sparse (subtree type = {})".format(
            tree_type))
    plt.show()


random_valid_subtree_idx = np.random.randint(low=0, high=len(added_subtrees))
random_valid_subtree = added_subtrees[random_valid_subtree_idx]

random_exp_subtree_idx = np.random.randint(low=0, high=int(len(exp_subtrees) / K))
random_exp_subtree = exp_subtrees[random_exp_subtree_idx:random_exp_subtree_idx + K]

# Valid subtree test
# evaluate_root_pooling_sparseness(node_pooling=random_valid_subtree[:, np.newaxis],
#                                  adjacency_matrix=adjacency_matrix,
#                                  node_spans=node_spans,
#                                  tree_type="valid")
#
# evaluate_root_pooling_sparseness(node_pooling=random_exp_subtree.transpose(),
#                                  adjacency_matrix=adjacency_matrix,
#                                  node_spans=node_spans,
#                                  tree_type="generated")

print('[Step 9] Completed!')

print('[Step 10] Check node/root pooling consistency constraint as sparsity constraint is enforced...')


def inter_pooling_consistency_constraint(node_pooling, root_pooling):
    # N x K
    # N x K

    consistency = np.maximum(root_pooling.transpose() - node_pooling, 0.)
    return np.sum(consistency, axis=0)


random_idx = np.random.randint(low=0, high=len(nodes))
remaining_idxs = np.array([idx for idx in range(len(nodes)) if idx != random_idx])
delta = 0.05
init_step = test_root_pooling[:, random_idx][0]
test_node_pooling = mock_pooling(nodes, K, use_sigmoid=use_sigmoid)
# test_root_pooling = mock_soft_root_pooling(nodes, K)
test_root_pooling = auto_root_pooling(node_pooling=test_node_pooling, adjacency_matrix=adjacency_matrix)

sparsity_values = []
for _ in np.arange(init_step, 1.0, delta):
    test_root_pooling[:, random_idx] += delta
    test_root_pooling[:, random_idx] = np.minimum(test_root_pooling[:, random_idx], 1.)
    test_root_pooling[:, remaining_idxs] -= delta / (remaining_idxs.shape[0] - 1)
    test_root_pooling[:, remaining_idxs] = np.maximum(test_root_pooling[:, remaining_idxs],
                                                      np.zeros_like(test_root_pooling[:, remaining_idxs]))

    sparsity_values.append(inter_pooling_consistency_constraint(node_pooling=test_node_pooling,
                                                                root_pooling=test_root_pooling))

sparsity_values = np.array(sparsity_values)

# for k in range(sparsity_values.shape[1]):
#     plt.plot(sparsity_values[:, k])
# plt.legend(["k={}".format(k) for k in range(sparsity_values.shape[1])])
# plt.title("Node-Root pooling consistency constraint as the root pooling becomes more sparse")
# plt.show()

print('[Step 10] Completed!')

print("[Step 11] Comparing constraints when considering uniform and hot-encoding root pooling")


def uniform_vs_hard_constraints_test(iterations, nodes, node_spans, adjacency_matrix):
    uniform_tree_constraint = []
    uniform_contiguous_constraint = []
    uniform_contiguous_complete_constraint = []
    uniform_general_disconnect_constraint = []
    uniform_root_node_disconnect_constraint = []
    uniform_unified_disconnect_constraint = []
    uniform_consistency_constraint = []
    uniform_sparsity_constraint = []

    hard_tree_constraint = []
    hard_contiguous_constraint = []
    hard_contiguous_complete_constraint = []
    hard_general_disconnect_constraint = []
    hard_root_node_disconnect_constraint = []
    hard_unified_disconnect_constraint = []
    hard_consistency_constraint = []
    hard_sparsity_constraint = []

    for it in range(iterations):
        node_pooling = mock_pooling(nodes=nodes, K=K, use_sigmoid=use_sigmoid)

        # Uniform case
        # root_pooling = mock_soft_root_pooling(nodes=nodes, K=K)
        root_pooling = auto_root_pooling(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)

        uniform_tree_constraint.extend(tree_constraint(node_pooling=node_pooling,
                                                       root_pooling=root_pooling,
                                                       node_spans=node_spans))
        uniform_contiguous_constraint.extend(contiguous_sequence_constraint_alt(node_pooling=node_pooling,
                                                                                adjacency_matrix=adjacency_matrix))

        uniform_contiguous_complete_constraint.extend(contiguous_sequence_constraint_complete(node_pooling=node_pooling,
                                                                                              adjacency_matrix=adjacency_matrix,
                                                                                              node_spans=node_spans,
                                                                                              root_pooling=root_pooling))

        uniform_general_disconnect, uniform_root_disconnect = disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                                            root_pooling=root_pooling,
                                                                                            adjacency_matrix=adjacency_matrix,
                                                                                            alpha=alpha)

        uniform_unified_disconnect = unified_disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                           root_pooling=root_pooling,
                                                                           adjacency_matrix=adjacency_matrix,
                                                                           alpha=alpha)

        uniform_general_disconnect_constraint.extend(uniform_general_disconnect)
        uniform_root_node_disconnect_constraint.extend(uniform_root_disconnect)
        uniform_unified_disconnect_constraint.extend(uniform_unified_disconnect)

        uniform_consistency_constraint.extend(inter_pooling_consistency_constraint(node_pooling=node_pooling,
                                                                                   root_pooling=root_pooling))

        uniform_sparsity_constraint.extend(sparsity_constraint(root_pooling=root_pooling))

        # Hard case
        # root_pooling = mock_root_pooling(nodes=nodes, K=K)
        hard_root_pooling = auto_root_pooling(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)
        for k in range(K):
            hard_root_pooling[k, np.argmax(node_pooling[:, k])] = 1.
            hard_root_pooling[k, hard_root_pooling[k, :] < 1.] = 0.

        hard_tree_constraint.extend(tree_constraint(node_pooling=node_pooling,
                                                    root_pooling=hard_root_pooling,
                                                    node_spans=node_spans))
        hard_contiguous_constraint.extend(contiguous_sequence_constraint_alt(node_pooling=node_pooling,
                                                                             adjacency_matrix=adjacency_matrix))

        hard_contiguous_complete_constraint.extend(contiguous_sequence_constraint_complete(node_pooling=node_pooling,
                                                                                           adjacency_matrix=adjacency_matrix,
                                                                                           node_spans=node_spans,
                                                                                           root_pooling=hard_root_pooling))

        hard_general_disconnect, hard_root_disconnect = disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                                      root_pooling=hard_root_pooling,
                                                                                      adjacency_matrix=adjacency_matrix,
                                                                                      alpha=alpha)

        hard_unified_disconnect = unified_disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                        root_pooling=hard_root_pooling,
                                                                        adjacency_matrix=adjacency_matrix,
                                                                        alpha=alpha)

        hard_general_disconnect_constraint.extend(hard_general_disconnect)
        hard_root_node_disconnect_constraint.extend(hard_root_disconnect)
        hard_unified_disconnect_constraint.extend(hard_unified_disconnect)

        hard_consistency_constraint.extend(inter_pooling_consistency_constraint(node_pooling=node_pooling,
                                                                                root_pooling=hard_root_pooling))

        hard_sparsity_constraint.extend(sparsity_constraint(root_pooling=hard_root_pooling))

    uniform_values = [np.mean(uniform_tree_constraint),
                      np.mean(uniform_contiguous_constraint),
                      np.mean(uniform_contiguous_complete_constraint),
                      np.mean(uniform_general_disconnect_constraint),
                      np.mean(uniform_root_node_disconnect_constraint),
                      np.mean(uniform_unified_disconnect_constraint),
                      np.mean(uniform_consistency_constraint),
                      np.mean(uniform_sparsity_constraint)
                      ]

    hard_values = [np.mean(hard_tree_constraint),
                   np.mean(hard_contiguous_constraint),
                   np.mean(hard_contiguous_complete_constraint),
                   np.mean(hard_general_disconnect_constraint),
                   np.mean(hard_root_node_disconnect_constraint),
                   np.mean(hard_unified_disconnect_constraint),
                   np.mean(hard_consistency_constraint),
                   np.mean(hard_sparsity_constraint)
                   ]

    plt.bar(np.arange(len(uniform_values)) - 0.2, uniform_values, color='g', align='center', width=0.2)
    plt.bar(np.arange(len(hard_values)), hard_values, color='r', align='center', width=0.2)
    plt.legend(['Uniform', 'Hard'])
    plt.title("Average constraint values when considering Uniform and Hard root pooling")
    plt.show()


# uniform_vs_hard_constraints_test(iterations=iterations,
#                                  nodes=nodes,
#                                  node_spans=node_spans,
#                                  adjacency_matrix=adjacency_matrix)

print("[Step 11] Completed!")

print("[Step 12] Comparing constraints when considering softmax- and sigmoid-based pooling")

print("[Step 12] Completed!")
