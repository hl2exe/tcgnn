"""

Given a task bullet points, a list of pre-trained models with their predictions on cord19 dataset:

    1. For each model:

        1A. Encode bullet points via pre-trained model settings

        1B. Obtain word embedding of bullet points via pre-trained model forward step

        1C. Obtain word embedding of cord19 dataset produced by pre-trained model

        1D. Filter out dataset entries that are not predicted as argumentative sentences by pre-trained model

        1E. Store argumentative sentences

        1F. For each similarity metric: cosine, L2, etc..

            1FA. Compute bullet points similarity

            1FB. Store similarity

    2. Store ensemble inference information:

        Index (w.r.t. CORD-19 dataset) | Sentence | Majority Tag | Model_ID_prediction | Metric_Model_ID_Query_value

"""

import os

import numpy as np
import pandas as pd

import const_define as cd
from tensorflow.python.keras import backend as K
from custom_callbacks_v2 import RepresentationRetriever
import const_define as exp_cd
from data_converter import DataConverterFactory
from data_loader import CORD19Loader
from data_processor import ProcessorFactory
from utility.data_utils import get_data_config_id
from nn_models_v2 import ModelFactory
from utility.pipeline_utils import get_dataset_fn
from tokenization import TokenizerFactory
from utility.json_utils import load_json
from utility.python_utils import merge, flatten
from collections import Counter
from itertools import product
from tqdm import tqdm


def cosine_similarity(a, b, transpose_b=True):
    a = a / np.linalg.norm(a, axis=1)[:, np.newaxis]
    b = b / np.linalg.norm(b, axis=1)[:, np.newaxis]
    if transpose_b:
        b = b.transpose()

    return np.matmul(a, b)


def dot_product(a, b, transpose_b=True):
    if transpose_b:
        b = b.transpose()

    return np.matmul(a, b)


# Settings

model_type = "ibm2015_experimental_single_gnn_v2"
test_name = "16-05-2020-15-37-20"
test_folder = cd.LOO_DIR
repetition_prefix = None
test_prefix = 'loo'
original_dataset_name = "IBM2015"
task_name = "Task1"
similarity_ops = ['cosine', 'dot_product']
not_argumentative_key = 'NOT-ARG'
save_info = True

saved_data_path = os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name)

save_prefixes = [name for name in os.listdir(saved_data_path) if 'graph_representations' in name]
save_prefixes = [name.split('key')[1].split('_')[1] for name in save_prefixes]
save_prefixes = list(set(save_prefixes))
# ---------------

model_path = os.path.join(test_folder, model_type, test_name)

repr_retriever = RepresentationRetriever(save_path=os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name),
                                         save_suffix=task_name)
callbacks = [
    repr_retriever
]

# Load task data

with open(os.path.join(cd.CORD19_DIR, "{}_trees.txt".format(task_name)), "r") as f:
    task_data_trees = f.readlines()

with open(os.path.join(cd.CORD19_DIR, "{}.txt".format(task_name)), "r") as f:
    task_data_sentences = f.readlines()

task_data = {}
for item, item_tree in zip(task_data_sentences, task_data_trees):
    task_data.setdefault('Sentence', []).append(item)
    task_data.setdefault('Tree', []).append(item_tree)
task_data = pd.DataFrame.from_dict(task_data)

# Load cord-19 data

data_handle = CORD19Loader().load()
dataset_data = data_handle.get_data()

# Original model data info

model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))
original_data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
original_data_loader_type = original_data_loader_config['type']
original_data_loader_info = original_data_loader_config['configs'][original_data_loader_type]
original_loader_additional_info = {key: value['value'] for key, value in model_config.items()
                                   if 'data_loader' in value['flags']}
original_data_loader_info = merge(original_data_loader_info, original_loader_additional_info)

config_args = {key: arg['value']
               for key, arg in model_config.items()
               if 'processor' in arg['flags']
               or 'tokenizer' in arg['flags']
               or 'converter' in arg['flags']}
config_args = flatten(config_args)
config_args = merge(config_args, original_data_loader_info)
config_args_tuple = [(key, value) for key, value in config_args.items()]
config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                               original_dataset_name,
                               model_type)
config_id = get_data_config_id(filepath=model_base_path, config=config_name)

#
# # Build model pipeline

model_config['build_embedding_matrix']['value'] = False

# Build Processor
processor_type = exp_cd.MODEL_CONFIG[model_type]['processor']
processor_args = {key: arg['value'] for key, arg in model_config.items() if 'processor' in arg['flags']}
processor_args['loader_info'] = data_handle.get_additional_info()
processor_args['retrieve_label'] = False
processor_factory = ProcessorFactory()
processor = processor_factory.factory(processor_type, **processor_args)

# # Build tokenizer
tokenizer_type = exp_cd.MODEL_CONFIG[model_type]['tokenizer']
tokenizer_factory = TokenizerFactory()
tokenizer_args = {key: arg['value'] for key, arg in model_config.items() if 'tokenizer' in arg['flags']}
tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)
#
# # Build converter
converter_type = exp_cd.MODEL_CONFIG[model_type]['converter']
converter_factory = DataConverterFactory()
converter_args = {key: arg['value'] for key, arg in model_config.items() if 'converter' in arg['flags']}
converter = converter_factory.factory(converter_type, **converter_args)
#
# # Task data processing
#
task_examples = processor.get_test_examples(data=task_data, ids=np.arange(task_data.shape[0]))


ensemble_info = {}

for save_prefix in tqdm(save_prefixes):
    print('Considering prefix: ', save_prefix)

    # Task data conversion
    #
    data_path = os.path.join(cd.TESTS_DATA_DIR, original_dataset_name, model_type, str(config_id))
    tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=data_path,
                                                                                      prefix='{0}_{1}'.format(test_prefix,
                                                                                                              save_prefix)
                                                                                      if test_prefix is not None else save_prefix)
    converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
        filepath=data_path,
        prefix='{0}_{1}'.format(test_prefix, save_prefix) if test_prefix is not None else save_prefix)

    #
    # Tokenizer
    tokenizer.initalize_with_vocab(tokenizer_info['vocab'])

    # Conversion
    for key, value in converter_info.items():
        setattr(converter, key, value)

    tmp_filepath = os.path.join(model_path, "{}_tmp".format(task_name))

    converter.convert_data(examples=task_examples,
                           label_list=None,
                           has_labels=False,
                           output_file=tmp_filepath,
                           tokenizer=tokenizer)

    test_data = get_dataset_fn(filepath=tmp_filepath,
                               batch_size=len(task_examples),
                               name_to_features=converter.feature_class.get_mappings(converter_info, has_labels=False),
                               selector=converter.feature_class.get_dataset_selector(),
                               is_training=False,
                               prefetch_amount=1)

    #
    # # Build network
    #
    network_retrieved_args = {key: value['value'] for key, value in model_config.items()
                              if 'model_class' in value['flags']}
    network_retrieved_args['name'] = exp_cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
    if repetition_prefix is not None:
        network_retrieved_args['name'] += '_repetition_{}'.format(repetition_prefix)
    if save_prefix is not None:
        network_retrieved_args['name'] += '_key_{}'.format(save_prefix)
    network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)
    #
    test_steps = 1
    #
    # Custom callbacks only
    for callback in callbacks:
        if hasattr(callback, 'on_build_model_begin'):
            callback.on_build_model_begin(logs={'network': network})

    text_info = merge(tokenizer_info, converter_info)
    network.build_model(text_info=text_info)

    # Custom callbacks only
    for callback in callbacks:
        if hasattr(callback, 'on_build_model_end'):
            callback.on_build_model_end(logs={'network': network})

    # Setup model by feeding an input
    network.predict(x=iter(test_data()), steps=1)

    # load pre-trained weights
    current_weight_filename = os.path.join(model_path,
                                           '{}.h5'.format(network_retrieved_args['name']))
    network.load(os.path.join(model_path, current_weight_filename))

    # Inference
    network.predict(x=iter(test_data()),
                    steps=test_steps,
                    callbacks=callbacks)

    K.clear_session()

    # Load graph embeddings

    task_representation_path = os.path.join(saved_data_path,
                                            '{0}_{1}_graph_representations.npy'.format(network.name,
                                                                                        task_name))
    dataset_representation_path = os.path.join(saved_data_path,
                                               '{0}_graph_representations.npy'.format(network.name))

    dataset_predictions_path = os.path.join(saved_data_path,
                                            '{}_unseen_predictions.json'.format(network.name))

    task_embeddings = np.load(task_representation_path)

    dataset_embeddings = np.load(dataset_representation_path, allow_pickle=True)

    print("Dataset embedding shape: ", dataset_embeddings.shape)

    dataset_predictions = load_json(dataset_predictions_path)

    assert dataset_embeddings.shape[0] == dataset_data.shape[0]
    assert dataset_predictions.shape[0] == dataset_data.shape[0]

    # Filter out non-argumentative predictions

    dataset_predictions = np.argmax(dataset_predictions, axis=1)

    label_map = {key: np.argmax(value) for key, value in converter_info['label_map'].items()}
    inverse_label_map = {value: key for key, value in label_map.items()}

    argumentative_indexes = np.where(dataset_predictions != label_map[not_argumentative_key])[0]
    dataset_embeddings = dataset_embeddings[argumentative_indexes]
    dataset_predictions = dataset_predictions[argumentative_indexes]

    print("Filtering non-argumentative sentences shape: ", dataset_embeddings.shape)

    argumentative_data = dataset_data.iloc[argumentative_indexes]

    # Similarity

    # [#task_data, #dataset_samples]
    similarity_info = {}
    for similarity_op in similarity_ops:
        if similarity_op == 'cosine':
            similarity = cosine_similarity(a=task_embeddings, b=dataset_embeddings)
        else:
            similarity = dot_product(a=task_embeddings, b=dataset_embeddings)

        similarity_info[similarity_op] = similarity

    for arg_local_idx, (arg_idx, arg_pred) in enumerate(zip(argumentative_indexes, dataset_predictions)):

        # General info
        ensemble_info.setdefault(arg_idx, {})['Sentence'] = argumentative_data['Sentence'][arg_idx]
        ensemble_info.setdefault(arg_idx, {})['Paper ID'] = argumentative_data['Paper ID'][arg_idx]
        ensemble_info.setdefault(arg_idx, {})['Title'] = argumentative_data['Title'][arg_idx]
        ensemble_info.setdefault(arg_idx, {})['{}-Tag'.format(save_prefix)] = inverse_label_map[arg_pred]

        # Similarity info
        for similarity_op, similarity_matrix in similarity_info.items():
            for query_idx in np.arange(similarity_matrix.shape[0]):
                ensemble_info.setdefault(arg_idx, {})['{0}-{1}-{2}-Score'.format(similarity_op,
                                                                                 save_prefix,
                                                                                 query_idx)] = similarity_matrix[query_idx,
                                                                                                                 arg_local_idx]


# Compute ensemble info

print('Ensemble info size: ', len(ensemble_info))

# Adding missing predictions
for idx, item in ensemble_info.items():
    for save_prefix in save_prefixes:
        if '{}-Tag'.format(save_prefix) not in item:
            ensemble_info[idx]['{}-Tag'.format(save_prefix)] = not_argumentative_key

# Ensemble Tag
for idx, item in ensemble_info.items():
    c = Counter([item[key] for key in item.keys() if 'Tag' in key])
    count_list = list(c.items())
    ensemble_tag = sorted(count_list, key=lambda pair: pair[1], reverse=True)[0][0]
    ensemble_info[idx]['Ensemble Tag'] = ensemble_tag
    ensemble_info[idx]['Winning Models'] = [save_prefix for save_prefix in save_prefixes
                                            if item['{}-Tag'.format(save_prefix)] == ensemble_tag]

# Drop NA majority tagged
print('Dropping sentences considered not argumentative by majority')
ensemble_info = {key: value for key, value in ensemble_info.items() if value['Ensemble Tag'] != not_argumentative_key}
print('Ensemble info size: ', len(ensemble_info))

# Compute rankings
for idx, item in ensemble_info.items():
    for (query_idx, similarity_op) in product(np.arange(len(task_data_sentences)), similarity_ops):
        scores = [item['{0}-{1}-{2}-Score'.format(similarity_op, save_prefix, query_idx)]
                  for save_prefix in save_prefixes
                  if '{0}-{1}-{2}-Score'.format(similarity_op, save_prefix, query_idx) in item]
        ensemble_info[idx]['{0}-{1} Avg Score'] = np.mean(scores)


# Save info
if save_info:
    save_path = os.path.join(saved_data_path, 'ensemble_info.npy')
    np.save(save_path, ensemble_info)


