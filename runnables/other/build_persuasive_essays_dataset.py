"""

Builds a pandas.DataFrame for Persuasive Essays v2 dataset with the following columns:

Doc     Sentence        Label       Split

where Label :- [MajorClaim, Claim, Premise, Not-Argumentative]; Split :- [Train, Test]

...starting from disgusting brat-compliant format (so shitty jeez)

How to get sentence-level info:

1. Parse each document with CoreNLP -> sentences
2. Read annotations -> argumentative phrases
3. Match argument phrase with corresponding containing sentence

"""

import const_define as cd
import pandas as pd
import os
from tqdm import tqdm
from pycorenlp import StanfordCoreNLP
import simplejson as sj
from utility.log_utils import Logger
import numpy as np
from nltk import word_tokenize

logger = Logger.get_logger(__name__)


def get_annotations(filename, file_path):
    with open(os.path.join(file_path, '{}.ann'.format(filename)), 'r') as f:
        data = f.readlines()

    data = [line for line in data if line.startswith('T')]

    arg_data = []

    for line in data:
        arg_type = line.split('\t')[1].split(' ')[0]
        arg_start_idx = int(line.split('\t')[1].split(' ')[1])
        arg_end_idx = int(line.split('\t')[1].split(' ')[2])
        arg_sentence = line.split('\t')[2:]
        arg_sentence = ' '.join(arg_sentence).strip()
        arg_data.append((arg_start_idx, arg_end_idx, filename.split('.')[0], arg_sentence, arg_type))

    arg_data = sorted(arg_data, key=lambda item: item[0])

    return arg_data


def find_annotation(orig_sent, doc_annotations):
    # MajorClaim    Claim   Premise     Not-Argumentative
    labels = [0, 0, 0, 0]
    to_remove = []
    for idx, (ann_start_idx, ann_end_idx, ann_sent, ann_type) in enumerate(doc_annotations):
        if ann_sent.lower() in orig_sent.lower() or ' '.join(
                    word_tokenize(ann_sent)).lower() in orig_sent.lower():

            current_label = ann_type
            to_remove.append(idx)

            if current_label == 'MajorClaim':
                labels[0] = 1
            elif current_label == 'Claim':
                labels[1] = 1
            elif current_label == 'Premise':
                labels[2] = 1
        # elif sequence_pointer_max not in [ann_start_idx - offset, ann_end_idx + offset]:
        #     logger.warn('Sequence pointer not in range! Pointer = {0} -- Range = [{1}, {2}]'.format(sequence_pointer_max,
        #                                                                                            ann_start_idx - offset,
        #                                                                                            ann_end_idx + offset))

    if np.sum(labels) == 0:
        labels[-1] = 1
    else:
        for item in to_remove:
            del doc_annotations[item]

    return labels, doc_annotations


# Settings

data_path = os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'brat-project-final')
port = 9000

split_info = pd.read_csv(os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'train-test-split.csv'), sep=';')

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

df_data = {}

# warning_debug = 0
# total_not_found = 0

for doc_name in tqdm(os.listdir(data_path), leave=True, position=0):
    if not doc_name.endswith('.txt'):
        continue
    else:

        parsed_doc_name = doc_name.split('.')[0]

        # Get set split
        doc_split = split_info[split_info.ID == parsed_doc_name].SET.values[0]

        # Get argumentative annotations
        doc_annotations = get_annotations(filename=parsed_doc_name, file_path=data_path)

        # # Get sentences with CoreNLP
        # with open(os.path.join(data_path, doc_name), 'r') as f:
        #     doc_data = f.readlines()
        #
        # doc_data = ''.join(doc_data)

        doc_arguments = [item[3] for item in doc_annotations]
        doc_labels = [item[4] for item in doc_annotations]

        # print('Sentences retrieved: {}'.format(len(doc_arguments)))

        for arg_idx, (arg_sent, arg_label) in enumerate(zip(doc_arguments, doc_labels)):

            annotation = parser.annotate(arg_sent, properties={'timeout': 10000000})

            if annotation:
                try:
                    data = sj.loads(annotation)
                except Exception as e:
                    logger.info('Failed to annotate doc: {}'.format(doc_name))
                    logger.info('{}'.format(e))

            assert len(data['sentences']) == 1

            # Sentence
            tokens = data['sentences'][0]['tokens']
            tokens = [item['word'] for item in tokens]

            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()

            # Tree
            data_tree = data['sentences'][0]['parse'].replace('\n', ' ')
            data_tree = data_tree.split()
            data_tree = ' '.join(data_tree)

            # Dependency tree
            data_dep_tree = [tuple((dep['dep'],
                                    dep['governor'],
                                    dep['governorGloss'],
                                    dep['dependent'],
                                    dep['dependentGloss']))
                             for dep in data['sentences'][0]['basicDependencies']]

            # Add data to df
            df_data.setdefault('Sentence ID', []).append(arg_idx)
            df_data.setdefault('ID', []).append(parsed_doc_name)
            df_data.setdefault('Split', []).append(doc_split)
            df_data.setdefault('Sentence', []).append(parsed_sentence)
            df_data.setdefault('Tree', []).append(data_tree)
            df_data.setdefault('Dep_Tree', []).append(data_dep_tree)
            df_data.setdefault('Label', []).append(arg_label)

df = pd.DataFrame.from_dict(df_data)

df.to_csv(os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'dataset_final.csv'), index=False)
