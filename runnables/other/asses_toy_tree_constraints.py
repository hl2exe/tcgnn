import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from utility.graph_utils import get_subtree_info


# Step 2: mock pooling

def softmax(x, axis=-1):
    return np.exp(x) / np.sum(np.exp(x), axis=axis)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def mock_pooling(nodes, K, use_sigmoid=False):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    att_operator = sigmoid if use_sigmoid else softmax

    for node_idx in np.arange(len(nodes)):
        # hard_pooling = np.random.randint(low=0, high=K)
        # hard_encoding = np.zeros(K)
        # hard_encoding[hard_pooling] = 1.0
        pooling_matrix[node_idx] = att_operator(np.random.rand(K) * 5)
        # pooling_matrix[node_idx] = hard_encoding

    return pooling_matrix


def mock_root_pooling(nodes, K):
    pooling_matrix = np.zeros((K, len(nodes)), dtype=np.float32)
    for k in range(K):
        hot_encoding = np.zeros(len(nodes))
        rand_pos = np.random.randint(low=0, high=len(nodes))
        hot_encoding[rand_pos] = 1.
        pooling_matrix[k] = hot_encoding

    return pooling_matrix


def mock_soft_root_pooling(nodes, K):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    for node_idx in np.arange(len(nodes)):
        pooling_matrix[node_idx] = np.random.rand(K) * 5

    for k in range(K):
        pooling_matrix[:, k] = softmax(pooling_matrix[:, k])

    return pooling_matrix.transpose()


def auto_root_pooling(node_pooling, adjacency_matrix):
    # Node pooling -> N x K

    # Mask: 1 x N x N
    mask = np.tril(np.ones(adjacency_matrix.shape, dtype=node_pooling.dtype))
    mask -= np.eye(adjacency_matrix.shape[0], dtype=node_pooling.dtype)
    mask = mask.astype(np.float32)
    mask = mask[np.newaxis, :, :]

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    # adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # K x N
    tree_pooling = np.sum(exp_node_pooling * mask * adjacency_matrix * exp_node_pooling.transpose([0, 2, 1]), axis=-1)

    # Normalize by connectivity
    # connectivity_norm = np.sum(mask * adjacency_matrix, axis=-1)
    # connectivity_norm = np.maximum(connectivity_norm, 1.)
    # tree_pooling = tree_pooling / connectivity_norm
    # tree_pooling = tree_pooling / np.sum(tree_pooling, axis=1)[:, np.newaxis]
    tree_pooling = 1. - tree_pooling

    tree_pooling = tree_pooling * node_pooling.transpose()

    # Normalize
    tree_norm = np.sum(tree_pooling, axis=-1)
    tree_pooling = tree_pooling / tree_norm[:, np.newaxis]

    return tree_pooling


# Step 3: define constraints (the higher the better)

def tree_constraint(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> N x K

    root_pooling = np.transpose(root_pooling)

    # This is a little better than just normalizing by np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    pooling_mask = node_pooling >= 1e-3
    pooling_mask = pooling_mask.astype(np.float32)
    normalization = pooling_mask * np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # N x K
    span_pooling = np.matmul(1. - node_spans, node_pooling) / normalization

    # K x 1
    weighted_span_pooling = np.sum(root_pooling * span_pooling, axis=0)

    # K x 1
    return weighted_span_pooling


def contiguous_sequence_constraint(node_pooling, adjacency_matrix):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N

    # K x N x N
    node_pooling = node_pooling.transpose()
    node_pooling = node_pooling[:, :, np.newaxis]
    node_pooling = np.repeat(node_pooling, axis=-1, repeats=node_pooling.shape[1])
    node_pooling = np.transpose(node_pooling, [0, 2, 1])

    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=node_pooling.shape[0])

    horizontal_sim = node_pooling * adjacency_matrix
    vertical_sim = node_pooling * np.transpose(horizontal_sim, [0, 2, 1])

    # Count pooled nodes only
    # N x K
    mask = vertical_sim >= 1e-3
    mask = mask.astype(vertical_sim.dtype)

    # Reduce
    vertical_sim = np.sum(vertical_sim, axis=-1)
    vertical_sim = np.sum(vertical_sim, axis=-1)

    # K x 1
    mask = np.sum(mask, axis=-1)
    mask = np.sum(mask, axis=-1)

    return 1. - vertical_sim / mask


def contiguous_sequence_constraint_alt(node_pooling, adjacency_matrix):
    # N x N
    adjacency_eye = np.eye(adjacency_matrix.shape[0])
    partial_adjacency = adjacency_matrix - adjacency_eye * adjacency_matrix
    partial_adjacency = partial_adjacency / 2

    # K x N
    constraint = np.matmul(node_pooling.transpose(), partial_adjacency)
    # constraint = np.matmul(constraint, mask)

    # K x K
    constraint = np.matmul(constraint, node_pooling)

    # K x 1 (take only diagonal values, other values are all possible k-k combinations (k1k2, k2k1, etc..)
    constraint = np.sum(constraint * np.eye(constraint.shape[0]), axis=1)

    # K x 1
    normalization = node_pooling >= 1e-3
    normalization = normalization.astype(np.float32)
    normalization = np.sum(normalization, axis=0) - 1.
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # K x 1
    return 1 - (constraint / normalization)


def contiguous_sequence_constraint_complete(node_pooling, adjacency_matrix, node_spans, root_pooling):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # node_spans -> N x N
    # root_pooling -> K x N

    node_amount = node_spans.shape[1]
    meta_nodes_amount = node_pooling.shape[1]

    # Node spans

    # N x N x N
    node_spans = node_spans[:, :, np.newaxis]
    node_spans = np.repeat(node_spans, axis=-1, repeats=node_amount)
    node_spans = np.transpose(node_spans, [0, 2, 1])

    # Adjacency matrix
    # N x N x N
    adjacency_matrix = np.tile(adjacency_matrix.ravel(), (1, 1, node_amount))
    adjacency_matrix = adjacency_matrix.reshape(node_amount, node_amount, node_amount)

    # N x N x N
    boolean_matrix = np.minimum(2 - (node_spans + node_spans.transpose(0, 2, 1)), 1.)
    c_matrix = adjacency_matrix * boolean_matrix

    # N x N x N
    eye_matrix = np.eye(node_spans.shape[1])
    eye_matrix = np.tile(eye_matrix.ravel(), (1, 1, node_amount))
    eye_matrix = eye_matrix.reshape(node_amount, node_amount, node_amount)

    # N x N x N
    c_matrix = c_matrix - c_matrix * eye_matrix
    c_matrix = 0.5 * c_matrix

    # Node pooling
    # N x N x K
    node_pooling = np.tile(node_pooling.ravel(), (node_amount, 1, 1))
    node_pooling = node_pooling.reshape(node_amount, node_amount, meta_nodes_amount)

    # N x K x N
    per_root_constraint = np.matmul(node_pooling.transpose([0, 2, 1]), c_matrix)

    #  N x K x K
    per_root_constraint = np.matmul(per_root_constraint, node_pooling)

    # Reduce by taking only diagonal values

    # N x K x K
    root_eye_matrix = np.eye(meta_nodes_amount)
    root_eye_matrix = np.tile(root_eye_matrix.ravel(), (node_amount, 1, 1))
    root_eye_matrix = root_eye_matrix.reshape(node_amount, meta_nodes_amount, meta_nodes_amount)

    # N x K
    per_root_constraint = np.sum(per_root_constraint * root_eye_matrix, axis=-1)

    # K x 1
    constraint = np.sum(root_pooling.transpose() * per_root_constraint, axis=0)

    # Normalization factor
    mask_pooling = node_pooling >= 1e-3
    mask_pooling = mask_pooling.astype(np.float)

    # N x K x K
    normalization = np.matmul(mask_pooling.transpose(0, 2, 1), c_matrix)
    normalization = np.matmul(normalization, mask_pooling)

    # Reduce by taking only diagonal values

    # N x K
    normalization = np.sum(normalization * root_eye_matrix, axis=-1)

    # K x 1
    normalization = np.sum(root_pooling.transpose() * normalization, axis=0)
    normalization = np.maximum(normalization, np.ones_like(normalization))

    return constraint / normalization
    # return constraint


def disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # root_pooling -> K x N

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # General connectivity constraint
    connectivity_penalization = np.sum(exp_node_pooling * adjacency_matrix, axis=-1)

    connectivity_normalization = exp_node_pooling >= 1e-3
    connectivity_normalization = connectivity_normalization.astype(np.float32)
    connectivity_normalization = np.sum(connectivity_normalization * adjacency_matrix, axis=-1)
    connectivity_normalization = np.maximum(connectivity_normalization, np.ones_like(connectivity_normalization))

    # K x N
    connectivity_penalization = 1. - connectivity_penalization / connectivity_normalization

    node_pooling = node_pooling.transpose()

    # K x 1
    all_disconnect_constraint = np.sum(node_pooling * connectivity_penalization, axis=-1)
    root_node_residual = np.sum(root_pooling * node_pooling * connectivity_penalization, axis=-1)

    general_disconnect_normalization = node_pooling >= 1e-3
    general_disconnect_normalization = general_disconnect_normalization.astype(np.float32)
    general_disconnect_normalization = np.sum(general_disconnect_normalization, axis=-1)
    general_disconnect_normalization = np.maximum(general_disconnect_normalization,
                                                  np.ones_like(general_disconnect_normalization))

    # K x 1
    general_disconnect_constraint = (all_disconnect_constraint - root_node_residual) / general_disconnect_normalization

    # Root node connectivity constraint
    root_node_disconnect_constraint = alpha * root_node_residual

    root_node_disconnect_normalization = node_pooling >= 1e-3
    root_node_disconnect_normalization = root_node_disconnect_normalization.astype(np.float32)
    root_node_disconnect_normalization = np.sum(root_node_disconnect_normalization * root_pooling, axis=-1)
    root_node_disconnect_normalization = np.maximum(root_node_disconnect_normalization,
                                                    np.ones_like(root_node_disconnect_normalization))

    # K x 1
    root_node_disconnect_constraint = root_node_disconnect_constraint / root_node_disconnect_normalization

    return general_disconnect_constraint, root_node_disconnect_constraint


def unified_disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # root_pooling -> K x N

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # General connectivity constraint
    connectivity_penalization = np.sum(exp_node_pooling * adjacency_matrix, axis=-1)

    connectivity_normalization = exp_node_pooling >= 1e-3
    connectivity_normalization = connectivity_normalization.astype(np.float32)
    connectivity_normalization = np.sum(connectivity_normalization * adjacency_matrix, axis=-1)
    connectivity_normalization = np.maximum(connectivity_normalization, np.ones_like(connectivity_normalization))

    # K x N
    connectivity_penalization = 1. - connectivity_penalization / connectivity_normalization

    node_pooling = node_pooling.transpose()

    # K x 1
    unified_disconnect_coefficient = 1 + (alpha - 1) * root_pooling
    unified_disconnect_constraint = np.sum(unified_disconnect_coefficient * node_pooling * connectivity_penalization,
                                           axis=-1)

    general_disconnect_normalization = node_pooling >= 1e-3
    general_disconnect_normalization = general_disconnect_normalization.astype(np.float32)
    general_disconnect_normalization = np.sum(general_disconnect_normalization, axis=-1)
    general_disconnect_normalization = np.maximum(general_disconnect_normalization,
                                                  np.ones_like(general_disconnect_normalization))

    unified_disconnect_constraint = unified_disconnect_constraint / general_disconnect_normalization

    return unified_disconnect_constraint


def st_specific_constraint(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> N x K

    root_pooling = np.transpose(root_pooling)

    # This is a little better than just normalizing by np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    pooling_mask = node_pooling >= 1e-3
    pooling_mask = pooling_mask.astype(np.float32)
    normalization = pooling_mask * np.sum(node_spans, axis=1)[:, np.newaxis]
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # N x K
    span_pooling = np.matmul(node_spans, 1 - node_pooling) / normalization

    # K x 1
    weighted_span_pooling = np.sum(root_pooling * span_pooling, axis=0)

    # K x 1
    return weighted_span_pooling


def sst_specific_constraint(node_pooling, adjacency_matrix):
    n_matrix = np.minimum(1. - np.sum(adjacency_matrix, axis=1), 1.)

    sst_constraint = np.zeros((node_pooling.shape[1]), dtype=np.float)
    for k in range(node_pooling.shape[1]):
        for i in range(node_pooling.shape[0]):
            inner_sum = 0.
            inner_norm = 0.
            for j in range(i + 1, node_pooling.shape[0]):
                inner_sum += adjacency_matrix[i, j] * (1 - node_pooling[j, k]) * n_matrix[j]
                inner_norm += adjacency_matrix[i, j] * n_matrix[j]
            inner_norm = np.maximum(inner_norm, 1.)
            inner_sum = inner_sum / inner_norm
            sst_constraint[k] += node_pooling[i, k] * inner_sum

    return sst_constraint


# Step 1: get tree info

print('[Step 1] Retrieving tree info...')

nodes = np.array(['S', "N", "Mary", "VP", "V", "brought", "NP", "D", "a", "N", "cat", "PP", "IN", "to", "N", "school"])
adjacency_matrix = np.zeros((nodes.shape[0], nodes.shape[0]))

# Fill values
adjacency_matrix[0, 1] = 1
adjacency_matrix[0, 3] = 1

adjacency_matrix[1, 0] = 1
adjacency_matrix[1, 2] = 1

adjacency_matrix[2, 1] = 1

adjacency_matrix[3, 0] = 1
adjacency_matrix[3, 4] = 1
adjacency_matrix[3, 6] = 1
adjacency_matrix[3, 11] = 1

adjacency_matrix[4, 3] = 1
adjacency_matrix[4, 5] = 1

adjacency_matrix[5, 4] = 1

adjacency_matrix[6, 3] = 1
adjacency_matrix[6, 7] = 1
adjacency_matrix[6, 9] = 1

adjacency_matrix[7, 6] = 1
adjacency_matrix[7, 8] = 1

adjacency_matrix[8, 7] = 1

adjacency_matrix[9, 6] = 1
adjacency_matrix[9, 10] = 1

adjacency_matrix[10, 9] = 1

adjacency_matrix[11, 3] = 1
adjacency_matrix[11, 12] = 1
adjacency_matrix[11, 14] = 1

adjacency_matrix[12, 11] = 1
adjacency_matrix[12, 13] = 1

adjacency_matrix[13, 12] = 1

adjacency_matrix[14, 11] = 1
adjacency_matrix[14, 15] = 1

adjacency_matrix[15, 14] = 1

node_idxs = np.arange(len(nodes))
node_depths = np.array([0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 2, 3, 4, 3, 4])

st_subtree_nodes, _, _, _, _, _ = get_subtree_info(node_idxs=node_idxs,
                                                   depths=node_depths,
                                                   is_directional=False,
                                                   kernel='stk')

sst_subtree_nodes, _, _, _, _, _ = get_subtree_info(node_idxs=node_idxs,
                                                    depths=node_depths,
                                                    is_directional=False,
                                                    kernel='sstk')


def build_node_spans(nodes, subtree_nodes):
    node_spans = np.zeros((len(nodes), len(nodes)))

    for subtree in subtree_nodes:
        node_spans[subtree[0], subtree] = 1.

    for node in nodes:
        node_spans[node, node] = 1.

    return node_spans


node_spans = build_node_spans(nodes=node_idxs, subtree_nodes=st_subtree_nodes)


# Test 1: Evaluate constraints on random configurations


def evaluate_on_random_configurations(K, iterations, subtree_nodes, alpha=2., use_sigmoid=False):
    exp_tree_constraint = []
    exp_contiguous_sequence_constraint = []
    exp_contiguous_sequence_complete_constraint = []
    exp_general_disconnect_constraint = []
    exp_root_node_disconnect_constraint = []
    exp_unified_disconnect_constraint = []

    exp_subtrees = np.zeros((iterations * K, len(nodes)))
    for it in tqdm(range(iterations)):
        node_pooling = mock_pooling(nodes, K, use_sigmoid=use_sigmoid)
        # root_pooling = mock_root_pooling(nodes, K)
        root_pooling = auto_root_pooling(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)

        exp_tree_constraint.extend(tree_constraint(node_pooling=node_pooling, root_pooling=root_pooling,
                                                   node_spans=node_spans))
        exp_contiguous_sequence_constraint.extend(contiguous_sequence_constraint_alt(node_pooling=node_pooling,
                                                                                     adjacency_matrix=adjacency_matrix))

        exp_contiguous_sequence_complete_constraint.extend(
            contiguous_sequence_constraint_complete(node_pooling=node_pooling,
                                                    adjacency_matrix=adjacency_matrix,
                                                    root_pooling=root_pooling,
                                                    node_spans=node_spans))

        general_disconnect_constraint, \
        root_node_disconnect_constraint = disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                        root_pooling=root_pooling,
                                                                        adjacency_matrix=adjacency_matrix,
                                                                        alpha=alpha)

        unified_disconnect_constraint = unified_disconnected_nodes_constraint(node_pooling=node_pooling,
                                                                              root_pooling=root_pooling,
                                                                              adjacency_matrix=adjacency_matrix,
                                                                              alpha=alpha)

        exp_general_disconnect_constraint.extend(general_disconnect_constraint)
        exp_root_node_disconnect_constraint.extend(root_node_disconnect_constraint)
        exp_unified_disconnect_constraint.extend(unified_disconnect_constraint)

        exp_subtrees[it * K: it * K + K] = node_pooling.transpose()

    exp_tree_constraint = np.array(exp_tree_constraint)
    exp_contiguous_sequence_constraint = np.array(exp_contiguous_sequence_constraint)
    exp_contiguous_sequence_complete_constraint = np.array(exp_contiguous_sequence_complete_constraint)
    exp_general_disconnect_constraint = np.array(exp_general_disconnect_constraint)
    exp_root_node_disconnect_constraint = np.array(exp_root_node_disconnect_constraint)
    exp_unified_disconnect_constraint = np.array(exp_unified_disconnect_constraint)

    print('[Step 2] Completed!')
    print('[Step 3] Assessing constraints on real subtrees...')

    # transform real subtrees
    added_subtrees = np.zeros((len(subtree_nodes), len(nodes)))
    added_tree_constraint = np.zeros(len(subtree_nodes))
    added_contiguous_sequence_constraint = np.zeros(len(subtree_nodes))
    added_contiguous_sequence_complete_constraint = np.zeros(len(subtree_nodes))
    added_general_disconnect_constraint = np.zeros(len(subtree_nodes))
    added_root_node_disconnect_constraint = np.zeros(len(subtree_nodes))
    added_unified_node_disconnect_constraint = np.zeros(len(subtree_nodes))
    for idx, subtree in enumerate(tqdm(subtree_nodes)):
        subtree_encoding = np.zeros(len(nodes))
        subtree_encoding[subtree] = 1.
        added_subtrees[idx] = subtree_encoding

        subtree_root_pooling = np.zeros(len(nodes))
        subtree_root_pooling[subtree[0]] = 1.
        subtree_root_pooling = subtree_root_pooling[np.newaxis, :]

        subtree_pooling = subtree_encoding[:, np.newaxis]
        added_tree_constraint[idx] = tree_constraint(node_pooling=subtree_pooling,
                                                     root_pooling=subtree_root_pooling,
                                                     node_spans=node_spans)[0]
        added_contiguous_sequence_constraint[idx] = contiguous_sequence_constraint_alt(node_pooling=subtree_pooling,
                                                                                       adjacency_matrix=adjacency_matrix)[
            0]

        added_contiguous_sequence_complete_constraint[idx] = \
            contiguous_sequence_constraint_complete(node_pooling=subtree_pooling,
                                                    root_pooling=subtree_root_pooling,
                                                    node_spans=node_spans,
                                                    adjacency_matrix=adjacency_matrix)[0]

        general_disconnect_constraint, \
        root_node_disconnect_constraint = disconnected_nodes_constraint(node_pooling=subtree_pooling,
                                                                        root_pooling=subtree_root_pooling,
                                                                        adjacency_matrix=adjacency_matrix,
                                                                        alpha=alpha)

        unified_disconnect_constraint = disconnected_nodes_constraint(node_pooling=subtree_pooling,
                                                                      root_pooling=subtree_root_pooling,
                                                                      adjacency_matrix=adjacency_matrix,
                                                                      alpha=alpha)

        added_general_disconnect_constraint[idx] = general_disconnect_constraint[0]
        added_root_node_disconnect_constraint[idx] = root_node_disconnect_constraint[0]
        added_unified_node_disconnect_constraint[idx] = unified_disconnect_constraint[0]

    plt.scatter(x=np.arange(iterations * K), y=exp_tree_constraint, c='green', marker='o')
    plt.scatter(x=np.arange(added_tree_constraint.shape[0]) + iterations * K, y=added_tree_constraint, c='red',
                marker='x')
    plt.legend(['Generated', 'Valid'])
    plt.title("Tree constraint (the lower the better)")
    plt.show()

    plt.scatter(x=np.arange(iterations * K), y=exp_contiguous_sequence_constraint, c='green', marker='o')
    plt.scatter(x=np.arange(added_contiguous_sequence_constraint.shape[0]) + iterations * K,
                y=added_contiguous_sequence_constraint, c='red', marker='x')
    plt.legend(['Generated', 'Valid'])
    plt.title("Contiguous sequence constraint (the lower the better)")
    plt.show()

    plt.scatter(x=np.arange(iterations * K),
                y=exp_contiguous_sequence_constraint + exp_contiguous_sequence_complete_constraint, c='green',
                marker='o')
    plt.scatter(x=np.arange(added_contiguous_sequence_complete_constraint.shape[0]) + iterations * K,
                y=added_contiguous_sequence_complete_constraint + added_contiguous_sequence_constraint, c='red',
                marker='x')
    plt.legend(['Generated', 'Valid'])
    plt.title("Complete contiguous sequence constraint (the lower the better)")
    plt.show()

    plt.scatter(x=np.arange(iterations * K), y=exp_general_disconnect_constraint, c='green', marker='o')
    plt.scatter(x=np.arange(added_general_disconnect_constraint.shape[0]) + iterations * K,
                y=added_general_disconnect_constraint, c='red', marker='x')
    plt.legend(['Generated', 'Valid'])
    plt.title("General disconnect constraint (the lower the better)")
    plt.show()

    plt.scatter(x=np.arange(iterations * K), y=exp_root_node_disconnect_constraint, c='green', marker='o')
    plt.scatter(x=np.arange(added_root_node_disconnect_constraint.shape[0]) + iterations * K,
                y=added_root_node_disconnect_constraint, c='red', marker='x')
    plt.legend(['Generated', 'Valid'])
    plt.title("Root node disconnect constraint (the lower the better, alpha = {})".format(alpha))
    plt.show()

    plt.scatter(x=np.arange(iterations * K), y=exp_unified_disconnect_constraint, c='green', marker='o')
    plt.scatter(x=np.arange(added_unified_node_disconnect_constraint.shape[0]) + iterations * K,
                y=added_unified_node_disconnect_constraint, c='red', marker='x')
    plt.legend(['Generated', 'Valid'])
    plt.title("Unified disconnect constraint (the lower the better, alpha = {})".format(alpha))
    plt.show()


# evaluate_on_random_configurations(K=3, iterations=30, subtree_nodes=st_subtree_nodes)
# evaluate_on_random_configurations(K=3, iterations=30, subtree_nodes=sst_subtree_nodes)


def evaluate_specific_constraints(subtree_nodes, st_amount, sst_amount):
    st_constraint = np.zeros(len(subtree_nodes))
    sst_constraint = np.zeros(len(subtree_nodes))

    for idx, subtree in enumerate(tqdm(subtree_nodes)):
        subtree_encoding = np.zeros(len(nodes))
        subtree_encoding[subtree] = 1.
        subtree_pooling = subtree_encoding[:, np.newaxis]

        subtree_root_pooling = np.zeros(len(nodes))
        subtree_root_pooling[subtree[0]] = 1.
        subtree_root_pooling = subtree_root_pooling[np.newaxis, :]

        st_constraint[idx] = st_specific_constraint(node_pooling=subtree_pooling,
                                                    root_pooling=subtree_root_pooling,
                                                    node_spans=node_spans)[0]

        sst_constraint[idx] = sst_specific_constraint(node_pooling=subtree_pooling,
                                                      adjacency_matrix=adjacency_matrix)[0]

    plt.scatter(x=np.arange(st_amount), y=st_constraint[:st_amount], c='green', marker='o')
    plt.scatter(x=np.arange(sst_amount) + st_amount, y=st_constraint[st_amount:], c='red', marker='o')
    plt.legend(['ST', 'SST'])
    plt.title("ST specific constraint")
    plt.show()

    plt.scatter(x=np.arange(st_amount), y=sst_constraint[:st_amount], c='green', marker='o')
    plt.scatter(x=np.arange(sst_amount) + st_amount, y=sst_constraint[st_amount:], c='red', marker='o')
    plt.legend(['ST', 'SST'])
    plt.title("SST specific constraint")
    plt.show()


evaluate_specific_constraints(subtree_nodes=st_subtree_nodes + sst_subtree_nodes,
                              st_amount=len(st_subtree_nodes),
                              sst_amount=len(sst_subtree_nodes))