"""

Builds UKP aspect dataset

"""

import os
from pycorenlp import StanfordCoreNLP
import pandas as pd
import const_define as cd
import simplejson as sj
from utility.log_utils import Logger
from tqdm import tqdm

logger = Logger.get_logger(__name__)


def parse_sentence(sentence):
    sentence = sentence.replace("%", "%25")
    sentence = sentence.replace("\\+", "%2B")
    annotation = parser.annotate(sentence, properties={'timeout': 10000000, 'annotators': 'parse,depparse'})

    if annotation:
        try:
            data = sj.loads(annotation)
        except Exception as e:
            logger.error('{}'.format(e))
            raise e

    best_idx = 0
    if len(data['sentences']) != 1:
        best_length = None
        lengths = []
        for item_idx, item in enumerate(data['sentences']):
            tokens = item['tokens']

            if best_length is None:
                best_length = len(tokens)
                best_idx = item_idx
            elif len(tokens) > best_length:
                best_length = len(tokens)
                best_idx = item_idx

            lengths.append(len(tokens))

            sent_dbg = ' '.join([item['word'] for item in tokens])
            logger.info('Sentence: {}'.format(sent_dbg))

        # if (len(lengths) == 2 and lengths[1] != 1) or len(lengths) > 2:
        #     input("More than one sentence detected! Stopping for debug. Press any key to keep the longest one")

    # Sentence
    tokens = data['sentences'][best_idx]['tokens']
    tokens = [item['word'] for item in tokens]

    parsed_sentence = ' '.join(tokens)
    parsed_sentence = parsed_sentence.lower()

    # Tree
    data_tree = data['sentences'][0]['parse'].replace('\n', ' ')
    data_tree = data_tree.split()
    data_tree = ' '.join(data_tree)

    # Dependency tree
    data_dep_tree = [tuple((dep['dep'],
                            dep['governor'],
                            dep['governorGloss'],
                            dep['dependent'],
                            dep['dependentGloss']))
                     for dep in data['sentences'][0]['basicDependencies']]

    return parsed_sentence, data_tree, data_dep_tree


# Settings

data_path = os.path.join(cd.UKP_ASPECT_DIR, 'UKP_ASPECT.tsv')
data = pd.read_csv(data_path, sep='\t')
port = 9000

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

df_data = {}

for row_idx, row in tqdm(data.iterrows()):

    sentence_1 = row['sentence_1']
    sentence_2 = row['sentence_2']

    parsed_sentence_1, sentence_tree_1, sentence_dep_tree_1 = parse_sentence(sentence_1)
    parsed_sentence_2, sentence_tree_2, sentence_dep_tree_2 = parse_sentence(sentence_2)

    # Add data to df
    df_data.setdefault('ID', []).append(row_idx)
    df_data.setdefault('topic', []).append(row['topic'])
    df_data.setdefault('label', []).append(row['label'])

    df_data.setdefault('sentence_1', []).append(parsed_sentence_1)
    df_data.setdefault('sentence_tree_1', []).append(sentence_tree_1)
    df_data.setdefault('sentence_dep_tree_1', []).append(sentence_dep_tree_1)

    df_data.setdefault('sentence_2', []).append(parsed_sentence_2)
    df_data.setdefault('sentence_tree_2', []).append(sentence_tree_2)
    df_data.setdefault('sentence_dep_tree_2', []).append(sentence_dep_tree_2)


df = pd.DataFrame.from_dict(df_data)

df.to_csv(os.path.join(cd.UKP_ASPECT_DIR, 'UKP_ASPECT_mod.tsv'), index=False)