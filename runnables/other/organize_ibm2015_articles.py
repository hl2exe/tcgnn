"""

Organizes articles by Article id and splits them into sentences via CoreNLP

"""

import os

import pandas as pd
import simplejson as sj
from pycorenlp import StanfordCoreNLP

import const_define as cd
import numpy as np
from tqdm import tqdm

# Settings

port = 9000

# Load article data

# Topic	Title	article Id
article_path = os.path.join(cd.IBM2015_DIR, 'articles.txt')
article_data = pd.read_csv(article_path, sep='\t')
article_ids = list(set(article_data['article Id'].values.tolist()))
# article_ids = os.listdir(os.path.join(cd.IBM2015_DIR, 'articles'))

# article Id    List of sentences
organized_data = {}
organized_tree_data = {}
organized_dep_tree_data = {}

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

# article_ids = [
#     "2718_a",
#     "2718_b",
#     "2718_c",
#     "2718_d",
#     "2717_a",
#     "2717_b",
#     "2790_a",
#     "2790_b",
#     "332_a",
#     "332_b",
#     "332_c"
# ]

article_ids.remove(2718)
article_ids.remove(2717)
article_ids.remove(2790)
article_ids.remove(332)

article_ids += [
    "2718_a",
    "2718_b",
    "2718_c",
    "2718_d",
    "2717_a",
    "2717_b",
    "2790_a",
    "2790_b",
    "332_a",
    "332_b",
    "332_c"
]

for art_id in tqdm(article_ids):
    print('Considering article: {}'.format(art_id))

    item_path = os.path.join(cd.IBM2015_DIR, 'articles', 'clean_{}.txt'.format(art_id))
    # item_path = os.path.join(cd.IBM2015_DIR, 'articles', art_id)
    with open(item_path, 'r') as f:
        item_data = f.readlines()

    item_data = ''.join(item_data)

    # Parse article
    annotation = parser.annotate(item_data, properties={'timeout': 10000000,
                                                        'max_char_length': 10000000,
                                                        'annotators': 'parse'})
    item_sentences = []
    item_trees = []
    item_dep_trees = []

    if annotation:
        try:
            data = sj.loads(annotation)
        except Exception as e:
            print('Failed to annotate article: {}'.format(art_id))

        sentences_found = len(data['sentences'])
        print('Sentences retrieved: {}'.format(sentences_found))
        for sent_idx in range(sentences_found):

            # Sentences
            tokens = data['sentences'][sent_idx]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            item_sentences.append(parsed_sentence)

            # Trees
            data_tree = data['sentences'][sent_idx]['parse'].replace('\n', ' ')
            data_tree = data_tree.split()
            data_tree = ' '.join(data_tree)
            item_trees.append(data_tree)

            # Dependency tree
            try:
                data_dep_tree = [tuple((dep['dep'],
                                        dep['governor'],
                                        dep['governorGloss'],
                                        dep['dependent'],
                                        dep['dependentGloss']))
                                 for dep in data['sentences'][sent_idx]['basicDependencies']]
                item_dep_trees.append(data_dep_tree)
            except KeyError as err:
                item_dep_trees.append("")

    organized_data[art_id] = item_sentences
    organized_tree_data[art_id] = item_trees
    organized_dep_tree_data[art_id] = item_dep_trees

save_path = os.path.join(cd.IBM2015_DIR, 'organized_articles.npy')
np.save(save_path, organized_data)

save_path = os.path.join(cd.IBM2015_DIR, 'organized_articles_trees.npy')
np.save(save_path, organized_tree_data)

save_path = os.path.join(cd.IBM2015_DIR, 'organized_articles_dep_trees.npy')
np.save(save_path, organized_dep_tree_data)