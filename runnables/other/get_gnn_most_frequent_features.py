"""

Extracts meta-nodes for a given

1. Get pooling matrix P
2. Extract columns of P
3. Convert node indexes to words
4. Accumulate all word sequences
5. Get greatest sequence (?)
6. Count most common (if repeated sequences)

TODO: compute n-grams between sequences to find smaller groups
TODO: report statistics about meta-nodes: #single clusters (distinct), #average max length

"""

import os
import const_define as cd
import const_define as exp_cd
import numpy as np
from utility.json_utils import load_json
from utility.pipeline_utils import get_dataset_fn
from utility.python_utils import flatten, merge
from utility.data_utils import get_data_config_id
from data_converter import DataConverterFactory
from data_processor import ProcessorFactory
from data_loader import DataLoaderFactory
from collections import Counter
from tokenization import TokenizerFactory
from tqdm import tqdm
from collections import OrderedDict
from utility.draw_graph_utils import draw_tree
import matplotlib.pyplot as plt


class StructuredNodes(object):

    def __init__(self, node_indices, node_vocab_indices, adjacency_matrix, labels):
        self.node_indices = np.array(node_indices)
        self.node_vocab_indices = node_vocab_indices
        self.labels = labels
        self.adjacency_matrix = adjacency_matrix

    @classmethod
    def build(cls, node_group, adjacency_matrix, node_indices, node_mask, inv_vocab):
        """
        Gets longest nodes sequence
        """

        all_sequences = []

        # Used to get children
        triu_adjacency_matrix = np.triu(adjacency_matrix) - np.eye(adjacency_matrix.shape[0])
        triu_adjacency_matrix = np.maximum(triu_adjacency_matrix, 0.)

        # Remove padding
        triu_adjacency_matrix *= node_mask[:, np.newaxis]
        triu_adjacency_matrix *= node_mask[np.newaxis, :]

        # Nodes are already in depth-first order
        for node_idx, node in enumerate(node_group):

            # Skip node
            if node == 0.0 or node_indices[node_idx] == 0:
                continue

            node_sequence = [node_idx]
            current_head = node_idx

            open_list = np.where(triu_adjacency_matrix[current_head])[0].tolist()

            while len(open_list):
                current_head = open_list.pop(0)

                if node_group[current_head] == .0 or node_indices[current_head] == 0:
                    continue

                node_sequence.append(current_head)
                head_open_list = np.where(triu_adjacency_matrix[current_head])[0].tolist()
                open_list = head_open_list + open_list

            all_sequences.append(node_sequence)

        all_lengths = [len(item) for item in all_sequences]
        if len(all_lengths):
            longest_sequence = all_sequences[np.argmax(all_lengths)]
            sequence_indices = node_indices[longest_sequence].tolist()
            labels = cls.get_labels(node_indices=longest_sequence,
                                    node_vocab_indices=sequence_indices,
                                    inv_vocab=inv_vocab)
        else:
            longest_sequence = []
            sequence_indices = []
            labels = {}

        return StructuredNodes(node_indices=longest_sequence,
                               node_vocab_indices=sequence_indices,
                               labels=labels,
                               adjacency_matrix=triu_adjacency_matrix)

    # TODO: adjust this
    @classmethod
    def plot_tree_data(cls, pooling_matrix, adjacency_matrix, node_mask, cluster_idx):
        draw_tree(nodes=np.array(pooling_matrix),
                  adjacency_matrix=adjacency_matrix,
                  pad_mask=node_mask,
                  title_name='Node pooling (cluster = {0})'.format(cluster_idx + 1))

    @classmethod
    def get_labels(cls, node_indices, node_vocab_indices, inv_vocab):
        return OrderedDict([(node_idx, str(inv_vocab[node_vocab_idx]) if inv_vocab[node_vocab_idx] != 1 else 'UNK')
                            for node_idx, node_vocab_idx in zip(node_indices, node_vocab_indices)])

    def _get_qtree_format(self, nodes):
        result = ''
        for node in nodes:
            node_label = self.labels[node]
            children = np.where(self.adjacency_matrix[node])[0].tolist()
            children = [node for node in children if node in self.node_indices]

            # If only one child and
            if len(children) == 1:
                result += '[.{0} {1} ]'.format(node_label, self._get_qtree_format(children))
                # the child is a leaf node
                # if np.sum(self.adjacency_matrix[children[0]]) == 0:
                #     child_label = self.labels[children[0]]
                #     result += '[.{0} {1} ]'.format(node_label, child_label)
                # else:
                #     result += '[.{0} {1} ]'.format(node_label, self._get_qtree_format(children))

            if len(children) == 0:
                if np.sum(self.adjacency_matrix[node]) == 0:
                    result += node_label
                else:
                    result += '[.{0} ]'.format(node_label)

            if len(children) > 1:
                result += '[.{0} {1} ]'.format(node_label, self._get_qtree_format(children))

        return result

    def get_qtree_format(self):
        if not len(self.labels) or not len(self.node_indices):
            return ''

        result = '\Tree'
        root_node = self.node_indices[0]
        root_label = self.labels[root_node]
        children = np.where(self.adjacency_matrix[root_node])[0].tolist()
        children = [node for node in children if node in self.node_indices]
        result += '[.{0} {1} ]'.format(root_label, self._get_qtree_format(children))

        return result

    def get_sequential_format(self):
        return '-'.join(self.labels.values())


def get_node_clusters(node_ids, pad_mask, pooling_matrix, adjacency_matrix,
                      filter_class, label_values, inv_vocab, predictions,
                      threshold=None):
    # Filter samples by class and where model predicts well
    label_values = np.argmax(label_values, axis=1)
    indexes_to_save = np.where((label_values == filter_class) & (label_values == predictions))[0]

    print("Considering {} samples".format(indexes_to_save.shape[0]))

    node_ids = node_ids[indexes_to_save]
    pad_mask = pad_mask[indexes_to_save]
    pooling_matrix = pooling_matrix[indexes_to_save] * pad_mask[:, :, np.newaxis]
    adjacency_matrix = adjacency_matrix[indexes_to_save]

    # [Optional] Thresholding
    if threshold is not None:
        pooling_matrix[pooling_matrix >= threshold] = 1.0
        pooling_matrix[pooling_matrix < threshold] = 0.0
    else:
        pooling_matrix = np.round(pooling_matrix)

    # Get groups -> each column of the pooling matrix is a node cluster
    clusters = pooling_matrix.shape[-1]
    node_clusters = []

    for cluster_idx in range(clusters):
        current_clusters = pooling_matrix[:, :, cluster_idx].squeeze().tolist()

        for cluster, adj_matrix, node_indices, node_mask in tqdm(
                zip(current_clusters, adjacency_matrix, node_ids, pad_mask)):
            node_cluster = StructuredNodes.build(node_group=cluster,
                                                 adjacency_matrix=adj_matrix,
                                                 node_indices=node_indices,
                                                 inv_vocab=inv_vocab,
                                                 node_mask=node_mask)
            if len(node_cluster.node_indices):
                node_clusters.append(node_cluster)

            # Debug
            # StructuredNodes.plot_tree_data(pooling_matrix=cluster,
            #                                adjacency_matrix=adj_matrix,
            #                                node_mask=node_mask,
            #                                cluster_idx=cluster_idx)
            #
            # print(node_cluster.get_sequential_format())
            # plt.show()
            # print()

    return node_clusters


def compute_average_intensity_reduction(pooling_matrix, pad_mask):
    total_nodes = np.sum(pad_mask, axis=-1)

    pooling_matrix *= pad_mask[:, :, np.newaxis]

    # samples, k
    pooling_intensities = np.sum(np.sum(pooling_matrix, axis=1), axis=-1)

    pooling_percentage = pooling_intensities / total_nodes

    return np.mean(pooling_percentage)


model_type = "persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2"
test_name = "f1_es_PTK_binary"

# IBM2015 -> cd.CV_DIR
# UKP -> cd.LOO_DIR
# PE, AbstRCT -> cd.TRAIN_AND_TEST_DIR
test_dir = cd.TRAIN_AND_TEST_DIR

# IBM2015 -> np.arange(1)
# PE -> np.arange(3)
# AbstRCT -> np.arange(10)
# UKP -> np.arange(10)
repetitions = np.arange(3)
# IBM2015 ['0', '1', '2', '3', '4']
# PE, AbstRCT [None]
# UKP ['topic1', 'topic2', ..., 'topicN']
save_prefixes = [
    # '0',
    # '1',
    # '2',
    # '3',
    # '4'
    None
]

# IBM2015 -> "fold"
# PE, AbstRCT -> ""
# UKP -> "key"
test_prefix = ""

# 'train', 'test'
set_split = 'train'

# keep this high if filter_unique = True
K = 300

# minimum fragments lengths
minimum_cluster_length = 3

# activation threshold (applies a rounding)
rounding_threshold = 0.3

# if enabled, it just considers fragments that are unique to each class.
filter_unique = True

model_path = os.path.join(test_dir, model_type, test_name)

model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

training_config = load_json(os.path.join(model_path, cd.JSON_TRAINING_CONFIG_NAME))

distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

per_class_clusters = {}

test_predictions = load_json(os.path.join(model_path, '{}_predictions.json'.format(set_split)))

total_pooling_reduction = []

for repetition in repetitions:
    for save_prefix in save_prefixes:

        if save_prefix is not None:
            corrected_save_prefix = test_prefix + '_' + save_prefix
        else:
            corrected_save_prefix = save_prefix

        network_name = model_type if save_prefix is None else '{0}_{1}'.format(model_type, corrected_save_prefix)

        # Step 1: Load test dataset

        data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
        data_loader_type = data_loader_config['type']
        data_loader_info = data_loader_config['configs'][data_loader_type]
        loader_additional_info = {key: value['value'] for key, value in model_config.items()
                                  if 'data_loader' in value['flags']}
        data_loader_info = merge(data_loader_info, loader_additional_info)

        data_loader = DataLoaderFactory.factory(data_loader_type)

        data_handle = data_loader.load(**data_loader_info)

        config_args = {key: arg['value']
                       for key, arg in model_config.items()
                       if 'processor' in arg['flags']
                       or 'tokenizer' in arg['flags']
                       or 'converter' in arg['flags']
                       or 'data_loader' in arg['flags']}
        config_args = flatten(config_args)
        config_args = merge(config_args, data_loader_info)
        config_args_tuple = [(key, value) for key, value in config_args.items()]
        config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

        config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
        model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                       data_handle.data_name,
                                       model_type)
        config_id = get_data_config_id(filepath=model_base_path, config=config_name)
        data_path = os.path.join(model_base_path, str(config_id))

        converter_type = exp_cd.MODEL_CONFIG[model_type]['converter']
        converter_factory = DataConverterFactory()
        converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
            filepath=data_path,
            prefix=corrected_save_prefix)

        tokenizer_type = exp_cd.MODEL_CONFIG[model_type]['tokenizer']
        tokenizer_factory = TokenizerFactory()
        tokenizer_args = {key: arg['value'] for key, arg in model_config.items() if 'tokenizer' in arg['flags']}
        tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=data_path,
                                                                                          prefix=corrected_save_prefix)

        processor_type = exp_cd.MODEL_CONFIG[model_type]['processor']
        processor_args = {key: arg['value'] for key, arg in model_config.items() if 'processor' in arg['flags']}
        processor_args['loader_info'] = data_handle.get_additional_info()
        processor_factory = ProcessorFactory()
        processor = processor_factory.factory(processor_type, **processor_args)

        # Build converter
        converter_type = exp_cd.MODEL_CONFIG[model_type]['converter']
        converter_factory = DataConverterFactory()
        converter_args = {key: arg['value'] for key, arg in model_config.items() if 'converter' in arg['flags']}
        converter = converter_factory.factory(converter_type, **converter_args)

        test_filepath_name = '{}_data'.format(set_split) if corrected_save_prefix is None else '{0}_data_{1}'.format(set_split,
            corrected_save_prefix)
        test_filepath = os.path.join(data_path, test_filepath_name)

        test_data = get_dataset_fn(filepath=test_filepath,
                                   batch_size=training_config['batch_size'],
                                   name_to_features=converter.feature_class.get_mappings(converter_info),
                                   selector=converter.feature_class.get_dataset_selector(),
                                   is_training=False,
                                   prefetch_amount=distributed_info['prefetch_amount'])

        test_node_ids = test_data().map(lambda x, y: x['node_ids'])
        test_node_ids_np = []
        for item in test_node_ids.take(10000000):
            test_node_ids_np.extend(item.numpy())

        test_node_ids_np = np.array(test_node_ids_np)

        test_labels = test_data().map(lambda x, y: y)
        test_labels_np = []
        for item in test_labels.take(10000000):
            test_labels_np.extend(item.numpy())

        test_labels_np = np.array(test_labels_np)

        print(test_node_ids_np.shape)
        print(test_labels_np.shape)

        # Test 2: Load pooling matrix

        if save_prefix is not None:
            network_name = network_name.replace(corrected_save_prefix,
                                                'repetition_{0}_{1}'.format(repetition, corrected_save_prefix))
        else:
            network_name += '_repetition_{0}'.format(repetition)

        pooling_matrix_path = os.path.join(model_path, '{0}_{1}_pooling_matrices.npy'.format(network_name, set_split))
        pooling_matrix = np.load(pooling_matrix_path)

        pad_mask_path = os.path.join(model_path, '{0}_{1}_pad_masks.npy'.format(network_name, set_split))
        pad_mask = np.load(pad_mask_path)

        adjacency_matrix_path = os.path.join(model_path,
                                             '{0}_{1}_adjacency_matrices.npy'.format(network_name, set_split))
        adjacency_matrix = np.load(adjacency_matrix_path)

        assert pooling_matrix.shape[0] == adjacency_matrix.shape[0] == pad_mask.shape[0]

        pooling_reduction_percentage = compute_average_intensity_reduction(pooling_matrix=pooling_matrix,
                                                                           pad_mask=pad_mask)
        total_pooling_reduction.append(pooling_reduction_percentage)

        # Test 3: Get most frequent terms

        label_map = converter_info['label_map']

        inv_vocab = {value: key for key, value in tokenizer_info['vocab'].items()}

        print('Label map: ', label_map)

        if save_prefix is not None:
            current_predictions = test_predictions[save_prefix]
        if len(repetitions) > 1:
            if save_prefix is not None:
                current_predictions = test_predictions[repetition]
            else:
                current_predictions = test_predictions[str(repetition)]

        current_predictions = current_predictions.reshape(-1, 2)
        current_predictions = np.argmax(current_predictions, axis=-1)

        # node_ids, gate_values, filter_class, label_values, inv_vocab, K=20
        for class_name, class_value in label_map.items():

            if 'NA' in class_name:
                continue

            node_clusters = get_node_clusters(node_ids=test_node_ids_np,
                                              pooling_matrix=pooling_matrix,
                                              filter_class=np.argmax(class_value),
                                              label_values=test_labels_np,
                                              inv_vocab=inv_vocab,
                                              predictions=current_predictions,
                                              threshold=rounding_threshold,
                                              adjacency_matrix=adjacency_matrix,
                                              pad_mask=pad_mask)

            per_class_clusters.setdefault(class_name, []).extend(node_clusters)


for class_name, class_clusters, in per_class_clusters.items():
    print("Class -> ", class_name)
    class_str_clusters = [item.get_sequential_format() for item in class_clusters]
    cluster_mapping = {cluster_str: cluster_obj for cluster_str, cluster_obj in zip(class_str_clusters, class_clusters)}

    counter = Counter(class_str_clusters)
    top_K_clusters = counter.most_common(n=K)

    print('Most K={0} frequent terms for class {1}:\n'.format(K, class_name))
    for cluster in top_K_clusters:
        cluster_obj = cluster_mapping[cluster[0]]
        if len(cluster_obj.node_indices) >= minimum_cluster_length:
            is_distinct = True
            if filter_unique:
                for other_class_name, other_class_clusters in per_class_clusters.items():
                    if other_class_name != class_name:
                        other_class_str_clusters = [item.get_sequential_format() for item in other_class_clusters]
                        if cluster[0] in other_class_str_clusters:
                            is_distinct = False

            if is_distinct:
                print('{0} <- {1}\n'.format(cluster[1], cluster_obj.get_qtree_format()))
    print()
    print()


intersection_mapping = {}
for class_name, class_clusters in per_class_clusters.items():
    class_str_clusters = [item.get_sequential_format() for item in class_clusters]
    for other_class_name, other_class_clusters in per_class_clusters.items():
        if other_class_name != class_name:
            other_class_str_clusters = set([item.get_sequential_format() for item in other_class_clusters])
            intersection = [item for item in class_str_clusters if item in other_class_str_clusters]

            # Compute intersection
            intersection_mapping['{0}_{1}'.format(class_name, other_class_name)] = '{0} ({1})'.format(len(intersection), len(intersection) / len(class_clusters))


print("Intersection mappings -> \n{0}".format(intersection_mapping))


print("Average pooling intensity reduction -> {}".format(np.mean(total_pooling_reduction)))
print("Std pooling intensity reduction -> {}".format(np.std(total_pooling_reduction)))