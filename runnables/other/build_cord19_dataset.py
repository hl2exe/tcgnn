""""

Builds CORD-19 dataset from JSON files

DataFrame format:

"Paper ID"  Title   Sentence    Section     "Section ID"    Tree

"""

from utility.json_utils import load_json
import simplejson as sj
import pandas as pd
import os
import const_define as cd
from tqdm import tqdm
from pycorenlp import StanfordCoreNLP
import multiprocessing as mp


def split_into_sentences(parser, text):
    parsed_sentences = []
    parsed_trees = []

    annotation = parser.annotate(text, properties={'timeout': 10000000, 'annotators': 'parse'})

    if annotation:
        try:
            data = sj.loads(annotation)
        except Exception as e:
            print('Failed to annotate text: {}'.format(text))
            return parsed_sentences, parsed_trees

    sentences_found = len(data['sentences'])
    # print('Sentences retrieved: {}'.format(sentences_found))

    for sent_idx in range(sentences_found):
        # Sentence parsing
        tokens = data['sentences'][sent_idx]['tokens']
        tokens = [item['word'] for item in tokens]
        parsed_sentence = ' '.join(tokens)
        parsed_sentence = parsed_sentence.lower()
        parsed_sentences.append(parsed_sentence)

        # Tree parsing
        data_tree = data['sentences'][sent_idx]['parse'].replace('\n', ' ')
        data_tree = data_tree.split()
        data_tree = ' '.join(data_tree)
        parsed_trees.append(data_tree)

    return parsed_sentences, parsed_trees


def parse_text_blocks(parser, df_info, abstract_data, paper_id, title):
    for block_id, abstract_block in enumerate(abstract_data):

        sentences, trees = split_into_sentences(parser, abstract_block['text'])

        for sent, tree in zip(sentences, trees):
            df_info.setdefault('Sentence', []).append(sent)
            df_info.setdefault('Tree', []).append(tree)
            df_info.setdefault('Section', []).append(abstract_block['section'])
            df_info.setdefault('Section ID', []).append(block_id)
            df_info.setdefault('Paper ID', []).append(paper_id)
            df_info.setdefault('Title', []).append(title)

    return df_info


def parsing_routine(port, files_to_parse, chunk_id, checkpoint_size=10000):

    parser = StanfordCoreNLP('http://localhost:{}'.format(port))

    next_checkpoint = checkpoint_size
    curr_step = 0
    df_info = {}

    for (filename, dir_name) in tqdm(files_to_parse):
        sub_path = os.path.join(cd.CORD19_DIR, dir_name, filename)

        data_json = load_json(sub_path)

        # Paper ID
        paper_id = data_json['paper_id']

        # Title
        title = data_json['metadata']['title']

        # Abstract
        df_info = parse_text_blocks(parser, df_info, data_json['abstract'], paper_id, title)

        # Body
        df_info = parse_text_blocks(parser, df_info, data_json['body_text'], paper_id, title)

        # Save intermediate output
        if curr_step >= next_checkpoint:
            print('Checkpoint! Step == {}'.format(next_checkpoint))
            check_df = pd.DataFrame.from_dict(df_info)
            save_path = os.path.join(cd.CORD19_DIR, 'dataset_chunk_{0}_cp_{1}.csv'.format(chunk_id, curr_step))
            check_df.to_csv(save_path, index=False)
            next_checkpoint += checkpoint_size

        curr_step = len(df_info['Sentence'])

    print('All done! Saving complete dataset...')

    df = pd.DataFrame.from_dict(df_info)

    save_path = os.path.join(cd.CORD19_DIR, 'dataset_chunk_{}.csv'.format(chunk_id))
    df.to_csv(save_path, index=False)

    return None


# Settings

if __name__ == '__main__':

    init_port = 9000

    checkpoint_size = 10000

    chunk_size = 800
    max_servers_per_time = 1

    df_info = {}
    curr_step = 0

    files_to_parse = [filename for filename in os.listdir(cd.CORD19_DIR)
                      if os.path.isdir(os.path.join(cd.CORD19_DIR, filename))]
    files_to_parse = [(filename, dir_name)
                      for dir_name in files_to_parse
                      for filename in os.listdir(os.path.join(cd.CORD19_DIR, dir_name))]

    total_chunks = int(len(files_to_parse) / chunk_size)

    chunk_ranges = [(chunk * chunk_size, chunk * chunk_size + chunk_size) for chunk in range(total_chunks)]
    chunk_ids = list(range(total_chunks))
    ports = [init_port + (chunk % max_servers_per_time) for chunk in range(total_chunks)]

    chunk_ranges = chunk_ranges[13:]
    chunk_ids = chunk_ids[13:]
    ports = ports[13:]

    cv_pool = mp.Pool(processes=max_servers_per_time)

    results = [cv_pool.apply_async(func=parsing_routine,
                                   args=(
                                       port_id,
                                       files_to_parse[ranges[0]:ranges[1]],
                                       chunk_id,
                                       checkpoint_size
                                   ))
               for ranges, chunk_id, port_id in zip(chunk_ranges, chunk_ids, ports)]

    output = [p.get() for p in results]

    cv_pool.close()
    cv_pool.join()
