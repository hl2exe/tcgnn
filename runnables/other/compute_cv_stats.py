"""

Simple script that computes useful info about cross-validation results (v2 only)

1. Average and standard deviation (over all repetitions)
2. Per-fold best metrics

** BEFORE RUNNING **

1. Change the test unique identifier 'test_name' variable.
2. Change the 'model_type' variable as to match your tested model.
3. Make sure the first element if 'metrics' variable is the metric you wish to find the best repetitions for.

"""

from utility.json_utils import load_json
import os
import const_define as cd
import numpy as np
from utility.python_utils import flatten, merge
from utility.data_utils import get_data_config_id
from data_converter import DataConverterFactory
import const_define as exp_cd


def compute_mean_and_std(info, metric):
    values = info[metric]
    values = np.array(values)

    if len(values.shape) == 1:
        values = values[np.newaxis, :]

    per_repetition_mean = np.mean(values, axis=1)
    values_mean = np.mean(per_repetition_mean)
    values_std = np.std(per_repetition_mean)

    return values_mean, values_std


def compute_per_class_mean_and_std(info, metric):
    values = info[metric]
    values = np.array(values)

    if len(values.shape) == 2:
        values = values[np.newaxis, :, :]

    per_repetition_mean = np.mean(values, axis=1)
    values_mean = np.mean(per_repetition_mean, axis=0)
    values_std = np.std(per_repetition_mean, axis=0)

    return values_mean, values_std


model_type = "ibm2015_bert-base-uncased"
test_name = "evidence"
metric = 'binary_f1_score'
per_class_metric = 'per_class_f1_score'
prefix = 'fold_0'
dataset_name = 'IBM2015CV'

model_path = os.path.join(cd.CV_DIR, model_type, test_name)

validation_info = load_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME))
test_info = load_json(os.path.join(model_path, cd.JSON_TEST_INFO_NAME))

# Compute avg and std values

val_mean, val_std = compute_mean_and_std(validation_info, metric)

print('[Validation - {0}] {1} +/- {2}'.format(metric, val_mean, val_std))

test_mean, test_std = compute_mean_and_std(test_info, metric)

print('[Test - {0}] {1} +/- {2}'.format(metric, test_mean, test_std))

# Per class avg and std values

network_args = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

data_loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
data_loader_info = data_loader_info['configs'][data_loader_info['type']]

config_args = {key: arg['value']
               for key, arg in network_args.items()
               if 'processor' in arg['flags']
               or 'tokenizer' in arg['flags']
               or 'converter' in arg['flags']}
config_args = flatten(config_args)
config_args = merge(config_args, data_loader_info)
config_args_tuple = [(key, value) for key, value in config_args.items()]
config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
info_base_path = os.path.join(cd.TESTS_DATA_DIR,
                              dataset_name,
                              model_type)
config_id = get_data_config_id(filepath=info_base_path, config=config_name)
info_path = os.path.join(info_base_path, str(config_id))

converter_type = exp_cd.MODEL_CONFIG[model_type]['converter']
converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
    filepath=info_path,
    prefix=prefix)

label_map = converter_info['label_map']

print('Label map: ', label_map)

val_pc_mean, val_pc_std = compute_per_class_mean_and_std(validation_info, per_class_metric)

# print('[Validation per class - {0}] {1} +/- {2}'.format(metric, val_pc_mean, val_pc_std))

test_pc_mean, test_pc_std = compute_per_class_mean_and_std(test_info, per_class_metric)

# print('[Test per class - {0}] {1} +/- {2}'.format(metric, test_pc_mean, test_pc_std))

for key, value in label_map.items():
    key_index = np.argmax(value)
    print('Validation - {0} - {1}: {2} +/- {3}'.format(per_class_metric,
                                                       key,
                                                       val_pc_mean[key_index],
                                                       val_pc_std[key_index]))

for key, value in label_map.items():
    key_index = np.argmax(value)
    print('Test - {0} - {1}: {2} +/- {3}'.format(per_class_metric,
                                                       key,
                                                       test_pc_mean[key_index],
                                                       test_pc_std[key_index]))