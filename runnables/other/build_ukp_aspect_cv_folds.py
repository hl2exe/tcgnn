"""

@Author: Federico Ruggeri

@Date: 26/03/2019

Builds UKP Aspect cv folds (4 folds)

"""

import os

import numpy as np

import const_define as cd
from data_loader import UKPAspectLoader
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json

if __name__ == '__main__':
    # Step 1: load dataset
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))['configs']

    loader = UKPAspectLoader()

    data_handle = loader.load(**data_loader_config['ukp_aspect'])

    data = data_handle.data

    topic_list = np.unique(data_handle.data.topic.values)

    cv = PrebuiltCV(n_splits=4, cv_type='kfold',
                    shuffle=True, random_state=None, held_out_key='test')
    cv.build_all_sets_folds(X=topic_list, y=topic_list, validation_n_splits=6)

    base_path = os.path.join(cd.PROJECT_DIR, 'prebuilt_folds')

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    folds_save_path = os.path.join(base_path, 'UKP_Aspect_splits_4_kfold.json')
    cv.save_folds(folds_save_path, tolist=True)

    list_save_path = os.path.join(base_path, 'UKP_Aspect_splits_4_kfold.txt')

    with open(list_save_path, 'w') as f:
        f.writelines(os.linesep.join(topic_list))
