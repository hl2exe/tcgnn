"""

Given IBM2015 data, builds the following csv file:

Topic Id    Topic   Sentence    Tree       Label    Data-set


"""

import pandas as pd
import numpy as np
import os
import const_define as cd
from utility.preprocessing_utils import filter_line
from nltk import word_tokenize
from pycorenlp import StanfordCoreNLP
from tqdm import tqdm
import json

# Step 0: Settings

filtering_threshold = 5
port = 9000

# Step 1: Load IBM2015 data

data_path = os.path.join(cd.IBM2015_DIR, '{}.txt')

# Topic 	Claim original text	    Claim corrected version
claim_data = pd.read_csv(data_path.format('claims'), sep='\t')[['Topic', 'Claim original text']]

# Topic 	Title	    article Id
article_data = pd.read_csv(data_path.format('articles'), sep='\t')[['Topic', 'article Id']]

# Topic 	Evidence	Tags
evidence_data = pd.read_csv(data_path.format('fixed_evidence'))[['Topic', 'Evidence']]

# Topic id  	Topic   	Data-set
motions = pd.read_csv(data_path.format('motions'), sep='\t')

# Step 2: Load article data

# articled Id : list of sentences
organized_articles = np.load(os.path.join(cd.IBM2015_DIR, 'organized_articles.npy'), allow_pickle=True).item()
organized_articles_trees = np.load(os.path.join(cd.IBM2015_DIR, 'organized_articles_trees.npy'),
                                   allow_pickle=True).item()


# Step 3: Filter article data (very short sentences are removed)

def parse_sentence(sentence):
    return filter_line(sentence, function_names=['sentence_to_lower'])


def filter_sentences(sentences, threshold):
    counts = list(map(lambda text: len(word_tokenize(text)), sentences))
    counts = np.array(counts)
    filter_indexes = np.argwhere(counts >= threshold).ravel()
    return np.array(sentences)[filter_indexes].tolist(), filter_indexes


# Parse sentences
organized_articles = {key: list(map(parse_sentence, value)) for key, value in organized_articles.items()}


# Filter sentences
def filter_on_sentences(articles, trees):
    filtered_organized_articles = {}
    filtered_organized_articles_trees = {}

    for key, item in articles.items():
        sentences, indexes = filter_sentences(item, filtering_threshold)
        filtered_organized_articles[key] = sentences
        filtered_organized_articles_trees[key] = np.array(trees[key])[indexes].tolist()

    return filtered_organized_articles, filtered_organized_articles_trees


organized_articles, organized_articles_trees = filter_on_sentences(organized_articles, organized_articles_trees)


# Step 5: Build dataset

# Merge arguments
claims = claim_data['Claim original text'].values
claim_topics = claim_data['Topic'].values

evidences = evidence_data['Evidence'].values
evidence_topics = evidence_data['Topic'].values

arguments = np.concatenate((claims, evidences))
argument_topics = np.concatenate((claim_topics, evidence_topics))
argument_labels = ['C'] * claims.shape[0] + ['E'] * evidences.shape[0]

argument_df = pd.DataFrame.from_dict({'Topic': argument_topics, 'Sentence': arguments, 'Label': argument_labels})

# Topic      Sentence   Topic Id    Data-set    Label
argument_df = argument_df.merge(motions, on='Topic', how='left')

# Build article DataFrame
article_sentences = []
article_trees = []
article_ids = []
for key, value in organized_articles.items():
    for sentence, sentence_tree in zip(value, organized_articles_trees[key]):
        article_sentences.append(sentence)
        article_trees.append(sentence_tree)
        article_ids.append(key)

article_df = pd.DataFrame.from_dict({'article Id': article_ids, 'Sentence': article_sentences})

# Topic  article Id   Sentence
article_df = article_df.merge(article_data, on='article Id', how='left')[['Topic', 'Sentence']]

# Topic     Topic Id    Sentence       Data-set
article_df = article_df.merge(motions, on='Topic', how='left')
article_labels = ['NOT-ARG'] * article_df.shape[0]
article_df['Label'] = article_labels

# Topic     Topic Id    Sentence    Data-set    Label
dataset = pd.concat((argument_df, article_df))


# Step 4: Build Dependency tree for each non-filtered sentence

def parse_corenlp(sentences, corenlp):
    trees = []
    parsed_sentences = []
    for sentence in tqdm(sentences):
        annotation = corenlp.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'parse'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            # Sentences
            tokens = data['sentences'][0]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            parsed_sentences.append(parsed_sentence)

            data_tree = data['sentences'][0]['parse'].replace('\n', ' ')
            data_tree = data_tree.split()
            data_tree = ' '.join(data_tree)
            trees.append(data_tree)

    return parsed_sentences, trees


argument_sentences = dataset['Sentence'].values[:argument_df.shape[0]]
nlp = StanfordCoreNLP('http://localhost:{}'.format(port))
argument_sentences, argument_trees = parse_corenlp(argument_sentences, nlp)

np.save('argument_sentences', argument_sentences)
np.save('argument_trees', argument_trees)

dataset['Tree'] = np.concatenate((argument_trees, article_trees))
dataset['Sentence'] = np.concatenate((argument_sentences, dataset.Sentence.values[argument_sentences.shape[0]:]))

# Filter on trees

dataset = dataset[dataset.Tree.str.startswith('(ROOT')]

# Step 6: Save dataset

save_path = os.path.join(cd.IBM2015_DIR, 'dataset.csv')
dataset.to_csv(save_path)
