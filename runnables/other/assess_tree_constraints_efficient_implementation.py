import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from utility.draw_graph_utils import draw_tree
from utility.graph_utils import retrieve_hierarchical_tree_adj_info, get_nodes_depths

import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

# tf.config.experimental_run_functions_eagerly(True)

# Step 0: settings

use_sigmoid = False
alpha = 0.5

# Step 1: get tree info

print('[Step 1] Retrieving tree info...')

tree_text = "(ROOT (S (NP (JJ manual) (NN harvest)) (VP (VBZ prevents) (NP (JJ severe) (JJ environmental) (NNS damages)))))"
nodes, _, adjacency_matrix, subtree_nodes = retrieve_hierarchical_tree_adj_info(tree_text=tree_text, kernel='stk')
node_indices = np.arange(len(nodes))

depths = get_nodes_depths(tree_text)
max_depth = max(depths)


def build_node_spans(nodes, subtree_nodes):
    node_spans = np.zeros((len(nodes), len(nodes)))

    for subtree in subtree_nodes:
        node_spans[subtree[0], subtree] = 1.

    for node in nodes:
        node_spans[node, node] = 1.

    node_spans[0, :] = 1.0

    return node_spans


node_spans = build_node_spans(nodes=node_indices, subtree_nodes=subtree_nodes)

print('[Step 1] Completed!')


# Step 2: mock pooling

def softmax(x, axis=-1):
    return np.exp(x) / np.sum(np.exp(x), axis=axis, keepdims=True)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def mock_pooling(nodes, K, use_sigmoid=False):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    att_operator = sigmoid if use_sigmoid else softmax

    for node_idx in np.arange(len(nodes)):
        # hard_pooling = np.random.randint(low=0, high=K)
        # hard_encoding = np.zeros(K)
        # hard_encoding[hard_pooling] = 1.0
        pooling_matrix[node_idx] = att_operator(np.random.rand(K) * 5)
        # pooling_matrix[node_idx] = hard_encoding

    return pooling_matrix


def mock_root_pooling(nodes, K):
    pooling_matrix = np.zeros((K, len(nodes)), dtype=np.float32)
    for k in range(K):
        hot_encoding = np.zeros(len(nodes))
        rand_pos = np.random.randint(low=0, high=len(nodes))
        hot_encoding[rand_pos] = 1.
        pooling_matrix[k] = hot_encoding

    return pooling_matrix


def mock_soft_root_pooling(nodes, K):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    for node_idx in np.arange(len(nodes)):
        pooling_matrix[node_idx] = np.random.rand(K) * 5

    for k in range(K):
        pooling_matrix[:, k] = softmax(pooling_matrix[:, k])

    return pooling_matrix.transpose()


# Step 3: define constraints (the lower the better)

def auto_tree_pooling_matrix(node_pooling, adjacency_matrix):
    # Node pooling -> N x K

    # Mask: 1 x N x N
    mask = np.tril(np.ones(adjacency_matrix.shape, dtype=node_pooling.dtype))
    mask -= np.eye(adjacency_matrix.shape[0], dtype=node_pooling.dtype)
    mask = mask.astype(np.float32)
    mask = mask[np.newaxis, :, :]

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    # adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # K x N
    tree_pooling = np.sum(exp_node_pooling * mask * adjacency_matrix * exp_node_pooling.transpose([0, 2, 1]), axis=-1)

    # Normalize by connectivity
    connectivity_norm = np.sum(mask * adjacency_matrix, axis=-1)
    connectivity_norm = np.maximum(connectivity_norm, 1.)
    tree_pooling = tree_pooling / connectivity_norm
    tree_pooling = tree_pooling / np.sum(tree_pooling, axis=1)[:, np.newaxis]
    tree_pooling = 1. - tree_pooling

    tree_pooling = tree_pooling * node_pooling.transpose()

    # Normalize
    tree_norm = np.sum(tree_pooling, axis=-1)
    tree_pooling = tree_pooling / tree_norm[:, np.newaxis]

    return tree_pooling


def auto_tree_pooling_naive(node_pooling, adjacency_matrix):
    # Node pooling -> N x K

    root_pooling = np.zeros_like(node_pooling)
    for k in range(node_pooling.shape[1]):
        for i in range(node_pooling.shape[0]):
            total_sum = 0.
            total_norm = 0.
            for j in range(i - 1, 0):
                total_sum += adjacency_matrix[i, j] * node_pooling[i, k] * node_pooling[j, k]
                total_norm += adjacency_matrix[i, j]
            total_norm = np.maximum(total_norm, 1.)
            print('Norm: ', total_norm)
            print('Sum: ', total_sum)
            root_pooling[i, k] = node_pooling[i, k] * (1. - (total_sum / total_norm))

    root_pooling = root_pooling / np.sum(root_pooling, axis=0)[np.newaxis, :]

    return root_pooling.transpose()


def tree_constraint(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> N x K

    root_pooling = np.transpose(root_pooling)

    # This is a little better than just normalizing by np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    pooling_mask = node_pooling >= 1e-3
    pooling_mask = pooling_mask.astype(np.float32)
    normalization = pooling_mask * np.sum(1. - node_spans, axis=1)[:, np.newaxis]
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # N x K
    span_pooling = np.matmul(1. - node_spans, node_pooling) / normalization

    # K x 1
    weighted_span_pooling = np.sum(root_pooling * span_pooling, axis=0)

    # K x 1
    return weighted_span_pooling


def tree_constraint_alt(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> K x N

    meta_node_amount = node_pooling.shape[1]

    # N x K
    numerator = np.matmul(1 - node_spans, node_pooling)

    mask = node_pooling >= 1e-3
    mask = mask.astype(np.float)

    # N x K
    denominator = np.matmul(1 - node_spans, mask)
    denominator = np.maximum(denominator, 1.)

    # K x K
    constraint = numerator / denominator
    constraint = np.matmul(root_pooling, constraint)

    # K x 1
    constraint = np.sum(constraint * np.eye(meta_node_amount), axis=-1)

    return constraint


def mod_tree_constraint_alt(node_pooling, root_pooling, node_spans):
    # node_pooling -> N x K
    # node_spans -> N x N
    # root_pooling -> K x N

    meta_node_amount = node_pooling.shape[1]

    # K x N
    compl_root_pooling = 1. - root_pooling
    adjusted_node_pooling = node_pooling * compl_root_pooling.transpose()

    # N x K
    numerator = np.matmul(1 - node_spans, adjusted_node_pooling)

    mask = node_pooling >= 1e-3
    mask = mask.astype(np.float)

    # N x K
    denominator = np.matmul(1 - node_spans, mask)
    denominator = np.maximum(denominator, 1.)

    # K x K
    constraint = numerator / denominator
    constraint = np.matmul(root_pooling, constraint)

    # K x 1
    constraint = np.sum(constraint * np.eye(meta_node_amount), axis=-1)

    return constraint


def tree_constraint_naive(node_pooling, root_pooling, node_spans):
    tree_plus = np.zeros((node_pooling.shape[1]))
    for k in range(node_pooling.shape[1]):
        for i in range(node_pooling.shape[0]):
            total_sum = 0.
            total_norm = 0.
            for j in range(node_pooling.shape[0]):
                total_sum += node_pooling[j, k] * (1 - node_spans[i, j])
                total_norm += float(node_pooling[j, k] >= 1e-3) * (1 - node_spans[i, j])
            total_norm = np.maximum(total_norm, 1.)
            total_sum = total_sum / total_norm
            total_sum *= root_pooling[k, i]
            tree_plus[k] += total_sum

    return tree_plus


def mod_tree_constraint_naive(node_pooling, root_pooling, node_spans):
    tree_plus = np.zeros((node_pooling.shape[1]))
    for k in range(node_pooling.shape[1]):
        for i in range(node_pooling.shape[0]):
            total_sum = 0.
            total_norm = 0.
            for j in range(node_pooling.shape[0]):
                total_sum += node_pooling[j, k] * (1. - node_spans[i, j]) * (1. - root_pooling[k, j])
                total_norm += float(node_pooling[j, k] >= 1e-3) * (1 - node_spans[i, j])
            total_norm = np.maximum(total_norm, 1.)
            total_sum = total_sum / total_norm
            total_sum *= root_pooling[k, i]
            tree_plus[k] += total_sum

    return tree_plus


# ------


def contiguous_sequence_constraint(node_pooling, adjacency_matrix):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N

    # Mask: 1 x N x N
    mask = np.triu(np.ones_like(adjacency_matrix))
    mask -= np.tril(np.ones_like(adjacency_matrix))
    mask[mask == -1] = 0.
    mask = mask.astype(np.float32)
    mask = mask[np.newaxis, :, :]

    # K x 1
    normalization = node_pooling >= 1e-3
    normalization = normalization.astype(np.float32)
    normalization = np.sum(normalization, axis=0) - 1.
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # Node Pooling
    # K x N x N
    node_pooling = node_pooling.transpose()
    node_pooling = node_pooling[:, :, np.newaxis]
    node_pooling = np.repeat(node_pooling, axis=-1, repeats=node_pooling.shape[1])
    node_pooling = np.transpose(node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=node_pooling.shape[0])

    # K x 1
    chain_strengths = node_pooling * mask * adjacency_matrix * np.transpose(node_pooling, [0, 2, 1])
    chain_strengths = np.sum(chain_strengths, -1)
    chain_strengths = np.sum(chain_strengths, -1)

    return 1 - chain_strengths / normalization


def contiguous_sequence_constraint_matrix(node_pooling, adjacency_matrix):
    # N x N
    adjacency_eye = np.eye(adjacency_matrix.shape[0])
    partial_adjacency = adjacency_matrix - adjacency_eye * adjacency_matrix
    partial_adjacency = partial_adjacency / 2

    # K x N
    constraint = np.matmul(node_pooling.transpose(), partial_adjacency)
    # constraint = np.matmul(constraint, mask)

    # K x K
    constraint = np.matmul(constraint, node_pooling)

    # K x 1 (take only diagonal values, other values are all possible k-k combinations (k1k2, k2k1, etc..)
    constraint = np.sum(constraint * np.eye(constraint.shape[0]), axis=1)

    # K x 1
    normalization = node_pooling >= 1e-3
    normalization = normalization.astype(np.float32)
    normalization = np.sum(normalization, axis=0) - 1.
    normalization = np.maximum(normalization, np.ones_like(normalization))

    # K x 1
    return 1 - (constraint / normalization)


def contiguous_sequence_constraint_naive(node_pooling, adjacency_matrix):
    contiguous_sequence = np.zeros((node_pooling.shape[1]))
    for k in range(node_pooling.shape[1]):
        total_sum = 0.
        total_norm = 0.
        for i in range(node_pooling.shape[0]):
            for j in range(i, node_pooling.shape[0]):
                total_sum += adjacency_matrix[i, j] * node_pooling[i, k] * node_pooling[j, k]
            total_norm += float(node_pooling[i, k] >= 1e-3)
        total_norm -= 1
        total_norm = np.maximum(total_norm, 1.)
        total_sum = total_sum / total_norm
        contiguous_sequence[k] = total_sum

    return 1. - contiguous_sequence


# ------


def contiguous_sequence_constraint_complete(node_pooling, adjacency_matrix, node_spans, root_pooling):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # node_spans -> N x N
    # root_pooling -> K x N

    node_amount = node_spans.shape[1]
    meta_nodes_amount = node_pooling.shape[1]

    # Node spans

    # N x N x N
    node_spans = node_spans[:, :, np.newaxis]
    node_spans = np.repeat(node_spans, axis=-1, repeats=node_amount)
    node_spans = np.transpose(node_spans, [0, 2, 1])

    # Adjacency matrix
    # N x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]

    # N x N x N
    boolean_matrix = np.minimum(2 - (node_spans + node_spans.transpose(0, 2, 1)), 1.)
    c_matrix = adjacency_matrix * boolean_matrix

    # N x N x N
    eye_matrix = np.eye(node_spans.shape[1])
    eye_matrix = eye_matrix[np.newaxis, :, :]

    # N x N x N
    c_matrix = c_matrix - c_matrix * eye_matrix
    c_matrix = 0.5 * c_matrix

    # Node pooling
    # N x N x K
    node_pooling = np.tile(node_pooling.ravel(), (node_amount, 1, 1))
    node_pooling = node_pooling.reshape(node_amount, node_amount, meta_nodes_amount)

    # N x K x N
    per_root_constraint = np.matmul(node_pooling.transpose([0, 2, 1]), c_matrix)

    #  N x K x K
    per_root_constraint = np.matmul(per_root_constraint, node_pooling)

    # Reduce by taking only diagonal values

    # N x K x K
    root_eye_matrix = np.eye(meta_nodes_amount)
    root_eye_matrix = np.tile(root_eye_matrix.ravel(), (node_amount, 1, 1))
    root_eye_matrix = root_eye_matrix.reshape(node_amount, meta_nodes_amount, meta_nodes_amount)

    # N x K
    per_root_constraint = np.sum(per_root_constraint * root_eye_matrix, axis=-1)

    # K x 1
    constraint = np.sum(root_pooling.transpose() * per_root_constraint, axis=0)

    # Normalization factor
    mask_pooling = node_pooling >= 1e-3
    mask_pooling = mask_pooling.astype(np.float)

    # N x K x K
    normalization = np.matmul(mask_pooling.transpose(0, 2, 1), c_matrix)
    normalization = np.matmul(normalization, mask_pooling)

    # Reduce by taking only diagonal values

    # N x K
    normalization = np.sum(normalization * root_eye_matrix, axis=-1)

    # K x 1
    normalization = np.sum(root_pooling.transpose() * normalization, axis=0)
    normalization = np.maximum(normalization, np.ones_like(normalization))

    return 1. - constraint / normalization
    # return constraint


def contiguous_sequence_constraint_complete_naive(node_pooling, adjacency_matrix, node_spans, root_pooling):
    span_chain_strengths = np.zeros((node_pooling.shape[1]))
    for k in range(node_pooling.shape[1]):
        total_sum = 0.
        total_norm = 0.
        for l in range(node_spans.shape[0]):
            inner_sum = 0.
            inner_norm = 0.
            for i in range(node_spans.shape[0]):
                for j in range(i + 1, node_spans.shape[0]):
                    inner_sum += adjacency_matrix[i, j] * node_pooling[i, k] * node_pooling[j, k] * min(
                        1 - node_spans[l, i] + 1 - node_spans[l, j], 1.)
                    # inner_sum += adjacency_matrix[i, j] * min(2 - node_spans[l, i] - node_spans[l, j], 1.)
                    inner_norm += adjacency_matrix[i, j] * float(node_pooling[i, k] >= 1e-3) * float(
                        node_pooling[j, k] >= 1e-3) * min(1 - node_spans[l, i] + 1 - node_spans[l, j], 1.)
            total_sum += root_pooling[k, l] * inner_sum
            # total_sum += inner_sum
            total_norm += root_pooling[k, l] * inner_norm

        total_norm = np.maximum(total_norm, 1.)
        span_chain_strengths[k] = total_sum / total_norm

    return 1. - span_chain_strengths
    # return span_chain_strengths


# ------

def disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # root_pooling -> K x N

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # General connectivity constraint
    connectivity_penalization = np.sum(exp_node_pooling * adjacency_matrix, axis=-1)

    connectivity_normalization = exp_node_pooling >= 1e-3
    connectivity_normalization = connectivity_normalization.astype(np.float32)
    connectivity_normalization = np.sum(connectivity_normalization * adjacency_matrix, axis=-1)
    connectivity_normalization = np.maximum(connectivity_normalization, np.ones_like(connectivity_normalization))

    # K x N
    connectivity_penalization = 1. - connectivity_penalization / connectivity_normalization

    node_pooling = node_pooling.transpose()

    # K x 1
    all_disconnect_constraint = np.sum(node_pooling * connectivity_penalization, axis=-1)
    root_node_residual = np.sum(root_pooling * node_pooling * connectivity_penalization, axis=-1)

    general_disconnect_normalization = node_pooling >= 1e-3
    general_disconnect_normalization = general_disconnect_normalization.astype(np.float32)
    general_disconnect_normalization = np.sum(general_disconnect_normalization, axis=-1)
    general_disconnect_normalization = np.maximum(general_disconnect_normalization,
                                                  np.ones_like(general_disconnect_normalization))

    # K x 1
    general_disconnect_constraint = (all_disconnect_constraint - root_node_residual) / general_disconnect_normalization

    # Root node connectivity constraint
    root_node_disconnect_constraint = alpha * root_node_residual

    root_node_disconnect_normalization = node_pooling >= 1e-3
    root_node_disconnect_normalization = root_node_disconnect_normalization.astype(np.float32)
    root_node_disconnect_normalization = np.sum(root_node_disconnect_normalization * root_pooling, axis=-1)
    root_node_disconnect_normalization = np.maximum(root_node_disconnect_normalization,
                                                    np.ones_like(root_node_disconnect_normalization))

    # K x 1
    root_node_disconnect_constraint = root_node_disconnect_constraint / root_node_disconnect_normalization

    return general_disconnect_constraint, root_node_disconnect_constraint


def disconnected_nodes_constraint_naive(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    general_disconnect = np.zeros((node_pooling.shape[1]))
    root_disconnect = np.zeros((node_pooling.shape[1]))

    for k in range(node_pooling.shape[1]):
        total_gen_sum = 0.
        total_gen_norm = 0.
        total_root_sum = 0.
        total_root_norm = 0.
        for i in range(node_pooling.shape[0]):
            disconnect_penalization = 0.
            disconnect_norm = 0.
            for j in range(node_pooling.shape[0]):
                disconnect_penalization += node_pooling[j, k] * adjacency_matrix[i, j]
                disconnect_norm += float(node_pooling[j, k] >= 1e-3) * adjacency_matrix[i, j]
            disconnect_norm = np.maximum(disconnect_norm, 1.)
            disconnect_penalization = disconnect_penalization / disconnect_norm

            total_gen_sum += node_pooling[i, k] * (1 - disconnect_penalization) \
                             - root_pooling[k, i] * node_pooling[i, k] * (1 - disconnect_penalization)
            total_gen_norm += float(node_pooling[i, k] >= 1e-3)

            total_root_sum += alpha * root_pooling[k, i] * node_pooling[i, k] * (1 - disconnect_penalization)
            total_root_norm += root_pooling[k, i] * float(node_pooling[i, k] >= 1e-3)

        total_gen_norm = np.maximum(total_gen_norm, 1.)
        general_disconnect[k] = total_gen_sum / total_gen_norm

        total_root_norm = np.maximum(total_root_norm, 1.)
        root_disconnect[k] = total_root_sum / total_root_norm

    return general_disconnect, root_disconnect


# ------

def unified_disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    # node_pooling -> N x K
    # adjacency_matrix -> N x N
    # root_pooling -> K x N

    # Node Pooling
    # K x N x N
    exp_node_pooling = node_pooling.transpose()
    exp_node_pooling = exp_node_pooling[:, :, np.newaxis]
    exp_node_pooling = np.repeat(exp_node_pooling, axis=-1, repeats=exp_node_pooling.shape[1])
    exp_node_pooling = np.transpose(exp_node_pooling, [0, 2, 1])

    # Adjacency Matrix
    # K x N x N
    adjacency_matrix = adjacency_matrix[np.newaxis, :, :]
    adjacency_matrix = np.repeat(adjacency_matrix, axis=0, repeats=exp_node_pooling.shape[0])

    # General connectivity constraint
    connectivity_penalization = np.sum(exp_node_pooling * adjacency_matrix, axis=-1)

    connectivity_normalization = exp_node_pooling >= 1e-3
    connectivity_normalization = connectivity_normalization.astype(np.float32)
    connectivity_normalization = np.sum(connectivity_normalization * adjacency_matrix, axis=-1)
    connectivity_normalization = np.maximum(connectivity_normalization, np.ones_like(connectivity_normalization))

    # K x N
    connectivity_penalization = 1. - connectivity_penalization / connectivity_normalization

    node_pooling = node_pooling.transpose()

    # K x 1
    unified_disconnect_coefficient = 1 + (alpha - 1) * root_pooling
    unified_disconnect_constraint = np.sum(unified_disconnect_coefficient * node_pooling * connectivity_penalization,
                                           axis=-1)

    general_disconnect_normalization = node_pooling >= 1e-3
    general_disconnect_normalization = general_disconnect_normalization.astype(np.float32)
    general_disconnect_normalization = np.sum(general_disconnect_normalization, axis=-1)
    general_disconnect_normalization = np.maximum(general_disconnect_normalization,
                                                  np.ones_like(general_disconnect_normalization))

    unified_disconnect_constraint = unified_disconnect_constraint / general_disconnect_normalization

    return unified_disconnect_constraint


def unified_disconnected_nodes_constraint_naive(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    unified_disconnect = np.zeros((node_pooling.shape[1]))

    for k in range(node_pooling.shape[1]):
        total_sum = 0.
        total_norm = 0.
        for i in range(node_pooling.shape[0]):
            disconnect_penalization = 0.
            disconnect_norm = 0.
            for j in range(node_pooling.shape[0]):
                disconnect_penalization += node_pooling[j, k] * adjacency_matrix[i, j]
                disconnect_norm += float(node_pooling[j, k] >= 1e-3) * adjacency_matrix[i, j]
            disconnect_norm = np.maximum(disconnect_norm, 1.)
            disconnect_penalization = disconnect_penalization / disconnect_norm

            total_sum += (1 + (alpha - 1) * root_pooling[k, i]) * node_pooling[i, k] * (1 - disconnect_penalization)
            total_norm += float(node_pooling[i, k] >= 1e-3)

        total_norm = np.maximum(total_norm, 1.)
        unified_disconnect[k] = total_sum / total_norm

    return unified_disconnect


# ------

def parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    # Combined adjacency and span mask

    # N x N x N
    boolean_matrix = adjacency_matrix[np.newaxis, :, :] * node_spans[:, np.newaxis, :]

    # Remove self-references and symmetries
    eye_matrix = np.eye(adjacency_matrix.shape[1])
    eye_matrix = eye_matrix[np.newaxis, :, :]

    # N x N x N
    boolean_matrix = boolean_matrix - boolean_matrix * eye_matrix
    # boolean_matrix = 0.5 * boolean_matrix

    # All pair intensities

    # K x N
    node_pooling = node_pooling.transpose()

    # K x N x N
    pair_intensities = node_pooling[:, :, np.newaxis] * node_pooling[:, np.newaxis, :]

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # Connectivity penalty

    # K x N x N x N
    all_intensities = pair_intensities[:, np.newaxis, :, :] * boolean_matrix[np.newaxis, :, :, :]

    # K x N x N
    all_intensities = np.max(all_intensities, axis=-1)
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)

    # filter out leaves and check node span
    # 1 x 1 x N
    leaf_mask = np.minimum(np.sum(adjacency_matrix, axis=-1) - 1., 1.)
    leaf_mask[0] = 1.0
    leaf_mask = leaf_mask[np.newaxis, np.newaxis, :]

    # K x N x N
    all_intensities = all_intensities * leaf_mask * node_spans[np.newaxis, :, :]

    # Penalty weight

    # K x N x N
    asyn_adjacency_matrix = adjacency_matrix - np.tril(adjacency_matrix)
    j_weight = np.max(node_pooling[:, np.newaxis, np.newaxis, :] * asyn_adjacency_matrix[np.newaxis, np.newaxis, :, :],
                      axis=-1)
    i_weight = node_pooling[:, np.newaxis, :]
    t_weight = root_pooling[:, np.newaxis, :]
    # penalty_weight = np.maximum(j_weight, i_weight)
    # penalty_weight = np.maximum(penalty_weight, t_weight)
    penalty_weight = np.maximum(j_weight, t_weight)

    # Reduction

    # K x N
    constraint = np.sum(all_intensities * penalty_weight, axis=-1)
    # constraint = np.sum(all_intensities, axis=-1)

    # K
    constraint = np.sum(constraint * root_pooling, axis=-1)

    return constraint


def slim_parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    node_pooling = node_pooling.transpose()

    # K x N x N
    pair_intensities = node_pooling[:, :, np.newaxis] * node_pooling[:, np.newaxis, :]

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # K x N x N
    all_intensities = pair_intensities * adjacency_matrix[np.newaxis, :, :]

    # K x N
    all_intensities = np.max(all_intensities, axis=-1)
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)

    # filter out leaves and check node span
    # 1 x N
    leaf_mask = np.minimum(np.sum(node_spans, axis=-1) - 1., 1.)

    all_intensities *= leaf_mask[np.newaxis, :]

    # root span aware
    # K x N x N
    all_intensities = all_intensities[:, np.newaxis, :] * node_spans[np.newaxis, :, :]

    # K x N x N
    asyn_adjacency_matrix = adjacency_matrix - np.tril(adjacency_matrix)
    j_weight = np.max(node_pooling[:, np.newaxis, :] * asyn_adjacency_matrix[np.newaxis, :, :],
                      axis=-1)
    # i_weight = node_pooling[:, np.newaxis, :]
    t_weight = root_pooling[:, np.newaxis, :]
    # penalty_weight = np.maximum(j_weight[:, np.newaxis, :], i_weight)
    # penalty_weight = np.maximum(penalty_weight, t_weight)
    penalty_weight = np.maximum(j_weight[:, np.newaxis, :], t_weight)

    # Residuals

    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]

    span_weights = np.sum(span_area, axis=1)

    weight_sum = np.sum(span_weights, axis=0)
    weight_sum = weight_sum - span_weights
    weight_sum = np.minimum(weight_sum, 1.)
    residual_diff = np.maximum(span_weights - weight_sum, 0.)
    residual_inter = span_weights * weight_sum
    diff_mask = np.max(residual_diff, axis=-1)
    residuals = diff_mask[:, np.newaxis] * residual_diff + (1 - diff_mask)[:, np.newaxis] * residual_inter
    residuals = residuals[:, :, np.newaxis]

    # K x N
    constraint = np.sum(all_intensities * penalty_weight * residuals, axis=-1)

    # K
    constraint = np.sum(constraint * root_pooling, axis=-1)

    return constraint


def naive_parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    constraints = np.zeros((root_pooling.shape[0]), dtype=np.float32)
    for k in range(root_pooling.shape[0]):
        for l in range(root_pooling.shape[1]):
            total = 0
            for i in range(root_pooling.shape[1]):

                # penalty

                j_values = [
                    adjacency_matrix[i, j] * node_pooling[i, k] * node_pooling[j, k] * node_spans[
                        l, j]
                    for j in range(i + 1, root_pooling.shape[1])]
                if len(j_values) >= 1:
                    j_max_value = np.max(j_values)
                    penalty = (1 / alpha) * np.maximum(alpha - j_max_value, 0.)
                else:
                    penalty = 0.0

                # weights

                j_weights = [adjacency_matrix[i, j] * node_pooling[j, k] * node_spans[l, j]
                             for j in range(i + 1, root_pooling.shape[1])]
                if len(j_weights) >= 1:
                    j_weight = np.max(j_weights)
                else:
                    j_weight = 0.0
                penalty_weight = np.max([j_weight, node_pooling[i, k], root_pooling[k, i]])

                total += penalty * penalty_weight * node_spans[l, i]

            constraints[k] += root_pooling[k, l] * total

    return constraints


# ------

def parametric_ptk_ratio_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    node_amount = node_pooling.shape[0]

    adjacency_matrix = adjacency_matrix.astype(np.float32)

    # All pair intensities

    # [K, N]
    node_pooling = node_pooling.transpose()

    # [K, N]
    node_pooling = np.minimum(node_pooling, alpha) / alpha

    # [K, N, N]
    pair_intensities = np.maximum(node_pooling[:, :, np.newaxis] + node_pooling[:, np.newaxis, :] - 1., 0.)

    # Get node-parent pairs only

    # [N, N]
    # lookback_mask = np.tril(np.ones(adjacency_matrix.shape)) - np.eye(node_amount)
    pair_intensities = 0.5 * (
            pair_intensities - np.eye(node_amount, dtype=node_pooling.dtype)[np.newaxis, :, :] * pair_intensities)
    pair_intensities *= adjacency_matrix[np.newaxis, :, :]

    # Filter out by root node span and root node

    # node_spans -= np.eye(node_amount)

    # [K, N, N]
    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]

    # [K, N]
    span_weights = np.sum(span_area, axis=1)

    pair_intensities *= span_weights[:, :, np.newaxis]
    pair_intensities *= span_weights[:, np.newaxis, :]

    # Compute numerator

    # [K]
    numerator = np.sum(pair_intensities, axis=-1)
    numerator = np.sum(numerator, axis=-1)

    # Compute denominator

    # [K]
    lookahead_mask = np.triu(np.ones((node_amount, node_amount), dtype=node_pooling.dtype)) - np.eye(node_amount)
    root_residual = (1. - node_pooling) * root_pooling \
                    + (1. - np.max(node_pooling[:, np.newaxis, :]
                                   * adjacency_matrix[np.newaxis, :, :]
                                   * span_weights[:, np.newaxis, :]
                                   * lookahead_mask[np.newaxis, :, :], axis=-1)) \
                    * root_pooling
    denominator = node_pooling * span_weights + root_residual
    denominator = np.sum(denominator, axis=-1)
    denominator = np.maximum(denominator - 1., 1.)

    # Compute constraint

    print(numerator)
    print(denominator)

    constraint = - numerator / denominator

    return constraint


def parametric_stk_ratio_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    node_amount = node_pooling.shape[0]

    adjacency_matrix = adjacency_matrix.astype(np.float32)

    # All pair intensities

    # [K, N]
    node_pooling = node_pooling.transpose()

    # [K, N]
    node_pooling = np.minimum(node_pooling, alpha) / alpha

    # [K, N, N]
    pair_intensities = np.maximum(node_pooling[:, :, np.newaxis] + node_pooling[:, np.newaxis, :] - 1., 0.)

    # Get node-parent pairs only

    # [N, N]
    # lookback_mask = np.tril(np.ones(adjacency_matrix.shape)) - np.eye(node_amount)
    # pair_intensities *= lookback_mask[np.newaxis, :, :]
    pair_intensities = 0.5 * (pair_intensities - np.eye(node_amount, dtype=node_pooling.dtype)[np.newaxis, :, :])
    pair_intensities *= adjacency_matrix[np.newaxis, :, :]

    # Filter out by root node span and root node

    # [K, N, N]
    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]

    # [K, N]
    span_weights = np.sum(span_area, axis=1)

    pair_intensities *= span_weights[:, :, np.newaxis]
    pair_intensities *= span_weights[:, np.newaxis, :]

    # Compute numerator

    # [K]
    numerator = np.sum(pair_intensities, axis=-1)
    numerator = np.sum(numerator, axis=-1)

    # Compute denominator

    no_root_node_spans = node_spans - np.eye(node_amount, dtype=node_pooling.dtype)
    no_root_span_area = root_pooling[:, :, np.newaxis] * no_root_node_spans[np.newaxis, :, :]
    no_root_span_weights = np.sum(no_root_span_area, axis=1)

    # [K]
    lookahead_mask = np.triu(np.ones((node_amount, node_amount), dtype=node_pooling.dtype)) - np.eye(node_amount)
    root_residual = (1. - node_pooling) * root_pooling \
                    + np.sum((1. - node_pooling[:, np.newaxis, :])
                             * adjacency_matrix[np.newaxis, :, :]
                             * lookahead_mask[np.newaxis, :, :]
                             * no_root_span_weights[:, np.newaxis, :], axis=-1)
    denominator = node_pooling * span_weights + root_residual
    denominator = np.sum(denominator, axis=-1)
    denominator = np.maximum(denominator - 1., 1.)

    # Compute constraint

    print(numerator)
    print(denominator)

    constraint = - numerator / denominator

    return constraint


def parametric_sstk_ratio_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    node_amount = node_pooling.shape[0]

    adjacency_matrix = adjacency_matrix.astype(np.float32)

    # All pair intensities

    # [K, N]
    node_pooling = node_pooling.transpose()

    # [K, N]
    node_pooling = np.minimum(node_pooling, alpha) / alpha

    # [K, N, N]
    pair_intensities = np.maximum(node_pooling[:, :, np.newaxis] + node_pooling[:, np.newaxis, :] - 1., 0.)

    # Get node-parent pairs only

    # [N, N]
    # lookback_mask = np.tril(np.ones(adjacency_matrix.shape)) - np.eye(node_amount)
    # pair_intensities *= lookback_mask[np.newaxis, :, :]
    pair_intensities = 0.5 * (pair_intensities - np.eye(node_amount, dtype=node_pooling.dtype)[np.newaxis, :, :])
    pair_intensities *= adjacency_matrix[np.newaxis, :, :]

    # Filter out by root node span and root node

    # [K, N, N]
    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]

    # [K, N]
    span_weights = np.sum(span_area, axis=1)

    pair_intensities *= span_weights[:, :, np.newaxis]
    pair_intensities *= span_weights[:, np.newaxis, :]

    # Compute numerator

    # [K]
    numerator = np.sum(pair_intensities, axis=-1)
    numerator = np.sum(numerator, axis=-1)

    # Compute denominator

    no_root_node_spans = node_spans - np.eye(node_amount, dtype=node_pooling.dtype)
    no_root_span_area = root_pooling[:, :, np.newaxis] * no_root_node_spans[np.newaxis, :, :]
    no_root_span_weights = np.sum(no_root_span_area, axis=1)

    # [K]
    active_nodes = np.sum(node_pooling * span_weights, axis=-1)

    leaf_mask = np.minimum(np.sum(adjacency_matrix, axis=-1) - 1., 1.)
    leaf_mask[0] = 1.0
    lookahead_mask = np.triu(np.ones((node_amount, node_amount), dtype=node_pooling.dtype)) - np.eye(node_amount)

    # [K]
    inactive_meta_children = np.sum((1. - node_pooling[:, np.newaxis, :])
                                    * no_root_span_weights[:, np.newaxis, :]
                                    * lookahead_mask[np.newaxis, :, :]
                                    * adjacency_matrix[np.newaxis, :, :]
                                    * leaf_mask[np.newaxis, np.newaxis, :], axis=-1)
    inactive_meta_children = np.sum(inactive_meta_children, axis=-1)

    root_residual = (1. - node_pooling) * root_pooling
    root_residual = np.sum(root_residual, axis=-1)

    # [K]
    denominator = active_nodes + inactive_meta_children + root_residual
    denominator = np.maximum(denominator - 1., 1.)

    # Compute constraint

    print(numerator)
    print(denominator)

    constraint = - numerator / denominator

    return constraint


# ------

def minimal_tree_constraint(node_spans, root_pooling, alpha=0.5, size_threshold=0.5):
    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    pooled_percentage = np.sum(span_weights, axis=-1) / node_spans.shape[-1]

    penalty = np.maximum(pooled_percentage - size_threshold, 0.)

    return penalty


# ------


def ptk_separation_constraint(node_pooling, root_pooling, node_spans, alpha=0.5):
    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    # [K, N]
    node_pooling = node_pooling.transpose()
    node_pooling = np.minimum(node_pooling, alpha) / alpha

    # [K, K, N]
    a = node_pooling[:, np.newaxis, :] * (1 - node_pooling[np.newaxis, :, :])
    b = (1 - node_pooling[:, np.newaxis, :]) * (node_pooling[np.newaxis, :, :])

    hamming = a + b

    hamming *= span_weights[:, np.newaxis, :]

    # [K, K]
    constraint = np.max(hamming, axis=-1)
    constraint = np.maximum(1. - constraint, 0.)
    constraint = np.minimum(constraint, constraint.transpose())
    constraint = constraint - np.tril(constraint)

    return constraint


def stk_separation_constraint(node_pooling, root_pooling, node_spans, alpha=0.5):
    # [K, K, N]
    a = root_pooling[:, np.newaxis, :] * (1 - root_pooling[np.newaxis, :, :])
    b = (1 - root_pooling[:, np.newaxis, :]) * (root_pooling[np.newaxis, :, :])

    hamming = a + b

    # [K, K]
    constraint = np.max(hamming, axis=-1)
    constraint = np.maximum(1. - constraint, 0.)
    constraint = np.minimum(constraint, constraint.transpose())
    constraint = constraint - np.tril(constraint)

    return constraint


def sstk_separation_constraint(node_pooling, root_pooling, node_spans, alpha=0.5):
    # [K, K]
    ptk_constraint = ptk_separation_constraint(node_pooling, root_pooling, node_spans, alpha)
    stk_constraint = stk_separation_constraint(node_pooling, root_pooling, node_spans, alpha)

    sstk_constraint = np.minimum(ptk_constraint, stk_constraint)

    return sstk_constraint


# ------

def auto_root_pooling(node_pooling, adjacency_matrix, node_spans, alpha=0.5):
    node_amount = node_pooling.shape[0]

    adjacency_matrix = adjacency_matrix.astype(node_pooling.dtype)

    node_pooling = np.minimum(node_pooling, alpha) / alpha

    lookback_mask = np.tril(adjacency_matrix) - np.eye(node_amount, dtype=node_pooling.dtype)
    lookback_mask = lookback_mask.transpose()

    raw_tree_pooling = np.matmul(node_pooling.transpose(), adjacency_matrix * lookback_mask)
    raw_tree_pooling = 1.0 - raw_tree_pooling

    raw_tree_pooling *= node_pooling.transpose()
    raw_tree_pooling = np.log(raw_tree_pooling + 1e-12)

    leaf_mask = np.minimum(np.sum(node_spans, axis=-1) - 1., 1.)

    raw_tree_pooling += -1e9 * (1 - leaf_mask[np.newaxis, :])

    return raw_tree_pooling, softmax(raw_tree_pooling, axis=-1)


# ------

# TODO: dunno if it requires connectivity re-scaling
def overlapping_constraint(node_pooling, root_pooling, node_spans, alpha=0.5):
    """
    Dice Loss: (2 * intersection) / union
    Numerator = 2 * \sum p_(i, k) * p_(i, k')
    Denominator = \sum p_(i,k)^2 + \sum p_{i, k')^2

    We only consider overlapping within the root node span
    """

    # [K, N]
    span_area = root_pooling[:, :, np.newaxis] * node_spans[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    node_pooling = node_pooling.transpose()
    node_pooling = np.minimum(node_pooling, alpha) / alpha
    node_pooling *= span_weights

    # [K, K]
    numerator = 2 * np.sum(node_pooling[:, np.newaxis, :] * node_pooling[np.newaxis, :, :], axis=-1)
    denominator = np.sum(node_pooling, axis=-1)[:, np.newaxis] + np.sum(node_pooling, axis=-1)[np.newaxis, :]

    constraint = 1 - numerator / (denominator + 1e-12)
    constraint = constraint - np.tril(constraint)

    return constraint


# ------

def cluster_separation_constraint(node_pooling):
    # K x K
    cluster_overlap = np.matmul(node_pooling.transpose(), node_pooling)
    cluster_overlap = 0.5 * (cluster_overlap - np.eye(cluster_overlap.shape[-1]) * cluster_overlap)

    # K x K
    normalization = np.sum(node_pooling, axis=0)
    normalization = np.minimum(normalization[:, np.newaxis], normalization[np.newaxis, :])

    # []
    constraint = np.sum(cluster_overlap / normalization)
    pairs_amount = node_pooling.shape[-1] * (node_pooling.shape[-1] - 1) / 2
    constraint = constraint / pairs_amount

    return constraint


def naive_cluster_separation_constraint(node_pooling):
    constraint = 0.
    for k in range(node_pooling.shape[-1]):
        for p in range(k + 1, node_pooling.shape[-1]):
            overlap = np.sum([node_pooling[i, k] * node_pooling[i, p] for i in range(node_pooling.shape[0])])
            normalization = np.minimum(np.sum(node_pooling[:, k]), np.sum(node_pooling[:, p]))
            constraint += overlap / normalization

    pairs_amount = node_pooling.shape[-1] * (node_pooling.shape[-1] - 1) / 2
    return constraint / pairs_amount


# -------

def lossy_constraints(pooling_matrix, node_mask, lossy_percentage=0.1, cluster_distr_threshold=0.3):
    # pooling_matrix -> [max_nodes, K]
    # node_mask -> [max_nodes]

    # [batch_size,]
    node_amount = np.sum(node_mask, axis=-1)

    # Lossy distribution

    # []
    pooling_mass = np.sum(pooling_matrix, axis=-1)
    pooling_mass = np.sum(pooling_mass, axis=-1)

    # total mass <= (1 - lossy_percentage) * node_amount
    lossy_constraint = np.maximum(pooling_mass - node_amount + node_amount * lossy_percentage, 0.)
    lossy_constraint = np.mean(lossy_constraint)

    # Proper distribution

    # [K]
    cluster_masses = np.sum(pooling_matrix, axis=0)
    cluster_distr = softmax(cluster_masses, axis=-1)

    # []
    cluster_entropy = - np.sum(np.log(cluster_distr + 1e-5) * cluster_distr)

    mass_distribution = np.maximum(cluster_distr_threshold - cluster_entropy, 0.)

    return lossy_constraint, mass_distribution


def compute_hamming_cluster_separation_constraint(pooling_matrix, disagreement_margin=0.5):
    """
    Average Hamming loss
    """

    # pooling_matrix -> [max_nodes, K]

    meta_nodes_amounts = pooling_matrix.shape[-1]
    nodes_amount = pooling_matrix.shape[0]

    # [K, max_nodes]
    pooling_matrix = pooling_matrix.transpose()

    # [K, K]
    a = np.matmul(pooling_matrix, np.transpose(1 - pooling_matrix))
    b = np.matmul(1 - pooling_matrix, np.transpose(pooling_matrix))

    # [K, K]
    a = a - np.tril(a)

    # [K, K]
    b = b - np.tril(b)

    # [K, K]
    hamming = (a + b) / nodes_amount

    # [meta_nodes, meta_nodes]
    hamming = np.maximum(disagreement_margin - hamming, 0.)

    hamming = hamming - np.tril(hamming)

    # []
    hamming = np.sum(hamming)
    hamming_norm = meta_nodes_amounts * (meta_nodes_amounts - 1)
    hamming = hamming / hamming_norm

    return hamming


# Final constraints


def final_contiguous_constraint(pooling_matrix, adjacency_matrix):
    node_amount = pooling_matrix.shape[0]

    pooling_matrix = pooling_matrix.transpose()

    adjacency_matrix = np.triu(adjacency_matrix)

    # [K, N, N]
    pair_intensities = pooling_matrix[:, :, np.newaxis] * pooling_matrix[:, np.newaxis, :]

    # [K, N, N]
    self_intensities = pair_intensities * np.eye(node_amount, dtype=pooling_matrix.dtype)[np.newaxis, :, :]

    # [K, N, N]
    pair_intensities = pair_intensities - self_intensities

    # [K, N, N]
    pair_intensities *= adjacency_matrix[np.newaxis, :, :]

    # [K]
    pooled_mask = self_intensities >= 1e-2
    pooled_mask = np.sum(pooled_mask, axis=-1)
    pooled_mask = np.sum(pooled_mask, axis=-1) + 1e-12

    # [K]
    mean_self_intensity = np.sum(self_intensities, axis=-1)
    mean_self_intensity = np.sum(mean_self_intensity, axis=-1)
    mean_self_intensity = mean_self_intensity / pooled_mask

    numerator = np.sum(pair_intensities, axis=-1)
    numerator = np.sum(numerator, axis=-1)
    numerator = np.sum(numerator, axis=-1)

    denominator = np.sum(self_intensities, axis=-1)
    denominator = np.sum(denominator, axis=-1) - mean_self_intensity
    denominator = np.sum(denominator, axis=-1) + 1e-12

    penalty = np.maximum(1. - numerator / denominator, 0)
    # penalty = np.sum(penalty)

    return penalty


def final_stk_constraint(pooling_matrix, adjacency_matrix):
    node_amount = pooling_matrix.shape[0]

    adjacency_matrix = adjacency_matrix.astype(pooling_matrix.dtype)

    # [N, N]
    forward_adjacency_matrix = np.triu(adjacency_matrix) - np.eye(node_amount, dtype=adjacency_matrix.dtype)
    backward_adjacency_matrix = np.tril(adjacency_matrix) - np.eye(node_amount, dtype=adjacency_matrix.dtype)

    # [N, N]
    forward_degree_matrix = np.sum(forward_adjacency_matrix, axis=-1)
    forward_degree_matrix = forward_degree_matrix[:, np.newaxis] * np.eye(node_amount, dtype=adjacency_matrix.dtype)

    # [N, N]
    backward_degree_matrix = np.sum(backward_adjacency_matrix, axis=-1)
    backward_degree_matrix = backward_degree_matrix[:, np.newaxis] * np.eye(node_amount, dtype=adjacency_matrix.dtype)

    # [N]
    leaf_mask = np.minimum(np.sum(forward_adjacency_matrix, axis=-1), 1.)

    numerator_adjacency_matrix = forward_adjacency_matrix + backward_adjacency_matrix * (1. - leaf_mask[:, np.newaxis])

    # [K, K]
    numerator = np.matmul(np.matmul(pooling_matrix.transpose(), numerator_adjacency_matrix), pooling_matrix)

    denominator_degree_matrix = forward_degree_matrix + backward_degree_matrix * (1. - leaf_mask[:, np.newaxis])

    # [K, K]
    denominator = np.matmul(np.matmul(pooling_matrix.transpose(), denominator_degree_matrix), pooling_matrix)

    # []
    numerator = np.trace(numerator)
    denominator = np.trace(denominator) + 1e-12

    penalty = np.maximum(1. - numerator / denominator, 0.)

    return penalty


def final_sstk_constraint(pooling_matrix, adjacency_matrix):
    node_amount = pooling_matrix.shape[0]

    adjacency_matrix = adjacency_matrix.astype(pooling_matrix.dtype)

    # [N, N]
    forward_adjacency_matrix = np.triu(adjacency_matrix) - np.eye(node_amount, dtype=adjacency_matrix.dtype)

    # [N]
    leaf_mask = np.minimum(np.sum(forward_adjacency_matrix, axis=-1), 1.)

    # [N, N]
    forward_degree_matrix = np.sum(forward_adjacency_matrix * leaf_mask[np.newaxis, :], axis=-1)
    forward_degree_matrix = forward_degree_matrix[:, np.newaxis] * np.eye(node_amount, dtype=adjacency_matrix.dtype)

    mod_pooling_matrix = pooling_matrix * leaf_mask[:, np.newaxis]

    # [K, K]
    numerator = np.matmul(np.matmul(mod_pooling_matrix.transpose(), forward_adjacency_matrix), mod_pooling_matrix)

    # [K, K]
    denominator = np.matmul(np.matmul(mod_pooling_matrix.transpose(), forward_degree_matrix), mod_pooling_matrix)

    # []
    numerator = np.trace(numerator)
    denominator = np.trace(denominator) + 1e-12

    penalty = np.maximum(1. - numerator / denominator, 0.)

    return penalty


def tf_final_sstk_constraint(pooling_matrix, adjacency_matrix, node_mask=None):
    adjacency_matrix = tf.cast(adjacency_matrix, dtype=pooling_matrix.dtype)

    if node_mask is None:
        node_mask = tf.ones_like(adjacency_matrix)
    else:
        node_mask = tf.expand_dims(node_mask, axis=-1)

    # avoid counting padding nodes
    adjacency_matrix *= node_mask
    adjacency_matrix *= tf.transpose(node_mask, [0, 2, 1])

    node_amount = adjacency_matrix.shape[-1]

    # Adjacency and degree matrices

    # [batch_size, N, N]
    forward_adjacency_matrix = tf.linalg.band_part(tf.ones((node_amount, node_amount), dtype=pooling_matrix.dtype),
                                                   0, -1)
    forward_adjacency_matrix -= tf.eye(node_amount, dtype=pooling_matrix.dtype)
    forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

    # Leaf mask
    # Mask out leaves: a leaf node no children -> the minimum returns 0.
    # is leaf? -> 0.0
    # [batch_size, max_nodes]
    leaf_mask = tf.minimum(tf.reduce_sum(forward_adjacency_matrix, axis=-1), 1.)

    # Ignore leaves from degree count
    forward_degree_matrix = tf.reduce_sum(forward_adjacency_matrix * leaf_mask[:, None, :], axis=-1)
    forward_degree_matrix = forward_degree_matrix[:, :, None] * tf.eye(adjacency_matrix.shape[-1],
                                                                       dtype=adjacency_matrix.dtype)[None, :, :]

    # Ignore pooled leaves from constraint
    pooling_matrix *= leaf_mask[:, :, None]

    # [batch_size, K, K]
    numerator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), forward_adjacency_matrix),
                          pooling_matrix)

    # Apply masked degree matrix

    denominator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), forward_degree_matrix),
                            pooling_matrix)

    # [batch_size]
    numerator = tf.linalg.trace(numerator)
    denominator = tf.linalg.trace(denominator) + 1e-12

    # [batch_size]
    stk_penalty = tf.nn.relu(1. - numerator / denominator)
    # stk_penalty = tf.linalg.trace(stk_penalty)

    return tf.reduce_mean(stk_penalty)


# ------

def collapse_reg(pooling_matrix):
    # [N, K]

    node_amount = pooling_matrix.shape[0]
    meta_node_amount = pooling_matrix.shape[1]

    # [K]
    cluster_size = np.sum(pooling_matrix, axis=0)

    penalty = np.linalg.norm(cluster_size) * (np.sqrt(float(meta_node_amount)) / node_amount)
    penalty = penalty - 1

    return penalty


def tf_collapse_reg(pooling_matrix):

    node_amount = pooling_matrix.shape[0]
    meta_node_amount = pooling_matrix.shape[1]

    # [K]
    cluster_sizes = tf.reduce_sum(pooling_matrix, axis=0)

    penalty = tf.norm(cluster_sizes) * tf.cast((tf.math.sqrt((float(meta_node_amount))) / node_amount), dtype=cluster_sizes.dtype)
    penalty = penalty - 1

    return penalty

# Step 4: run some experiments
K = 1
iterations = 1

print('[Step 2] Running experiments...')


def compare_auto_tree_pooling(node_pooling, adjacency_matrix):
    efficient = auto_tree_pooling_matrix(node_pooling, adjacency_matrix)
    naive = auto_tree_pooling_naive(node_pooling, adjacency_matrix)

    assert np.allclose(efficient, naive)


def compare_tree_constraint(node_pooling, root_pooling, node_spans):
    efficient = mod_tree_constraint_alt(node_pooling, root_pooling, node_spans)
    naive = mod_tree_constraint_naive(node_pooling, root_pooling, node_spans)

    assert np.equal(efficient, naive).all()


def compare_contiguous_sequence_constraint(node_pooling, adjacency_matrix):
    efficient = contiguous_sequence_constraint_matrix(node_pooling, adjacency_matrix)
    naive = contiguous_sequence_constraint_naive(node_pooling, adjacency_matrix)

    assert np.allclose(efficient, naive)


def compare_complete_contiguous_sequence_constraint(node_pooling, adjacency_matrix, node_spans, root_pooling):
    efficient = contiguous_sequence_constraint_complete(node_pooling, adjacency_matrix, node_spans, root_pooling)
    naive = contiguous_sequence_constraint_complete_naive(node_pooling, adjacency_matrix, node_spans, root_pooling)

    assert np.allclose(efficient, naive)


def compare_disconnect_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    efficient = disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha)
    naive = disconnected_nodes_constraint_naive(node_pooling, adjacency_matrix, root_pooling, alpha)

    assert np.allclose(efficient[0], naive[0])
    assert np.allclose(efficient[1], naive[1])


def compare_unified_disconnect_constraint(node_pooling, adjacency_matrix, root_pooling, alpha=2.):
    efficient = unified_disconnected_nodes_constraint(node_pooling, adjacency_matrix, root_pooling, alpha)
    naive = unified_disconnected_nodes_constraint_naive(node_pooling, adjacency_matrix, root_pooling, alpha)

    assert np.allclose(efficient, naive)


def compare_parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha=0.5):
    efficient = parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha)
    slim = slim_parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha)
    naive = naive_parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha)

    try:
        assert np.allclose(efficient, slim)
    except AssertionError as e:
        print("Efficient -> {}".format(efficient))
        print("Slim -> {}".format(slim))
        print("Naive -> {}".format(naive))
        for k in range(node_pooling.shape[-1]):
            draw_tree(nodes=node_pooling[:, k], adjacency_matrix=adjacency_matrix,
                      title_name='Node pooling (k = {})'.format(k))
            draw_tree(nodes=root_pooling[k, :], adjacency_matrix=adjacency_matrix,
                      title_name='Root pooling (K = {})'.format(k))
        plt.show()


def compare_cluster_separation_constraint(node_pooling):
    efficient = cluster_separation_constraint(node_pooling)
    naive = naive_cluster_separation_constraint(node_pooling)

    assert np.allclose(efficient, naive)


for it in tqdm(range(iterations)):
    # node_pooling = mock_pooling(nodes, K, use_sigmoid=use_sigmoid)
    # root_pooling = mock_root_pooling(nodes, K)
    # draw_tree(nodes=node_pooling[:, 0], adjacency_matrix=adjacency_matrix, title_name="Node pooling")
    # draw_tree(nodes=root_pooling[0, :], adjacency_matrix=adjacency_matrix, title_name="Root pooling")
    # plt.show()
    # print()

    node_pooling = np.array([0,
                             1,
                             0.,
                             1.,
                             1.,
                             1.,
                             0.,]).reshape(7, 1)
    # node_pooling = np.array([0.5, 0.5, 0.5, 0.5,
    #                          0.5, 0.5, 0.5, 0.5]).reshape(2, 4).transpose()
    adjacency_matrix = np.array([1, 1, 0, 0, 0, 0, 0,
                                 1, 1, 1., 1, 0, 0, 0,
                                 0, 1, 1, 0, 0, 0, 0,
                                 0, 1, 0, 1, 1, 0, 0,
                                 0, 0, 0, 1, 1, 1, 0,
                                 0, 0, 0, 0, 1, 1, 1,
                                 0, 0, 0, 0, 0, 1, 1]).reshape(7, 7)
    # node_pooling = np.load('example_pooling.npy')
    # adjacency_matrix = np.load('example_adjacency.npy')

    # 0) Tree pooling
    # compare_auto_tree_pooling(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)

    # 1) Tree constraint
    # compare_tree_constraint(node_pooling=node_pooling, root_pooling=root_pooling, node_spans=node_spans)

    # 2) Contiguous sequence constraint
    # compare_contiguous_sequence_constraint(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix)

    # 3) Complete contiguous sequence constraint
    # compare_complete_contiguous_sequence_constraint(node_pooling=node_pooling, root_pooling=root_pooling,
    #                                                 adjacency_matrix=adjacency_matrix, node_spans=node_spans)

    # 4) Disconnect constraint
    # compare_disconnect_constraint(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix,
    #                               root_pooling=root_pooling, alpha=alpha)

    # 5) Unified disconnect constraint
    # compare_unified_disconnect_constraint(node_pooling=node_pooling, adjacency_matrix=adjacency_matrix,
    #                                       root_pooling=root_pooling, alpha=alpha)

    # 6) Parametric contiguous constraint
    # compare_parametric_contiguous_constraint(node_pooling, adjacency_matrix, root_pooling, node_spans, alpha)

    # 7) Cluster separation constraint
    # compare_cluster_separation_constraint(node_pooling)

    # print('PTK: ', parametric_ptk_ratio_contiguous_constraint(node_pooling=node_pooling,
    #                                                           adjacency_matrix=adjacency_matrix,
    #                                                           root_pooling=root_pooling,
    #                                                           node_spans=node_spans,
    #                                                           alpha=0.3))
    #
    # print('STK: ', parametric_stk_ratio_contiguous_constraint(node_pooling=node_pooling,
    #                                                           adjacency_matrix=adjacency_matrix,
    #                                                           root_pooling=root_pooling,
    #                                                           node_spans=node_spans,
    #                                                           alpha=0.3))
    #
    # print('SSTK: ', parametric_sstk_ratio_contiguous_constraint(node_pooling=node_pooling,
    #                                                             adjacency_matrix=adjacency_matrix,
    #                                                             root_pooling=root_pooling,
    #                                                             node_spans=node_spans,
    #                                                             alpha=0.3))
    #
    # print('PTK separation: \n:', ptk_separation_constraint(node_pooling=node_pooling,
    #                                                        root_pooling=root_pooling,
    #                                                        node_spans=node_spans,
    #                                                        alpha=alpha))
    #
    # print('STK separation: \n:', stk_separation_constraint(node_pooling=node_pooling,
    #                                                        root_pooling=root_pooling,
    #                                                        node_spans=node_spans,
    #                                                        alpha=alpha))
    #
    # print('SSTK separation: \n:', sstk_separation_constraint(node_pooling=node_pooling,
    #                                                          root_pooling=root_pooling,
    #                                                          node_spans=node_spans,
    #                                                          alpha=alpha))

    # print('Auto root pooling: \n:', auto_root_pooling(node_pooling=node_pooling,
    #                                                   adjacency_matrix=adjacency_matrix,
    #                                                   node_spans=node_spans,
    #                                                   alpha=alpha))

    # print('Overlap constraint: \n', overlapping_constraint(node_pooling=node_pooling,
    #                                                        root_pooling=root_pooling,
    #                                                        node_spans=node_spans,
    #                                                        alpha=alpha))

    print("Contiguous constraint: \n", final_contiguous_constraint(pooling_matrix=node_pooling,
                                                                   adjacency_matrix=adjacency_matrix))
    #
    print("STK Contiguous constraint: \n", final_stk_constraint(pooling_matrix=node_pooling,
                                                                adjacency_matrix=adjacency_matrix))
    #
    print("SSTK Contiguous constraint: \n", final_sstk_constraint(pooling_matrix=node_pooling,
                                                                  adjacency_matrix=adjacency_matrix))
    #
    # print("SSTK Contiguous constraint: \n", tf_final_sstk_constraint(pooling_matrix=node_pooling[np.newaxis, :, :],
    #                                                                  adjacency_matrix=adjacency_matrix[np.newaxis, :,
    #                                                                                   :]))

    print("Collapse regularization: \n", tf_collapse_reg(node_pooling))

    for k in range(node_pooling.shape[-1]):
        draw_tree(nodes=node_pooling[:, k], adjacency_matrix=adjacency_matrix,
                  title_name="Node pooling {}".format(k + 1))
        # draw_tree(nodes=root_pooling[k, :], adjacency_matrix=adjacency_matrix,
        #           title_name="Root pooling {}".format(k + 1))
    plt.show()
