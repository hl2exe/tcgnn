"""

For each argumentative predicted sentence it computes the corresponding BERT sentence embedding

"""

import tensorflow_hub as hub
from tqdm import tqdm
from tokenization import BertTokenizer
import numpy as np
import const_define as cd
import const_define as exp_cd
import os
import pandas as pd


model_type = "ibm2015_experimental_single_gnn_v2"
test_name = "15-04-2020-23-23-58"
test_folder = cd.LOO_DIR
save_prefix = 1.0
repetition_prefix = None
bert_model = "https://tfhub.dev/google/bert_uncased_L-12_H-768_A-12/1"
vocab_file = os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', 'vocab.txt')

network_name = exp_cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
if repetition_prefix is not None:
    network_name += '_repetition_{}'.format(repetition_prefix)
if save_prefix is not None:
    network_name += '_key_{}'.format(save_prefix)

saved_data_path = os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name)

arg_df = pd.read_csv(os.path.join(saved_data_path, '{}_argumentative_dataset.csv'.format(network_name)))
text = arg_df.Sentence.values

tokenizer = BertTokenizer(vocab_file=vocab_file, do_lower_case=True, max_len=512)

text_bert = []
with hub.eval_function_for_module(bert_model) as bert:
    for sentence in tqdm(text):
        inputs = tokenizer.encode_plus(
            sentence,
            None,
            add_special_tokens=True,
            max_length=512,
        )
        input_ids, token_type_ids = inputs["input_ids"], inputs["token_type_ids"]
        attention_mask = [1] * len(input_ids)
        batch_in = {'input_ids': [input_ids],
                    'input_mask': [token_type_ids],
                    'segment_ids': [attention_mask]}
        batch_out = bert(batch_in, as_dict=True, signature='tokens')['pooled_output'][0]
        text_bert.append(batch_out)

conv_text = np.array(text_bert)

save_path = os.path.join(saved_data_path, '{}_argumentative_bert_representations.npy'.format(network_name))
np.save(save_path, conv_text)