"""

Builds a unique pd.DataFrame from UKP Sentential Argument Mining Corpus .tsv files

"""

import const_define as cd
import os
import pandas as pd

overall_df = None
for filename in os.listdir(cd.UKP_DIR):
    sub_path = os.path.join(cd.UKP_DIR, filename)
    df = pd.read_csv(sub_path, sep="\t")

    if overall_df is None:
        overall_df = df
    else:
        overall_df = pd.concat((overall_df, df), axis=0)

print(overall_df.shape)

overall_df.to_csv(os.path.join(cd.UKP_DIR, 'dataset.csv'), index=False)
