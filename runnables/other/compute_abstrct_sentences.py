import os
import const_define as cd
from nltk import sent_tokenize
from tqdm import tqdm

data_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'AbstRCT_corpus', 'data')

total_count = 0

for filename in tqdm(os.listdir(data_path)):
    for folder in ['dev', 'test', 'train']:
        sub_folder_path = os.path.join(data_path, folder)
        for subfolder in os.listdir(sub_folder_path):
            subsub_folder_path = os.path.join(sub_folder_path, subfolder)
            for filename in os.listdir(subsub_folder_path):
                if filename.endswith('.txt'):
                    sub_path = os.path.join(subsub_folder_path, filename)
                    with open(sub_path, 'r') as f:
                        data = f.readlines()

                    data = os.linesep.join(data)
                    data_sentences = sent_tokenize(data)
                    total_count += len(data_sentences)

print('Total sentences -> ', total_count)