

"""

Builds CORD-19 dataset bow feature matrix

"""

from sklearn.feature_extraction.text import TfidfVectorizer
import const_define as cd
import pandas as pd
import os
from scipy.sparse import save_npz


df_path = os.path.join(cd.CORD19_DIR, 'all_dataset_filtered.csv')
df = pd.read_csv(df_path)

text = df.Sentence.values

vect = TfidfVectorizer()

bow_feature_matrix = vect.fit_transform(text)

print(bow_feature_matrix.shape)

save_path = os.path.join(cd.CORD19_DIR, 'cord19_feature_matrix.npz')
save_npz(save_path, bow_feature_matrix)