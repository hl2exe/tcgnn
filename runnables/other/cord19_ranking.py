"""

Given a task bullet points, a pre-trained model with its predictions on cord19 dataset:

    1. Encode bullet points via pre-trained model settings

    2. Obtain word embedding of bullet points via pre-trained model forward step

    3. Obtain word embedding of cord19 dataset produced by pre-trained model

    4. Filter out dataset entries that are not predicted as argumentative sentences by pre-trained model

    5. Compute similarity (dot product?, L2?) and print top K ranked dataset sentences for each given bullet point

"""

import os

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from MulticoreTSNE import MulticoreTSNE as TSNE

import const_define as cd
from custom_callbacks_v2 import RepresentationRetriever
import const_define as exp_cd
from data_converter import DataConverterFactory
from data_loader import CORD19Loader
from data_processor import ProcessorFactory
from utility.data_utils import get_data_config_id
from nn_models_v2 import ModelFactory
from utility.pipeline_utils import get_dataset_fn
from tokenization import TokenizerFactory
from utility.json_utils import load_json
from utility.python_utils import merge, flatten
import pandas as pd
import tensorflow as tf
from scipy.sparse import load_npz, save_npz


def get_top_K_indexes(data, K):
    best_indexes = np.argsort(data, axis=1)[:, ::-1]
    best_indexes = best_indexes[:, :K]

    return best_indexes


def cosine_similarity(a, b, transpose_b=True):
    a = a / np.linalg.norm(a, axis=1)[:, np.newaxis]
    b = b / np.linalg.norm(b, axis=1)[:, np.newaxis]
    if transpose_b:
        b = b.transpose()

    return np.matmul(a, b)


def dot_product(a, b, transpose_b=True):
    if transpose_b:
        b = b.transpose()

    return np.matmul(a, b)


# Settings

model_type = "ibm2015_experimental_single_gnn_v2"
test_name = "16-05-2020-15-37-20"
test_folder = cd.LOO_DIR
save_prefix = 1
repetition_prefix = None
test_prefix = 'loo'
original_dataset_name = "IBM2015"
task_name = "Task1"
similarity_op = 'cosine'
K = 10
argument_key = 'C'
not_argumentative_key = 'NOT-ARG'
threshold = None
save_info = True

# ---------------

model_path = os.path.join(test_folder, model_type, test_name)

repr_retriever = RepresentationRetriever(save_path=os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name),
                                         save_suffix=task_name)
callbacks = [
    repr_retriever
]

# Load task data

with open(os.path.join(cd.CORD19_DIR, "{}_trees.txt".format(task_name)), "r") as f:
    task_data_trees = f.readlines()

with open(os.path.join(cd.CORD19_DIR, "{}.txt".format(task_name)), "r") as f:
    task_data_sentences = f.readlines()

task_data = {}
for item, item_tree in zip(task_data_sentences, task_data_trees):
    task_data.setdefault('Sentence', []).append(item)
    task_data.setdefault('Tree', []).append(item_tree)
task_data = pd.DataFrame.from_dict(task_data)

# Load cord-19 data

data_handle = CORD19Loader().load()
dataset_data = data_handle.get_data()

# Original model data info

model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))
original_data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
original_data_loader_type = original_data_loader_config['type']
original_data_loader_info = original_data_loader_config['configs'][original_data_loader_type]
original_loader_additional_info = {key: value['value'] for key, value in model_config.items()
                                   if 'data_loader' in value['flags']}
original_data_loader_info = merge(original_data_loader_info, original_loader_additional_info)

config_args = {key: arg['value']
               for key, arg in model_config.items()
               if 'processor' in arg['flags']
               or 'tokenizer' in arg['flags']
               or 'converter' in arg['flags']}
config_args = flatten(config_args)
config_args = merge(config_args, original_data_loader_info)
config_args_tuple = [(key, value) for key, value in config_args.items()]
config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                               original_dataset_name,
                               model_type)
config_id = get_data_config_id(filepath=model_base_path, config=config_name)
#
# # Build model pipeline
#
model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

# Build Processor
processor_type = exp_cd.MODEL_CONFIG[model_type]['processor']
processor_args = {key: arg['value'] for key, arg in model_config.items() if 'processor' in arg['flags']}
processor_args['loader_info'] = data_handle.get_additional_info()
processor_args['retrieve_label'] = False
processor_factory = ProcessorFactory()
processor = processor_factory.factory(processor_type, **processor_args)

# # Build tokenizer
tokenizer_type = exp_cd.MODEL_CONFIG[model_type]['tokenizer']
tokenizer_factory = TokenizerFactory()
tokenizer_args = {key: arg['value'] for key, arg in model_config.items() if 'tokenizer' in arg['flags']}
tokenizer = tokenizer_factory.factory(tokenizer_type, **tokenizer_args)
#
# # Build converter
converter_type = exp_cd.MODEL_CONFIG[model_type]['converter']
converter_factory = DataConverterFactory()
converter_args = {key: arg['value'] for key, arg in model_config.items() if 'converter' in arg['flags']}
converter = converter_factory.factory(converter_type, **converter_args)
#
# # Task data processing
#
task_examples = processor.get_test_examples(data=task_data, ids=np.arange(task_data.shape[0]))

# Task data conversion
#
data_path = os.path.join(cd.TESTS_DATA_DIR, original_dataset_name, model_type, str(config_id))
tokenizer_info = tokenizer_factory.supported_tokenizers[tokenizer_type].load_info(filepath=data_path,
                                                                                  prefix='{0}_{1}'.format(test_prefix,
                                                                                                          save_prefix) if test_prefix is not None else save_prefix)
converter_info = converter_factory.supported_data_converters[converter_type].load_conversion_args(
    filepath=data_path,
    prefix='{0}_{1}'.format(test_prefix, save_prefix) if test_prefix is not None else save_prefix)

#
# Tokenizer
tokenizer.initalize_with_vocab(tokenizer_info['vocab'])

# Conversion
for key, value in converter_info.items():
    setattr(converter, key, value)

tmp_filepath = os.path.join(model_path, "{}_tmp".format(task_name))

converter.convert_data(examples=task_examples,
                       label_list=None,
                       has_labels=False,
                       output_file=tmp_filepath,
                       tokenizer=tokenizer)

test_data = get_dataset_fn(filepath=tmp_filepath,
                           batch_size=len(task_examples),
                           name_to_features=converter.feature_class.get_mappings(converter_info, has_labels=False),
                           selector=converter.feature_class.get_dataset_selector(),
                           is_training=False,
                           prefetch_amount=1)

#
# # Build network
#
network_retrieved_args = {key: value['value'] for key, value in model_config.items()
                          if 'model_class' in value['flags']}
network_retrieved_args['name'] = exp_cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
if repetition_prefix is not None:
    network_retrieved_args['name'] += '_repetition_{}'.format(repetition_prefix)
if save_prefix is not None:
    network_retrieved_args['name'] += '_key_{}'.format(save_prefix)
network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)
#
test_steps = 1
#
# Custom callbacks only
for callback in callbacks:
    if hasattr(callback, 'on_build_model_begin'):
        callback.on_build_model_begin(logs={'network': network})

text_info = merge(tokenizer_info, converter_info)
network.build_model(text_info=text_info)

# Custom callbacks only
for callback in callbacks:
    if hasattr(callback, 'on_build_model_end'):
        callback.on_build_model_end(logs={'network': network})

# Setup model by feeding an input
network.predict(x=iter(test_data()), steps=test_steps)

# load pre-trained weights
current_weight_filename = os.path.join(model_path,
                                       '{}.h5'.format(network_retrieved_args['name']))
network.load(os.path.join(model_path, current_weight_filename))

# Inference
network.predict(x=iter(test_data()),
                steps=test_steps,
                callbacks=callbacks)

# Load graph embeddings

saved_data_path = os.path.join(cd.UNSEEN_DATA_DIR, model_type, test_name)
task_representation_path = os.path.join(saved_data_path,
                                        '{0}_{1}_graph_representations.json'.format(network.name,
                                                                                    task_name))
dataset_representation_path = os.path.join(saved_data_path,
                                           '{0}_graph_representations.json'.format(network.name))

dataset_predictions_path = os.path.join(saved_data_path,
                                        '{}_unseen_predictions.json'.format(network.name))

task_embeddings = load_json(task_representation_path)

dataset_embeddings = load_json(dataset_representation_path)

print("Dataset embedding shape: ", dataset_embeddings.shape)

dataset_predictions = load_json(dataset_predictions_path)

assert dataset_embeddings.shape[0] == dataset_data.shape[0]
assert dataset_predictions.shape[0] == dataset_data.shape[0]

# Filter out non-argumentative predictions

dataset_predictions = np.argmax(dataset_predictions, axis=1)

label_map = {key: np.argmax(value) for key, value in converter_info['label_map'].items()}
inverse_label_map = {value: key for key, value in label_map.items()}

argumentative_indexes = np.where(dataset_predictions != label_map[not_argumentative_key])[0]

dataset_embeddings = dataset_embeddings[argumentative_indexes]

dataset_predictions = dataset_predictions[argumentative_indexes]

print("Filtering non-argumentative sentences shape: ", dataset_embeddings.shape)

# Save info
if save_info:
    # Textual info -> pandas.DataFrame -> Sentence | Paper Id | Title | Tag | Index
    argumentative_data = dataset_data.iloc[argumentative_indexes]
    argumentative_data.loc[:, 'Index'] = argumentative_indexes
    argumentative_data.loc[:, 'Tag'] = [inverse_label_map[pred] for pred in dataset_predictions]
    argumentative_data.to_csv(os.path.join(saved_data_path, '{}_argumentative_dataset.csv'.format(network.name)),
                              index=False)

    # Graph embeddings
    np.save(os.path.join(saved_data_path, '{}_argumentative_graph_representations.npy'.format(network.name)),
            dataset_embeddings)

    # BoW Features
    bow_matrix = load_npz(os.path.join(cd.CORD19_DIR, 'cord19_feature_matrix.npz'))
    argumentative_bow_matrix = bow_matrix.tocsr()[argumentative_indexes, :]
    save_npz(os.path.join(saved_data_path, '{}_bow_feature_matrix.npz'.format(network.name)),
             argumentative_bow_matrix)

# [Optional] Select specific argument class

if argument_key is not None:
    filtered_indexes = np.where(dataset_predictions == label_map[argument_key])[0]
    dataset_embeddings = dataset_embeddings[filtered_indexes]
    dataset_predictions = dataset_predictions[filtered_indexes]

    print("Selecting arguments of class {} shape: ".format(argument_key), dataset_embeddings.shape)

    # [Optional] Filter out predictions whose confidence is under specified threshold
    if threshold is not None:
        dataset_raw_predictions_path = os.path.join(saved_data_path,
                                                    '{}_raw_predictions.json'.format(network.name))
        raw_predictions = load_json(dataset_raw_predictions_path)
        raw_predictions = raw_predictions[filtered_indexes]
        raw_predictions = tf.nn.softmax(raw_predictions).numpy()

        threshold_indexes = np.where(np.max(raw_predictions, axis=1) >= threshold)[0]

        dataset_embeddings = dataset_embeddings[threshold_indexes]
        dataset_predictions = dataset_predictions[threshold_indexes]

        print("Thresholding prediction confidence by {}: ".format(threshold), dataset_embeddings.shape)

# Similarity

# [#task_data, #dataset_samples]
if similarity_op == 'cosine':
    similarity = cosine_similarity(a=task_embeddings, b=dataset_embeddings)
else:
    similarity = dot_product(a=task_embeddings, b=dataset_embeddings)

best_K_indexes = get_top_K_indexes(similarity, K)

with open(os.path.join(cd.CORD19_DIR, "{}.txt".format(task_name)), "r") as f:
    task_data = f.readlines()

for task_sample_id in range(similarity.shape[0]):
    print("Task bullet point {0}: {1}".format(task_sample_id, task_data[task_sample_id]))
    print("Top {0} ranking indexes: {1}".format(K, best_K_indexes[task_sample_id]))
    print("Top {0} ranking values: {1}".format(K, similarity[task_sample_id, best_K_indexes[task_sample_id]]))
    print("Top {0} ranking items:\n {1}".format(K, dataset_data.iloc[best_K_indexes[task_sample_id]].Sentence.values))
    print('*' * 50)

# Visualization

visualizer = TSNE(n_components=2, n_jobs=3)
all_embeddings = visualizer.fit_transform(np.concatenate((dataset_embeddings, task_embeddings), axis=0))

visualized_dataset_embeddings = all_embeddings[:-task_embeddings.shape[0]]
visualized_task_embeddings = all_embeddings[-task_embeddings.shape[0]:]

all_predictions = np.concatenate((dataset_predictions, np.full(shape=(task_embeddings.shape[0]),
                                                               fill_value=np.max(list(label_map.values())) + 1)),
                                 axis=0)
colors_number_for_palette = len(set(all_predictions))

sns.scatterplot(all_embeddings[:, 0], all_embeddings[:, 1], hue=all_predictions,
                legend='full', palette=sns.color_palette('hls', colors_number_for_palette))
plt.show()

# Clear

# os.remove(tmp_filepath)
