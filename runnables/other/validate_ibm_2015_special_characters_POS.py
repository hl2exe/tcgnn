"""

For each special character in the dataset, checks its POS tags

"""

import const_define as cd
import os
import pandas as pd
import tensorflow as tf
from utility.graph_utils import retrieve_trees_info
import numpy as np
from collections import Counter
from tqdm import tqdm


def get_tagging_info(symbols, node_set, edge_set, current_info):

    for symbol in symbols:
        if symbol in node_set:
            symbol_indexes = [idx for idx, el in enumerate(node_set) if el == symbol]
            parent_indexes = [pair[0] for idx in symbol_indexes for pair in edge_set if idx in pair and idx == pair[1]]
            symbol_tags = [el for idx, el in enumerate(node_set) if idx in parent_indexes]
            current_info.setdefault(symbol, []).extend(symbol_tags)

    return current_info


tok = tf.keras.preprocessing.text.Tokenizer(filters="", lower=False, oov_token=1)

df = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'dataset.csv'))

sentences = df.Sentence.values

if os.path.isfile('ibm_2015_memo.npy'):
    memo = np.load('ibm_2015_memo.npy', allow_pickle=True).item()
else:
    memo = {}

tree_info, upd_memo = retrieve_trees_info(df.Tree.values, memo=memo)

trees = tree_info[0]
edge_indices = tree_info[1]

tok.fit_on_texts(trees)

# short_symbols = [str(key).lower() for key in tok.word_index if len(str(key)) <= 5]
short_symbols = ['-LRB-', '-RRB-']

# key -> symbol, value -> Counter
tags_info = {}

for node_set, edge_set in tqdm(zip(trees, edge_indices)):
    tags_info = get_tagging_info(short_symbols, node_set, edge_set, tags_info)

tags_count_info = {key: Counter(item) for key, item in tags_info.items()}

for key, item in tags_count_info.items():
    print('Symbol {0} tags: {1}'.format(key, item))