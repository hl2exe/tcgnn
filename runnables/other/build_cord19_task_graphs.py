import os
import const_define as cd
from pycorenlp import StanfordCoreNLP
from tqdm import tqdm
import json


def corenlp_parse(texts, parser):
    trees = []
    for sentence in tqdm(texts, position=0, leave=True):
        # print('Considering sentence: {}'.format(sentence))

        annotation = parser.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'parse'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            data_tree = data['sentences'][0]['parse'].replace('\n', ' ')
            data_tree = data_tree.split()
            data_tree = ' '.join(data_tree)
            trees.append(data_tree)

    return trees


# Settings

port = 9002

task_name = "Task1"

# Load dataset

with open(os.path.join(cd.CORD19_DIR, '{}.txt'.format(task_name)), 'r') as f:
    task_data = f.readlines()

# CoreNLP parsing

parser = StanfordCoreNLP('http://localhost:{}'.format(port))

task_trees = corenlp_parse(texts=task_data, parser=parser)
task_trees = [item + os.linesep for item in task_trees]

with open(os.path.join(cd.CORD19_DIR, '{}_trees.txt'.format(task_name)), 'w') as f:
    f.writelines(task_trees)
