"""

@Author: Federico Ruggeri

@Date: 09/12/2019

"""

import os
from datetime import datetime

import numpy as np
import tensorflow as tf
from sklearn.model_selection import LeaveOneOut

import const_define as cd
from custom_callbacks_v2 import GeneratorEarlyStopping, GeneratorTrainingLogger
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import pairwise_loo_test
from utility.json_utils import save_json, load_json
from utility.log_utils import Logger
from utility.python_utils import merge

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    loo_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_LOO_TEST_CONFIG_NAME))

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[
        loo_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_GENERATOR_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)

    data_handle = data_loader.load(**data_loader_info)

    # LOO
    loo = LeaveOneOut()

    # Distributed info
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    if loo_test_config['pre_loaded_model'] is None:
        current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
        save_base_path = os.path.join(cd.LOO_DIR, loo_test_config['model_type'], current_date)

        if loo_test_config['save_model'] and not os.path.isdir(save_base_path):
            os.makedirs(save_base_path)
    else:
        save_base_path = os.path.join(cd.LOO_DIR, loo_test_config['model_type'], loo_test_config['pre_loaded_model'])
        if not os.path.isdir(save_base_path):
            msg = "Can't find given pre-trained model. Got: {}".format(save_base_path)
            raise RuntimeError(msg)

    # Logging
    Logger.set_log_path(save_base_path)
    logger = Logger.get_logger(__name__)

    # Callbacks
    # TODO: automatize callbacks creation
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = GeneratorEarlyStopping(**callbacks_data['generatorearlystopping'])
    training_logger = GeneratorTrainingLogger(filepath=save_base_path, suffix=None)
    callbacks = [
        early_stopper
    ]

    if loo_test_config['save_model']:
        callbacks.append(training_logger)

    scores = pairwise_loo_test(data_handle=data_handle,
                               callbacks=callbacks,
                               model_type=loo_test_config['model_type'],
                               training_config=training_config,
                               error_metrics=loo_test_config['error_metrics'],
                               error_metrics_additional_info=loo_test_config['error_metrics_additional_info'],
                               error_metrics_nicknames=loo_test_config['error_metrics_nicknames'],
                               compute_test_info=loo_test_config['compute_test_info'],
                               network_args=model_config,
                               save_model=loo_test_config['save_model'],
                               test_path=save_base_path,
                               split_key=loo_test_config['split_key'],
                               data_loader_info=data_loader_info,
                               loo=loo,
                               validation_percentage=loo_test_config['validation_percentage'],
                               distributed_info=distributed_info)

    # Validation
    logger.info('Average validation scores: {}'.format(
        {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))

    # Test
    logger.info('Average test scores: {}'.format(
        {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if not key.startswith('avg')}))

    if loo_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_PREDICTIONS_NAME), scores['predictions'])
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME), model_config)
        save_json(os.path.join(save_base_path, cd.JSON_TRAINING_CONFIG_NAME), training_config)
        save_json(os.path.join(save_base_path, cd.JSON_LOO_TEST_CONFIG_NAME), data=loo_test_config)
        save_json(os.path.join(save_base_path, cd.JSON_CALLBACKS_NAME), data=callbacks_data)
        save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
