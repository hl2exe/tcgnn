"""

Runs a sequence of tests with different configurations.
Alternates between
1. quick_test_setup.py + distributed_model_config.json
2. test_cross_validation.py

"""

import os
import const_define as cd
from subprocess import Popen
from tqdm import tqdm
from utility.json_utils import load_json, save_json
from utility.log_utils import Logger

logger = Logger.get_logger(__name__)


def run_quick_setup(quick_setup_info):
    script_path = os.path.join(cd.PROJECT_DIR, 'runnables', 'other', 'quick_test_setup.py')
    cmd = ['python', script_path]
    for key, value in quick_setup_info.items():
        cmd.append(key)
        # cmd.append('\'{0}\''.format(value))
        cmd.append(value)

    process = Popen(cmd)
    process.wait()


def update_model_config(model_config_info):
    filepath = os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME)
    file_config = load_json(filepath)
    model_type = model_config_info['model_type']
    model_args = model_config_info['model_args']
    logger.info("[Model Config] Model Type -> {}".format(model_type))
    logger.info("[Model Config] Model Args -> {}".format(model_args))
    for key, value in model_args.items():
        file_config[model_type][key]['value'] = value

    save_json(filepath, file_config)


def run_test(test_script):
    script_path = os.path.join(cd.PROJECT_DIR, 'runnables', '{}.py'.format(test_script))
    assert os.path.isfile(script_path)
    cmd = ['python', script_path]
    process = Popen(cmd)
    process.wait()


tests_info = {
    'quick_setup': [
        {
            '-dn': 'ibm2015',
            '-la': '{"merge": "True", "selector": "tree", "merge_label": "C_Label"}',
            '-mt': "bert-base-uncased",
            '-mdt': "ibm2015",
            '-tt': 'ibm2015cvmerged',
            '-test': 'cv_test',
            '-ca': '{"earlystopping": {"patience": 10, "monitor": "val_binary_f1_score"}}',
            '-ivt': 'True',
            '-rf': "C_f1_es_300",
            '-gsi': '0',
            '-gei': '1',
            '-e': "100",
            '-bs': "64"
        },
        {
            '-dn': 'ibm2015',
            '-la': '{"merge": "True", "selector": "tree", "merge_label": "E_Label"}',
            '-mt': "bert-base-uncased",
            '-mdt': "ibm2015",
            '-tt': 'ibm2015cvmerged',
            '-test': 'cv_test',
            '-ca': '{"earlystopping": {"patience": 10, "monitor": "val_binary_f1_score"}}',
            '-ivt': 'True',
            '-rf': "E_f1_es_300",
            '-gsi': '0',
            '-gei': '1',
            '-e': "100",
            '-bs': "64"
        },
    ],
    'model_config': [
        {
            "model_type": "ibm2015_bert-base-uncased",
            "model_args": {
            }
        },
        {
            "model_type": "ibm2015_bert-base-uncased",
            "model_args": {
            }
        },
    ],
    'test_script': [
        'test_cross_validation_v2',
        'test_cross_validation_v2',
    ],
}

total_runs = len(tests_info['quick_setup'])
for key, value in tests_info.items():
    assert len(value) == total_runs

for run in tqdm(range(total_runs)):
    # Quick setup
    logger.info("[Run {0}] Quick setup ".format(run + 1))
    run_quick_setup_info = tests_info['quick_setup'][run]
    run_quick_setup(run_quick_setup_info)

    # Model config
    logger.info("[Run {0}] Model config".format(run + 1))
    run_model_config_info = tests_info['model_config'][run]
    update_model_config(run_model_config_info)

    # Test
    logger.info("[Run {0}] Test".format(run + 1))
    test_script = tests_info['test_script'][run]
    run_test(test_script)
