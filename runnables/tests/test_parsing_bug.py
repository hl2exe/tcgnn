"""

Checking if tree parsing is consistent across all dataset

"""

import pandas as pd
from old_stuff import const_define as cd
import os
from utility.graph_utils import retrieve_trees_info, pad_tree_data
import numpy as np
from keras.preprocessing.text import Tokenizer
from utility.embedding_utils import convert_number_to_binary_list

df_path = os.path.join(cd.IBM2015_DIR, 'dataset.csv')
df = pd.read_csv(df_path)

result, _ = retrieve_trees_info(df.Tree.values)

tokenizer = Tokenizer(filters='', oov_token=1)
nodes = result[0][df.shape[0] // 2]
nodes = set([node for graph in nodes for node in graph])
tokenizer.fit_on_texts(nodes)

result = pad_tree_data(nodes=tokenizer.texts_to_sequences(result[0]),
                       edge_indices=result[1],
                       edge_features=[list(map(convert_number_to_binary_list, edge_features))
                                      for edge_features in result[2]],
                       node_indices=result[3],
                       node_segments=result[4])

nodes = result[0]
segments = result[-2]

check = [len(s) != (len(n) - 1) * 2 for s, n in zip(segments, nodes)]
indexes = np.argwhere(check)

if indexes:
    print(indexes)
