import numpy as np
from utility.draw_graph_utils import draw_tree
import matplotlib.pyplot as plt
from utility.graph_utils import get_subtree_info
from tqdm import tqdm


def cut_loss(P, T, S, A):
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    eye_mask = np.eye(P.shape[-1])
    mask = np.triu(np.ones_like(eye_mask)) - eye_mask
    mask = mask[:, :, np.newaxis]

    residuals = span_weights[np.newaxis, :, :] * mask
    residuals = np.sum(residuals, axis=1)
    residuals = np.maximum(span_weights - residuals, 0.)
    residuals = residuals.transpose()

    A = 0.5 * A

    # w_P = P * span_weights.transpose()
    w_P = P * residuals

    A_pool = np.matmul(np.matmul(w_P.transpose(), A), w_P)
    # A_pool = np.matmul(w_P.transpose(), w_P)
    num = np.trace(A_pool)
    # num = np.sum(w_P)

    # D = np.sum(A, axis=-1)
    # D = D[:, np.newaxis] * np.eye(A.shape[-1])
    # D_pool = np.matmul(np.matmul(D, w_P).transpose(), w_P)
    D_pool = np.matmul(np.matmul(P.transpose(), A), P)
    # D_pool = np.matmul(P.transpose(), P)

    den = np.trace(D_pool) + 1e-12
    # den = np.sum(P) + 1e-12

    return - (num / den)


def alt_cut_loss(P, T, S, A):

    # K x K
    A_pool = np.matmul(T, np.matmul(S, P))

    # K x 1
    D_pool = np.sum(P, axis=0)

    return - np.trace(A_pool / (D_pool[:, np.newaxis] + 1e-12)) / P.shape[-1]


def ortho_loss(P, T, S):
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    eye_mask = np.eye(P.shape[-1])
    mask = np.triu(np.ones_like(eye_mask)) - eye_mask
    mask = mask[:, :, np.newaxis]

    residuals = span_weights[np.newaxis, :, :] * mask
    residuals = np.sum(residuals, axis=1)
    residuals = np.maximum(span_weights - residuals, 0.)
    residuals = residuals.transpose()

    # w_P = P * span_weights.transpose()
    w_P = P * residuals

    pooled_span_area = np.matmul(w_P.transpose(), w_P)

    I_S = np.eye(P.shape[-1], dtype=np.float)
    loss = pooled_span_area / (np.linalg.norm(pooled_span_area, ord='fro') + 1e-12) - I_S / np.linalg.norm(I_S, ord='fro')
    loss = np.linalg.norm(loss, ord='fro')
    return loss


def alt_ortho_loss(P, T, S):

    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    span_inter = span_weights[:, np.newaxis, :] * span_weights[np.newaxis, :, :]

    P = P.transpose()
    pooled_inter = P[:, np.newaxis, :]

    inter_intensity = np.sum(span_inter * pooled_inter, axis=-1)
    inter_intensity = inter_intensity - np.eye(T.shape[0]) * inter_intensity

    prod_mask = span_weights[:, np.newaxis, :] * span_weights[np.newaxis, :, :]
    prod_mask = np.sum(prod_mask, axis=-1)
    prod_mask = np.maximum(prod_mask, 0.)
    prod_mask = np.minimum(prod_mask, 1.)

    diff_mask = span_weights[:, np.newaxis, :] - span_weights[np.newaxis, :, :]
    diff_mask = np.sum(diff_mask, axis=-1)
    diff_mask[diff_mask >= 0] = 1.0
    diff_mask = np.maximum(diff_mask, 0.)

    mask = prod_mask * diff_mask

    inter_intensity *= mask

    return np.linalg.norm(inter_intensity, ord='fro')


def res_ortho_loss(P, T, S):
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]

    span_weights = np.sum(span_area, axis=1)

    eye_mask = np.eye(P.shape[-1])
    mask = np.triu(np.ones_like(eye_mask)) - eye_mask
    mask = mask[:, :, np.newaxis]

    residuals = span_weights[np.newaxis, :, :] * mask
    residuals = np.sum(residuals, axis=1)
    residuals = np.maximum(span_weights - residuals, 0.)
    residuals = residuals.transpose()

    # weight_sum = np.sum(span_weights, axis=0)
    # weight_sum = weight_sum - span_weights
    # weight_sum = np.minimum(weight_sum, 1.)
    # residual_diff = np.maximum(span_weights - weight_sum, 0.)
    # residual_inter = span_weights * weight_sum
    # diff_mask = np.max(residual_diff, axis=-1)
    # residuals = diff_mask[:, np.newaxis] * residual_diff + (1 - diff_mask)[:, np.newaxis] * residual_inter
    # residuals = residuals.transpose()

    pooled_span_area = np.matmul(residuals.transpose(), residuals)

    I_S = np.eye(P.shape[-1], dtype=np.float)
    I_S *= S.shape[-1] / P.shape[-1]   # N / K
    loss = pooled_span_area - I_S
    loss = np.linalg.norm(loss, ord='fro')
    return 0.


# PTK

def ptk_separation_loss(P, T, S, alpha=.5):

    meta_node_amount = P.shape[-1]

    # K x N
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    # K x N
    P = P.transpose()

    P = np.minimum(P, alpha) / alpha

    # K x K x N
    pooling_union = np.minimum(P[np.newaxis, :, :] + P[:, np.newaxis, :], 1.)
    pooled_span_weights = pooling_union * span_weights[:, np.newaxis, :]

    # Step 1: Kernel specific weight
    # PTK has no particular restriction -> weight is 1 everywhere within the root span region

    # K x K x N
    W = np.minimum(span_weights[np.newaxis, :, :] + span_weights[:, np.newaxis, :], 1.)

    W *= pooled_span_weights

    # Step 2: Similarity

    # Hamming distance
    # K x K x N
    # dissimilarity = P[:, np.newaxis, :] * (1. - P[np.newaxis, :, :]) \
    #                 + (1. - P[:, np.newaxis, :]) * P[np.newaxis, :, :]
    dissimilarity = np.abs(P[:, np.newaxis, :] - P[np.newaxis, :, :])

    dissimilarity *= W

    # K x K
    dissimilarity = np.max(dissimilarity, axis=-1)

    penalty = np.maximum(1. - dissimilarity, 0.)
    penalty = penalty - np.tril(penalty)

    penalty = np.sum(penalty)

    # combinations = meta_node_amount * (meta_node_amount - 1) / 2
    # return penalty / combinations

    return penalty


def ptk_parametric_contiguous_constraint(P, T, S, A, alpha=0.5):
    P = P.transpose()

    # K x N x N
    # pair_intensities = P[:, :, np.newaxis] * P[:, np.newaxis, :]
    scaled_P = np.minimum(P, alpha) / alpha
    pair_intensities = np.maximum(scaled_P[:, :, np.newaxis] + scaled_P[:, np.newaxis, :] - 1., 0.)

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # K x N x N
    all_intensities = pair_intensities * A[np.newaxis, :, :]

    # K x N
    all_intensities = np.max(all_intensities, axis=-1)
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)
    # all_intensities = 1 / alpha * np.maximum(all_intensities - alpha, 0.)

    # filter out leaves and check node span
    # 1 x N
    leaf_mask = np.minimum(np.sum(S, axis=-1) - 1., 1.)

    all_intensities *= leaf_mask[np.newaxis, :]

    # root span aware
    # K x N x N
    all_intensities = all_intensities[:, np.newaxis, :] * S[np.newaxis, :, :]

    # K x N x N
    asyn_adjacency_matrix = A - np.tril(A)
    j_weight = np.max(scaled_P[:, np.newaxis, :] * asyn_adjacency_matrix[np.newaxis, :, :],
                      axis=-1)
    # i_weight = node_pooling[:, np.newaxis, :]
    t_weight = T[:, np.newaxis, :]
    # penalty_weight = np.maximum(j_weight[:, np.newaxis, :], i_weight)
    # penalty_weight = np.maximum(penalty_weight, t_weight)
    penalty_weight = np.maximum(j_weight[:, np.newaxis, :], t_weight)

    # K x N
    constraint = np.sum(all_intensities * penalty_weight, axis=-1)

    # K
    constraint = np.sum(constraint * T, axis=-1)

    return np.mean(constraint)


# STK


def stk_separation_loss(P, T, S, alpha=.5):

    meta_node_amount = P.shape[-1]

    # K x N
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    # K x N
    P = P.transpose()

    P = np.minimum(P, alpha) / alpha

    # K x K x N
    pooling_union = np.minimum(P[np.newaxis, :, :] + P[:, np.newaxis, :], 1.)
    span_union = np.minimum(span_weights[np.newaxis, :, :] + span_weights[:, np.newaxis, :], 1.)
    pooled_span_weights = pooling_union * span_union

    # Step 1: Kernel specific weight
    # STK just cares about root nodes -> we follow T distribution

    # K x K x N
    W = span_weights * T
    W = np.minimum(W[np.newaxis, :, :] + W[:, np.newaxis, :], 1.)

    W *= pooled_span_weights

    # Step 2: Similarity

    # Hamming distance
    # K x K x N
    # dissimilarity = P[:, np.newaxis, :] * (1. - P[np.newaxis, :, :]) \
    #                 + (1. - P[:, np.newaxis, :]) * P[np.newaxis, :, :]
    dissimilarity = np.abs(P[:, np.newaxis, :] - P[np.newaxis, :, :])

    dissimilarity *= W

    # K x K
    dissimilarity = np.max(dissimilarity, axis=-1)

    penalty = np.maximum(1. - dissimilarity, 0.)
    penalty = penalty - np.tril(penalty)

    penalty = np.sum(penalty)

    # combinations = meta_node_amount * (meta_node_amount - 1) / 2
    # return penalty / combinations

    return penalty


def stk_parametric_contiguous_constraint(P, T, S, A, alpha=0.5):
    P = P.transpose()

    A = A - np.tril(A)

    # K x N x N
    # pair_intensities = P[:, :, np.newaxis] * P[:, np.newaxis, :]
    scaled_P = np.minimum(P, alpha) / alpha
    pair_intensities = np.maximum(scaled_P[:, :, np.newaxis] + scaled_P[:, np.newaxis, :] - 1., 0.)

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # K x N x N
    all_intensities = pair_intensities

    # K x N
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)
    all_intensities *= A[np.newaxis, :, :]
    all_intensities = np.sum(all_intensities, axis=-1)
    # all_intensities = 1 / alpha * np.maximum(all_intensities - alpha, 0.)

    # filter out leaves and check node span
    # 1 x N
    leaf_mask = np.minimum(np.sum(S, axis=-1) - 1., 1.)

    all_intensities *= leaf_mask[np.newaxis, :]

    # root span aware
    # K x N x N
    all_intensities = all_intensities[:, np.newaxis, :] * S[np.newaxis, :, :]

    # K x N
    constraint = np.sum(all_intensities, axis=-1)

    # K
    constraint = np.sum(constraint * T, axis=-1)

    return np.mean(constraint)


# SSTK

def sstk_separation_loss(P, T, S, alpha=.5):

    meta_node_amount = P.shape[-1]

    # K x N
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    # K x N
    P = P.transpose()

    P = np.minimum(P, alpha) / alpha

    # K x K x N
    pooling_union = np.minimum(P[np.newaxis, :, :] + P[:, np.newaxis, :], 1.)
    pooled_span_weights = pooling_union * span_weights[:, np.newaxis, :]

    # Step 1: Kernel specific weight
    # SSTK has no particular restriction -> weight is 1 everywhere within the root span region

    # K x K x N
    W = np.minimum(span_weights[np.newaxis, :, :] + span_weights[:, np.newaxis, :], 1.)

    W *= pooled_span_weights

    # Step 2: Similarity

    # Hamming distance
    # K x K x N
    # dissimilarity = P[:, np.newaxis, :] * (1. - P[np.newaxis, :, :]) \
    #                 + (1. - P[:, np.newaxis, :]) * P[np.newaxis, :, :]
    dissimilarity = np.abs(P[:, np.newaxis, :] - P[np.newaxis, :, :])

    dissimilarity *= W

    # K x K
    dissimilarity = np.max(dissimilarity, axis=-1)

    penalty = np.maximum(1. - dissimilarity, 0.)
    penalty = penalty - np.tril(penalty)

    penalty = np.sum(penalty)

    # combinations = meta_node_amount * (meta_node_amount - 1) / 2
    # return penalty / combinations

    return penalty


def sstk_parametric_contiguous_constraint(P, T, S, A, alpha=0.5):
    P = P.transpose()

    A = A - np.tril(A)

    # N
    leaf_mask = np.minimum(np.sum(S, axis=-1) - 1., 1.)

    # K x N x N
    # pair_intensities = P[:, :, np.newaxis] * P[:, np.newaxis, :]
    scaled_P = np.minimum(P, alpha) / alpha
    pair_intensities = np.maximum(scaled_P[:, :, np.newaxis] + scaled_P[:, np.newaxis, :] - 1., 0.)

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # K x N x N
    all_intensities = pair_intensities

    # K x N
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)
    all_intensities *= A[np.newaxis, :, :]

    # weight only for meta-meta pair
    all_intensities *= leaf_mask[np.newaxis, :] * leaf_mask[:, np.newaxis]

    all_intensities = np.sum(all_intensities, axis=-1)
    # all_intensities = 1 / alpha * np.maximum(all_intensities - alpha, 0.)

    # filter out leaves and check node span
    all_intensities *= leaf_mask[np.newaxis, :]

    # filter out meta-nodes th

    # root span aware
    # K x N x N
    all_intensities = all_intensities[:, np.newaxis, :] * S[np.newaxis, :, :]

    # K x N
    constraint = np.sum(all_intensities, axis=-1)

    # K
    constraint = np.sum(constraint * T, axis=-1)

    return np.mean(constraint)


nodes = np.array(['S', "N", "Mary", "VP", "V", "brought", "NP", "D", "a", "N", "cat", "PP", "IN", "to", "N", "school"])
A = np.zeros((nodes.shape[0], nodes.shape[0]))

# Fill values
A[0, 1] = 1
A[0, 3] = 1

A[1, 0] = 1
A[1, 2] = 1

A[2, 1] = 1

A[3, 0] = 1
A[3, 4] = 1
A[3, 6] = 1
A[3, 11] = 1

A[4, 3] = 1
A[4, 5] = 1

A[5, 4] = 1

A[6, 3] = 1
A[6, 7] = 1
A[6, 9] = 1

A[7, 6] = 1
A[7, 8] = 1

A[8, 7] = 1

A[9, 6] = 1
A[9, 10] = 1

A[10, 9] = 1

A[11, 3] = 1
A[11, 12] = 1
A[11, 14] = 1

A[12, 11] = 1
A[12, 13] = 1

A[13, 12] = 1

A[14, 11] = 1
A[14, 15] = 1

A[15, 14] = 1

node_idxs = np.arange(len(nodes))
node_depths = np.array([0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 2, 3, 4, 3, 4])

st_subtree_nodes, _, _, _, _, _ = get_subtree_info(node_idxs=node_idxs,
                                                   depths=node_depths,
                                                   is_directional=False,
                                                   kernel='stk')


def build_node_spans(nodes, subtree_nodes):
    node_spans = np.zeros((len(nodes), len(nodes)))

    for subtree in subtree_nodes:
        node_spans[subtree[0], subtree] = 1.

    for node in nodes:
        node_spans[node, node] = 1.

    return node_spans


S = build_node_spans(nodes=node_idxs, subtree_nodes=st_subtree_nodes)


def softmax(x, axis=-1):
    return np.exp(x) / np.sum(np.exp(x), axis=axis)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def mock_pooling(nodes, K, use_sigmoid=False):
    pooling_matrix = np.zeros((len(nodes), K), dtype=np.float32)
    att_operator = sigmoid if use_sigmoid else softmax

    for node_idx in np.arange(len(nodes)):
        hard_pooling = np.random.randint(low=0, high=K)
        hard_encoding = np.zeros(K)
        hard_encoding[hard_pooling] = 1.0
        # pooling_matrix[node_idx] = att_operator(np.random.rand(K) * 5)
        pooling_matrix[node_idx] = hard_encoding

    return pooling_matrix


def mock_root_pooling(nodes, K, node_spans):
    pooling_matrix = np.zeros((K, len(nodes)), dtype=np.float32)
    leaf_mask = np.minimum(np.sum(node_spans, axis=-1) - 1., 1.)
    valid_indexes = np.where(leaf_mask)[0]
    for k in range(K):
        hot_encoding = np.zeros(len(nodes))
        rand_pos = np.random.choice(valid_indexes, size=1)
        hot_encoding[rand_pos] = 1.
        pooling_matrix[k] = hot_encoding

    return pooling_matrix


P = np.array([1, 1, 1,
              0, 1, 1,
              0, 0, 0,
              0, 1, 1,
              1, 0, 1,
              0, 1, 1,
              0, 0, 1]).reshape(-1, 3).astype(np.float)
T = np.array([1, 0, 0, 0, 0, 0, 0,
              1, 0, 0, 0, 0, 0, 0,
              1, 0, 0, 0, 0, 0, 0]).reshape(3, -1).astype(np.float)
S = np.array(
    [1, 1, 1, 1, 1, 1, 1,
     0, 1, 1, 0, 0, 0, 0,
     0, 0, 1, 0, 0, 0, 0,
     0, 0, 0, 1, 1, 0, 0,
     0, 0, 0, 0, 1, 0, 0,
     0, 0, 0, 0, 0, 1, 1,
     0, 0, 0, 0, 0, 0, 1]).reshape(7, 7).astype(np.float)
A = np.array([1, 1, 0, 1, 0, 1, 0,
              1, 1, 1, 0, 0, 0, 0,
              0, 1, 1, 0, 0, 0, 0,
              1, 0, 0, 1, 1, 0, 0,
              0, 0, 0, 1, 1, 0, 0,
              1, 0, 0, 0, 0, 1, 1,
              0, 0, 0, 0, 0, 1, 1]).reshape(7,7).astype(np.float)

iterations = 100
K = 3
connectivity_threshold = 1.

results = []

for it in tqdm(range(iterations)):

    # P = mock_pooling(nodes=node_idxs, K=K, use_sigmoid=False)
    # T = mock_root_pooling(nodes=node_idxs, K=K, node_spans=S)

    # results.append((cut_loss(P, T, S, A), ortho_loss(P, T, S), res_ortho_loss(P, T, S), P, T))
    results.append((alt_cut_loss(P, T, S, A),
                    sstk_separation_loss(P, T, S, connectivity_threshold),
                    sstk_parametric_contiguous_constraint(P, T, S, A, connectivity_threshold),
                    P, T))


results = sorted(results, key=lambda x: x[0] + x[1] + x[2])


print("Best result -> Cut loss: {0},"
      " Kernel separation loss: {1},"
      " Kernel contiguous loss: {2}".format(results[0][0], results[0][1], results[0][2]))

# print('P = ', P)
# print('T = ', T)
# print('S = ', S)
# print('A = ', A)

for k in range(K):
    best_P = results[0][-2]
    best_T = results[0][-1]
    draw_tree(nodes=best_P[:, k], adjacency_matrix=A, title_name='Node pooling {}'.format(k))
    draw_tree(nodes=best_T[k, :], adjacency_matrix=A, title_name='Root pooling {}'.format(k))
plt.show()
print()


