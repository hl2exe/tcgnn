import tensorflow as tf

tf.compat.v1.enable_eager_execution()
import numpy as np

batch_size = 4

# Examples:
# N1 -- E1 -- N2 -- E2 -- N3
# N1 -- E1 -- N2 -- E2 -- N3 -- E3 -- N1
# N1 -- E1 -- N2
# N1 -- E1 -- N3 -- E2 -- N2

# [batch_size, max_nodes, dim]
nodes = np.array([
    1, 1, 2, 2, 1,
    0, 0, 1, 5, 4,
    0, 1, 2, 3, 4,
    1, 1, 2, 2, 1,
    0, 0, 1, 5, 4,
    0, 1, 2, 3, 4,
    1, 1, 2, 2, 1,
    0, 0, 1, 5, 4,
    0, 0, 0, 0, 0,
    1, 1, 2, 2, 1,
    0, 0, 1, 5, 4,
    0, 1, 2, 3, 4,
]).reshape(batch_size, 3, 5)

# [batch_size, max_edges, dim]
edges = np.array([
    0, 0, 0, 0, 1,
    0, 0, 0, 1, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 1,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 1,
    0, 0, 0, 0, 1,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 1,
    0, 0, 0, 1, 0,
    0, 0, 0, 0, 0,
]).reshape(batch_size, 3, 5)

# [batch_size, max_edges, 2]
edge_indices = np.array(
    [[[0, 1], [1, 2], [0, 0]], [[0, 1], [1, 2], [2, 0]], [[0, 1], [0, 0], [0, 0]], [[0, 2], [2, 1], [0, 0]]])

# [batch_size, max_edges]
edge_mask = np.array([[1, 1, 0], [1, 1, 1], [1, 0, 0], [1, 1, 0]])

# [batch_size, max_edges, 2, dim]
pairs = tf.gather(nodes, edge_indices, axis=1, batch_dims=1)
print('Pairs: ', pairs)

# [batch_size, max_edges, 2 * dim]
pairs = tf.reshape(pairs, [batch_size, 3, -1])

# [batch-size, max_edges, 3 * dim]
passing_input = tf.concat((pairs, edges), axis=-1)

edge_mask = tf.expand_dims(edge_mask, axis=-1)
passing_input = passing_input * edge_mask

print('Nodes: \n', nodes)
print('Message passing input: \n', passing_input)
print(passing_input.shape)

# [batch_size, 6]
node_indices = np.array([[0, 0, 1, 1, 2, 2], [0, 2, 0, 1, 1, 2], [0, 0, 1, 1, 1, 1], [0, 1, 0, 1, 2, 2]])

# [batch_size, 6, 3 * dim]
per_node_messages = tf.gather(passing_input, node_indices, batch_dims=1)
print('Per node messages: \n', per_node_messages)
print(per_node_messages.shape)

# [batch_size * 6]
node_segments = np.array([[0, 1, 1, 2, 2, 2], [3, 3, 4, 4, 5, 5], [6, 7, 8, 8, 8, 8], [9, 10, 11, 11, 11, 11]]).reshape(
    -1)
print(node_segments.shape)

# [batch_size * 6, 3 * dim]
per_node_messages = tf.reshape(per_node_messages, [-1, per_node_messages.shape[-1]])
print(per_node_messages.shape)

# [batch_size, max_nodes, 3 * dim]
aggregated_per_node_info = tf.math.segment_sum(data=per_node_messages, segment_ids=node_segments)
aggregated_per_node_info = tf.reshape(aggregated_per_node_info, [-1, 3, per_node_messages.shape[-1]])

print('Aggregated per node info: \n', aggregated_per_node_info)
print(aggregated_per_node_info.shape)
