from utility.draw_graph_utils import retrieve_pooling_largest_sequences, compute_pooling_sequence_statistics
import numpy as np
from tqdm import tqdm

vocab = {0: 'ciao',
         1: 'come',
         2: 'stai',
         3: 'io',
         4: 'sto',
         5: 'diocan'}
X = np.array([0, 1, 2, 3, 4, 5,
              0, 5, 5, 5, 5, 5,
              1, 2, 1, 5, 5, 5]).reshape(3, -1)
Y = np.array([0, 1, 1]).reshape(3, -1)
P = np.array([0., 1., 1., 1., 0., 1.,
              0., 1., 1., 0., 0., 0.,
              1., 1., 0., 0., 1., 1.]).reshape(3, 1, 6)
A = np.array([1., 1., 0., 1., 0., 0.,
              1., 1., 1., 0., 0., 0.,
              0., 1., 1., 0., 0., 0.,
              1., 0., 0., 1., 1., 1.,
              0., 0., 0., 1., 1., 0.,
              0., 0., 0., 1., 0., 1.,

              1., 1., 0., 1., 0., 0.,
              1., 1., 1., 0., 0., 0.,
              0., 1., 1., 0., 0., 0.,
              1., 0., 0., 1., 1., 1.,
              0., 0., 0., 1., 1., 0.,
              0., 0., 0., 1., 0., 1.,

              1., 1., 0., 1., 0., 0.,
              1., 1., 1., 0., 0., 0.,
              0., 1., 1., 0., 0., 0.,
              1., 0., 0., 1., 1., 1.,
              0., 0., 0., 1., 1., 0.,
              0., 0., 0., 1., 0., 1.]).reshape(3, 6, 6)
S = np.array([1., 1., 1., 1., 1., 1.,
              0., 1., 1., 0., 0., 0.,
              0., 0., 1., 0., 0., 0.,
              0., 0., 0., 1., 1., 1.,
              0., 0., 0., 0., 1., 0.,
              0., 0., 0., 0., 0., 1.,

              1., 1., 1., 1., 1., 1.,
              0., 1., 1., 0., 0., 0.,
              0., 0., 1., 0., 0., 0.,
              0., 0., 0., 1., 1., 1.,
              0., 0., 0., 0., 1., 0.,
              0., 0., 0., 0., 0., 1.,

              1., 1., 1., 1., 1., 1.,
              0., 1., 1., 0., 0., 0.,
              0., 0., 1., 0., 0., 0.,
              0., 0., 0., 1., 1., 1.,
              0., 0., 0., 0., 1., 0.,
              0., 0., 0., 0., 0., 1.]).reshape(3, 6, 6)
node_mask = np.ones(18, dtype=np.int32).reshape(3, 6)

sequences = retrieve_pooling_largest_sequences(pooling_matrix=P,
                                               adjacency_matrix=A,
                                               node_spans=S,
                                               node_mask=node_mask,
                                               pooling_threshold=1.)

print(sequences)

token_sequences = {}
for sample, ids_seq in tqdm(enumerate(X)):
    for relative_id_seq in sequences[sample]:
        absolute_id_seq = ids_seq[relative_id_seq]
        conv_id_seq = [vocab[item] for item in absolute_id_seq]
        token_sequences.setdefault(sample, []).append(conv_id_seq)

compute_pooling_sequence_statistics(token_sequences, Y, 2, max_samples=3)