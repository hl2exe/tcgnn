import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

import numpy as np
import matplotlib.pyplot as plt


def _ptk_contiguous_sequence_constraint(pooling_matrix, adjacency_matrix):
    adjacency_matrix = tf.cast(adjacency_matrix, dtype=pooling_matrix.dtype)

    node_amount = adjacency_matrix.shape[-1]

    # [batch_size, K, max_nodes]
    pooling_matrix = tf.transpose(pooling_matrix, [0, 2, 1])

    # [batch_size, K, max_nodes, max_nodes]
    pair_intensities = tf.expand_dims(pooling_matrix, axis=-1) * tf.expand_dims(pooling_matrix, axis=2)

    # [batch_size, K, max_nodes, max_nodes]
    self_pairs_intensities = pair_intensities * tf.eye(adjacency_matrix.shape[-1], dtype=pair_intensities.dtype)[
                                                None, None, :, :]

    # [batch_size, K]
    pooled_mask = tf.where(self_pairs_intensities >= 1e-2, tf.ones_like(self_pairs_intensities),
                           tf.zeros_like(self_pairs_intensities))
    pooled_mask = tf.reduce_sum(pooled_mask, axis=-1)
    pooled_mask = tf.reduce_sum(pooled_mask, axis=-1)

    # [batch_size, K]
    mean_self_intensity = tf.reduce_sum(self_pairs_intensities, axis=-1)
    mean_self_intensity = tf.reduce_sum(mean_self_intensity, axis=-1)
    mean_self_intensity = tf.stop_gradient(mean_self_intensity / pooled_mask)

    pair_intensities = 0.5 * (pair_intensities - tf.reshape(tf.eye(node_amount, dtype=pair_intensities.dtype),
                                                            [1, 1, node_amount, node_amount]) * pair_intensities)
    pair_intensities *= tf.expand_dims(adjacency_matrix, axis=1)

    # [batch_size, K]
    numerator = tf.reduce_sum(pair_intensities, axis=-1)
    numerator = tf.reduce_sum(numerator, axis=-1)

    # [batch_size, K]
    denominator = tf.reduce_sum(self_pairs_intensities, axis=-1)
    denominator = tf.reduce_sum(denominator, axis=-1) - mean_self_intensity

    # [batch_size, K]
    per_cluster_contiguous_penalty = tf.nn.relu(1. - numerator / denominator)

    # [batch_size]
    numerator = tf.reduce_sum(numerator, axis=-1)
    denominator = tf.reduce_sum(denominator, axis=-1) + 1e-12

    # [batch_size]
    contiguous_penalty = tf.nn.relu(1. - numerator / denominator)

    return tf.nn.compute_average_loss(contiguous_penalty,
                                      global_batch_size=1), per_cluster_contiguous_penalty


def _compute_out_of_span_constraint(pooling_matrix, root_pooling, node_spans):
    # [batch_size, K, max_nodes, max_nodes]
    span_area = tf.expand_dims(root_pooling, axis=-1) * tf.expand_dims(node_spans, axis=1)

    # [batch_size, K, max_nodes]
    span_weights = tf.reduce_sum(span_area, axis=2)

    # [batch_size, K]
    numerator = pooling_matrix * tf.transpose(span_weights, [0, 2, 1])
    numerator = tf.reduce_sum(numerator, axis=1)

    # [batch_size, K]
    denominator = tf.reduce_sum(pooling_matrix, axis=1) + 1e-12

    # [batch_size, K]
    per_cluster_constraint = tf.nn.relu(1. - numerator / denominator)

    # [batch_size]
    numerator = tf.reduce_sum(numerator, axis=-1)
    denominator = tf.reduce_sum(denominator, axis=-1)

    # [batch_size]
    constraint = tf.nn.relu(1. - numerator / denominator)

    return tf.nn.compute_average_loss(constraint, global_batch_size=1), per_cluster_constraint


def _compute_out_of_span_constraint_alt(pooling_matrix, root_pooling, node_spans):
    node_amount = pooling_matrix.shape[1]

    # [batch_size, K, max_nodes, max_nodes]
    span_area = tf.expand_dims(root_pooling, axis=-1) * tf.expand_dims(node_spans, axis=1)

    # [batch_size, K, max_nodes]
    span_weights = tf.reduce_sum(span_area, axis=2)

    # [batch_size, K, max_nodes]
    pooling_matrix = tf.transpose(pooling_matrix, [0, 2, 1])

    # [batch_size, K, max_nodes, max_nodes]
    pair_intensities = tf.expand_dims(pooling_matrix, axis=-1) * tf.expand_dims(pooling_matrix, axis=2)

    # [batch_size, K, max_nodes, max_nodes]
    self_pairs_intensities = pair_intensities * tf.eye(node_amount, dtype=pair_intensities.dtype)[
                                                None, None, :, :]
    # [batch_size, K, max_nodes]
    self_pairs_intensities = tf.reduce_sum(self_pairs_intensities, axis=2)

    # [batch_size, K]
    numerator = self_pairs_intensities * span_weights
    numerator = tf.reduce_sum(numerator, axis=-1)

    # [batch_size, K]
    denominator = tf.reduce_sum(self_pairs_intensities, axis=-1) + 1e-12

    # [batch_size, K]
    per_cluster_constraint = tf.nn.relu(1. - numerator / denominator)

    # [batch_size]
    numerator = tf.reduce_sum(numerator, axis=-1)
    denominator = tf.reduce_sum(denominator, axis=-1) + 1e-12

    # [batch_size]
    constraint = tf.nn.relu(1. - numerator / denominator)

    return tf.nn.compute_average_loss(constraint, global_batch_size=1), per_cluster_constraint


def minimal_tree_loss_gaussian(pooling_matrix):
    # pooling_matrix -> [batch_size, max_nodes, K]

    meta_node_amount = pooling_matrix.shape[-1]

    # [batch_size, K, K]
    self_intensities = tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)
    normalized_self_intensities = self_intensities / (
            tf.norm(self_intensities, axis=(-1, -2))[:, None, None] + 1e-12)

    # [1, K, K]
    normalized_percentage = tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)
    normalized_percentage = normalized_percentage / tf.norm(normalized_percentage, axis=(-1, -2))
    normalized_percentage = tf.expand_dims(normalized_percentage, axis=0)

    per_cluster_penalty = normalized_self_intensities - normalized_percentage

    # [batch_size,]
    red_per_cluster_penalty = tf.norm(per_cluster_penalty, axis=(-1, -2))
    red_per_cluster_penalty = tf.reduce_mean(red_per_cluster_penalty)

    return red_per_cluster_penalty, tf.norm(per_cluster_penalty, axis=-1)


def _stk_contiguous_sequence_constraint(pooling_matrix, adjacency_matrix):
    node_amount = adjacency_matrix.shape[-1]
    meta_node_amount = pooling_matrix.shape[-1]

    adjacency_matrix = tf.cast(adjacency_matrix, dtype=pooling_matrix.dtype)

    # if node_mask is None:
    #     node_mask = tf.ones_like(adjacency_matrix)
    # else:
    #     node_mask = tf.expand_dims(node_mask, axis=-1)

    # avoid counting padding nodes
    # adjacency_matrix *= node_mask
    # adjacency_matrix *= tf.transpose(node_mask, [0, 2, 1])

    # [batch_size, max_nodes, max_nodes]
    triu_adjacency_matrix = tf.linalg.band_part(tf.ones((node_amount, node_amount), dtype=adjacency_matrix.dtype),
                                                0, -1) \
                            - tf.eye(node_amount, dtype=adjacency_matrix.dtype)
    adjacency_matrix = adjacency_matrix * triu_adjacency_matrix[None, :, :]

    # [batch_size, max_nodes, max_nodes]
    degree_matrix = tf.reduce_sum(adjacency_matrix, axis=-1)
    degree_matrix = degree_matrix[:, :, None] * tf.eye(degree_matrix.shape[-1], dtype=degree_matrix.dtype)[None, :,
                                                :]

    # [batch_size, K, K]
    numerator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), adjacency_matrix), pooling_matrix)
    denominator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), degree_matrix), pooling_matrix) + 1e-12

    # # [batch_size, K, max_nodes, max_nodes]
    # root_adjancency_matrix = root_pooling[:, :, :, None] * adjacency_matrix[:, None, :, :]
    #
    # # [batch_size, K, max_nodes, max_nodes]
    # root_degree_matrix = tf.reduce_sum(root_adjancency_matrix, axis=-1)
    # root_degree_matrix = root_degree_matrix[:, :, :, None] * tf.eye(root_degree_matrix.shape[-1],
    #                                                                 dtype=root_degree_matrix.dtype)[None, None, :, :]
    #
    # # [batch_size, K]
    # root_numerator = tf.transpose(pooling_matrix, [0, 2, 1])[:, :, :, None] * root_adjancency_matrix * tf.transpose(pooling_matrix, [0, 2, 1])[:,
    #                                                                                                    :, None, :]
    # root_numerator = tf.reduce_sum(root_numerator, axis=-1)
    # root_numerator = tf.reduce_sum(root_numerator, axis=-1)
    #
    # # [batch_size, K]
    # root_denominator = tf.transpose(pooling_matrix, [0, 2, 1])[:, :, :, None] * root_degree_matrix * tf.transpose(pooling_matrix, [0, 2, 1])[:,
    #                                                                                                  :, None, :]
    # root_denominator = tf.reduce_sum(root_denominator, axis=-1)
    # root_denominator = tf.reduce_sum(root_denominator, axis=-1)
    #
    # # [batch_size, K, K]
    # root_numerator = root_numerator[:, :, None] * tf.eye(meta_node_amount, dtype=root_numerator.dtype)[None, :, :]
    # root_denominator = root_denominator[:, :, None] * tf.eye(meta_node_amount, dtype=root_denominator.dtype)[None, :, :]
    #
    # # [batch_size, K, K]
    # numerator = numerator - root_numerator
    # denominator = denominator - root_denominator

    # [batch_size]
    contiguous_penalty = tf.nn.relu(1. - numerator / denominator)
    contiguous_penalty = tf.linalg.trace(contiguous_penalty)

    return tf.nn.compute_average_loss(contiguous_penalty, global_batch_size=1), contiguous_penalty


def numpy_stk_contiguous_sequence_constraint(P, A):
    A = np.triu(A)  # - np.eye(A.shape[-1])
    P = P.transpose(0, 2, 1)
    pairs = P[:, :, :, np.newaxis] * P[:, :, np.newaxis, :]
    pairs *= A[:, np.newaxis, :, :]

    # [batch_size, K]
    num = np.sum(pairs, axis=-1)
    num = np.sum(num, axis=-1)

    self_pairs = P[:, :, :, np.newaxis] * P[:, :, np.newaxis, :]

    D = np.sum(A, axis=-1)
    # D = np.maximum(D, 1.)
    D = D[:, :, np.newaxis] * np.eye(D.shape[-1])

    self_pairs = self_pairs * D[:, np.newaxis, :]

    # [batch_size, K]
    den = np.sum(self_pairs, axis=-1)
    den = np.sum(den, axis=-1)
    den += 1e-12

    # # [batch_size, K, N, N]
    # root_A = A[:, np.newaxis, :, :] * T[:, :, :, np.newaxis]
    # root_D = np.sum(root_A, axis=-1)[:, :, :, np.newaxis] * np.eye(A.shape[-1])[np.newaxis, np.newaxis, :, :]
    #
    # # [batch_size, K]
    # root_num = P[:, :, :, np.newaxis] * root_A * P[:, :, np.newaxis, :]
    # root_num = np.sum(root_num, axis=-1)
    # root_num = np.sum(root_num, axis=-1)
    #
    # # [batch_size, K]
    # root_den = P[:, :, :, np.newaxis] * root_D * P[:, :, np.newaxis, :]
    # root_den = np.sum(root_den, axis=-1)
    # root_den = np.sum(root_den, axis=-1)
    #
    # # [batch_size, K]
    # num = num - root_num
    # den = den - root_den

    penalty = np.maximum(1. - num / den, 0.)
    penalty = np.sum(penalty, axis=-1)

    return np.mean(penalty), penalty


def numpy_cut_loss_contiguous_sequence_constraint(P, A):
    P = P.transpose(0, 2, 1)
    pairs = P[:, :, :, np.newaxis] * P[:, :, np.newaxis, :]
    pairs *= A[:, np.newaxis, :, :]

    num = np.sum(pairs, axis=-1)
    num = np.sum(num, axis=-1)

    self_pairs = P[:, :, :, np.newaxis] * P[:, :, np.newaxis, :]

    D = np.sum(A, axis=-1)
    # D = np.maximum(D, 1.)
    D = D[:, :, np.newaxis] * np.eye(D.shape[-1])

    self_pairs = self_pairs * D[:, np.newaxis, :]

    den = np.sum(self_pairs, axis=-1)
    den = np.sum(den, axis=-1)
    den += 1e-12

    penalty = np.maximum(1. - num / den, 0.)

    return np.mean(penalty), penalty


def get_root_distribution(P, A, S):
    # 1) Compute pooled area
    P = P.transpose(0, 2, 1)

    S = S - np.eye(S.shape[-1]) * S

    # [batch_size, K, N, N]
    pooled_area = P[:, :, :, np.newaxis] * S[:, np.newaxis, :, :]
    pooled_area = pooled_area * P[:, :, np.newaxis, :]

    # [batch_size, K, N]
    pooled_area = np.sum(pooled_area, axis=-1)
    pooled_area = pooled_area / (np.linalg.norm(pooled_area, axis=-1)[:, :, None] + 1e-12)

    # 2) Parent node
    adjacency_tril = np.tril(A) - np.eye(A.shape[-1]) * A
    adjacency_tril[:, 0, 0] = 1.

    # [batch_size, K, N]
    parent_intensity = P[:, :, :, np.newaxis] * adjacency_tril[:, np.newaxis, :, :]
    parent_intensity = np.sum(parent_intensity, axis=-1)
    parent_intensity = 1. - parent_intensity
    parent_intensity = -np.log(parent_intensity + 1e-12)
    parent_intensity = parent_intensity / (np.linalg.norm(parent_intensity, axis=-1)[:, :, None] + 1e-12)

    # 3) connectivity
    adjacency_triu = np.triu(A) - np.eye(A.shape[-1]) * A
    leaf_mask = np.sum(adjacency_triu, axis=-1)

    # [batch_size, K, N]
    children_intensity = P[:, :, :, np.newaxis] * adjacency_triu[:, np.newaxis, :, :]
    children_intensity = np.sum(children_intensity, axis=-1)
    children_intensity = np.minimum(children_intensity, P)
    children_intensity = children_intensity / (np.linalg.norm(children_intensity, axis=-1)[:, :, None] + 1e-12)

    # Compute root distribution
    raw_root_pooling = pooled_area + parent_intensity + children_intensity
    raw_root_pooling *= leaf_mask[:, None, :]
    root_pooling = raw_root_pooling / np.sum(raw_root_pooling, axis=-1)[:, :, None]

    return root_pooling


P = np.array([0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
              0.5, 0.5, 0.5, 0.5, 0.5, 0.5]).reshape(1, 2, 6).transpose(0, 2, 1)
A = np.array([1, 1, 0, 0, 0, 0,
              1, 1, 1, 0, 0, 1,
              0, 1, 1, 1, 0, 0,
              0, 0, 1, 1, 1, 0,
              0, 0, 0, 1, 1, 0,
              0, 1, 0, 0, 0, 1]).reshape(1, 6, 6)
S = np.array([1, 1, 1, 1, 1, 1.,
              0, 1, 1, 1, 1, 1,
              0, 0, 1, 1, 1, 0,
              0, 0, 0, 1, 1, 0,
              0, 0, 0, 0, 1, 0,
              0, 0, 0, 0, 0, 1]).reshape(1, 6, 6)

# T = get_root_distribution(P, A, S)
T = np.array([1., 0., 0., 0., 0., 0.,
              1., 0., 0., 0., 0., 0]).reshape(1, 2, 6)

cont, per_cluster_cont = _ptk_contiguous_sequence_constraint(P, A)
print(cont.numpy())
print(per_cluster_cont.numpy())

print('*' * 50)

span, per_cluster_span = _compute_out_of_span_constraint(P, T, S)
print(span.numpy())
print(per_cluster_span.numpy())
print(np.mean(per_cluster_span.numpy()))

print('*' * 50)

span_alt, per_cluster_span_alt = _compute_out_of_span_constraint_alt(P, T, S)
print(span_alt.numpy())
print(per_cluster_span_alt.numpy())
print(np.mean(per_cluster_span_alt.numpy()))

iterations = 10
constraints = []

for it in range(iterations):
    P[:, :, 0] += 0.5 / iterations
    P[:, :, 1] -= 0.5 / iterations
    constraints.append([_ptk_contiguous_sequence_constraint(P, A)[0].numpy(),
                        _compute_out_of_span_constraint(P, T, S)[0].numpy(),
                        minimal_tree_loss_gaussian(P)[0].numpy()
                        ])

print(P)
constraints = np.array(constraints).reshape(-1, 3).transpose()

for idx in range(constraints.shape[0]):
    plt.plot(constraints[idx, :])

plt.legend(['contiguous', 'out of span', 'minimal tree'])
plt.show()
