import numpy as np


def matrix_hamming(x):
    # x -> N x K

    x = x.transpose()

    K = x.shape[0]
    N = x.shape[1]

    a = np.matmul(x, np.transpose(1 - x))
    b = np.matmul(1 - x, x.transpose())
    a = a - np.tril(a)
    b = b - np.tril(b)

    return np.sum((a + b) / N)


def naive_hamming(x):
    # x -> N x K

    K = x.shape[1]

    total = 0
    for i in range(K):
        for j in range(i + 1, K):
            total += np.mean(x[:, i] * (1 - x[:, j]) + (1 - x[:, i]) * x[:, j])

    return total


def generate_random_binary_matrices(N, K, size):

    for i in range(size):
        yield np.random.randint(low=0, high=2, size=N * K).reshape(N, K)


N = 5
K = 3
size = 100

for item in generate_random_binary_matrices(N=N, K=K, size=size):
    assert np.allclose(matrix_hamming(item), naive_hamming(item))
    print(matrix_hamming(item))
