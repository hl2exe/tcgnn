"""

Get all subtrees

"""

import numpy as np
from itertools import combinations
from utility.graph_utils import retrieve_dep_tree_info_adj, get_subtree_info, get_dep_nodes_depths


def build_assignment_matrix(subtrees, nodes):
    assignment_matrix = np.zeros((len(nodes), len(subtrees)), dtype=np.float32)

    for idx, subtree in enumerate(subtrees):
        for node_idx in subtree:
            assignment_matrix[node_idx, idx] = 1.0

    return assignment_matrix


dep_tree = "[('ROOT', 0, 'ROOT', 2, 'is'), ('expl', 2, 'is', 1, 'There'), ('advmod', 2, 'is', 3, 'also')," \
           " ('det', 6, 'trend', 4, 'a'), ('amod', 6, 'trend', 5, 'growing'), ('nsubj', 2, 'is', 6, 'trend')," \
           " ('case', 8, 'parents', 7, 'of'), ('nmod', 6, 'trend', 8, 'parents'), ('acl', 8, 'parents', 9, 'failing')," \
           " ('mark', 11, 'follow', 10, 'to'), ('xcomp', 9, 'failing', 11, 'follow')," \
           " ('amod', 14, 'schedules', 12, 'full'), ('compound', 14, 'schedules', 13, 'vaccine')," \
           " ('dobj', 11, 'follow', 14, 'schedules'), ('cc', 14, 'schedules', 15, 'and')," \
           " ('conj', 14, 'schedules', 16, 'schools'), ('cc', 14, 'schedules', 17, 'and')," \
           " ('compound', 19, 'centers', 18, 'daycare'), ('conj', 14, 'schedules', 19, 'centers')," \
           " ('acl', 19, 'centers', 20, 'failing'), ('mark', 22, 'track', 21, 'to')," \
           " ('xcomp', 20, 'failing', 22, 'track'), ('det', 24, 'families', 23, 'those')," \
           " ('dobj', 22, 'track', 24, 'families'), ('nsubj', 27, 'pledged', 25, 'that')," \
           " ('aux', 27, 'pledged', 26, 'have'), ('acl:relcl', 24, 'families', 27, 'pledged')," \
           " ('mark', 29, 'get', 28, 'to'), ('xcomp', 27, 'pledged', 29, 'get')," \
           " ('det', 32, 'vaccines', 30, 'the'), ('amod', 32, 'vaccines', 31, 'required')," \
           " ('dobj', 29, 'get', 32, 'vaccines'), ('mark', 36, 'begins', 33, 'after')," \
           " ('det', 35, 'year', 34, 'the'), ('nsubj', 36, 'begins', 35, 'year')," \
           " ('advcl', 29, 'get', 36, 'begins'), ('punct', 2, 'is', 37, '.')]"

is_directional = True
kernel = 'stk'

nodes, edge_features, adjacency_matrix = retrieve_dep_tree_info_adj(dep_tree, is_directional=True)
nodes = np.array(nodes)

node_idxs = np.arange(len(nodes))
node_depths = get_dep_nodes_depths(dep_tree)

subtree_nodes, subtree_edge_indices, subtree_edge_features, \
subtree_node_indices, subtree_node_segments, \
subtree_directionality_mask = get_subtree_info(node_idxs=node_idxs,
                                               depths=node_depths,
                                               is_directional=is_directional,
                                               kernel=kernel)
print()
print('Sub Trees (ST)')
print()

print('Subtrees: ', subtree_nodes)
print()
print('Edge indices: ', subtree_edge_indices)
print()
print('Edge features: ', subtree_edge_features)
print()
print('Node indices: ', subtree_node_indices)
print()
print('Node segments: ', subtree_node_segments)
print()
print('Directionality mask: ', subtree_directionality_mask)
print()
print('Assignment matrix: ', adjacency_matrix)
print()

for seq in subtree_nodes:
    print(nodes[seq])

print()
print('Sub Trees (ST) Connections!')
print()

for pair in subtree_edge_indices:
    print('{0} ---- {1}'.format(nodes[subtree_nodes[pair[0]]], nodes[subtree_nodes[pair[1]]]))

print()
