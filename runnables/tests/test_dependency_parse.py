

"""

Builds dependency trees with CoreNLP for ACL 18 (IBM) dataset

"""

from utility.graph_utils import retrieve_dep_tree_info
import ast
from nltk.parse.corenlp import CoreNLPDependencyParser
from nltk.draw import draw_trees


text = "In an ambient intelligence world, devices work in concert to support people in carrying out their everyday" \
       " life activities, tasks and rituals in easy, natural way using information and intelligence that is hidden" \
       " in the network connecting these devices (see Internet of Things)."
tree = "[('ROOT', 0, 'ROOT', 8, 'work'), ('case', 5, 'world', 1, 'In'), ('det', 5, 'world', 2, 'an')," \
       " ('amod', 5, 'world', 3, 'ambient'), ('compound', 5, 'world', 4, 'intelligence')," \
       " ('nmod', 8, 'work', 5, 'world'), ('punct', 8, 'work', 6, ','), ('nsubj', 8, 'work', 7, 'devices')," \
       " ('case', 10, 'concert', 9, 'in'), ('nmod', 8, 'work', 10, 'concert'), ('mark', 12, 'support', 11, 'to')," \
       " ('xcomp', 8, 'work', 12, 'support'), ('dobj', 12, 'support', 13, 'people')," \
       " ('mark', 15, 'carrying', 14, 'in')," \
       " ('advcl', 12, 'support', 15, 'carrying'), ('compound:prt', 15, 'carrying', 16, 'out')," \
       " ('nmod:poss', 20, 'activities', 17, 'their'), ('amod', 20, 'activities', 18, 'everyday')," \
       " ('compound', 20, 'activities', 19, 'life'), ('dobj', 15, 'carrying', 20, 'activities')," \
       " ('punct', 8, 'work', 21, ','), ('dobj', 8, 'work', 22, 'tasks'), ('cc', 22, 'tasks', 23, 'and')," \
       " ('conj', 22, 'tasks', 24, 'rituals'), ('case', 29, 'way', 25, 'in'), ('amod', 29, 'way', 26, 'easy')," \
       " ('punct', 29, 'way', 27, ','), ('amod', 29, 'way', 28, 'natural'), ('nmod', 22, 'tasks', 29, 'way')," \
       " ('acl', 22, 'tasks', 30, 'using'), ('dobj', 30, 'using', 31, 'information')," \
       " ('cc', 31, 'information', 32, 'and'), ('conj', 31, 'information', 33, 'intelligence')," \
       " ('nsubjpass', 36, 'hidden', 34, 'that'), ('auxpass', 36, 'hidden', 35, 'is')," \
       " ('acl:relcl', 31, 'information', 36, 'hidden'), ('case', 39, 'network', 37, 'in')," \
       " ('det', 39, 'network', 38, 'the'), ('nmod', 36, 'hidden', 39, 'network')," \
       " ('acl', 39, 'network', 40, 'connecting'), ('det', 42, 'devices', 41, 'these')," \
       " ('dobj', 40, 'connecting', 42, 'devices'), ('punct', 44, 'see', 43, '-LRB-')," \
       " ('dep', 22, 'tasks', 44, 'see'), ('dobj', 44, 'see', 45, 'Internet')," \
       " ('case', 47, 'Things', 46, 'of'), ('nmod', 45, 'Internet', 47, 'Things')," \
       " ('punct', 44, 'see', 48, '-RRB-'), ('punct', 8, 'work', 49, '.')]"


is_directional = True
nodes, features, edge_indices, node_indices,\
node_segments, directionality_mask, leaf_indicator = retrieve_dep_tree_info(tree, is_directional=is_directional)

print(ast.literal_eval(tree))
print()

print(nodes)
print()
print(features)
print()
print(edge_indices)
print()
print()
print(node_indices)
print()
print(node_segments)
print()
print(directionality_mask)
print()

print(len(nodes))
print(len(edge_indices))
print(len(features))
print(len(node_indices))
print(len(node_segments))
print(len(directionality_mask))

dep_parser = CoreNLPDependencyParser(url='http://localhost:9000')
corenlp_tree = list(dep_parser.parse(text.split()))[0].tree()

draw_trees(corenlp_tree)

# nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask = retrieve_dep_tree_info(tree)
#
# print(text)
# print()
# print(tree)
# print()
# print(nodes)
# print()
# print(edge_indices)
# print()
# print(edge_features)
# print()
# print(node_indices)
# print()
# print(node_segments)
# print()
# print(directionality_mask)
# print()
#