from utility.graph_utils import retrieve_trees_info, pad_tree_data
import tensorflow as tf
from utility.embedding_utils import convert_number_to_binary_list

tree1 = '(ROOT (S (NP (PRP We)) (VP (VBD allowed) (NP (NP (NP (DT the) (NN cross)) (NP (NNS immunities))) (, ,)' \
            ' (NP (NP (NN duration)) (PP (IN of) (NP (NN immunity)))) (, ,) (NP (NP (NN degree)) (PP (IN of) (NP (NP' \
            ' (JJ seasonal) (NN variation)) (PP (IN in) (NP (NN R0)))))) (, ,) (CC and) (NP (NP (NN establishment)' \
            ' (NN time)) (PP (IN of) (NP (NN SARS-CoV-2))))) (S (VP (TO to) (VP (VB vary))))) (. .)))'

# tree2 = '(ROOT (S (NP (DT The) (NN Keystone) (NNP XL) (NN pipeline)) (VP (MD would) (VP (VB allow) (S (NP (DT the)' \
#         ' (NNP U.S.)) (VP (TO to) (VP (VP (VB increase) (NP (PRP$ its) (NN energy) (NN security))) (CC and)' \
#         ' (VP (VB reduce) (NP (PRP$ its) (NN dependence)) (PP (IN on) (NP (JJ foreign) (NN oil)))))))))))'
#
# tree3 = '(ROOT (S (NP (DT the) (NN bill)) (VP (VP (VBD was) (VP (VBN dismissed) (PP (IN by) (NP (DT the) (NP (PRP us))' \
#         ' (NN senate))) (PP (IN by) (NP (NP (DT a) (NN vote)) (PP (IN of) (NP (CD 5148))))) (PP (IN along) (NP (ADJP' \
#         ' (RB largely) (JJ partisan)) (NNS lines))))) (CC and) (VP (VBZ is) (VP (VBN viewed) (PP (IN as) (NP (DT a)' \
#         ' (NN victory))) (PP (IN for) (NP (NN president) (NN barack)) (NP (JJ obama) (NNS s) (NN health) (NN care)' \
#         ' (NN law) (NN lsb) (NN ref) (NN rsb))))))))'

(nodes, edge_indices, edge_features, node_indices, node_segments), _ = retrieve_trees_info([tree1])

edge_features = [list(map(convert_number_to_binary_list, features)) for features in edge_features]
lengths = [len(graph) for graph in nodes]
print(lengths)
max_graph_nodes = max(lengths)
print('Max nodes: ', max_graph_nodes)

tokenizer = tf.keras.preprocessing.text.Tokenizer(filters='')
to_fit = set([node for graph in nodes for node in graph])
tokenizer.fit_on_texts(to_fit)
nodes = tokenizer.texts_to_sequences(nodes)

nodes, edge_indices, edge_features, \
node_indices, node_segments, \
edge_mask, truncate_mask = pad_tree_data(nodes=nodes,
                                         edge_indices=edge_indices,
                                         edge_features=edge_features,
                                         node_indices=node_indices,
                                         node_segments=node_segments,
                                         padding_length=len(nodes[0]),
                                         padding='post',
                                         )

print('Nodes: ', nodes.shape)
print('Edge indices: ', edge_indices.shape)
print('Edge features: ', edge_features.shape)
print('Node indices: ', node_indices.shape)
print('Node segments: ', node_segments.shape)
print('Edge mask: ', edge_mask.shape)
print('Truncate mask: ', truncate_mask)

print('Node segments: ', node_segments)
