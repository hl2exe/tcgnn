"""

Testing agglomerative clustering with some fake word embeddings

In order to compute centroids

"""

import numpy as np
from sklearn.cluster import AgglomerativeClustering

n_clusters = 10
clustering = AgglomerativeClustering(linkage='complete', n_clusters=n_clusters, affinity='cosine')

x = np.random.rand(64, 128)

clustering.fit(X=x)

for cluster_id in np.arange(n_clusters):
    centroid = np.mean(x[clustering.labels_ == cluster_id], axis=0)
    print(centroid.shape)