"""

Debugging training routine in order to find out potential memory leaks

1. Load dataset
2. Convert dataset
3. Build model
4. Train

"""

from nn_models_v2 import Single_Adj_GNN
import os
from data_loader import UKPLooLoader
from data_processor import TextTreeProcessor
from data_converter import GraphAdjConverter
from tokenization import KerasTreeTokenizer
import numpy as np
from utility.pipeline_utils import get_dataset_fn
from utility.python_utils import merge

if __name__ == '__main__':

    excluded_key = 'abortion'
    batch_size = 256
    prefetch_amount = 15
    reshuffle_each_iteration = True
    shuffle_amount = 10000
    network_retrieved_args = {
        'optimizer_args': {"learning_rate": 1e-3},
        'timesteps': 1,
        'node_mlp_weights': [64],
        'message_weights': [64],
        'pooling_mlp_weights': [64, 1],
        'aggregation_mlp_weights': [64],
        'representation_mlp_weights': [64],
        "embedding_dimension": 50,
        "l2_regularization": 0.
    }

    loader = UKPLooLoader()
    data_handle = loader.load()

    processor = TextTreeProcessor(loader_info=data_handle.get_additional_info())

    tokenizer = KerasTreeTokenizer(tokenizer_args={'filters': '', 'oov_token': 1})

    converter = GraphAdjConverter(max_graph_nodes_limit=140, feature_class='text_graph_adj_features')

    train_df, val_df, test_df = data_handle.get_split(key='topic', key_values=excluded_key, validation_percentage=None)

    train_filepath = os.path.join('train_data_loo_{}'.format(excluded_key))
    val_filepath = os.path.join('val_data_loo_{}'.format(excluded_key))
    test_filepath = os.path.join('test_data_loo_{}'.format(excluded_key))

    if not os.path.isfile(test_filepath):
        train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
        val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
        test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

        train_texts = train_data.get_data()
        tokenizer.build_vocab(data=train_texts, filepath='.', prefix=None)
        tokenizer.save_info(filepath='.', prefix=None)
        tokenizer_info = tokenizer.get_info()

        converter.convert_data(examples=train_data,
                               label_list=processor.get_labels(),
                               output_file=train_filepath,
                               tokenizer=tokenizer,
                               checkpoint=None,
                               is_training=True)
        converter.save_conversion_args(filepath='.', prefix=None)
        converter_info = converter.get_conversion_args()

        converter.convert_data(examples=val_data,
                               label_list=processor.get_labels(),
                               output_file=val_filepath,
                               tokenizer=tokenizer,
                               checkpoint=None)

        converter.convert_data(examples=test_data,
                               label_list=processor.get_labels(),
                               output_file=test_filepath,
                               tokenizer=tokenizer,
                               checkpoint=None)
    else:
        tokenizer_info = KerasTreeTokenizer.load_info(filepath='.', prefix=None)
        converter_info = GraphAdjConverter.load_conversion_args(filepath='.', prefix=None)

    # Create Datasets

    train_data = get_dataset_fn(filepath=train_filepath,
                                batch_size=batch_size,
                                name_to_features=converter.feature_class.get_mappings(converter_info),
                                selector=converter.feature_class.get_dataset_selector(),
                                is_training=True,
                                shuffle_amount=shuffle_amount,
                                reshuffle_each_iteration=reshuffle_each_iteration,
                                prefetch_amount=prefetch_amount)

    fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                      batch_size=batch_size,
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=prefetch_amount)

    val_data = get_dataset_fn(filepath=val_filepath,
                              batch_size=batch_size,
                              name_to_features=converter.feature_class.get_mappings(converter_info),
                              selector=converter.feature_class.get_dataset_selector(),
                              is_training=False,
                              prefetch_amount=prefetch_amount)

    test_data = get_dataset_fn(filepath=test_filepath,
                               batch_size=batch_size,
                               name_to_features=converter.feature_class.get_mappings(converter_info),
                               selector=converter.feature_class.get_dataset_selector(),
                               is_training=False,
                               prefetch_amount=prefetch_amount)


    network_retrieved_args['additional_data'] = data_handle.get_additional_info()
    network_retrieved_args['name'] = 'test_network'
    network = Single_Adj_GNN(**network_retrieved_args)

    # Useful stuff
    train_steps = int(np.ceil(train_df.shape[0] / batch_size))
    eval_steps = int(np.ceil(val_df.shape[0] / batch_size))
    test_steps = int(np.ceil(test_df.shape[0] / batch_size))

    np_train_y = np.concatenate([item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)])
    np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
    np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

    print('Total train steps: {}'.format(train_steps))
    print('Total eval steps: {}'.format(eval_steps))
    print('Total test steps: {}'.format(test_steps))

    # computing positive label weights (for unbalanced dataset)
    network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                   mode='multi-label' if network.is_multilabel else 'multi-class')

    text_info = merge(tokenizer_info, converter_info)
    network.build_model(text_info=text_info)

    # Training
    network.fit(train_data=train_data,
                epochs=10000, verbose=False,
                callbacks=[],
                validation_data=val_data,
                step_checkpoint=None,
                metrics=None,
                additional_metrics_info=None,
                metrics_nicknames=None,
                train_num_batches=train_steps,
                eval_num_batches=eval_steps,
                np_val_y=np_val_y)