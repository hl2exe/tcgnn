from utility.graph_utils import retrieve_hierarchical_tree_info
from nltk.tree import Tree
import numpy as np

tree_text = "(ROOT (S (NP (DT A) (CD 2001) (NN study)) (VP (VBD found) (SBAR (SBAR (IN that) (S (NP (NP (NN exposure))" \
            " (PP (TO to) (NP (JJ violent) (NN video) (NNS games)))) (VP (VBZ causes) (NP (NP (ADVP (IN at) (JJS least))" \
            " (DT a) (JJ temporary) (NN increase)) (PP (IN in) (NP (NN aggression))))))) (CC and) (SBAR (IN that)" \
            " (S (NP (DT this) (NN exposure)) (VP (VBZ correlates) (PP (IN with) (NP (NP (NN aggression))" \
            " (PP (IN in) (NP (DT the) (JJ real) (NN world)))))))))) (. .)))"

nodes, edge_indices, edge_features, node_indices, node_segments, directionality_mask, \
subtree_nodes, subtree_edge_indices, subtree_edge_features, subtree_node_indices, \
subtree_node_segments, subtree_directionality_mask = retrieve_hierarchical_tree_info(tree_text=tree_text)

print('Subtrees ({}): \n'.format(len(subtree_nodes)), subtree_nodes)
print()

nodes = np.array(nodes)

for seq in subtree_nodes:
    print(nodes[seq])

print()

tree_conv = Tree.fromstring(tree_text)
nltk_subtrees = list(tree_conv.subtrees())

print('Nltk subtrees ({}): \n'.format(len(nltk_subtrees)))
for t in nltk_subtrees:
    print(t.__repr__())
