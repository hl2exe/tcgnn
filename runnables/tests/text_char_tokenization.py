
import tensorflow as tf
from nltk import word_tokenize


word_tokenizer = tf.keras.preprocessing.text.Tokenizer()
char_tokenizer = tf.keras.preprocessing.text.Tokenizer()

texts = ["hey how do you do?", "i am fine thank you"]

word_tokenizer.fit_on_texts(texts)
char_tokenizer.fit_on_texts([c for seq in texts for c in seq])

print('Word index: ', word_tokenizer.word_index)
print('Char index: ', char_tokenizer.word_index)

word_ids = word_tokenizer.texts_to_sequences(texts)
char_ids = char_tokenizer.texts_to_sequences([[word for word in word_tokenize(seq)] for seq in texts])

print('Word ids: ', word_ids)
print('Char ids: ', char_ids)