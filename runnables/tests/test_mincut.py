"""

Testing some variants of mincut

"""

import numpy as np
from utility.draw_graph_utils import draw_tree
import matplotlib.pyplot as plt


def cut_loss(P, T, A, S):
    # K x K
    A_pool = np.matmul(T, np.matmul(S, P))

    # K x 1
    D_pool = np.sum(P, axis=0)

    return - np.trace(A_pool / (D_pool[:, np.newaxis] + 1e-12)) / P.shape[-1]


def ortho_loss(P, T, S):
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    span_inter = span_weights[:, np.newaxis, :] * span_weights[np.newaxis, :, :]

    P = P.transpose()
    pooled_inter = P[:, np.newaxis, :]

    inter_intensity = np.sum(span_inter * pooled_inter, axis=-1)
    inter_intensity = inter_intensity - np.eye(T.shape[0]) * inter_intensity

    mask = np.triu(inter_intensity)

    inter_intensity *= mask

    return np.linalg.norm(inter_intensity, ord='fro')


def separation_loss(P, T, S, A, alpha=.5):

    # K x N
    span_area = T[:, :, np.newaxis] * S[np.newaxis, :, :]
    span_weights = np.sum(span_area, axis=1)

    # K x N
    P = P.transpose()

    # Step 1: Kernel specific weight (PTK here)
    A = A - np.tril(A)
    W = A[np.newaxis, :, :] * P[:, :, np.newaxis]
    W = 1 - np.max(W, axis=-1)

    leaf_mask = np.minimum(np.sum(S, axis=-1) - 1., 1.)

    W *= leaf_mask[np.newaxis, :]
    W += (1 - leaf_mask[np.newaxis, :])

    # K x N
    W *= span_weights

    # K x K x N
    W = np.minimum(W[np.newaxis, :, :] + W[:, np.newaxis, :], alpha) / alpha
    W = W - np.eye(W.shape[0])[:, :, np.newaxis] * W

    # weight by pooling (only areas of pooling are valid)
    W *= P[:, np.newaxis, :]

    # Step 2: Similarity

    # Hamming distance
    # K x K x N
    scaled_P = np.minimum(P, alpha) / alpha
    dissimilarity = scaled_P[:, np.newaxis, :] * (1. - scaled_P[np.newaxis, :, :]) \
                    + (1. - scaled_P[:, np.newaxis, :]) * scaled_P[np.newaxis, :, :]

    dissimilarity *= W

    # K x K
    dissimilarity = np.max(dissimilarity, axis=-1)

    penalty = np.maximum(1. - dissimilarity, 0.)
    penalty = penalty - np.tril(penalty)

    penalty = np.sum(penalty)

    return penalty


def slim_parametric_contiguous_constraint(P, T, S, A, alpha=0.5):
    P = P.transpose()

    # K x N x N
    # pair_intensities = P[:, :, np.newaxis] * P[:, np.newaxis, :]
    scaled_P = np.minimum(P, alpha) / alpha
    pair_intensities = np.maximum(scaled_P[:, :, np.newaxis] + scaled_P[:, np.newaxis, :] - 1., 0.)

    # remove self-reference and symmetry
    pair_intensities = pair_intensities - np.tril(pair_intensities)

    # K x N x N
    all_intensities = pair_intensities * A[np.newaxis, :, :]

    # K x N
    all_intensities = np.max(all_intensities, axis=-1)
    all_intensities = 1 / alpha * np.maximum(alpha - all_intensities, 0.)
    # all_intensities = 1 / alpha * np.maximum(all_intensities - alpha, 0.)

    # filter out leaves and check node span
    # 1 x N
    leaf_mask = np.minimum(np.sum(S, axis=-1) - 1., 1.)

    all_intensities *= leaf_mask[np.newaxis, :]

    # root span aware
    # K x N x N
    all_intensities = all_intensities[:, np.newaxis, :] * S[np.newaxis, :, :]

    # K x N x N
    asyn_adjacency_matrix = A - np.tril(A)
    j_weight = np.max(P[:, np.newaxis, :] * asyn_adjacency_matrix[np.newaxis, :, :],
                      axis=-1)
    # i_weight = node_pooling[:, np.newaxis, :]
    t_weight = T[:, np.newaxis, :]
    # penalty_weight = np.maximum(j_weight[:, np.newaxis, :], i_weight)
    # penalty_weight = np.maximum(penalty_weight, t_weight)
    penalty_weight = np.maximum(j_weight[:, np.newaxis, :], t_weight)

    # K x N
    constraint = np.sum(all_intensities * penalty_weight, axis=-1)

    # K
    constraint = np.sum(constraint * T, axis=-1)

    return np.mean(constraint)


A = np.array([1, 1, 1, 0,
              1, 1, 0, 0,
              1, 0, 1, 1,
              0, 0, 1, 1]).reshape(4, 4).astype(np.float)
S = np.array([1, 1, 1, 1,
              0, 1, 0, 0,
              0, 0, 1, 1,
              0, 0, 0, 1]).reshape(4, 4).astype(np.float)
P = np.array([0, 0,
              1, 0,
              0, 1,
              0, 1]).reshape(4, 2)
T = np.array([1, 0, 0, 0,
              0, 0, 1, 0]).reshape(2, 4)

c_loss = cut_loss(P, T, A, S)
cont_loss = slim_parametric_contiguous_constraint(P, T, S, A, alpha=0.5)
sep_loss = separation_loss(P, T, S, A, alpha=0.5)
print('cut loss -> ', c_loss)
print('contiguous loss -> ', cont_loss)
print('separation loss (PTK) -> ', sep_loss)

for k in range(P.shape[-1]):
    draw_tree(nodes=P[:, k], adjacency_matrix=A, title_name='Node pooling {}'.format(k))
    draw_tree(nodes=T[k, :], adjacency_matrix=A, title_name='Root pooling {}'.format(k))

plt.show()
