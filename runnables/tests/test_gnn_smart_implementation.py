
"""

Testing GNN implementation with adjacency matrix and fully connected trick

Example tree:

    0
   / \
  1  2
    / \
   3  4

"""

import numpy as np

node_idxs = np.arange(5)
adj_matrix = np.array([
    1, 1, 1, 0, 0,
    1, 1, 0, 0, 0,
    1, 0, 1, 1, 1,
    0, 0, 1, 1, 0,
    0, 0, 1, 0, 1
]).reshape(5, 5)

# Run-time computation

# [nodes * nodes, 2]
edge_indices_A = np.repeat(node_idxs, len(node_idxs)).reshape(-1, len(node_idxs), 1)
edge_indices_B = np.repeat(node_idxs[:, np.newaxis], len(node_idxs), axis=1).transpose().reshape(-1, len(node_idxs), 1)
edge_indices = np.concatenate((edge_indices_A, edge_indices_B), axis=2)

# [nodes * nodes]
node_indices = np.arange(len(node_idxs) * len(node_idxs))

# [nodes * nodes]
node_segments = np.repeat(np.arange(len(node_idxs)), len(node_idxs)).reshape(-1)


print(node_idxs.shape)
print(adj_matrix.shape)
print(edge_indices.shape)
print(node_indices.shape)
print(node_segments.shape)

print()
print()

print(node_idxs)
print(adj_matrix)
print(edge_indices)
print(node_indices)
print(node_segments)