"""

Parses a nltk.tree.Tree in order to build GNN input

"""

import nltk
from nltk.draw import draw_trees
from utility.graph_utils import retrieve_tree_info_adj, get_nodes_depths
from utility.draw_graph_utils import draw_nltk_tree


def depth_node_search(nodes, parent, depth, edges, node_indexes, edge_indices,
                      node_indices, node_segments, directional_mask, is_directional=True):
    forward_value = 1
    backward_value = 1

    if is_directional:
        backward_value = -1

    parent_value = parent.label()
    for node in parent:
        # If not leaf
        if type(node) is nltk.Tree:
            node_value = node.label()
            nodes.append(node_value)

            edges.append(depth + 1)
            edge_idx = len(edges) - 1

            parent_idx = node_indexes[parent_value]
            current_idx = max(list(node_indexes.values())) + 1
            edge_indices.append([parent_idx, current_idx])
            node_indexes[node_value] = current_idx

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(edge_idx)
                node_segments[parent_idx].append(parent_idx)
                directional_mask[parent_idx].append(backward_value)
            else:
                node_indices.append([edge_idx])
                node_segments.append([parent_idx])
                directional_mask.append([backward_value])
            node_indices.append([edge_idx])
            node_segments.append([current_idx])
            directional_mask.append([forward_value])

            depth_node_search(nodes=nodes, parent=node, depth=depth + 1,
                              edges=edges, node_indexes=node_indexes,
                              edge_indices=edge_indices, node_indices=node_indices,
                              node_segments=node_segments, directional_mask=directional_mask,
                              is_directional=is_directional)
        else:
            node_value = node
            nodes.append(node_value)

            edges.append(depth + 1)
            edge_idx = len(edges) - 1

            parent_idx = node_indexes[parent_value]
            current_idx = max(list(node_indexes.values())) + 1
            edge_indices.append([parent_idx, current_idx])
            node_indexes[node_value] = current_idx

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(edge_idx)
                node_segments[parent_idx].append(parent_idx)
                directional_mask[parent_idx].append(backward_value)
            else:
                node_indices.append([edge_idx])
                node_segments.append([parent_idx])
                directional_mask.append([backward_value])
            node_indices.append([edge_idx])
            node_segments.append([current_idx])
            directional_mask.append([forward_value])

    return nodes, edge_indices, edges, node_indices, node_segments, directional_mask


tree_text = '(ROOT (S (PP (IN In) (NP (DT an) (JJ ambient) (NN intelligence) (NN world))) (, ,) (NP (NNS devices))' \
            ' (VP (VBP work) (PP (IN in) (NP (NN concert))) (S (VP (TO to) (VP (VB support) (NP (NNS people))' \
            ' (PP (IN in) (S (VP (VBG carrying) (PRT (RP out)) (NP (NP (PRP$ their) (JJ everyday) (NN life)' \
            ' (NNS activities) (, ,) (NNS tasks) (CC and) (NNS rituals)) (PP (IN in) (NP (NP (JJ easy) (, ,)' \
            ' (JJ natural) (NN way)) (VP (VBG using) (NP (NP (NN information) (CC and) (NN intelligence))' \
            ' (SBAR (WHNP (WDT that)) (S (VP (VBZ is) (VP (VBN hidden) (PP (IN in) (NP (NP (DT the)' \
            ' (NN network)) (VP (VBG connecting) (NP (DT these) (NNS devices))) (PRN (-LRB- -LRB-) ' \
            '(S (VP (VB see) (NP (NP (NN Internet)) (PP (IN of) (NP (NNS Things))))))' \
            ' (-RRB- -RRB-)))))))))))))))))))) (. .)))'


full_nodes, repeated_edge_features, adjacency_matrix = retrieve_tree_info_adj(tree_text)

print('Nodes: ', len(full_nodes), '\n')
print(full_nodes)
print('*' * 50)

print('Edge Features: ', len(repeated_edge_features), '\n')
print(repeated_edge_features)
print('*' * 50)

node_depths = get_nodes_depths(tree_text)

print('Depths: ', node_depths)
print('Max depth: ', max(node_depths))

draw_nltk_tree(tree_text)
