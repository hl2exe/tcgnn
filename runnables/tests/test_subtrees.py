"""

Get all subtrees

"""

import numpy as np
from utility.graph_utils import get_stk, get_sstk, build_assignment_matrix


# def get_stk(node_idxs, depths, is_directional=False):
#     sub_trees = []
#     subtrees_per_node = {}
#     subtree_to_idx = {}
#
#     edge_indices = []
#     edge_features = []
#     node_indices = []
#     node_segments = []
#     directionality_mask = []
#
#     forward_value = 1
#     backward_value = 1
#
#     if is_directional:
#         backward_value = -1
#
#     max_depth = np.max(depths)
#
#     for node_enum, (node_idx, node_depth) in enumerate(zip(node_idxs, depths)):
#         if node_idx == 0:
#             continue
#
#         if node_depth == max_depth:
#             continue
#
#         limit_boundary = []
#         for idx, value in enumerate(depths[node_enum + 1:]):
#             if value < node_depth:
#                 limit_boundary.append(idx + node_enum + 1)
#                 break
#             if value == node_depth:
#                 limit_boundary.append(idx + node_enum + 1)
#
#         if not limit_boundary:
#             limit_boundary = len(node_idxs)
#         else:
#             limit_boundary = limit_boundary[0]
#
#         to_add = node_idxs[node_enum:limit_boundary]
#         if len(to_add) > 1:
#             sub_trees.append(to_add)
#             subtrees_per_node.setdefault(node_idx, []).append(to_add)
#             subtree_to_idx["-".join(to_add.astype(str))] = len(sub_trees) - 1
#
#         # connect subtree to previous node subtrees
#         if node_depth > 1 and len(to_add) > 1:
#             previous_node = [other_node_idx for other_node_enum, (other_node_idx, other_node_depth) in
#                              enumerate(zip(node_idxs, depths))
#                              if other_node_depth == node_depth - 1 and other_node_enum < node_enum]
#             assert len(previous_node) >= 1
#             previous_node = previous_node[-1]
#
#             assert previous_node in subtrees_per_node
#
#             left_edge_index = subtree_to_idx['-'.join(subtrees_per_node[previous_node][0].astype(str))]
#             right_edge_index = len(sub_trees) - 1
#             edge_indices.append([left_edge_index, right_edge_index])
#             edge_features.append(node_depth)
#
#     # Connect first level subtrees in sequential pairwise fashion
#     first_level_node_indexes = [node_idx for node_idx, node_depth in zip(node_idxs, depths) if node_depth == 1]
#     for j in range(len(first_level_node_indexes) - 1):
#         left_pair_element = first_level_node_indexes[j]
#         right_pair_element = first_level_node_indexes[j + 1]
#
#         left_edge_index = subtree_to_idx['-'.join(subtrees_per_node[left_pair_element][0].astype(str))]
#         right_edge_index = subtree_to_idx['-'.join(subtrees_per_node[right_pair_element][0].astype(str))]
#
#         edge_indices.append([left_edge_index, right_edge_index])
#         edge_features.append(1)
#
#     # Build node indices
#     for tree_idx in range(len(sub_trees)):
#         for edge_idx, edge_pair in enumerate(edge_indices):
#             if tree_idx in edge_pair:
#                 node_indices.append(edge_idx)
#                 node_segments.append(tree_idx)
#
#                 # Node is a sender
#                 if tree_idx == edge_pair[0]:
#                     directionality_mask.append(backward_value)
#                 else:
#                     directionality_mask.append(forward_value)
#
#     return sub_trees, edge_indices, edge_features, node_indices, node_segments, directionality_mask
#
#
# def get_terminal_indexes(node_idxs, depths):
#     """
#     Detect depth decrease or plateau
#     """
#
#     local_terminal_idxs = []
#
#     if len(node_idxs) == 2:
#         return local_terminal_idxs
#
#     for idx, (node_idx, depth_value) in enumerate(zip(node_idxs, depths)):
#         if idx + 1 < len(depths) and depths[idx + 1] <= depth_value:
#             local_terminal_idxs.append(node_idx)
#
#     local_terminal_idxs.append(node_idxs[-1])
#
#     return local_terminal_idxs
#
#
# def get_sstk(node_idxs, depths, is_directional=False):
#     sub_trees = []
#     subtrees_per_node = {}
#     subtree_to_idx = {}
#
#     edge_features = []
#     edge_indices = []
#     node_indices = []
#     node_segments = []
#     directionality_mask = []
#
#     forward_value = 1
#     backward_value = 1
#
#     if is_directional:
#         backward_value = -1
#
#     max_depth = np.max(depths)
#
#     for node_enum, (node_idx, node_depth) in enumerate(zip(node_idxs, depths)):
#         if node_idx == 0:
#             continue
#
#         if node_depth == max_depth:
#             continue
#
#         limit_boundary = []
#         for idx, value in enumerate(depths[node_enum + 1:]):
#             if value < node_depth:
#                 limit_boundary.append(idx + node_enum + 1)
#                 break
#             if value == node_depth:
#                 limit_boundary.append(idx + node_enum + 1)
#
#         if not limit_boundary:
#             limit_boundary = len(node_idxs)
#         else:
#             limit_boundary = limit_boundary[0]
#
#         to_add = node_idxs[node_enum:limit_boundary]
#         if len(to_add) > 1:
#             sub_trees.append(to_add)
#             subtrees_per_node.setdefault(node_idx, []).append(to_add)
#             subtree_to_idx["-".join(to_add.astype(str))] = len(sub_trees) - 1
#
#             # get local terminals
#             local_terminal_idxs = get_terminal_indexes(to_add, depths[node_enum:limit_boundary])
#
#             # remove local terminal combinations (from single local terminal to all of them)
#             for combination_range_value in range(1, len(local_terminal_idxs) + 1):
#                 curr_combination_set = list(combinations(local_terminal_idxs, combination_range_value))
#                 for comb in curr_combination_set:
#                     mod_to_add = np.array([item for item in to_add if item not in comb])
#                     sub_trees.append(mod_to_add)
#                     subtrees_per_node.setdefault(node_idx, []).append(mod_to_add)
#                     subtree_to_idx["-".join(mod_to_add.astype(str))] = len(sub_trees) - 1
#
#             # Connect node subtrees to each other
#             for j in range(len(subtrees_per_node[node_idx]) - 1):
#                 for k in range(j + 1, len(subtrees_per_node[node_idx])):
#                     left_pair_element = subtrees_per_node[node_idx][j]
#                     right_pair_element = subtrees_per_node[node_idx][k]
#
#                     left_edge_index = subtree_to_idx['-'.join(left_pair_element.astype(str))]
#                     right_edge_index = subtree_to_idx['-'.join(right_pair_element.astype(str))]
#
#                     edge_indices.append([left_edge_index, right_edge_index])
#                     edge_features.append(node_depth)
#
#             # Connect subtree to previous node subtrees
#             if node_depth > 1 and len(to_add) > 1:
#                 previous_node = [other_node_idx for other_node_enum, (other_node_idx, other_node_depth) in
#                                  enumerate(zip(node_idxs, depths))
#                                  if other_node_depth == node_depth - 1 and other_node_enum < node_enum]
#                 assert len(previous_node) >= 1
#                 previous_node = previous_node[-1]
#
#                 assert previous_node in subtrees_per_node
#
#                 for j in range(len(subtrees_per_node[previous_node])):
#                     for k in range(len(subtrees_per_node[node_idx])):
#                         left_pair_element = subtrees_per_node[previous_node][j]
#                         right_pair_element = subtrees_per_node[node_idx][k]
#
#                         left_edge_index = subtree_to_idx['-'.join(left_pair_element.astype(str))]
#                         right_edge_index = subtree_to_idx['-'.join(right_pair_element.astype(str))]
#
#                         edge_indices.append([left_edge_index, right_edge_index])
#                         edge_features.append(node_depth)
#
#     # Connect first level subtrees in sequential pairwise fashion
#     first_level_node_indexes = [node_idx for node_idx, node_depth in zip(node_idxs, depths) if node_depth == 1]
#     for j in range(len(first_level_node_indexes) - 1):
#         left_tree_idx = first_level_node_indexes[j]
#         right_tree_idx = first_level_node_indexes[j + 1]
#
#         for z in range(len(subtrees_per_node[left_tree_idx])):
#             for k in range(len(subtrees_per_node[right_tree_idx])):
#                 left_pair_element = subtrees_per_node[left_tree_idx][z]
#                 right_pair_element = subtrees_per_node[right_tree_idx][k]
#
#                 left_edge_index = subtree_to_idx['-'.join(left_pair_element.astype(str))]
#                 right_edge_index = subtree_to_idx['-'.join(right_pair_element.astype(str))]
#
#                 edge_indices.append([left_edge_index, right_edge_index])
#                 edge_features.append(1)
#
#     # Build node indices
#     for tree_idx in range(len(sub_trees)):
#         for edge_idx, edge_pair in enumerate(edge_indices):
#             if tree_idx in edge_pair:
#                 node_indices.append(edge_idx)
#                 node_segments.append(tree_idx)
#
#                 # Node is a sender
#                 if tree_idx == edge_pair[0]:
#                     directionality_mask.append(backward_value)
#                 else:
#                     directionality_mask.append(forward_value)
#
#     return sub_trees, edge_indices, edge_features, node_indices, node_segments, directionality_mask
#
#
# def build_assignment_matrix(subtrees, nodes):
#     assignment_matrix = np.zeros((len(nodes), len(subtrees)), dtype=np.float32)
#
#     for idx, subtree in enumerate(subtrees):
#         for node_idx in subtree:
#             assignment_matrix[node_idx, idx] = 1.0
#
#     return assignment_matrix


# nodes = np.array(['S', "N", "Mary", "VP", "V", "brought", "NP", "D", "a", "N", "cat"])
nodes = np.array(['0', '1', '2', '3', '4', '5', '6', '7'])
node_idxs = np.arange(len(nodes))
# depths = [0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 4]
depths = [0, 1, 2, 2, 1, 2, 2, 1]
first_level_indexes = np.where(np.array(depths) == 1)[0]

res_stk, stk_edge_indices, stk_edge_features, \
stk_node_indices, stk_node_segments, \
stk_directionality_mask = get_stk(node_idxs, depths, is_directional=True)

stk_am = build_assignment_matrix(res_stk, nodes)

print()
print('Sub Trees (ST)')
print()

print('Subtrees: ', res_stk)
print()
print('Edge indices: ', stk_edge_indices)
print()
print('Edge features: ', stk_edge_features)
print()
print('Node indices: ', stk_node_indices)
print()
print('Node segments: ', stk_node_segments)
print()
print('Directionality mask: ', stk_directionality_mask)
print()

for seq in res_stk:
    print(nodes[seq])

print()
print('Sub Trees (ST) Connections!')
print()

for pair in stk_edge_indices:
    print('{0} ---- {1}'.format(nodes[res_stk[pair[0]]], nodes[res_stk[pair[1]]]))

print()

print('Assignment matrix: ', stk_am)
print()

print()
print('SubSet Trees (SST)')
print()

res_sstk, sstk_edge_indices, sstk_edge_features, \
sstk_node_indices, sstk_node_segments, \
sstk_directionality_mask = get_sstk(node_idxs, depths, is_directional=True)

print('Subtrees: ', res_sstk)
print()
print('Edge indices: ', sstk_edge_indices)
print()
print('Edge features: ', sstk_edge_features)
print()
print('Node indices: ', sstk_node_indices)
print()
print('Node segments: ', sstk_node_segments)
print()
print('Directionality mask: ', sstk_directionality_mask)
print()

for seq in res_sstk:
    print(nodes[seq])

print()
print('SubSet Trees (SST) Connections!')
print()

for pair in sstk_edge_indices:
    print('{0} ---- {1}'.format(nodes[res_sstk[pair[0]]], nodes[res_sstk[pair[1]]]))

print()
print()

print('Total nodes: ', len(node_idxs))
print('Total ST trees: ', len(res_stk))
print('Total SST trees: ', len(res_sstk))
print('Total ST connections: ', len(stk_edge_indices))
print('Total SST connections: ', len(sstk_edge_indices))
