
"""

Simple keras test of ibm2015 loo test

"""

import pandas as pd
import const_define as cd
import os
import tensorflow as tf
from sklearn.model_selection import LeaveOneOut
import numpy as np
from sklearn.metrics import f1_score
from sklearn.utils.class_weight import compute_class_weight

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)


# Step 1: Load dataset

df = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'dataset_fixed.csv'))

train_data = df[df['Data-set'] == 'train and test']
val_data = df[df['Data-set'] != 'train and test']

print('Train: ', train_data.shape)
print('Validation: ', val_data.shape)

loo = LeaveOneOut()

topics = np.unique(train_data['Topic id'].values)

for train_indexes, test_indexes in loo.split(topics):

    # train_keys = split_values[train_keys_indexes]
    excluded_key = topics[test_indexes]

    if len(excluded_key) == 1:
        excluded_key = excluded_key[0]

    print('Excluding: {}'.format(excluded_key))

    # Splitting

    train_df = train_data[~train_data['Topic id'].isin([excluded_key])]
    test_df = train_data[train_data['Topic id'].isin([excluded_key])]

    x_train = train_df.Sentence.values
    x_val = val_data.Sentence.values
    x_test = test_df.Sentence.values

    y_train = train_df.Label.values
    y_val = val_data.Label.values
    y_test = test_df.Label.values

    # Pre-processing
    tokenizer = tf.keras.preprocessing.text.Tokenizer()
    tokenizer.fit_on_texts(x_train)

    x_train = tokenizer.texts_to_sequences(x_train)
    x_val = tokenizer.texts_to_sequences(x_val)
    x_test = tokenizer.texts_to_sequences(x_test)

    x_train = tf.keras.preprocessing.sequence.pad_sequences(x_train, padding='pre', maxlen=80)
    x_val = tf.keras.preprocessing.sequence.pad_sequences(x_val, padding='pre', maxlen=80)
    x_test = tf.keras.preprocessing.sequence.pad_sequences(x_test, padding='pre', maxlen=80)

    print('X_train: ', x_train.shape)
    print('X_val: ', x_val.shape)
    print('X_test: ', x_test.shape)

    label_list = np.unique(y_train)

    label_map = {}
    for (i, label) in enumerate(label_list):
        hot_encoding = [0] * len(label_list)
        hot_encoding[i] = 1
        label_map[label] = hot_encoding

    print("Label map: ", label_map)

    y_train = np.array([label_map[item] for item in y_train])
    y_val = np.array([label_map[item] for item in y_val])
    y_test = np.array([label_map[item] for item in y_test])

    print('Y_train: ', y_train.shape)
    print('Y_val: ', y_val.shape)
    print('Y_test: ', y_test.shape)

    # Build model
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=[80, ]))
    model.add(tf.keras.layers.Embedding(input_dim=len(tokenizer.word_index) + 1,
                                        output_dim=128,
                                        input_length=80,
                                        # mask_zero=True,
                                        name='embedding'))
    # model.add(tf.keras.layers.Dense(256))
    # model.add(tf.keras.layers.Lambda(lambda x: tf.reduce_sum(x, axis=1)))
    model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64)))
    model.add(tf.keras.layers.Dense(len(label_list), activation='softmax'))

    model.compile(optimizer='adam', loss='categorical_crossentropy',  metrics=['accuracy'])
    print(model.summary())

    # Training
    class_weight = compute_class_weight(class_weight='balanced', y=np.argmax(y_train, axis=1),
                                        classes=np.unique(np.argmax(y_train, axis=1)))
    es = tf.keras.callbacks.EarlyStopping(patience=10)
    model.fit(x_train, y_train, epochs=30, batch_size=128,
              validation_data=(x_val, y_val),
              verbose=1, callbacks=[es], class_weight=class_weight)

    # Inference
    val_preds = model.predict(x_val, batch_size=128)
    test_preds = model.predict(x_test, batch_size=128)

    val_f1 = f1_score(y_true=np.argmax(y_val, axis=1), y_pred=np.argmax(val_preds, axis=1), average=None)
    test_f1 = f1_score(y_true=np.argmax(y_test, axis=1), y_pred=np.argmax(test_preds, axis=1), average=None)

    print('Validation F1: ', val_f1)
    print('Test F1: ', test_f1)





