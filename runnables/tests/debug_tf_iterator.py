
import numpy as np
import tensorflow as tf
import psutil

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

# try:
#     tf.config.set_visible_devices([], 'GPU')
#     visible_devices = tf.config.get_visible_devices()
#     for device in visible_devices:
#         assert device.device_type != 'GPU'
# except:
#     pass

data = np.ones([int(1e7), 1], dtype=np.float32)
dataset = tf.data.Dataset.from_tensor_slices(data)
iterator = dataset.shuffle(int(1e7)).batch(int(1e6)).repeat(2)
# iterator = dataset.batch(int(1e6)).repeat(2)

for it in iterator:
    used_mem = psutil.virtual_memory().used
    print("used memory: {} Mb".format(used_mem / 1024 / 1024))