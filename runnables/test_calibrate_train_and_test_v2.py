"""

@Author: Federico Ruggeri

@Date: 03/09/2019

Hyperopt hyper-parameters calibration.

** BEFORE RUNNING **

1. Check the following configuration files:
    calibrator_info.json, hyperopt_model_gridsearch.json, distributed_config.json, callbacks.json, data_loader.json,
    model_config.json, training_config.json

"""

import os

from custom_callbacks_v2 import EarlyStopping, EarlyPerformanceStopping, TreeLangrangianEnabler

import const_define as cd
from calibrators import HyperOptCalibrator
from data_loader import DataLoaderFactory
from utility.json_utils import load_json
from utility.distributed_test_utils import train_and_test
from utility.python_utils import merge
import tensorflow as tf


if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')

    # Step 1: Validator config

    train_and_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = train_and_test_config['gpu_start_index']
            gpu_end_index = train_and_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[train_and_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Step 2: Calibrator config

    # Strategy
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))
    strategy_name = distributed_info['strategy']
    if hasattr(tf.distribute.experimental, strategy_name):
        strategy = getattr(tf.distribute.experimental, strategy_name)
    elif hasattr(tf.distribute, strategy_name):
        strategy = getattr(tf.distribute, strategy_name)
    else:
        raise RuntimeError('Could not find distributed strategy! Got: {}'.format(strategy_name))

    strategy = strategy(**distributed_info['strategy_args'][strategy_name])

    calibrator_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALIBRATOR_INFO_NAME))

    # Callbacks
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    early_performance_stopper = EarlyPerformanceStopping(**callbacks_data['earlyperformancestopping'])
    lagrangian_enabler = TreeLangrangianEnabler(**callbacks_data['treelagrangianenabler'])

    validator_base_args = {
        'validation_percentage': train_and_test_config['validation_percentage'],
        'data_handle': data_handle,
        'network_args': model_config,
        'data_loader_info': data_loader_info,
        'model_type': train_and_test_config['model_type'],
        'training_config': training_config,
        'error_metrics': train_and_test_config['error_metrics'],
        'error_metrics_nicknames': train_and_test_config['error_metrics_nicknames'],
        'error_metrics_additional_info': train_and_test_config['error_metrics_additional_info'],
        'save_model': False,
        'test_path': None,
        'use_tensorboard': False,
        'compute_test_info': False,
        'distributed_info': distributed_info,
        'callbacks': [early_stopper, early_performance_stopper, lagrangian_enabler],
        # 'strategy': strategy,
        'repetitions': train_and_test_config['repetitions']
    }

    calibrator = HyperOptCalibrator(model_type=train_and_test_config['model_type'],
                                    validator_method=train_and_test,
                                    validator_base_args=validator_base_args,
                                    **calibrator_config)

    space = calibrator.load_space()
    calibrator.run(space=space)
