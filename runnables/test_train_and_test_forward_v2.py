"""

@Author: Federico Ruggeri

@Date: 27/02/2020

"""

import os

import tensorflow as tf

import const_define as cd
from custom_callbacks_v2 import TreeDebugger, TreeConstraintCalculator
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import train_and_test_forward
from utility.json_utils import save_json, load_json
from utility.log_utils import Logger
from utility.python_utils import merge

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    model_type = "persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2"
    test_name = "f1_es_STK_binary"
    save_predictions = True

    model_path = os.path.join(cd.TRAIN_AND_TEST_DIR, model_type, test_name)

    # Logging
    Logger.set_log_path(model_path)
    logger = Logger.get_logger(__name__)

    train_and_test_config = load_json(os.path.join(model_path, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = train_and_test_config['gpu_start_index']
            gpu_end_index = train_and_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

    training_config = load_json(os.path.join(model_path, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)

    data_handle = data_loader.load(**data_loader_info)

    # Strategy
    distributed_info = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    # Callbacks
    # TODO: automatize callbacks creation
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    tree_debugger = TreeDebugger(save_path=model_path, full_inference=True)
    tree_constraint_calculator = TreeConstraintCalculator(save_path=model_path, save_suffix=None)

    callbacks = [
        tree_constraint_calculator
    ]

    scores = train_and_test_forward(data_handle=data_handle,
                                    callbacks=callbacks,
                                    model_type=train_and_test_config['model_type'],
                                    training_config=training_config,
                                    error_metrics=train_and_test_config['error_metrics'],
                                    error_metrics_additional_info=train_and_test_config[
                                        'error_metrics_additional_info'],
                                    error_metrics_nicknames=train_and_test_config[
                                        'error_metrics_nicknames'],
                                    compute_test_info=train_and_test_config['compute_test_info'],
                                    network_args=model_config,
                                    test_path=model_path,
                                    data_loader_info=data_loader_info,
                                    validation_percentage=train_and_test_config['validation_percentage'],
                                    distributed_info=distributed_info,
                                    repetitions=train_and_test_config['repetitions'])

    logger.info('Validation scores: {}'.format(scores['validation_info']))
    logger.info('Test scores: {}'.format(scores['test_info']))

    if save_predictions:
        save_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(model_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(model_path, cd.JSON_TRAIN_PREDICTIONS_NAME), scores['train_predictions'])
        save_json(os.path.join(model_path, cd.JSON_VAL_PREDICTIONS_NAME), scores['val_predictions'])
        save_json(os.path.join(model_path, cd.JSON_TEST_PREDICTIONS_NAME), scores['test_predictions'])
