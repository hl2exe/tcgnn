import os

import tensorflow as tf

import const_define as cd
from data_loader import DataLoaderFactory
from custom_callbacks_v2 import TreeDebugger, TreeConstraintCalculator
from utility.distributed_test_utils import cross_validation_forward
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import save_json, load_json
from utility.log_utils import Logger
from utility.python_utils import merge
import numpy as np

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    # Inputs

    model_type = "ibm2015_experimental_single_tree_pooled_adj_gnn_v2"
    test_name = "C_f1_es_STK"
    save_predictions = True

    model_path = os.path.join(cd.CV_DIR, model_type, test_name)

    # Logging
    Logger.set_log_path(model_path)
    logger = Logger.get_logger(__name__)

    if not os.path.isdir(model_path):
        raise RuntimeError('Could not find test folder. Got: {}'.format(model_path))

    cv_test_config = load_json(os.path.join(model_path, cd.JSON_CV_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = cv_test_config['gpu_start_index']
            gpu_end_index = cv_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

    training_config = load_json(os.path.join(model_path, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Distributed info
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)

    # Callbacks
    # TODO: automatize callbacks creation
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    tree_debugger = TreeDebugger(save_path=model_path, full_inference=True)
    tree_constraint_calculator = TreeConstraintCalculator(save_path=model_path, save_suffix=None)
    callbacks = [
        tree_constraint_calculator
    ]

    scores = cross_validation_forward(validation_percentage=cv_test_config['validation_percentage'],
                                      data_handle=data_handle,
                                      network_args=model_config,
                                      data_loader_info=data_loader_info,
                                      model_type=cv_test_config['model_type'],
                                      error_metrics=cv_test_config['error_metrics'],
                                      error_metrics_additional_info=cv_test_config[
                                          'error_metrics_additional_info'],
                                      error_metrics_nicknames=cv_test_config['error_metrics_nicknames'],
                                      training_config=training_config,
                                      cv=cv,
                                      test_path=model_path,
                                      callbacks=callbacks,
                                      save_predictions=True,
                                      repetitions=cv_test_config['repetitions'],
                                      split_key=cv_test_config['split_key'],
                                      distributed_info=distributed_info)

    # Validation
    if cv_test_config['repetitions'] > 1:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item) for key, item in scores['validation_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))

    # Test
    if cv_test_config['repetitions'] > 1:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item) for key, item in scores['test_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item) for key, item in scores['test_info'].items() if not key.startswith('avg')}))

    if save_predictions:
        save_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(model_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(model_path, cd.JSON_TRAIN_PREDICTIONS_NAME), scores['train_predictions'])
        save_json(os.path.join(model_path, cd.JSON_VAL_PREDICTIONS_NAME), scores['val_predictions'])
        save_json(os.path.join(model_path, cd.JSON_TEST_PREDICTIONS_NAME), scores['test_predictions'])
