## Tree-Constrained Graph Neural Networks for Argument Mining

## Repository maintainer: Federico Ruggeri

Repository is structured according to a WIP internal framework. For better understanding, we will briefly describe the overall project structure and, subsequently, describe how to reproduce results and launch experiments.

# Version Control

Current framework version is heavily based on tensorflow library.

* tensorflow-gpu: 2.3
* python: 3.6

For more information, please check ```requirements_v2.txt``` file.

# Project Structure

The framework proposes the following data pipeline:

* Data Loading
* Data Pre-processing
* Data Conversion
* Test Routine (e.g. cross-validation)
* Model Definition
* Model Training
* Model Evaluation
* Results post-processing

The framework is heavily inspired to Keras library and it basically extends some of its functionality. It also offers a whole modular pipeline for quick benchmarking.


## Framework Workflow

We now give more details about each pipeline step while pointing out the corresponding python script files for quick inspection.

* Data Loading (```data_loader.py```): each dataset is wrapped with a custom data loader object. Loaded data is then passed to a specific data wrapper that is compliant with the specified test routine.
* Data Pre-processing (```data_processor.py```): input data is converted into a set of input examples. In particular, text pre-processing operations like text normalization and tokenization are applied.
* Data Conversion (```data_converter.py```): textual data is then converted into numerical format in order to be compliant with a neural network model. Data is serialized by using Tensorflow APIs.
* Test Routine (```utility/distributed_test_utils.py```): all test routines are defined here. Each routine follows an internal workflow:
    * Load evaluation metrics
    * Load model specific configuration ID
    * Build workflow wrappers: data pre-processing, data tokenization, data conversion
    * Test routine loop:
        * Get data splits (train, val, test)
        * Build dataset if it does not exist
        * Build network model
        * Model training
        * Model evaluation
    * Wrap-up test routine performance scores
* Save model configuration, test routine configuration, performance scores

## Configuration files

All project scripts are heavily based on JSON configuration files (check ```/configs``` folder). Intuitively, each configuration file is dedicated to a particular workflow step, spanning from model parameters (```configs/distributed_model_config.json```) to test routine specific arguments (```configs/cv_test_config.json```).


# FAQ

## How to run an experiment?

There are two ways to run an experiment. First of all, runnable scripts are stored in the ```runnables/``` folder.

* ```test_cross_validation_v2.py```: (repeated) cross-validation test.
* ```test_train_and_test_v2.py```: (repeated) train and test.
* ```test_loo_v2.py```: (repeated) LOO test.

### Manual way

You can manually modify each JSON configuration file and then launch test routine scripts like ```runnables/test_cross_validation_v2.py```. However, due to the large amount of parameters and configuration files, this is not the recommended way.

### Script-based way

To avoid any kind of error, you can automatically configure configuration files for your model by running ```runnables/other/quick_test_setup.py```. The script can be also launched from terminal (check argparse help).

You only have to specify 5-6 arguments and you are then ready to launch your routine script (e.g. ```runnables/test_cross_validation_v2.py```). Please, check argparse help functionality for more details.

*Before that, don't forget to check model configuration (```configs/distributed_model_config.json```).* This file is where all model parameters are stored.


## How to run sequential experiments?

It is also possible to run experiments in sequence. Open and modify ```runnables/run_test_sequence.py``` according to you needs.

Unfortunately, the script currently does not support argparse, but the behaviour is quite simple: it basically runs the following sequence of commands:

* ```runnables/other/quick_test_setup.py```
* *test script routine* (cv, loo, train and test).

In particular, the script waits for the termination of each executed script.

It is also possible to run ```runnables/run_test_sequence.py``` in parallel. Each instance modifies configuration files without interference via a lock system.

## Where do test specific files get saved? (model, configuration files, etc..)

Everytime a test routine is executed by means of a specific runnable test script, e.g. ```runnables/test_cross_validation.py```, a folder is created concerning current test instance. Depending on the given test, the folder is located in a corresponding root folder.

For instance, cross-validation tests are saved into ```cv_test``` folder. Model architectures are distinguished by a specific root folder as well.
The overall architecture is as follows:

* test_routine_folder
    * model_architecture_folder
        * test_instance_folder

In particular, the ```test_instance_folder``` is by default determined by the test instance execution time under the following date format: DD-MM-YYYY-hh-mm-ss.

However, it is also possible to specificy a specific name (check ```configs/cv_test_config.json```) by which the folder will be renamed at test completion. 

## What information gets saved into each test instance folder?

A lot of information for reproducibility and easy tracing is stored.

* Configuration files (a copy of ```configs/``` directory)
* Model weights (.h5) and, if any, model specific states. For instance, sampling strategies require to save the learnt priority distribution.
* Test evaluation metrics: both for validation and test sets.
* Model predictions on test set

Files are saved progressively during the test routine, i.e. cross-validation, so that it is possible to resume the test from the latest checkpoint or to quickly inspect model content.

## How to retrieve the pooling matrix for inspection?

Once a test instance has completed, it is possible to quickly collect the pooling matrix P. 
For each test routine, there's a ```_forward``` variant. Currently, these scripts do not support argparse, but it is just necessary to modify the following settings:

```
model_type = *name_of_the_model* #(e.g. ibm2015_experimental_single_tree_pooled_adj_gnn_v2)
test_name = *test_folder_name* #(e.g. C_f1_es_PTK)
save_predictions = True|False #(whether to save metrics and predictions or not)
```

Each ```_forward``` test script repeated the same test routine with the selected pre-trained model (**inference only**).

At the end of the process, for each test split, the pooling matrix is stored in the same test instance original folder (.npy format).

## How to retrieve most frequent tree fragments?

Once the pooling matrix has been retrieved (check *How to retrieve the pooling matrix for inspection?* section for how to), it is just necessary to run ```runnables/other/get_gnn_most_frequent_features.py```. Currently, this script does not support argparse, but it is just necessary to modify few settings. Further details are given in the script in order to handle each dataset properly.

For each class, the script prints out the top-K most frequent tree fragments (with counts). Additionally, it also reports the average cumulative node intensity filter given by sigmoid-based pooling.

What does it mean? If we consider the original input tree to have N (#number of nodes) total intensity. We compute the ratio between the total pooling intensity (summing pooling values across *k* clusters) and N. Note that, in principle, the ratio might be greater than 1, but it might be rare and possibly avoided with appropriate tree regularizations.

## How to quickly inspect evaluation metrics?

For each dataset (in this experimental setup), there's a dedicated script for quickly metric retrieval.

* ```runnables/other/compute_cv_stats.py``` : for IBM2015
* ```runnables/other/compute_loo_stats.py```: for UKP
* ```runnables/other/compute_tat_stats.py```: for PE
* ```runnables/other/compute_tat_stats_abstrct.py```: for AbstRCT

## Are there any cv-test pre-built folds for quick comparison?

Yes of course! Please check ```prebuild_folds``` folder. It contains all prebuilt cv test folds.

In particular, there's a JSON file containing fold indexes, whereas, corresponding indexed data is stored in a TXT file with the same name.

# Contact

Federico Ruggeri: federico.ruggeri6@unibo.it

# Credits

Many thanks to all these people for their valuable feedback!

* *Andrea Galassi*
* Michele Lombardi

Special thanks to:

* Marco Lippi
* Paolo Torroni