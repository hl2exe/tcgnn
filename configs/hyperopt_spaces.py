"""

Hyperopt spaces for calibration

"""

from hyperopt import hp
import numpy as np

spaces = {
    "ibm2015_experimental_single_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "repeated_shared": hp.choice("repeated_shared", [False, True]),
        "timesteps": hp.choice("timesteps", [1, 2, 3]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "edge_mlp_weights": hp.choice("edge_mlp_weights", [
            [hp.choice('edge_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
    },
    "ibm2015_experimental_single_dep_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "timesteps": hp.choice("timesteps", [1, 2, 3, 4, 5, 6]),
        "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128, 256]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_layer1_1', [64, 128, 256, 512])],
            [hp.choice("answer_layer1_2", [64, 128, 256, 512]),
             hp.choice("answer_layer2_2", [64, 128, 256, 512])]]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "edge_mlp_weights": hp.choice("edge_mlp_weights", [
            [hp.choice('edge_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
    },
    "ibm2015_experimental_single_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "repeated_shared": hp.choice("repeated_shared", [False, True]),
        "timesteps": hp.choice("timesteps", [1, 2, 3, 4]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "pooling_mlp_weights": hp.choice("pooling_mlp_weights", [
            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_2", [1])],
            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer3_3", [1])]]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]])
    },
    "ibm2015_experimental_single_dep_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "timesteps": hp.choice("timesteps", [1, 2, 3, 4]),
        "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128, 256]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_layer1_1', [64, 128, 256, 512])],
            [hp.choice("answer_layer1_2", [64, 128, 256, 512]),
             hp.choice("answer_layer2_2", [64, 128, 256, 512])],
            [hp.choice("answer_layer1_3", [64, 128, 256, 512]),
             hp.choice("answer_layer2_3", [64, 128, 256, 512]),
             hp.choice("answer_layer3_3", [64, 128, 256, 512])]]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer3_3", [64, 128, 256, 512])]]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]])
    },
    "ibm2015_experimental_single_pooled_adj_gnn_v2": {
        "gcn_info": {
            "2": {
                "message_weights": [
                    256
                ],
                "aggregation_weights": [
                    256,
                    64,
                    512
                ],
                "node_weights": [
                    512,
                    64
                ],
                "pooling_weights": [
                    64,
                    1
                ]
            },
            "1": {
                "message_weights": hp.choice("message_weights", [
                    [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
                "aggregation_weights": hp.choice("aggregation_mlp_weights", [
                    [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
                "node_weights": hp.choice("node_mlp_weights", [
                    [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
                "pooling_weights": hp.choice("pooling_weights_1", [
                    [hp.choice("next_level_pooling_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer2_2", np.arange(4) + 2)],
                    [hp.choice("next_level_pooling_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer3_3", np.arange(4) + 2)]
                ]),
            },
            "0": {
                "message_weights": [
                    64
                ],
                "aggregation_weights": [
                    512,
                    512
                ],
                "node_weights": [
                    64
                ],
                "pooling_weights": [
                    256,
                    512,
                    hp.choice("pooling_nodes_0", np.arange(10) + 15)
                ]
            }
        },
    },
    "ibm2015_experimental_baseline_lstm_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_weights_layer1_1', [64, 128, 256, 512])],
            [hp.choice("answer_weights_layer1_2", [64, 128, 256, 512]),
             hp.choice("answer_weights_layer2_2", [64, 128, 256, 512])],
        ]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "lstm_weights": hp.choice("lstm_weights", [
            [hp.choice('lstm_weights_layer1_1', [64, 128, 256])]
        ])
    },
    "ibm2015_experimental_single_tree_pooled_adj_gnn_v2": {
        # "overlapping_threshold": hp.choice("overlapping_threshold", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]),
        # "connectivity_threshold": hp.choice("connectivity_threshold", [0.1, 0.2, 0.3, 0.4, 0.5]),
        # "tree_intensity_coefficient": hp.choice("tree_intensity_coefficient",
        #                                         [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]),
        # "kernel_coefficient": hp.choice("kernel_coefficient",
        #                                 [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]),
        # "minimal_size_coefficient": hp.choice("minimal_size_coefficient",
        #                                       [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]),
        # "contiguous_coefficient": hp.choice("contiguous_coefficient",
        #                                     [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]),
        "lambda_values": [hp.choice("contiguous_constraint", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]),
                          hp.choice("kernel_constraint", [0.0]),
                          hp.choice("minimal_size_constraint", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]),
                          hp.choice("tree_intensity_constraint", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])]
    },

    "ibm2015_experimental_graph_baseline_lstm_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_weights_layer1_1', [64, 128, 256, 512])],
            [hp.choice("answer_weights_layer1_2", [64, 128, 256, 512]),
             hp.choice("answer_weights_layer2_2", [64, 128, 256, 512])],
        ]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "lstm_weights": hp.choice("lstm_weights", [
            [hp.choice('lstm_weights_layer1_1', [64, 128, 256])]
        ])
    },

    "ibm2015_experimental_dual_graph_baseline_lstm_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_weights_layer1_1', [64, 128, 256, 512])],
            [hp.choice("answer_weights_layer1_2", [64, 128, 256, 512]),
             hp.choice("answer_weights_layer2_2", [64, 128, 256, 512])],
        ]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "lstm_weights": hp.choice("lstm_weights", [
            [hp.choice('lstm_weights_layer1_1', [64, 128, 256])]
        ]),
        "self_attention_weights": hp.choice("self_attention_weights", [
            [hp.choice('self_attention_layer1_1', [64, 128, 256, 512])]
        ])
    },

    "ukp_experimental_single_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "repeated_shared": hp.choice("repeated_shared", [False, True]),
        "timesteps": hp.choice("timesteps", [1, 2, 3]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "edge_mlp_weights": hp.choice("edge_mlp_weights", [
            [hp.choice('edge_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
        ]),
    },
    "ukp_experimental_pairwise_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-02
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "timesteps": hp.choice("timesteps", [1, 2, 3]),
        "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128, 256]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "edge_mlp_weights": hp.choice("edge_mlp_weights", [
            [hp.choice('edge_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer3_3", [64, 128, 256, 512])]]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer3_3", [64, 128, 256, 512])]]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "prediction_args": hp.choice("prediction_args", [
            {"voting_members": 3}, {"voting_members": 1}, {"voting_members": 5}, {"voting_members": 7}
        ]),
        "pairwise_margin": hp.choice('parwise_margin', [0., 0.1, 0.2, 0.5, 0.7, 0.9, 1.5, 2.]),
        "cluster_args": hp.choice("cluster_args", [
            {"n_clusters": 10, "max_iter": 100, "batch_size": 500},
            {"n_clusters": 20, "max_iter": 100, "batch_size": 500},
            {"n_clusters": 5, "max_iter": 100, "batch_size": 500},
            {"n_clusters": 3, "max_iter": 100, "batch_size": 500},
            {"n_clusters": 50, "max_iter": 100, "batch_size": 500},
            {"n_clusters": 100, "max_iter": 100, "batch_size": 500}
        ])
    },
    "ukp_experimental_single_dep_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "timesteps": hp.choice("timesteps", [1, 2, 3, 4, 5, 6]),
        "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128, 256]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_layer1_1', [64, 128, 256, 512])],
            [hp.choice("answer_layer1_2", [64, 128, 256, 512]),
             hp.choice("answer_layer2_2", [64, 128, 256, 512])],
            [hp.choice("answer_layer1_3", [64, 128, 256, 512]),
             hp.choice("answer_layer2_3", [64, 128, 256, 512]),
             hp.choice("answer_layer3_3", [64, 128, 256, 512])]]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "edge_mlp_weights": hp.choice("edge_mlp_weights", [
            [hp.choice('edge_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("edge_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("edge_mlp_layer3_3", [64, 128, 256, 512])]]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("gate_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("gate_mlp_layer3_3", [64, 128, 256, 512])]]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]])
    },
    "ukp_experimental_single_char_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-02
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "timesteps": hp.choice("timesteps", [1, 2, 3, 4, 5, 6]),
        "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "answer_weights": hp.choice("answer_weights", [
            [hp.choice('answer_layer1_1', [64, 128, 256])],
            [hp.choice("answer_layer1_2", [64, 128, 256]),
             hp.choice("answer_layer2_2", [64, 128, 256])]]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256])],
        ]),
        "edge_mlp_weights": hp.choice("edge_mlp_weights", [
            [hp.choice('edge_mlp_layer1_1', [64, 128, 256])],
            [hp.choice("edge_mlp_layer1_2", [64, 128, 256]),
             hp.choice("edge_mlp_layer2_2", [64, 128, 256])],
        ]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256])],
        ]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256])],
        ]),
        "gate_mlp_weights": hp.choice("gate_mlp_weights", [
            [hp.choice('gate_mlp_layer1_1', [64, 128, 256])],
            [hp.choice("gate_mlp_layer1_2", [64, 128, 256]),
             hp.choice("gate_mlp_layer2_2", [64, 128, 256])],
        ]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256])],
        ]),
    },
    "ukp_experimental_single_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "embedding_dimension": hp.choice("embedding_dimension", [50]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "gcn_info": hp.choice("gcn_info", [
            {
                "0":
                    {
                        "node_weights": hp.choice("node_weights", [
                            [hp.choice('node_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("node_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("node_mlp_layer2_2", [64, 128, 256])]]),
                        "message_weights": hp.choice("message_weights", [
                            [hp.choice('message_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("message_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("message_mlp_layer2_2", [64, 128, 256])]]),
                        "aggregation_weights": hp.choice("aggregation_mlp_weights", [
                            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256])]]),
                        "pooling_weights": hp.choice("pooling_mlp_weights", [
                            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer2_2", [1])],
                            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer2_3", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer3_3", [1])]]),
                    }

            }
        ]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]]),
    },
    "ukp_experimental_single_pooled_adj_gnn_v2": {
        "gcn_info": hp.choice("gcn_info", [
            {
                "1": {
                    "message_weights": [
                        128
                    ],
                    "aggregation_weights": [
                        256,
                        64
                    ],
                    "node_weights": [
                        64
                    ],
                    "pooling_weights": [
                        256,
                        1
                    ]
                },
                "0":
                    {
                        "node_weights": hp.choice("node_weights", [
                            [hp.choice('node_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("node_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("node_mlp_layer2_2", [64, 128, 256])]]),
                        "message_weights": hp.choice("message_weights", [
                            [hp.choice('message_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("message_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("message_mlp_layer2_2", [64, 128, 256])]]),
                        "aggregation_weights": hp.choice("aggregation_mlp_weights", [
                            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256])]]),
                        "pooling_weights": hp.choice("pooling_mlp_weights", [
                            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer2_2", np.arange(5) + 2)],
                            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer2_3", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer3_3", np.arange(5) + 2)]]),
                    }

            }
        ]),
    },
    "ukp_experimental_single_pooled_reg_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "repeated_shared": hp.choice("repeated_shared", [False, True]),
        "timesteps": hp.choice("timesteps", [1, 2, 3, 4]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "pooling_mlp_weights": hp.choice("pooling_mlp_weights", [
            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_2", [1])],
            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer3_3", [1])]]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_message_weights": hp.choice("next_level_message_weights", [
            [hp.choice('next_level_message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_node_mlp_weights": hp.choice("next_level_node_mlp_weights", [
            [hp.choice('next_level_node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_pooling_weights": hp.choice("next_level_pooling_weights", [
            [hp.choice("next_level_pooling_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_pooling_mlp_layer2_2", np.arange(4) + 2)],
            [hp.choice("next_level_pooling_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_pooling_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_pooling_mlp_layer3_3", np.arange(4) + 2)]
        ]),
        "next_level_aggregation_mlp_weights": hp.choice("next_level_aggregation_mlp_weights", [
            [hp.choice('next_level_aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "target_mlp_weights": hp.choice("target_mlp_weights", [
            [hp.choice('target_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("target_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("target_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("target_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("target_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("target_mlp_layer3_3", [64, 128, 256, 512])]]),
    },
    "ukp_experimental_single_pooled_dep_adj_gnn_v2": {
        "meta_nodes_amount": hp.choice("meta_nodes_amount", [5, 10, 15, 20, 25, 30, 35]),
        "next_level_message_weights": hp.choice("next_level_message_weights", [
            [hp.choice('next_level_message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "pooling_weights": hp.choice("pooling_weights", [
            [hp.choice('pooling_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_aggregation_mlp_weights": hp.choice("next_level_aggregation_mlp_weights", [
            [hp.choice('next_level_aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
    },
    "ukp_experimental_single_tree_pooled_adj_gnn_v2": {
        # "optimizer_args": hp.choice("optimizer_args", [
        #     {
        #         "learning_rate": 1e-03
        #     }, {"learning_rate": 2e-04}, {
        #         "learning_rate": 2e-03
        #     }]),
        # "repeated_shared": hp.choice("repeated_shared", [False, True]),
        # "timesteps": hp.choice("timesteps", [1, 2, 3, 4]),
        # "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        # "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        # "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        # "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        # "node_mlp_weights": hp.choice("node_mlp_weights", [
        #     [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "message_weights": hp.choice("message_weights", [
        #     [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
        #     [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "pooling_mlp_weights": hp.choice("pooling_mlp_weights", [
        #     [hp.choice("pooling_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("pooling_mlp_layer2_2", [1])],
        #     [hp.choice("pooling_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("pooling_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("pooling_mlp_layer3_3", [1])]]),
        # "representation_mlp_weights": hp.choice("representation_mlp_weights", [
        #     [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "next_level_message_weights": hp.choice("next_level_message_weights", [
        #     [hp.choice('next_level_message_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("next_level_message_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("next_level_message_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("next_level_message_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_message_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_message_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "next_level_node_mlp_weights": hp.choice("next_level_node_mlp_weights", [
        #     [hp.choice('next_level_node_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("next_level_node_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("next_level_node_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("next_level_node_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_node_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_node_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "next_level_pooling_weights": hp.choice("next_level_pooling_weights", [
        #     [hp.choice("next_level_pooling_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("next_level_pooling_mlp_layer2_2", np.arange(5) + 2)],
        #     [hp.choice("next_level_pooling_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_pooling_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_pooling_mlp_layer3_3", np.arange(5) + 2)]
        # ]),
        # "next_level_aggregation_mlp_weights": hp.choice("next_level_aggregation_mlp_weights", [
        #     [hp.choice('next_level_aggregation_mlp_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("next_level_aggregation_mlp_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("next_level_aggregation_mlp_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("next_level_aggregation_mlp_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_aggregation_mlp_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("next_level_aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        # "out_of_span_coefficient": hp.uniform("out_of_span_coefficient", 1, 5.),
        # "contiguous_coefficient": hp.uniform("contiguous_coefficient", 1., 5.),
        # "alo_coefficient": hp.uniform("alo_coefficient", 1., 5.),
        # "separation_coefficient": hp.uniform("separation_coefficient", 1., 5.),
        # "minimal_size_coefficient": hp.uniform("minimal_size_coefficient", 1., 5.),
        # "overlapping_coefficient": hp.uniform("overlapping_coefficient", 1., 5.),
        # "overlapping_threshold": hp.choice("overlapping_threshold", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]),
        # "connectivity_threshold": hp.choice("connectivity_threshold", [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]),
        # "tree_size_threshold": hp.choice("tree_size_threshold", [0.5, 0.4, 0.3, 0.2]),
        # "tree_pooling_weights": hp.choice("tree_pooling_weights", [
        #     [hp.choice('tree_pooling_layer1_1', [64, 128, 256, 512])],
        #     [hp.choice("tree_pooling_layer1_2", [64, 128, 256, 512]),
        #      hp.choice("tree_pooling_layer2_2", [64, 128, 256, 512])],
        #     [hp.choice("tree_pooling_layer1_3", [64, 128, 256, 512]),
        #      hp.choice("tree_pooling_layer2_3", [64, 128, 256, 512]),
        #      hp.choice("tree_pooling_layer3_3", [64, 128, 256, 512])]]),
        "overlapping_threshold": hp.choice("overlapping_threshold", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]),
        "connectivity_threshold": hp.choice("connectivity_threshold", [0.2, 0.3, 0.4, 0.5]),
        "lambda_optimizer_args": hp.choice("lambda_optimizer_args", [
            {"learning_rate": 1e-03},
            {"learning_rate": 5e-03},
            {"learning_rate": 2e-04},
            {"learning_rate": 2e-03},
            {"learning_rate": 1e-02},
        ]),
    },

    "persuasive_essays_experimental_single_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {

                "learning_rate": 5e-03
            }, {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "gcn_info": {
            "0": {
                "message_weights": hp.choice("message_weights", [
                    [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
                "aggregation_weights": hp.choice("aggregation_mlp_weights", [
                    [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
                "node_weights": hp.choice("node_mlp_weights", [
                    [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
                "pooling_weights": hp.choice("pooling_weights_1", [
                    [hp.choice("next_level_pooling_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer2_2", [1])],
                    [hp.choice("next_level_pooling_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer3_3", [1])]
                ]),
            },
        },
    },
    "persuasive_essays_experimental_single_pooled_adj_gnn_v2": {
        "gcn_info": {
            "1": {
                "message_weights": [
                    64,
                    512,
                    256
                ],
                "aggregation_weights": [
                    512,
                    256,
                    64
                ],
                "node_weights": [
                    128
                ],
                "pooling_weights": [
                    64,
                    1
                ]
            },
            "0": {
                "message_weights": hp.choice("message_weights", [
                    [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
                "aggregation_weights": hp.choice("aggregation_mlp_weights", [
                    [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
                "node_weights": hp.choice("node_mlp_weights", [
                    [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
                "pooling_weights": hp.choice("pooling_weights_1", [
                    [hp.choice("next_level_pooling_mlp_layer1_2", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer2_2", np.arange(7) + 1)],
                    [hp.choice("next_level_pooling_mlp_layer1_3", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer2_3", [64, 128, 256, 512]),
                     hp.choice("next_level_pooling_mlp_layer3_3", np.arange(7) + 1)]
                ]),
            },
        },
    },
    "persuasive_essays_experimental_single_pooled_dep_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "repeated_shared": hp.choice("repeated_shared", [True]),
        "timesteps": hp.choice("timesteps", [1, 2, 3]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200, 300]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "node_mlp_weights": hp.choice("node_mlp_weights", [
            [hp.choice('node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "message_weights": hp.choice("message_weights", [
            [hp.choice('message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "aggregation_mlp_weights": hp.choice("aggregation_mlp_weights", [
            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "pooling_mlp_weights": hp.choice("pooling_mlp_weights", [
            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_2", [1])],
            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("pooling_mlp_layer3_3", [1])]]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_message_weights": hp.choice("next_level_message_weights", [
            [hp.choice('next_level_message_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_message_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_message_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_message_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_node_mlp_weights": hp.choice("next_level_node_mlp_weights", [
            [hp.choice('next_level_node_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_node_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_node_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_node_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_node_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_node_mlp_layer3_3", [64, 128, 256, 512])]]),
        "next_level_pooling_weights": hp.choice("next_level_pooling_weights", [
            [hp.choice("next_level_pooling_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_pooling_mlp_layer2_2", np.arange(4) + 2)],
            [hp.choice("next_level_pooling_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_pooling_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_pooling_mlp_layer3_3", np.arange(4) + 2)]
        ]),
        "next_level_aggregation_mlp_weights": hp.choice("next_level_aggregation_mlp_weights", [
            [hp.choice('next_level_aggregation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("next_level_aggregation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("next_level_aggregation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("next_level_aggregation_mlp_layer3_3", [64, 128, 256, 512])]]),
    },
    "persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2": {
        "overlapping_threshold": hp.choice("overlapping_threshold", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]),
        "connectivity_threshold": hp.choice("connectivity_threshold", [0.2, 0.3, 0.4, 0.5]),
        # "lambda_optimizer_args": hp.choice("lambda_optimizer_args", [
        #     {"learning_rate": 1e-03},
        #     {"learning_rate": 5e-03},
        #     {"learning_rate": 2e-04},
        #     {"learning_rate": 2e-03},
        #     {"learning_rate": 1e-02},
        # ]),
        # "next_level_pooling_weights": hp.choice("next_level_pooling_weights", [
        #     [hp.choice("next_level_pooling_mlp_layer1_3", [256]),
        #      hp.choice("next_level_pooling_mlp_layer2_3", [512]),
        #      hp.choice("next_level_pooling_mlp_layer3_3", np.arange(7) + 2)]
        # ]),
    },

    "abst_rct_experimental_single_adj_gnn_v2": {
        "optimizer_args": hp.choice("optimizer_args", [
            {
                "learning_rate": 1e-03
            }, {"learning_rate": 2e-04}, {
                "learning_rate": 2e-03
            }]),
        "embedding_dimension": hp.choice("embedding_dimension", [50, 100, 200]),
        "l2_regularization": hp.choice("l2_regularization", [0.0, 1e-05, 1e-04, 2e-04, 2e-05]),
        "dropout_rate": hp.choice("dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "input_dropout_rate": hp.choice("input_dropout_rate", [0.2, 0.3, 0.4, 0.5]),
        "gcn_info": hp.choice("gcn_info", [
            {
                "0":
                    {
                        "node_weights": hp.choice("node_weights", [
                            [hp.choice('node_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("node_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("node_mlp_layer2_2", [64, 128, 256])]]),
                        "message_weights": hp.choice("message_weights", [
                            [hp.choice('message_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("message_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("message_mlp_layer2_2", [64, 128, 256])]]),
                        "aggregation_weights": hp.choice("aggregation_mlp_weights", [
                            [hp.choice('aggregation_mlp_layer1_1', [64, 128, 256])],
                            [hp.choice("aggregation_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("aggregation_mlp_layer2_2", [64, 128, 256])]]),
                        "pooling_weights": hp.choice("pooling_mlp_weights", [
                            [hp.choice("pooling_mlp_layer1_2", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer2_2", [1])],
                            [hp.choice("pooling_mlp_layer1_3", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer2_3", [64, 128, 256]),
                             hp.choice("pooling_mlp_layer3_3", [1])]]),
                    }

            }
        ]),
        "representation_mlp_weights": hp.choice("representation_mlp_weights", [
            [hp.choice('representation_mlp_layer1_1', [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_2", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_2", [64, 128, 256, 512])],
            [hp.choice("representation_mlp_layer1_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer2_3", [64, 128, 256, 512]),
             hp.choice("representation_mlp_layer3_3", [64, 128, 256, 512])]]),
    },
    "abst_rct_experimental_single_pooled_adj_gnn_v2": {
        "gcn_info": {
            "1": {
                "aggregation_weights": [
                    128,
                    256
                ],
                "pooling_weights": [
                    256,
                    1
                ],
                "node_weights": [
                    64,
                    256
                ],
                "message_weights": [
                    128,
                    64
                ]
            },
            "0": {
                "message_weights": hp.choice("message_weights_0", [
                    [hp.choice('message_0_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("message_0_layer1_2", [64, 128, 256, 512]),
                     hp.choice("message_0_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("message_0_layer1_3", [64, 128, 256, 512]),
                     hp.choice("message_0_layer2_3", [64, 128, 256, 512]),
                     hp.choice("message_0_layer3_3", [64, 128, 256, 512])]]),
                "aggregation_weights": hp.choice("aggregation_mlp_weights_0", [
                    [hp.choice('aggregation_0_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("aggregation_0_layer1_2", [64, 128, 256, 512]),
                     hp.choice("aggregation_0_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("aggregation_0_layer1_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_0_layer2_3", [64, 128, 256, 512]),
                     hp.choice("aggregation_0_layer3_3", [64, 128, 256, 512])]]),
                "node_weights": hp.choice("node_mlp_weights_0", [
                    [hp.choice('node_0_layer1_1', [64, 128, 256, 512])],
                    [hp.choice("node_0_layer1_2", [64, 128, 256, 512]),
                     hp.choice("node_0_layer2_2", [64, 128, 256, 512])],
                    [hp.choice("node_0_layer1_3", [64, 128, 256, 512]),
                     hp.choice("node_0_layer2_3", [64, 128, 256, 512]),
                     hp.choice("node_0_layer3_3", [64, 128, 256, 512])]]),
                "pooling_weights": hp.choice("pooling_weights_0", [
                    [hp.choice("pooling_0_layer1_2", [64, 128, 256, 512]),
                     hp.choice("pooling_0_layer2_2", np.arange(10) + 15)],
                    [hp.choice("pooling_0_layer1_3", [64, 128, 256, 512]),
                     hp.choice("pooling_0_layer2_3", [64, 128, 256, 512]),
                     hp.choice("pooling_0_layer3_3", np.arange(5) + 2)]
                ]),
            }
        },
    },
    "abst_rct_experimental_single_tree_pooled_adj_gnn_v2": {
        "overlapping_threshold": hp.choice("overlapping_threshold", [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]),
        "connectivity_threshold": hp.choice("connectivity_threshold", [0.2, 0.3, 0.4, 0.5]),
        "lambda_optimizer_args": hp.choice("lambda_optimizer_args", [
            {"learning_rate": 1e-03},
            {"learning_rate": 5e-03},
            {"learning_rate": 2e-04},
            {"learning_rate": 2e-03},
            {"learning_rate": 1e-02},
        ]),
    }
}
