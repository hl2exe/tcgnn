"""

Simple wrappers for each dataset example

"""

from collections import OrderedDict

import numpy as np
import tensorflow as tf

from utility.embedding_utils import convert_number_to_binary_list
from utility.graph_utils import retrieve_tree_info, retrieve_dep_tree_info, retrieve_hierarchical_tree_info, \
    get_most_frequent_edges_to_truncate, retrieve_tree_info_adj, build_assignment_matrix, \
    retrieve_hierarchical_tree_adj_info, retrieve_dep_tree_info_adj, retrieve_hierarchical_dep_tree_info, \
    retrieve_hierarchical_dep_tree_adj_info, build_node_spans
from utility.wrapping_utils import create_int_feature, create_text_feature


# Examples


class TextExample(object):

    def __init__(self, guid, text, label=None):
        self.guid = guid
        self.text = text
        self.label = label

    def get_data(self):
        return self.text


class PosTextExample(TextExample):

    def __init__(self, position, **kwargs):
        super(PosTextExample, self).__init__(**kwargs)
        self.position = position


class TextGraphExample(object):

    def __init__(self, guid, graph, label=None):
        self.guid = guid
        self.graph = graph
        self.label = label

    def get_data(self):
        return self.graph


class TextMultiGraphExample(object):

    def __init__(self, guid, graph_1, graph_2, label=None):
        self.guid = guid
        self.graph_1 = graph_1
        self.graph_2 = graph_2
        self.label = label

    def get_data(self):
        return self.graph_1, self.graph_2


class TextDualExample(object):

    def __init__(self, guid, graph, text, label=None):
        self.guid = guid
        self.graph = graph
        self.text = text
        self.label = label

    def get_data(self):
        return self.graph


class PosTextDualExample(TextDualExample):

    def __init__(self, position, **kwargs):
        super(PosTextDualExample, self).__init__(**kwargs)
        self.position = position


class PosTextGraphExample(TextGraphExample):

    def __init__(self, position, **kwargs):
        super(PosTextGraphExample, self).__init__(**kwargs)
        self.position = position


class TextKBExample(TextExample):

    def __init__(self, kb, **kwargs):
        super(TextKBExample, self).__init__(**kwargs)
        self.kb = kb

    def get_data(self):
        return self.text + ' ' + self.kb


class TextKBSupervisionExample(TextKBExample):

    def __init__(self, targets, **kwargs):
        super(TextKBSupervisionExample, self).__init__(**kwargs)
        self.targets = targets


# Example List Wrappers

class TextExampleList(object):

    def __init__(self):
        self.content = []

    def __iter__(self):
        return self.content.__iter__()

    def append(self, item):
        self.content.append(item)

    def __len__(self):
        return len(self.content)

    def __getitem__(self, item):
        return self.content[item]

    def get_data(self):
        return [item.text for item in self.content]


class TextGraphExampleList(TextExampleList):

    def get_data(self):
        return [item.graph for item in self.content]


class TextMultiGraphExampleList(TextExampleList):

    def get_data(self):
        data = [item.get_data() for item in self.content]
        return [item for group in data for item in group]


class TextKBExampleList(TextExampleList):

    def get_data(self):
        texts = super(TextKBExampleList, self).get_data()
        return texts + [self.content[0].kb]


# Features


class Features(object):

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        raise NotImplementedError()

    @classmethod
    def get_feature_records(cls, feature):
        raise NotImplementedError()

    @classmethod
    def get_dataset_selector(cls):
        raise NotImplementedError()

    @classmethod
    def _convert_labels(cls, example_label, label_list, has_labels=True, converter_args=None):

        label_map = {}
        if label_list is not None or has_labels:
            for (i, label) in enumerate(label_list):
                hot_encoding = [0] * len(label_list)
                hot_encoding[i] = 1
                label_map[label] = hot_encoding

        if not converter_args['is_multilabel']:
            if has_labels:
                if type(example_label) is list:
                    label_id = example_label
                else:
                    label_id = label_map[example_label]
            else:
                label_id = None
        else:
            if has_labels:
                if type(example_label) is not list:
                    label_id = label_map[example_label]
                else:
                    label_id = example_label
            else:
                label_id = None

        return label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args,
                     has_labels=True, converter_args=None):
        raise NotImplementedError()

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        raise NotImplementedError()


class TextFeatures(Features):

    def __init__(self, text_ids, label_id=None):
        self.text_ids = text_ids
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_seq_length = conversion_args['max_seq_length']
        num_labels = conversion_args['num_labels']

        mappings = dict()
        mappings['text_ids'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = OrderedDict()
        features['text_ids'] = create_int_feature(feature.text_ids)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'text': record['text_ids'],
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        tokens = tokenizer.tokenize(example.text)
        text_ids = tokenizer.convert_tokens_to_ids(tokens)

        return text_ids, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        text_ids, label_id, label_map = cls.convert_example(example,
                                                            label_list,
                                                            tokenizer,
                                                            has_labels=has_labels,
                                                            converter_args=converter_args
                                                            )

        # Padding
        text_ids += [0] * (max_seq_length - len(text_ids))
        text_ids = text_ids[:max_seq_length]

        assert len(text_ids) == max_seq_length

        feature = cls(text_ids=text_ids, label_id=label_id)
        return feature


class TextELMOFeatures(TextFeatures):

    def __init__(self, text, label_id=None):
        self.text = text
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        num_labels = conversion_args['num_labels']

        mappings = dict()
        mappings['text'] = tf.io.FixedLenFeature([], tf.string)

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):

        features = OrderedDict()
        features['text'] = create_text_feature(feature.text)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'text': record['text'],
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        return example.text, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        text, label_id, label_map = cls.convert_example(example,
                                                        label_list,
                                                        tokenizer,
                                                        has_labels=has_labels,
                                                        converter_args=converter_args
                                                        )

        feature = cls(text=text, label_id=label_id)
        return feature


class PosTextFeatures(TextFeatures):

    def __init__(self, position, **kwargs):
        super(PosTextFeatures, self).__init__(**kwargs)
        self.position = position

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(PosTextFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                            has_labels=has_labels)
        mappings['position'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(PosTextFeatures, cls).get_feature_records(feature=feature)
        features['position'] = create_int_feature([feature.position])

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'text': record['text_ids'],
                'position': record['position']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, PosTextExample):
            raise AttributeError('Expected PosTextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        text_ids, label_id, label_map = cls.convert_example(example,
                                                            label_list,
                                                            tokenizer,
                                                            has_labels=has_labels,
                                                            converter_args=converter_args
                                                            )

        # Padding
        text_ids += [0] * (max_seq_length - len(text_ids))
        text_ids = text_ids[:max_seq_length]

        assert len(text_ids) == max_seq_length

        feature = cls(text_ids=text_ids, label_id=label_id, position=example.position)
        return feature


class TextGraphFeatures(Features):

    def __init__(self, node_ids, edge_features, edge_indices,
                 edge_mask, node_indices, node_segments, directionality_mask, label_id=None):
        self.node_ids = node_ids
        self.edge_features = edge_features
        self.edge_indices = edge_indices
        self.edge_mask = edge_mask
        self.node_indices = node_indices
        self.node_segments = node_segments
        self.directionality_mask = directionality_mask
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        num_labels = conversion_args['num_labels']

        mappings = {
            'node_ids': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
            # 'edge_features': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'edge_indices': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'edge_mask': tf.io.FixedLenFeature([max_graph_edges], tf.int64),
            'node_indices': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'node_segments': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'directionality_mask': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):

        features = OrderedDict()
        features['node_ids'] = create_int_feature(feature.node_ids)
        # features['edge_features'] = create_int_feature(feature.edge_features)
        features['edge_indices'] = create_int_feature(feature.edge_indices)
        features['edge_mask'] = create_int_feature(feature.edge_mask)
        features['node_indices'] = create_int_feature(feature.node_indices)
        features['node_segments'] = create_int_feature(feature.node_segments)
        features['directionality_mask'] = create_int_feature(feature.directionality_mask)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                # 'edge_features': record['edge_features'],
                'edge_indices': record['edge_indices'],
                'edge_mask': record['edge_mask'],
                'node_indices': record['node_indices'],
                'node_segments': record['node_segments'],
                'directionality_mask': record['directionality_mask']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask, leaf_indicator = retrieve_tree_info(example.graph,
                                                                 is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        # edge features as : depth and depth-first position
        depth_ids = np.array(edge_features) + 1
        depth_ids = depth_ids.tolist()
        position_ids = np.arange(len(edge_features)) + 1
        position_ids = position_ids.tolist()
        edge_features_ids = [[depth_id, position_id] for depth_id, position_id in zip(depth_ids, position_ids)]

        # edge_features_ids = [convert_number_to_binary_list(features) for features in edge_features]

        return node_ids, edge_indices, edge_features_ids, node_indices, \
               node_segments, directionality_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        max_graph_depth = conversion_args['max_graph_depth']

        truncated = False

        node_ids, edge_indices, edge_features, \
        node_indices, node_segments, directionality_mask, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_edges, 2
        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges, max_graph_depth
        # edge_features = list(map(
        #     lambda item: [0] * (max_graph_depth - len(item)) + item
        #     if max_graph_depth - len(item) > 0 else item[:max_graph_depth],
        #     edge_features))
        # Set to 0 indices that are about maximum values
        edge_features = [[item[0] if item[0] <= max_graph_depth else 0,
                          item[1] if item[1] <= max_graph_edges - 1 else 0] for item in edge_features]

        if not truncated:
            # edge_features = edge_features + [[0] * max_graph_depth for _ in range(max_graph_edges - len(edge_features))]
            edge_features += [[0, 0] for _ in range(pad_edges_amount)]

        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1, (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        assert len(node_ids) == max_graph_nodes
        assert len(edge_features) == max_graph_edges
        assert len(edge_indices) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        # Flatten
        edge_features = np.array(edge_features).flatten()
        edge_indices = np.array(edge_indices).flatten()

        feature = cls(node_ids=node_ids, edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, directionality_mask=directionality_mask)
        return feature


class TextDualGraphFeatures(TextGraphFeatures):

    def __init__(self, subtree_indices, subtree_indices_mask, **kwargs):
        super(TextDualGraphFeatures, self).__init__(**kwargs)
        self.subtree_indices = subtree_indices
        self.subtree_indices_mask = subtree_indices_mask

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextDualGraphFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                  has_labels=has_labels)

        max_graph_subtree_size = conversion_args['max_graph_subtree_size']

        mappings['subtree_indices'] = tf.io.FixedLenFeature([2 * max_graph_subtree_size],
                                                            tf.int64)
        mappings['subtree_indices_mask'] = tf.io.FixedLenFeature([2 * max_graph_subtree_size],
                                                                 tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextDualGraphFeatures, cls).get_feature_records(feature=feature)

        features['subtree_indices'] = create_int_feature(feature.subtree_indices)
        features['subtree_indices_mask'] = create_int_feature(feature.subtree_indices_mask)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                'edge_features': record['edge_features'],
                'edge_indices': record['edge_indices'],
                'edge_mask': record['edge_mask'],
                'node_indices': record['node_indices'],
                'node_segments': record['node_segments'],
                'directionality_mask': record['directionality_mask'],
                'subtree_indices': record['subtree_indices'],
                'subtree_indices_mask': record['subtree_indices_mask']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask, leaf_indicator = retrieve_tree_info(example.graph,
                                                                 is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))
        node_ids_indices = np.arange(len(node_ids))

        # edge features as : depth and depth-first position
        depth_ids = np.array(edge_features) + 1
        depth_ids = depth_ids.tolist()
        position_ids = np.arange(len(edge_features)) + 1
        position_ids = position_ids.tolist()
        edge_features_ids = [[depth_id, position_id] for depth_id, position_id in zip(depth_ids, position_ids)]

        subtree_indices = [[node_id for node_id, is_leaf in zip(node_ids_indices, leaf_indicator) if not is_leaf],
                           [node_id for node_id, is_leaf in zip(node_ids_indices, leaf_indicator) if is_leaf]]

        # edge_features_ids = [convert_number_to_binary_list(features) for features in edge_features]

        return node_ids, edge_indices, edge_features_ids, node_indices, \
               node_segments, directionality_mask, subtree_indices, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        max_graph_depth = conversion_args['max_graph_depth']

        max_graph_subtree_size = conversion_args['max_graph_subtree_size']

        truncated = False

        node_ids, edge_indices, edge_features, \
        node_indices, node_segments, directionality_mask, subtree_indices, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True
            truncated_node_ids = node_ids[max_graph_nodes:]

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_edges, 2
        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges, max_graph_depth
        # edge_features = list(map(
        #     lambda item: [0] * (max_graph_depth - len(item)) + item
        #     if max_graph_depth - len(item) > 0 else item[:max_graph_depth],
        #     edge_features))
        # Set to 0 indices that are about maximum values
        edge_features = [[item[0] if item[0] <= max_graph_depth else 0,
                          item[1] if item[1] <= max_graph_edges - 1 else 0] for item in edge_features]

        if not truncated:
            # edge_features = edge_features + [[0] * max_graph_depth for _ in range(max_graph_edges - len(edge_features))]
            edge_features += [[0, 0] for _ in range(pad_edges_amount)]

        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1, (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        subtree_indices_mask = [[1] * len(seq) for seq in subtree_indices]
        if truncated:
            subtree_indices = [[item for item in seq if item not in truncated_node_ids]
                               for seq in subtree_indices]
            subtree_indices_mask = [[1] * max_graph_subtree_size for _ in subtree_indices_mask]

        subtree_indices = [seq + [0] * (max_graph_subtree_size - len(seq)) if len(seq) < max_graph_subtree_size
                           else seq[:max_graph_subtree_size]
                           for seq in subtree_indices]
        subtree_indices_mask = [seq + [0] * (max_graph_subtree_size - len(seq)) if len(seq) < max_graph_subtree_size
                                else seq[:max_graph_subtree_size]
                                for seq in subtree_indices_mask]

        assert len(node_ids) == max_graph_nodes
        assert len(edge_features) == max_graph_edges
        assert len(edge_indices) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        assert len(subtree_indices) == 2
        assert len(subtree_indices_mask) == 2

        # Flatten
        edge_features = np.array(edge_features).flatten()
        edge_indices = np.array(edge_indices).flatten()
        subtree_indices = np.array(subtree_indices).flatten()
        subtree_indices_mask = np.array(subtree_indices_mask).flatten()

        assert len(subtree_indices) == 2 * max_graph_subtree_size
        assert len(subtree_indices_mask) == 2 * max_graph_subtree_size

        feature = cls(node_ids=node_ids, edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, directionality_mask=directionality_mask,
                      subtree_indices=subtree_indices, subtree_indices_mask=subtree_indices_mask)
        return feature


class TextSimpleGraphFeatures(Features):

    def __init__(self, node_ids, label_id=None):
        self.node_ids = node_ids
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_graph_nodes = conversion_args['max_graph_nodes']
        num_labels = conversion_args['num_labels']

        mappings = {
            'node_ids': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):

        features = OrderedDict()
        features['node_ids'] = create_int_feature(feature.node_ids)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask, leaf_indicator = retrieve_tree_info(example.graph,
                                                                 is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        node_ids, label_id, label_map = cls.convert_example(example,
                                                            label_list,
                                                            tokenizer,
                                                            has_labels=has_labels,
                                                            converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        assert len(node_ids) == max_graph_nodes

        feature = cls(node_ids=node_ids, label_id=label_id)
        return feature


class TextSimpleDepGraphFeatures(TextSimpleGraphFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes = retrieve_dep_tree_info(example.graph,
                                       is_directional=converter_args['is_directional'])[0]

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, label_id, label_map


class PosTextSimpleGraphFeatures(TextSimpleGraphFeatures):

    def __init__(self, position, **kwargs):
        super(PosTextSimpleGraphFeatures, self).__init__(**kwargs)
        self.position = position

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(PosTextSimpleGraphFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                       has_labels=has_labels)
        mappings['position'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(PosTextSimpleGraphFeatures, cls).get_feature_records(feature=feature)
        features['position'] = create_int_feature([feature.position])

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                'position': record['position']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, PosTextGraphExample):
            raise AttributeError('Expected PosTextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        node_ids, label_id, label_map = cls.convert_example(example,
                                                            label_list,
                                                            tokenizer,
                                                            has_labels=has_labels,
                                                            converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        assert len(node_ids) == max_graph_nodes

        feature = cls(node_ids=node_ids, label_id=label_id, position=example.position)
        return feature


class PosTextSimpleDepGraphFeatures(PosTextSimpleGraphFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes = retrieve_dep_tree_info(example.graph,
                                       is_directional=converter_args['is_directional'])[0]

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, label_id, label_map


class TextSimpleDualGraphFeatures(TextSimpleGraphFeatures):

    def __init__(self, structure_node_ids, **kwargs):
        super(TextSimpleDualGraphFeatures, self).__init__(**kwargs)
        self.structure_node_ids = structure_node_ids

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_graph_nodes = conversion_args['max_graph_nodes']
        num_labels = conversion_args['num_labels']

        mappings = {
            'sentence_node_ids': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
            'structure_node_ids': tf.io.FixedLenFeature([max_graph_nodes], tf.int64)
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):

        features = OrderedDict()
        features['sentence_node_ids'] = create_int_feature(feature.node_ids)
        features['structure_node_ids'] = create_int_feature(feature.structure_node_ids)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'sentence_node_ids': record['sentence_node_ids'],
                'structure_node_ids': record['structure_node_ids']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask, leaf_indicator = retrieve_tree_info(example.graph,
                                                                 is_directional=converter_args['is_directional'])
        structure_nodes = [item for item, is_leaf in zip(nodes, leaf_indicator) if not is_leaf]
        sentence_nodes = [item for item, is_leaf in zip(nodes, leaf_indicator) if is_leaf]

        structure_nodes = tokenizer.convert_tokens_to_ids(' '.join(structure_nodes))
        sentence_nodes = tokenizer.convert_tokens_to_ids(' '.join(sentence_nodes))

        return structure_nodes, sentence_nodes, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        structure_nodes, sentence_nodes, label_id, label_map = cls.convert_example(example,
                                                                                   label_list,
                                                                                   tokenizer,
                                                                                   has_labels=has_labels,
                                                                                   converter_args=converter_args)

        # Padding

        # max_graph_nodes
        structure_nodes = structure_nodes + [0] * (max_graph_nodes - len(structure_nodes))
        structure_nodes = structure_nodes[:max_graph_nodes]

        sentence_nodes = sentence_nodes + [0] * (max_graph_nodes - len(sentence_nodes))
        sentence_nodes = sentence_nodes[:max_graph_nodes]

        assert np.sum(sentence_nodes) > 0
        assert np.sum(structure_nodes) > 0
        assert len(structure_nodes) == max_graph_nodes
        assert len(sentence_nodes) == max_graph_nodes

        feature = cls(node_ids=sentence_nodes, structure_node_ids=structure_nodes, label_id=label_id)
        return feature


class TextSimpleDualDepGraphFeatures(TextSimpleDualGraphFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask, leaf_indicator = retrieve_dep_tree_info(example.graph,
                                                                     is_directional=converter_args['is_directional'])
        structure_nodes = [item for item, is_leaf in zip(nodes, leaf_indicator) if not is_leaf]
        sentence_nodes = [item for item, is_leaf in zip(nodes, leaf_indicator) if is_leaf]

        structure_nodes = tokenizer.convert_tokens_to_ids(' '.join(structure_nodes))
        sentence_nodes = tokenizer.convert_tokens_to_ids(' '.join(sentence_nodes))

        return structure_nodes, sentence_nodes, label_id, label_map


class PosTextSimpleDualGraphFeatures(TextSimpleDualGraphFeatures):

    def __init__(self, position, **kwargs):
        super(PosTextSimpleDualGraphFeatures, self).__init__(**kwargs)
        self.position = position

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(PosTextSimpleDualGraphFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                           has_labels=has_labels)
        mappings['position'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(PosTextSimpleDualGraphFeatures, cls).get_feature_records(feature=feature)
        features['position'] = create_int_feature([feature.position])

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'sentence_node_ids': record['sentence_node_ids'],
                'structure_node_ids': record['structure_node_ids'],
                'position': record['position']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, PosTextGraphExample):
            raise AttributeError('Expected PosTextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        structure_nodes, sentence_nodes, label_id, label_map = cls.convert_example(example,
                                                                                   label_list,
                                                                                   tokenizer,
                                                                                   has_labels=has_labels,
                                                                                   converter_args=converter_args)

        # Padding

        # max_graph_nodes
        structure_nodes = structure_nodes + [0] * (max_graph_nodes - len(structure_nodes))
        structure_nodes = structure_nodes[:max_graph_nodes]

        sentence_nodes = sentence_nodes + [0] * (max_graph_nodes - len(sentence_nodes))
        sentence_nodes = sentence_nodes[:max_graph_nodes]

        assert len(structure_nodes) == max_graph_nodes
        assert len(sentence_nodes) == max_graph_nodes

        feature = cls(node_ids=sentence_nodes, structure_node_ids=structure_nodes, label_id=label_id,
                      position=example.position)
        return feature


class TextGraphAdjFeatures(Features):

    def __init__(self, guid, node_ids,
                 # edge_features,
                 adjacency_matrix, label_id=None):
        self.guid = guid
        self.node_ids = node_ids
        # self.edge_features = edge_features
        self.adjacency_matrix = adjacency_matrix
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_graph_nodes = conversion_args['max_graph_nodes']
        num_labels = conversion_args['num_labels']

        mappings = {
            'guid': tf.io.FixedLenFeature([], tf.int64),
            'node_ids': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
            # 'edge_features': tf.io.FixedLenFeature([max_graph_nodes * max_graph_nodes], tf.int64),
            'adjacency_matrix': tf.io.FixedLenFeature([max_graph_nodes * max_graph_nodes], tf.int64),
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):

        features = OrderedDict()
        features['guid'] = create_int_feature([feature.guid])
        features['node_ids'] = create_int_feature(feature.node_ids)
        # features['edge_features'] = create_int_feature(feature.edge_features)
        features['adjacency_matrix'] = create_int_feature(feature.adjacency_matrix)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids': record['node_ids'],
                # 'edge_features': record['edge_features'],
                'adjacency_matrix': record['adjacency_matrix'],
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix = retrieve_tree_info_adj(example.graph,
                                                  is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, edge_features, \
               adjacency_matrix, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        truncated = False

        node_ids, edge_features, \
        adjacency_matrix, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_nodes, max_graph_depth
        # if not truncated:
        #     padded_edge_features = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
        #     padded_edge_features[:original_node_amount, :original_node_amount] = edge_features
        # else:
        #     padded_edge_features = np.array(edge_features, dtype=np.int32).reshape(original_node_amount, original_node_amount)
        #     padded_edge_features = padded_edge_features[:max_graph_nodes, :max_graph_nodes]
        #
        # padded_edge_features = padded_edge_features.ravel()

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()

        assert len(node_ids) == max_graph_nodes
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes

        feature = cls(node_ids=node_ids, guid=example.guid,
                      adjacency_matrix=padded_adjacency_matrix, label_id=label_id)
        return feature


class TextMultiGraphAdjFeatures(Features):

    def __init__(self, guid, node_ids_1, node_ids_2,
                 # edge_features,
                 adjacency_matrix_1, adjacency_matrix_2, label_id=None):
        self.guid = guid
        self.node_ids_1 = node_ids_1
        self.node_ids_2 = node_ids_2
        # self.edge_features = edge_features
        self.adjacency_matrix_1 = adjacency_matrix_1
        self.adjacency_matrix_2 = adjacency_matrix_2
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_graph_nodes = conversion_args['max_graph_nodes']
        num_labels = conversion_args['num_labels']

        mappings = {
            'guid': tf.io.FixedLenFeature([], tf.int64),
            'node_ids_1': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
            'node_ids_2': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
            # 'edge_features': tf.io.FixedLenFeature([max_graph_nodes * max_graph_nodes], tf.int64),
            'adjacency_matrix_1': tf.io.FixedLenFeature([max_graph_nodes * max_graph_nodes], tf.int64),
            'adjacency_matrix_2': tf.io.FixedLenFeature([max_graph_nodes * max_graph_nodes], tf.int64),
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):

        features = OrderedDict()
        features['guid'] = create_int_feature([feature.guid])
        features['node_ids_1'] = create_int_feature(feature.node_ids_1)
        features['node_ids_2'] = create_int_feature(feature.node_ids_2)
        # features['edge_features'] = create_int_feature(feature.edge_features)
        features['adjacency_matrix_1'] = create_int_feature(feature.adjacency_matrix_1)
        features['adjacency_matrix_2'] = create_int_feature(feature.adjacency_matrix_2)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids_1': record['node_ids_1'],
                'node_ids_2': record['node_ids_2'],
                # 'edge_features': record['edge_features'],
                'adjacency_matrix_1': record['adjacency_matrix_1'],
                'adjacency_matrix_2': record['adjacency_matrix_2'],
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Graph 1
        nodes_1, edge_features_1, \
        adjacency_matrix_1 = retrieve_tree_info_adj(example.graph_1,
                                                    is_directional=converter_args['is_directional'])

        node_ids_1 = tokenizer.convert_tokens_to_ids(' '.join(nodes_1))

        # Graph 2
        nodes_2, edge_features_2, \
        adjacency_matrix_2 = retrieve_tree_info_adj(example.graph_2,
                                                    is_directional=converter_args['is_directional'])

        node_ids_2 = tokenizer.convert_tokens_to_ids(' '.join(nodes_2))

        return node_ids_1, edge_features_1, adjacency_matrix_1, \
               node_ids_2, edge_features_2, adjacency_matrix_2, \
               label_id, label_map

    @classmethod
    def pad_graph(cls, max_graph_nodes, node_ids, edge_features, adjacency_matrix):
        truncated = False

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_nodes, max_graph_depth
        # if not truncated:
        #     padded_edge_features = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
        #     padded_edge_features[:original_node_amount, :original_node_amount] = edge_features
        # else:
        #     padded_edge_features = np.array(edge_features, dtype=np.int32).reshape(original_node_amount, original_node_amount)
        #     padded_edge_features = padded_edge_features[:max_graph_nodes, :max_graph_nodes]
        #
        # padded_edge_features = padded_edge_features.ravel()

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()

        assert len(node_ids) == max_graph_nodes
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes

        return node_ids, padded_adjacency_matrix

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextMultiGraphExample):
            raise AttributeError('Expected TextMultiGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        node_ids_1, edge_features_1, adjacency_matrix_1, \
        node_ids_2, edge_features_2, adjacency_matrix_2, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        node_ids_1, adjacency_matrix_1 = cls.pad_graph(max_graph_nodes=max_graph_nodes,
                                                       node_ids=node_ids_1,
                                                       edge_features=edge_features_1,
                                                       adjacency_matrix=adjacency_matrix_1)

        node_ids_2, adjacency_matrix_2 = cls.pad_graph(max_graph_nodes=max_graph_nodes,
                                                       node_ids=node_ids_2,
                                                       edge_features=edge_features_2,
                                                       adjacency_matrix=adjacency_matrix_2)

        feature = cls(guid=example.guid, node_ids_1=node_ids_1, adjacency_matrix_1=adjacency_matrix_1,
                      node_ids_2=node_ids_2, adjacency_matrix_2=adjacency_matrix_2, label_id=label_id)
        return feature


class TextDualGraphAdjFeatures(TextGraphAdjFeatures):

    def __init__(self, text_ids, **kwargs):
        super(TextDualGraphAdjFeatures, self).__init__(**kwargs)
        self.text_ids = text_ids

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextDualGraphAdjFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                     has_labels=has_labels)

        max_seq_length = conversion_args['max_seq_length']
        mappings['text_ids'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextDualGraphAdjFeatures, cls).get_feature_records(feature)
        features['text_ids'] = create_int_feature(feature.text_ids)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids': record['node_ids'],
                'text_ids': record['text_ids'],
                'adjacency_matrix': record['adjacency_matrix'],
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix = retrieve_tree_info_adj(example.graph,
                                                  is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))
        text_tokens = tokenizer.tokenize(example.text)
        text_ids = tokenizer.convert_tokens_to_ids(text_tokens)

        return text_ids, node_ids, edge_features, \
               adjacency_matrix, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextDualExample):
            raise AttributeError('Expected TextDualExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_seq_length = conversion_args['max_seq_length']

        truncated = False

        text_ids, node_ids, edge_features, \
        adjacency_matrix, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # Padding
        text_ids += [0] * (max_seq_length - len(text_ids))
        text_ids = text_ids[:max_seq_length]

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()

        assert len(node_ids) == max_graph_nodes
        assert len(text_ids) == max_seq_length
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes

        feature = cls(node_ids=node_ids, guid=example.guid, text_ids=text_ids,
                      adjacency_matrix=padded_adjacency_matrix, label_id=label_id)
        return feature


class PosTextGraphAdjFeatures(TextGraphAdjFeatures):

    def __init__(self, position, **kwargs):
        super(PosTextGraphAdjFeatures, self).__init__(**kwargs)
        self.position = position

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(PosTextGraphAdjFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                    has_labels=has_labels)
        mappings['position'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(PosTextGraphAdjFeatures, cls).get_feature_records(feature=feature)
        features['position'] = create_int_feature([feature.position])

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids': record['node_ids'],
                # 'edge_features': record['edge_features'],
                'adjacency_matrix': record['adjacency_matrix'],
                'position': record['position']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, PosTextGraphExample):
            raise AttributeError('Expected PosTextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        truncated = False

        node_ids, edge_features, \
        adjacency_matrix, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_nodes, max_graph_depth
        # if not truncated:
        #     padded_edge_features = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
        #     padded_edge_features[:original_node_amount, :original_node_amount] = edge_features
        # else:
        #     padded_edge_features = np.array(edge_features, dtype=np.int32).reshape(original_node_amount, original_node_amount)
        #     padded_edge_features = padded_edge_features[:max_graph_nodes, :max_graph_nodes]
        #
        # padded_edge_features = padded_edge_features.ravel()

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()

        assert len(node_ids) == max_graph_nodes
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes

        feature = cls(node_ids=node_ids, guid=example.guid,
                      adjacency_matrix=padded_adjacency_matrix, label_id=label_id,
                      position=example.position)
        return feature


class PosTextDualGraphAdjFeatures(TextDualGraphAdjFeatures):

    def __init__(self, position, **kwargs):
        super(PosTextDualGraphAdjFeatures, self).__init__(**kwargs)
        self.position = position

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(PosTextDualGraphAdjFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                        has_labels=has_labels)
        mappings['position'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(PosTextDualGraphAdjFeatures, cls).get_feature_records(feature=feature)
        features['position'] = create_int_feature([feature.position])

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids': record['node_ids'],
                'text_ids': record['text_ids'],
                'adjacency_matrix': record['adjacency_matrix'],
                'position': record['position']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, PosTextDualExample):
            raise AttributeError('Expected PosTextDualExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_seq_length = conversion_args['max_seq_length']

        truncated = False

        text_ids, node_ids, edge_features, \
        adjacency_matrix, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # Padding
        text_ids += [0] * (max_seq_length - len(text_ids))
        text_ids = text_ids[:max_seq_length]

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()

        assert len(node_ids) == max_graph_nodes
        assert len(text_ids) == max_seq_length
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes

        feature = cls(node_ids=node_ids, guid=example.guid, text_ids=text_ids,
                      adjacency_matrix=padded_adjacency_matrix, label_id=label_id,
                      position=example.position)
        return feature


class TextGraphDepAdjFeatures(TextGraphAdjFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix = retrieve_dep_tree_info_adj(example.graph,
                                                      is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, edge_features, \
               adjacency_matrix, label_id, label_map


class PosTextGraphDepAdjFeatures(PosTextGraphAdjFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix = retrieve_dep_tree_info_adj(example.graph,
                                                      is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, edge_features, \
               adjacency_matrix, label_id, label_map


class TextGraphPooledAdjFeatures(TextGraphAdjFeatures):

    def __init__(self, assignment_matrix, **kwargs):
        super(TextGraphPooledAdjFeatures, self).__init__(**kwargs)
        self.assignment_matrix = assignment_matrix

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextGraphPooledAdjFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                       has_labels=has_labels)

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_subtrees = conversion_args['max_graph_subtrees']

        mappings['assignment_matrix'] = tf.io.FixedLenFeature([max_graph_nodes * max_graph_subtrees], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextGraphPooledAdjFeatures, cls).get_feature_records(feature)
        features['assignment_matrix'] = create_int_feature(feature.assignment_matrix)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids': record['node_ids'],
                # 'edge_features': record['edge_features'],
                'adjacency_matrix': record['adjacency_matrix'],
                'assignment_matrix': record['assignment_matrix']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix, node_subtrees = retrieve_hierarchical_tree_adj_info(example.graph,
                                                                              is_directional=converter_args[
                                                                                  'is_directional'],
                                                                              kernel=converter_args['kernel'])
        assignment_matrix = build_assignment_matrix(nodes=nodes,
                                                    subtrees=node_subtrees)

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, \
               adjacency_matrix, assignment_matrix, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_subtrees = conversion_args['max_graph_subtrees']

        truncated = False

        node_ids, \
        adjacency_matrix, assignment_matrix, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)
        original_subtrees_amount = assignment_matrix.shape[1]

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_nodes, max_graph_depth
        # if not truncated:
        #     padded_edge_features = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
        #     padded_edge_features[:original_node_amount, :original_node_amount] = edge_features
        # else:
        #     padded_edge_features = np.array(edge_features, dtype=np.int32).reshape(original_node_amount, original_node_amount)
        #     padded_edge_features = padded_edge_features[:max_graph_nodes, :max_graph_nodes]
        #
        # padded_edge_features = padded_edge_features.ravel()

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1

            padded_assignment_matrix = np.zeros([max_graph_nodes, max_graph_subtrees], dtype=np.int32)
            padded_assignment_matrix[:original_node_amount,
            :min(max_graph_subtrees, original_subtrees_amount)] = assignment_matrix[:original_node_amount,
                                                                  :min(max_graph_subtrees, original_subtrees_amount)]
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

            padded_assignment_matrix = np.zeros([max_graph_nodes, max_graph_subtrees], dtype=np.int32)
            padded_assignment_matrix[:max_graph_nodes,
            :min(max_graph_subtrees, original_subtrees_amount)] = assignment_matrix[:max_graph_nodes,
                                                                  :min(max_graph_subtrees, original_subtrees_amount)]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()
        padded_assignment_matrix = padded_assignment_matrix.ravel()

        assert len(node_ids) == max_graph_nodes
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes
        assert len(padded_assignment_matrix) == max_graph_nodes * max_graph_subtrees

        feature = cls(node_ids=node_ids, assignment_matrix=padded_assignment_matrix,
                      adjacency_matrix=padded_adjacency_matrix, label_id=label_id, guid=example.guid)
        return feature


class TextGraphTreePooledAdjFeatures(TextGraphAdjFeatures):

    def __init__(self, node_spans, **kwargs):
        super(TextGraphTreePooledAdjFeatures, self).__init__(**kwargs)
        self.node_spans = node_spans

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextGraphTreePooledAdjFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                           has_labels=has_labels)
        max_graph_nodes = conversion_args['max_graph_nodes']
        mappings['node_spans'] = tf.io.FixedLenFeature([max_graph_nodes * max_graph_nodes], tf.int64)
        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextGraphTreePooledAdjFeatures, cls).get_feature_records(feature)
        features['node_spans'] = create_int_feature(feature.node_spans)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'guid': record['guid'],
                'node_ids': record['node_ids'],
                # 'edge_features': record['edge_features'],
                'adjacency_matrix': record['adjacency_matrix'],
                'node_spans': record['node_spans']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix, node_subtrees = retrieve_hierarchical_tree_adj_info(example.graph,
                                                                              is_directional=converter_args[
                                                                                  'is_directional'],
                                                                              kernel="stk")
        node_indices = np.arange(len(nodes))
        node_spans = build_node_spans(nodes=node_indices, subtree_nodes=node_subtrees)

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, \
               adjacency_matrix, node_spans, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']

        truncated = False

        node_ids, \
        adjacency_matrix, node_spans, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        original_node_amount = len(node_ids)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_nodes, max_graph_depth
        # if not truncated:
        #     padded_edge_features = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
        #     padded_edge_features[:original_node_amount, :original_node_amount] = edge_features
        # else:
        #     padded_edge_features = np.array(edge_features, dtype=np.int32).reshape(original_node_amount, original_node_amount)
        #     padded_edge_features = padded_edge_features[:max_graph_nodes, :max_graph_nodes]
        #
        # padded_edge_features = padded_edge_features.ravel()

        if not truncated:
            padded_adjacency_matrix = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_adjacency_matrix[:original_node_amount, :original_node_amount] = adjacency_matrix

            # Attach padded nodes (ensures symmetry)
            padded_adjacency_matrix[np.arange(original_node_amount, max_graph_nodes), 0] = 1
            padded_adjacency_matrix[0, np.arange(original_node_amount, max_graph_nodes)] = 1

            padded_node_spans = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_node_spans[:original_node_amount, :original_node_amount] = node_spans[:original_node_amount,
                                                                              :original_node_amount]

            # Make padded nodes valid
            padded_node_spans[
                np.arange(original_node_amount, max_graph_nodes), np.arange(original_node_amount, max_graph_nodes)] = 1
        else:
            padded_adjacency_matrix = np.array(adjacency_matrix).reshape(original_node_amount, original_node_amount)
            padded_adjacency_matrix = padded_adjacency_matrix[:max_graph_nodes, :max_graph_nodes]

            padded_node_spans = np.zeros([max_graph_nodes, max_graph_nodes], dtype=np.int32)
            padded_node_spans[:max_graph_nodes, :max_graph_nodes] = node_spans[:max_graph_nodes, : max_graph_nodes]

        padded_adjacency_matrix = padded_adjacency_matrix.ravel()
        padded_node_spans = padded_node_spans.ravel()

        assert len(node_ids) == max_graph_nodes
        # assert len(padded_edge_features) == max_graph_nodes * max_graph_nodes
        assert len(padded_adjacency_matrix) == max_graph_nodes * max_graph_nodes
        assert len(padded_node_spans) == max_graph_nodes * max_graph_nodes

        feature = cls(node_ids=node_ids, node_spans=padded_node_spans, guid=example.guid,
                      adjacency_matrix=padded_adjacency_matrix, label_id=label_id)
        return feature


class TextGraphPooledDepAdjFeatures(TextGraphPooledAdjFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_features, \
        adjacency_matrix, node_subtrees = retrieve_hierarchical_dep_tree_adj_info(example.graph,
                                                                                  is_directional=converter_args[
                                                                                      'is_directional'],
                                                                                  kernel=converter_args['kernel'])
        assignment_matrix = build_assignment_matrix(nodes=nodes,
                                                    subtrees=node_subtrees)

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))

        return node_ids, \
               adjacency_matrix, assignment_matrix, label_id, label_map


class TextHierarchicalGraphFeatures(TextGraphFeatures):

    def __init__(self, subtree_indices, subtree_edge_features, subtree_edge_indices,
                 subtree_edge_mask, subtree_node_indices, subtree_node_segments,
                 subtree_directionality_mask, subtree_indices_mask, **kwargs):
        super(TextHierarchicalGraphFeatures, self).__init__(**kwargs)
        self.subtree_indices = subtree_indices
        self.subtree_edge_features = subtree_edge_features
        self.subtree_edge_indices = subtree_edge_indices
        self.subtree_edge_mask = subtree_edge_mask
        self.subtree_node_indices = subtree_node_indices
        self.subtree_node_segments = subtree_node_segments
        self.subtree_directionality_mask = subtree_directionality_mask
        self.subtree_indices_mask = subtree_indices_mask

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextHierarchicalGraphFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                          has_labels=has_labels)

        max_graph_subtrees = conversion_args['max_graph_subtrees']
        max_graph_subtree_size = conversion_args['max_graph_subtree_size']
        max_graph_subtree_edges = conversion_args['max_graph_subtree_edges']
        max_graph_subtree_depth = conversion_args['max_graph_subtree_depth']

        mappings['subtree_indices'] = tf.io.FixedLenFeature([max_graph_subtrees * max_graph_subtree_size],
                                                            tf.int64)
        # mappings['subtree_edge_features'] = tf.io.FixedLenFeature([max_graph_subtree_edges * max_graph_subtree_depth],
        #                                                           tf.int64)
        mappings['subtree_edge_indices'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                 tf.int64)
        mappings['subtree_edge_mask'] = tf.io.FixedLenFeature([max_graph_subtree_edges],
                                                              tf.int64)
        mappings['subtree_node_indices'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                 tf.int64)
        mappings['subtree_node_segments'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                  tf.int64)
        mappings['subtree_directionality_mask'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                        tf.int64)
        mappings['subtree_indices_mask'] = tf.io.FixedLenFeature([max_graph_subtrees * max_graph_subtree_size],
                                                                 tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextHierarchicalGraphFeatures, cls).get_feature_records(feature=feature)

        features['subtree_indices'] = create_int_feature(feature.subtree_indices)
        # features['subtree_edge_features'] = create_int_feature(feature.subtree_edge_features)
        features['subtree_edge_indices'] = create_int_feature(feature.subtree_edge_indices)
        features['subtree_edge_mask'] = create_int_feature(feature.subtree_edge_mask)
        features['subtree_node_indices'] = create_int_feature(feature.subtree_node_indices)
        features['subtree_node_segments'] = create_int_feature(feature.subtree_node_segments)
        features['subtree_directionality_mask'] = create_int_feature(feature.subtree_directionality_mask)
        features['subtree_indices_mask'] = create_int_feature(feature.subtree_indices_mask)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                # 'edge_features': record['edge_features'],
                'edge_indices': record['edge_indices'],
                'edge_mask': record['edge_mask'],
                'node_indices': record['node_indices'],
                'node_segments': record['node_segments'],
                'directionality_mask': record['directionality_mask'],
                'subtree_indices': record['subtree_indices'],
                # 'subtree_edge_features': record['subtree_edge_features'],
                'subtree_edge_indices': record['subtree_edge_indices'],
                'subtree_edge_mask': record['subtree_edge_mask'],
                'subtree_node_indices': record['subtree_node_indices'],
                'subtree_node_segments': record['subtree_node_segments'],
                'subtree_directionality_mask': record['subtree_directionality_mask'],
                'subtree_indices_mask': record['subtree_indices_mask']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, directionality_mask, \
        subtree_indices, subtree_edge_indices, \
        subtree_edge_features, subtree_node_indices, \
        subtree_node_segments, \
        subtree_directionality_mask = retrieve_hierarchical_tree_info(example.graph,
                                                                      is_directional=converter_args['is_directional'],
                                                                      kernel=converter_args['kernel'])

        subtree_indices = [seq.tolist() for seq in subtree_indices]
        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))
        edge_features_ids = [convert_number_to_binary_list(features) for features in edge_features]
        subtree_edge_features = [convert_number_to_binary_list(features) for features in subtree_edge_features]

        return node_ids, edge_indices, edge_features_ids, node_indices, \
               node_segments, directionality_mask, subtree_indices, subtree_edge_indices, \
               subtree_edge_features, subtree_node_indices, subtree_node_segments, \
               subtree_directionality_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        # Level 1: Word level

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        max_graph_depth = conversion_args['max_graph_depth']

        # Level 2: Subtree level

        max_graph_subtrees = conversion_args['max_graph_subtrees']
        max_graph_subtree_size = conversion_args['max_graph_subtree_size']
        max_graph_subtree_edges = conversion_args['max_graph_subtree_edges']
        max_graph_subtree_depth = conversion_args['max_graph_subtree_depth']

        truncated = False

        node_ids, edge_indices, edge_features, node_indices, \
        node_segments, directionality_mask, subtree_indices, subtree_edge_indices, \
        subtree_edge_features, subtree_node_indices, subtree_node_segments, \
        subtree_directionality_mask, label_id, label_map = cls.convert_example(example,
                                                                               label_list,
                                                                               tokenizer,
                                                                               has_labels=has_labels,
                                                                               converter_args=converter_args)

        # Level 1: Word level

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True
            truncated_node_ids = node_ids[max_graph_nodes:]

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_edges, 2
        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges, max_graph_depth
        edge_features = list(map(
            lambda item: [0] * (max_graph_depth - len(item)) + item
            if max_graph_depth - len(item) > 0 else item[:max_graph_depth],
            edge_features))
        if not truncated:
            edge_features = edge_features + [[0] * max_graph_depth for _ in range(max_graph_edges - len(edge_features))]
        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1, (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        assert len(node_ids) == max_graph_nodes
        assert len(edge_features) == max_graph_edges
        assert len(edge_indices) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        # Flatten
        edge_features = [item for seq in edge_features for item in seq]
        edge_indices = [item for seq in edge_indices for item in seq]

        # Level 2: Subtree level

        assert max([item for pair in subtree_edge_indices for item in pair]) == len(subtree_indices) - 1

        # Check graph truncating
        subtree_truncated = False

        if truncated:
            subtree_indices = [[item for item in seq if item not in truncated_node_ids]
                               for seq in subtree_indices]

            # Re-indexing map!
            truncation_mapping = {}
            truncation_mapping_counter = 0
            truncated_subtrees = []

            for idx, subtree in enumerate(subtree_indices):
                if len(subtree) <= 1:
                    truncated_subtrees.append(idx)
                else:
                    truncation_mapping[idx] = truncation_mapping_counter
                    truncation_mapping_counter += 1

            truncated_subtrees = [idx for idx, subtree in enumerate(subtree_indices)
                                  if len(subtree) <= 1]
            if truncated_subtrees:
                subtree_truncated = True

        # Adjust in case of subtree truncation
        # This requires re-indexing!!!
        if subtree_truncated:
            subtree_indices = [seq for seq in subtree_indices if len(seq) > 1]

            # Truncate and re-index edge indices
            subtree_edge_indices = [([truncation_mapping[pair[0]], truncation_mapping[pair[1]]], idx)
                                    for idx, pair in enumerate(subtree_edge_indices)
                                    if pair[0] not in truncated_subtrees and pair[1] not in truncated_subtrees]

            # Re-indexing map!
            truncated_edges_mapping = {}
            for idx, item in enumerate(subtree_edge_indices):
                truncated_edges_mapping[item[1]] = idx

            truncated_subtree_edges = [item[1] for item in subtree_edge_indices]
            subtree_edge_indices = [item[0] for item in subtree_edge_indices]

            subtree_edge_features = [item for idx, item in enumerate(subtree_edge_features)
                                     if idx in truncated_subtree_edges]

            subtree_node_indices = [(truncated_edges_mapping[item], idx)
                                    for idx, item in enumerate(subtree_node_indices)
                                    if item in truncated_subtree_edges]
            truncated_subtree_indices = [item[1] for item in subtree_node_indices]
            subtree_node_indices = [item[0] for item in subtree_node_indices]

            subtree_node_segments = [truncation_mapping[item] for idx, item in enumerate(subtree_node_segments)
                                     if idx in truncated_subtree_indices]

            subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask)
                                           if idx in truncated_subtree_indices]

        assert len(subtree_node_indices) == len(subtree_edge_indices) * 2
        assert max([item for pair in subtree_edge_indices for item in pair]) == len(subtree_indices) - 1

        debug = ""

        # Padding
        # Note: we may have individual truncations here
        pad_subtree_amount = max_graph_subtrees - len(subtree_indices)
        pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

        truncated_subtree_amount = False
        truncated_subtree_edges_amount = False

        if pad_subtree_amount < 0:
            truncated_subtree_amount = True

        if pad_subtree_edges_amount < 0:
            truncated_subtree_edges_amount = True

        # Split into cases

        # Case 1: Subtrees amount OK
        if pad_subtree_amount == 0:

            debug = "1"

            subtree_indices_mask = [[1] * len(seq) + [0] * (max_graph_subtree_size - len(seq))
                                    if len(seq) < max_graph_subtree_size else seq[:max_graph_subtree_size]
                                    for seq in subtree_indices]
            subtree_indices = [seq[:max_graph_subtree_size] if len(seq) > max_graph_subtree_size else
                               seq + [len(node_ids) - 1] * (max_graph_subtree_size - len(seq))
                               for seq in subtree_indices]
            subtree_edge_features = list(map(
                lambda item: [0] * (max_graph_subtree_depth - len(item)) + item
                if max_graph_subtree_depth - len(item) > 0 else item[:max_graph_subtree_depth],
                subtree_edge_features))

            # Case 1.A: Edges amount OK -> Nothing to do
            if pad_subtree_edges_amount == 0:
                subtree_edge_mask = [1] * len(subtree_edge_features)
                debug = "1.A"

            # Case 1.B: Edges amount too high (truncation)
            if truncated_subtree_edges_amount:
                debug = "1.B"

                truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                     truncation_amount=pad_subtree_edges_amount * -1)
                subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                        if idx not in truncated_subtree_edge_indices]
                subtree_edge_mask = [1] * len(max_graph_subtree_edges)
                subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                         idx not in truncated_subtree_edge_indices]
                subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                        item not in truncated_subtree_edge_indices]
                subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                subtree_node_indices = [item[0] for item in subtree_node_indices]
                subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                         idx in subtree_segment_indexes]
                subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                               idx in subtree_segment_indexes]

            # Case 1.C: Edges amount too low (padding)
            if not truncated_subtree_edges_amount and pad_subtree_edges_amount > 0:
                debug = "1.C"

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

        # Case 2: Subtrees amount is too high (truncation)
        elif truncated_subtree_amount:

            debug = "2"

            subtree_indices_mask = [[1] * len(seq) + [0] * (max_graph_subtree_size - len(seq))
                                    if len(seq) < max_graph_subtree_size else seq[:max_graph_subtree_size]
                                    for seq in subtree_indices]
            subtree_indices_mask = subtree_indices_mask[:max_graph_subtrees]

            subtree_indices = [seq[:max_graph_subtree_size] if len(seq) > max_graph_subtree_size else
                               seq + [len(node_ids) - 1] * (max_graph_subtree_size - len(seq))
                               for seq in subtree_indices]
            subtree_indices = subtree_indices[:max_graph_subtrees]

            subtree_edge_features = list(map(
                lambda item: [0] * (max_graph_subtree_depth - len(item)) + item
                if max_graph_subtree_depth - len(item) > 0 else item[:max_graph_subtree_depth],
                subtree_edge_features))

            # Remove corresponding edges
            subtree_edge_indices = [(pair, idx) for idx, pair in enumerate(subtree_edge_indices)
                                    if pair[0] in np.arange(max_graph_subtrees) and pair[1] in np.arange(
                    max_graph_subtrees)]
            truncated_subtree_indices = [item[1] for item in subtree_edge_indices]
            subtree_edge_indices = [item[0] for item in subtree_edge_indices]
            subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                     idx in truncated_subtree_indices]
            subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                    item in truncated_subtree_indices]
            subtree_segment_indexes = [item[1] for item in subtree_node_indices]
            subtree_node_indices = [item[0] for item in subtree_node_indices]
            subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                     idx in subtree_segment_indexes]
            subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                           idx in subtree_segment_indexes]

            # Case 2.A: Edges amount OK -> pad with fictitious edges
            if pad_subtree_edges_amount == 0:
                debug = "2.A"

                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

            # Case 2.B: Edges amount too high (truncation):
            if truncated_subtree_edges_amount:

                debug = "2.B"

                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                # Still requires truncation
                if pad_subtree_edges_amount < 0:
                    debug = "2.B.1"

                    truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                         truncation_amount=pad_subtree_edges_amount * -1)
                    subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                            if idx not in truncated_subtree_edge_indices]
                    subtree_edge_mask = [1] * len(max_graph_subtree_edges)
                    subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                             idx not in truncated_subtree_edge_indices]
                    subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                            item not in truncated_subtree_edge_indices]
                    subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                    subtree_node_indices = [item[0] for item in subtree_node_indices]
                    subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                             idx in subtree_segment_indexes]
                    subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                                   idx in subtree_segment_indexes]

                # Padding
                if pad_subtree_edges_amount > 0:
                    debug = "2.B.2"

                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

                # It is fine:
                if pad_subtree_edges_amount == 0:
                    debug = "2.B.3"

                    subtree_edge_mask = [1] * max_graph_subtree_edges

            # Case 2.C: Edges amount too low (padding)
            if not truncated_subtree_edges_amount and pad_subtree_edges_amount > 0:
                debug = "2.C"

                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                 in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

        # Case 3: Subtrees amount is too low (padding)
        else:
            subtree_indices_mask = [[1] * len(seq) + [0] * (max_graph_subtree_size - len(seq))
                                    if len(seq) < max_graph_subtree_size else seq[:max_graph_subtree_size]
                                    for seq in subtree_indices]
            subtree_indices_mask += [[0] * max_graph_subtree_size] * pad_subtree_amount

            subtree_indices = [seq[:max_graph_subtree_size] if len(seq) > max_graph_subtree_size else
                               seq + [len(node_ids) - 1] * (max_graph_subtree_size - len(seq))
                               for seq in subtree_indices]

            subtree_indices += [[len(node_ids) - 1] * max_graph_subtree_size] * pad_subtree_amount

            subtree_edge_features = list(map(
                lambda item: [0] * (max_graph_subtree_depth - len(item)) + item
                if max_graph_subtree_depth - len(item) > 0 else item[:max_graph_subtree_depth],
                subtree_edge_features))

            # Case 3.1: remove some edges from top connected subtrees
            # and make sure each added subtree has at least one connection
            if pad_subtree_edges_amount == 0:
                debug = "3.1"

                # Remove
                truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                     truncation_amount=pad_subtree_amount - 1)
                subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                        if idx not in truncated_subtree_edge_indices]
                subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                         idx not in truncated_subtree_edge_indices]
                subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                        item not in truncated_subtree_edge_indices]
                subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                subtree_node_indices = [item[0] for item in subtree_node_indices]
                subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                         idx in subtree_segment_indexes]
                subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                               idx in subtree_segment_indexes]

                # Pad without isolating subtrees
                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_max = max(subtree_node_segments)
                subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                         subtree_node_segments_max + pad_subtree_amount).tolist()
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

            # Case 3.2: remove some edges from top connected subtrees
            # and replace a number of edges equal to added padding subtrees,
            # while making sure no subtree is left isolated.
            if truncated_subtree_edges_amount:
                debug = "3.2"

                # Remove
                truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                     truncation_amount=pad_subtree_edges_amount * -1 + pad_subtree_amount - 1)
                subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                        if idx not in truncated_subtree_edge_indices]
                subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                         idx not in truncated_subtree_edge_indices]
                subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                        item not in truncated_subtree_edge_indices]
                subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                subtree_node_indices = [item[0] for item in subtree_node_indices]
                subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                         idx in subtree_segment_indexes]
                subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                               idx in subtree_segment_indexes]

                # Pad without isolating subtrees
                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_max = max(subtree_node_segments)
                subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                         (
                                                                 subtree_node_segments_max + 1) + pad_subtree_edges_amount).tolist()
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

            # Case 3.3: edges amount too low (padding)
            if not truncated_subtree_edges_amount and pad_subtree_edges_amount > 0:

                # Case 3.3.1: if padding amount are the same
                if pad_subtree_amount == pad_subtree_edges_amount:
                    debug = "3.3.1"

                    # Pad without isolating subtrees
                    pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_max = max(subtree_node_segments)
                    subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                             (
                                                                     subtree_node_segments_max + 1) + pad_subtree_edges_amount).tolist()
                    # subtree_node_segments_to_add += [max(subtree_node_segments_to_add)]
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

                # Case 3.3.2: if more nodes than edges
                if pad_subtree_amount > pad_subtree_edges_amount:
                    debug = "3.3.2"

                    # Remove edges based on pad difference
                    pad_difference = pad_subtree_amount - pad_subtree_edges_amount

                    truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                         truncation_amount=pad_difference)
                    subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                            if idx not in truncated_subtree_edge_indices]
                    subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                             idx not in truncated_subtree_edge_indices]
                    subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                            item not in truncated_subtree_edge_indices]
                    subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                    subtree_node_indices = [item[0] for item in subtree_node_indices]
                    subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                             idx in subtree_segment_indexes]
                    subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                                   idx in subtree_segment_indexes]

                    # Pad
                    pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_max = max(subtree_node_segments)
                    subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                             (
                                                                     subtree_node_segments_max + 1) + pad_subtree_edges_amount).tolist()
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

                # Case 3.3.3: if more edges than nodes
                if pad_subtree_amount < pad_subtree_edges_amount:
                    debug = "3.3.3"

                    pad_difference = pad_subtree_edges_amount - pad_subtree_amount

                    # Pad until node padding and then do not add new nodes
                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_max = max(subtree_node_segments)
                    subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                             (
                                                                     subtree_node_segments_max + 1) + pad_subtree_amount).tolist()
                    subtree_node_segments_to_add += [max(subtree_node_segments_to_add)] * pad_difference
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

        try:
            assert len(subtree_indices) == max_graph_subtrees
            assert len(subtree_indices_mask) == max_graph_subtrees
            assert len(subtree_edge_features) == max_graph_subtree_edges
            assert len(subtree_edge_indices) == max_graph_subtree_edges
            assert len(subtree_edge_mask) == max_graph_subtree_edges
            assert len(subtree_node_indices) == max_graph_subtree_edges * 2
            assert len(subtree_node_segments) == max_graph_subtree_edges * 2
            assert len(subtree_directionality_mask) == max_graph_subtree_edges * 2
            # assert max([item for pair in subtree_edge_indices for item in pair]) == len(subtree_indices) - 1
            assert len(set(subtree_node_segments)) == len(subtree_indices)
        except AssertionError as e:
            print(e)
            print("Debug section: ", debug)
            print()

        # Flatten
        subtree_indices = [item for seq in subtree_indices for item in seq]
        subtree_indices_mask = [item for seq in subtree_indices_mask for item in seq]
        subtree_edge_indices = [item for seq in subtree_edge_indices for item in seq]
        subtree_edge_features = [item for seq in subtree_edge_features for item in seq]

        assert len(subtree_indices) == max_graph_subtrees * max_graph_subtree_size
        assert len(subtree_indices_mask) == max_graph_subtrees * max_graph_subtree_size
        assert len(subtree_edge_indices) == max_graph_subtree_edges * 2
        assert len(subtree_edge_features) == max_graph_subtree_edges * max_graph_subtree_depth

        feature = cls(node_ids=node_ids, edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, directionality_mask=directionality_mask,
                      subtree_indices=subtree_indices, subtree_indices_mask=subtree_indices_mask,
                      subtree_edge_indices=subtree_edge_indices, subtree_edge_mask=subtree_edge_mask,
                      subtree_edge_features=subtree_edge_features, subtree_node_indices=subtree_node_indices,
                      subtree_node_segments=subtree_node_segments,
                      subtree_directionality_mask=subtree_directionality_mask)
        return feature


class TextDepGraphFeatures(TextGraphFeatures):

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        num_labels = conversion_args['num_labels']

        mappings = {
            'node_ids': tf.io.FixedLenFeature([max_graph_nodes], tf.int64),
            'edge_features': tf.io.FixedLenFeature([max_graph_edges], tf.int64),
            'edge_indices': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'edge_mask': tf.io.FixedLenFeature([max_graph_edges], tf.int64),
            'node_indices': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'node_segments': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64),
            'directionality_mask': tf.io.FixedLenFeature([max_graph_edges * 2], tf.int64)
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask = retrieve_dep_tree_info(example.graph,
                                                     is_directional=converter_args['is_directional'])

        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))
        edge_features_ids = tokenizer.convert_tokens_to_ids(' '.join(edge_features))

        return node_ids, edge_indices, edge_features_ids, node_indices, \
               node_segments, directionality_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']

        truncated = False

        node_ids, edge_indices, edge_features, \
        node_indices, node_segments, directionality_mask, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges
        if not truncated:
            edge_features = edge_features + [0] * pad_edges_amount
        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1,
                                             (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        assert len(node_ids) == max_graph_nodes
        assert len(edge_indices) == max_graph_edges
        assert len(edge_features) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        # Flatten
        edge_indices = [item for seq in edge_indices for item in seq]

        feature = cls(node_ids=node_ids, edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, directionality_mask=directionality_mask)
        return feature


class TextHierarchicalDepGraphFeatures(TextDepGraphFeatures):

    def __init__(self, subtree_indices, subtree_edge_features, subtree_edge_indices,
                 subtree_edge_mask, subtree_node_indices, subtree_node_segments,
                 subtree_directionality_mask, subtree_indices_mask, **kwargs):
        super(TextHierarchicalDepGraphFeatures, self).__init__(**kwargs)
        self.subtree_indices = subtree_indices
        self.subtree_edge_features = subtree_edge_features
        self.subtree_edge_indices = subtree_edge_indices
        self.subtree_edge_mask = subtree_edge_mask
        self.subtree_node_indices = subtree_node_indices
        self.subtree_node_segments = subtree_node_segments
        self.subtree_directionality_mask = subtree_directionality_mask
        self.subtree_indices_mask = subtree_indices_mask

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextHierarchicalDepGraphFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                             has_labels=has_labels)

        max_graph_subtrees = conversion_args['max_graph_subtrees']
        max_graph_subtree_size = conversion_args['max_graph_subtree_size']
        max_graph_subtree_edges = conversion_args['max_graph_subtree_edges']
        max_graph_subtree_depth = conversion_args['max_graph_subtree_depth']

        mappings['subtree_indices'] = tf.io.FixedLenFeature([max_graph_subtrees * max_graph_subtree_size],
                                                            tf.int64)
        mappings['subtree_edge_features'] = tf.io.FixedLenFeature([max_graph_subtree_edges * max_graph_subtree_depth],
                                                                  tf.int64)
        mappings['subtree_edge_indices'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                 tf.int64)
        mappings['subtree_edge_mask'] = tf.io.FixedLenFeature([max_graph_subtree_edges],
                                                              tf.int64)
        mappings['subtree_node_indices'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                 tf.int64)
        mappings['subtree_node_segments'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                  tf.int64)
        mappings['subtree_directionality_mask'] = tf.io.FixedLenFeature([max_graph_subtree_edges * 2],
                                                                        tf.int64)
        mappings['subtree_indices_mask'] = tf.io.FixedLenFeature([max_graph_subtrees * max_graph_subtree_size],
                                                                 tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextHierarchicalDepGraphFeatures, cls).get_feature_records(feature=feature)

        features['subtree_indices'] = create_int_feature(feature.subtree_indices)
        features['subtree_edge_features'] = create_int_feature(feature.subtree_edge_features)
        features['subtree_edge_indices'] = create_int_feature(feature.subtree_edge_indices)
        features['subtree_edge_mask'] = create_int_feature(feature.subtree_edge_mask)
        features['subtree_node_indices'] = create_int_feature(feature.subtree_node_indices)
        features['subtree_node_segments'] = create_int_feature(feature.subtree_node_segments)
        features['subtree_directionality_mask'] = create_int_feature(feature.subtree_directionality_mask)
        features['subtree_indices_mask'] = create_int_feature(feature.subtree_indices_mask)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                'edge_features': record['edge_features'],
                'edge_indices': record['edge_indices'],
                'edge_mask': record['edge_mask'],
                'node_indices': record['node_indices'],
                'node_segments': record['node_segments'],
                'directionality_mask': record['directionality_mask'],
                'subtree_indices': record['subtree_indices'],
                'subtree_edge_features': record['subtree_edge_features'],
                'subtree_edge_indices': record['subtree_edge_indices'],
                'subtree_edge_mask': record['subtree_edge_mask'],
                'subtree_node_indices': record['subtree_node_indices'],
                'subtree_node_segments': record['subtree_node_segments'],
                'subtree_directionality_mask': record['subtree_directionality_mask'],
                'subtree_indices_mask': record['subtree_indices_mask']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, directionality_mask, \
        subtree_indices, subtree_edge_indices, \
        subtree_edge_features, subtree_node_indices, \
        subtree_node_segments, \
        subtree_directionality_mask = retrieve_hierarchical_dep_tree_info(example.graph,
                                                                          is_directional=converter_args[
                                                                              'is_directional'],
                                                                          kernel=converter_args['kernel'])

        subtree_indices = [seq.tolist() for seq in subtree_indices]
        node_ids = tokenizer.convert_tokens_to_ids(' '.join(nodes))
        edge_features_ids = tokenizer.convert_tokens_to_ids(' '.join(edge_features))
        subtree_edge_features = [convert_number_to_binary_list(features) for features in subtree_edge_features]

        return node_ids, edge_indices, edge_features_ids, node_indices, \
               node_segments, directionality_mask, subtree_indices, subtree_edge_indices, \
               subtree_edge_features, subtree_node_indices, subtree_node_segments, \
               subtree_directionality_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        # Level 1: Word level

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']

        # Level 2: Subtree level

        max_graph_subtrees = conversion_args['max_graph_subtrees']
        max_graph_subtree_size = conversion_args['max_graph_subtree_size']
        max_graph_subtree_edges = conversion_args['max_graph_subtree_edges']
        max_graph_subtree_depth = conversion_args['max_graph_subtree_depth']

        truncated = False

        node_ids, edge_indices, edge_features, node_indices, \
        node_segments, directionality_mask, subtree_indices, subtree_edge_indices, \
        subtree_edge_features, subtree_node_indices, subtree_node_segments, \
        subtree_directionality_mask, label_id, label_map = cls.convert_example(example,
                                                                               label_list,
                                                                               tokenizer,
                                                                               has_labels=has_labels,
                                                                               converter_args=converter_args)

        if len(subtree_indices) == 1:
            subtree_indices.append([0])
            subtree_edge_indices = [[0, 1]]
            subtree_node_indices = [0, 0]
            subtree_node_segments = [0, 1]
            subtree_directionality_mask = [1, 1]
            subtree_edge_features = [[0]]

        # Level 1: Word level

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True
            truncated_node_ids = node_ids[max_graph_nodes:]

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_edges, 2
        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges, max_graph_depth
        if not truncated:
            edge_features = edge_features + [0] * pad_edges_amount
        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1, (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        assert len(node_ids) == max_graph_nodes
        assert len(edge_features) == max_graph_edges
        assert len(edge_indices) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        # Flatten
        edge_indices = [item for seq in edge_indices for item in seq]

        # Level 2: Subtree level

        assert max([item for pair in subtree_edge_indices for item in pair]) == len(subtree_indices) - 1

        # Check graph truncating
        subtree_truncated = False

        if truncated:
            subtree_indices = [[item for item in seq if item not in truncated_node_ids]
                               for seq in subtree_indices]

            # Re-indexing map!
            truncation_mapping = {}
            truncation_mapping_counter = 0
            truncated_subtrees = []

            for idx, subtree in enumerate(subtree_indices):
                if len(subtree) <= 1:
                    truncated_subtrees.append(idx)
                else:
                    truncation_mapping[idx] = truncation_mapping_counter
                    truncation_mapping_counter += 1

            truncated_subtrees = [idx for idx, subtree in enumerate(subtree_indices)
                                  if len(subtree) <= 1]
            if truncated_subtrees:
                subtree_truncated = True

        # Adjust in case of subtree truncation
        # This requires re-indexing!!!
        if subtree_truncated:
            subtree_indices = [seq for seq in subtree_indices if len(seq) > 1]

            # Truncate and re-index edge indices
            subtree_edge_indices = [([truncation_mapping[pair[0]], truncation_mapping[pair[1]]], idx)
                                    for idx, pair in enumerate(subtree_edge_indices)
                                    if pair[0] not in truncated_subtrees and pair[1] not in truncated_subtrees]

            # Re-indexing map!
            truncated_edges_mapping = {}
            for idx, item in enumerate(subtree_edge_indices):
                truncated_edges_mapping[item[1]] = idx

            truncated_subtree_edges = [item[1] for item in subtree_edge_indices]
            subtree_edge_indices = [item[0] for item in subtree_edge_indices]

            subtree_edge_features = [item for idx, item in enumerate(subtree_edge_features)
                                     if idx in truncated_subtree_edges]

            subtree_node_indices = [(truncated_edges_mapping[item], idx)
                                    for idx, item in enumerate(subtree_node_indices)
                                    if item in truncated_subtree_edges]
            truncated_subtree_indices = [item[1] for item in subtree_node_indices]
            subtree_node_indices = [item[0] for item in subtree_node_indices]

            subtree_node_segments = [truncation_mapping[item] for idx, item in enumerate(subtree_node_segments)
                                     if idx in truncated_subtree_indices]

            subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask)
                                           if idx in truncated_subtree_indices]

        assert len(subtree_node_indices) == len(subtree_edge_indices) * 2
        assert max([item for pair in subtree_edge_indices for item in pair]) == len(subtree_indices) - 1

        debug = ""

        # Padding
        # Note: we may have individual truncations here
        pad_subtree_amount = max_graph_subtrees - len(subtree_indices)
        pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

        truncated_subtree_amount = False
        truncated_subtree_edges_amount = False

        if pad_subtree_amount < 0:
            truncated_subtree_amount = True

        if pad_subtree_edges_amount < 0:
            truncated_subtree_edges_amount = True

        # Split into cases

        # Case 1: Subtrees amount OK
        if pad_subtree_amount == 0:

            debug = "1"

            subtree_indices_mask = [[1] * len(seq) + [0] * (max_graph_subtree_size - len(seq))
                                    if len(seq) < max_graph_subtree_size else seq[:max_graph_subtree_size]
                                    for seq in subtree_indices]
            subtree_indices = [seq[:max_graph_subtree_size] if len(seq) > max_graph_subtree_size else
                               seq + [len(node_ids) - 1] * (max_graph_subtree_size - len(seq))
                               for seq in subtree_indices]
            subtree_edge_features = list(map(
                lambda item: [0] * (max_graph_subtree_depth - len(item)) + item
                if max_graph_subtree_depth - len(item) > 0 else item[:max_graph_subtree_depth],
                subtree_edge_features))

            # Case 1.A: Edges amount OK -> Nothing to do
            if pad_subtree_edges_amount == 0:
                subtree_edge_mask = [1] * len(subtree_edge_features)
                debug = "1.A"

            # Case 1.B: Edges amount too high (truncation)
            if truncated_subtree_edges_amount:
                debug = "1.B"

                truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                     truncation_amount=pad_subtree_edges_amount * -1)
                subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                        if idx not in truncated_subtree_edge_indices]
                subtree_edge_mask = [1] * len(max_graph_subtree_edges)
                subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                         idx not in truncated_subtree_edge_indices]
                subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                        item not in truncated_subtree_edge_indices]
                subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                subtree_node_indices = [item[0] for item in subtree_node_indices]
                subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                         idx in subtree_segment_indexes]
                subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                               idx in subtree_segment_indexes]

            # Case 1.C: Edges amount too low (padding)
            if not truncated_subtree_edges_amount and pad_subtree_edges_amount > 0:
                debug = "1.C"

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

        # Case 2: Subtrees amount is too high (truncation)
        elif truncated_subtree_amount:

            debug = "2"

            subtree_indices_mask = [[1] * len(seq) + [0] * (max_graph_subtree_size - len(seq))
                                    if len(seq) < max_graph_subtree_size else seq[:max_graph_subtree_size]
                                    for seq in subtree_indices]
            subtree_indices_mask = subtree_indices_mask[:max_graph_subtrees]

            subtree_indices = [seq[:max_graph_subtree_size] if len(seq) > max_graph_subtree_size else
                               seq + [len(node_ids) - 1] * (max_graph_subtree_size - len(seq))
                               for seq in subtree_indices]
            subtree_indices = subtree_indices[:max_graph_subtrees]

            subtree_edge_features = list(map(
                lambda item: [0] * (max_graph_subtree_depth - len(item)) + item
                if max_graph_subtree_depth - len(item) > 0 else item[:max_graph_subtree_depth],
                subtree_edge_features))

            # Remove corresponding edges
            subtree_edge_indices = [(pair, idx) for idx, pair in enumerate(subtree_edge_indices)
                                    if pair[0] in np.arange(max_graph_subtrees) and pair[1] in np.arange(
                    max_graph_subtrees)]
            truncated_subtree_indices = [item[1] for item in subtree_edge_indices]
            subtree_edge_indices = [item[0] for item in subtree_edge_indices]
            subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                     idx in truncated_subtree_indices]
            subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                    item in truncated_subtree_indices]
            subtree_segment_indexes = [item[1] for item in subtree_node_indices]
            subtree_node_indices = [item[0] for item in subtree_node_indices]
            subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                     idx in subtree_segment_indexes]
            subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                           idx in subtree_segment_indexes]

            # Case 2.A: Edges amount OK -> pad with fictitious edges
            if pad_subtree_edges_amount == 0:
                debug = "2.A"

                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

            # Case 2.B: Edges amount too high (truncation):
            if truncated_subtree_edges_amount:

                debug = "2.B"

                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                # Still requires truncation
                if pad_subtree_edges_amount < 0:
                    debug = "2.B.1"

                    truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                         truncation_amount=pad_subtree_edges_amount * -1)
                    subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                            if idx not in truncated_subtree_edge_indices]
                    subtree_edge_mask = [1] * len(max_graph_subtree_edges)
                    subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                             idx not in truncated_subtree_edge_indices]
                    subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                            item not in truncated_subtree_edge_indices]
                    subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                    subtree_node_indices = [item[0] for item in subtree_node_indices]
                    subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                             idx in subtree_segment_indexes]
                    subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                                   idx in subtree_segment_indexes]

                # Padding
                if pad_subtree_edges_amount > 0:
                    debug = "2.B.2"

                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

                # It is fine:
                if pad_subtree_edges_amount == 0:
                    debug = "2.B.3"

                    subtree_edge_mask = [1] * max_graph_subtree_edges

            # Case 2.C: Edges amount too low (padding)
            if not truncated_subtree_edges_amount and pad_subtree_edges_amount > 0:
                debug = "2.C"

                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_to_add = [max(subtree_node_segments) - 1] * pad_subtree_edges_amount
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                 in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

        # Case 3: Subtrees amount is too low (padding)
        else:
            subtree_indices_mask = [[1] * len(seq) + [0] * (max_graph_subtree_size - len(seq))
                                    if len(seq) < max_graph_subtree_size else seq[:max_graph_subtree_size]
                                    for seq in subtree_indices]
            subtree_indices_mask += [[0] * max_graph_subtree_size] * pad_subtree_amount

            subtree_indices = [seq[:max_graph_subtree_size] if len(seq) > max_graph_subtree_size else
                               seq + [0] * (max_graph_subtree_size - len(seq))
                               for seq in subtree_indices]

            subtree_indices += [[0] * max_graph_subtree_size] * pad_subtree_amount

            subtree_edge_features = list(map(
                lambda item: [0] * (max_graph_subtree_depth - len(item)) + item
                if max_graph_subtree_depth - len(item) > 0 else item[:max_graph_subtree_depth],
                subtree_edge_features))

            # Case 3.1: remove some edges from top connected subtrees
            # and make sure each added subtree has at least one connection
            if pad_subtree_edges_amount == 0:
                debug = "3.1"

                # Remove
                truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                     truncation_amount=pad_subtree_amount - 1)
                subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                        if idx not in truncated_subtree_edge_indices]
                subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                         idx not in truncated_subtree_edge_indices]
                subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                        item not in truncated_subtree_edge_indices]
                subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                subtree_node_indices = [item[0] for item in subtree_node_indices]
                subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                         idx in subtree_segment_indexes]
                subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                               idx in subtree_segment_indexes]

                # Pad without isolating subtrees
                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_max = max(subtree_node_segments)
                subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                         subtree_node_segments_max + pad_subtree_amount).tolist()
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

            # Case 3.2: remove some edges from top connected subtrees
            # and replace a number of edges equal to added padding subtrees,
            # while making sure no subtree is left isolated.
            if truncated_subtree_edges_amount:
                debug = "3.2"

                # Remove
                truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                     truncation_amount=pad_subtree_edges_amount * -1 + pad_subtree_amount - 1)
                subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                        if idx not in truncated_subtree_edge_indices]
                subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                         idx not in truncated_subtree_edge_indices]
                subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                        item not in truncated_subtree_edge_indices]
                subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                subtree_node_indices = [item[0] for item in subtree_node_indices]
                subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                         idx in subtree_segment_indexes]
                subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                               idx in subtree_segment_indexes]

                # Pad without isolating subtrees
                pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                subtree_node_segments_max = max(subtree_node_segments)
                subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                         (
                                                                 subtree_node_segments_max + 1) + pad_subtree_edges_amount).tolist()
                subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _ in
                                                                 (0, 1)]
                subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

            # Case 3.3: edges amount too low (padding)
            if not truncated_subtree_edges_amount and pad_subtree_edges_amount > 0:

                # Case 3.3.1: if padding amount are the same
                if pad_subtree_amount == pad_subtree_edges_amount:
                    debug = "3.3.1"

                    # Pad without isolating subtrees
                    pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_max = max(subtree_node_segments)
                    subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                             (
                                                                     subtree_node_segments_max + 1) + pad_subtree_edges_amount).tolist()
                    # subtree_node_segments_to_add += [max(subtree_node_segments_to_add)]
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

                # Case 3.3.2: if more nodes than edges
                if pad_subtree_amount > pad_subtree_edges_amount:
                    debug = "3.3.2"

                    # Remove edges based on pad difference
                    pad_difference = pad_subtree_amount - pad_subtree_edges_amount

                    truncated_subtree_edge_indices = get_most_frequent_edges_to_truncate(subtree_edge_indices,
                                                                                         truncation_amount=pad_difference)
                    subtree_edge_indices = [item for idx, item in enumerate(subtree_edge_indices)
                                            if idx not in truncated_subtree_edge_indices]
                    subtree_edge_features = [seq for idx, seq in enumerate(subtree_edge_features) if
                                             idx not in truncated_subtree_edge_indices]
                    subtree_node_indices = [(item, idx) for idx, item in enumerate(subtree_node_indices) if
                                            item not in truncated_subtree_edge_indices]
                    subtree_segment_indexes = [item[1] for item in subtree_node_indices]
                    subtree_node_indices = [item[0] for item in subtree_node_indices]
                    subtree_node_segments = [item for idx, item in enumerate(subtree_node_segments) if
                                             idx in subtree_segment_indexes]
                    subtree_directionality_mask = [item for idx, item in enumerate(subtree_directionality_mask) if
                                                   idx in subtree_segment_indexes]

                    # Pad
                    pad_subtree_edges_amount = max_graph_subtree_edges - len(subtree_edge_indices)

                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_max = max(subtree_node_segments)
                    subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                             (
                                                                     subtree_node_segments_max + 1) + pad_subtree_edges_amount).tolist()
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

                # Case 3.3.3: if more edges than nodes
                if pad_subtree_amount < pad_subtree_edges_amount:
                    debug = "3.3.3"

                    pad_difference = pad_subtree_edges_amount - pad_subtree_amount

                    # Pad until node padding and then do not add new nodes
                    subtree_edge_indices = subtree_edge_indices + [[0, 0] for _ in range(pad_subtree_edges_amount)]
                    subtree_edge_mask = [1] * len(subtree_edge_features) + [0] * pad_subtree_edges_amount
                    subtree_edge_features += [[0] * max_graph_subtree_depth] * pad_subtree_edges_amount
                    subtree_node_indices += [max(subtree_node_indices)] * pad_subtree_edges_amount * 2
                    subtree_node_segments_max = max(subtree_node_segments)
                    subtree_node_segments_to_add = np.arange(subtree_node_segments_max + 1,
                                                             (
                                                                     subtree_node_segments_max + 1) + pad_subtree_amount).tolist()
                    subtree_node_segments_to_add += [max(subtree_node_segments_to_add)] * pad_difference
                    subtree_node_segments = subtree_node_segments + [val for val in subtree_node_segments_to_add for _
                                                                     in
                                                                     (0, 1)]
                    subtree_directionality_mask += [1] * pad_subtree_edges_amount * 2

        try:
            assert len(subtree_indices) == max_graph_subtrees
            assert len(subtree_indices_mask) == max_graph_subtrees
            assert len(subtree_edge_features) == max_graph_subtree_edges
            assert len(subtree_edge_indices) == max_graph_subtree_edges
            assert len(subtree_edge_mask) == max_graph_subtree_edges
            assert len(subtree_node_indices) == max_graph_subtree_edges * 2
            assert len(subtree_node_segments) == max_graph_subtree_edges * 2
            assert len(subtree_directionality_mask) == max_graph_subtree_edges * 2
            # assert max([item for pair in subtree_edge_indices for item in pair]) == len(subtree_indices) - 1
            assert len(set(subtree_node_segments)) == len(subtree_indices)
        except AssertionError as e:
            print(e)
            print("Debug section: ", debug)
            print()

        # Flatten
        subtree_indices = [item for seq in subtree_indices for item in seq]
        subtree_indices_mask = [item for seq in subtree_indices_mask for item in seq]
        subtree_edge_indices = [item for seq in subtree_edge_indices for item in seq]
        subtree_edge_features = [item for seq in subtree_edge_features for item in seq]

        assert len(subtree_indices) == max_graph_subtrees * max_graph_subtree_size
        assert len(subtree_indices_mask) == max_graph_subtrees * max_graph_subtree_size
        assert len(subtree_edge_indices) == max_graph_subtree_edges * 2
        assert len(subtree_edge_features) == max_graph_subtree_edges * max_graph_subtree_depth

        feature = cls(node_ids=node_ids, edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, directionality_mask=directionality_mask,
                      subtree_indices=subtree_indices, subtree_indices_mask=subtree_indices_mask,
                      subtree_edge_indices=subtree_edge_indices, subtree_edge_mask=subtree_edge_mask,
                      subtree_edge_features=subtree_edge_features, subtree_node_indices=subtree_node_indices,
                      subtree_node_segments=subtree_node_segments,
                      subtree_directionality_mask=subtree_directionality_mask)
        return feature


class TextGraphCharFeatures(TextGraphFeatures):

    def __init__(self, node_chars_ids, **kwargs):
        super(TextGraphCharFeatures, self).__init__(**kwargs)
        self.node_chars_ids = node_chars_ids

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextGraphCharFeatures, cls).get_mappings(conversion_args, has_labels=has_labels)

        max_graph_chars = conversion_args['max_graph_chars']
        max_graph_nodes = conversion_args['max_graph_nodes']

        mappings['node_chars_ids'] = tf.io.FixedLenFeature([max_graph_nodes * max_graph_chars], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextGraphCharFeatures, cls).get_feature_records(feature)
        features['node_chars_ids'] = create_int_feature(feature.node_chars_ids)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                'node_chars_ids': record['node_chars_ids'],
                'edge_features': record['edge_features'],
                'edge_indices': record['edge_indices'],
                'edge_mask': record['edge_mask'],
                'node_indices': record['node_indices'],
                'node_segments': record['node_segments'],
                'directionality_mask': record['directionality_mask']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask = retrieve_tree_info(example.graph,
                                                 is_directional=converter_args['is_directional'])

        node_chars = [[c for c in word] for word in nodes]
        tokenizer_input = {'word': ' '.join(nodes), 'char': node_chars}
        node_ids, node_chars_ids = tokenizer.convert_tokens_to_ids(tokenizer_input)
        edge_features_ids = [convert_number_to_binary_list(features) for features in edge_features]

        return node_ids, node_chars_ids, edge_indices, edge_features_ids, \
               node_indices, node_segments, directionality_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        max_graph_depth = conversion_args['max_graph_depth']
        max_graph_chars = conversion_args['max_graph_chars']

        truncated = False

        node_ids, node_chars_ids, edge_indices, edge_features, \
        node_indices, node_segments, directionality_mask, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_chars
        node_chars_ids = [item + [0] * (max_graph_chars - len(item)) if len(item) < max_graph_chars
                          else item[:max_graph_chars]
                          for item in node_chars_ids]
        node_chars_ids = node_chars_ids + [[0] * max_graph_chars for _ in range(pad_nodes_amount)]
        node_chars_ids = np.array(node_chars_ids).flatten()
        node_chars_ids = node_chars_ids[:max_graph_nodes * max_graph_chars]

        # max_graph_edges, 2
        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges, max_graph_depth
        edge_features = list(map(
            lambda item: [0] * (max_graph_depth - len(item)) + item
            if max_graph_depth - len(item) > 0 else item[:max_graph_depth],
            edge_features))
        if not truncated:
            edge_features = edge_features + [[0] * max_graph_depth for _ in
                                             range(max_graph_edges - len(edge_features))]
        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1,
                                             (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        assert len(node_ids) == max_graph_nodes
        assert len(node_chars_ids) == max_graph_chars * max_graph_nodes
        assert len(edge_features) == max_graph_edges
        assert len(edge_indices) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        # Flatten
        edge_features = [item for seq in edge_features for item in seq]
        edge_indices = [item for seq in edge_indices for item in seq]

        feature = cls(node_ids=node_ids, node_chars_ids=node_chars_ids,
                      edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, directionality_mask=directionality_mask)
        return feature


class TextDepGraphCharFeatures(TextDepGraphFeatures):

    def __init__(self, node_chars_ids, edge_char_features_ids, **kwargs):
        super(TextDepGraphCharFeatures, self).__init__(**kwargs)
        self.node_chars_ids = node_chars_ids
        self.edge_char_features_ids = edge_char_features_ids

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(TextDepGraphCharFeatures, cls).get_mappings(conversion_args, has_labels=has_labels)

        max_graph_chars = conversion_args['max_graph_chars']
        max_graph_edges = conversion_args['max_graph_edges']
        max_graph_nodes = conversion_args['max_graph_nodes']

        mappings['node_chars_ids'] = tf.io.FixedLenFeature([max_graph_nodes * max_graph_chars], tf.int64)
        mappings['edge_char_features_ids'] = tf.io.FixedLenFeature([max_graph_edges * max_graph_chars], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(TextDepGraphCharFeatures, cls).get_feature_records(feature)
        features['node_chars_ids'] = create_int_feature(feature.node_chars_ids)
        features['edge_char_features_ids'] = create_int_feature(feature.edge_char_features_ids)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'node_ids': record['node_ids'],
                'node_chars_ids': record['node_chars_ids'],
                'edge_features': record['edge_features'],
                'edge_char_features_ids': record['edge_char_features_ids'],
                'edge_indices': record['edge_indices'],
                'edge_mask': record['edge_mask'],
                'node_indices': record['node_indices'],
                'node_segments': record['node_segments'],
                'directionality_mask': record['directionality_mask']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        nodes, edge_indices, edge_features, \
        node_indices, node_segments, \
        directionality_mask = retrieve_dep_tree_info(example.graph,
                                                     is_directional=converter_args['is_directional'])
        node_chars = [[c for c in word] for word in nodes]
        features_chars = [[c for c in word] for word in edge_features]

        tokenizer_input = {'word': ' '.join(nodes), 'char': node_chars}
        node_ids, node_chars_ids = tokenizer.convert_tokens_to_ids(tokenizer_input)

        tokenizer_input = {'word': ' '.join(edge_features), 'char': features_chars}
        edge_features_ids, edge_char_features_ids = tokenizer.convert_tokens_to_ids(tokenizer_input)

        return node_ids, node_chars_ids, edge_indices, edge_features_ids, edge_char_features_ids, \
               node_indices, node_segments, directionality_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextGraphExample):
            raise AttributeError('Expected TextGraphExample instance, got: {}'.format(type(example)))

        max_graph_nodes = conversion_args['max_graph_nodes']
        max_graph_edges = conversion_args['max_graph_edges']
        max_graph_chars = conversion_args['max_graph_chars']

        truncated = False

        node_ids, node_chars_ids, edge_indices, edge_features, edge_char_features_ids, \
        node_indices, node_segments, directionality_mask, \
        label_id, label_map = cls.convert_example(example,
                                                  label_list,
                                                  tokenizer,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        # Padding
        pad_nodes_amount = max_graph_nodes - len(node_ids)
        pad_edges_amount = max_graph_edges - len(edge_features)

        if pad_nodes_amount < 0:
            truncated = True

        # max_graph_nodes
        node_ids = node_ids + [0] * pad_nodes_amount
        node_ids = node_ids[:max_graph_nodes]

        # max_graph_nodes * max_graph_chars
        node_chars_ids = [item + [0] * (max_graph_chars - len(item)) if len(item) < max_graph_chars
                          else item[:max_graph_chars]
                          for item in node_chars_ids]
        node_chars_ids = node_chars_ids + [[0] * max_graph_chars for _ in range(pad_nodes_amount)]
        node_chars_ids = np.array(node_chars_ids).flatten()
        node_chars_ids = node_chars_ids[:max_graph_nodes * max_graph_chars]

        # max_graph_edges, 2
        if not truncated:
            edge_indices = edge_indices + [[0, 0] for _ in range(pad_edges_amount)]
        else:
            edge_indices = [(pair, idx) for idx, pair in enumerate(edge_indices)
                            if pair[0] in np.arange(max_graph_nodes) and pair[1] in np.arange(max_graph_nodes)]
            truncated_indexes = [item[1] for item in edge_indices]
            edge_indices = [item[0] for item in edge_indices]

        # max_graph_edges
        if not truncated:
            edge_mask = [1] * len(edge_features) + [0] * pad_edges_amount
        else:
            edge_mask = [1] * max_graph_edges

        # max_graph_edges * 2
        if not truncated:
            node_indices = node_indices + [max(node_indices)] * pad_edges_amount * 2
        else:
            node_indices = [(item, idx) for idx, item in enumerate(node_indices) if item in truncated_indexes]
            segment_indexes = [item[1] for item in node_indices]
            node_indices = [item[0] for item in node_indices]

        # max_graph_edges * 2
        if not truncated:
            node_segments_max = max(node_segments)
            node_segments_to_add = np.arange(node_segments_max + 1,
                                             (node_segments_max + 1) + pad_edges_amount).tolist()
            node_segments = node_segments + [val for val in node_segments_to_add for _ in (0, 1)]
        else:
            node_segments = [item for idx, item in enumerate(node_segments) if idx in segment_indexes]

        # max_graph_edges,
        if not truncated:
            edge_features = edge_features + [0] * pad_edges_amount
        else:
            edge_features = [seq for idx, seq in enumerate(edge_features) if idx in truncated_indexes]

        # max_graph_edges * max_graph_chars
        edge_char_features_ids = [item + [0] * (max_graph_chars - len(item)) if len(item) < max_graph_chars
                                  else item[:max_graph_chars]
                                  for item in edge_char_features_ids]
        if not truncated:
            edge_char_features_ids = edge_char_features_ids + [[0] * max_graph_chars for _ in range(pad_edges_amount)]
        else:
            edge_char_features_ids = [seq for idx, seq in enumerate(edge_char_features_ids) if idx in truncated_indexes]
        edge_char_features_ids = np.array(edge_char_features_ids).flatten()
        edge_char_features_ids = edge_char_features_ids[:max_graph_edges * max_graph_chars]

        # max_graph_edges * 2
        if not truncated:
            directionality_mask += [1] * pad_edges_amount * 2
        else:
            directionality_mask = [item for idx, item in enumerate(directionality_mask) if idx in segment_indexes]

        assert len(node_ids) == max_graph_nodes
        assert len(node_chars_ids) == max_graph_chars * max_graph_nodes
        assert len(edge_features) == max_graph_edges
        assert len(edge_indices) == max_graph_edges
        assert len(edge_mask) == max_graph_edges
        assert len(node_indices) == max_graph_edges * 2
        assert len(node_segments) == max_graph_edges * 2
        assert len(directionality_mask) == max_graph_edges * 2

        # Flatten
        edge_indices = [item for seq in edge_indices for item in seq]

        feature = cls(node_ids=node_ids, node_chars_ids=node_chars_ids,
                      edge_features=edge_features, edge_indices=edge_indices,
                      node_indices=node_indices, node_segments=node_segments, edge_mask=edge_mask,
                      label_id=label_id, edge_char_features_ids=edge_char_features_ids,
                      directionality_mask=directionality_mask)
        return feature


class BertTextFeatures(Features):

    def __init__(self, input_ids, attention_mask, label_id):
        self.input_ids = input_ids
        self.attention_mask = attention_mask
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        max_seq_length = conversion_args['max_seq_length']
        num_labels = conversion_args['num_labels']

        mappings = {
            'input_ids': tf.io.FixedLenFeature([max_seq_length], tf.int64),
            'attention_mask': tf.io.FixedLenFeature([max_seq_length], tf.int64),
        }

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = OrderedDict()
        features['input_ids'] = create_int_feature(feature.input_ids)
        features['attention_mask'] = create_int_feature(feature.attention_mask)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'input_ids': record['input_ids'],
                'attention_mask': record['attention_mask'],
            }
            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        inputs = tokenizer.tokenizer.encode_plus(
            example.text,
            None,
            add_special_tokens=True,
            max_length=min(converter_args['max_tokens_limit'], 512),
            truncation=True
        )
        input_ids, \
        attention_mask = inputs["input_ids"], inputs['attention_mask']

        assert np.equal(attention_mask, [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)).all()

        return input_ids, attention_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        input_ids, attention_mask, label_id, label_map = cls.convert_example(example=example,
                                                                             label_list=label_list,
                                                                             tokenizer=tokenizer,
                                                                             has_labels=has_labels,
                                                                             converter_args=converter_args,
                                                                             conversion_args=conversion_args)

        # Padding

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        # attention_mask = [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)
        if converter_args['pad_on_left']:
            input_ids = ([converter_args['pad_token']] * padding_length) + input_ids
            attention_mask = ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length) + attention_mask
        else:
            input_ids = input_ids + ([converter_args['pad_token']] * padding_length)
            attention_mask = attention_mask + ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length)

        input_ids = input_ids[:max_seq_length]
        attention_mask = attention_mask[:max_seq_length]

        assert len(input_ids) == max_seq_length, "Error with input length {} vs {}".format(len(input_ids),
                                                                                           max_seq_length)
        assert len(attention_mask) == max_seq_length, "Error with input length {} vs {}".format(len(attention_mask),
                                                                                                max_seq_length)

        feature = cls(input_ids=input_ids, label_id=label_id,
                      attention_mask=attention_mask)
        return feature


class PosBertTextFeatures(BertTextFeatures):

    def __init__(self, position, **kwargs):
        super(PosBertTextFeatures, self).__init__(**kwargs)
        self.position = position

    @classmethod
    def get_mappings(cls, conversion_args, has_labels=True):
        mappings = super(PosBertTextFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                            has_labels=has_labels)
        mappings['position'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature):
        features = super(PosBertTextFeatures, cls).get_feature_records(feature=feature)
        features['position'] = create_int_feature([feature.position])

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'input_ids': record['input_ids'],
                'attention_mask': record['attention_mask'],
                'position': record['position']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None):

        if not isinstance(example, PosTextExample):
            raise AttributeError('Expected PosTextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        input_ids, attention_mask, label_id, label_map = cls.convert_example(example=example,
                                                                             label_list=label_list,
                                                                             tokenizer=tokenizer,
                                                                             has_labels=has_labels,
                                                                             converter_args=converter_args,
                                                                             conversion_args=conversion_args)

        # Padding

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        # attention_mask = [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)
        if converter_args['pad_on_left']:
            input_ids = ([converter_args['pad_token']] * padding_length) + input_ids
            attention_mask = ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length) + attention_mask
        else:
            input_ids = input_ids + ([converter_args['pad_token']] * padding_length)
            attention_mask = attention_mask + ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length)

        input_ids = input_ids[:max_seq_length]
        attention_mask = attention_mask[:max_seq_length]

        assert len(input_ids) == max_seq_length, "Error with input length {} vs {}".format(len(input_ids),
                                                                                           max_seq_length)
        assert len(attention_mask) == max_seq_length, "Error with input length {} vs {}".format(len(attention_mask),
                                                                                                max_seq_length)

        feature = cls(input_ids=input_ids, label_id=label_id,
                      attention_mask=attention_mask, position=example.position)
        return feature




class FeatureFactory(object):
    supported_features = {
        'text_features': TextFeatures,
        'text_elmo_features': TextELMOFeatures,
        'text_simple_graph_features': TextSimpleGraphFeatures,
        'text_simple_dep_graph_features': TextSimpleDepGraphFeatures,
        'text_simple_dual_graph_features': TextSimpleDualGraphFeatures,
        'text_simple_dual_dep_graph_features': TextSimpleDualDepGraphFeatures,
        'text_graph_features': TextGraphFeatures,
        'text_dual_graph_features': TextDualGraphFeatures,
        'text_graph_adj_features': TextGraphAdjFeatures,
        'text_graph_dep_adj_features': TextGraphDepAdjFeatures,
        'text_graph_pooled_adj_features': TextGraphPooledAdjFeatures,
        'text_graph_tree_pooled_adj_features': TextGraphTreePooledAdjFeatures,
        'text_graph_pooled_dep_adj_features': TextGraphPooledDepAdjFeatures,
        'text_hierarchical_graph_features': TextHierarchicalGraphFeatures,
        'text_hierarchical_dep_graph_features': TextHierarchicalDepGraphFeatures,
        'text_dep_graph_features': TextDepGraphFeatures,
        'text_graph_char_features': TextGraphCharFeatures,
        'text_dep_graph_char_features': TextDepGraphCharFeatures,

        'text_dual_graph_adj_features': TextDualGraphAdjFeatures,
        'pos_text_dual_graph_adj_features': PosTextDualGraphAdjFeatures,

        'pos_text_features': PosTextFeatures,
        'pos_text_simple_graph_features': PosTextSimpleGraphFeatures,
        'pos_text_simple_dep_graph_features': PosTextSimpleDepGraphFeatures,
        'pos_text_simple_dual_graph_features': PosTextSimpleDualGraphFeatures,
        'pos_text_graph_adj_features': PosTextGraphAdjFeatures,
        'pos_text_graph_dep_adj_features': PosTextGraphDepAdjFeatures,

        'text_multi_graph_adj_features': TextMultiGraphAdjFeatures,

        'bert_text_features': BertTextFeatures,
        'pos_bert_text_features': PosBertTextFeatures
    }
