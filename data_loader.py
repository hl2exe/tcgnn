"""

Dataset loading script

The data handle holds two type of data: all sentences for conversion, sampled pairs

"""

import os

import pandas as pd
import numpy as np

import const_define as cd


class IBM2015Loader(object):

    def load(self, selector='tree', merge=False, merge_label=None):
        # Step 1: load aggregated data
        # Topic Id  Topic   Data-set   Sentence  Label  Tree
        data_path = os.path.join(cd.IBM2015_DIR, 'dataset_final.csv')
        data = pd.read_csv(data_path)

        train_data = data[data['Data-set'] == 'train and test']
        validation_data = data[data['Data-set'] == 'held-out']

        if merge:
            assert merge_label is not None

            to_merge = 'C_Label' if merge_label == 'E_Label' else 'E_Label'

            train_data.loc[(train_data[to_merge] == 1) & (train_data[merge_label] == 0), 'NA_Label'] = 1
            validation_data.loc[(validation_data[to_merge] == 1) & (validation_data[merge_label] == 0), 'NA_Label'] = 1

            inference_labels = ['NA_Label', merge_label]
            num_classes = 2
        else:
            inference_labels = ['NA_Label', 'C_Label', 'E_Label']
            num_classes = 3

        data_keys = {'text': 'Sentence', 'tree': 'Tree' if selector == 'tree' else 'Dep_Tree'}

        return LooDataHandle(train_data=train_data, other_data=validation_data, is_validation=True,
                             data_name='IBM2015', num_classes=num_classes, label='Label',
                             data_keys=data_keys, inference_labels=inference_labels,
                             pairwise_labels=['C', 'E'])


class IBM2015CVLoader(object):

    def load(self, selector='tree', merge=False, merge_label=None):
        # Step 1: load aggregated data
        # Topic Id  Topic   Data-set   Sentence  Label  Tree
        data_path = os.path.join(cd.IBM2015_DIR, 'dataset_final.csv')
        data = pd.read_csv(data_path)

        train_data = data[data['Data-set'] == 'train and test']
        validation_data = data[data['Data-set'] == 'held-out']

        if merge:
            assert merge_label is not None

            to_merge = 'C_Label' if merge_label == 'E_Label' else 'E_Label'

            train_data.loc[(train_data[to_merge] == 1) & (train_data[merge_label] == 0), 'NA_Label'] = 1
            validation_data.loc[(validation_data[to_merge] == 1) & (validation_data[merge_label] == 0), 'NA_Label'] = 1

            inference_labels = ['NA_Label', merge_label]
            num_classes = 2
        else:
            inference_labels = ['NA_Label', 'C_Label', 'E_Label']
            num_classes = 3

        data_keys = {'text': 'Sentence', 'tree': 'Tree' if selector == 'tree' else 'Dep_Tree'}

        return DataHandle(data=train_data, data_name='IBM2015CV', num_classes=num_classes, label='Label',
                          data_keys=data_keys, inference_labels=inference_labels,
                          has_fixed_validation=True, val_data=validation_data)


class IBM2015CalibrationLoader(object):

    def load(self, selector='tree', merge=False, merge_label=None):
        # Step 1: load aggregated data
        # Topic Id  Topic   Data-set   Sentence  Label  Tree
        data_path = os.path.join(cd.IBM2015_DIR, 'dataset_final.csv')
        data = pd.read_csv(data_path)

        train_data = data[data['Data-set'] == 'train and test']
        validation_data = data[data['Data-set'] == 'held-out']

        if merge:
            assert merge_label is not None

            to_merge = 'C_Label' if merge_label == 'E_Label' else 'E_Label'

            train_data.loc[(train_data[to_merge] == 1) & (train_data[merge_label] == 0), 'NA_Label'] = 1
            validation_data.loc[(validation_data[to_merge] == 1) & (validation_data[merge_label] == 0), 'NA_Label'] = 1

            inference_labels = ['NA_Label', merge_label]
            num_classes = 2
        else:
            inference_labels = ['NA_Label', 'C_Label', 'E_Label']
            num_classes = 3

        data_keys = {'text': 'Sentence', 'tree': 'Tree' if selector == 'tree' else 'Dep_Tree'}

        return TrainAndTestDataHandle(train_data=train_data, validation_data=validation_data, build_validation=True,
                                      test_data=None,
                                      data_name='IBM2015Calibration', num_classes=num_classes, label='Label',
                                      data_keys=data_keys, inference_labels=inference_labels,
                                      pairwise_labels=['C', 'E'])


class ACL2018Loader(object):

    def load(self, selector='tree'):
        data_path = os.path.join(cd.ACL2018_DIR, '{}_mod_dep.csv')

        train_data = pd.read_csv(data_path.format('train'))
        test_data = pd.read_csv(data_path.format('test'))

        data_keys = {'text': 'candidate',
                     'tree': 'candidate_tree' if selector == 'tree' else 'candidate_dep_tree'}

        return TrainAndTestDataHandle(train_data=train_data, test_data=test_data, validation_data=None,
                                      data_name='ACL2018', data_keys=data_keys, num_classes=2,
                                      label='label', inference_labels=[0, 1], build_validation=False,
                                      pairwise_labels=[1])


class PersuasiveEssaysLoader(object):

    def load(self, selector='tree', merge=False):
        data_path = os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'dataset_final.csv')
        data = pd.read_csv(data_path)

        train_data = data[data.Split == 'TRAIN']
        test_data = data[data.Split == 'TEST']

        if merge:
            train_data.loc[train_data['Label'] == 'MajorClaim', 'Label'] = 'Claim'
            test_data.loc[test_data['Label'] == 'MajorClaim', 'Label'] = 'Claim'

            inference_labels = ['Claim', 'Premise']
            num_classes = 2
        else:
            inference_labels = ['Claim', 'MajorClaim', 'Premise']
            num_classes = 3

        data_keys = {'text': 'Sentence',
                     'tree': 'Tree' if selector == 'tree' else 'Dep_Tree',
                     'position': 'Sentence ID'}

        return TrainAndTestDataHandle(train_data=train_data, test_data=test_data, validation_data=None,
                                      data_name='PersuasiveEssays', data_keys=data_keys,
                                      num_classes=num_classes, label='Label',
                                      inference_labels=inference_labels,
                                      pairwise_labels=inference_labels)


class PersuasiveEssaysCalibrationLoader(object):

    def load(self, selector='tree', merge=False):
        data_path = os.path.join(cd.PERSUASIVE_ESSAYS_DIR, 'dataset_final.csv')
        data = pd.read_csv(data_path)

        if merge:
            data.loc[data['Label'] == 'MajorClaim', 'Label'] = 'Claim'

            inference_labels = ['Claim', 'Premise']
            num_classes = 2
        else:
            inference_labels = ['Claim', 'MajorClaim', 'Premise']
            num_classes = 3

        train_data = data[data.Split == 'TRAIN']
        test_data = data[data.Split == 'TEST']

        data_keys = {'text': 'Sentence',
                     'tree': 'Tree' if selector == 'tree' else 'Dep_Tree',
                     'position': 'Sentence ID'}

        return DataHandle(data=train_data, data_name='PersuasiveEssaysCalibration', data_keys=data_keys,
                          num_classes=num_classes, label='Label', inference_labels=inference_labels,
                          test_data=test_data, has_fixed_test=True, to_upper=False)


class AbstRCTLoader(object):

    def load(self, selector='tree', merge=True):
        data_path = os.path.join(cd.RCT_DIR, 'dataset_final.csv')
        data = pd.read_csv(data_path)

        train_data = data[data.Split == 'train']
        test_data = data[data.Split == 'test']
        val_data = data[data.Split == 'dev']

        if merge:
            train_data.loc[train_data['Label'] == 'MajorClaim', 'Label'] = 'Claim'
            test_data.loc[test_data['Label'] == 'MajorClaim', 'Label'] = 'Claim'
            val_data.loc[val_data['Label'] == 'MajorClaim', 'Label'] = 'Claim'

            inference_labels = ['Claim', 'Premise']
            num_classes = 2
        else:
            inference_labels = ['Claim', 'MajorClaim', 'Premise']
            num_classes = 3

        data_keys = {'text': 'Sentence',
                     'tree': 'Tree' if selector == 'tree' else 'Dep_Tree',
                     'position': 'Sentence ID'}

        return TrainAndTestDataHandle(train_data=train_data, test_data=test_data, validation_data=val_data,
                                      data_name='AbstRCT', data_keys=data_keys, build_validation=False,
                                      num_classes=num_classes, label='Label',
                                      inference_labels=inference_labels,
                                      pairwise_labels=inference_labels)


class UKPAspectLoader(object):

    def load(self, selector='tree', merge=True):
        data_path = os.path.join(cd.UKP_ASPECT_DIR, 'UKP_ASPECT_mod.tsv')
        data = pd.read_csv(data_path)

        if merge:
            data.loc[data['label'] == 'DTORCD', 'label'] = 'Dissimilar'
            data.loc[data['label'] == 'NS', 'label'] = 'Dissimilar'
            data.loc[data['label'] == 'SS', 'label'] = 'Similar'
            data.loc[data['label'] == 'HS', 'label'] = 'Similar'

            inference_labels = ['Dissimilar', 'Similar']
            num_classes = 2
        else:
            inference_labels = ['DTORCD', 'NS', 'SS', 'HS']
            num_classes = 4

        data_keys = {'text_1': 'sentence_1',
                     'text_2': 'sentence_2',
                     'tree_1': 'sentence_tree_1' if selector == 'tree' else 'sentence_dep_tree_1',
                     'tree_2': 'sentence_tree_2' if selector == 'tree' else 'sentence_dep_tree_2'}

        return DataHandle(data=data, data_name='UKP_Aspect', data_keys=data_keys,
                          num_classes=num_classes, label='label', inference_labels=inference_labels,
                          to_upper=False)


# TODO: add merge argument option
class UKPLoader(object):

    def load(self, merge=False, selector='tree'):
        data_path = os.path.join(cd.UKP_DIR, 'dataset_mod.csv')
        data = pd.read_csv(data_path)

        # Build original test splits (cross-topic)
        train_data = data[(data.topic != 'school uniforms') & (data.set == 'train')]
        val_data = data[(data.topic != 'school uniforms') & (data.set == 'val')]
        test_data = data[(data.topic == 'school uniforms') & (data.set == 'test')]

        if merge:
            train_data[train_data.annotation == 'Argument_against']['annotation'] = 'Argument'
            train_data[train_data.annotation == 'Argument_for']['annotation'] = 'Argument'

            val_data[val_data.annotation == 'Argument_against']['annotation'] = 'Argument'
            val_data[val_data.annotation == 'Argument_for']['annotation'] = 'Argument'

            test_data[test_data.annotation == 'Argument_against']['annotation'] = 'Argument'
            test_data[test_data.annotation == 'Argument_for']['annotation'] = 'Argument'

            inference_labels = ['Argument', 'NoArgument']
            pairwise_labels = ['Argument']
        else:
            inference_labels = ['Argument_against', 'Argument_for', 'NoArgument']
            pairwise_labels = ['Argument_against', 'Argument_for']

        data_keys = {'text': 'Sentence',
                     'tree': 'Tree' if selector == 'tree' else 'Dep_Tree'}

        return TrainAndTestDataHandle(train_data=train_data, test_data=test_data, validation_data=val_data,
                                      data_name='UKP2.0', data_keys=data_keys,
                                      num_classes=3, label='annotation',
                                      inference_labels=inference_labels,
                                      pairwise_labels=pairwise_labels)


class UKPLooLoader(object):

    def load(self, merge=False, selector='tree'):
        data_path = os.path.join(cd.UKP_DIR, 'dataset_mod.csv')
        data = pd.read_csv(data_path)

        if merge:
            data.loc[data.annotation == 'Argument_against', 'annotation'] = 'Argument'
            data.loc[data.annotation == 'Argument_for', 'annotation'] = 'Argument'

            inference_labels = ['Argument', 'NoArgument']
            pairwise_labels = ['Argument']
        else:
            inference_labels = ['Argument_against', 'Argument_for', 'NoArgument']
            pairwise_labels = ['Argument_against', 'Argument_for']

        data_keys = {'text': 'sentence',
                     'tree': 'Tree' if selector == 'tree' else 'Dep_Tree'}

        return LooDataHandle(train_data=data, other_data=None, has_splits=True, splits_keys={'train': 'train',
                                                                                             'val': 'val',
                                                                                             'test': 'test'},
                             split_key='set',
                             data_name='UKPLoo2.0', data_keys=data_keys,
                             num_classes=3, label='annotation',
                             inference_labels=inference_labels,
                             pairwise_labels=pairwise_labels)


class CORD19Loader(object):

    def load(self):
        data_path = os.path.join(cd.CORD19_DIR, 'all_dataset_filtered.csv')
        data = pd.read_csv(data_path)

        data_keys = {'text': 'Sentence', 'tree': 'Tree'}

        return UnseenDataHandle(data=data, data_name='CORD19', data_keys=data_keys)


class DataHandle(object):
    """
    General dataset wrapper. Additional behaviour can be expressed via attributes.
    """

    def __init__(self, data, data_name, num_classes, inference_labels,
                 data_keys, label=None, to_upper=True, has_fixed_test=False, test_data=None,
                 has_fixed_validation=False, val_data=None):
        self.data = data
        self.data_name = data_name
        self.num_classes = num_classes
        self.to_upper = to_upper
        self.inference_labels = inference_labels
        self.has_fixed_test = has_fixed_test
        self.test_data = test_data
        self.has_fixed_validation = has_fixed_validation
        self.val_data = val_data

        if label:
            category = label.upper() if to_upper else label
            self.label = category
        else:
            self.label = 'label'

        self.data_keys = data_keys

    def get_split(self, key_values, key=None, val_indexes=None, validation_percentage=None):
        if key is None:
            key = 'Unnamed: 0' if self.data.index.name is None else self.data.index.name

        train_df = self.data[np.logical_not(self.data[key].isin(key_values))]

        if self.has_fixed_test:
            # self.data should not contain test_data
            test_df = self.test_data
            val_df = self.data[self.data[key].isin(key_values)]
        else:
            test_df = self.data[self.data[key].isin(key_values)]

            if self.has_fixed_validation:
                val_df = self.val_data
            else:
                if val_indexes is None:
                    assert validation_percentage is not None

                    validation_amount = int(len(train_df) * validation_percentage)
                    train_amount = len(train_df) - validation_amount
                    val_df = train_df[train_amount:]
                    train_df = train_df[:train_amount]
                else:
                    val_df = train_df[train_df[key].isin(val_indexes)]
                    train_df = train_df[np.logical_not(train_df[key].isin(val_indexes))]

        return train_df, val_df, test_df

    def get_additional_info(self):
        return {
            'label': self.label,
            'inference_labels': self.inference_labels,
            'data_keys': self.data_keys
        }


class LooDataHandle(object):
    """
    General LOO dataset wrapper. Additional behaviour can be expressed via attributes.
    """

    def __init__(self, train_data, data_name, num_classes, inference_labels,
                 label, data_keys, other_data, is_validation=True, has_splits=False,
                 splits_keys=None, split_key=None, pairwise_labels=None):
        self.train_data = train_data
        self.inference_labels = inference_labels
        self.other_data = other_data
        self.data_name = data_name
        self.label = label
        self.num_classes = num_classes
        self.pairwise_labels = pairwise_labels
        self.is_validation = is_validation
        self.data_keys = data_keys
        self.has_splits = has_splits
        self.splits_keys = splits_keys
        self.split_key = split_key

    def get_loo_data(self, split_key):
        return np.unique(self.train_data[split_key].values)

    def get_split(self, key_values, key=None, validation_percentage=None):
        if key is None:
            key = 'Unnamed: 0' if self.train_data.index.name is None else self.train_data.index.name

        if type(key_values) not in [list, np.array, set]:
            key_values = [key_values]

        if not self.has_splits:
            train_df = self.train_data[~self.train_data[key].isin(key_values)]

            if self.is_validation:
                if validation_percentage is None:
                    val_df = self.other_data
                else:
                    validation_amount = int(len(train_df) * validation_percentage)
                    train_amount = len(train_df) - validation_amount
                    val_df = train_df[train_amount:]
                    train_df = train_df[:train_amount]

                test_df = self.train_data[self.train_data[key].isin(key_values)]

            else:
                test_df = self.other_data
                val_df = self.train_data[self.train_data[key].isin(key_values)]
        else:
            assert self.splits_keys is not None
            assert type(self.splits_keys) == dict

            # TODO: consider other scenarios

            train_df = self.train_data[(~self.train_data[key].isin(key_values)) &
                                       (self.train_data[self.split_key] == self.splits_keys['train'])]
            val_df = self.train_data[(~self.train_data[key].isin(key_values)) &
                                     (self.train_data[self.split_key] == self.splits_keys['val'])]
            test_df = self.train_data[(self.train_data[key].isin(key_values)) &
                                      (self.train_data[self.split_key] == self.splits_keys['test'])]

        return train_df, val_df, test_df

    def get_additional_info(self):
        return {'label': self.label,
                'inference_labels': self.inference_labels,
                'pairwise_labels': self.pairwise_labels,
                'data_keys': self.data_keys
                }


class TrainAndTestDataHandle(object):

    def __init__(self, train_data, test_data, data_name, num_classes, inference_labels,
                 label, data_keys, validation_data=None, build_validation=True, pairwise_labels=None):
        self.train_data = train_data
        self.test_data = test_data
        self.data_name = data_name
        self.num_classes = num_classes
        self.inference_labels = inference_labels
        self.label = label
        self.validation_data = validation_data
        self.data_keys = data_keys
        self.pairwise_labels = pairwise_labels
        self.build_validation = build_validation

    def get_data(self, validation_percentage=None):

        train_df = self.train_data
        test_df = self.test_data

        if self.build_validation:
            if validation_percentage is None:
                val_df = self.validation_data
            else:
                validation_amount = int(len(train_df) * validation_percentage)
                train_amount = len(train_df) - validation_amount
                val_df = train_df[train_amount:]
                train_df = train_df[:train_amount]
        else:
            val_df = self.validation_data

        return train_df, val_df, test_df

    def get_additional_info(self):
        return {'label': self.label,
                'inference_labels': self.inference_labels,
                'pairwise_labels': self.pairwise_labels,
                'data_keys': self.data_keys
                }


class UnseenDataHandle(object):

    def __init__(self, data, data_name, data_keys):
        self.data = data
        self.data_name = data_name
        self.data_keys = data_keys

    def get_data(self):
        return self.data

    def get_additional_info(self):
        return {
            'data_keys': self.data_keys
        }


class DataLoaderFactory(object):
    supported_data_loaders = {
        'ibm2015': IBM2015Loader,
        'ibm2015calibration': IBM2015CalibrationLoader,
        'ibm2015merged': IBM2015Loader,
        'ibm2015calibrationmerged': IBM2015CalibrationLoader,
        'ibm2015cv': IBM2015CVLoader,
        'ibm2015cvmerged': IBM2015CVLoader,
        'acl2018': ACL2018Loader,
        'persuasive_essays': PersuasiveEssaysLoader,
        'persuasive_essays_calibration': PersuasiveEssaysCalibrationLoader,
        'abst_rct': AbstRCTLoader,
        'ukp': UKPLoader,
        'ukp_loo': UKPLooLoader,
        'cord19': CORD19Loader,
        'ukp_aspect': UKPAspectLoader
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if DataLoaderFactory.supported_data_loaders[key]:
            return DataLoaderFactory.supported_data_loaders[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
