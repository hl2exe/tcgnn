"""


"""

import pandas as pd
from tqdm import tqdm

from utility import preprocessing_utils
from sample_wrappers import TextExampleList, TextExample, TextGraphExampleList, TextGraphExample,\
    PosTextGraphExample, PosTextExample, TextDualExample, TextMultiGraphExample, TextMultiGraphExampleList, \
    PosTextDualExample


# TODO: add preprocessing filter functions
class DataProcessor(object):
    """Base class for data converters for sequence classification data sets."""

    def __init__(self, loader_info, filter_names=None, retrieve_label=True):
        self.loader_info = loader_info
        self.filter_names = filter_names if filter_names is not None else preprocessing_utils.filter_methods
        self.retrieve_label = retrieve_label

    def get_train_examples(self, filepath=None, ids=None, data=None):
        """Gets a collection of `Example`s for the train set."""
        raise NotImplementedError()

    def get_dev_examples(self, filepath=None, ids=None, data=None):
        """Gets a collection of `Example`s for the dev set."""
        raise NotImplementedError()

    def get_test_examples(self, filepath=None, ids=None, data=None):
        """Gets a collection of `Example`s for the test set."""
        raise NotImplementedError()

    def get_labels(self):
        """Gets the list of labels for this data set."""
        return self.loader_info['inference_labels'] if self.retrieve_label else None

    def get_processor_name(self):
        """Gets the string identifier of the processor."""
        return self.loader_info['data_name']

    @classmethod
    def read_csv(cls, input_file, quotechar=None):
        """Reads a tab separated value file."""
        df = pd.read_csv(input_file)
        return df


class TextProcessor(DataProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text = preprocessing_utils.filter_line(row[self.loader_info['data_keys']['text']],
                                                   function_names=self.filter_names)
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = TextExample(guid=guid, text=text, label=label)
            examples.append(example)

        return examples

    def get_train_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='train')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='train')

    def get_dev_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='dev')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='dev')

    def get_test_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='test')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='test')


class PosTextProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text = preprocessing_utils.filter_line(row[self.loader_info['data_keys']['text']],
                                                   function_names=self.filter_names)
            position = row[self.loader_info['data_keys']['position']]
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = PosTextExample(guid=guid, text=text, label=label, position=position)
            examples.append(example)

        return examples


class ML_TextProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text = preprocessing_utils.filter_line(row[self.loader_info['data_keys']['text']],
                                                   function_names=self.filter_names)
            if self.retrieve_label:
                label = [int(row[inf_label]) for inf_label in self.loader_info['inference_labels']]
            else:
                label = None
            example = TextExample(guid=guid, text=text, label=label)
            examples.append(example)

        return examples


class TextTreeProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextGraphExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = row_id
            tree = row[self.loader_info['data_keys']['tree']]
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = TextGraphExample(guid=guid, graph=tree, label=label)
            examples.append(example)

        return examples


class TextMultiTreeProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextMultiGraphExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = row_id
            tree_1 = row[self.loader_info['data_keys']['tree_1']]
            tree_2 = row[self.loader_info['data_keys']['tree_2']]
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = TextMultiGraphExample(guid=guid, graph_1=tree_1, graph_2=tree_2, label=label)
            examples.append(example)

        return examples


class TextDualProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextGraphExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = row_id
            text = row[self.loader_info['data_keys']['text']]
            tree = row[self.loader_info['data_keys']['tree']]
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = TextDualExample(guid=guid, text=text, graph=tree, label=label)
            examples.append(example)

        return examples


class PosTextDualProcessor(TextDualProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextGraphExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = row_id
            text = row[self.loader_info['data_keys']['text']]
            tree = row[self.loader_info['data_keys']['tree']]
            position = row[self.loader_info['data_keys']['position']]
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = PosTextDualExample(guid=guid, text=text, graph=tree, label=label, position=position)
            examples.append(example)

        return examples


class PosTextTreeProcessor(TextTreeProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextGraphExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = row_id
            tree = row[self.loader_info['data_keys']['tree']]
            position = row[self.loader_info['data_keys']['position']]
            if self.retrieve_label:
                label = row[self.loader_info['label']]
            else:
                label = None
            example = PosTextGraphExample(guid=guid, graph=tree, label=label, position=position)
            examples.append(example)

        return examples


class ML_TextTreeProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextGraphExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = row_id
            tree = row[self.loader_info['data_keys']['tree']]
            if self.retrieve_label:
                label = [int(row[inf_label]) for inf_label in self.loader_info['inference_labels']]
            else:
                label = None
            example = TextGraphExample(guid=guid, graph=tree, label=label)
            examples.append(example)

        return examples


class ProcessorFactory(object):
    supported_processors = {
        'text_processor': TextProcessor,
        'ml_text_processor': ML_TextProcessor,
        'text_tree_processor': TextTreeProcessor,
        'ml_text_tree_processor': ML_TextTreeProcessor,

        'pos_text_processor': PosTextProcessor,
        'pos_text_tree_processor': PosTextTreeProcessor,

        'text_dual_processor': TextDualProcessor,
        'pos_text_dual_processor': PosTextDualProcessor,

        'text_multi_tree_processor': TextMultiTreeProcessor
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ProcessorFactory.supported_processors[key]:
            return ProcessorFactory.supported_processors[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
