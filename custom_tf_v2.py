"""

@Author: Federico Ruggeri

@Date: 24/07/19

Core part of Tensorflow 2 models. Each network defined in nn_models_v2.py has a corresponding model here.

"""

from collections import OrderedDict

import numpy as np
import tensorflow as tf
from transformers import TFBertModel

from utility.log_utils import Logger


######################################################
####################### MODELS #######################
######################################################

# GNNs

# TODO: add layer normalization
class M_Pairwise_GNN(tf.keras.Model):

    def __init__(self, embedding_dimension, vocab_size,
                 graph_max_nodes, timesteps, node_mlp_weights,
                 message_weights, gate_mlp_weights,
                 aggregation_mlp_weights, representation_mlp_weights, graph_max_depth=None, edge_mlp_weights=None,
                 l2_regularization=0., input_dropout_rate=0.4, rnn_encoding=False,
                 dropout_rate=0.4, embedding_matrix=None, repeated_shared=True, batch_size=32, **kwargs):
        super(M_Pairwise_GNN, self).__init__(**kwargs)
        self.timesteps = timesteps
        self.graph_max_nodes = graph_max_nodes
        self.graph_max_depth = graph_max_depth
        self.node_mlp_weights = node_mlp_weights if type(node_mlp_weights) is list else list(node_mlp_weights)
        self.edge_mlp_weights = edge_mlp_weights if type(edge_mlp_weights) is list else list(edge_mlp_weights)
        self.message_weights = message_weights if type(message_weights) is list else list(message_weights)
        self.gate_mlp_weights = gate_mlp_weights if type(gate_mlp_weights) is list else list(gate_mlp_weights)
        self.aggregation_mlp_weights = aggregation_mlp_weights if type(aggregation_mlp_weights) is list else list(
            aggregation_mlp_weights)
        self.representation_mlp_weights = representation_mlp_weights if type(
            representation_mlp_weights) is list else list(representation_mlp_weights)
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.input_dropout_rate = input_dropout_rate
        self.embedding_dimension = embedding_dimension
        self.rnn_encoding = rnn_encoding
        self.repeated_shared = repeated_shared
        self.batch_size = batch_size

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=graph_max_nodes,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='input_embedding')

        self.depth_embedding = tf.keras.layers.Embedding(input_dim=self.graph_max_depth + 1,
                                                         output_dim=int(
                                                             np.ceil(np.log(self.graph_max_depth) / np.log(2))),
                                                         input_length=graph_max_nodes - 1,
                                                         weights=None,
                                                         mask_zero=True,
                                                         name='depth_embedding')

        self.position_embedding = tf.keras.layers.Embedding(input_dim=self.graph_max_nodes - 1,
                                                            output_dim=int(
                                                                np.ceil(np.log(self.graph_max_nodes - 1) / np.log(2))),
                                                            input_length=graph_max_nodes - 1,
                                                            weights=None,
                                                            mask_zero=True,
                                                            name='position_embedding')

        self.input_dropout = tf.keras.layers.Dropout(self.input_dropout_rate)

        if rnn_encoding:
            assert embedding_dimension % 2 == 0
            self.rnn_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=embedding_dimension // 2,
                                                                                  return_sequences=True))

        self.edge_blocks = []
        for idx, weight in enumerate(edge_mlp_weights):
            self.edge_blocks.append(tf.keras.layers.Dense(units=weight,
                                                          activation=tf.nn.leaky_relu,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              l2_regularization)))
            self.edge_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        if self.repeated_shared:
            # 1 GCN layer repeated k times
            self.node_mlp_weights.append(self.embedding_dimension)
            gcn_shared = MP_GCN(node_weights=self.node_mlp_weights,
                                edge_weights=edge_mlp_weights,
                                message_weights=message_weights,
                                l2_regularization=l2_regularization,
                                dropout_rate=dropout_rate,
                                node_encoding=True,
                                edge_encoding=False)
            self.gcn_blocks = [gcn_shared for _ in range(timesteps)]
        else:
            # K GCN layers where K is the number of weights
            if not len(node_mlp_weights) == len(message_weights):
                Logger.get_logger(__name__).warn('Node encoding and Message passing weights are not of the same size!'
                                                 ' Replicating the shortest one to ensure alignment...(Is this on purpose?)')
                if len(node_mlp_weights) < len(message_weights):
                    self.node_mlp_weights += [self.node_mlp_weights[-1]] * (
                            len(self.message_weights) - len(self.node_mlp_weights))
                else:
                    self.message_weights += [self.message_weights[-1]] * (
                            len(self.node_mlp_weights) - len(self.message_weights))

            self.gcn_blocks = [MP_GCN(node_weights=[node_w],
                                      edge_weights=[edge_w],
                                      message_weights=[message_w],
                                      l2_regularization=l2_regularization,
                                      dropout_rate=dropout_rate,
                                      node_encoding=True,
                                      edge_encoding=False)
                               for node_w, edge_w, message_w in
                               zip(node_mlp_weights, edge_mlp_weights, message_weights)]
            self.timesteps = len(node_mlp_weights)

        self.pooling = GatedAggregation(aggregation_weights=aggregation_mlp_weights,
                                        gate_weights=gate_mlp_weights,
                                        l2_regularization=l2_regularization,
                                        dropout_rate=dropout_rate)

        # Graph embedding
        self.representation_blocks = []
        for idx, weight in enumerate(representation_mlp_weights):
            self.representation_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                    activation=tf.nn.leaky_relu if idx < len(
                                                                        representation_mlp_weights) - 1 else None,
                                                                    kernel_regularizer=tf.keras.regularizers.l2(
                                                                        l2_regularization)))

            if idx < len(representation_mlp_weights) - 1:
                self.representation_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        arg0 = inputs['left_node_ids']
        arg1 = inputs['right_node_ids']

        # [batch_size, max_edges, max_depth]
        arg0_edge_features = inputs['left_edge_features']
        arg1_edge_features = inputs['right_edge_features']

        arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, 2])
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        arg1_edge_features = tf.reshape(arg1_edge_features, [arg1_edge_features.shape[0], -1, 2])
        arg1_edge_features = tf.cast(arg1_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['left_edge_indices']
        arg1_edge_indices = inputs['right_edge_indices']

        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])
        arg1_edge_indices = tf.reshape(arg1_edge_indices, [arg1_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['left_edge_mask']
        arg1_edge_mask = inputs['right_edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['left_node_indices']
        arg1_node_indices = inputs['right_node_indices']

        # [batch_size * max_edges * 2]
        arg0_node_segments = inputs['left_node_segments']
        arg1_node_segments = inputs['right_node_segments']

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['left_directionality_mask']
        arg1_directionality_mask = inputs['right_directionality_mask']

        # Compute batch-wise node segments for correct segment sum
        arg0_sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        arg0_batch_indexes = tf.range(arg0_node_segments.shape[0])
        arg0_to_add = (arg0_sample_max + 1) * arg0_batch_indexes
        arg0_to_add = tf.expand_dims(arg0_to_add, axis=-1)
        arg0_node_segments += arg0_to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # Compute batch-wise node segments for correct segment sum
        arg1_sample_max = tf.reduce_max(arg1_node_segments, axis=1)
        arg1_batch_indexes = tf.range(arg1_node_segments.shape[0])
        arg1_to_add = (arg1_sample_max + 1) * arg1_batch_indexes
        arg1_to_add = tf.expand_dims(arg1_to_add, axis=-1)
        arg1_node_segments += arg1_to_add

        arg1_node_segments = tf.reshape(arg1_node_segments, [-1, ])

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb)
        arg1_emb = self.input_embedding(arg1)
        arg1_emb = self.input_dropout(arg1_emb)

        arg0_depth_edge_features = arg0_edge_features[:, :, 0]
        arg0_position_edge_features = arg0_edge_features[:, :, 1]
        arg0_depth_edge_features_emb = self.depth_embedding(arg0_depth_edge_features)
        arg0_position_edge_features_emb = self.position_embedding(arg0_position_edge_features)
        arg0_edge_features_emb = tf.concat((arg0_depth_edge_features_emb, arg0_position_edge_features_emb), axis=-1)

        arg1_depth_edge_features = arg1_edge_features[:, 0]
        arg1_position_edge_features = arg1_edge_features[:, 1]
        arg1_depth_edge_features_emb = self.depth_embedding(arg1_depth_edge_features)
        arg1_position_edge_features_emb = self.position_embedding(arg1_position_edge_features)
        arg1_edge_features_emb = tf.concat((arg1_depth_edge_features_emb, arg1_position_edge_features_emb), axis=-1)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)
            arg1_emb = self.rnn_encoder(arg1_emb)

        # Step 2: Edge encoding
        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)
            arg1_edge_features_emb = edge_block(arg1_edge_features_emb, training=training)

        # Iterate over reasoning steps
        for gcn_block in self.gcn_blocks:
            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)
            arg1_emb = gcn_block({'nodes': arg1_emb, 'edge_indices': arg1_edge_indices,
                                  'edge_features': arg1_edge_features_emb,
                                  'edge_mask': arg1_edge_mask, 'node_indices': arg1_node_indices,
                                  'node_segments': arg1_node_segments,
                                  'directionality_mask': arg1_directionality_mask}, training=training)

        # Step 5: Aggregated representation (Graph embedding)

        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_emb}, training=training)
        arg1_aggregated, arg1_gate = self.pooling({'nodes': arg1_emb}, training=training)

        # Step 6: Final encoding

        arg0_representation = arg0_aggregated
        arg1_representation = arg1_aggregated
        for representation_block in self.representation_blocks:
            arg0_representation = representation_block(arg0_representation, training=training)
            arg1_representation = representation_block(arg1_representation, training=training)

        # [batch_size, ]
        representation_distance = tf.reduce_sum(arg0_representation * arg1_representation, axis=1)

        # [batch_size,]
        return representation_distance, {'left_representation': arg0_representation,
                                         'right_representation': arg1_representation,
                                         'left_gate': arg0_gate,
                                         'right_gate': arg1_gate
                                         }


class M_Single_GNN(M_Pairwise_GNN):

    def __init__(self, num_classes, **kwargs):
        super(M_Single_GNN, self).__init__(**kwargs)

        self.representation_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                                kernel_regularizer=tf.keras.regularizers.l2(
                                                                    self.l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_edges, 2]
        arg0_edge_features = inputs['edge_features']
        arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, 2])
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # Step 2: Edge encoding
        arg0_depth_edge_features = arg0_edge_features[:, :, 0]
        arg0_position_edge_features = arg0_edge_features[:, :, 1]
        arg0_depth_edge_features_emb = self.depth_embedding(arg0_depth_edge_features)
        arg0_position_edge_features_emb = self.position_embedding(arg0_position_edge_features)
        arg0_edge_features_emb = tf.concat((arg0_depth_edge_features_emb, arg0_position_edge_features_emb), axis=-1)

        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        # Iterate over reasoning steps
        for gcn_block in self.gcn_blocks:
            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        # Step 5: Aggregated representation (Graph embedding)

        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_emb}, training=training)

        # Step 6: Classification

        arg0_representation = arg0_aggregated
        for representation_block in self.representation_blocks:
            arg0_representation = representation_block(arg0_representation, training=training)

        # [batch_size, num_classes]
        return arg0_representation, {"gate": arg0_gate,
                                     "answer": arg0_representation,
                                     "representation": arg0_aggregated}


class M_Single_Dual_GNN(M_Single_GNN):

    def __init__(self, max_graph_subtree_size, self_attention_weights, **kwargs):
        super(M_Single_Dual_GNN, self).__init__(**kwargs)
        self.max_graph_subtree_size = max_graph_subtree_size

        self.pooling = MP_Hier_Pooling(gate_weights=self.gate_mlp_weights,
                                       aggregation_weights=self.aggregation_mlp_weights,
                                       l2_regularization=self.l2_regularization,
                                       dropout_rate=self.dropout_rate)

        self.self_attention_blocks = []
        for idx, weight in enumerate(self_attention_weights):
            self.self_attention_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                    activation=tf.nn.tanh,
                                                                    kernel_regularizer=tf.keras.regularizers.l2(
                                                                        self.l2_regularization)))
            self.self_attention_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.self_attention_blocks.append(tf.keras.layers.Dense(units=1, activation=tf.nn.sigmoid))

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_edges, 2]
        arg0_edge_features = inputs['edge_features']
        arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, 2])
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # [batch_size, max_subtrees * max_subtree_size]
        arg0_subtree_indices = inputs['subtree_indices']
        arg0_subtree_indices = tf.reshape(arg0_subtree_indices,
                                          [arg0.shape[0], 2, self.max_graph_subtree_size])

        # [batch_size, max_subtrees * max_subtree_size]
        arg0_subtree_indices_mask = inputs['subtree_indices_mask']
        arg0_subtree_indices_mask = tf.reshape(arg0_subtree_indices_mask,
                                               [arg0.shape[0], 2, self.max_graph_subtree_size])

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # Step 2: Edge encoding
        arg0_depth_edge_features = arg0_edge_features[:, :, 0]
        arg0_position_edge_features = arg0_edge_features[:, :, 1]
        arg0_depth_edge_features_emb = self.depth_embedding(arg0_depth_edge_features)
        arg0_position_edge_features_emb = self.position_embedding(arg0_position_edge_features)
        arg0_edge_features_emb = tf.concat((arg0_depth_edge_features_emb, arg0_position_edge_features_emb), axis=-1)

        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        # Iterate over reasoning steps
        for gcn_block in self.gcn_blocks:
            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        # Step 5: Aggregated representation (Graph embedding)

        # [batch_size, 2, dim]
        arg0_dual_nodes = self.pooling({'nodes': arg0_emb,
                                        'next_node_indices': arg0_subtree_indices,
                                        'next_node_mask': arg0_subtree_indices_mask},
                                       training=training)

        # [batch_size, 2, 1]
        arg0_dual_attention = arg0_dual_nodes
        for self_attention_block in self.self_attention_blocks:
            arg0_dual_attention = self_attention_block(arg0_dual_attention, training=training)

        # [batch_size, 2 * dim]
        arg0_dual_nodes *= arg0_dual_attention
        arg0_aggregated = tf.reshape(arg0_dual_nodes, [arg0_dual_nodes.shape[0], -1])

        # Step 6: Classification

        arg0_representation = arg0_aggregated
        for representation_block in self.representation_blocks:
            arg0_representation = representation_block(arg0_representation, training=training)

        # [batch_size, num_classes]
        return arg0_representation, {"self_attention": arg0_dual_attention,
                                     "answer": arg0_representation,
                                     "representation": arg0_aggregated}


class M_Single_Char_GNN(M_Single_GNN):

    def __init__(self, char_vocab_size, max_graph_chars, **kwargs):
        super(M_Single_Char_GNN, self).__init__(**kwargs)

        assert self.embedding_dimension % 2 == 0

        self.char_vocab_size = char_vocab_size
        self.max_graph_chars = max_graph_chars

        self.char_input_embedding = tf.keras.layers.Embedding(input_dim=char_vocab_size,
                                                              output_dim=self.embedding_dimension,
                                                              input_length=max_graph_chars,
                                                              mask_zero=True,
                                                              name='char_input_embedding')

        self.char_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=self.embedding_dimension // 2,
                                                                               return_sequences=False))

    def call(self, inputs, training=False, **kwargs):
        # Step 0: Inputs

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_nodes * max_chars]
        arg0_chars = inputs['node_chars_ids']
        arg0_chars = tf.reshape(arg0_chars, [arg0.shape[0], arg0.shape[1], self.max_graph_chars])

        # [batch_size, max_edges, max_depth]
        arg0_edge_features = inputs['edge_features']
        arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, self.graph_max_depth])
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # [batch_size, max_nodes, max_chars, embedding_dim]
        arg0_chars_emb = self.char_input_embedding(arg0_chars)

        # [batch_size, max_nodes, embedding_dim]
        arg0_chars_emb = tf.reshape(arg0_chars_emb, [-1, arg0_chars_emb.shape[2], arg0_chars_emb.shape[3]])
        arg0_chars_emb = self.char_encoder(arg0_chars_emb)
        arg0_chars_emb = tf.reshape(arg0_chars_emb, [arg0_emb.shape[0], arg0_emb.shape[1], -1])

        # Step 2: Edge encoding
        arg0_edge_features_emb = arg0_edge_features
        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        for gcn_block in self.gcn_blocks:
            arg0_chars_emb = gcn_block({'nodes': arg0_chars_emb, 'edge_indices': arg0_edge_indices,
                                        'edge_features': arg0_edge_features_emb,
                                        'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                        'node_segments': arg0_node_segments,
                                        'directionality_mask': arg0_directionality_mask}, training=training)

            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        arg0_chars_aggregated, arg0_chars_gate = self.pooling({'nodes': arg0_chars_emb}, training=training)
        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_emb}, training=training)

        final_repr = tf.concat((arg0_aggregated, arg0_chars_aggregated), axis=-1)

        # [batch_size, final_dim * 2]
        final_representation = final_repr
        for representation_block in self.representation_blocks:
            final_representation = representation_block(final_representation, training=training)

        # [batch_size, 3]
        return final_representation, {"gate": {"word": arg0_gate, "char": arg0_chars_gate},
                                      "answer": final_representation,
                                      "representation": final_repr}


class M_Single_Hierarchical_GNN(M_Single_GNN):

    def __init__(self, max_subtrees, max_subtree_size, max_subtree_depth,
                 subtree_gate_mlp_weights, subtree_aggregation_mlp_weights,
                 subtree_message_weights, subtree_node_mlp_weights, **kwargs):
        super(M_Single_Hierarchical_GNN, self).__init__(**kwargs)
        self.max_subtrees = max_subtrees
        self.max_subtree_size = max_subtree_size
        self.max_subtree_depth = max_subtree_depth
        self.subtree_node_mlp_weights = subtree_node_mlp_weights if type(subtree_node_mlp_weights) is list else list(
            subtree_node_mlp_weights)

        self.hier_pooling = MP_Hier_Pooling(aggregation_weights=subtree_aggregation_mlp_weights,
                                            gate_weights=subtree_gate_mlp_weights,
                                            dropout_rate=self.dropout_rate,
                                            l2_regularization=self.l2_regularization)

        if self.repeated_shared:
            # 1 GCN layer repeated k times
            self.subtree_node_mlp_weights.append(self.gcn_blocks[0].node_weights[-1])
            gcn_shared = MP_GCN(node_weights=self.subtree_node_mlp_weights,
                                edge_weights=None,
                                message_weights=subtree_message_weights,
                                l2_regularization=self.l2_regularization,
                                dropout_rate=self.dropout_rate,
                                node_encoding=True,
                                edge_encoding=False)
            self.tree_gcn_blocks = [gcn_shared for _ in range(self.timesteps)]
        else:
            # K GCN layers where K is the number of weights
            assert len(subtree_node_mlp_weights) == len(subtree_message_weights)
            self.tree_gcn_blocks = [MP_GCN(node_weights=[node_w],
                                           edge_weights=None,
                                           message_weights=[message_w],
                                           l2_regularization=self.l2_regularization,
                                           dropout_rate=self.dropout_rate,
                                           node_encoding=True,
                                           edge_encoding=False)
                                    for node_w, message_w in
                                    zip(subtree_node_mlp_weights, subtree_message_weights)]

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # Level 1: word level graph

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_edges, max_depth]
        # arg0_edge_features = inputs['edge_features']
        # arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, self.graph_max_depth])
        # arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # Level 2: sub-tree level graph

        # [batch_size, max_subtrees * max_subtree_size]
        arg0_subtree_indices = inputs['subtree_indices']
        arg0_subtree_indices = tf.reshape(arg0_subtree_indices,
                                          [arg0.shape[0], self.max_subtrees, self.max_subtree_size])

        # [batch_size, max_subtrees * max_subtree_size]
        arg0_subtree_indices_mask = inputs['subtree_indices_mask']
        arg0_subtree_indices_mask = tf.reshape(arg0_subtree_indices_mask,
                                               [arg0.shape[0], self.max_subtrees, self.max_subtree_size])

        # [batch_size, max_subtree_edges, max_subtree_depth]
        # arg0_subtree_edge_features = inputs['subtree_edge_features']
        # arg0_subtree_edge_features = tf.reshape(arg0_subtree_edge_features, [arg0_subtree_edge_features.shape[0],
        #                                                                      -1,
        #                                                                      self.max_subtree_depth])
        # arg0_subtree_edge_features = tf.cast(arg0_subtree_edge_features, tf.float32)

        # [batch_size, max_subtree_edges]
        arg0_subtree_edge_mask = inputs['subtree_edge_mask']

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_edge_indices = inputs['subtree_edge_indices']
        arg0_subtree_edge_indices = tf.reshape(arg0_subtree_edge_indices, [arg0.shape[0], -1, 2])

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_node_indices = inputs['subtree_node_indices']

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_node_segments = inputs['subtree_node_segments']

        # Compute batch-wise node segments for correct segment sum
        subtree_sample_max = tf.reduce_max(arg0_subtree_node_segments, axis=1)
        subtree_batch_indexes = tf.range(arg0_subtree_node_segments.shape[0])
        subtree_to_add = (subtree_sample_max + 1) * subtree_batch_indexes
        subtree_to_add = tf.expand_dims(subtree_to_add, axis=-1)
        arg0_subtree_node_segments += subtree_to_add

        arg0_subtree_node_segments = tf.reshape(arg0_subtree_node_segments, [-1, ])

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_directionality_mask = inputs['subtree_directionality_mask']

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # Step 2: Edge encoding
        # arg0_edge_features_emb = arg0_edge_features
        # for edge_block in self.edge_blocks:
        #     arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        # for edge_block in self.edge_blocks:
        #     arg0_subtree_edge_features = edge_block(arg0_subtree_edge_features, training=training)

        # Iterate over reasoning steps
        for gcn_block in self.gcn_blocks:
            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        arg0_next_emb = self.hier_pooling({'nodes': arg0_emb, 'next_node_indices': arg0_subtree_indices,
                                           'next_node_mask': arg0_subtree_indices_mask}, training=training)

        for tree_gcn_block in self.tree_gcn_blocks:
            arg0_next_emb = tree_gcn_block({'nodes': arg0_next_emb, 'edge_indices': arg0_subtree_edge_indices,
                                            'edge_mask': arg0_subtree_edge_mask,
                                            'node_indices': arg0_subtree_node_indices,
                                            'node_segments': arg0_subtree_node_segments,
                                            'directionality_mask': arg0_subtree_directionality_mask}, training=training)

        # Step 5: Aggregated representation (Graph embedding)

        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_next_emb}, training=training)

        # Step 6: Classification

        arg0_representation = arg0_aggregated
        for representation_block in self.representation_blocks:
            arg0_representation = representation_block(arg0_representation, training=training)

        # [batch_size, num_classes]
        return arg0_representation, {"gate": arg0_gate,
                                     "answer": arg0_representation,
                                     "representation": arg0_aggregated}


class M_Single_Dep_GNN(M_Single_GNN):

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_edges]
        arg0_edge_features = inputs['edge_features']
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # [batch_size, max_edges, embedding_dim]
        arg0_edge_features = self.input_embedding(arg0_edge_features)

        # Step 2: Edge encoding
        arg0_edge_features_emb = arg0_edge_features
        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        # Iterate over reasoning steps
        for gcn_block in self.gcn_blocks:
            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        # Step 5: Aggregated representation (Graph embedding)

        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_emb}, training=training)

        # Step 6: Classification

        arg0_representation = arg0_aggregated
        for representation_block in self.representation_blocks:
            arg0_representation = representation_block(arg0_representation, training=training)

        # [batch_size, num_classes]
        return arg0_representation, {"gate": arg0_gate,
                                     "answer": arg0_representation,
                                     "representation": arg0_aggregated}


class M_Single_Dep_Char_GNN(M_Single_Dep_GNN):

    def __init__(self, char_vocab_size, max_graph_chars, **kwargs):
        super(M_Single_Dep_Char_GNN, self).__init__(**kwargs)

        assert self.embedding_dimension % 2 == 0

        self.char_vocab_size = char_vocab_size
        self.max_graph_chars = max_graph_chars

        self.char_input_embedding = tf.keras.layers.Embedding(input_dim=char_vocab_size,
                                                              output_dim=self.embedding_dimension,
                                                              input_length=max_graph_chars,
                                                              mask_zero=True,
                                                              name='char_input_embedding')

        self.char_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=self.embedding_dimension // 2,
                                                                               return_sequences=False))

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_nodes * max_chars]
        arg0_chars = inputs['node_chars_ids']
        arg0_chars = tf.reshape(arg0_chars, [arg0.shape[0], arg0.shape[1], self.max_graph_chars])

        # [batch_size, max_edges]
        arg0_edge_features = inputs['edge_features']
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges, max_graph_chars]
        arg0_edge_char_features = inputs['edge_char_features_ids']
        arg0_edge_char_features = tf.reshape(arg0_edge_char_features, [arg0.shape[0], -1, self.max_graph_chars])

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # [batch_size, max_nodes, max_chars, embedding_dim]
        arg0_chars_emb = self.char_input_embedding(arg0_chars)

        # [batch_size, max_nodes, embedding_dim]
        arg0_chars_emb = tf.reshape(arg0_chars_emb, [-1, arg0_chars_emb.shape[2], arg0_chars_emb.shape[3]])
        arg0_chars_emb = self.char_encoder(arg0_chars_emb)
        arg0_chars_emb = tf.reshape(arg0_chars_emb, [arg0_emb.shape[0], arg0_emb.shape[1], -1])

        # [batch_size, max_edges, embedding_dim]
        arg0_edge_features = self.input_embedding(arg0_edge_features)

        # [batch_size, max_edges, embedding_dim]
        arg0_edge_char_features = self.char_input_embedding(arg0_edge_char_features)

        # [batch_size, max_nodes, embedding_dim]
        arg0_edge_char_features = tf.reshape(arg0_edge_char_features,
                                             [-1, arg0_edge_char_features.shape[2], arg0_edge_char_features.shape[3]])
        arg0_edge_char_features = self.char_encoder(arg0_edge_char_features)
        arg0_edge_char_features = tf.reshape(arg0_edge_char_features,
                                             [arg0_chars_emb.shape[0], -1, arg0_edge_char_features.shape[1]])

        # Step 2: Edge encoding
        arg0_edge_features_emb = arg0_edge_features
        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        arg0_edge_char_features_emb = arg0_edge_char_features
        for edge_block in self.edge_blocks:
            arg0_edge_char_features_emb = edge_block(arg0_edge_char_features_emb, training=training)

        for gcn_block in self.gcn_blocks:
            arg0_chars_emb = gcn_block({'nodes': arg0_chars_emb, 'edge_indices': arg0_edge_indices,
                                        'edge_features': arg0_edge_char_features_emb,
                                        'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                        'node_segments': arg0_node_segments,
                                        'directionality_mask': arg0_directionality_mask}, training=training)

            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        arg0_chars_aggregated, arg0_chars_gate = self.pooling({'nodes': arg0_chars_emb}, training=training)
        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_emb}, training=training)

        final_repr = tf.concat((arg0_aggregated, arg0_chars_aggregated), axis=-1)

        # [batch_size, final_dim * 2]
        final_representation = final_repr
        for representation_block in self.representation_blocks:
            final_representation = representation_block(final_representation, training=training)

        # [batch_size, 3]
        return final_representation, {"gate": {"word": arg0_gate, "char": arg0_chars_gate},
                                      "answer": final_representation,
                                      "representation": final_repr}


class M_Single_Hierarchical_Dep_GNN(M_Single_Dep_GNN):

    def __init__(self, max_subtrees, max_subtree_size, max_subtree_depth,
                 subtree_gate_mlp_weights, subtree_aggregation_mlp_weights,
                 subtree_message_weights, subtree_node_mlp_weights, **kwargs):
        super(M_Single_Hierarchical_Dep_GNN, self).__init__(**kwargs)
        self.max_subtrees = max_subtrees
        self.max_subtree_size = max_subtree_size
        self.max_subtree_depth = max_subtree_depth
        self.subtree_node_mlp_weights = subtree_node_mlp_weights if type(subtree_node_mlp_weights) is list else list(
            subtree_node_mlp_weights)

        self.hier_pooling = MP_Hier_Pooling(aggregation_weights=subtree_aggregation_mlp_weights,
                                            gate_weights=subtree_gate_mlp_weights,
                                            dropout_rate=self.dropout_rate,
                                            l2_regularization=self.l2_regularization)

        if self.repeated_shared:
            # 1 GCN layer repeated k times
            self.subtree_node_mlp_weights.append(self.gcn_blocks[0].node_weights[-1])
            gcn_shared = MP_GCN(node_weights=self.subtree_node_mlp_weights,
                                edge_weights=None,
                                message_weights=subtree_message_weights,
                                l2_regularization=self.l2_regularization,
                                dropout_rate=self.dropout_rate,
                                node_encoding=True,
                                edge_encoding=False)
            self.tree_gcn_blocks = [gcn_shared for _ in range(self.timesteps)]
        else:
            # K GCN layers where K is the number of weights
            assert len(subtree_node_mlp_weights) == len(subtree_message_weights)
            self.tree_gcn_blocks = [MP_GCN(node_weights=[node_w],
                                           edge_weights=None,
                                           message_weights=[message_w],
                                           l2_regularization=self.l2_regularization,
                                           dropout_rate=self.dropout_rate,
                                           node_encoding=True,
                                           edge_encoding=False)
                                    for node_w, message_w in
                                    zip(subtree_node_mlp_weights, subtree_message_weights)]

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # Level 1: word level graph

        # [batch_size, max_nodes]
        arg0 = inputs['node_ids']

        # [batch_size, max_edges, max_depth]
        arg0_edge_features = inputs['edge_features']
        arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_edges * 2]
        arg0_edge_indices = inputs['edge_indices']
        arg0_edge_indices = tf.reshape(arg0_edge_indices, [arg0_edge_indices.shape[0], -1, 2])

        # [batch_size, max_edges]
        arg0_edge_mask = inputs['edge_mask']

        # [batch_size, max_edges * 2]
        arg0_node_indices = inputs['node_indices']

        # [batch_size, max_edges * 2]
        arg0_node_segments = inputs['node_segments']

        # Compute batch-wise node segments for correct segment sum
        sample_max = tf.reduce_max(arg0_node_segments, axis=1)
        batch_indexes = tf.range(arg0_node_segments.shape[0])
        to_add = (sample_max + 1) * batch_indexes
        to_add = tf.expand_dims(to_add, axis=-1)
        arg0_node_segments += to_add

        arg0_node_segments = tf.reshape(arg0_node_segments, [-1, ])

        # [batch_size, max_edges * 2]
        arg0_directionality_mask = inputs['directionality_mask']

        # Level 2: sub-tree level graph

        # [batch_size, max_subtrees * max_subtree_size]
        arg0_subtree_indices = inputs['subtree_indices']
        arg0_subtree_indices = tf.reshape(arg0_subtree_indices,
                                          [arg0.shape[0], self.max_subtrees, self.max_subtree_size])

        # [batch_size, max_subtrees * max_subtree_size]
        arg0_subtree_indices_mask = inputs['subtree_indices_mask']
        arg0_subtree_indices_mask = tf.reshape(arg0_subtree_indices_mask,
                                               [arg0.shape[0], self.max_subtrees, self.max_subtree_size])

        # [batch_size, max_subtree_edges, max_subtree_depth]
        arg0_subtree_edge_features = inputs['subtree_edge_features']
        arg0_subtree_edge_features = tf.reshape(arg0_subtree_edge_features, [arg0_subtree_edge_features.shape[0],
                                                                             -1,
                                                                             self.max_subtree_depth])
        arg0_subtree_edge_features = tf.cast(arg0_subtree_edge_features, tf.float32)

        # [batch_size, max_subtree_edges]
        arg0_subtree_edge_mask = inputs['subtree_edge_mask']

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_edge_indices = inputs['subtree_edge_indices']
        arg0_subtree_edge_indices = tf.reshape(arg0_subtree_edge_indices, [arg0.shape[0], -1, 2])

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_node_indices = inputs['subtree_node_indices']

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_node_segments = inputs['subtree_node_segments']

        # Compute batch-wise node segments for correct segment sum
        subtree_sample_max = tf.reduce_max(arg0_subtree_node_segments, axis=1)
        subtree_batch_indexes = tf.range(arg0_subtree_node_segments.shape[0])
        subtree_to_add = (subtree_sample_max + 1) * subtree_batch_indexes
        subtree_to_add = tf.expand_dims(subtree_to_add, axis=-1)
        arg0_subtree_node_segments += subtree_to_add

        arg0_subtree_node_segments = tf.reshape(arg0_subtree_node_segments, [-1, ])

        # [batch_size, max_subtree_edges * 2]
        arg0_subtree_directionality_mask = inputs['subtree_directionality_mask']

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        arg0_emb = self.input_embedding(arg0)
        arg0_emb = self.input_dropout(arg0_emb, training=training)

        if self.rnn_encoding:
            arg0_emb = self.rnn_encoder(arg0_emb)

        # Step 2: Edge encoding

        # [batch_size, max_edges, embedding_dim]
        arg0_edge_features = self.input_embedding(arg0_edge_features)

        arg0_edge_features_emb = arg0_edge_features
        for edge_block in self.edge_blocks:
            arg0_edge_features_emb = edge_block(arg0_edge_features_emb, training=training)

        for edge_block in self.subtree_edge_blocks:
            arg0_subtree_edge_features = edge_block(arg0_subtree_edge_features, training=training)

        # Iterate over reasoning steps
        for gcn_block in self.gcn_blocks:
            arg0_emb = gcn_block({'nodes': arg0_emb, 'edge_indices': arg0_edge_indices,
                                  'edge_features': arg0_edge_features_emb,
                                  'edge_mask': arg0_edge_mask, 'node_indices': arg0_node_indices,
                                  'node_segments': arg0_node_segments,
                                  'directionality_mask': arg0_directionality_mask}, training=training)

        arg0_next_emb = self.hier_pooling({'nodes': arg0_emb, 'next_node_indices': arg0_subtree_indices,
                                           'next_node_mask': arg0_subtree_indices_mask}, training=training)

        for tree_gcn_block in self.tree_gcn_blocks:
            arg0_next_emb = tree_gcn_block({'nodes': arg0_next_emb, 'edge_indices': arg0_subtree_edge_indices,
                                            'edge_features': arg0_subtree_edge_features,
                                            'edge_mask': arg0_subtree_edge_mask,
                                            'node_indices': arg0_subtree_node_indices,
                                            'node_segments': arg0_subtree_node_segments,
                                            'directionality_mask': arg0_subtree_directionality_mask}, training=training)

        # Step 5: Aggregated representation (Graph embedding)

        arg0_aggregated, arg0_gate = self.pooling({'nodes': arg0_next_emb}, training=training)

        # Step 6: Classification

        arg0_representation = arg0_aggregated
        for representation_block in self.representation_blocks:
            arg0_representation = representation_block(arg0_representation, training=training)

        # [batch_size, num_classes]
        return arg0_representation, {"gate": arg0_gate,
                                     "answer": arg0_representation,
                                     "representation": arg0_aggregated}


class M_Single_Adj_GNN(tf.keras.Model):

    def __init__(self, embedding_dimension, vocab_size,
                 graph_max_nodes, gcn_info, num_classes,
                 representation_mlp_weights, graph_max_depth=None,
                 l2_regularization=0., input_dropout_rate=0.4, rnn_encoding=False,
                 dropout_rate=0.4, embedding_matrix=None,
                 use_cluster_regularization=False, use_link_regularization=False,
                 use_sigmoid=False, batch_size=32, **kwargs):
        super(M_Single_Adj_GNN, self).__init__(**kwargs)
        self.graph_max_nodes = graph_max_nodes
        self.graph_max_depth = graph_max_depth if graph_max_depth is not None else graph_max_nodes
        self.representation_mlp_weights = representation_mlp_weights if type(
            representation_mlp_weights) is list else list(representation_mlp_weights)
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.input_dropout_rate = input_dropout_rate
        self.embedding_dimension = embedding_dimension
        self.rnn_encoding = rnn_encoding
        self.num_classes = num_classes
        self.batch_size = batch_size
        self.use_cluster_regularization = use_cluster_regularization
        self.use_link_regularization = use_link_regularization
        self.use_sigmoid = use_sigmoid

        gcn_info = OrderedDict(sorted(gcn_info.items(), key=lambda pair: int(pair[0])))  # sort keys
        self.gcn_info = {int(key): value for key, value in gcn_info.items()}

        assert 0 in self.gcn_info

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=graph_max_nodes,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='input_embedding')

        self.input_dropout = tf.keras.layers.Dropout(self.input_dropout_rate)

        if rnn_encoding:
            assert embedding_dimension % 2 == 0
            self.rnn_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=embedding_dimension // 2,
                                                                                  return_sequences=True))

        # GCN blocks
        self.gcn_blocks = []
        self.pooling_blocks = []
        current_key = 0
        last_key = list(self.gcn_info.keys())[-1]
        for key, value in self.gcn_info.items():

            # Sharing works by checking if there are discontinuities in keys listing
            key_difference = key - current_key

            # Some discontinuity -> repeat previous layer
            if key_difference > 1:
                for _ in range(key_difference - 1):
                    self.gcn_blocks.append(self.gcn_blocks[-1])
                    self.pooling_blocks.append(self.pooling_blocks[-1])

            # Check if last one
            if key == last_key:
                if value['pooling_weights'][-1] != 1:
                    value['pooling_weights'].append(1)

            curr_gcn_layer = GCN(node_weights=value['node_weights'],
                                 message_weights=value['message_weights'],
                                 l2_regularization=l2_regularization,
                                 dropout_rate=dropout_rate,
                                 node_encoding=True)

            curr_pooling_layer = DiffPool(pooling_weights=value['pooling_weights'],
                                          aggregation_weights=value['aggregation_weights'],
                                          use_cluster_regularization=self.use_cluster_regularization,
                                          use_link_regularization=self.use_link_regularization,
                                          l2_regularization=l2_regularization,
                                          dropout_rate=dropout_rate,
                                          use_sigmoid=self.use_sigmoid)

            # Add current
            self.gcn_blocks.append(curr_gcn_layer)
            self.pooling_blocks.append(curr_pooling_layer)

            current_key = key

        assert len(self.gcn_blocks) == len(self.pooling_blocks)

        # Graph embedding
        self.representation_blocks = []
        for idx, weight in enumerate(representation_mlp_weights):
            self.representation_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                    activation=tf.nn.leaky_relu if idx < len(
                                                                        representation_mlp_weights) - 1 else None,
                                                                    kernel_regularizer=tf.keras.regularizers.l2(
                                                                        l2_regularization)))

            if idx < len(representation_mlp_weights) - 1:
                self.representation_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.representation_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                                kernel_regularizer=tf.keras.regularizers.l2(
                                                                    self.l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size,]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time                                   -> [batch_size, nodes]
            # None after the first block (we do not have padding nodes anymore!) -> None
            block_nodes, \
            block_adjacency_matrix, \
            block_node_mask, \
            block_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                  'adjacency_matrix': current_adjacency_matrix,
                                                  'node_mask': current_node_mask},
                                                 training=training,
                                                 enable_regularization=layer_id != len(self.gcn_blocks) - 1)

            # Store for quick retrieval
            pooling_block.adjacency_matrix = current_adjacency_matrix
            pooling_block.pooling_matrix = block_pooling_matrix
            pooling_block.node_mask = current_node_mask

            current_nodes = block_nodes
            current_adjacency_matrix = block_adjacency_matrix
            current_node_mask = block_node_mask

        # Step 5: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        graph_representation = current_nodes
        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        model_additional_info = {
            "answer": graph_representation
        }

        # Additional info
        for layer_id in range(len(self.pooling_blocks)):
            for key in ['adjacency_matrix',
                        'pooling_matrix',
                        'node_mask']:
                if hasattr(self.pooling_blocks[layer_id], key):
                    model_additional_info['{0}_{1}'.format(key, layer_id)] = getattr(self.pooling_blocks[layer_id], key)

        # [batch_size, num_classes]
        return graph_representation, model_additional_info


class Pos_M_Single_Adj_GNN(M_Single_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_Single_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes, max_depth]
        # arg0_edge_features = inputs['edge_features']
        # arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, 1])
        # arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size,]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            block_nodes, \
            block_adjacency_matrix, \
            block_node_mask, \
            block_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                  'adjacency_matrix': current_adjacency_matrix,
                                                  'node_mask': current_node_mask},
                                                 training=training,
                                                 enable_regularization=layer_id != len(self.gcn_blocks) - 1)

            # Store for quick retrieval
            pooling_block.adjacency_matrix = current_adjacency_matrix
            pooling_block.pooling_matrix = block_pooling_matrix
            pooling_block.node_mask = current_node_mask

            current_nodes = block_nodes
            current_adjacency_matrix = block_adjacency_matrix
            current_node_mask = block_node_mask

        # Step 5: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], current_nodes.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            current_nodes = tf.concat((current_nodes, position_id), axis=-1)

        graph_representation = current_nodes
        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        model_additional_info = {
            "answer": graph_representation
        }

        # Additional info
        for layer_id in range(len(self.pooling_blocks)):
            for key in ['adjacency_matrix',
                        'pooling_matrix',
                        'node_mask']:
                if hasattr(self.pooling_blocks[layer_id], key):
                    model_additional_info['{0}_{1}'.format(key, layer_id)] = getattr(self.pooling_blocks[layer_id], key)

        # [batch_size, num_classes]
        return graph_representation, model_additional_info


class M_Dual_Adj_GNN(M_Single_Adj_GNN):

    def __init__(self, rnn_weights, self_attention_weights, **kwargs):
        super(M_Dual_Adj_GNN, self).__init__(**kwargs)

        self.rnn_weights = rnn_weights

        self.rnn_blocks = []
        for weight in self.rnn_weights[:-1]:
            self.rnn_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                      return_sequences=True,
                                                                                      kernel_regularizer=tf.keras.regularizers.l2(
                                                                                          self.l2_regularization))))
            self.rnn_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.rnn_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.rnn_weights[-1],
                                                                                  return_sequences=False,
                                                                                  kernel_regularizer=tf.keras.regularizers.l2(
                                                                                      self.l2_regularization))))

        self.self_attention_blocks = []
        for idx, weight in enumerate(self_attention_weights):
            self.self_attention_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                    activation=tf.nn.tanh,
                                                                    kernel_regularizer=tf.keras.regularizers.l2(
                                                                        self.l2_regularization)))
            self.self_attention_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.self_attention_blocks.append(tf.keras.layers.Dense(units=1, activation=tf.nn.sigmoid))

    def call(self, inputs, training=False, **kwargs):
        # Step 0: Inputs

        # [batch_size, max_sentence_size]
        text_ids = inputs['text_ids']

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes, max_depth]
        # arg0_edge_features = inputs['edge_features']
        # arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, 1])
        # arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size,]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_sentence_size, embedding_dim]
        text_ids_emb = self.input_embedding(text_ids)
        text_ids_emb = self.input_dropout(text_ids_emb, training=training)

        for rnn_block in self.rnn_blocks:
            text_ids_emb = rnn_block(text_ids_emb, training=training)

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            current_nodes, \
            current_adjacency_matrix, \
            current_node_mask, \
            current_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                    'adjacency_matrix': current_adjacency_matrix,
                                                    'node_mask': current_node_mask},
                                                   training=training,
                                                   enable_regularization=layer_id != len(self.gcn_blocks) - 1)

        # Step 6: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        current_nodes = tf.expand_dims(current_nodes, axis=1)
        text_ids_emb = tf.expand_dims(text_ids_emb, axis=1)

        # [batch_size, 2, dim]
        graph_representation = tf.concat((current_nodes, text_ids_emb), axis=1)

        # [batch_size, 2, 1]
        dual_attention = graph_representation
        for self_attention_block in self.self_attention_blocks:
            dual_attention = self_attention_block(dual_attention, training=training)

        # [batch_size, 2 * dim]
        graph_representation *= dual_attention
        graph_representation = tf.reshape(graph_representation, [graph_representation.shape[0], -1])

        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        # [batch_size, num_classes]
        return graph_representation, {
            "answer": graph_representation,
            "representation": current_nodes
        }


class Pos_M_Dual_Adj_GNN(M_Dual_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_Dual_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):
        # Step 0: Inputs

        # [batch_size, max_sentence_size]
        text_ids = inputs['text_ids']

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes, max_depth]
        # arg0_edge_features = inputs['edge_features']
        # arg0_edge_features = tf.reshape(arg0_edge_features, [arg0_edge_features.shape[0], -1, 1])
        # arg0_edge_features = tf.cast(arg0_edge_features, tf.float32)

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size,]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_sentence_size, embedding_dim]
        text_ids_emb = self.input_embedding(text_ids)
        text_ids_emb = self.input_dropout(text_ids_emb, training=training)

        for rnn_block in self.rnn_blocks:
            text_ids_emb = rnn_block(text_ids_emb)

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            current_nodes, \
            current_adjacency_matrix, \
            current_node_mask, \
            current_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                    'adjacency_matrix': current_adjacency_matrix,
                                                    'node_mask': current_node_mask},
                                                   training=training,
                                                   enable_regularization=layer_id != len(self.gcn_blocks) - 1)

        # Step 6: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        current_nodes = tf.expand_dims(current_nodes, axis=1)
        text_ids_emb = tf.expand_dims(text_ids_emb, axis=1)

        # [batch_size, 2, dim]
        graph_representation = tf.concat((current_nodes, text_ids_emb), axis=1)

        # [batch_size, 2, 1]
        dual_attention = graph_representation
        for self_attention_block in self.self_attention_blocks:
            dual_attention = self_attention_block(dual_attention, training=training)

        # [batch_size, 2 * dim]
        graph_representation *= dual_attention
        graph_representation = tf.reshape(graph_representation, [graph_representation.shape[0], -1])

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], current_nodes.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            graph_representation = tf.concat((graph_representation, position_id), axis=-1)

        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        # [batch_size, num_classes]
        return graph_representation, {
            "answer": graph_representation,
            "representation": current_nodes
        }


class M_Single_Tree_Pooled_Adj_GNN(M_Single_Adj_GNN):

    def __init__(self, connectivity_threshold=0.5,
                 overlapping_threshold=0.5, kernel='ptk',
                 coefficients_learning='fixed', lambda_values=None, **kwargs):
        super(M_Single_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)
        self.connectivity_threshold = connectivity_threshold
        self.overlapping_threshold = overlapping_threshold
        self.kernel = kernel

        # Add Lagrangian multipliers
        # 4 constraints:
        # contiguous, kernel specific, size and overlap, intensity
        if coefficients_learning == 'lagrangian':
            if lambda_values is not None:
                assert type(lambda_values) in [tuple, list] and len(lambda_values) == 4

            init_multipliers = lambda_values if lambda_values is not None else [0., 0., 0., 0.]
            self.multipliers = tf.Variable(initial_value=tf.constant_initializer(init_multipliers)(shape=(4,)),
                                           dtype=tf.float32,
                                           trainable=True, name='multipliers')
        else:
            self.multipliers = None

    # Kernel specific constraints

    def _stk_contiguous_sequence_constraint(self, pooling_matrix, adjacency_matrix, node_mask=None):
        adjacency_matrix = tf.cast(adjacency_matrix, dtype=pooling_matrix.dtype)

        if node_mask is None:
            node_mask = tf.ones_like(adjacency_matrix)

        # avoid counting padding nodes
        adjacency_matrix *= node_mask
        adjacency_matrix *= tf.transpose(node_mask, [0, 2, 1])

        node_amount = adjacency_matrix.shape[-1]
        meta_node_amount = pooling_matrix.shape[-1]

        # Adjacency and degree matrices

        # [batch_size, N, N]
        forward_adjacency_matrix = tf.linalg.band_part(tf.ones((node_amount, node_amount), dtype=pooling_matrix.dtype),
                                                       0, -1)
        forward_adjacency_matrix -= tf.eye(node_amount, dtype=pooling_matrix.dtype)
        forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

        # [batch_size, N, N]
        backward_adjacency_matrix = tf.linalg.band_part(tf.ones((node_amount, node_amount), dtype=pooling_matrix.dtype),
                                                        -1, 0)
        backward_adjacency_matrix -= tf.eye(node_amount, dtype=pooling_matrix.dtype)
        backward_adjacency_matrix = adjacency_matrix * backward_adjacency_matrix[None, :, :]

        # [batch_size, N, N]
        forward_degree_matrix = tf.reduce_sum(forward_adjacency_matrix, axis=-1)
        forward_degree_matrix = forward_degree_matrix[:, :, None] * tf.eye(adjacency_matrix.shape[-1],
                                                                           dtype=adjacency_matrix.dtype)[None, :, :]

        backward_degree_matrix = tf.reduce_sum(backward_adjacency_matrix, axis=-1)
        backward_degree_matrix = backward_degree_matrix[:, :, None] * tf.eye(adjacency_matrix.shape[-1],
                                                                             dtype=adjacency_matrix.dtype)[None, :, :]

        # Leaf mask
        # Mask out leaves: a leaf node no children -> the minimum returns 0.
        # is leaf? -> 0.0
        # [batch_size, max_nodes]
        leaf_mask = tf.minimum(tf.reduce_sum(forward_adjacency_matrix, axis=-1), 1.)

        # Apply masked adjacency matrix

        # [batch_size, max_nodes, max_nodes]
        numerator_adjacency_matrix = forward_adjacency_matrix + backward_adjacency_matrix * (
                1. - leaf_mask[:, :, None])

        # [batch_size, K, K]
        numerator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), numerator_adjacency_matrix),
                              pooling_matrix)

        # Apply masked degree matrix

        # [batch_size, max_nodes, max_nodes]
        denominator_degree_matrix = forward_degree_matrix + backward_degree_matrix * (
                1. - leaf_mask[:, :, None])

        # [batch_size, K, K]
        denominator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), denominator_degree_matrix),
                                pooling_matrix)

        # [batch_size, K]
        per_cluster_contiguous_penalty = tf.nn.relu(1. - numerator / (denominator + 1e-12)) * tf.eye(meta_node_amount,
                                                                                                     dtype=pooling_matrix.dtype)[
                                                                                              None, :, :]
        per_cluster_contiguous_penalty = tf.reduce_sum(per_cluster_contiguous_penalty, axis=-1)

        # [batch_size]
        numerator = tf.linalg.trace(numerator)
        denominator = tf.linalg.trace(denominator) + 1e-12

        # [batch_size]
        stk_penalty = tf.nn.relu(1. - numerator / denominator)
        # stk_penalty = tf.linalg.trace(stk_penalty)

        return tf.nn.compute_average_loss(stk_penalty,
                                          global_batch_size=self.batch_size), per_cluster_contiguous_penalty

    def _sstk_contiguous_sequence_constraint(self, pooling_matrix, adjacency_matrix, node_mask=None):
        adjacency_matrix = tf.cast(adjacency_matrix, dtype=pooling_matrix.dtype)

        if node_mask is None:
            node_mask = tf.ones_like(adjacency_matrix)

        # avoid counting padding nodes
        adjacency_matrix *= node_mask
        adjacency_matrix *= tf.transpose(node_mask, [0, 2, 1])

        node_amount = adjacency_matrix.shape[-1]
        meta_node_amount = pooling_matrix.shape[-1]

        # Adjacency and degree matrices

        # [batch_size, N, N]
        forward_adjacency_matrix = tf.linalg.band_part(tf.ones((node_amount, node_amount), dtype=pooling_matrix.dtype),
                                                       0, -1)
        forward_adjacency_matrix -= tf.eye(node_amount, dtype=pooling_matrix.dtype)
        forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

        # Leaf mask
        # Mask out leaves: a leaf node no children -> the minimum returns 0.
        # is leaf? -> 0.0
        # [batch_size, max_nodes]
        leaf_mask = tf.minimum(tf.reduce_sum(forward_adjacency_matrix, axis=-1), 1.)

        # Ignore leaves from degree count
        forward_degree_matrix = tf.reduce_sum(forward_adjacency_matrix * leaf_mask[:, None, :], axis=-1)

        # Ignore pooled leaves from constraint
        pooling_matrix *= leaf_mask[:, :, None]

        forward_degree_matrix = forward_degree_matrix[:, :, None] * tf.eye(adjacency_matrix.shape[-1],
                                                                           dtype=adjacency_matrix.dtype)[None, :, :]

        # [batch_size, K, K]
        numerator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), forward_adjacency_matrix),
                              pooling_matrix)

        # Apply masked degree matrix

        denominator = tf.matmul(tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), forward_degree_matrix),
                                pooling_matrix)

        # [batch_size, K]
        per_cluster_contiguous_penalty = tf.nn.relu(1. - numerator / (denominator + 1e-12)) * tf.eye(meta_node_amount,
                                                                                                     dtype=pooling_matrix.dtype)[
                                                                                              None, :, :]
        per_cluster_contiguous_penalty = tf.reduce_sum(per_cluster_contiguous_penalty, axis=-1)

        # [batch_size]
        numerator = tf.linalg.trace(numerator)
        denominator = tf.linalg.trace(denominator) + 1e-12

        # [batch_size]
        stk_penalty = tf.nn.relu(1. - numerator / denominator)
        # stk_penalty = tf.linalg.trace(stk_penalty)

        return tf.nn.compute_average_loss(stk_penalty,
                                          global_batch_size=self.batch_size), per_cluster_contiguous_penalty

    # General constraints

    def contiguous_sequence_constraint(self, pooling_matrix, adjacency_matrix, node_mask=None):
        node_amount = adjacency_matrix.shape[-1]

        adjacency_matrix = tf.cast(adjacency_matrix, dtype=pooling_matrix.dtype)

        if node_mask is None:
            node_mask = tf.ones_like(adjacency_matrix)

        # avoid counting padding nodes
        adjacency_matrix *= node_mask
        adjacency_matrix *= tf.transpose(node_mask, [0, 2, 1])

        # [batch_size, max_nodes, max_nodes]
        forward_adjacency_matrix = tf.linalg.band_part(tf.ones((node_amount, node_amount), dtype=pooling_matrix.dtype),
                                                       0, -1)
        forward_adjacency_matrix = adjacency_matrix * forward_adjacency_matrix[None, :, :]

        # [batch_size, K, max_nodes]
        pooling_matrix = tf.transpose(pooling_matrix, [0, 2, 1])

        # [batch_size, K, max_nodes, max_nodes]
        pair_intensities = tf.expand_dims(pooling_matrix, axis=-1) * tf.expand_dims(pooling_matrix, axis=2)

        # [batch_size, K, max_nodes, max_nodes]
        self_pairs_intensities = pair_intensities * tf.eye(node_amount, dtype=pair_intensities.dtype)[
                                                    None, None, :, :]

        # [batch_size, K]
        pooled_mask = tf.where(self_pairs_intensities >= 1e-3, tf.ones_like(self_pairs_intensities),
                               tf.zeros_like(self_pairs_intensities))
        pooled_mask = tf.reduce_sum(pooled_mask, axis=-1)
        pooled_mask = tf.reduce_sum(pooled_mask, axis=-1)

        pair_intensities = pair_intensities - self_pairs_intensities
        pair_intensities *= tf.expand_dims(forward_adjacency_matrix, axis=1)

        # [batch_size, K]
        numerator = tf.reduce_sum(pair_intensities, axis=-1)
        numerator = tf.reduce_sum(numerator, axis=-1)

        # [batch_size, K]
        mean_self_intensity = tf.reduce_sum(self_pairs_intensities, axis=-1)
        mean_self_intensity = tf.reduce_sum(mean_self_intensity, axis=-1)
        mean_self_intensity = tf.stop_gradient(mean_self_intensity / pooled_mask)

        # [batch_size, K]
        denominator = tf.reduce_sum(self_pairs_intensities, axis=-1)
        denominator = tf.reduce_sum(denominator, axis=-1) - mean_self_intensity

        # [batch_size, K]
        per_cluster_contiguous_penalty = tf.nn.relu(1. - numerator / (denominator + 1e-12))

        # [batch_size]
        numerator = tf.reduce_sum(numerator, axis=-1)
        denominator = tf.reduce_sum(denominator, axis=-1) + 1e-12

        # [batch_size]
        contiguous_penalty = tf.nn.relu(1. - numerator / denominator)

        return tf.nn.compute_average_loss(contiguous_penalty,
                                          global_batch_size=self.batch_size), per_cluster_contiguous_penalty

    def kernel_contiguous_constraint(self, pooling_matrix, adjacency_matrix, node_mask):

        if self.kernel.lower() == 'ptk':
            return 0., tf.zeros((pooling_matrix.shape[0], pooling_matrix.shape[-1]), dtype=pooling_matrix.dtype)
        elif self.kernel.lower() == 'stk':
            return self._stk_contiguous_sequence_constraint(pooling_matrix=pooling_matrix,
                                                            adjacency_matrix=adjacency_matrix,
                                                            node_mask=node_mask)
        else:
            return self._sstk_contiguous_sequence_constraint(pooling_matrix=pooling_matrix,
                                                             adjacency_matrix=adjacency_matrix,
                                                             node_mask=node_mask)

    def minimal_tree_loss(self, pooling_matrix, overlapping_threshold=0.1):
        # pooling_matrix -> [batch_size, max_nodes, K]
        # node_mask -> [batch_size, max_nodes]

        meta_node_amount = pooling_matrix.shape[-1]

        # [batch_size, K, K]
        self_intensities = tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)
        normalized_self_intensities = self_intensities / (
                tf.norm(self_intensities, axis=(-1, -2))[:, None, None] + 1e-12)

        # [1, K, K]
        normalized_percentage = tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)
        normalized_percentage = normalized_percentage / tf.norm(normalized_percentage, axis=(-1, -2))
        normalized_percentage = tf.expand_dims(normalized_percentage, axis=0)

        # [batch_size, K, K]
        per_cluster_penalty = normalized_self_intensities - normalized_percentage
        per_cluster_penalty *= tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :]

        per_cluster_overlap_penalty = tf.nn.relu(normalized_self_intensities - overlapping_threshold)
        per_cluster_overlap_penalty *= (
                tf.ones_like(per_cluster_overlap_penalty) - tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[
                                                            None, :, :])

        per_cluster_total_penalty = per_cluster_penalty + per_cluster_overlap_penalty

        # [batch_size,]
        red_per_cluster_penalty = tf.norm(per_cluster_total_penalty, axis=(-1, -2))
        red_per_cluster_penalty = tf.nn.compute_average_loss(red_per_cluster_penalty, global_batch_size=self.batch_size)

        return red_per_cluster_penalty, tf.norm(per_cluster_penalty, axis=-1)

    def minimal_tree_loss_separate(self, pooling_matrix, overlapping_threshold=0.85):

        # pooling_matrix -> [batch_size, max_nodes, K]
        # node_mask -> [batch_size, max_nodes]

        meta_node_amount = pooling_matrix.shape[-1]

        # Computing thresholds
        # target_value = tf.sqrt(tf.cast(meta_node_amount, pooling_matrix.dtype))
        # overlap_bound = overlapping_threshold * target_value
        # size_bound = size_threshold * target_value

        # [batch_size, K, K]
        self_intensities = tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)
        # normalized_self_intensities = self_intensities / (
        #         tf.norm(self_intensities, axis=(-1, -2))[:, None, None] + 1e-12)

        # [batch_size, K]
        diag_self_intensities = self_intensities * tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :]
        diag_self_intensities = tf.reduce_sum(diag_self_intensities, axis=-1)

        # [batch_size, K, K]
        cross_intensities = self_intensities * (
                1. - tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :])
        cross_intensities = cross_intensities / (diag_self_intensities[:, :, None] + 1e-7)
        cross_intensities = tf.minimum(cross_intensities, 1.)  # avoid overflows

        # [batch_size, K, K]
        # per_cluster_penalty = tf.nn.relu(tf.abs(normalized_self_intensities - target_value) - size_bound)
        # per_cluster_penalty *= tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :]

        per_cluster_overlap_penalty = tf.nn.relu(cross_intensities - overlapping_threshold)
        per_cluster_overlap_penalty *= (
                tf.ones_like(per_cluster_overlap_penalty) - tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[
                                                            None, :, :])

        # per_cluster_total_penalty = per_cluster_penalty + per_cluster_overlap_penalty

        # [batch_size,]
        red_per_cluster_penalty = tf.norm(per_cluster_overlap_penalty, axis=(-1, -2))
        red_per_cluster_penalty = tf.nn.compute_average_loss(red_per_cluster_penalty, global_batch_size=self.batch_size)

        return red_per_cluster_penalty, tf.norm(per_cluster_overlap_penalty, axis=-1)

    def minimal_tree_intensity_loss(self, pooling_matrix, node_mask, connectivity_threshold=0.3):
        # pooling_matrix -> [batch_size, max_nodes, K]
        # node_mask -> [batch_size, max_nodes]
        node_mask = tf.squeeze(node_mask, axis=-1)

        meta_node_amount = pooling_matrix.shape[-1]

        # [batch_size, K, K]
        self_intensities = tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)
        diag_self_intensities = self_intensities * tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :]

        # [batch_size]
        true_tree_size = tf.reduce_sum(node_mask, axis=-1)
        bound_intensity = connectivity_threshold * (true_tree_size / meta_node_amount)

        # [batch_size, K, K]
        minimum_intensity = tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)[None, :, :] * bound_intensity[:, None,
                                                                                               None]

        # [batch_size, K, K]
        intensity_penalty = tf.nn.relu(minimum_intensity - diag_self_intensities)

        # [batch_size, K]
        per_cluster_intensity_penalty = tf.reduce_sum(intensity_penalty, axis=-1)

        # [batch_size,]
        intensity_penalty = tf.norm(intensity_penalty, axis=(-1, -2))

        # []
        intensity_penalty = tf.nn.compute_average_loss(intensity_penalty, global_batch_size=self.batch_size)

        return intensity_penalty, per_cluster_intensity_penalty

    def _compute_tree_constraints(self, pooling_block, pooling_matrix, adjacency_matrix, node_mask):

        if node_mask is None:
            node_mask = tf.ones(shape=(adjacency_matrix.shape[0], pooling_matrix.shape[1], 1), dtype=tf.float32)
        else:
            node_mask = node_mask[:, :, None]

        adjacency_matrix = tf.reshape(adjacency_matrix,
                                      [adjacency_matrix.shape[0], pooling_matrix.shape[1], pooling_matrix.shape[1]])
        adjacency_matrix = tf.cast(adjacency_matrix, tf.float32)

        # Ignore padding nodes
        pooling_matrix = pooling_matrix * node_mask

        # Losses
        sequence_reg, \
        cluster_sequence_reg = self.contiguous_sequence_constraint(pooling_matrix=pooling_matrix,
                                                                   adjacency_matrix=adjacency_matrix,
                                                                   node_mask=node_mask)

        kernel_reg, \
        cluster_kernel_reg = self.kernel_contiguous_constraint(pooling_matrix=pooling_matrix,
                                                               adjacency_matrix=adjacency_matrix,
                                                               node_mask=node_mask)

        mean_size_reg, \
        cluster_mean_size_reg = self.minimal_tree_loss_separate(pooling_matrix=pooling_matrix,
                                                                overlapping_threshold=self.overlapping_threshold)

        tree_intensity_reg, \
        cluster_tree_intensity_reg = self.minimal_tree_intensity_loss(pooling_matrix=pooling_matrix,
                                                                      node_mask=node_mask,
                                                                      connectivity_threshold=self.connectivity_threshold)

        # Add losses to block
        pooling_block.sequence_reg = sequence_reg
        pooling_block.cluster_sequence_reg = cluster_sequence_reg
        pooling_block.kernel_reg = kernel_reg
        pooling_block.cluster_kernel_reg = cluster_kernel_reg
        pooling_block.mean_size_reg = mean_size_reg
        pooling_block.cluster_mean_size_reg = cluster_mean_size_reg
        pooling_block.tree_intensity_reg = tree_intensity_reg
        pooling_block.cluster_tree_intensity_reg = cluster_tree_intensity_reg

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size, max_nodes]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            block_nodes, \
            block_adjacency_matrix, \
            block_node_mask, \
            block_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                  'adjacency_matrix': current_adjacency_matrix,
                                                  'node_mask': current_node_mask},
                                                 training=training,
                                                 enable_regularization=layer_id != len(self.gcn_blocks) - 1)

            # Store for quick retrieval
            pooling_block.adjacency_matrix = current_adjacency_matrix
            pooling_block.pooling_matrix = block_pooling_matrix
            pooling_block.node_mask = current_node_mask

            # Avoid computing rules on the last layer (makes no sense)
            if layer_id != len(self.gcn_blocks) - 1:
                self._compute_tree_constraints(pooling_matrix=block_pooling_matrix,
                                               pooling_block=pooling_block,
                                               adjacency_matrix=current_adjacency_matrix,
                                               node_mask=current_node_mask)

            current_nodes = block_nodes
            current_adjacency_matrix = block_adjacency_matrix
            current_node_mask = block_node_mask

        # Step 6: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        graph_representation = current_nodes
        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        model_additional_info = {
            "answer": graph_representation,
            'lagrangian_multipliers': self.multipliers,
        }

        # Additional info
        for layer_id in range(len(self.pooling_blocks)):
            for key in ['adjacency_matrix',
                        'pooling_matrix',
                        'node_mask',
                        'cluster_sequence_reg',
                        'cluster_kernel_reg',
                        'cluster_mean_size_reg',
                        'cluster_tree_intensity_reg']:
                if hasattr(self.pooling_blocks[layer_id], key):
                    model_additional_info['{0}_{1}'.format(key, layer_id)] = getattr(self.pooling_blocks[layer_id], key)

        # [batch_size, num_classes]
        return graph_representation, model_additional_info


class M_Dual_Tree_Pooled_Adj_GNN(M_Single_Tree_Pooled_Adj_GNN):

    def __init__(self, rnn_weights, self_attention_weights, **kwargs):
        super(M_Dual_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)

        self.rnn_weights = rnn_weights
        self.rnn_test = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.rnn_weights[-1],
                                                                           return_sequences=False,
                                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                                               self.l2_regularization)))

        self.rnn_blocks = []
        for weight in self.rnn_weights[:-1]:
            self.rnn_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                      return_sequences=True,
                                                                                      kernel_regularizer=tf.keras.regularizers.l2(
                                                                                          self.l2_regularization))))
            self.rnn_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.rnn_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.rnn_weights[-1],
                                                                                  return_sequences=False,
                                                                                  kernel_regularizer=tf.keras.regularizers.l2(
                                                                                      self.l2_regularization))))

        self.self_attention_blocks = []
        for idx, weight in enumerate(self_attention_weights):
            self.self_attention_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                    activation=tf.nn.tanh,
                                                                    kernel_regularizer=tf.keras.regularizers.l2(
                                                                        self.l2_regularization)))
            self.self_attention_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.self_attention_blocks.append(tf.keras.layers.Dense(units=1, activation=tf.nn.sigmoid))

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, max_sentence_size]
        text_ids = inputs['text_ids']

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size, max_nodes]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # [batch_size, max_sentence_size, embedding_dim]
        text_ids_emb = self.input_embedding(text_ids)
        text_ids_emb = self.input_dropout(text_ids_emb, training=training)

        for rnn_block in self.rnn_blocks:
            text_ids_emb = rnn_block(text_ids_emb, training=training)

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            block_nodes, \
            block_adjacency_matrix, \
            block_node_mask, \
            block_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                  'adjacency_matrix': current_adjacency_matrix,
                                                  'node_mask': current_node_mask},
                                                 training=training,
                                                 enable_regularization=layer_id != len(self.gcn_blocks) - 1)

            # Avoid computing rules on the last layer (makes no sense)
            if layer_id != len(self.gcn_blocks) - 1:
                self._compute_tree_constraints(pooling_matrix=block_pooling_matrix,
                                               pooling_block=pooling_block,
                                               adjacency_matrix=current_adjacency_matrix,
                                               node_mask=current_node_mask)

            current_nodes = block_nodes
            current_adjacency_matrix = block_adjacency_matrix
            current_node_mask = block_node_mask

        # Step 6: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        # current_nodes = tf.expand_dims(current_nodes, axis=1)
        # text_ids_emb = tf.expand_dims(text_ids_emb, axis=1)

        # [batch_size, 2, dim]
        graph_representation = tf.concat((current_nodes, text_ids_emb), axis=-1)

        # graph_representation = current_nodes
        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        # [batch_size, num_classes]
        return graph_representation, {
            "answer": graph_representation,
            'lagrangian_multipliers': self.multipliers,
        }


class Pos_M_Single_Tree_Pooled_Adj_GNN(M_Single_Tree_Pooled_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_Single_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size, max_nodes]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            block_nodes, \
            block_adjacency_matrix, \
            block_node_mask, \
            block_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                  'adjacency_matrix': current_adjacency_matrix,
                                                  'node_mask': current_node_mask},
                                                 training=training,
                                                 enable_regularization=layer_id != len(self.gcn_blocks) - 1)

            # Store for quick retrieval
            pooling_block.adjacency_matrix = current_adjacency_matrix
            pooling_block.pooling_matrix = block_pooling_matrix
            pooling_block.node_mask = current_node_mask

            # Avoid computing rules on the last layer (makes no sense)
            if layer_id != len(self.gcn_blocks) - 1:
                self._compute_tree_constraints(pooling_matrix=block_pooling_matrix,
                                               pooling_block=pooling_block,
                                               adjacency_matrix=current_adjacency_matrix,
                                               node_mask=current_node_mask)
            current_nodes = block_nodes
            current_adjacency_matrix = block_adjacency_matrix
            current_node_mask = block_node_mask

        # Step 6: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], current_nodes.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            current_nodes = tf.concat((current_nodes, position_id), axis=-1)

        graph_representation = current_nodes
        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        model_additional_info = {
            "answer": graph_representation,
            'lagrangian_multipliers': self.multipliers,
        }

        # Additional info
        for layer_id in range(len(self.pooling_blocks)):
            for key in ['adjacency_matrix',
                        'pooling_matrix',
                        'node_mask',
                        'cluster_sequence_reg',
                        'cluster_kernel_reg',
                        'cluster_mean_size_reg',
                        'cluster_tree_intensity_reg']:
                if hasattr(self.pooling_blocks[layer_id], key):
                    model_additional_info['{0}_{1}'.format(key, layer_id)] = getattr(self.pooling_blocks[layer_id], key)

        # [batch_size, num_classes]
        return graph_representation, model_additional_info


class Pos_M_Dual_Tree_Pooled_Adj_GNN(M_Dual_Tree_Pooled_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_Dual_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, max_sentence_size]
        text_ids = inputs['text_ids']

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids = inputs['node_ids']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix = inputs['adjacency_matrix']

        # [batch_size, max_nodes]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_sentence_size, embedding_dim]
        text_ids_emb = self.input_embedding(text_ids)
        text_ids_emb = self.input_dropout(text_ids_emb, training=training)

        for rnn_block in self.rnn_blocks:
            text_ids_emb = rnn_block(text_ids_emb, training=training)

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            block_nodes, \
            block_adjacency_matrix, \
            block_node_mask, \
            block_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                  'adjacency_matrix': current_adjacency_matrix,
                                                  'node_mask': current_node_mask},
                                                 training=training,
                                                 enable_regularization=layer_id != len(self.gcn_blocks) - 1)

            # Avoid computing rules on the last layer (makes no sense)
            if layer_id != len(self.gcn_blocks) - 1:
                self._compute_tree_constraints(pooling_matrix=block_pooling_matrix,
                                               pooling_block=pooling_block,
                                               adjacency_matrix=current_adjacency_matrix,
                                               node_mask=current_node_mask)

            current_nodes = block_nodes
            current_adjacency_matrix = block_adjacency_matrix
            current_node_mask = block_node_mask

        # Step 6: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        current_nodes = tf.expand_dims(current_nodes, axis=1)
        text_ids_emb = tf.expand_dims(text_ids_emb, axis=1)

        # [batch_size, 2, dim]
        graph_representation = tf.concat((current_nodes, text_ids_emb), axis=1)

        # [batch_size, 2, 1]
        dual_attention = graph_representation
        for self_attention_block in self.self_attention_blocks:
            dual_attention = self_attention_block(dual_attention, training=training)

        # [batch_size, 2 * dim]
        graph_representation *= dual_attention
        graph_representation = tf.reshape(graph_representation, [graph_representation.shape[0], -1])

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], current_nodes.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            graph_representation = tf.concat((graph_representation, position_id), axis=-1)

        for representation_block in self.representation_blocks:
            graph_representation = representation_block(graph_representation, training=training)

        # [batch_size, num_classes]
        return graph_representation, {
            "answer": graph_representation,
            'lagrangian_multipliers': self.multipliers,
        }


class M_Multi_Adj_GNN(M_Single_Adj_GNN):

    def _gnn_block(self, node_ids, adjacency_matrix, training=False):
        # [batch_size,]
        node_mask = tf.cast(tf.not_equal(node_ids, 0), tf.float32)

        # Step 1: Input embedding

        # [batch_size, max_nodes, embedding_dim]
        node_ids_emb = self.input_embedding(node_ids)
        node_ids_emb = self.input_dropout(node_ids_emb, training=training)

        if self.rnn_encoding:
            node_ids_emb = self.rnn_encoder(node_ids_emb, training=training)

        # Step 2: Iterate over reasoning steps
        current_nodes = node_ids_emb
        current_adjacency_matrix = adjacency_matrix
        current_node_mask = node_mask

        for layer_id, (gcn_block, pooling_block) in enumerate(zip(self.gcn_blocks, self.pooling_blocks)):
            # Step 3: GCN block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, nodes, new_dim]
            current_nodes = gcn_block({'nodes': current_nodes,
                                       'adjacency_matrix': current_adjacency_matrix},
                                      training=training)

            # Step 4: Pooling block
            # current_nodes goes
            # from -> [batch_size, nodes, dim]
            # to   -> [batch_size, new_nodes, dim]
            #
            # current_adjacency_matrix goes
            # from  -> [batch_size, nodes, nodes]
            # to    -> [batch_size, new_nodes, new_nodes]
            #
            # current_node_mask is
            # a proper mask for the first time
            # None after the first block (we do not have padding nodes anymore!)
            current_nodes, \
            current_adjacency_matrix, \
            current_node_mask, \
            current_pooling_matrix = pooling_block({'nodes': current_nodes,
                                                    'adjacency_matrix': current_adjacency_matrix,
                                                    'node_mask': current_node_mask},
                                                   training=training,
                                                   enable_regularization=layer_id != len(self.gcn_blocks) - 1)

        # Step 5: Final encoding
        current_nodes = tf.squeeze(current_nodes)

        return current_nodes

    def call(self, inputs, training=False, **kwargs):

        # Step 0: Inputs

        # [batch_size, max_nodes]
        node_ids_1 = inputs['node_ids_1']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix_1 = inputs['adjacency_matrix_1']

        # [batch_size, max_nodes]
        node_ids_2 = inputs['node_ids_2']

        # [batch_size, max_nodes * max_nodes]
        adjacency_matrix_2 = inputs['adjacency_matrix_2']

        graph_representation_1 = self._gnn_block(node_ids=node_ids_1,
                                                 adjacency_matrix=adjacency_matrix_1,
                                                 training=training)

        graph_representation_2 = self._gnn_block(node_ids=node_ids_2,
                                                 adjacency_matrix=adjacency_matrix_2,
                                                 training=training)

        # Step 6: Final encoding
        graph_representation = tf.concat((graph_representation_1, graph_representation_2), axis=-1)
        for representation_block in self.representation_blocks[-1:]:
            graph_representation = representation_block(graph_representation, training=training)

        # [batch_size, num_classes]
        return graph_representation, {
            "answer": graph_representation,
        }


# Baselines


class M_Baseline_Sum(tf.keras.Model):

    def __init__(self, sentence_size, vocab_size,
                 answer_weights, embedding_dimension,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None, num_labels=1):
        super(M_Baseline_Sum, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=num_labels,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        sentence_encoding = tf.reduce_sum(sentence_emb, axis=1)

        answer = sentence_encoding
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class M_Baseline_LSTM(tf.keras.Model):
    """
    """

    def __init__(self, sentence_size, vocab_size, lstm_weights,
                 answer_weights, embedding_dimension, num_classes,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None):
        super(M_Baseline_LSTM, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension
        self.num_classes = num_classes

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=[
                                                             embedding_matrix] if embedding_matrix is not None else None,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.lstm_blocks = []
        for weight in self.lstm_weights[:-1]:
            self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                       return_sequences=True,
                                                                                       kernel_regularizer=tf.keras.regularizers.l2(
                                                                                           self.l2_regularization))))
            self.lstm_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.lstm_weights[-1],
                                                                                   return_sequences=False,
                                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                                       self.l2_regularization))))

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        lstm_input = sentence_emb
        for block in self.lstm_blocks:
            lstm_input = block(lstm_input, training=training)

        answer = lstm_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class M_BERT(tf.keras.Model):

    def __init__(self, bert_config, preloaded_model_name,
                 answer_weights, num_classes,
                 l2_regularization=0., dropout_rate=0.2, is_bert_trainable=False):
        super(M_BERT, self).__init__()
        self.bert_config = bert_config
        self.preloaded_model_name = preloaded_model_name
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.num_classes = num_classes
        self.is_bert_trainable = is_bert_trainable

        self.distilbert = TFBertModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                      config=bert_config, name='bert')

        if not self.is_bert_trainable:
            Logger.get_logger(__name__).info('Freezing BERT layers...')
            for layer in self.distilbert.layers:
                layer.trainable = False

        self.pre_classifier = tf.keras.layers.Dense(self.bert_config.hidden_size,
                                                    activation='relu',
                                                    name="pre_classifier")
        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, state='prediction', **kwargs):
        bert_output = self.distilbert(inputs, training=training, **kwargs)
        pooled_output = bert_output[1]  # (bs, seq_len, dim)
        pooled_output = self.pre_classifier(pooled_output, training=training)  # (bs, dim)

        answer = pooled_output
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class M_Pos_BERT(M_BERT):

    def __init__(self, use_position_feature=False, **kwargs):
        super(M_Pos_BERT, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, state='prediction', **kwargs):
        bert_output = self.distilbert(inputs, training=training, **kwargs)
        pooled_output = bert_output[1]  # (bs, seq_len, dim)
        pooled_output = self.pre_classifier(pooled_output, training=training)  # (bs, dim)

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], pooled_output.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            answer = tf.concat((pooled_output, position_id), axis=-1)
        else:
            answer = pooled_output

        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class Pos_M_baseline_LSTM(M_Baseline_LSTM):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_baseline_LSTM, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        lstm_input = sentence_emb
        for block in self.lstm_blocks:
            lstm_input = block(lstm_input, training=training)

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], lstm_input.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            lstm_input = tf.concat((lstm_input, position_id), axis=-1)

        answer = lstm_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class M_Baseline_Dense(tf.keras.Model):
    """
    LSTM baseline for ToS Task 1. This is a simple stacked-LSTMs model.
    """

    def __init__(self, sentence_size, vocab_size, dense_weights,
                 answer_weights, embedding_dimension, num_classes,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None):
        super(M_Baseline_Dense, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension
        self.num_classes = num_classes
        self.dense_weights = dense_weights

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=[
                                                             embedding_matrix] if embedding_matrix is not None else None,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.dense_blocks = []
        for weight in dense_weights:
            self.dense_blocks.append(tf.keras.layers.Dense(units=weight,
                                                           activation=tf.nn.leaky_relu,
                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                               l2_regularization)))
            self.dense_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.dense_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                       kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        dense_input = sentence_emb
        for block in self.dense_blocks:
            dense_input = block(dense_input, training=training)

        dense_input = tf.reduce_sum(dense_input, axis=1)

        answer = dense_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class M_Graph_Baseline_LSTM(tf.keras.Model):

    def __init__(self, sentence_size, vocab_size, lstm_weights,
                 answer_weights, embedding_dimension, num_classes,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None):
        super(M_Graph_Baseline_LSTM, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension
        self.num_classes = num_classes

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=[
                                                             embedding_matrix] if embedding_matrix is not None else None,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.lstm_blocks = []
        for weight in self.lstm_weights[:-1]:
            self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                       return_sequences=True,
                                                                                       kernel_regularizer=tf.keras.regularizers.l2(
                                                                                           self.l2_regularization))))
            self.lstm_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.lstm_weights[-1],
                                                                                   return_sequences=False,
                                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                                       self.l2_regularization))))

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['node_ids']

        sentence_emb = self.input_embedding(sentence)

        lstm_input = sentence_emb
        for block in self.lstm_blocks:
            lstm_input = block(lstm_input, training=training)

        answer = lstm_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class Pos_M_Graph_Baseline_LSTM(M_Graph_Baseline_LSTM):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_Graph_Baseline_LSTM, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['node_ids']

        sentence_emb = self.input_embedding(sentence)

        lstm_input = sentence_emb
        for block in self.lstm_blocks:
            lstm_input = block(lstm_input, training=training)

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], lstm_input.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            lstm_input = tf.concat((lstm_input, position_id), axis=-1)

        answer = lstm_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class M_Dual_Graph_Baseline_LSTM(tf.keras.Model):

    def __init__(self, sentence_size, vocab_size, lstm_weights, self_attention_weights,
                 answer_weights, embedding_dimension, num_classes,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None):
        super(M_Dual_Graph_Baseline_LSTM, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension
        self.num_classes = num_classes

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=[
                                                             embedding_matrix] if embedding_matrix is not None else None,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.lstm_blocks = []
        for weight in self.lstm_weights[:-1]:
            self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                       return_sequences=True,
                                                                                       kernel_regularizer=tf.keras.regularizers.l2(
                                                                                           self.l2_regularization))))
            self.lstm_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.lstm_weights[-1],
                                                                                   return_sequences=False,
                                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                                       self.l2_regularization))))

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=num_classes,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

        self.self_attention_blocks = []
        for idx, weight in enumerate(self_attention_weights):
            self.self_attention_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                    activation=tf.nn.tanh,
                                                                    kernel_regularizer=tf.keras.regularizers.l2(
                                                                        self.l2_regularization)))
            self.self_attention_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.self_attention_blocks.append(tf.keras.layers.Dense(units=1, activation=tf.nn.sigmoid))

    def _forward(self, x, training=False):
        x_emb = self.input_embedding(x)

        lstm_input = x_emb
        for block in self.lstm_blocks:
            lstm_input = block(lstm_input, training=training)

        return lstm_input

    def call(self, inputs, training=False, **kwargs):
        structure_node_ids = inputs['structure_node_ids']
        sentence_node_ids = inputs['sentence_node_ids']

        structure_node_emb = self._forward(structure_node_ids, training=training)
        sentence_node_emb = self._forward(sentence_node_ids, training=training)

        # [batch_size, 1, dim]
        structure_node_emb = tf.expand_dims(structure_node_emb, 1)
        sentence_node_emb = tf.expand_dims(sentence_node_emb, 1)

        # [batch_size, 2, dim]
        dual_nodes = tf.concat((structure_node_emb, sentence_node_emb), axis=1)

        # [batch_size, 2, 1]
        dual_attention = dual_nodes
        for self_attention_block in self.self_attention_blocks:
            dual_attention = self_attention_block(dual_attention, training=training)

        # [batch_size, 2 * dim]
        dual_nodes *= dual_attention
        aggregated = tf.reshape(dual_nodes, [dual_nodes.shape[0], -1])

        answer = aggregated
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None


class Pos_M_Dual_Graph_Baseline_LSTM(M_Dual_Graph_Baseline_LSTM):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_M_Dual_Graph_Baseline_LSTM, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def call(self, inputs, training=False, **kwargs):
        structure_node_ids = inputs['structure_node_ids']
        sentence_node_ids = inputs['sentence_node_ids']

        structure_node_emb = self._forward(structure_node_ids, training=training)
        sentence_node_emb = self._forward(sentence_node_ids, training=training)

        # [batch_size, 1, dim]
        structure_node_emb = tf.expand_dims(structure_node_emb, 1)
        sentence_node_emb = tf.expand_dims(sentence_node_emb, 1)

        # [batch_size, 2, dim]
        dual_nodes = tf.concat((structure_node_emb, sentence_node_emb), axis=1)

        # [batch_size, 2, 1]
        dual_attention = dual_nodes
        for self_attention_block in self.self_attention_blocks:
            dual_attention = self_attention_block(dual_attention, training=training)

        # [batch_size, 2 * dim]
        dual_nodes *= dual_attention
        aggregated = tf.reshape(dual_nodes, [dual_nodes.shape[0], -1])

        if self.use_position_feature:
            position_id = tf.cast(inputs['position'], aggregated.dtype)
            position_id = tf.expand_dims(position_id, axis=-1)
            aggregated = tf.concat((aggregated, position_id), axis=-1)

        answer = aggregated
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, None



# Layers #


class MP_GCN(tf.keras.layers.Layer):

    def __init__(self, node_weights, edge_weights, message_weights, l2_regularization=0.,
                 dropout_rate=0., node_encoding=True, edge_encoding=True, **kwargs):
        super(MP_GCN, self).__init__(**kwargs)
        self.node_weights = node_weights
        self.edge_weights = edge_weights
        self.message_weights = message_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.node_encoding = node_encoding
        self.edge_encoding = edge_encoding
        self.last_dimension = node_weights[-1]

        # Node embedding
        self.node_blocks = []
        for idx, weight in enumerate(node_weights):
            self.node_blocks.append(tf.keras.layers.Dense(units=weight,
                                                          activation=tf.nn.leaky_relu if idx < len(node_weights) - 1
                                                          else None,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              l2_regularization)))
            self.node_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        # Edge embedding
        if edge_weights is not None and edge_encoding:
            self.edge_blocks = []
            for idx, weight in enumerate(edge_weights):
                self.edge_blocks.append(tf.keras.layers.Dense(units=weight,
                                                              activation=tf.nn.leaky_relu if idx < len(
                                                                  edge_weights) - 1 else None,
                                                              kernel_regularizer=tf.keras.regularizers.l2(
                                                                  l2_regularization)))
                self.edge_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        # Message passing embedding
        self.message_blocks = []
        for weight in message_weights:
            self.message_blocks.append(tf.keras.layers.Dense(units=weight,
                                                             activation=tf.nn.leaky_relu,
                                                             kernel_regularizer=tf.keras.regularizers.l2(
                                                                 l2_regularization)))
            self.message_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.message_blocks.append(tf.keras.layers.Dense(units=self.last_dimension,
                                                         kernel_regularizer=tf.keras.regularizers.l2(
                                                             l2_regularization)))

        self.node_update_cell = tf.keras.layers.LSTMCell(self.last_dimension)

    def _message_passing(self, nodes, edge_features, edge_indices, edge_mask,
                         node_indices, node_segments, directionality_mask, message_blocks,
                         training=False):
        # nodes -> [batch_size, max_nodes, hidden_size]
        # edge_features -> [batch_size, max_edges, hidden_size]
        # edge_indices -> [batch_size, max_edges, 2]
        # edge_mask ->  [batch_size, max_edges]
        # node_indices -> [batch_size, max_connections]
        # node_segments -> [batch_size * max_connections]
        # In a tree: max_connections -> max_edges * 2 -> (max_nodes - 1) * 2

        # Step 1: computing message passing information

        # [batch_size, max_edges, 2, hidden_size]
        pairs = tf.gather(nodes, edge_indices, axis=1, batch_dims=1)
        pairs = tf.reshape(pairs, [tf.shape(nodes)[0], tf.shape(edge_indices)[1], 2, nodes.shape[-1]])

        # [batch_size, max_edges, 2 * hidden_size]
        pairs = tf.reshape(pairs, [tf.shape(pairs)[0], tf.shape(pairs)[1], 2 * nodes.shape[-1]])

        # [batch_size, max_edges, 3 * hidden_size]
        if self.edge_encoding:
            mp_input = tf.concat((pairs, edge_features), axis=-1)
        else:
            mp_input = pairs

        # [batch_size, max_edges, hidden_size]
        mp_output = mp_input
        for message_block in message_blocks:
            mp_output = message_block(mp_output, training=training)

        # [batch_size, max_edges, 1]
        edge_mask = tf.expand_dims(edge_mask, axis=-1)
        edge_mask = tf.cast(edge_mask, tf.float32)
        mp_output = mp_output * edge_mask

        # Step 2: aggregating message passing info per node

        # [batch_size, max_connections, hidden_size]
        per_node_messages = tf.gather(mp_output, node_indices, batch_dims=1)

        # [batch_size * max_connections, hidden_size]
        per_node_messages = tf.reshape(per_node_messages, [-1, per_node_messages.shape[-1]])

        directionality_mask = tf.cast(directionality_mask, tf.float32)
        directionality_mask = tf.reshape(directionality_mask, [-1, 1])
        per_node_messages *= directionality_mask

        # [batch_size, max_nodes, hidden_size]
        aggregated_per_node_info = tf.math.segment_sum(data=per_node_messages, segment_ids=node_segments)
        aggregated_per_node_info = tf.reshape(aggregated_per_node_info,
                                              [-1, tf.shape(nodes)[1], per_node_messages.shape[-1]])

        # [batch_size, max_nodes, hidden_size]
        return aggregated_per_node_info

    def _node_update(self, update_cell, input, state):

        original_shape = tf.shape(input)
        input = tf.reshape(input, [-1, input.shape[-1]])
        state = tf.reshape(state, [-1, state.shape[-1]])
        output, _ = update_cell(input, [state, state])
        output = tf.reshape(output, original_shape)

        return output

    def _mlp_node_update(self, update_mlp, input, state):

        # [batch_size, max_nodes, hidden_size * 2]
        mlp_input = tf.concat([input, state], axis=-1)

        # [batch_size, max_nodes, hidden_size]
        return update_mlp(mlp_input)

    def call(self, inputs, training=False, **kwargs):

        nodes = inputs['nodes']
        edge_indices = inputs['edge_indices']
        # edge_features = inputs['edge_features']
        edge_mask = inputs['edge_mask']
        node_indices = inputs['node_indices']
        node_segments = inputs['node_segments']
        directionality_mask = inputs['directionality_mask']

        # Node encoding
        if self.node_encoding:
            for node_block in self.node_blocks:
                nodes = node_block(nodes, training=training)

        # Edge encoding
        if self.edge_encoding:
            for edge_block in self.edge_blocks:
                edge_features = edge_block(edge_features)

        # Message passing
        messages = self._message_passing(nodes=nodes,
                                         edge_features=None,
                                         edge_indices=edge_indices,
                                         edge_mask=edge_mask,
                                         node_indices=node_indices,
                                         node_segments=node_segments,
                                         directionality_mask=directionality_mask,
                                         message_blocks=self.message_blocks,
                                         training=training)

        # Node update
        nodes = self._node_update(update_cell=self.node_update_cell,
                                  input=messages,
                                  state=nodes)

        return nodes


# TODO: add edge_features
class GCN(tf.keras.layers.Layer):

    def __init__(self, node_weights, message_weights, l2_regularization=0.,
                 dropout_rate=0., node_encoding=True, **kwargs):
        super(GCN, self).__init__(**kwargs)
        self.node_weights = node_weights
        self.message_weights = message_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.node_encoding = node_encoding
        self.last_dimension = node_weights[-1]

        # Node embedding
        self.node_blocks = []
        for idx, weight in enumerate(node_weights):
            self.node_blocks.append(tf.keras.layers.Dense(units=weight,
                                                          activation=tf.nn.leaky_relu if idx < len(
                                                              node_weights) - 1 else None,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              l2_regularization)))
            self.node_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        # Message passing embedding
        self.message_blocks = []
        for weight in message_weights:
            self.message_blocks.append(tf.keras.layers.Dense(units=weight,
                                                             activation=tf.nn.leaky_relu,
                                                             kernel_regularizer=tf.keras.regularizers.l2(
                                                                 l2_regularization)))
            self.message_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.message_blocks.append(tf.keras.layers.Dense(units=self.last_dimension,
                                                         kernel_regularizer=tf.keras.regularizers.l2(
                                                             l2_regularization)))

        self.node_update_cell = tf.keras.layers.LSTMCell(self.last_dimension)

    def _message_passing_adj(self, nodes, adjacency_matrix, message_blocks, training=False):
        # past_messages -> [batch_size, max_nodes, hidden_size]
        # adjacency_matrix -> [batch_size, max_nodes * max_nodes]

        reshaped_adjacency_matrix = tf.reshape(adjacency_matrix, [-1, nodes.shape[1], nodes.shape[1]])
        reshaped_adjacency_matrix = tf.cast(reshaped_adjacency_matrix, tf.float32)

        # Step 1: computing message passing information

        # At first layer, we have self-loops, thus we do not need to add the identity matrix
        # At pooling layers, we don't have self-loops, thus we add them
        # [batch_size, max_nodes, max_nodes]
        A = reshaped_adjacency_matrix + tf.eye(num_rows=nodes.shape[1], batch_shape=[adjacency_matrix.shape[0]]) * (
                1 - reshaped_adjacency_matrix)

        # [batch_size, max_nodes, max_nodes]
        D = tf.linalg.diag(tf.reduce_sum(A, axis=2))
        D = tf.math.reciprocal_no_nan(tf.linalg.sqrtm(D))

        # [batch_size, max_nodes, max_nodes]
        mp_input = tf.matmul(D, A)

        # [batch_size, max_nodes, max_nodes]
        mp_input = tf.matmul(mp_input, D)

        # [batch_size, max_nodes, hidden_size]
        mp_input = tf.matmul(mp_input, nodes)

        # [batch_size, max_nodes, hidden_size]
        mp_output = mp_input
        for message_block in message_blocks:
            mp_output = message_block(mp_output, training=training)

        # [batch_size, max_nodes, hidden_size]
        return mp_output

    def _node_update(self, update_cell, input, state):

        original_shape = tf.shape(input)
        input = tf.reshape(input, [-1, input.shape[-1]])
        state = tf.reshape(state, [-1, state.shape[-1]])
        output, _ = update_cell(input, [state, state])
        output = tf.reshape(output, original_shape)

        return output

    def call(self, inputs, training=False, **kwargs):

        nodes = inputs['nodes']
        adjacency_matrix = inputs['adjacency_matrix']

        # Node encoding
        if self.node_encoding:
            for node_block in self.node_blocks:
                nodes = node_block(nodes, training=training)

        # Message passing
        messages = self._message_passing_adj(nodes=nodes,
                                             adjacency_matrix=adjacency_matrix,
                                             message_blocks=self.message_blocks,
                                             training=training)

        # Node update
        nodes = self._node_update(update_cell=self.node_update_cell,
                                  input=messages,
                                  state=nodes)

        return nodes


class MP_Hier_Pooling(tf.keras.layers.Layer):

    def __init__(self, aggregation_weights, gate_weights, l2_regularization=0.,
                 dropout_rate=0., **kwargs):
        super(MP_Hier_Pooling, self).__init__(**kwargs)
        self.aggregation_weights = aggregation_weights
        self.gate_weights = gate_weights
        self.dropout_rate = dropout_rate
        self.l2_regularization = l2_regularization

        self.subtree_aggregation_blocks = []
        for idx, weight in enumerate(aggregation_weights):
            self.subtree_aggregation_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                         activation=tf.nn.leaky_relu if idx < len(
                                                                             aggregation_weights) - 1 else None,
                                                                         kernel_regularizer=tf.keras.regularizers.l2(
                                                                             self.l2_regularization)))

            self.subtree_aggregation_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.subtree_gate_blocks = []
        for idx, weight in enumerate(gate_weights):
            self.subtree_gate_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                  activation=tf.nn.leaky_relu
                                                                  if idx < len(gate_weights) - 1 else None,
                                                                  kernel_regularizer=tf.keras.regularizers.l2(
                                                                      self.l2_regularization)))
            self.subtree_gate_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.subtree_gate_blocks.append(tf.keras.layers.Dense(units=1,
                                                              activation=tf.nn.sigmoid,
                                                              kernel_regularizer=tf.keras.regularizers.l2(
                                                                  self.l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        nodes = inputs['nodes']
        next_node_indices = inputs['next_node_indices']
        next_node_mask = inputs['next_node_mask']

        # [batch_size, max_subtrees, max_subtree_size, hidden_size]
        next_node_groups = tf.gather(nodes, next_node_indices, axis=1, batch_dims=1)

        next_node_groups_final = next_node_groups
        for aggregation_block in self.subtree_aggregation_blocks:
            next_node_groups_final = aggregation_block(next_node_groups_final, training=training)

        next_node_groups_gate = next_node_groups
        for gate_block in self.subtree_gate_blocks:
            next_node_groups_gate = gate_block(next_node_groups_gate, training=training)

        # [batch_size, max_subtrees, max_subtree_size, 1]
        next_level_mask = tf.cast(next_node_mask, next_node_groups_final.dtype)
        next_level_mask = tf.expand_dims(next_level_mask, -1)

        # [batch_size, max_subtrees, embedding_dim]
        next_nodes = tf.reduce_sum(next_node_groups_final * next_node_groups_gate * next_level_mask, axis=2)

        return next_nodes


class DiffPool(tf.keras.layers.Layer):

    def __init__(self, pooling_weights, aggregation_weights,
                 use_cluster_regularization=False, use_link_regularization=False,
                 graph_max_nodes=None, l2_regularization=0., dropout_rate=0., use_sigmoid=False, **kwargs):
        super(DiffPool, self).__init__(**kwargs)
        self.pooling_weights = pooling_weights
        self.aggregation_weights = aggregation_weights
        self.use_cluster_regularization = use_cluster_regularization
        self.use_link_regularization = use_link_regularization
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.graph_max_nodes = graph_max_nodes
        self.use_sigmoid = use_sigmoid

        self.aggregation_blocks = []
        for idx, weight in enumerate(aggregation_weights):
            self.aggregation_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                 activation=tf.nn.leaky_relu if idx < len(
                                                                     aggregation_weights) - 1 else None,
                                                                 kernel_regularizer=tf.keras.regularizers.l2(
                                                                     self.l2_regularization)))

            self.aggregation_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.pooling_blocks = []
        for idx, weight in enumerate(self.pooling_weights):
            # Mmm, probably relu saturates to 0 too much -> always 0.5 for sigmoid activation
            self.pooling_blocks.append(tf.keras.layers.Dense(units=weight,
                                                             activation=tf.nn.leaky_relu if idx < len(
                                                                 self.pooling_weights) - 1 else None,
                                                             # kernel_initializer=tf.keras.initializers.RandomNormal(
                                                             #     mean=-0.5, stddev=1.),
                                                             kernel_regularizer=tf.keras.regularizers.l2(
                                                                 self.l2_regularization)))
            if idx < len(self.pooling_weights) - 1:
                self.pooling_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        # Losses
        self.cluster_reg = None
        self.link_reg = None

    def _compute_cluster_regularization(self, target, logits):
        # [batch_size, nodes, nodes]
        # -> [batch_size, nodes]
        pooling_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=target,
                                                                  logits=logits)

        # []
        pooling_entropy = tf.reduce_mean(pooling_entropy)
        return pooling_entropy

    def _compute_link_regularization(self, adjacency_matrix, pooling_matrix,
                                     batch_num_nodes=None, nodes_mask=None, epsilon=1e-7):
        """
        In the original code there's only one link loss, defined as follows:
        Given pooling_matrix and adjacency_matrix as PM_l, AM_l (l = layer), eps = 1e-7, BS_num_nodes -> [bs, 1]
        PM_pred_l = PM_l * (PM_l)^T
        PM_pred_l = min(PM_pred_l, tf.ones_like(PM_pred_l))
        Link_loss = -AM_l * tf.math.log(PM_pred_l + eps) - (1 - AM_l) * tf.math.log(1 - PM_pred_l + eps)
        num_entries = tf.reduce_sum(BS_num_nodes * BS_num_nodes)
        node_mask = build_mask(max_nodes, BS_num_nodes)
        AM_mask = node_mask * (node_mask)^T
        Link_loss[1 - AM_mask] = 0.
        Link_loss = tf.reduce_sum(link_loss) / num_entries
        """

        # [batch_size, max_nodes, max_nodes]
        adjacency_matrix = tf.reshape(adjacency_matrix, [-1, pooling_matrix.shape[1], pooling_matrix.shape[1]])
        adjacency_matrix = tf.cast(adjacency_matrix, tf.float32)

        # [batch_size, max_nodes, max_nodes]
        pred_pooling_matrix = tf.matmul(pooling_matrix, tf.transpose(pooling_matrix, [0, 2, 1]))
        pred_pooling_matrix = tf.minimum(pred_pooling_matrix, tf.ones_like(pred_pooling_matrix))

        # [batch_size, max_nodes, max_nodes]
        link_loss = - adjacency_matrix * tf.math.log(pred_pooling_matrix + epsilon) \
                    - (1 - adjacency_matrix) * tf.math.log(1 - pred_pooling_matrix + epsilon)

        if batch_num_nodes is not None:
            # [batch_size,]
            num_entries = tf.reduce_sum(batch_num_nodes * batch_num_nodes)

            # [batch_size, max_nodes, max_nodes]
            nodes_mask = tf.expand_dims(nodes_mask, -1)
            adjacency_mask = nodes_mask * tf.transpose(nodes_mask, [0, 2, 1])
            link_loss *= (1 - adjacency_mask)
        else:
            num_nodes = adjacency_matrix.shape[1]
            num_entries = num_nodes * num_nodes

        link_loss = tf.reduce_sum(link_loss, axis=-1)
        link_loss = tf.reduce_sum(link_loss, axis=-1)
        link_loss /= num_entries

        return link_loss

    def _retrieve_pooling_matrix(self, nodes, pooling_blocks, training=False):

        # [batch_size, max_graph_nodes, meta_nodes_amount]
        node_pool_matrix = nodes
        for pool_block in pooling_blocks:
            node_pool_matrix = pool_block(node_pool_matrix, training=training)

        if self.use_sigmoid:
            att_node_pool_matrix = tf.nn.sigmoid(node_pool_matrix)
        else:
            att_node_pool_matrix = tf.nn.softmax(node_pool_matrix, axis=-1)

        return att_node_pool_matrix, node_pool_matrix

    def _build_next_level_info(self, nodes, pooling_matrix, adj_matrix, aggregation_blocks, training=False):

        # Build next level nodes

        # [batch_size, max_graph_nodes, dim]
        node_final_repr = nodes
        for aggregation_block in aggregation_blocks:
            node_final_repr = aggregation_block(node_final_repr, training=training)

        # [batch_size, meta_nodes_amount, dim]
        next_level_nodes = tf.matmul(pooling_matrix, node_final_repr, transpose_a=True)

        # Build next level adjacency matrix
        adj_matrix = tf.cast(adj_matrix, tf.float32)
        adj_matrix = tf.reshape(adj_matrix, [-1, node_final_repr.shape[1], node_final_repr.shape[1]])

        # [batch_size, meta_nodes_amount, meta_nodes_amount]
        next_level_adj_matrix = tf.matmul(pooling_matrix, adj_matrix, transpose_a=True)
        next_level_adj_matrix = tf.matmul(next_level_adj_matrix, pooling_matrix)

        # next_level_adj_matrix = next_level_adj_matrix - tf.expand_dims(tf.eye(next_level_adj_matrix.shape[-1]),
        #                                                                axis=0) * next_level_adj_matrix
        #
        # # [batch_size, meta_nodes_amount, 1]
        # degree_matrix = tf.reduce_sum(next_level_adj_matrix, axis=-1)
        # degree_matrix = tf.sqrt(degree_matrix) + 1e-12
        # degree_matrix = tf.expand_dims(degree_matrix, axis=-1)
        #
        # next_level_adj_matrix = (next_level_adj_matrix / degree_matrix) / tf.transpose(degree_matrix, [0, 2, 1])

        return next_level_nodes, next_level_adj_matrix

    def call(self, inputs, training=False, enable_regularization=True, **kwargs):

        nodes = inputs['nodes']
        adjacency_matrix = inputs['adjacency_matrix']
        node_mask = inputs['node_mask']

        if node_mask is not None:
            # [batch_size,]
            batch_num_nodes = tf.reduce_sum(node_mask, axis=-1)
        else:
            batch_num_nodes = None

        # [batch_size, max_graph_nodes, meta_nodes_amount]
        pooling_matrix, raw_pooling_matrix = self._retrieve_pooling_matrix(nodes=nodes,
                                                                           pooling_blocks=self.pooling_blocks,
                                                                           training=training)

        next_nodes, \
        next_adjacency_matrix = self._build_next_level_info(nodes=nodes,
                                                            pooling_matrix=pooling_matrix,
                                                            adj_matrix=adjacency_matrix,
                                                            aggregation_blocks=self.aggregation_blocks,
                                                            training=training)

        if self.use_cluster_regularization and enable_regularization:
            self.cluster_reg = self._compute_cluster_regularization(logits=raw_pooling_matrix,
                                                                    target=pooling_matrix)

        if self.use_link_regularization and enable_regularization:
            self.link_reg = self._compute_link_regularization(adjacency_matrix=adjacency_matrix,
                                                              pooling_matrix=pooling_matrix,
                                                              batch_num_nodes=batch_num_nodes,
                                                              nodes_mask=node_mask)

        return next_nodes, \
               next_adjacency_matrix, \
               None, \
               pooling_matrix


class ST_DiffPool(DiffPool):

    def __init__(self, target_mlp_weights, max_subtrees, target_threshold=0., **kwargs):
        super(ST_DiffPool, self).__init__(**kwargs)
        self.target_mlp_weights = target_mlp_weights
        self.max_subtrees = max_subtrees
        self.target_threshold = target_threshold

        self.target_mlp_blocks = []
        for idx, weight in enumerate(target_mlp_weights):
            self.target_mlp_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                activation=tf.nn.leaky_relu if idx < len(
                                                                    target_mlp_weights) - 1 else None,
                                                                kernel_regularizer=tf.keras.regularizers.l2(
                                                                    self.l2_regularization)))

            self.target_mlp_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.target_mlp_blocks.append(tf.keras.layers.Dense(units=self.max_subtrees))

    def _compute_subtree_regularization(self, target_matrix, subtree_mask, assignment_matrix):
        # target_matrix -> [batch_size, meta_nodes_amount, max_subtrees]
        # subtree_mask -> [batch_size, 1, max_subtrees]
        # assignment_matrix -> [batch_size, max_nodes, max_subtrees]

        assignment_matrix = tf.reshape(assignment_matrix, [target_matrix.shape[0], self.graph_max_nodes, -1])
        assignment_matrix = tf.cast(assignment_matrix, tf.float32)

        # Sparsity constraints

        # [batch_size, meta_nodes_amount, max_subtrees]
        base_distribution = target_matrix + -1e9 * (1 - subtree_mask)

        # At least one assignment

        # [batch_size, meta_nodes_amount, max_subtrees]
        alo_distribution = tf.nn.softmax(base_distribution, axis=-1)

        # [batch_size, meta_nodes_amount]
        alo_assignment = tf.nn.softmax_cross_entropy_with_logits(labels=alo_distribution,
                                                                 logits=target_matrix,
                                                                 axis=-1)
        # []
        alo_assignment = tf.reduce_mean(alo_assignment)

        # At most one assignment

        # [batch_size, max_subtrees, meta_nodes_amount]
        alo_distribution = tf.transpose(alo_distribution, [0, 2, 1])
        meta_node_amount = alo_distribution.shape[-1]

        # [batch_size, max_subtrees, meta_nodes_amount, meta_nodes_amount]
        amo_pairs = tf.expand_dims(alo_distribution, axis=2) * tf.expand_dims(alo_distribution, axis=-1)

        # self-reference and symmetry
        tril_mask = tf.linalg.band_part(tf.ones((meta_node_amount, meta_node_amount), dtype=tf.float32), -1, 0)
        amo_pairs = amo_pairs - tf.reshape(tril_mask, [1, 1, meta_node_amount, meta_node_amount]) * amo_pairs

        # [batch_size, max_subtrees]
        amo_assignment = tf.reduce_sum(amo_pairs, axis=-1)
        amo_assignment = tf.reduce_sum(amo_assignment, axis=-1)

        subtree_sizes = tf.reduce_sum(assignment_matrix, axis=1)
        subtree_norm = tf.maximum(subtree_mask, tf.ones_like(subtree_mask))
        subtree_norm = tf.squeeze(subtree_norm)
        size_penalty = (subtree_sizes / subtree_norm) + 1

        amo_assignment *= size_penalty

        amo_assignment *= tf.squeeze(subtree_mask)

        amo_assignment = tf.reduce_sum(amo_assignment, axis=-1)
        amo_assignment = tf.reduce_mean(amo_assignment)

        # # [batch_size, max_subtrees]
        # amo_assignment = tf.nn.relu(tf.reduce_sum(alo_distribution, axis=1) - 1.)
        #
        # # (penalization may be proportional to the subtree size)
        #
        # # [batch_size, max_subtrees]
        # subtree_sizes = tf.reduce_sum(assignment_matrix, axis=1)
        # subtree_norm = tf.maximum(subtree_mask, tf.ones_like(subtree_mask))
        # subtree_norm = tf.squeeze(subtree_norm)
        # size_penalty = (subtree_sizes / subtree_norm) + 1
        #
        # amo_assignment *= size_penalty
        #
        # # Avoid padding subtrees
        # amo_assignment *= tf.squeeze(subtree_mask)
        #
        # # []
        # amo_assignment = tf.reduce_sum(amo_assignment, axis=-1)
        # amo_assignment = tf.reduce_mean(amo_assignment)

        return alo_assignment, amo_assignment

    def _compute_cluster_separation_constraint(self, pooling_matrix):
        # pooling_matrix -> [batch_size, max_nodes, K]

        meta_node_amount = pooling_matrix.shape[-1]

        # [batch_size, K, K]
        cluster_overlap = tf.matmul(tf.transpose(pooling_matrix, [0, 2, 1]), pooling_matrix)

        # Remove self-reference and symmetry
        eye_matrix = tf.eye(meta_node_amount, dtype=pooling_matrix.dtype)
        cluster_overlap = 0.5 * (cluster_overlap - tf.expand_dims(eye_matrix, 0) * cluster_overlap)

        # [batch_size, K, K]
        normalization = tf.reduce_sum(pooling_matrix, axis=1)
        normalization = tf.minimum(tf.expand_dims(normalization, -1), tf.expand_dims(normalization, 1))

        # [batch_size,]
        constraint = tf.reduce_sum(cluster_overlap / normalization, axis=-1)
        constraint = tf.reduce_sum(constraint, axis=-1)

        pairs_amount = meta_node_amount * (meta_node_amount - 1) / 2
        pairs_amount = tf.maximum(pairs_amount, 1.)

        return tf.reduce_mean(constraint / pairs_amount)

    def _retrieve_guided_pooling_matrix(self, nodes, pooling_blocks, assignment_matrix, node_mask, training=False):
        # nodes -> [batch_size, max_nodes, hidden_dim]
        # pooling_blocks -> block of dense layers
        # assignment_matrix -> [batch_size, max_nodes, max_subtrees]
        # node_mask -> [batch_size, max_nodes]

        assignment_matrix = tf.reshape(assignment_matrix, [nodes.shape[0], self.graph_max_nodes, -1])
        assignment_matrix = tf.cast(assignment_matrix, tf.float32)

        node_mask = tf.ones((nodes.shape[0], nodes.shape[1]), dtype=nodes.dtype) \
            if node_mask is None else node_mask

        # Build target matrix

        # [batch_size, max_graph_nodes, meta_nodes_amount]
        node_pool_matrix = nodes
        for pool_block in pooling_blocks:
            node_pool_matrix = pool_block(node_pool_matrix, training=training)

        # Filter out padding nodes in pooling
        node_pool_matrix *= tf.expand_dims(node_mask, -1)

        # [batch_size, meta_nodes_amount, max_subtrees]
        target_matrix = tf.transpose(node_pool_matrix, [0, 2, 1])
        for block in self.target_mlp_blocks:
            target_matrix = block(target_matrix, training=training)

        # Build pooling matrix

        # Mask out padding subtrees
        # [batch_size, 1, subtrees_amount]
        subtree_mask = tf.cast(tf.not_equal(tf.reduce_sum(assignment_matrix, axis=1), 0.), tf.float32)
        subtree_mask = tf.expand_dims(subtree_mask, axis=1)

        # [batch_size, meta_nodes_amount, max_subtrees]
        target_distribution = target_matrix + -1e9 * (1 - subtree_mask)
        target_distribution = tf.nn.softmax(target_distribution, axis=-1)

        # [batch_size, meta_nodes_amount, 1, max_subtrees]
        ext_target_distribution = tf.expand_dims(target_distribution, axis=2)

        # [batch_size, 1, max_nodes, max_subtrees]
        ext_assignment_matrix = tf.expand_dims(assignment_matrix, axis=1)

        # [batch_size, max_nodes, meta_nodes_amount]
        target_pooling_matrix = tf.reduce_sum(ext_target_distribution * ext_assignment_matrix, axis=-1)
        target_pooling_matrix = tf.transpose(target_pooling_matrix, [0, 2, 1])

        pooling_matrix = tf.nn.softmax(node_pool_matrix, axis=-1) * tf.expand_dims(node_mask, -1)

        return node_pool_matrix, pooling_matrix, target_pooling_matrix, target_matrix, subtree_mask

    def _compute_target_distance(self, raw_pooling_matrix, target_pooling_matrix, alpha=0.):
        # pooling_matrix -> [batch_size, max_nodes, K]
        # target_pooling_matrix -> [batch_size, max_nodes, K]

        target_pooling_matrix = tf.transpose(target_pooling_matrix, [0, 2, 1])
        raw_pooling_matrix = tf.transpose(raw_pooling_matrix, [0, 2, 1])

        # Average Hamming distance
        # [batch_size, K]
        avg_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.stop_gradient(target_pooling_matrix),
                                                           logits=raw_pooling_matrix)

        target_penalty = tf.nn.relu(avg_loss - alpha)
        target_penalty = tf.reduce_sum(target_penalty, axis=-1)

        return tf.reduce_mean(target_penalty)

    def call(self, inputs, training=False, **kwargs):

        nodes = inputs['nodes']
        adjacency_matrix = inputs['adjacency_matrix']
        node_mask = inputs['node_mask']
        assignment_matrix = inputs['assignment_matrix']

        # [batch_size, max_graph_nodes, meta_nodes_amount]
        raw_pooling_matrix, pooling_matrix, target_pooling_matrix, \
        target_matrix, subtree_mask = self._retrieve_guided_pooling_matrix(
            nodes=nodes,
            assignment_matrix=assignment_matrix,
            pooling_blocks=self.pooling_blocks,
            node_mask=node_mask,
            training=training)

        next_nodes, \
        next_adjacency_matrix = self._build_next_level_info(nodes=nodes,
                                                            pooling_matrix=pooling_matrix,
                                                            adj_matrix=adjacency_matrix,
                                                            aggregation_blocks=self.aggregation_blocks,
                                                            training=training)

        target_penalty = self._compute_target_distance(raw_pooling_matrix=raw_pooling_matrix,
                                                       target_pooling_matrix=target_pooling_matrix,
                                                       alpha=self.target_threshold)

        alo_reg, amo_reg = self._compute_subtree_regularization(
            target_matrix=target_matrix,
            subtree_mask=subtree_mask,
            assignment_matrix=assignment_matrix)

        cluster_reg = self._compute_cluster_separation_constraint(pooling_matrix)

        return next_nodes, next_adjacency_matrix, pooling_matrix, {'alo_regularization': alo_reg,
                                                                   'amo_regularization': amo_reg,
                                                                   'cluster_regularization': cluster_reg,
                                                                   'target_regularization': target_penalty}


class GatedAggregation(tf.keras.layers.Layer):

    def __init__(self, aggregation_weights, gate_weights, l2_regularization=0., dropout_rate=0., **kwargs):
        super(GatedAggregation, self).__init__(**kwargs)
        self.aggregation_weights = aggregation_weights
        self.gate_weights = gate_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate

        self.gate_blocks = []
        for idx, weight in enumerate(gate_weights):
            self.gate_blocks.append(tf.keras.layers.Dense(units=weight,
                                                          activation=tf.nn.leaky_relu if idx < len(gate_weights) - 1
                                                          else None,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              l2_regularization)))
            self.gate_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.gate_blocks.append(tf.keras.layers.Dense(units=1,
                                                      activation=tf.nn.sigmoid,
                                                      kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

        # Graph encoding embedding
        self.aggregation_blocks = []
        for idx, weight in enumerate(aggregation_weights):
            self.aggregation_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                 activation=tf.nn.leaky_relu if idx < len(
                                                                     aggregation_weights) - 1 else None,
                                                                 kernel_regularizer=tf.keras.regularizers.l2(
                                                                     l2_regularization)))

            if idx < len(aggregation_weights) - 1:
                self.aggregation_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

    def call(self, inputs, training=False, **kwargs):

        nodes = inputs['nodes']

        nodes_aggr = nodes
        for aggregation_block in self.aggregation_blocks:
            nodes_aggr = aggregation_block(nodes_aggr, training=training)

        nodes_gate = nodes
        for gate_block in self.gate_blocks:
            nodes_gate = gate_block(nodes_gate, training=training)

        # [batch_size, dim]
        aggregated = tf.reduce_sum(nodes_aggr * nodes_gate, axis=1)

        return aggregated, nodes_gate
