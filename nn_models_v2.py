"""

@Author: Federico Ruggeri

@Date: 18/09/19

"""

import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

import numpy as np

from custom_tf_v2 import M_Single_GNN, M_Pairwise_GNN, M_Baseline_Sum, M_Single_Char_GNN, \
    M_Single_Dep_GNN, M_Baseline_LSTM, M_Single_Hierarchical_GNN, M_Single_Dep_Char_GNN, M_Single_Adj_GNN, \
    M_Graph_Baseline_LSTM, M_Baseline_Dense, M_Single_Hierarchical_Dep_GNN, M_Single_Dual_GNN, \
    M_Dual_Graph_Baseline_LSTM, M_Single_Tree_Pooled_Adj_GNN, \
    Pos_M_Single_Tree_Pooled_Adj_GNN, Pos_M_baseline_LSTM, Pos_M_Graph_Baseline_LSTM, Pos_M_Dual_Graph_Baseline_LSTM, \
    M_Dual_Adj_GNN, Pos_M_Single_Adj_GNN, M_Multi_Adj_GNN, M_Dual_Tree_Pooled_Adj_GNN, Pos_M_Dual_Tree_Pooled_Adj_GNN, \
    Pos_M_Dual_Adj_GNN, M_BERT, M_Pos_BERT
from utility.cross_validation_utils import build_metrics, compute_metrics
from utility.log_utils import Logger
from utility.python_utils import merge, get_top_n_values2d
from utility.tensorflow_utils_v2 import add_gradient_noise
from tqdm import tqdm
from sklearn.metrics import pairwise_distances_argmin_min
from utility.pipeline_utils import transform_to_pairwise
from sklearn import cluster
from sklearn.utils.class_weight import compute_class_weight
from transformers import BertConfig


# TODO: add checkpoint validation set evaluation during fit()
class Network(object):

    def __init__(self, embedding_dimension, name='network', additional_data=None,
                 is_multilabel=False):
        self.embedding_dimension = embedding_dimension
        self.model = None
        self.optimizer = None
        self.name = name
        self.additional_data = additional_data
        self.is_multilabel = is_multilabel

    def get_weights(self, strategy=None):
        if strategy is not None:
            with strategy.scope():
                weights = self.model.get_weights()
        else:
            weights = self.model.get_weights()

        return weights

    def set_weights(self, weights, strategy=None):
        if strategy is not None:
            with strategy.scope():
                self.model.set_weights(weights)
        else:
            self.model.set_weights(weights)

    def save(self, filepath, overwrite=True):
        if not filepath.endswith('.h5'):
            filepath += '.h5'
        self.model.save_weights(filepath)

    def load(self, filepath):
        self.model.load_weights(filepath=filepath)

    def get_attentions_weights(self, x, batch_size=32):
        return None

    def build_tensorboard_info(self):
        pass

    def compute_output_weights(self, y_train, num_classes, mode='multi-class'):

        self.num_classes = num_classes

        # Multi-class
        if mode == 'multi-class':
            if len(y_train.shape) > 1:
                if y_train.shape[1] > 1:
                    y_train = np.argmax(y_train, axis=1)
                else:
                    y_train = y_train.ravel()

            classes = np.unique(y_train)
            class_weights = compute_class_weight(class_weight='balanced', classes=classes, y=y_train)
            self.class_weights = {cls: weight for cls, weight in zip(classes, class_weights)}

        # Multi-label
        else:
            assert len(y_train.shape) == 2
            class_weights = {}
            for cls in range(y_train.shape[1]):
                class_weights[cls] = compute_class_weight(class_weight='balanced',
                                                          classes=[0, 1], y=y_train[:, cls])
            self.class_weights = class_weights

    def predict(self, x, steps, callbacks=None, suffix='test'):

        callbacks = callbacks or []

        total_preds = []

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self)
                callback.on_prediction_begin(logs={'suffix': suffix})

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_begin(batch=batch_idx, logs={'suffix': suffix})

            batch = next(x)
            if type(batch) in [tuple, list]:
                batch = batch[0]
            preds, model_additional_info = self.batch_predict(x=batch)
            preds = preds.numpy()

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model_additional_info': model_additional_info,
                                                                            'suffix': suffix})

            if type(preds) is list:
                if batch_idx == 0:
                    total_preds.extend(preds)
                else:
                    for idx, pred in enumerate(preds):
                        total_preds[idx] = np.append(total_preds[idx], pred, axis=0)
            else:
                total_preds.extend(preds)

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs={'suffix': suffix})

        return np.array(total_preds)

    # TODO: inference is not correct: investigate what's wrong (is dropout active? is the order of samples maintained?)
    def distributed_predict(self, x, steps, strategy, callbacks=None, suffix='test'):

        callbacks = callbacks or []

        total_preds = []

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self)
                callback.on_prediction_begin(logs={'suffix': suffix})

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'suffix': suffix})

            batch = next(x)
            if type(batch) in [tuple, list]:
                batch = batch[:1]
            preds, model_additional_info = self.distributed_batch_predict(inputs=list(batch), strategy=strategy)
            preds = np.concatenate([chunk.numpy() for chunk in preds.values], axis=0)
            model_additional_info = {key: np.concatenate([chunk.numpy() for chunk in value.values], axis=0)
                                     for key, value in model_additional_info.items()}

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model_additional_info': model_additional_info,
                                                                            'suffix': suffix})

            if type(preds) is list:
                if batch_idx == 0:
                    total_preds.extend(preds)
                else:
                    for idx, pred in enumerate(preds):
                        total_preds[idx] = np.append(total_preds[idx], pred, axis=0)
            else:
                total_preds.extend(preds)

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs={'suffix': suffix})

        return np.array(total_preds)

    def evaluate(self, data, steps):
        total_loss = {}

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            batch_additional_info = self._get_additional_info()
            batch_info = self.batch_evaluate(*next(data), batch_additional_info)
            batch_info = {key: item.numpy() for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

        total_loss = {key: item / steps for key, item in total_loss.items()}
        return total_loss

    # TODO: unusued until distributed_predict has been fixed
    def distributed_evaluate(self, data, steps, strategy):
        total_loss = {}

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            batch_additional_info = self._get_additional_info()
            batch_info = self.distributed_batch_evaluate(inputs=list(next(data)) + [batch_additional_info],
                                                         strategy=strategy)
            batch_info = {key: item.numpy() for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

        total_loss = {key: item / steps for key, item in total_loss.items()}
        return total_loss

    # TODO: evaluate should return predictions as well in order to avoid double forward pass
    def fit(self, train_data=None,
            epochs=1, verbose=1,
            callbacks=None, validation_data=None,
            step_checkpoint=None,
            metrics=None, additional_metrics_info=None, metrics_nicknames=None,
            train_num_batches=None, eval_num_batches=None,
            np_val_y=None):

        # self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self)
            callback.on_train_begin(logs=None)

        if verbose:
            Logger.get_logger(__name__).info('Start Training!')

            if train_num_batches is not None:
                Logger.get_logger(__name__).info('Total batches: {}'.format(train_num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(train_num_batches * step_checkpoint)
                Logger.get_logger(__name__).info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > train_num_batches:
                    step_checkpoint = int(train_num_batches * 0.1)
                    Logger.get_logger(__name__).info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        train_data = iter(train_data())

        # Training
        for epoch in range(epochs):

            if hasattr(self.model, 'stop_training') and self.model.stop_training:
                break

            for callback in callbacks:
                callback.on_epoch_begin(epoch=epoch, logs={'epochs': epochs})

            train_loss = {}
            batch_idx = 0

            # Run epoch
            pbar = tqdm(total=train_num_batches, position=0, leave=True)
            while batch_idx < train_num_batches:

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                batch_additional_info = self._get_additional_info()
                batch_info = self.batch_fit(*next(train_data), batch_additional_info)
                batch_info = {key: item.numpy() for key, item in batch_info.items()}

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                for key, item in batch_info.items():

                    # Stop if something went wrong at optimization level...
                    if np.isnan(item):
                        raise RuntimeError('NaNs encountered ({0} is nan)! Aborting...\n{1}'.format(key, batch_info))

                    if key in train_loss:
                        train_loss[key] += item
                    else:
                        train_loss[key] = item

                batch_idx += 1
                pbar.update(1)

            pbar.close()

            train_loss = {key: item / train_num_batches for key, item in train_loss.items()}
            train_loss_str = {key: float('{:.2f}'.format(value)) for key, value in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                val_info = self.evaluate(data=iter(validation_data()), steps=eval_num_batches)
                val_info_str = {key: float('{:.2f}'.format(value)) for key, value in val_info.items()}

                # TODO: extend metrics for multi-labeling
                if metrics is not None:
                    val_predictions = self.predict(iter(validation_data()),
                                                   steps=eval_num_batches,
                                                   callbacks=callbacks)
                    val_predictions = val_predictions.reshape(np_val_y.shape).astype(np_val_y.dtype)

                    all_val_metrics = compute_metrics(parsed_metrics,
                                                      true_values=np_val_y,
                                                      predicted_values=val_predictions,
                                                      additional_metrics_info=additional_metrics_info,
                                                      metrics_nicknames=metrics_nicknames,
                                                      prefix='val',
                                                      is_multilabel=self.is_multilabel)
                    val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in all_val_metrics.items()])]
                    Logger.get_logger(__name__).info('Epoch: {0} -- Train Loss: {1}'
                                                     ' -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                    train_loss_str,
                                                                                                    val_info_str,
                                                                                                    ' -- '.join(
                                                                                                        val_metrics_str_result)))
                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        Logger.get_logger(__name__).info(
                            'Epoch: {0} -- Train Loss: {1} -- Val Loss: {2}'.format(epoch + 1,
                                                                                    train_loss_str,
                                                                                    val_info_str))
            else:
                Logger.get_logger(__name__).info('Epoch: {0} -- Train Loss: {1}'.format(epoch + 1,
                                                                                        train_loss_str))

            for callback in callbacks:
                callback_args = train_loss
                if validation_data is not None:
                    callback_args = merge(callback_args, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

            # view_used_mem()

        for callback in callbacks:
            callback.on_train_end(logs=None)

    def _get_input_iterator(self, input_fn, strategy):
        """Returns distributed dataset iterator."""
        # When training with TPU pods, datasets needs to be cloned across
        # workers. Since Dataset instance cannot be cloned in eager mode, we instead
        # pass callable that returns a dataset.
        if not callable(input_fn):
            raise ValueError('`input_fn` should be a closure that returns a dataset.')
        iterator = iter(
            strategy.experimental_distribute_datasets_from_function(input_fn))
        return iterator

    def distributed_fit(self, train_data=None,
                        epochs=1, verbose=1, strategy=None,
                        callbacks=None, validation_data=None,
                        step_checkpoint=None,
                        metrics=None, additional_metrics_info=None, metrics_nicknames=None,
                        train_num_batches=None, eval_num_batches=None,
                        np_val_y=None):

        # self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self)
            callback.on_train_begin(logs=None)

        if verbose:
            Logger.get_logger(__name__).info('Start Training!')

            if train_num_batches is not None:
                Logger.get_logger(__name__).info('Total batches: {}'.format(train_num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(train_num_batches * step_checkpoint)
                Logger.get_logger(__name__).info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > train_num_batches:
                    step_checkpoint = int(train_num_batches * 0.1)
                    Logger.get_logger(__name__).info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        train_data = self._get_input_iterator(train_data, strategy)

        # Training
        for epoch in range(epochs):

            if hasattr(self.model, 'stop_training') and self.model.stop_training:
                break

            for callback in callbacks:
                callback.on_epoch_begin(epoch=epoch, logs={'epochs': epochs})

            train_loss = {}
            batch_idx = 0

            # Run epoch
            pbar = tqdm(total=train_num_batches, position=0, leave=True)
            while batch_idx < train_num_batches:

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                batch_additional_info = self._get_additional_info()
                batch_info = self.distributed_batch_fit(inputs=list(next(train_data)) + [batch_additional_info],
                                                        strategy=strategy)
                batch_info = {key: item.numpy() for key, item in batch_info.items()}

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                for key, item in batch_info.items():

                    # Stop if something went wrong at optimization level...
                    if np.isnan(item):
                        raise RuntimeError('NaNs encountered! Aborting...')

                    if key in train_loss:
                        train_loss[key] += item
                    else:
                        train_loss[key] = item

                batch_idx += 1
                pbar.update(1)

            pbar.close()

            train_loss = {key: item / train_num_batches for key, item in train_loss.items()}
            train_loss_str = {key: float('{:.3f}'.format(value)) for key, value in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                # val_info = self.distributed_evaluate(data=self._get_input_iterator(validation_data, strategy),
                #                                      strategy=strategy,
                #                                      steps=eval_num_batches)
                val_info = self.evaluate(data=iter(validation_data()), steps=eval_num_batches)
                val_info_str = {key: float('{:.3f}'.format(value)) for key, value in val_info.items()}

                if metrics is not None:
                    # val_predictions = self.distributed_predict(self._get_input_iterator(validation_data, strategy),
                    #                                            steps=eval_num_batches,
                    #                                            callbacks=callbacks,
                    #                                            strategy=strategy)
                    val_predictions = self.predict(iter(validation_data()),
                                                   steps=eval_num_batches,
                                                   callbacks=callbacks)
                    val_predictions = val_predictions.reshape(np_val_y.shape).astype(np_val_y.dtype)

                    all_val_metrics = compute_metrics(parsed_metrics,
                                                      true_values=np_val_y,
                                                      predicted_values=val_predictions,
                                                      additional_metrics_info=additional_metrics_info,
                                                      metrics_nicknames=metrics_nicknames,
                                                      prefix='val',
                                                      is_multilabel=self.is_multilabel)
                    val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in all_val_metrics.items()])]
                    Logger.get_logger(__name__).info('Epoch: {0} -- Train Loss: {1}'
                                                     ' -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                    train_loss_str,
                                                                                                    val_info_str,
                                                                                                    ' -- '.join(
                                                                                                        val_metrics_str_result)))
                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        Logger.get_logger(__name__).info(
                            'Epoch: {0} -- Train Loss: {1} -- Val Loss: {2}'.format(epoch + 1,
                                                                                    train_loss_str,
                                                                                    val_info_str))
            else:
                Logger.get_logger(__name__).info('Epoch: {0} -- Train Loss: {1}'.format(epoch + 1,
                                                                                        train_loss_str))

            for callback in callbacks:
                callback_args = train_loss
                if validation_data is not None:
                    callback_args = merge(callback_args, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback_args['strategy'] = strategy
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

        for callback in callbacks:
            callback.on_train_end(logs=None)

    def _get_additional_info(self):
        return None

    def pairwise_fit(self, train_generator, train_data, step_checkpoint, verbose=1, callbacks=None,
                     validation_data=None, metrics=None, batch_size=32, shuffle_amount=1024,
                     additional_metrics_info=None, metrics_nicknames=None, train_num_batches=None,
                     eval_num_batches=None, np_val_y=None, np_train_y=None):

        # self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self.model)
            callback.on_train_begin(logs=None)

        if verbose:
            Logger.get_logger(__name__).info('Start Training!')

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        train_generator = iter(train_generator())

        batch_idx = 0

        while True:

            # Exit condition
            if hasattr(self.model, 'stop_training') and self.model.stop_training:
                break

            for callback in callbacks:
                callback.on_batch_begin(batch=batch_idx, logs=None)

            batch_additional_info = self._get_additional_info()
            batch_info = self.batch_fit(*next(train_generator), batch_additional_info)
            batch_info = {key: item.numpy() for key, item in batch_info.items()}

            for callback in callbacks:
                callback.on_batch_end(batch=batch_idx, logs=batch_info)

            if batch_idx > 0 and batch_idx % step_checkpoint == 0:
                if validation_data is not None:
                    val_info = self.pairwise_evaluate(validation_data, steps=eval_num_batches, batch_size=batch_size,
                                                      shuffle_amount=shuffle_amount)

                    # TODO: extend metrics for multi-labeling
                    self.model.knowledge = self.prepare_inference(train_data(), train_num_batches, np_train_y,
                                                                  batch_size)
                    val_predictions = self.pairwise_predict(validation_data,
                                                            eval_num_batches,
                                                            self.model.knowledge)
                    val_predictions = val_predictions.reshape(np_val_y.shape).astype(np_val_y.dtype)

                    all_val_metrics = compute_metrics(parsed_metrics,
                                                      true_values=np_val_y,
                                                      predicted_values=val_predictions,
                                                      additional_metrics_info=additional_metrics_info,
                                                      metrics_nicknames=metrics_nicknames,
                                                      prefix='val',
                                                      is_multilabel=self.is_multilabel)
                    val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in all_val_metrics.items()])]
                    batch_info_str = ' -- '.join(
                        ['{0}: {1}'.format(key, value) for key, value in batch_info.items()])
                    Logger.get_logger(__name__).info(
                        'Batch Id: {0} -- {1} -- Val loss {2} -- Val Metrics: {3}'.format(batch_idx,
                                                                                          batch_info_str,
                                                                                          val_info,
                                                                                          ' -- '.join(
                                                                                              val_metrics_str_result)))

                    callback_args = merge(batch_info, all_val_metrics)
                    callback_args = merge(callback_args, val_info)

                    for callback in callbacks:
                        if hasattr(callback, 'on_step_checkpoint'):
                            callback.on_step_checkpoint(step_checkpoint=batch_idx % step_checkpoint,
                                                        logs=callback_args)

            batch_idx += 1

        for callback in callbacks:
            callback.on_train_end(logs=None)

    def pairwise_evaluate(self, data, steps, batch_size=32, shuffle_amount=1024):
        total_loss = {}

        data = data()
        shuffled_data = data.unbatch().shuffle(shuffle_amount)
        shuffled_data = shuffled_data.batch(batch_size)

        pairwise_data = transform_to_pairwise(data, shuffled_data, is_callable=False)
        pairwise_data = iter(pairwise_data)

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            batch_additional_info = self._get_additional_info()
            batch_info = self.batch_evaluate(*next(pairwise_data), batch_additional_info)
            batch_info = {key: item.numpy() for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

        total_loss = {key: item / steps for key, item in total_loss.items()}
        return total_loss

    def prepare_inference(self, knowledge_data, steps, np_knowledge_y, batch_size):
        raise NotImplementedError()

    def determine_prediction(self, similarity):
        raise NotImplementedError()

    def pairwise_predict(self, x, steps, knowledge=None, callbacks=None):

        callbacks = callbacks or []

        total_preds = []

        pairwise_data = iter(transform_to_pairwise(x))

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self.model)
                callback.on_prediction_begin(logs=None)

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            for callback in callbacks:
                if hasattr(callback, 'on_generator_batch_prediction_begin'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs=None)

            _, model_additional_info = self.batch_predict(x=next(pairwise_data)[0])

            for callback in callbacks:
                if hasattr(callback, 'on_generator_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': model_additional_info,
                                                                            'model_additional_info': model_additional_info})

            label_batch_preds = None
            for label_idx, label_centroid in enumerate(knowledge[0]):

                # Step 3: apply similarity
                similarity = np.matmul(model_additional_info['left_representation'].numpy(), label_centroid.transpose())

                # Step 4: determine prediction and apply threshold
                batch_preds = self.determine_prediction(similarity=similarity)

                if label_idx == 0:
                    label_batch_preds = batch_preds
                else:
                    label_batch_preds = np.concatenate((label_batch_preds[:, np.newaxis],
                                                        batch_preds[:, np.newaxis]), axis=1)

            # [ #samples, #classes - 1]
            total_preds.extend(label_batch_preds)

        total_preds = np.array(total_preds)

        if len(total_preds.shape) == 1:
            total_preds = total_preds[:, np.newaxis]

        # Add not-argumentative prediction (function of argumentative predictions)
        # [#samples, #classes]
        not_argumentative_preds = np.zeros(shape=[total_preds.shape[0], 1])
        not_argumentative_preds[np.argwhere(np.sum(total_preds, axis=1) == 0).ravel()] = 1
        total_preds = np.concatenate((not_argumentative_preds, total_preds), axis=1)

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs=None)

        return total_preds

    def _parse_predictions(self, predictions):
        return predictions

    @tf.function
    def batch_fit(self, x, y, additional_info=None):
        loss, loss_info, grads = self.train_op(x, y, additional_info=additional_info)
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))
        train_loss_info = {'train_{}'.format(key): item for key, item in loss_info.items()}
        train_loss_info['train_loss'] = loss
        return train_loss_info

    @tf.function
    def distributed_batch_fit(self, inputs, strategy):
        train_loss_info = strategy.run(self.batch_fit, args=inputs)
        train_loss_info = {key: strategy.reduce(tf.distribute.ReduceOp.SUM, item, axis=None)
                           for key, item in train_loss_info.items()}
        return train_loss_info

    @tf.function
    def batch_predict(self, x):
        predictions, model_additional_info = self.model(x, state='prediction', training=False)
        predictions = self._parse_predictions(predictions)
        return predictions, model_additional_info

    @tf.function
    def distributed_batch_predict(self, inputs, strategy):
        predictions, model_additional_info = strategy.run(self.batch_predict, args=inputs)
        # predictions = strategy.experimental_local_results(predictions)
        # predictions = tf.concat(predictions, axis=0)
        # model_additional_info = tf.nest.map_structure(strategy.experimental_local_results, model_additional_info)
        return predictions, model_additional_info

    @tf.function
    def distributed_batch_evaluate(self, inputs, strategy):
        val_loss_info = strategy.run(self.batch_evaluate, args=inputs)
        val_loss_info = {key: strategy.reduce(tf.distribute.ReduceOp.SUM, item, axis=None)
                         for key, item in val_loss_info.items()}
        return val_loss_info

    @tf.function
    def batch_evaluate(self, x, y, additional_info=None):
        loss, true_loss, loss_info = self.loss_op(x, y, training=False, additional_info=additional_info)
        val_loss_info = {'val_{}'.format(key): item for key, item in loss_info.items()}
        val_loss_info['val_loss'] = true_loss
        return val_loss_info

    @tf.function
    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, true_loss, loss_info = self.loss_op(x, y, training=True, additional_info=additional_info)
        grads = tape.gradient(loss, self.model.trainable_variables)
        return true_loss, loss_info, grads

    def build_model(self, text_info):
        raise NotImplementedError()

    @tf.function
    def loss_op(self, x, targets, training=False, additional_info=None):
        raise NotImplementedError()


class Single_GNN(Network):

    def __init__(self, optimizer_args, timesteps,
                 node_mlp_weights, message_weights, gate_mlp_weights, aggregation_mlp_weights,
                 representation_mlp_weights, input_dropout_rate=.4, edge_mlp_weights=None,
                 clip_gradient=False, max_grad_norm=40., add_gradient_noise=True, rnn_encoding=False,
                 l2_regularization=None, dropout_rate=.4, weight_predictions=True,
                 repeated_shared=True, **kwargs):
        super(Single_GNN, self).__init__(**kwargs)
        self.optimizer_args = optimizer_args
        self.node_mlp_weights = node_mlp_weights
        self.edge_mlp_weights = edge_mlp_weights
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.max_grad_norm = max_grad_norm
        self.l2_regularization = l2_regularization
        self.timesteps = timesteps
        self.gate_mlp_weights = gate_mlp_weights
        self.representation_mlp_weights = representation_mlp_weights
        self.aggregation_mlp_weights = aggregation_mlp_weights
        self.message_weights = message_weights
        self.dropout_rate = dropout_rate
        self.input_dropout_rate = input_dropout_rate
        self.weight_predictions = weight_predictions
        self.gating_prior_coeff = 0.1
        self.rnn_encoding = rnn_encoding
        self.repeated_shared = repeated_shared

    def build_model(self, text_info):

        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_GNN(embedding_dimension=self.embedding_dimension,
                                  vocab_size=self.vocab_size,
                                  graph_max_nodes=self.graph_max_nodes,
                                  graph_max_depth=self.graph_max_depth,
                                  timesteps=self.timesteps,
                                  node_mlp_weights=self.node_mlp_weights,
                                  edge_mlp_weights=self.edge_mlp_weights,
                                  message_weights=self.message_weights,
                                  gate_mlp_weights=self.gate_mlp_weights,
                                  aggregation_mlp_weights=self.aggregation_mlp_weights,
                                  representation_mlp_weights=self.representation_mlp_weights,
                                  l2_regularization=self.l2_regularization,
                                  dropout_rate=self.dropout_rate,
                                  embedding_matrix=text_info['embedding_matrix'],
                                  input_dropout_rate=self.input_dropout_rate,
                                  num_classes=text_info['num_labels'],
                                  rnn_encoding=self.rnn_encoding,
                                  repeated_shared=self.repeated_shared)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _get_additional_info(self):
        return {
            'gating_prior_coeff': tf.convert_to_tensor(self.gating_prior_coeff)
        }

    def loss_op(self, x, targets, training=False, additional_info=None):
        # [batch_size, #labels]
        logits, model_additional_info = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        if self.is_multilabel:
            # [batch_size, #labels]
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            if self.weight_predictions:
                weights = tf.zeros_like(cross_entropy)
                for cls, cls_weights in self.class_weights.items():
                    cls_values = tf.where(targets[:, cls] == 1., cls_weights[1], cls_weights[0])
                    cls_values = tf.expand_dims(tf.cast(cls_values, cross_entropy.dtype), -1)
                    cls_values_mask = tf.expand_dims(tf.one_hot(cls, depth=targets.shape[1]), 0)
                    weights += cls_values * cls_values_mask

                cross_entropy *= weights

            cross_entropy = tf.reduce_sum(cross_entropy, axis=1)
            cross_entropy = tf.reduce_mean(cross_entropy)
        else:

            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights
            cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Gating prior
        # gate_values = tf.squeeze(model_additional_info['gate'])
        # gating_prior = -tf.reduce_sum(gate_values, axis=1)
        # gating_prior = tf.reduce_mean(gating_prior) * additional_info['gating_prior_coeff']
        #
        # total_loss += gating_prior
        # loss_info['gating_prior'] = gating_prior

        return total_loss, total_loss, loss_info

    def train_op(self, x, y, additional_info=None):
        with tf.GradientTape() as tape:
            loss, true_loss, loss_info = self.loss_op(x, y, training=True, additional_info=additional_info)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return true_loss, loss_info, grads

    def _parse_predictions(self, predictions):
        if self.is_multilabel:
            predictions = tf.nn.sigmoid(predictions)
            predictions = tf.round(predictions)
        else:
            depth = predictions.shape[-1]
            predictions = tf.math.argmax(predictions, axis=1)
            predictions = tf.one_hot(predictions, depth=depth)

        return predictions


class Single_Dual_GNN(Single_GNN):

    def __init__(self, self_attention_weights, **kwargs):
        super(Single_Dual_GNN, self).__init__(**kwargs)
        self.self_attention_weights = self_attention_weights

    def build_model(self, text_info):

        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.max_graph_subtree_size = text_info['max_graph_subtree_size']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_Dual_GNN(embedding_dimension=self.embedding_dimension,
                                       vocab_size=self.vocab_size,
                                       graph_max_nodes=self.graph_max_nodes,
                                       graph_max_depth=self.graph_max_depth,
                                       max_graph_subtree_size=self.max_graph_subtree_size,
                                       timesteps=self.timesteps,
                                       node_mlp_weights=self.node_mlp_weights,
                                       edge_mlp_weights=self.edge_mlp_weights,
                                       message_weights=self.message_weights,
                                       gate_mlp_weights=self.gate_mlp_weights,
                                       aggregation_mlp_weights=self.aggregation_mlp_weights,
                                       representation_mlp_weights=self.representation_mlp_weights,
                                       l2_regularization=self.l2_regularization,
                                       dropout_rate=self.dropout_rate,
                                       embedding_matrix=text_info['embedding_matrix'],
                                       input_dropout_rate=self.input_dropout_rate,
                                       num_classes=text_info['num_labels'],
                                       rnn_encoding=self.rnn_encoding,
                                       repeated_shared=self.repeated_shared,
                                       self_attention_weights=self.self_attention_weights)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_predictions(self, predictions):
        if self.is_multilabel:
            predictions = tf.nn.sigmoid(predictions)
            predictions = tf.round(predictions)
        else:
            depth = predictions.shape[-1]
            predictions = tf.math.argmax(predictions, axis=1)
            predictions = tf.one_hot(predictions, depth=depth)

        return predictions


class Single_Adj_GNN(Network):

    def __init__(self, optimizer_args, gcn_info,
                 representation_mlp_weights, input_dropout_rate=.4,
                 clip_gradient=False, max_grad_norm=40., add_gradient_noise=True, rnn_encoding=False,
                 l2_regularization=None, dropout_rate=.4, weight_predictions=True,
                 use_cluster_regularization=False, use_link_regularization=False,
                 use_sigmoid=False, **kwargs):
        super(Single_Adj_GNN, self).__init__(**kwargs)
        self.optimizer_args = optimizer_args
        self.gcn_info = gcn_info
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.max_grad_norm = max_grad_norm
        self.l2_regularization = l2_regularization
        self.representation_mlp_weights = representation_mlp_weights
        self.dropout_rate = dropout_rate
        self.input_dropout_rate = input_dropout_rate
        self.weight_predictions = weight_predictions
        self.gating_prior_coeff = 0.1
        self.rnn_encoding = rnn_encoding
        self.use_cluster_regularization = use_cluster_regularization
        self.use_link_regularization = use_link_regularization
        self.use_sigmoid = use_sigmoid

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Single_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                      vocab_size=self.vocab_size,
                                      graph_max_nodes=self.graph_max_nodes,
                                      graph_max_depth=None,
                                      gcn_info=self.gcn_info,
                                      representation_mlp_weights=self.representation_mlp_weights,
                                      l2_regularization=self.l2_regularization,
                                      dropout_rate=self.dropout_rate,
                                      embedding_matrix=text_info['embedding_matrix'],
                                      input_dropout_rate=self.input_dropout_rate,
                                      num_classes=text_info['num_labels'],
                                      rnn_encoding=self.rnn_encoding,
                                      use_cluster_regularization=self.use_cluster_regularization,
                                      use_link_regularization=self.use_link_regularization,
                                      use_sigmoid=self.use_sigmoid)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, additional_info=None):
        # [batch_size, #labels]
        logits, model_additional_info = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        if self.is_multilabel:
            # [batch_size, #labels]
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            if self.weight_predictions:
                weights = tf.zeros_like(cross_entropy)
                for cls, cls_weights in self.class_weights.items():
                    cls_values = tf.where(targets[:, cls] == 1., cls_weights[1], cls_weights[0])
                    cls_values = tf.expand_dims(tf.cast(cls_values, cross_entropy.dtype), -1)
                    cls_values_mask = tf.expand_dims(tf.one_hot(cls, depth=targets.shape[1]), 0)
                    weights += cls_values * cls_values_mask

                cross_entropy *= weights

            cross_entropy = tf.reduce_sum(cross_entropy, axis=1)
            cross_entropy = tf.reduce_mean(cross_entropy)
        else:

            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights
            cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Pooling losses

        # Link regularization
        if self.use_link_regularization:
            if len(self.model.pooling_blocks) > 1:
                link_losses = tf.stack([block.link_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
                link_losses = tf.nn.compute_average_loss(link_losses,
                                                         global_batch_size=self.batch_size)
                total_loss += link_losses
                loss_info['link_regularization'] = link_losses

        # Cluster regularization
        if self.use_cluster_regularization:
            if len(self.model.pooling_blocks) > 1:
                cluster_losses = tf.stack([block.cluster_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
                cluster_losses = tf.nn.compute_average_loss(cluster_losses,
                                                            global_batch_size=self.batch_size)
                total_loss += cluster_losses
                loss_info['cluster_regularization'] = cluster_losses

        return total_loss, total_loss, loss_info

    def train_op(self, x, y, additional_info=None):
        with tf.GradientTape() as tape:
            loss, true_loss, loss_info = self.loss_op(x, y, training=True, additional_info=additional_info)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return true_loss, loss_info, grads

    def _parse_predictions(self, predictions):
        if self.is_multilabel:
            predictions = tf.nn.sigmoid(predictions)
            predictions = tf.round(predictions)
        else:
            depth = predictions.shape[-1]
            predictions = tf.math.argmax(predictions, axis=1)
            predictions = tf.one_hot(predictions, depth=depth)

        return predictions


class Pos_Single_Adj_GNN(Single_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Single_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_Single_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                          vocab_size=self.vocab_size,
                                          graph_max_nodes=self.graph_max_nodes,
                                          graph_max_depth=None,
                                          gcn_info=self.gcn_info,
                                          representation_mlp_weights=self.representation_mlp_weights,
                                          l2_regularization=self.l2_regularization,
                                          dropout_rate=self.dropout_rate,
                                          embedding_matrix=text_info['embedding_matrix'],
                                          input_dropout_rate=self.input_dropout_rate,
                                          num_classes=text_info['num_labels'],
                                          rnn_encoding=self.rnn_encoding,
                                          use_cluster_regularization=self.use_cluster_regularization,
                                          use_link_regularization=self.use_link_regularization,
                                          use_position_feature=self.use_position_feature,
                                          use_sigmoid=self.use_sigmoid)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Dual_Adj_GNN(Single_Adj_GNN):

    def __init__(self, rnn_weights, self_attention_weights, **kwargs):
        super(Dual_Adj_GNN, self).__init__(**kwargs)
        self.rnn_weights = rnn_weights
        self.self_attention_weights = self_attention_weights

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Dual_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                    vocab_size=self.vocab_size,
                                    graph_max_nodes=self.graph_max_nodes,
                                    graph_max_depth=None,
                                    gcn_info=self.gcn_info,
                                    representation_mlp_weights=self.representation_mlp_weights,
                                    l2_regularization=self.l2_regularization,
                                    dropout_rate=self.dropout_rate,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    input_dropout_rate=self.input_dropout_rate,
                                    num_classes=text_info['num_labels'],
                                    rnn_encoding=self.rnn_encoding,
                                    use_cluster_regularization=self.use_cluster_regularization,
                                    use_link_regularization=self.use_link_regularization,
                                    use_sigmoid=self.use_sigmoid,
                                    rnn_weights=self.rnn_weights,
                                    self_attention_weights=self.self_attention_weights)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Pos_Dual_Adj_GNN(Dual_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Dual_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_Dual_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                        vocab_size=self.vocab_size,
                                        graph_max_nodes=self.graph_max_nodes,
                                        graph_max_depth=None,
                                        gcn_info=self.gcn_info,
                                        representation_mlp_weights=self.representation_mlp_weights,
                                        l2_regularization=self.l2_regularization,
                                        dropout_rate=self.dropout_rate,
                                        embedding_matrix=text_info['embedding_matrix'],
                                        input_dropout_rate=self.input_dropout_rate,
                                        num_classes=text_info['num_labels'],
                                        rnn_encoding=self.rnn_encoding,
                                        use_cluster_regularization=self.use_cluster_regularization,
                                        use_link_regularization=self.use_link_regularization,
                                        use_sigmoid=self.use_sigmoid,
                                        rnn_weights=self.rnn_weights,
                                        self_attention_weights=self.self_attention_weights,
                                        use_position_feature=self.use_position_feature)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Single_Tree_Pooled_Adj_GNN(Single_Adj_GNN):

    def __init__(self, contiguous_coefficient=1.0, kernel_coefficient=1.0,
                 minimal_size_coefficient=1.0, tree_intensity_coefficient=1.0,
                 connectivity_threshold=0.5, overlapping_threshold=0.5, kernel='ptk',
                 coefficients_learning='fixed', lambda_optimizer_args=None, lambda_values=None, **kwargs):
        super(Single_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)

        self.contiguous_coefficient = contiguous_coefficient
        self.kernel_coefficient = kernel_coefficient
        self.minimal_size_coefficient = minimal_size_coefficient
        self.tree_intensity_coefficient = tree_intensity_coefficient

        self.connectivity_threshold = connectivity_threshold
        self.overlapping_threshold = overlapping_threshold

        self.kernel = kernel

        assert kernel.lower() in ['ptk', 'stk', 'sstk']
        assert coefficients_learning in ['fixed', 'priority', 'lagrangian']

        if coefficients_learning in ['priority']:
            raise NotImplementedError("Currently only lagrangian and fixed are supported!")

        self.coefficients_learning = coefficients_learning

        if self.coefficients_learning == 'lagrangian':
            assert lambda_optimizer_args is not None
            assert type(lambda_optimizer_args) is dict

        self.lambda_optimizer_args = lambda_optimizer_args
        self.lambda_gradient_coefficient = 0.
        self.lambda_values = lambda_values

    def _get_additional_info(self):
        return {
            'contiguous_coefficient': tf.convert_to_tensor(self.contiguous_coefficient, dtype=tf.float32),
            'kernel_coefficient': tf.convert_to_tensor(self.kernel_coefficient, dtype=tf.float32),
            'minimal_size_coefficient': tf.convert_to_tensor(self.minimal_size_coefficient, dtype=tf.float32),
            'tree_intensity_coefficient': tf.convert_to_tensor(self.tree_intensity_coefficient, dtype=tf.float32),
            'lambda_gradient_coefficient': tf.convert_to_tensor(self.lambda_gradient_coefficient, dtype=tf.float32)
        }

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Single_Tree_Pooled_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                                  vocab_size=self.vocab_size,
                                                  graph_max_nodes=self.graph_max_nodes,
                                                  graph_max_depth=None,
                                                  representation_mlp_weights=self.representation_mlp_weights,
                                                  l2_regularization=self.l2_regularization,
                                                  dropout_rate=self.dropout_rate,
                                                  embedding_matrix=text_info['embedding_matrix'],
                                                  input_dropout_rate=self.input_dropout_rate,
                                                  num_classes=text_info['num_labels'],
                                                  rnn_encoding=self.rnn_encoding,
                                                  use_cluster_regularization=self.use_cluster_regularization,
                                                  use_link_regularization=self.use_link_regularization,
                                                  connectivity_threshold=self.connectivity_threshold,
                                                  use_sigmoid=self.use_sigmoid,
                                                  kernel=self.kernel,
                                                  overlapping_threshold=self.overlapping_threshold,
                                                  coefficients_learning=self.coefficients_learning,
                                                  batch_size=self.batch_size,
                                                  gcn_info=self.gcn_info,
                                                  lambda_values=self.lambda_values)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

        if self.coefficients_learning == 'lagrangian':
            self.lambda_optimizer = tf.keras.optimizers.Adam(**self.lambda_optimizer_args)

    def _standard_tree_regularizations(self, total_loss, loss_info,
                                       additional_info, model_additional_info):

        # Tree regularizations

        true_total_loss = total_loss

        contiguous_coefficient = additional_info['contiguous_coefficient']
        kernel_coefficient = additional_info['kernel_coefficient']

        sequence_regularization = tf.stack([block.sequence_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
        sequence_regularization = tf.reduce_mean(sequence_regularization)

        true_total_loss += sequence_regularization
        total_loss += sequence_regularization * contiguous_coefficient
        loss_info['sequence_regularization'] = sequence_regularization

        kernel_regularization = tf.stack([block.kernel_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
        kernel_regularization = tf.reduce_mean(kernel_regularization)

        total_loss += kernel_regularization * kernel_coefficient
        true_total_loss += kernel_regularization
        loss_info['kernel_regularization'] = kernel_regularization

        # Cluster regularizations

        minimal_size_coefficient = additional_info['minimal_size_coefficient']
        tree_intensity_coefficient = additional_info['tree_intensity_coefficient']

        mean_regularization = tf.stack([block.mean_size_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
        mean_regularization = tf.reduce_mean(mean_regularization)

        total_loss += mean_regularization * minimal_size_coefficient
        true_total_loss += mean_regularization
        loss_info['mean_size_regularization'] = mean_regularization

        tree_intensity_regularization = tf.stack([block.tree_intensity_reg for block in self.model.pooling_blocks[:-1]],
                                                 axis=-1)
        tree_intensity_regularization = tf.reduce_mean(tree_intensity_regularization)

        total_loss += tree_intensity_regularization * tree_intensity_coefficient
        true_total_loss += tree_intensity_regularization
        loss_info['tree_intensity_regularization'] = tree_intensity_regularization

        return total_loss, true_total_loss, loss_info

    def _prioritized_tree_regularizations(self, total_loss, loss_info, additional_info, model_additional_info):

        # Apply priority
        # A: Contiguous [0, 1]        -> multiplication factor: 1 - A
        # B: Size and overlap   [0, 1]        -> multiplication factor: 1 - A
        # C: Intensity          [0, 1]   -> multiplication factor: 1 - C
        # D: Kernel specific  [0, 1]

        # [batch_size, K]
        cluster_contiguous_reg = model_additional_info['cluster_contiguous_sequence_regularization']
        cluster_size_and_overlap_reg = model_additional_info['mean_size_regularization']
        cluster_intensity_reg = model_additional_info['cluster_tree_intensity_regularization']
        cluster_kernel_reg = model_additional_info['cluster_kernel_regularization']

        # [batch_size, K]
        factor_contiguous_reg = tf.stop_gradient(1 - cluster_contiguous_reg)
        factor_size_and_overlap_reg = tf.stop_gradient(1. - cluster_size_and_overlap_reg)
        factor_intensity_reg = tf.stop_gradient(1. - cluster_intensity_reg)

        # []
        contiguous_reg = tf.nn.compute_average_loss(cluster_contiguous_reg, global_batch_size=self.batch_size)
        size_and_overlap_reg = tf.nn.compute_average_loss(cluster_size_and_overlap_reg * factor_contiguous_reg,
                                                          global_batch_size=self.batch_size)
        intensity_reg = tf.nn.compute_average_loss(
            cluster_intensity_reg * factor_contiguous_reg * factor_size_and_overlap_reg,
            global_batch_size=self.batch_size)
        kernel_reg = tf.nn.compute_average_loss(
            cluster_kernel_reg * factor_contiguous_reg * factor_size_and_overlap_reg * factor_intensity_reg,
            global_batch_size=self.batch_size)

        # Tree regularizations

        contiguous_coefficient = additional_info['contiguous_coefficient']
        kernel_coefficient = additional_info['kernel_coefficient']

        true_total_loss = total_loss

        true_total_loss += contiguous_reg
        total_loss += contiguous_reg * contiguous_coefficient
        loss_info['sequence_regularization'] = contiguous_reg

        total_loss += kernel_reg * kernel_coefficient
        true_total_loss += kernel_reg
        loss_info['kernel_regularization'] = kernel_reg

        # Cluster regularizations

        minimal_size_coefficient = additional_info['minimal_size_coefficient']
        tree_intensity_coefficient = additional_info['tree_intensity_coefficient']

        total_loss += size_and_overlap_reg * minimal_size_coefficient
        true_total_loss += size_and_overlap_reg
        loss_info['mean_size_regularization'] = size_and_overlap_reg

        total_loss += intensity_reg * tree_intensity_coefficient
        true_total_loss += intensity_reg
        loss_info['tree_intensity_regularization'] = intensity_reg

        return total_loss, true_total_loss, loss_info

    def _lagrangian_tree_regularizations(self, total_loss, loss_info,
                                         additional_info, model_additional_info):

        # Lagrangian multipliers
        # [contiguous, kernel_specific, minimal_size]
        multipliers = model_additional_info['lagrangian_multipliers']

        contiguous_coefficient = multipliers[0]
        kernel_coefficient = multipliers[1]
        minimal_size_coefficient = multipliers[2]
        tree_intensity_coefficient = multipliers[3]

        # Tree regularizations

        true_total_loss = total_loss

        sequence_regularization = tf.stack([block.sequence_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
        sequence_regularization = tf.reduce_mean(sequence_regularization)

        total_loss += sequence_regularization * contiguous_coefficient
        true_total_loss += sequence_regularization
        loss_info['sequence_regularization'] = sequence_regularization

        kernel_regularization = tf.stack([block.kernel_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
        kernel_regularization = tf.reduce_mean(kernel_regularization)

        total_loss += kernel_regularization * kernel_coefficient
        true_total_loss += kernel_regularization
        loss_info['kernel_regularization'] = kernel_regularization

        # Cluster regularizations

        mean_regularization = tf.stack([block.mean_size_reg for block in self.model.pooling_blocks[:-1]], axis=-1)
        mean_regularization = tf.reduce_mean(mean_regularization)

        total_loss += mean_regularization * minimal_size_coefficient
        true_total_loss += mean_regularization
        loss_info['mean_size_regularization'] = mean_regularization

        tree_intensity_regularization = tf.stack([block.tree_intensity_reg for block in self.model.pooling_blocks[:-1]],
                                                 axis=-1)
        tree_intensity_regularization = tf.reduce_mean(tree_intensity_regularization)

        total_loss += tree_intensity_regularization * tree_intensity_coefficient
        true_total_loss += tree_intensity_regularization
        loss_info['tree_intensity_regularization'] = tree_intensity_regularization

        return total_loss, true_total_loss, loss_info

    @tf.function
    def loss_op(self, x, targets, training=False, additional_info=None):
        # [batch_size, #labels]
        logits, model_additional_info = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        if self.is_multilabel:
            # [batch_size, #labels]
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            if self.weight_predictions:
                weights = tf.zeros_like(cross_entropy)
                for cls, cls_weights in self.class_weights.items():
                    cls_values = tf.where(targets[:, cls] == 1., cls_weights[1], cls_weights[0])
                    cls_values = tf.expand_dims(tf.cast(cls_values, cross_entropy.dtype), -1)
                    cls_values_mask = tf.expand_dims(tf.one_hot(cls, depth=targets.shape[1]), 0)
                    weights += cls_values * cls_values_mask

                cross_entropy *= weights

            cross_entropy = tf.reduce_sum(cross_entropy, axis=1)
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)

        else:

            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)

        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses) * 0.
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Pooling losses

        # Link regularization
        if self.use_link_regularization:
            link_losses = tf.stack([block.link_reg for block in self.model.pooling_blocks], axis=-1)
            link_losses = tf.nn.compute_average_loss(link_losses,
                                                     global_batch_size=self.batch_size)
            total_loss += link_losses
            loss_info['link_regularization'] = tf.nn.compute_average_loss(link_losses,
                                                                          global_batch_size=self.batch_size)

        # Cluster regularization
        if self.use_cluster_regularization:
            cluster_losses = tf.stack([block.cluster_reg for block in self.model.pooling_blocks], axis=-1)
            cluster_losses = tf.nn.compute_average_loss(cluster_losses,
                                                        global_batch_size=self.batch_size)
            total_loss += cluster_losses
            loss_info['cluster_regularization'] = tf.nn.compute_average_loss(cluster_losses,
                                                                             global_batch_size=self.batch_size)

        # Tree regularizations

        if self.coefficients_learning == 'fixed':
            total_loss, true_total_loss, loss_info = self._standard_tree_regularizations(total_loss=total_loss,
                                                                                         loss_info=loss_info,
                                                                                         additional_info=additional_info,
                                                                                         model_additional_info=model_additional_info)
        elif self.coefficients_learning == 'priority':
            total_loss, true_total_loss, loss_info = self._prioritized_tree_regularizations(total_loss=total_loss,
                                                                                            loss_info=loss_info,
                                                                                            additional_info=additional_info,
                                                                                            model_additional_info=model_additional_info)
        else:
            total_loss, true_total_loss, loss_info = self._lagrangian_tree_regularizations(total_loss=total_loss,
                                                                                           loss_info=loss_info,
                                                                                           additional_info=additional_info,
                                                                                           model_additional_info=model_additional_info)

        return total_loss, true_total_loss, loss_info

    def train_op(self, x, y, additional_info=None):
        if self.coefficients_learning != 'lagrangian':
            return super(Single_Tree_Pooled_Adj_GNN, self).train_op(x, y, additional_info)
        else:
            with tf.GradientTape(persistent=True) as tape:
                loss, true_loss, loss_info = self.loss_op(x, y, training=True, additional_info=additional_info)
                lagrangian_loss = -loss

            # Model optimizer
            model_trainable_ops = self.model.trainable_variables[:-1]  # ignore lagrangian multipliers
            grads = tape.gradient(loss, model_trainable_ops)
            if self.clip_gradient:
                grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                                  for g, v in zip(grads, model_trainable_ops)]
                grads = [item[0] for item in grads_and_vars]
            if self.add_gradient_noise:
                grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, model_trainable_ops)]
                grads = [item[0] for item in grads_and_vars]

            # Lagrangian optimizer (minimize negative loss!)
            lambda_gradient_coefficient = additional_info['lambda_gradient_coefficient']
            lagrangian_grads = tape.gradient(lagrangian_loss, [self.model.multipliers])
            lagrangian_grads = [item * lambda_gradient_coefficient for item in lagrangian_grads]

            return true_loss, loss_info, grads, lagrangian_grads

    @tf.function
    def batch_fit(self, x, y, additional_info=None):
        if self.coefficients_learning != 'lagrangian':
            return super(Single_Tree_Pooled_Adj_GNN, self).batch_fit(x, y, additional_info)
        else:
            loss, loss_info, grads, lagrangian_grads = self.train_op(x, y, additional_info=additional_info)

            # Model optimizer
            model_trainable_ops = self.model.trainable_variables[:-1]  # ignore lagrangian multipliers
            self.optimizer.apply_gradients(zip(grads, model_trainable_ops))

            # Lagrangian optimizer
            self.lambda_optimizer.apply_gradients(zip(lagrangian_grads, [self.model.multipliers]))

            train_loss_info = {'train_{}'.format(key): item for key, item in loss_info.items()}
            train_loss_info['train_loss'] = loss
            return train_loss_info


class Pos_Single_Tree_Pooled_Adj_GNN(Single_Tree_Pooled_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Single_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_Single_Tree_Pooled_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                                      vocab_size=self.vocab_size,
                                                      graph_max_nodes=self.graph_max_nodes,
                                                      graph_max_depth=None,
                                                      representation_mlp_weights=self.representation_mlp_weights,
                                                      l2_regularization=self.l2_regularization,
                                                      dropout_rate=self.dropout_rate,
                                                      embedding_matrix=text_info['embedding_matrix'],
                                                      input_dropout_rate=self.input_dropout_rate,
                                                      num_classes=text_info['num_labels'],
                                                      rnn_encoding=self.rnn_encoding,
                                                      use_cluster_regularization=self.use_cluster_regularization,
                                                      use_link_regularization=self.use_link_regularization,
                                                      connectivity_threshold=self.connectivity_threshold,
                                                      use_sigmoid=self.use_sigmoid,
                                                      kernel=self.kernel,
                                                      overlapping_threshold=self.overlapping_threshold,
                                                      coefficients_learning=self.coefficients_learning,
                                                      batch_size=self.batch_size,
                                                      gcn_info=self.gcn_info,
                                                      use_position_feature=self.use_position_feature)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

        if self.coefficients_learning == 'lagrangian':
            self.lambda_optimizer = tf.keras.optimizers.Adam(**self.lambda_optimizer_args)


class Dual_Tree_Pooled_Adj_GNN(Single_Tree_Pooled_Adj_GNN):

    def __init__(self, rnn_weights, self_attention_weights, **kwargs):
        super(Dual_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)
        self.rnn_weights = rnn_weights
        self.self_attention_weights = self_attention_weights

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Dual_Tree_Pooled_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                                vocab_size=self.vocab_size,
                                                graph_max_nodes=self.graph_max_nodes,
                                                graph_max_depth=None,
                                                representation_mlp_weights=self.representation_mlp_weights,
                                                l2_regularization=self.l2_regularization,
                                                dropout_rate=self.dropout_rate,
                                                embedding_matrix=text_info['embedding_matrix'],
                                                input_dropout_rate=self.input_dropout_rate,
                                                num_classes=text_info['num_labels'],
                                                rnn_encoding=self.rnn_encoding,
                                                use_cluster_regularization=self.use_cluster_regularization,
                                                use_link_regularization=self.use_link_regularization,
                                                connectivity_threshold=self.connectivity_threshold,
                                                use_sigmoid=self.use_sigmoid,
                                                kernel=self.kernel,
                                                overlapping_threshold=self.overlapping_threshold,
                                                coefficients_learning=self.coefficients_learning,
                                                batch_size=self.batch_size,
                                                gcn_info=self.gcn_info,
                                                rnn_weights=self.rnn_weights,
                                                self_attention_weights=self.self_attention_weights)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

        if self.coefficients_learning == 'lagrangian':
            self.lambda_optimizer = tf.keras.optimizers.Adam(**self.lambda_optimizer_args)


class Pos_Dual_Tree_Pooled_Adj_GNN(Dual_Tree_Pooled_Adj_GNN):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Dual_Tree_Pooled_Adj_GNN, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_Dual_Tree_Pooled_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                                    vocab_size=self.vocab_size,
                                                    graph_max_nodes=self.graph_max_nodes,
                                                    graph_max_depth=None,
                                                    representation_mlp_weights=self.representation_mlp_weights,
                                                    l2_regularization=self.l2_regularization,
                                                    dropout_rate=self.dropout_rate,
                                                    embedding_matrix=text_info['embedding_matrix'],
                                                    input_dropout_rate=self.input_dropout_rate,
                                                    num_classes=text_info['num_labels'],
                                                    rnn_encoding=self.rnn_encoding,
                                                    use_cluster_regularization=self.use_cluster_regularization,
                                                    use_link_regularization=self.use_link_regularization,
                                                    connectivity_threshold=self.connectivity_threshold,
                                                    use_sigmoid=self.use_sigmoid,
                                                    kernel=self.kernel,
                                                    overlapping_threshold=self.overlapping_threshold,
                                                    coefficients_learning=self.coefficients_learning,
                                                    batch_size=self.batch_size,
                                                    gcn_info=self.gcn_info,
                                                    rnn_weights=self.rnn_weights,
                                                    self_attention_weights=self.self_attention_weights,
                                                    use_position_feature=self.use_position_feature)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

        if self.coefficients_learning == 'lagrangian':
            self.lambda_optimizer = tf.keras.optimizers.Adam(**self.lambda_optimizer_args)


class Single_Hierarchical_GNN(Single_GNN):

    def __init__(self, subtree_aggregation_mlp_weights, subtree_gate_mlp_weights,
                 subtree_message_weights, subtree_node_mlp_weights,
                 **kwargs):
        super(Single_Hierarchical_GNN, self).__init__(**kwargs)
        self.subtree_aggregation_mlp_weights = subtree_aggregation_mlp_weights
        self.subtree_gate_mlp_weights = subtree_gate_mlp_weights
        self.subtree_message_weights = subtree_message_weights
        self.subtree_node_mlp_weights = subtree_node_mlp_weights

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.max_graph_subtrees = text_info['max_graph_subtrees']
        self.max_graph_subtree_size = text_info['max_graph_subtree_size']
        self.max_graph_subtree_edges = text_info['max_graph_subtree_edges']
        self.max_graph_subtree_depth = text_info['max_graph_subtree_depth']

        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_Hierarchical_GNN(embedding_dimension=self.embedding_dimension,
                                               vocab_size=self.vocab_size,
                                               graph_max_nodes=self.graph_max_nodes,
                                               graph_max_depth=self.graph_max_depth,
                                               max_subtrees=self.max_graph_subtrees,
                                               max_subtree_size=self.max_graph_subtree_size,
                                               max_subtree_depth=self.max_graph_subtree_depth,
                                               subtree_aggregation_mlp_weights=self.subtree_aggregation_mlp_weights,
                                               subtree_gate_mlp_weights=self.subtree_gate_mlp_weights,
                                               subtree_message_weights=self.subtree_message_weights,
                                               subtree_node_mlp_weights=self.subtree_node_mlp_weights,
                                               timesteps=self.timesteps,
                                               node_mlp_weights=self.node_mlp_weights,
                                               edge_mlp_weights=self.edge_mlp_weights,
                                               message_weights=self.message_weights,
                                               gate_mlp_weights=self.gate_mlp_weights,
                                               aggregation_mlp_weights=self.aggregation_mlp_weights,
                                               representation_mlp_weights=self.representation_mlp_weights,
                                               l2_regularization=self.l2_regularization,
                                               dropout_rate=self.dropout_rate,
                                               embedding_matrix=text_info['embedding_matrix'],
                                               input_dropout_rate=self.input_dropout_rate,
                                               num_classes=text_info['num_labels'],
                                               rnn_encoding=self.rnn_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Single_Hierarchical_Dep_GNN(Single_Hierarchical_GNN):

    def __init__(self, subtree_edge_mlp_weights, **kwargs):
        super(Single_Hierarchical_Dep_GNN, self).__init__(**kwargs)
        self.subtree_edge_mlp_weights = subtree_edge_mlp_weights

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.max_graph_subtrees = text_info['max_graph_subtrees']
        self.max_graph_subtree_size = text_info['max_graph_subtree_size']
        self.max_graph_subtree_edges = text_info['max_graph_subtree_edges']
        self.max_graph_subtree_depth = text_info['max_graph_subtree_depth']

        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_Hierarchical_Dep_GNN(embedding_dimension=self.embedding_dimension,
                                                   vocab_size=self.vocab_size,
                                                   graph_max_nodes=self.graph_max_nodes,
                                                   max_subtrees=self.max_graph_subtrees,
                                                   max_subtree_size=self.max_graph_subtree_size,
                                                   max_subtree_depth=self.max_graph_subtree_depth,
                                                   subtree_aggregation_mlp_weights=self.subtree_aggregation_mlp_weights,
                                                   subtree_gate_mlp_weights=self.subtree_gate_mlp_weights,
                                                   subtree_message_weights=self.subtree_message_weights,
                                                   subtree_node_mlp_weights=self.subtree_node_mlp_weights,
                                                   timesteps=self.timesteps,
                                                   node_mlp_weights=self.node_mlp_weights,
                                                   edge_mlp_weights=self.edge_mlp_weights,
                                                   message_weights=self.message_weights,
                                                   gate_mlp_weights=self.gate_mlp_weights,
                                                   aggregation_mlp_weights=self.aggregation_mlp_weights,
                                                   representation_mlp_weights=self.representation_mlp_weights,
                                                   subtree_edge_mlp_weights=self.subtree_edge_mlp_weights,
                                                   l2_regularization=self.l2_regularization,
                                                   dropout_rate=self.dropout_rate,
                                                   embedding_matrix=text_info['embedding_matrix'],
                                                   input_dropout_rate=self.input_dropout_rate,
                                                   num_classes=text_info['num_labels'],
                                                   rnn_encoding=self.rnn_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Single_Dep_GNN(Single_GNN):

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_Dep_GNN(embedding_dimension=self.embedding_dimension,
                                      vocab_size=self.vocab_size,
                                      graph_max_nodes=self.graph_max_nodes,
                                      graph_max_depth=None,
                                      timesteps=self.timesteps,
                                      node_mlp_weights=self.node_mlp_weights,
                                      edge_mlp_weights=self.edge_mlp_weights,
                                      message_weights=self.message_weights,
                                      gate_mlp_weights=self.gate_mlp_weights,
                                      aggregation_mlp_weights=self.aggregation_mlp_weights,
                                      representation_mlp_weights=self.representation_mlp_weights,
                                      l2_regularization=self.l2_regularization,
                                      dropout_rate=self.dropout_rate,
                                      embedding_matrix=text_info['embedding_matrix'],
                                      input_dropout_rate=self.input_dropout_rate,
                                      num_classes=text_info['num_labels'],
                                      rnn_encoding=self.rnn_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Single_Char_GNN(Single_GNN):

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.graph_max_chars = text_info['max_graph_chars']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_Char_GNN(embedding_dimension=self.embedding_dimension,
                                       vocab_size=self.vocab_size['word'],
                                       graph_max_nodes=self.graph_max_nodes,
                                       graph_max_depth=self.graph_max_depth,
                                       timesteps=self.timesteps,
                                       node_mlp_weights=self.node_mlp_weights,
                                       edge_mlp_weights=self.edge_mlp_weights,
                                       message_weights=self.message_weights,
                                       gate_mlp_weights=self.gate_mlp_weights,
                                       aggregation_mlp_weights=self.aggregation_mlp_weights,
                                       representation_mlp_weights=self.representation_mlp_weights,
                                       l2_regularization=self.l2_regularization,
                                       dropout_rate=self.dropout_rate,
                                       input_dropout_rate=self.input_dropout_rate,
                                       embedding_matrix=text_info['embedding_matrix'],
                                       num_classes=text_info['num_labels'],
                                       max_graph_chars=self.graph_max_chars,
                                       char_vocab_size=self.vocab_size['char'],
                                       rnn_encoding=self.rnn_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Single_Dep_Char_GNN(Single_Dep_GNN):

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_chars = text_info['max_graph_chars']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Single_Dep_Char_GNN(embedding_dimension=self.embedding_dimension,
                                           vocab_size=self.vocab_size['word'],
                                           graph_max_nodes=self.graph_max_nodes,
                                           graph_max_depth=None,
                                           timesteps=self.timesteps,
                                           node_mlp_weights=self.node_mlp_weights,
                                           edge_mlp_weights=self.edge_mlp_weights,
                                           message_weights=self.message_weights,
                                           gate_mlp_weights=self.gate_mlp_weights,
                                           aggregation_mlp_weights=self.aggregation_mlp_weights,
                                           representation_mlp_weights=self.representation_mlp_weights,
                                           l2_regularization=self.l2_regularization,
                                           dropout_rate=self.dropout_rate,
                                           input_dropout_rate=self.input_dropout_rate,
                                           embedding_matrix=text_info['embedding_matrix'],
                                           num_classes=text_info['num_labels'],
                                           max_graph_chars=self.graph_max_chars,
                                           char_vocab_size=self.vocab_size['char'],
                                           rnn_encoding=self.rnn_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Pairwise_GNN(Single_GNN):

    def __init__(self, pairwise_margin, cluster_args, cluster_method,
                 prediction_mode='max', prediction_args=None, **kwargs):
        super(Pairwise_GNN, self).__init__(answer_weights=None, **kwargs)
        self.pairwise_margin = pairwise_margin
        self.cluster_args = cluster_args
        self.cluster_method_str = cluster_method
        self.prediction_mode = prediction_mode
        self.prediction_args = prediction_args

        if hasattr(cluster, cluster_method):
            self.cluster_method = getattr(cluster, cluster_method)(**cluster_args)
        else:
            raise AttributeError('Invalid cluster method! Got: {}'.format(cluster_method))
        self.cluster_args = cluster_args

    def build_model(self, text_info):

        self.graph_max_nodes = text_info['max_graph_nodes']
        self.graph_max_depth = text_info['max_graph_depth']
        self.vocab_size = text_info['vocab_size']
        self.label_map = text_info['label_map']

        self.model = M_Pairwise_GNN(embedding_dimension=self.embedding_dimension,
                                    vocab_size=self.vocab_size,
                                    graph_max_nodes=self.graph_max_nodes,
                                    graph_max_depth=self.graph_max_depth,
                                    timesteps=self.timesteps,
                                    node_mlp_weights=self.node_mlp_weights,
                                    edge_mlp_weights=self.edge_mlp_weights,
                                    message_weights=self.message_weights,
                                    gate_mlp_weights=self.gate_mlp_weights,
                                    aggregation_mlp_weights=self.aggregation_mlp_weights,
                                    representation_mlp_weights=self.representation_mlp_weights,
                                    input_dropout_rate=self.input_dropout_rate,
                                    l2_regularization=self.l2_regularization,
                                    dropout_rate=self.dropout_rate,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    rnn_encoding=self.rnn_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, match, training=False, additional_info=None):
        # [batch_size, #labels]
        logits, _ = self.model(x, training=training)
        match = tf.cast(match, logits.dtype)
        match = tf.reshape(match, logits.shape)

        # Good pairs -> match = 1 -> logits > margin
        # Bad pairs -> match = -1 -> logits < - margin
        pairwise_margin_loss = tf.maximum(0., self.pairwise_margin - match * logits)
        pairwise_margin_loss = tf.reduce_mean(pairwise_margin_loss)

        total_loss = pairwise_margin_loss
        loss_info = {
            'pairwise_margin_loss': pairwise_margin_loss
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        return total_loss, loss_info

    def _parse_predictions(self, predictions):
        return predictions

    def get_cluster_info(self, x):

        if self.cluster_method_str == 'MiniBatchKMeans':
            centroids = self.cluster_method.cluster_centers_
            closest_indexes, _ = pairwise_distances_argmin_min(centroids, x, metric='cosine')
        elif self.cluster_method_str == 'AgglomerativeClustering':
            n_clusters = np.unique(self.cluster_method.labels_).shape[0]
            centroids = [np.mean(x[self.cluster_method.labels_ == cluster_idx], axis=0) for cluster_idx in
                         np.arange(n_clusters)]
            centroids = np.array(centroids)
            closest_indexes, _ = pairwise_distances_argmin_min(centroids, x, metric=self.cluster_args['affinity'])
        else:
            raise NotImplementedError('Specified clustering method is currently not supported ;)')

        return centroids, closest_indexes

    def determine_prediction(self, similarity):
        if self.prediction_mode == 'max':
            predictions = np.max(similarity, axis=1)
        elif self.prediction_mode == 'voting':
            predictions = np.mean(get_top_n_values2d(similarity, n=self.prediction_args['voting_members']), axis=1)
        else:
            raise RuntimeError('Invalid prediction mode! Got: {}'.format(self.prediction_mode))

        # Apply threshold
        predictions[predictions >= self.pairwise_margin] = 1
        predictions[predictions < self.pairwise_margin] = 0

        return predictions

    def prepare_inference(self, knowledge_data, steps, np_knowledge_y, batch_size):

        total_centroids, total_closest_points, total_closest_indexes = [], [], []

        mapped_pairwise_labels = [np.argmax(self.label_map[item]) for item in self.additional_data['pairwise_labels']]

        # Retrieve representations
        for label in mapped_pairwise_labels:
            filtered_knowledge_data = knowledge_data.unbatch().filter(
                lambda x, y: tf.equal(tf.argmax(y), tf.constant(label)))
            filtered_knowledge_data = filtered_knowledge_data.batch(batch_size)
            filtered_knowledge_data = transform_to_pairwise(filtered_knowledge_data, is_callable=False)
            filtered_knowledge_data = iter(filtered_knowledge_data)
            filter_indexes = np.argwhere(np.argmax(np_knowledge_y, axis=1) == label).ravel()
            filtered_steps = int((filter_indexes.shape[0] * steps) / np_knowledge_y.shape[0])

            total_representations = []

            for batch_idx in tqdm(range(filtered_steps), leave=True, position=0):
                _, model_additional_info = self.batch_predict(x=next(filtered_knowledge_data)[0])
                total_representations.extend(model_additional_info['left_representation'].numpy())

            total_representations = np.array(total_representations)

            # Cluster

            self.cluster_method.fit(total_representations)
            centroids, closest_indexes = self.get_cluster_info(total_representations)

            total_centroids.append(centroids)
            total_closest_points.append(total_representations[closest_indexes])
            total_closest_indexes.append(closest_indexes)

        return np.array(total_centroids), \
               np.array(total_closest_points), \
               np.array(total_closest_indexes)


class Multi_Adj_GNN(Single_Adj_GNN):

    def build_model(self, text_info):
        self.graph_max_nodes = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Multi_Adj_GNN(embedding_dimension=self.embedding_dimension,
                                     vocab_size=self.vocab_size,
                                     graph_max_nodes=self.graph_max_nodes,
                                     graph_max_depth=None,
                                     gcn_info=self.gcn_info,
                                     representation_mlp_weights=self.representation_mlp_weights,
                                     l2_regularization=self.l2_regularization,
                                     dropout_rate=self.dropout_rate,
                                     embedding_matrix=text_info['embedding_matrix'],
                                     input_dropout_rate=self.input_dropout_rate,
                                     num_classes=text_info['num_labels'],
                                     rnn_encoding=self.rnn_encoding,
                                     use_cluster_regularization=self.use_cluster_regularization,
                                     use_link_regularization=self.use_link_regularization,
                                     use_sigmoid=self.use_sigmoid)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Baseline_Sum(Network):

    def __init__(self, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_Sum, self).__init__(**kwargs)
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def loss_op(self, x, targets, training=False, additional_info=None):
        # [batch_size, #labels]
        logits, _ = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                logits=logits)

        # build weights for unbalanced classification
        weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
        target_classes = tf.argmax(targets, axis=1)
        for cls, weight in self.class_weights.items():
            to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
            weights = tf.where(target_classes == cls, to_fill, weights)

        cross_entropy *= weights
        cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        return total_loss, loss_info

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Baseline_Sum(sentence_size=self.sentence_size,
                                    vocab_size=self.vocab_size,
                                    answer_weights=self.answer_weights,
                                    embedding_dimension=self.embedding_dimension,
                                    l2_regularization=self.l2_regularization,
                                    dropout_rate=self.dropout_rate,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    num_labels=text_info['num_labels'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Baseline_Dense(Network):

    def __init__(self, answer_weights, dense_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_Dense, self).__init__(**kwargs)
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data
        self.dense_weights = dense_weights

    def loss_op(self, x, targets, training=False, additional_info=None):
        logits, _ = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                logits=logits)
        cross_entropy = tf.reduce_mean(cross_entropy)

        additional_losses = tf.reduce_sum(self.model.losses)

        total_loss = cross_entropy + additional_losses

        return total_loss, {'cross_entropy': cross_entropy,
                            'l2_regularization': additional_losses}

    def _parse_predictions(self, predictions):
        depth = predictions.shape[-1]
        predictions = tf.math.argmax(predictions, axis=1)
        predictions = tf.one_hot(predictions, depth=depth)
        return predictions

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Baseline_Dense(sentence_size=self.sentence_size,
                                      vocab_size=self.vocab_size,
                                      answer_weights=self.answer_weights,
                                      embedding_dimension=self.embedding_dimension,
                                      l2_regularization=self.l2_regularization,
                                      dropout_rate=self.dropout_rate,
                                      embedding_matrix=text_info['embedding_matrix'],
                                      dense_weights=self.dense_weights,
                                      num_classes=text_info['num_labels'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Baseline_LSTM(Network):

    def __init__(self, lstm_weights, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, weight_predictions=True, **kwargs):
        super(Baseline_LSTM, self).__init__(**kwargs)
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data
        self.weight_predictions = weight_predictions

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Baseline_LSTM(sentence_size=self.sentence_size,
                                     vocab_size=self.vocab_size,
                                     lstm_weights=self.lstm_weights,
                                     answer_weights=self.answer_weights,
                                     embedding_dimension=self.embedding_dimension,
                                     l2_regularization=self.l2_regularization,
                                     dropout_rate=self.dropout_rate,
                                     embedding_matrix=text_info['embedding_matrix'],
                                     num_classes=text_info['num_labels'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, additional_info=None):
        logits, _ = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        if self.is_multilabel:
            # [batch_size, #labels]
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            if self.weight_predictions:
                weights = tf.zeros_like(cross_entropy)
                for cls, cls_weights in self.class_weights.items():
                    cls_values = tf.where(targets[:, cls] == 1., cls_weights[1], cls_weights[0])
                    cls_values = tf.expand_dims(tf.cast(cls_values, cross_entropy.dtype), -1)
                    cls_values_mask = tf.expand_dims(tf.one_hot(cls, depth=targets.shape[1]), 0)
                    weights += cls_values * cls_values_mask

                cross_entropy *= weights

            cross_entropy = tf.reduce_sum(cross_entropy, axis=1)
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)
        else:

            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)

        additional_losses = tf.reduce_sum(self.model.losses)

        total_loss = cross_entropy + additional_losses

        return total_loss, total_loss, {'cross_entropy': cross_entropy,
                                        'l2_regularization': additional_losses}

    def _parse_predictions(self, predictions):
        if self.is_multilabel:
            predictions = tf.nn.sigmoid(predictions)
            predictions = tf.round(predictions)
        else:
            depth = predictions.shape[-1]
            predictions = tf.math.argmax(predictions, axis=1)
            predictions = tf.one_hot(predictions, depth=depth)

        return predictions


class BERT(Network):

    def __init__(self, config_args, preloaded_name, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 is_bert_trainable=False,
                 additional_data=None, weight_predictions=True, **kwargs):
        super(BERT, self).__init__(**kwargs)
        self.preloaded_model_name = preloaded_name
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data
        self.weight_predictions = weight_predictions
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = BertConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_BERT(answer_weights=self.answer_weights,
                            l2_regularization=self.l2_regularization,
                            dropout_rate=self.dropout_rate,
                            num_classes=text_info['num_labels'],
                            bert_config=self.bert_config,
                            preloaded_model_name=self.preloaded_model_name,
                            is_bert_trainable=self.is_bert_trainable)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, additional_info=None):
        logits, _ = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        if self.is_multilabel:
            # [batch_size, #labels]
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            if self.weight_predictions:
                weights = tf.zeros_like(cross_entropy)
                for cls, cls_weights in self.class_weights.items():
                    cls_values = tf.where(targets[:, cls] == 1., cls_weights[1], cls_weights[0])
                    cls_values = tf.expand_dims(tf.cast(cls_values, cross_entropy.dtype), -1)
                    cls_values_mask = tf.expand_dims(tf.one_hot(cls, depth=targets.shape[1]), 0)
                    weights += cls_values * cls_values_mask

                cross_entropy *= weights

            cross_entropy = tf.reduce_sum(cross_entropy, axis=1)
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)
        else:
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)

        additional_losses = tf.reduce_sum(self.model.losses)

        total_loss = cross_entropy + additional_losses

        return total_loss, total_loss, {'cross_entropy': cross_entropy,
                                        'l2_regularization': additional_losses}

    def _parse_predictions(self, predictions):
        if self.is_multilabel:
            predictions = tf.nn.sigmoid(predictions)
            predictions = tf.round(predictions)
        else:
            depth = predictions.shape[-1]
            predictions = tf.math.argmax(predictions, axis=1)
            predictions = tf.one_hot(predictions, depth=depth)

        return predictions


class Pos_BERT(BERT):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_BERT, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Pos_BERT(answer_weights=self.answer_weights,
                                l2_regularization=self.l2_regularization,
                                dropout_rate=self.dropout_rate,
                                num_classes=text_info['num_labels'],
                                bert_config=self.bert_config,
                                preloaded_model_name=self.preloaded_model_name,
                                use_position_feature=self.use_position_feature,
                                is_bert_trainable=self.is_bert_trainable)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Pos_Baseline_LSTM(Baseline_LSTM):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Baseline_LSTM, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_baseline_LSTM(sentence_size=self.sentence_size,
                                         vocab_size=self.vocab_size,
                                         lstm_weights=self.lstm_weights,
                                         answer_weights=self.answer_weights,
                                         embedding_dimension=self.embedding_dimension,
                                         l2_regularization=self.l2_regularization,
                                         dropout_rate=self.dropout_rate,
                                         embedding_matrix=text_info['embedding_matrix'],
                                         num_classes=text_info['num_labels'],
                                         use_position_feature=self.use_position_feature)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Graph_Baseline_LSTM(Network):

    def __init__(self, lstm_weights, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, weight_predictions=True, **kwargs):
        super(Graph_Baseline_LSTM, self).__init__(**kwargs)
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data
        self.weight_predictions = weight_predictions

    def build_model(self, text_info):
        self.sentence_size = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Graph_Baseline_LSTM(sentence_size=self.sentence_size,
                                           vocab_size=self.vocab_size,
                                           lstm_weights=self.lstm_weights,
                                           answer_weights=self.answer_weights,
                                           embedding_dimension=self.embedding_dimension,
                                           l2_regularization=self.l2_regularization,
                                           dropout_rate=self.dropout_rate,
                                           embedding_matrix=text_info['embedding_matrix'],
                                           num_classes=text_info['num_labels'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, additional_info=None):
        logits, _ = self.model(x, training=training)
        targets = tf.cast(targets, logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        if self.is_multilabel:
            # [batch_size, #labels]
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            if self.weight_predictions:
                weights = tf.zeros_like(cross_entropy)
                for cls, cls_weights in self.class_weights.items():
                    cls_values = tf.where(targets[:, cls] == 1., cls_weights[1], cls_weights[0])
                    cls_values = tf.expand_dims(tf.cast(cls_values, cross_entropy.dtype), -1)
                    cls_values_mask = tf.expand_dims(tf.one_hot(cls, depth=targets.shape[1]), 0)
                    weights += cls_values * cls_values_mask

                cross_entropy *= weights

            cross_entropy = tf.reduce_sum(cross_entropy, axis=1)
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)
        else:

            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights
            cross_entropy = tf.nn.compute_average_loss(cross_entropy, global_batch_size=self.batch_size)

        additional_losses = tf.reduce_sum(self.model.losses)

        total_loss = cross_entropy + additional_losses

        return total_loss, total_loss, {'cross_entropy': cross_entropy,
                                        'l2_regularization': additional_losses}

    def _parse_predictions(self, predictions):
        if self.is_multilabel:
            predictions = tf.nn.sigmoid(predictions)
            predictions = tf.round(predictions)
        else:
            depth = predictions.shape[-1]
            predictions = tf.math.argmax(predictions, axis=1)
            predictions = tf.one_hot(predictions, depth=depth)

        return predictions


class Pos_Graph_Baseline_LSTM(Graph_Baseline_LSTM):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Graph_Baseline_LSTM, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.sentence_size = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_Graph_Baseline_LSTM(sentence_size=self.sentence_size,
                                               vocab_size=self.vocab_size,
                                               lstm_weights=self.lstm_weights,
                                               answer_weights=self.answer_weights,
                                               embedding_dimension=self.embedding_dimension,
                                               l2_regularization=self.l2_regularization,
                                               dropout_rate=self.dropout_rate,
                                               embedding_matrix=text_info['embedding_matrix'],
                                               num_classes=text_info['num_labels'],
                                               use_position_feature=self.use_position_feature)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Dual_Graph_Baseline_LSTM(Graph_Baseline_LSTM):

    def __init__(self, self_attention_weights, **kwargs):
        super(Dual_Graph_Baseline_LSTM, self).__init__(**kwargs)
        self.self_attention_weights = self_attention_weights

    def build_model(self, text_info):
        self.sentence_size = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = M_Dual_Graph_Baseline_LSTM(sentence_size=self.sentence_size,
                                                vocab_size=self.vocab_size,
                                                lstm_weights=self.lstm_weights,
                                                answer_weights=self.answer_weights,
                                                self_attention_weights=self.self_attention_weights,
                                                embedding_dimension=self.embedding_dimension,
                                                l2_regularization=self.l2_regularization,
                                                dropout_rate=self.dropout_rate,
                                                embedding_matrix=text_info['embedding_matrix'],
                                                num_classes=text_info['num_labels'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Pos_Dual_Graph_Baseline_LSTM(Dual_Graph_Baseline_LSTM):

    def __init__(self, use_position_feature=False, **kwargs):
        super(Pos_Dual_Graph_Baseline_LSTM, self).__init__(**kwargs)
        self.use_position_feature = use_position_feature

    def build_model(self, text_info):
        self.sentence_size = text_info['max_graph_nodes']
        self.vocab_size = text_info['vocab_size']
        self.batch_size = text_info['batch_size']

        self.model = Pos_M_Dual_Graph_Baseline_LSTM(sentence_size=self.sentence_size,
                                                    vocab_size=self.vocab_size,
                                                    lstm_weights=self.lstm_weights,
                                                    answer_weights=self.answer_weights,
                                                    self_attention_weights=self.self_attention_weights,
                                                    embedding_dimension=self.embedding_dimension,
                                                    l2_regularization=self.l2_regularization,
                                                    dropout_rate=self.dropout_rate,
                                                    embedding_matrix=text_info['embedding_matrix'],
                                                    num_classes=text_info['num_labels'],
                                                    use_position_feature=self.use_position_feature)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class ModelFactory(object):
    supported_models = {
        'ibm2015_experimental_single_gnn_v2': Single_GNN,
        'acl2018_experimental_single_gnn_v2': Single_GNN,
        'ukp_experimental_single_gnn_v2': Single_GNN,
        'persuasive_essays_experimental_single_gnn_v2': Single_GNN,

        'ibm2015_experimental_single_dual_gnn_v2': Single_Dual_GNN,
        'acl2018_experimental_single_dual_gnn_v2': Single_Dual_GNN,
        'ukp_experimental_single_dual_gnn_v2': Single_Dual_GNN,
        'persuasive_essays_experimental_single_dual_gnn_v2': Single_Dual_GNN,

        'ibm2015_experimental_single_adj_gnn_v2': Single_Adj_GNN,
        'acl2018_experimental_single_adj_gnn_v2': Single_Adj_GNN,
        'ukp_experimental_single_adj_gnn_v2': Single_Adj_GNN,
        'persuasive_essays_experimental_single_adj_gnn_v2': Pos_Single_Adj_GNN,
        'abst_rct_experimental_single_adj_gnn_v2': Pos_Single_Adj_GNN,

        'ibm2015_experimental_dual_adj_gnn_v2': Dual_Adj_GNN,
        'acl2018_experimental_dual_adj_gnn_v2': Dual_Adj_GNN,
        'ukp_experimental_dual_adj_gnn_v2': Dual_Adj_GNN,
        'persuasive_essays_experimental_dual_adj_gnn_v2': Pos_Dual_Adj_GNN,
        'abst_rct_experimental_dual_adj_gnn_v2': Dual_Adj_GNN,

        'ibm2015_experimental_single_dep_adj_gnn_v2': Single_Adj_GNN,
        'acl2018_experimental_single_dep_adj_gnn_v2': Single_Adj_GNN,
        'ukp_experimental_single_dep_adj_gnn_v2': Single_Adj_GNN,
        'persuasive_essays_experimental_single_dep_adj_gnn_v2': Single_Adj_GNN,
        'abst_rct_experimental_single_dep_adj_gnn_v2': Single_Adj_GNN,

        'ibm2015_experimental_single_pooled_adj_gnn_v2': Single_Adj_GNN,
        'acl2018_experimental_single_pooled_adj_gnn_v2': Single_Adj_GNN,
        'ukp_experimental_single_pooled_adj_gnn_v2': Single_Adj_GNN,
        'persuasive_essays_experimental_single_pooled_adj_gnn_v2': Pos_Single_Adj_GNN,
        'abst_rct_experimental_single_pooled_adj_gnn_v2': Pos_Single_Adj_GNN,

        'ibm2015_experimental_dual_pooled_adj_gnn_v2': Dual_Adj_GNN,
        'acl2018_experimental_dual_pooled_adj_gnn_v2': Dual_Adj_GNN,
        'ukp_experimental_dual_pooled_adj_gnn_v2': Dual_Adj_GNN,
        'persuasive_essays_experimental_dual_pooled_adj_gnn_v2': Pos_Dual_Adj_GNN,
        'abst_rct_experimental_dual_pooled_adj_gnn_v2': Dual_Adj_GNN,

        'ibm2015_experimental_single_pooled_dep_adj_gnn_v2': Single_Adj_GNN,
        'acl2018_experimental_single_pooled_dep_adj_gnn_v2': Single_Adj_GNN,
        'ukp_experimental_single_pooled_dep_adj_gnn_v2': Single_Adj_GNN,
        'abst_rct_experimental_single_pooled_dep_adj_gnn_v2': Single_Adj_GNN,
        'persuasive_essays_experimental_single_pooled_dep_adj_gnn_v2': Pos_Single_Adj_GNN,

        'ibm2015_experimental_single_pooled_reg_adj_gnn_v2': Single_Adj_GNN,
        'acl2018_experimental_single_pooled_reg_adj_gnn_v2': Single_Adj_GNN,
        'ukp_experimental_single_pooled_reg_adj_gnn_v2': Single_Adj_GNN,
        'persuasive_essays_experimental_single_pooled_reg_adj_gnn_v2': Single_Adj_GNN,

        'ibm2015_experimental_single_tree_pooled_adj_gnn_v2': Single_Tree_Pooled_Adj_GNN,
        'acl2018_experimental_single_tree_pooled_adj_gnn_v2': Single_Tree_Pooled_Adj_GNN,
        'ukp_experimental_single_tree_pooled_adj_gnn_v2': Single_Tree_Pooled_Adj_GNN,
        'persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2': Pos_Single_Tree_Pooled_Adj_GNN,
        'abst_rct_experimental_single_tree_pooled_adj_gnn_v2': Pos_Single_Tree_Pooled_Adj_GNN,

        'ibm2015_experimental_dual_tree_pooled_adj_gnn_v2': Dual_Tree_Pooled_Adj_GNN,
        'acl2018_experimental_dual_tree_pooled_adj_gnn_v2': Dual_Tree_Pooled_Adj_GNN,
        'ukp_experimental_dual_tree_pooled_adj_gnn_v2': Dual_Tree_Pooled_Adj_GNN,
        'persuasive_essays_experimental_dual_tree_pooled_adj_gnn_v2': Pos_Dual_Tree_Pooled_Adj_GNN,
        'abst_rct_experimental_dual_tree_pooled_adj_gnn_v2': Dual_Tree_Pooled_Adj_GNN,

        'ibm2015_experimental_single_tree_pooled_dep_adj_gnn_v2': Single_Tree_Pooled_Adj_GNN,
        'acl2018_experimental_single_tree_pooled_dep_adj_gnn_v2': Single_Tree_Pooled_Adj_GNN,
        'ukp_experimental_single_tree_pooled_dep_adj_gnn_v2': Single_Tree_Pooled_Adj_GNN,
        'persuasive_essays_experimental_single_tree_pooled_dep_adj_gnn_v2': Pos_Single_Tree_Pooled_Adj_GNN,
        'abst_rct_experimental_single_tree_pooled_dep_adj_gnn_v2': Pos_Single_Tree_Pooled_Adj_GNN,

        'ibm2015_experimental_single_pooled_reg_dep_adj_gnn_v2': Single_Adj_GNN,
        'acl2018_experimental_single_pooled_reg_dep_adj_gnn_v2': Single_Adj_GNN,
        'ukp_experimental_single_pooled_reg_dep_adj_gnn_v2': Single_Adj_GNN,
        'persuasive_essays_experimental_single_pooled_dep_reg_adj_gnn_v2': Single_Adj_GNN,

        'ibm2015_experimental_single_hierarchical_gnn_v2': Single_Hierarchical_GNN,
        'acl2018_experimental_single_hierarchical_gnn_v2': Single_Hierarchical_GNN,
        'ukp_experimental_single_hierarchical_gnn_v2': Single_Hierarchical_GNN,
        'persuasive_essays_experimental_single_hierarchical_gnn_v2': Single_Hierarchical_GNN,

        'ibm2015_experimental_single_hierarchical_dep_gnn_v2': Single_Hierarchical_Dep_GNN,
        'acl2018_experimental_single_hierarchical_dep_gnn_v2': Single_Hierarchical_Dep_GNN,
        'ukp_experimental_single_hierarchical_dep_gnn_v2': Single_Hierarchical_Dep_GNN,
        'persuasive_essays_experimental_single_hierarchical_dep_gnn_v2': Single_Hierarchical_Dep_GNN,

        'ibm2015_experimental_single_dep_gnn_v2': Single_Dep_GNN,
        'acl2018_experimental_single_dep_gnn_v2': Single_Dep_GNN,
        'ukp_experimental_single_dep_gnn_v2': Single_Dep_GNN,
        'persuasive_essays_experimental_single_dep_gnn_v2': Single_Dep_GNN,

        'ibm2015_experimental_single_char_dep_gnn_v2': Single_Dep_Char_GNN,
        'acl2018_experimental_single_char_dep_gnn_v2': Single_Dep_Char_GNN,
        'ukp_experimental_single_char_dep_gnn_v2': Single_Dep_Char_GNN,
        'persuasive_essays_experimental_single_char_dep_gnn_v2': Single_Dep_Char_GNN,

        'ibm2015_experimental_single_char_gnn_v2': Single_Char_GNN,
        'acl2018_experimental_single_char_gnn_v2': Single_Char_GNN,
        'ukp_experimental_single_char_gnn_v2': Single_Char_GNN,
        'persuasive_essays_experimental_single_char_gnn_v2': Single_Char_GNN,

        'ibm2015_experimental_pairwise_gnn_v2': Pairwise_GNN,
        'acl2018_experimental_pairwise_gnn_v2': Pairwise_GNN,
        'ukp_experimental_pairwise_gnn_v2': Pairwise_GNN,
        'persuasive_essays_experimental_pairwise_gnn_v2': Pairwise_GNN,

        'ibm2015_experimental_baseline_sum_v2': Baseline_Sum,
        'acl2018_experimental_baseline_sum_v2': Baseline_Sum,
        'ukp_experimental_baseline_sum_v2': Baseline_Sum,
        'persuasive_essays_experimental_baseline_sum_v2': Baseline_Sum,

        'ibm2015_experimental_baseline_lstm_v2': Baseline_LSTM,
        'persuasive_essays_experimental_baseline_lstm_v2': Pos_Baseline_LSTM,
        'ukp_experimental_baseline_lstm_v2': Baseline_LSTM,
        'abst_rct_experimental_baseline_lstm_v2': Pos_Baseline_LSTM,

        'ibm2015_experimental_graph_baseline_lstm_v2': Graph_Baseline_LSTM,
        'ukp_experimental_graph_baseline_lstm_v2': Graph_Baseline_LSTM,
        'persuasive_essays_experimental_graph_baseline_lstm_v2': Pos_Graph_Baseline_LSTM,
        'abst_rct_experimental_graph_baseline_lstm_v2': Pos_Graph_Baseline_LSTM,

        'ibm2015_experimental_dep_graph_baseline_lstm_v2': Graph_Baseline_LSTM,
        'ukp_experimental_dep_graph_baseline_lstm_v2': Graph_Baseline_LSTM,
        'persuasive_essays_experimental_dep_graph_baseline_lstm_v2': Pos_Graph_Baseline_LSTM,
        'abst_rct_experimental_dep_graph_baseline_lstm_v2': Pos_Graph_Baseline_LSTM,

        'ibm2015_experimental_dual_graph_baseline_lstm_v2': Dual_Graph_Baseline_LSTM,
        'ukp_experimental_dual_graph_baseline_lstm_v2': Dual_Graph_Baseline_LSTM,
        'persuasive_essays_experimental_dual_graph_baseline_lstm_v2': Pos_Dual_Graph_Baseline_LSTM,
        'abst_rct_experimental_dual_graph_baseline_lstm_v2': Pos_Dual_Graph_Baseline_LSTM,

        'ibm2015_experimental_dual_dep_graph_baseline_lstm_v2': Dual_Graph_Baseline_LSTM,
        'ukp_experimental_dual_dep_graph_baseline_lstm_v2': Dual_Graph_Baseline_LSTM,
        'persuasive_essays_experimental_dual_dep_graph_baseline_lstm_v2': Pos_Dual_Graph_Baseline_LSTM,
        'abst_rct_experimental_dual_dep_graph_baseline_lstm_v2': Pos_Dual_Graph_Baseline_LSTM,

        "ibm2015_experimental_baseline_dense_v2": Baseline_Dense,

        "ukp_aspect_experimental_multi_adj_gnn_v2": Multi_Adj_GNN,

        "ibm2015_bert-base-uncased": BERT,
        "ukp_bert-base-uncased": BERT,
        "persuasive_essays_bert-base-uncased": Pos_BERT,
        "abst_rct_bert-base-uncased": BERT
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ModelFactory.supported_models[key]:
            return ModelFactory.supported_models[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
