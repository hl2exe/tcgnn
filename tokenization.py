# coding=utf-8
# Copyright 2019 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""
Tokenization classes implementation.
The file is forked from:
https://github.com/google-research/bert/blob/master/tokenization.py.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import os

import numpy as np
import tensorflow as tf

from utility.embedding_utils import build_embeddings_matrix, load_embedding_model
from utility.graph_utils import retrieve_trees_info, retrieve_dep_trees_info
from utility.log_utils import Logger
from transformers import BertTokenizer


class Tokenizer(object):

    def __init__(self, build_embedding_matrix=False, embedding_dimension=None,
                 embedding_model_type=None, merge_vocabularies=False):
        if build_embedding_matrix:
            assert embedding_model_type is not None
            assert embedding_dimension is not None and type(embedding_dimension) == int

        self.build_embedding_matrix = build_embedding_matrix
        self.embedding_dimension = embedding_dimension
        self.embedding_model_type = embedding_model_type
        self.embedding_model = None
        self.embedding_matrix = None
        self.merge_vocabularies = merge_vocabularies
        self.vocab = None

    def build_vocab(self, data, **kwargs):
        raise NotImplementedError()

    def initalize_with_vocab(self, vocab):
        raise NotImplementedError()

    def tokenize(self, text):
        raise NotImplementedError()

    def convert_tokens_to_ids(self, tokens):
        raise NotImplementedError()

    def convert_ids_to_tokens(self, ids):
        raise NotImplementedError()

    def get_info(self):
        return {
            'build_embedding_matrix': self.build_embedding_matrix,
            'embedding_dimension': self.embedding_dimension,
            'embedding_model_type': self.embedding_model_type,
            'embedding_matrix': self.embedding_matrix,
            'embedding_model': self.embedding_model,
            'vocab_size': len(self.vocab) + 1,
            'vocab': self.vocab
        }

    def show_info(self, info=None):

        info = info if info is not None else self.get_info()
        info = {key: value for key, value in info.items() if key != 'vocab'}

        Logger.get_logger(__name__).info('Tokenizer info: {}'.format(info))

    def save_info(self, filepath, prefix=None):
        save_name = 'tokenizer_info'
        if prefix:
            save_name += '_{}'.format(prefix)

        filepath = os.path.join(filepath, '{}.npy'.format(save_name))
        np.save(filepath, self.get_info())

    @staticmethod
    def load_info(filepath, prefix=None):
        load_name = 'tokenizer_info'
        if prefix:
            load_name += '_{}'.format(prefix)

        filepath = os.path.join(filepath, '{}.npy'.format(load_name))
        return np.load(filepath, allow_pickle=True).item()


class KerasTokenizer(Tokenizer):

    def __init__(self, tokenizer_args=None, **kwargs):
        super(KerasTokenizer, self).__init__(**kwargs)

        tokenizer_args = {} if tokenizer_args is None else tokenizer_args

        assert isinstance(tokenizer_args, dict) or isinstance(tokenizer_args, collections.OrderedDict)

        self.tokenizer_args = tokenizer_args

    def build_vocab(self, data, **kwargs):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)
        self.tokenizer.fit_on_texts(data)
        self.vocab = self.tokenizer.word_index

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, self.vocab = build_embeddings_matrix(vocab_size=len(self.vocab) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=self.vocab,
                                                                        merge_vocabularies=self.merge_vocabularies)

            self.tokenizer.word_index = self.vocab

    def initalize_with_vocab(self, vocab):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)
        self.tokenizer.word_index = vocab

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, self.vocab = build_embeddings_matrix(vocab_size=len(vocab) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=vocab,
                                                                        merge_vocabularies=self.merge_vocabularies)

            self.tokenizer.word_index = self.vocab

    def tokenize(self, text):
        return text

    def convert_tokens_to_ids(self, tokens):
        if type(tokens) == str:
            return self.tokenizer.texts_to_sequences([tokens])[0]
        else:
            return self.tokenizer.texts_to_sequences(tokens)

    def convert_ids_to_tokens(self, ids):
        return self.tokenizer.sequences_to_texts(ids)


class KerasTreeTokenizer(KerasTokenizer):

    def distinguish_POS_tokens(self, nodes):
        """
        Distinguish POS tags (upper case) from sentence tokens for separate embeddings
        """

        flattened_nodes = [item for node_seq in nodes for item in node_seq]
        return [item.lower() if not item.isupper() else item for item in flattened_nodes]

    def build_vocab(self, data, **kwargs):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(lower=False, **self.tokenizer_args)
        graph_info, _ = retrieve_trees_info(data)
        graph_nodes = graph_info[0]

        graph_nodes = self.distinguish_POS_tokens(graph_nodes)
        self.tokenizer.fit_on_texts(graph_nodes)
        self.vocab = self.tokenizer.word_index

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, self.vocab = build_embeddings_matrix(vocab_size=len(self.vocab) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=self.vocab,
                                                                        merge_vocabularies=self.merge_vocabularies)

            self.tokenizer.word_index = self.vocab


class KerasDepTreeTokenizer(KerasTreeTokenizer):

    def distinguish_POS_tokens(self, nodes):
        """
        Distinguish POS tags (upper case) from sentence tokens for separate embeddings
        """

        token_nodes = nodes[0]
        features = nodes[1]

        flattened_nodes = [item.lower() for node_seq in token_nodes for item in node_seq]
        return flattened_nodes + features

    def build_vocab(self, data, **kwargs):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(lower=False, **self.tokenizer_args)
        graph_info, _ = retrieve_dep_trees_info(data)
        graph_nodes = graph_info[0]
        graph_features = graph_info[2]

        graph_nodes = self.distinguish_POS_tokens([graph_nodes, graph_features])
        self.tokenizer.fit_on_texts(graph_nodes)
        self.vocab = self.tokenizer.word_index

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, self.vocab = build_embeddings_matrix(vocab_size=len(self.vocab) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=self.vocab,
                                                                        merge_vocabularies=self.merge_vocabularies)

            self.tokenizer.word_index = self.vocab


class KerasTreeCharTokenizer(KerasTreeTokenizer):

    def build_vocab(self, data, **kwargs):
        self.word_tokenizer = tf.keras.preprocessing.text.Tokenizer(lower=False, **self.tokenizer_args)
        self.char_tokenizer = tf.keras.preprocessing.text.Tokenizer(lower=False, **self.tokenizer_args)

        graph_info, _ = retrieve_trees_info(data)
        graph_nodes = graph_info[0]
        graph_nodes = self.distinguish_POS_tokens(graph_nodes)
        graph_char_nodes = set([node_char for word in graph_nodes for node_char in word])

        self.word_tokenizer.fit_on_texts(graph_nodes)
        self.char_tokenizer.fit_on_texts(graph_char_nodes)

        self.vocab = {
            "word": self.word_tokenizer.word_index,
            "char": self.char_tokenizer.word_index
        }

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, word_vocab = build_embeddings_matrix(vocab_size=len(self.vocab['word']) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=self.vocab['word'],
                                                                        merge_vocabularies=self.merge_vocabularies)
            self.vocab['word'] = word_vocab
            self.word_tokenizer.word_index = word_vocab

    def initalize_with_vocab(self, vocab):
        self.word_tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)
        self.char_tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)

        self.word_tokenizer.word_index = vocab['word']
        self.char_tokenizer.word_index = vocab['char']

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, word_vocab = build_embeddings_matrix(vocab_size=len(self.vocab['word']) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=self.vocab['word'],
                                                                        merge_vocabularies=self.merge_vocabularies)
            self.vocab['word'] = word_vocab
            self.word_tokenizer.word_index = word_vocab

    def tokenize(self, text):
        return text, [item for item in text]

    def convert_tokens_to_ids(self, tokens):

        assert type(tokens) in [list, dict]

        if type(tokens) == list:
            word_tokens = tokens[0]
            char_tokens = tokens[1]
        else:
            word_tokens = tokens['word']
            char_tokens = tokens['char']

        if type(word_tokens) == str:
            word_sequences = self.word_tokenizer.texts_to_sequences([word_tokens])[0]
        else:
            word_sequences = self.word_tokenizer.texts_to_sequences(word_tokens)

        char_sequences = self.char_tokenizer.texts_to_sequences(char_tokens)

        return word_sequences, char_sequences

    def convert_ids_to_tokens(self, ids):

        assert type(ids) in [list, dict]

        if type(ids) == list:
            word_ids = ids[0]
            char_ids = ids[1]
        else:
            word_ids = ids['word']
            char_ids = ids['char']

        return self.word_tokenizer.sequences_to_texts(word_ids), \
               self.char_tokenizer.sequences_to_texts(char_ids)

    def get_info(self):
        info = super(KerasTreeCharTokenizer, self).get_info()
        info['vocab_size'] = {key: len(value) + 1 for key, value in self.vocab.items()}
        return info


class KerasDepTreeCharTokenizer(KerasTreeCharTokenizer):

    def distinguish_POS_tokens(self, nodes):
        """
        Distinguish POS tags (upper case) from sentence tokens for separate embeddings
        """

        token_nodes = nodes[0]
        features = nodes[1]

        flattened_nodes = [item.lower() for node_seq in token_nodes for item in node_seq]
        return flattened_nodes + features

    def build_vocab(self, data, **kwargs):
        self.word_tokenizer = tf.keras.preprocessing.text.Tokenizer(lower=False, **self.tokenizer_args)
        self.char_tokenizer = tf.keras.preprocessing.text.Tokenizer(lower=False, **self.tokenizer_args)

        graph_info, _ = retrieve_dep_trees_info(data)
        graph_nodes = graph_info[0]
        graph_features = graph_info[2]
        graph_nodes = self.distinguish_POS_tokens([graph_nodes, graph_features])
        graph_char_nodes = set([node_char for word in graph_nodes for node_char in word])

        self.word_tokenizer.fit_on_texts(graph_nodes)
        self.char_tokenizer.fit_on_texts(graph_char_nodes)

        self.vocab = {
            "word": self.word_tokenizer.word_index,
            "char": self.char_tokenizer.word_index
        }

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, word_vocab = build_embeddings_matrix(vocab_size=len(self.vocab['word']) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=self.vocab['word'],
                                                                        merge_vocabularies=self.merge_vocabularies)
            self.vocab['word'] = word_vocab
            self.word_tokenizer.word_index = word_vocab


class BertTokenizerWrapper(Tokenizer):

    def __init__(self, preloaded_name, **kwargs):
        super(BertTokenizerWrapper, self).__init__(**kwargs)
        self.tokenizer = BertTokenizer.from_pretrained(preloaded_name)
        self.vocab = self.tokenizer.get_vocab()
        self.vocab_size = len(self.vocab)
        self.preloaded_model_name = preloaded_name

    def build_vocab(self, data, **kwargs):
        pass

    def tokenize(self, text):
        return self.tokenizer(text)

    def get_info(self):
        info = super(BertTokenizerWrapper, self).get_info()
        info['tokenizer'] = self

        return info

    @classmethod
    def from_pretrained(cls, preloaded_name):
        tokenizer = BertTokenizerWrapper(preloaded_name)
        return tokenizer

    def initialize_with_info(self, info):
        self.tokenizer = BertTokenizer.from_pretrained(self.preloaded_model_name)
        self.tokenizer.word_index = info['vocab']

    @property
    def sep_token(self):
        return self.tokenizer.sep_token

    @property
    def sep_token_id(self):
        return self.tokenizer.sep_token_id

    @property
    def pad_token(self):
        return self.tokenizer.pad_token

    @property
    def pad_token_id(self):
        return self.tokenizer.pad_token_id


class TokenizerFactory(object):
    supported_tokenizers = {
        'keras_tokenizer': KerasTokenizer,
        'keras_tree_tokenizer': KerasTreeTokenizer,
        'keras_dep_tree_tokenizer': KerasDepTreeTokenizer,
        'keras_tree_char_tokenizer': KerasTreeCharTokenizer,
        'keras_dep_tree_char_tokenizer': KerasDepTreeCharTokenizer,

        'bert_tokenizer': BertTokenizerWrapper,
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if TokenizerFactory.supported_tokenizers[key]:
            return TokenizerFactory.supported_tokenizers[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
