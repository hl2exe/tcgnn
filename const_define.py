import os

NAME_LOG = 'daily_log.log'

# Algorithms

SUPPORTED_LEARNERS = ['sentence_wise', 'context_wise']
SUPPORTED_ALGORITHMS = {
    'ibm2015_experimental_single_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_dual_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_dual_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_dual_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_dual_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_dual_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_dual_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_dual_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_dual_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_single_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_single_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_dual_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_dual_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_dual_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_dual_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_dual_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_dual_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_dual_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_dual_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_dual_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_dual_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_dep_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_dep_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_dep_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_dep_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_single_dep_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_single_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_pooled_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_pooled_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_pooled_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_pooled_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_single_pooled_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_single_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_dual_pooled_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_dual_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_dual_pooled_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_dual_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_dual_pooled_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_dual_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_dual_pooled_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_dual_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_dual_pooled_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_dual_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_pooled_dep_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_pooled_dep_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_pooled_dep_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_pooled_dep_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_single_pooled_dep_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_single_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_tree_pooled_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_tree_pooled_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_tree_pooled_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_single_tree_pooled_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_single_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_dual_tree_pooled_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_dual_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_dual_tree_pooled_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_dual_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_dual_tree_pooled_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_dual_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_dual_tree_pooled_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_dual_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_dual_tree_pooled_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_dual_tree_pooled_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_tree_pooled_dep_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_tree_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_tree_pooled_dep_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_tree_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_tree_pooled_dep_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_tree_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_tree_pooled_dep_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_tree_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "abst_rct_experimental_single_tree_pooled_dep_adj_gnn_v2": {
        'save_suffix': 'abst_rct_experimental_single_tree_pooled_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_pooled_reg_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_pooled_reg_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_pooled_reg_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_pooled_reg_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_pooled_reg_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_pooled_reg_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_pooled_reg_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_pooled_reg_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_pooled_reg_dep_adj_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_pooled_reg_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_pooled_reg_dep_adj_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_pooled_reg_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_pooled_reg_dep_adj_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_pooled_reg_dep_adj_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_pooled_reg_dep_adj_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_pooled_dep_reg_adj_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_hierarchical_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_hierarchical_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_hierarchical_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_hierarchical_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_hierarchical_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_hierarchical_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_hierarchical_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_hierarchical_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_hierarchical_dep_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_hierarchical_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_hierarchical_dep_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_hierarchical_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    "ukp_experimental_single_hierarchical_dep_gnn_v2": {
        'save_suffix': 'ukp_experimental_single_hierarchical_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    "persuasive_essays_experimental_single_hierarchical_dep_gnn_v2": {
        'save_suffix': 'persuasive_essays_experimental_single_hierarchical_dep_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_dep_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_dep_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'ukp_experimental_single_dep_gnn_v2': {
        'save_suffix': 'ukp_experimental_single_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'persuasive_essays_experimental_single_dep_gnn_v2': {
        'save_suffix': 'persuasive_essays_experimental_single_dep_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_char_dep_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_char_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_char_dep_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_char_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'ukp_experimental_single_char_dep_gnn_v2': {
        'save_suffix': 'ukp_experimental_single_char_dep_gnn_v2',
        'model_type': 'context_wise'
    },
    'persuasive_essays_experimental_single_char_dep_gnn_v2': {
        'save_suffix': 'persuasive_essays_experimental_single_char_dep_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_single_char_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_single_char_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_single_char_gnn_v2': {
        'save_suffix': 'acl2018_experimental_single_char_gnn_v2',
        'model_type': 'context_wise'
    },
    'ukp_experimental_single_char_gnn_v2': {
        'save_suffix': 'ukp_experimental_single_char_gnn_v2',
        'model_type': 'context_wise'
    },
    'persuasive_essays_experimental_single_char_gnn_v2': {
        'save_suffix': 'persuasive_essays_experimental_single_char_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015_experimental_pairwise_gnn_v2': {
        'save_suffix': 'ibm2015_experimental_pairwise_gnn_v2',
        'model_type': 'context_wise'
    },
    'acl2018_experimental_pairwise_gnn_v2': {
        'save_suffix': 'acl2018_experimental_pairwise_gnn_v2',
        'model_type': 'context_wise'
    },
    'ukp_experimental_pairwise_gnn_v2': {
        'save_suffix': 'ukp_experimental_pairwise_gnn_v2',
        'model_type': 'context_wise'
    },
    'persuasive_essays_experimental_pairwise_gnn_v2': {
        'save_suffix': 'persuasive_essays_experimental_pairwise_gnn_v2',
        'model_type': 'context_wise'
    },

    'ibm2015__experimental_baseline_sum_v2': {
        'save_suffix': 'ibm2015_experimental_baseline_sum_v2',
        'model_type': 'sentence_wise'
    },
    'acl2018_experimental_baseline_sum_v2': {
        'save_suffix': 'acl2018_experimental_baseline_sum_v2',
        'model_type': 'sentence_wise'
    },
    'ukp_experimental_baseline_sum_v2': {
        'save_suffix': 'ukp_experimental_baseline_sum_v2',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_experimental_baseline_sum_v2': {
        'save_suffix': 'persuasive_essays_experimental_baseline_sum_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_baseline_lstm_v2': {
        'save_suffix': 'ibm2015_experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'acl2018_experimental_baseline_lstm_v2': {
        'save_suffix': 'acl2018_experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'ukp_experimental_baseline_lstm_v2': {
        'save_suffix': 'ukp_experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_experimental_baseline_lstm_v2': {
        'save_suffix': 'persuasive_essays_experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'abst_rct_experimental_baseline_lstm_v2': {
        'save_suffix': 'abst_rct_experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },

    'ukp_experimental_baseline_lstm_elmo_v2': {
        'save_suffix': 'ukp_experimental_baseline_lstm_elmo_v2',
        'model_type': 'sentence_wise'
    },

    'ukp_experimental_baseline_lstm_bert_v2': {
        'save_suffix': 'ukp_experimental_baseline_lstm_bert_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_bert-base-uncased': {
        'save_suffix': 'ibm2015_bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'ukp_bert-base-uncased': {
        'save_suffix': 'ukp_bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_bert-base-uncased': {
        'save_suffix': 'persuasive_essays_bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'abst_rct_bert-base-uncased': {
        'save_suffix': 'abst_rct_bert-base-uncased',
        'model_type': 'sentence_wise'
    },

    'ukp_experimental_baseline_bert_large_v2': {
        'save_suffix': 'ukp_experimental_baseline_bert_large_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_graph_baseline_lstm_v2': {
        'save_suffix': 'ibm2015_experimental_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'ukp_experimental_graph_baseline_lstm_v2': {
        'save_suffix': 'ukp_experimental_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_experimental_graph_baseline_lstm_v2': {
        'save_suffix': 'persuasive_essays_experimental_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'abst_rct_experimental_graph_baseline_lstm_v2': {
        'save_suffix': 'abst_rct_experimental_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'ibm2015_experimental_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'ukp_experimental_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'ukp_experimental_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_experimental_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'persuasive_essays_experimental_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'abst_rct_experimental_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'abst_rct_experimental_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_dual_graph_baseline_lstm_v2': {
        'save_suffix': 'ibm2015_experimental_dual_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'ukp_experimental_dual_graph_baseline_lstm_v2': {
        'save_suffix': 'ukp_experimental_dual_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_experimental_dual_graph_baseline_lstm_v2': {
        'save_suffix': 'persuasive_essays_experimental_dual_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'abst_rct_experimental_dual_graph_baseline_lstm_v2': {
        'save_suffix': 'abst_rct_experimental_dual_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_dual_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'ibm2015_experimental_dual_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'ukp_experimental_dual_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'ukp_experimental_dual_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'persuasive_essays_experimental_dual_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'persuasive_essays_experimental_dual_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'abst_rct_experimental_dual_dep_graph_baseline_lstm_v2': {
        'save_suffix': 'abst_rct_experimental_dual_dep_graph_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_baseline_dense_v2': {
        'save_suffix': 'ibm2015_experimental_baseline_dense_v2',
        'model_type': 'sentence_wise'
    },

    "ukp_aspect_experimental_multi_adj_gnn_v2": {
        'save_suffix': 'ukp_aspect_experimental_multi_adj_gnn_v2',
        'model_type': 'sentence_wise'
    }
}

MODEL_CONFIG = {
    'ibm2015_experimental_single_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },
    'acl2018_experimental_single_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },
    'ukp_experimental_single_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },
    'persuasive_essays_experimental_single_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },

    'ibm2015_experimental_single_dual_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_converter'
    },
    'acl2018_experimental_single_dual_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_converter'
    },
    'ukp_experimental_single_dual_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_converter'
    },
    'persuasive_essays_experimental_single_dual_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_converter'
    },

    'ibm2015_experimental_single_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'acl2018_experimental_single_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'ukp_experimental_single_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'persuasive_essays_experimental_single_adj_gnn_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'abst_rct_experimental_single_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },

    'ibm2015_experimental_dual_adj_gnn_v2': {
        'processor': 'ml_text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'acl2018_experimental_dual_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'ukp_experimental_dual_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'persuasive_essays_experimental_dual_adj_gnn_v2': {
        'processor': 'pos_text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'abst_rct_experimental_dual_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },

    'ibm2015_experimental_single_dep_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'acl2018_experimental_single_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'ukp_experimental_single_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'persuasive_essays_experimental_single_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'abst_rct_experimental_single_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },

    'ibm2015_experimental_single_pooled_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'acl2018_experimental_single_pooled_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'ukp_experimental_single_pooled_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'persuasive_essays_experimental_single_pooled_adj_gnn_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'abst_rct_experimental_single_pooled_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },

    'ibm2015_experimental_dual_pooled_adj_gnn_v2': {
        'processor': 'ml_text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'acl2018_experimental_dual_pooled_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'ukp_experimental_dual_pooled_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'persuasive_essays_experimental_dual_pooled_adj_gnn_v2': {
        'processor': 'pos_text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'abst_rct_experimental_dual_pooled_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },

    'ibm2015_experimental_single_tree_pooled_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'acl2018_experimental_single_tree_pooled_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_tree_pooled_adj_converter'
    },
    'ukp_experimental_single_tree_pooled_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'persuasive_essays_experimental_single_tree_pooled_adj_gnn_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'abst_rct_experimental_single_tree_pooled_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },

    'ibm2015_experimental_dual_tree_pooled_adj_gnn_v2': {
        'processor': 'ml_text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'acl2018_experimental_dual_tree_pooled_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'ukp_experimental_dual_tree_pooled_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'persuasive_essays_experimental_dual_tree_pooled_adj_gnn_v2': {
        'processor': 'pos_text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },
    'abst_rct_experimental_dual_tree_pooled_adj_gnn_v2': {
        'processor': 'text_dual_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'dual_graph_adj_converter'
    },

    'ibm2015_experimental_single_pooled_dep_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'acl2018_experimental_single_pooled_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'ukp_experimental_single_pooled_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'persuasive_essays_experimental_single_pooled_dep_adj_gnn_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'abst_rct_experimental_single_pooled_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },

    'ibm2015_experimental_single_tree_pooled_dep_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'acl2018_experimental_single_tree_pooled__dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_tree_pooled_adj_converter'
    },
    'ukp_experimental_single_tree_pooled_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'persuasive_essays_experimental_single_tree_pooled_dep_adj_gnn_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },
    'abst_rct_experimental_single_tree_pooled_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_adj_converter'
    },

    'ibm2015_experimental_single_pooled_reg_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },
    'acl2018_experimental_single_pooled_reg_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },
    'ukp_experimental_single_pooled_reg_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },
    'persuasive_essays_experimental_single_pooled_reg_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },

    'ibm2015_experimental_single_pooled_reg_dep_adj_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },
    'acl2018_experimental_single_pooled_reg_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },
    'ukp_experimental_single_pooled_reg_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },
    'persuasive_essays_experimental_single_pooled_reg_dep_adj_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_pooled_adj_converter'
    },

    'ibm2015_experimental_single_hierarchical_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'hierarchical_graph_converter'
    },
    'acl2018_experimental_single_hierarchical_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'hierarchical_graph_converter'
    },
    'ukp_experimental_single_hierarchical_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'hierarchical_graph_converter'
    },
    'persuasive_essays_experimental_single_hierarchical_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'hierarchical_graph_converter'
    },

    'ibm2015_experimental_single_hierarchical_dep_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'hierarchical_graph_dep_converter'
    },
    'acl2018_experimental_single_hierarchical_dep_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'hierarchical_graph_dep_converter'
    },
    'ukp_experimental_single_hierarchical_dep_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'hierarchical_graph_dep_converter'
    },
    'persuasive_essays_experimental_single_hierarchical_dep_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'hierarchical_graph_dep_converter'
    },

    'acl2018_experimental_single_dep_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_dep_converter'
    },
    'ukp_experimental_single_dep_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_dep_converter'
    },
    'ibm2015_experimental_single_dep_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'graph_dep_converter'
    },

    'ukp_experimental_single_char_dep_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_char_tokenizer',
        'converter': 'graph_dep_char_converter'
    },

    'ibm2015_experimental_single_char_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_char_tokenizer',
        'converter': 'graph_char_converter'
    },
    'acl2018_experimental_single_char_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_char_tokenizer',
        'converter': 'graph_char_converter'
    },
    'ukp_experimental_single_char_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_char_tokenizer',
        'converter': 'graph_char_converter'
    },
    'persuasive_essays_experimental_single_char_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_char_tokenizer',
        'converter': 'graph_char_converter'
    },

    'ibm2015_experimental_pairwise_gnn_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },
    'acl2018_experimental_pairwise_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },
    'ukp_experimental_pairwise_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },
    'persuasive_essays_experimental_pairwise_gnn_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'graph_converter'
    },

    'ibm2015_experimental_baseline_sum_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'acl2018_experimental_baseline_sum_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'ukp_experimental_baseline_sum_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'persuasive_essays_experimental_baseline_sum_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ibm2015_experimental_baseline_lstm_v2': {
        'processor': 'ml_text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'acl2018_experimental_baseline_lstm_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'persuasive_essays_experimental_baseline_lstm_v2': {
        'processor': 'pos_text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'ukp_experimental_baseline_lstm_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'abst_rct_experimental_baseline_lstm_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ukp_experimental_baseline_lstm_elmo_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ukp_experimental_baseline_lstm_bert_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ibm2015_bert-base-uncased': {
        'processor': 'ml_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },
    'persuasive_essays_bert-base-uncased': {
        'processor': 'pos_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },
    'ukp_bert-base-uncased': {
        'processor': 'text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },
    'abst_rct_bert-base-uncased': {
        'processor': 'text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },

    'ukp_experimental_baseline_bert_large_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ibm2015_experimental_graph_baseline_lstm_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },
    'ukp_experimental_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },
    'persuasive_essays_experimental_graph_baseline_lstm_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },
    'abst_rct_experimental_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },

    'ibm2015_experimental_dep_graph_baseline_lstm_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },
    'ukp_experimental_dep_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },
    'persuasive_essays_experimental_dep_graph_baseline_lstm_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },
    'abst_rct_experimental_dep_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_graph_converter'
    },

    'ibm2015_experimental_dual_graph_baseline_lstm_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },
    'ukp_experimental_dual_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },
    'persuasive_essays_experimental_dual_graph_baseline_lstm_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },
    'abst_rct_experimental_dual_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },

    'ibm2015_experimental_dual_dep_graph_baseline_lstm_v2': {
        'processor': 'ml_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },
    'ukp_experimental_dual_dep_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },
    'persuasive_essays_experimental_dual_dep_graph_baseline_lstm_v2': {
        'processor': 'pos_text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },
    'abst_rct_experimental_dual_dep_graph_baseline_lstm_v2': {
        'processor': 'text_tree_processor',
        'tokenizer': 'keras_dep_tree_tokenizer',
        'converter': 'simple_dual_graph_converter'
    },

    'ibm2015_experimental_baseline_dense_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ukp_aspect_experimental_multi_adj_gnn_v2': {
        'processor': 'text_multi_tree_processor',
        'tokenizer': 'keras_tree_tokenizer',
        'converter': 'multi_graph_adj_converter'
    }
}

ALGORITHM_CONFIG = {
    'sentence_wise': {
        'processor': 'tos_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'context_wise': {
        'processor': 'tos_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    }
}

# DEFAULT DIRECTORIES
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIGS_DIR = os.path.join(PROJECT_DIR, 'configs')
INTERNAL_DATASETS_DIR = os.path.join(PROJECT_DIR, 'datasets')
SAVED_MODELS_DIR = os.path.join(PROJECT_DIR, 'saved_models')
SAVED_MODELS_ADDITIONAL_RESULTS_DIR = os.path.join(PROJECT_DIR, 'saved_models_additional_results')
INFERENCE_DIR = os.path.join(PROJECT_DIR, 'inference')
PATH_LOG = os.path.join(PROJECT_DIR, 'log')
REPORT_PATH = os.path.join(PROJECT_DIR, 'reports')
RUNNABLES_DIR = os.path.join(PROJECT_DIR, 'runnables')
LOCAL_DATASETS_DIR = os.path.join(PROJECT_DIR, 'local_database')
CALIBRATION_RESULTS_DIR = os.path.join(PROJECT_DIR, 'calibration_results')
EMBEDDING_MODELS_DIR = os.path.join(PROJECT_DIR, 'embedding_models')
PREBUILT_FOLDS_DIR = os.path.join(PROJECT_DIR, 'prebuilt_folds')
SAVED_INFO_DIR = os.path.join(PROJECT_DIR, 'saved_info')
MONGO_DB_DIR = os.path.join(PROJECT_DIR, 'mongo_db')
MARGOT_DIR = os.path.join(PROJECT_DIR, 'predictor')
TESTS_DATA_DIR = os.path.join(PROJECT_DIR, 'tests_data')

# EVALUATION METHOD DIRS
LOO_DIR = os.path.join(PROJECT_DIR, 'loo_test')
CV_DIR = os.path.join(PROJECT_DIR, 'cv_test')
TRAIN_AND_TEST_DIR = os.path.join(PROJECT_DIR, 'train_and_test')
UNSEEN_DATA_DIR = os.path.join(PROJECT_DIR, 'unseen_data_test')

# DATASET DIRS
IBM2015_DIR = os.path.join(LOCAL_DATASETS_DIR, 'IBM_Debater_(R)_CE-EMNLP-2015.v3')
ACL2018_DIR = os.path.join(LOCAL_DATASETS_DIR, 'IBMDebaterEvidenceSentences')
PERSUASIVE_ESSAYS_DIR = os.path.join(LOCAL_DATASETS_DIR, 'ArgumentAnnotatedEssays-2.0')
RCT_DIR = os.path.join(LOCAL_DATASETS_DIR, 'AbstRCT_corpus')
DR_INVENTOR_DIR = os.path.join(LOCAL_DATASETS_DIR, 'dr-inventor')
UKP_DIR = os.path.join(LOCAL_DATASETS_DIR, 'UKP_Sentential_Argument_Mining_Corpus_Complete')
UKP_ASPECT_DIR = os.path.join(LOCAL_DATASETS_DIR, 'UKP_ASPECT')
CORD19_DIR = os.path.join(LOCAL_DATASETS_DIR, 'CORD-19')
KB_DIR = os.path.join(LOCAL_DATASETS_DIR, 'KB')

# JSON FILES
JSON_CALLBACKS_NAME = 'callbacks.json'
JSON_MODEL_CONFIG_NAME = 'model_config.json'
JSON_MODEL_HISTORY_NAME = 'model_history.json'
JSON_GENERATOR_TRAINING_CONFIG_NAME = 'generator_training_config.json'
JSON_TRAINING_CONFIG_NAME = 'training_config.json'
JSON_LOO_TEST_CONFIG_NAME = 'loo_test_config.json'
JSON_TRAIN_AND_TEST_CONFIG_NAME = 'train_and_test_config.json'
JSON_LOO_FORWARD_TEST_CONFIG_NAME = 'loo_forward_test_config.json'
JSON_CV_TEST_CONFIG_NAME = 'cv_test_config.json'
JSON_DATA_LOADER_CONFIG_NAME = 'data_loader.json'
JSON_HYPEROPT_MODEL_GRIDSEARCH_NAME = 'hyperopt_model_gridsearch.json'
JSON_CALIBRATOR_INFO_NAME = 'calibrator_info.json'
JSON_VALIDATION_INFO_NAME = 'validation_info.json'
JSON_TEST_INFO_NAME = 'test_info.json'
JSON_TEST_PREDICTIONS_NAME = 'test_predictions.json'
JSON_TRAIN_PREDICTIONS_NAME = 'train_predictions.json'
JSON_VAL_PREDICTIONS_NAME = 'val_predictions.json'
JSON_DISTRIBUTED_MODEL_CONFIG_NAME = 'distributed_model_config.json'
JSON_DISTRIBUTED_CONFIG_NAME = 'distributed_config.json'
JSON_MODEL_DATA_CONFIGS_NAME = 'data_configs.json'
JSON_UNSEEN_PREDICTIONS_NAME = 'unseen_predictions.json'
JSON_QUICK_SETUP_CONFIG_NAME = "quick_setup_config.json"
